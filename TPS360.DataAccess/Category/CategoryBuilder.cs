﻿using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class CategoryBuilder : IEntityBuilder<Category >
    {
        IList<Category> IEntityBuilder<Category>.BuildEntities(IDataReader reader)
        {
            List<Category> category = new List<Category>();

            while (reader.Read())
            {
                category.Add(((IEntityBuilder<Category >)this).BuildEntity(reader));
            }

            return (category.Count > 0) ? category : null;
        }

        Category  IEntityBuilder<Category >.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_NAME = 1;


            Category category = new Category();

            category.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            category.Name = reader.IsDBNull(FLD_NAME) ? string.Empty : reader.GetString(FLD_NAME);
          

            return category ;
        }
    }
}