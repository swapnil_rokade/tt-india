﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class CommonNoteBuilder : IEntityBuilder<CommonNote>
    {
        IList<CommonNote> IEntityBuilder<CommonNote>.BuildEntities(IDataReader reader)
        {
            List<CommonNote> commonNotes = new List<CommonNote>();

            while (reader.Read())
            {
                commonNotes.Add(((IEntityBuilder<CommonNote>)this).BuildEntity(reader));
            }

            return (commonNotes.Count > 0) ? commonNotes : null;
        }

        CommonNote IEntityBuilder<CommonNote>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_NOTEDETAIL = 1;
            const int FLD_ISINTERNAL = 2;
            const int FLD_STATUS = 3;
            const int FLD_ISREMOVED = 4;
            const int FLD_NOTECATEGORYLOOKUPID = 5;
            const int FLD_CREATORID = 6;
            const int FLD_UPDATORID = 7;
            const int FLD_CREATEDATE = 8;
            const int FLD_UPDATEDATE = 9;
            const int FLD_JOBPOSTINGID = 10;
            const int FLD_JOBPOSTINGCODE = 11;
            const int FLD_JOBTITLE = 12;
            CommonNote commonNote = new CommonNote();

            commonNote.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            commonNote.NoteDetail = reader.IsDBNull(FLD_NOTEDETAIL) ? string.Empty : reader.GetString(FLD_NOTEDETAIL);
            commonNote.IsInternal = reader.IsDBNull(FLD_ISINTERNAL) ? false : reader.GetBoolean(FLD_ISINTERNAL);
            commonNote.Status = reader.IsDBNull(FLD_STATUS) ? 0 : reader.GetInt32(FLD_STATUS);
            commonNote.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            commonNote.NoteCategoryLookupId = reader.IsDBNull(FLD_NOTECATEGORYLOOKUPID) ? 0 : reader.GetInt32(FLD_NOTECATEGORYLOOKUPID);
            commonNote.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            commonNote.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            commonNote.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            commonNote.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            if (reader.FieldCount > FLD_JOBPOSTINGID)
            {
                commonNote.JobPostingId = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);
                commonNote.JobPostingCode = reader.IsDBNull(FLD_JOBPOSTINGCODE) ? string.Empty : reader.GetString(FLD_JOBPOSTINGCODE);
                commonNote.JobTitle = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
            }
            return commonNote;
        }
    }
}