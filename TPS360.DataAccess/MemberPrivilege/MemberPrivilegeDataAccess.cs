﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class MemberPrivilegeDataAccess : BaseDataAccess, IMemberPrivilegeDataAccess
    {
        #region Constructors

        public MemberPrivilegeDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberPrivilege> CreateEntityBuilder<MemberPrivilege>()
        {
            return (new MemberPrivilegeBuilder()) as IEntityBuilder<MemberPrivilege>;
        }

        #endregion

        #region  Methods

        MemberPrivilege IMemberPrivilegeDataAccess.Add(MemberPrivilege memberPrivilege)
        {
            const string SP = "dbo.MemberPrivilege_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Privilege", DbType.Int32, memberPrivilege.Privilege);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberPrivilege.IsRemoved);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberPrivilege.MemberId);
                Database.AddInParameter(cmd, "@CustomSiteMapId", DbType.Int32, memberPrivilege.CustomSiteMapId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberPrivilege.CreatorId);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberPrivilege = CreateEntityBuilder<MemberPrivilege>().BuildEntity(reader);
                    }
                    else
                    {
                        memberPrivilege = null;
                    }
                }

                if (memberPrivilege == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberPrivilege already exists. Please specify another memberPrivilege.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this memberPrivilege.");
                            }
                    }
                }

                return memberPrivilege;
            }
        }

        MemberPrivilege IMemberPrivilegeDataAccess.Update(MemberPrivilege memberPrivilege)
        {
            const string SP = "dbo.MemberPrivilege_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberPrivilege.Id);
                Database.AddInParameter(cmd, "@Privilege", DbType.Int32, memberPrivilege.Privilege);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberPrivilege.IsRemoved);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberPrivilege.MemberId);
                Database.AddInParameter(cmd, "@CustomSiteMapId", DbType.Int32, memberPrivilege.CustomSiteMapId);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberPrivilege.UpdatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberPrivilege = CreateEntityBuilder<MemberPrivilege>().BuildEntity(reader);
                    }
                    else
                    {
                        memberPrivilege = null;
                    }
                }

                if (memberPrivilege == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberPrivilege already exists. Please specify another memberPrivilege.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this memberPrivilege.");
                            }
                    }
                }

                return memberPrivilege;
            }
        }

        MemberPrivilege IMemberPrivilegeDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberPrivilege_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberPrivilege>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }
        bool IMemberPrivilegeDataAccess.DeleteByMemberIdWithOutAdminAccess(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberPrivilege_DeleteByMemberIdWithOutAdminAccess";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a privilege which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this privilege.");
                        }
                }
            }
        }

        IList<MemberPrivilege> IMemberPrivilegeDataAccess.GetAllByMemberId(int memberId)
        {
            const string SP = "dbo.MemberPrivilege_GetAllByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberPrivilege>().BuildEntities(reader);
                }
            }
        }

        IList<MemberPrivilege> IMemberPrivilegeDataAccess.GetAll()
        {
            const string SP = "dbo.MemberPrivilege_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberPrivilege>().BuildEntities(reader);
                }
            }
        }

        bool IMemberPrivilegeDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberPrivilege_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                    {
                        return true;
                    }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                    {
                        throw new ArgumentException("Cannot delete a memberPrivilege which has association.");
                    }
                    default:
                    {
                        throw new SystemException("An unexpected error has occurred while deleting this memberPrivilege.");
                    }
                }
            }
        }

        bool IMemberPrivilegeDataAccess.DeleteByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberPrivilege_DeleteByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                    {
                        return true;
                    }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                    {
                        throw new ArgumentException("Cannot delete a privilege which has association.");
                    }
                    default:
                    {
                        throw new SystemException("An unexpected error has occurred while deleting this privilege.");
                    }
                }
            }
        }
        bool IMemberPrivilegeDataAccess.GetMemberPrivilegeForMemberIdAndRequiredURL(int memberId,string Url)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }
            if (Url == string.Empty)
            {
                throw new ArgumentNullException("Url");
            }
            const string SP = "dbo.MemberPrivilege_GetByMemberIDAndURL";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                Database.AddInParameter(cmd, "@URL", DbType.AnsiString , Url);
                    using (IDataReader reader = Database.ExecuteReader(cmd))
                    {
                        while (reader.Read())
                        {
                            return reader.IsDBNull(0) ? false : reader.GetBoolean(0);
                        }

                        return false;
                    }
            }
        }

        void IMemberPrivilegeDataAccess.DeleteAll_MemberPrivilege()
        {
            const string SP = "dbo.MemberPrivilege_DeleteAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.ExecuteNonQuery(cmd);
            }
        }

        #endregion
    }
}