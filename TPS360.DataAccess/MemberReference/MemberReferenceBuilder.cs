﻿using System;
using System.Collections.Generic;
using System.Data;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberReferenceBuilder : IEntityBuilder<MemberReference>
    {
        IList<MemberReference> IEntityBuilder<MemberReference>.BuildEntities(IDataReader reader)
        {
            List<MemberReference> memberReferences = new List<MemberReference>();

            while (reader.Read())
            {
                memberReferences.Add(((IEntityBuilder<MemberReference>)this).BuildEntity(reader));
            }

            return (memberReferences.Count > 0) ? memberReferences : null;
        }

        MemberReference IEntityBuilder<MemberReference>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_REFERENCENAME = 1;
            const int FLD_REFERENCEPOSITION = 2;
            const int FLD_COMPANYNAME = 3;
            const int FLD_EMAIL = 4;
            const int FLD_PHONE = 5;
            const int FLD_PHONEEXTENSION = 6;
            const int FLD_CELLPHONE = 7;
            const int FLD_REMARK = 8;
            const int FLD_REFERENCECATEGORYLOOKUPID = 9;
            const int FLD_STATUS = 10;
            const int FLD_ISREMOVED = 11;
            const int FLD_MEMBERID = 12;
            const int FLD_CREATORID = 13;
            const int FLD_UPDATORID = 14;
            const int FLD_CREATEDATE = 15;
            const int FLD_UPDATEDATE = 16;

            MemberReference memberReference = new MemberReference();

            memberReference.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberReference.ReferenceName = reader.IsDBNull(FLD_REFERENCENAME) ? string.Empty : reader.GetString(FLD_REFERENCENAME);
            memberReference.ReferencePosition = reader.IsDBNull(FLD_REFERENCEPOSITION) ? string.Empty : reader.GetString(FLD_REFERENCEPOSITION);
            memberReference.CompanyName = reader.IsDBNull(FLD_COMPANYNAME) ? string.Empty : reader.GetString(FLD_COMPANYNAME);
            memberReference.Email = reader.IsDBNull(FLD_EMAIL) ? string.Empty : reader.GetString(FLD_EMAIL);
            memberReference.Phone = reader.IsDBNull(FLD_PHONE) ? string.Empty : reader.GetString(FLD_PHONE);
            memberReference.PhoneExtension = reader.IsDBNull(FLD_PHONEEXTENSION) ? string.Empty : reader.GetString(FLD_PHONEEXTENSION);
            memberReference.CellPhone = reader.IsDBNull(FLD_CELLPHONE) ? string.Empty : reader.GetString(FLD_CELLPHONE);
            memberReference.Remark = reader.IsDBNull(FLD_REMARK) ? string.Empty : reader.GetString(FLD_REMARK);
            memberReference.ReferenceCategoryLookupId = reader.IsDBNull(FLD_REFERENCECATEGORYLOOKUPID) ? 0 : reader.GetInt32(FLD_REFERENCECATEGORYLOOKUPID);
            memberReference.Status = reader.IsDBNull(FLD_STATUS) ? 0 : reader.GetInt32(FLD_STATUS);
            memberReference.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberReference.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberReference.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberReference.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberReference.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberReference.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return memberReference;
        }
    }
}