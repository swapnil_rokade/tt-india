﻿/*-------------------------------------------------------------------------------------------------------------   FileName: MemberReferenceDataAccess.cs
   Description: 
   Created By: 
   Created On:
   Modification Log:
   ------------------------------------------------------------------------------------------------------------   Ver.No.             Date                  Author              Modification
   ------------------------------------------------------------------------------------------------------------    0.1             Feb-02-2008             Sandeesh            Defect Fix - ID: 9376; Implemented sorting.
*/
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class MemberReferenceDataAccess : BaseDataAccess, IMemberReferenceDataAccess
    {
        #region Constructors

        public MemberReferenceDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberReference> CreateEntityBuilder<MemberReference>()
        {
            return (new MemberReferenceBuilder()) as IEntityBuilder<MemberReference>;
        }

        #endregion

        #region  Methods

        MemberReference IMemberReferenceDataAccess.Add(MemberReference memberReference)
        {
            const string SP = "dbo.MemberReference_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@ReferenceName", DbType.AnsiString, StringHelper.Convert(memberReference.ReferenceName));
                Database.AddInParameter(cmd, "@ReferencePosition", DbType.AnsiString, StringHelper.Convert(memberReference.ReferencePosition));
                Database.AddInParameter(cmd, "@CompanyName", DbType.AnsiString, StringHelper.Convert(memberReference.CompanyName));
                Database.AddInParameter(cmd, "@Email", DbType.AnsiString, StringHelper.Convert(memberReference.Email));
                Database.AddInParameter(cmd, "@Phone", DbType.AnsiString, StringHelper.Convert(memberReference.Phone));
                Database.AddInParameter(cmd, "@PhoneExtension", DbType.AnsiString, StringHelper.Convert(memberReference.PhoneExtension));
                Database.AddInParameter(cmd, "@CellPhone", DbType.AnsiString, StringHelper.Convert(memberReference.CellPhone));
                Database.AddInParameter(cmd, "@Remark", DbType.AnsiString, StringHelper.Convert(memberReference.Remark));
                Database.AddInParameter(cmd, "@ReferenceCategoryLookupId", DbType.Int32, memberReference.ReferenceCategoryLookupId);
                Database.AddInParameter(cmd, "@Status", DbType.Int32, memberReference.Status);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberReference.IsRemoved);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberReference.MemberId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberReference.CreatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberReference = CreateEntityBuilder<MemberReference>().BuildEntity(reader);
                    }
                    else
                    {
                        memberReference = null;
                    }
                }

                if (memberReference == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberReference already exists. Please specify another memberReference.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this memberReference.");
                            }
                    }
                }

                return memberReference;
            }
        }

        MemberReference IMemberReferenceDataAccess.Update(MemberReference memberReference)
        {
            const string SP = "dbo.MemberReference_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberReference.Id);
                Database.AddInParameter(cmd, "@ReferenceName", DbType.AnsiString, StringHelper.Convert(memberReference.ReferenceName));
                Database.AddInParameter(cmd, "@ReferencePosition", DbType.AnsiString, StringHelper.Convert(memberReference.ReferencePosition));
                Database.AddInParameter(cmd, "@CompanyName", DbType.AnsiString, StringHelper.Convert(memberReference.CompanyName));
                Database.AddInParameter(cmd, "@Email", DbType.AnsiString, StringHelper.Convert(memberReference.Email));
                Database.AddInParameter(cmd, "@Phone", DbType.AnsiString, StringHelper.Convert(memberReference.Phone));
                Database.AddInParameter(cmd, "@PhoneExtension", DbType.AnsiString, StringHelper.Convert(memberReference.PhoneExtension));
                Database.AddInParameter(cmd, "@CellPhone", DbType.AnsiString, StringHelper.Convert(memberReference.CellPhone));
                Database.AddInParameter(cmd, "@Remark", DbType.AnsiString, StringHelper.Convert(memberReference.Remark));
                Database.AddInParameter(cmd, "@ReferenceCategoryLookupId", DbType.Int32, memberReference.ReferenceCategoryLookupId);
                Database.AddInParameter(cmd, "@Status", DbType.Int32, memberReference.Status);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberReference.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberReference.UpdatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberReference = CreateEntityBuilder<MemberReference>().BuildEntity(reader);
                    }
                    else
                    {
                        memberReference = null;
                    }
                }

                if (memberReference == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberReference already exists. Please specify another memberReference.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this memberReference.");
                            }
                    }
                }

                return memberReference;
            }
        }

        MemberReference IMemberReferenceDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberReference_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberReference>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberReference> IMemberReferenceDataAccess.GetAll()
        {
            const string SP = "dbo.MemberReference_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberReference>().BuildEntities(reader);
                }
            }
        }

        IList<MemberReference> IMemberReferenceDataAccess.GetAllByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberReference_GetAllMemberReferenceByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberReference>().BuildEntities(reader);
                }
            }
        }

//0.1 start

        IList<MemberReference> IMemberReferenceDataAccess.GetAllByMemberId(int memberId, string sortExpression)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberReference_GetAllMemberReferenceByMemberId";

            string SortColumn = "ReferenceCategoryLookupId";
            string SortOrder = "ASC";
            string[] part = (string.IsNullOrEmpty(sortExpression)) ? null : sortExpression.Split(' ');

            if (part != null && part.Length > 0)
            {
                SortColumn = part[0];

                if (part.Length > 1 && part[1] != null)
                {
                    SortOrder = part[1];
                }
                else
                {
                    SortOrder = "ASC";
                }
            }

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                Database.AddInParameter(cmd, "@SortColumn", DbType.String, SortColumn);
                Database.AddInParameter(cmd, "@SortOrder", DbType.String, SortOrder);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberReference>().BuildEntities(reader);
                }
            }
        }

//0.1 end

        PagedResponse<MemberReference> IMemberReferenceDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.MemberReference_GetPaged";
            string whereClause = string.Empty;

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "ReferenceName";
            }

            request.SortColumn = "[MR].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberReference> response = new PagedResponse<MemberReference>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<MemberReference>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        bool IMemberReferenceDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberReference_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a memberReference which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this memberReference.");
                        }
                }
            }
        }

        string IMemberReferenceDataAccess.GetEmailById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberReference_GetEmailById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        return reader.IsDBNull(0) ? string.Empty : reader.GetString(0);
                    }
                    return string.Empty;
                }
            }
        }
        #endregion
    }
}