﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class MemberEmailDetailDataAccess : BaseDataAccess, IMemberEmailDetailDataAccess
    {
        #region Constructors

        public MemberEmailDetailDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberEmailDetail> CreateEntityBuilder<MemberEmailDetail>()
        {
            return (new MemberEmailDetailBuilder()) as IEntityBuilder<MemberEmailDetail>;
        }

        #endregion

        #region  Methods

        MemberEmailDetail IMemberEmailDetailDataAccess.Add(MemberEmailDetail memberEmailDetail)
        {
            const string SP = "dbo.MemberEmailDetail_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberEmailDetail.MemberId);
                Database.AddInParameter(cmd, "@EmailAddress", DbType.AnsiString, memberEmailDetail.EmailAddress);
                Database.AddInParameter(cmd, "@AddressTypeId", DbType.Int32, memberEmailDetail.AddressTypeId);
                Database.AddInParameter(cmd, "@SendingTypeId", DbType.Int32, memberEmailDetail.SendingTypeId);
                Database.AddInParameter(cmd, "@MemberEmailId", DbType.Int32, memberEmailDetail.MemberEmailId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberEmailDetail = CreateEntityBuilder<MemberEmailDetail>().BuildEntity(reader);
                    }
                    else
                    {
                        memberEmailDetail = null;
                    }
                }

                if (memberEmailDetail == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member email detail already exists. Please specify another member email detail.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this member email detail.");
                            }
                    }
                }

                return memberEmailDetail;
            }
        }

        MemberEmailDetail IMemberEmailDetailDataAccess.Update(MemberEmailDetail memberEmailDetail)
        {
            const string SP = "dbo.MemberEmailDetail_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberEmailDetail.Id);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberEmailDetail.MemberId);
                Database.AddInParameter(cmd, "@EmailAddress", DbType.AnsiString, memberEmailDetail.EmailAddress);
                Database.AddInParameter(cmd, "@AddressTypeId", DbType.Int32, memberEmailDetail.AddressTypeId);
                Database.AddInParameter(cmd, "@SendingTypeId", DbType.Int32, memberEmailDetail.SendingTypeId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberEmailDetail = CreateEntityBuilder<MemberEmailDetail>().BuildEntity(reader);
                    }
                    else
                    {
                        memberEmailDetail = null;
                    }
                }

                if (memberEmailDetail == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member email detail already exists. Please specify another member email detail.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this member email detail.");
                            }
                    }
                }

                return memberEmailDetail;
            }
        }

        MemberEmailDetail IMemberEmailDetailDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberEmailDetail_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberEmailDetail>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberEmailDetail> IMemberEmailDetailDataAccess.GetAllByMemberEmailId(int memberEmailId)
        {
            if (memberEmailId < 1)
            {
                throw new ArgumentNullException("memberEmailId");
            }

            const string SP = "dbo.MemberEmailDetail_GetAllMemberEmailDetailByMemberEmailId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberEmailId", DbType.Int32, memberEmailId);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberEmailDetail>().BuildEntities(reader);
                }
            }
        }

        IList<MemberEmailDetail> IMemberEmailDetailDataAccess.GetAll()
        {
            const string SP = "dbo.MemberEmailDetail_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberEmailDetail>().BuildEntities(reader);
                }
            }
        }

        bool IMemberEmailDetailDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberEmailDetail_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member email detail which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member email detail.");
                        }
                }
            }
        }

        bool IMemberEmailDetailDataAccess.DeleteByMemberEmailId(int memberEmailId)
        {
            if (memberEmailId < 1)
            {
                throw new ArgumentNullException("memberEmailId");
            }

            const string SP = "dbo.MemberEmailDetail_DeleteByMemberEmailId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberEmailId", DbType.Int32, memberEmailId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member email detail which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member email detail.");
                        }
                }
            }
        }

        #endregion
    }
}