﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;
namespace TPS360.DataAccess
{
    internal sealed class CandidateRequisitionStatusDataAccess: BaseDataAccess ,ICandidateRequisitionStatusDataAccess 
    {
             #region Constructors

        public CandidateRequisitionStatusDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<CandidateRequisitionStatusDataAccess> CreateEntityBuilder<CandidateRequisitionStatusDataAccess>()
        {
            return (new CandidateRequisitionStatusBuilder()) as IEntityBuilder<CandidateRequisitionStatusDataAccess>;
        }

        #endregion

        #region Methods
        void ICandidateRequisitionStatusDataAccess.Add(int JobPostingId, string MemberId, int CreatorId)
        {
            if (JobPostingId < 0)
            {
                throw new ArgumentException("JobPostingId");
            }
            if (MemberId == null && MemberId ==string .Empty )
            {
                throw new ArgumentException("MemberId");
            }
            if (CreatorId < 0)
            {
                throw new ArgumentException("CreatorId");
            }
            const string SP = "dbo.CandidateRequisitionStatus_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, JobPostingId);
                Database.AddInParameter(cmd, "@MemberId", DbType.AnsiString , MemberId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, CreatorId);
                Database.ExecuteNonQuery(cmd);
            }
        }

        void ICandidateRequisitionStatusDataAccess.Update(int JobPostingId, string MemberId, int CreatorId, int StatusId)
        {
            if (JobPostingId < 0)
            {
                throw new ArgumentException("JobPostingId");
            }
            //if (MemberId <0)
            //{
            //    throw new ArgumentException("MemberId");
            //}
            if (CreatorId < 0)
            {
                throw new ArgumentException("CreatorId");
            }
            if (StatusId < 0)
            {
                throw new ArgumentException("StatusId");
            }
            const string SP = "dbo.CandidateRequisitionStatus_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, JobPostingId);
                Database.AddInParameter(cmd, "@MemberId", DbType.AnsiString, MemberId);
                Database.AddInParameter(cmd, "@StatusId", DbType.Int32, StatusId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, CreatorId);
                Database.ExecuteNonQuery(cmd);
            }
        }

        CandidateRequisitionStatus ICandidateRequisitionStatusDataAccess.GetByRequisitionIdandMemberId(int RequisitionId, int MemberId)
        {
            if (RequisitionId < 0)
            {
                throw new ArgumentNullException("RequisitionId");
            }
            if (MemberId < 0)
            {
                throw new ArgumentNullException("MemberId");
            }
            const string SP = "dbo.CandidateRequisitionStatus_GetByRequisitionIdAndMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@RequisitionId", DbType.Int32, RequisitionId);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, MemberId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<CandidateRequisitionStatus>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        PagedResponse<CandidateRequisitionStatus> ICandidateRequisitionStatusDataAccess.GetPaged(PagedRequest request,int ManagerId)
        {
            const string SP = "dbo.CandidateRecentStatusChanges_GetPaged";
           
            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[CRS].CreateDate";
            }

            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(""),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder),
                                                    ManagerId 
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<CandidateRequisitionStatus> response = new PagedResponse<CandidateRequisitionStatus>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<CandidateRequisitionStatus>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }



        #endregion
    }
}
