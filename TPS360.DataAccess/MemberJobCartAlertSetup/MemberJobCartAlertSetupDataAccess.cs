﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class MemberJobCartAlertSetupDataAccess : BaseDataAccess, IMemberJobCartAlertSetupDataAccess
    {
        #region Constructors

        public MemberJobCartAlertSetupDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberJobCartAlertSetup> CreateEntityBuilder<MemberJobCartAlertSetup>()
        {
            return (new MemberJobCartAlertSetupBuilder()) as IEntityBuilder<MemberJobCartAlertSetup>;
        }

        #endregion

        #region  Methods

        MemberJobCartAlertSetup IMemberJobCartAlertSetupDataAccess.Add(MemberJobCartAlertSetup memberJobCartAlertSetup)
        {
            const string SP = "dbo.MemberJobCartAlertSetup_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@AlertTitle", DbType.AnsiString, memberJobCartAlertSetup.AlertTitle);
                Database.AddInParameter(cmd, "@IsEmailAlert", DbType.Boolean, memberJobCartAlertSetup.IsEmailAlert);
                Database.AddInParameter(cmd, "@IsSMSAlert", DbType.Boolean, memberJobCartAlertSetup.IsSMSAlert);
                Database.AddInParameter(cmd, "@JobTitle", DbType.AnsiString, memberJobCartAlertSetup.JobTitle);
                Database.AddInParameter(cmd, "@City", DbType.AnsiString, memberJobCartAlertSetup.City);
                Database.AddInParameter(cmd, "@CountryId", DbType.Int32, memberJobCartAlertSetup.CountryId);
                Database.AddInParameter(cmd, "@StateId", DbType.Int32, memberJobCartAlertSetup.StateId);
                Database.AddInParameter(cmd, "@SkillSet", DbType.AnsiString, memberJobCartAlertSetup.SkillSet);
                Database.AddInParameter(cmd, "@EmploymentLength", DbType.AnsiString, memberJobCartAlertSetup.EmploymentLength);
                Database.AddInParameter(cmd, "@SalaryRangeFrom", DbType.Decimal, memberJobCartAlertSetup.SalaryRangeFrom);
                Database.AddInParameter(cmd, "@SalaryRangeTo", DbType.Decimal, memberJobCartAlertSetup.SalaryRangeTo);
                Database.AddInParameter(cmd, "@SalaryRangeCurrencyLookupId", DbType.Int32, memberJobCartAlertSetup.SalaryRangeCurrencyLookupId);
                Database.AddInParameter(cmd, "@SalaryType", DbType.Int32, memberJobCartAlertSetup.SalaryType);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberJobCartAlertSetup.MemberId);
                Database.AddInParameter(cmd, "@AlertStatus", DbType.Int32, memberJobCartAlertSetup.AlertStatus);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberJobCartAlertSetup.IsRemoved);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberJobCartAlertSetup.CreatorId);                

                using ( IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberJobCartAlertSetup = CreateEntityBuilder<MemberJobCartAlertSetup>().BuildEntity(reader);
                    }
                    else
                    {
                        memberJobCartAlertSetup = null;
                    }
                }

                if (memberJobCartAlertSetup == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberJobCartAlertSetup already exists. Please specify another memberJobCartAlertSetup.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this memberJobCartAlertSetup.");
                            }
                    }
                }

                return memberJobCartAlertSetup;
            }
        }

        MemberJobCartAlertSetup IMemberJobCartAlertSetupDataAccess.Update(MemberJobCartAlertSetup memberJobCartAlertSetup)
        {
            const string SP = "dbo.MemberJobCartAlertSetup_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberJobCartAlertSetup.Id);
                Database.AddInParameter(cmd, "@AlertTitle", DbType.AnsiString, memberJobCartAlertSetup.AlertTitle);
                Database.AddInParameter(cmd, "@IsEmailAlert", DbType.Boolean, memberJobCartAlertSetup.IsEmailAlert);
                Database.AddInParameter(cmd, "@IsSMSAlert", DbType.Boolean, memberJobCartAlertSetup.IsSMSAlert);
                Database.AddInParameter(cmd, "@JobTitle", DbType.AnsiString, memberJobCartAlertSetup.JobTitle);
                Database.AddInParameter(cmd, "@City", DbType.AnsiString, memberJobCartAlertSetup.City);
                Database.AddInParameter(cmd, "@CountryId", DbType.Int32, memberJobCartAlertSetup.CountryId);
                Database.AddInParameter(cmd, "@StateId", DbType.Int32, memberJobCartAlertSetup.StateId);
                Database.AddInParameter(cmd, "@SkillSet", DbType.AnsiString, memberJobCartAlertSetup.SkillSet);
                Database.AddInParameter(cmd, "@EmploymentLength", DbType.AnsiString, memberJobCartAlertSetup.EmploymentLength);
                Database.AddInParameter(cmd, "@SalaryRangeFrom", DbType.Decimal, memberJobCartAlertSetup.SalaryRangeFrom);
                Database.AddInParameter(cmd, "@SalaryRangeTo", DbType.Decimal, memberJobCartAlertSetup.SalaryRangeTo);
                Database.AddInParameter(cmd, "@SalaryRangeCurrencyLookupId", DbType.Int32, memberJobCartAlertSetup.SalaryRangeCurrencyLookupId);
                Database.AddInParameter(cmd, "@SalaryType", DbType.Int32, memberJobCartAlertSetup.SalaryType);                
                Database.AddInParameter(cmd, "@AlertStatus", DbType.Int32, memberJobCartAlertSetup.AlertStatus);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberJobCartAlertSetup.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberJobCartAlertSetup.UpdatorId);                

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberJobCartAlertSetup = CreateEntityBuilder<MemberJobCartAlertSetup>().BuildEntity(reader);
                    }
                    else
                    {
                        memberJobCartAlertSetup = null;
                    }
                }

                if (memberJobCartAlertSetup == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberJobCartAlertSetup already exists. Please specify another memberJobCartAlertSetup.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this memberJobCartAlertSetup.");
                            }
                    }
                }

                return memberJobCartAlertSetup;
            }
        }

        MemberJobCartAlertSetup IMemberJobCartAlertSetupDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberJobCartAlertSetup_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberJobCartAlertSetup>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        MemberJobCartAlertSetup IMemberJobCartAlertSetupDataAccess.GetByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberJobCartAlertSetup_GetByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberJobCartAlertSetup>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberJobCartAlertSetup> IMemberJobCartAlertSetupDataAccess.GetAllByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberJobCartAlertSetup_GetAllMemberJobCartAlertSetupByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberJobCartAlertSetup>().BuildEntities(reader);
                }
            }
        }

        IList<MemberJobCartAlertSetup> IMemberJobCartAlertSetupDataAccess.GetAll()
        {
            const string SP = "dbo.MemberJobCartAlertSetup_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberJobCartAlertSetup>().BuildEntities(reader);
                }
            }
        }

        bool IMemberJobCartAlertSetupDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberJobCartAlertSetup_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a memberJobCartAlertSetup which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this memberJobCartAlertSetup.");
                        }
                }
            }
        }

        bool IMemberJobCartAlertSetupDataAccess.DeleteByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberJobCartAlertSetup_DeleteMemberJobCartAlertSetupByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a memberJobCartAlertSetup which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this memberJobCartAlertSetup.");
                        }
                }
            }
        }

        #endregion
    }
}