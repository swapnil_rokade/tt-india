﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class MemberJobCartAlertSetupBuilder : IEntityBuilder<MemberJobCartAlertSetup>
    {
        IList<MemberJobCartAlertSetup> IEntityBuilder<MemberJobCartAlertSetup>.BuildEntities(IDataReader reader)
        {
            List<MemberJobCartAlertSetup> memberJobCartAlertSetups = new List<MemberJobCartAlertSetup>();

            while (reader.Read())
            {
                memberJobCartAlertSetups.Add(((IEntityBuilder<MemberJobCartAlertSetup>)this).BuildEntity(reader));
            }

            return (memberJobCartAlertSetups.Count > 0) ? memberJobCartAlertSetups : null;
        }

        MemberJobCartAlertSetup IEntityBuilder<MemberJobCartAlertSetup>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_ALERTTITLE = 1;
            const int FLD_ISEMAILALERT = 2;
            const int FLD_ISSMSALERT = 3;
            const int FLD_JOBTITLE = 4;
            const int FLD_CITY = 5;
            const int FLD_COUNTRYID = 6;
            const int FLD_STATEID = 7;
            const int FLD_SKILLSET = 8;
            const int FLD_EMPLOYMENTLENGTH = 9;
            const int FLD_SALARYRANGEFROM = 10;
            const int FLD_SALARYRANGETO = 11;
            const int FLD_SALARYRANGECURRENCYLOOKUPID = 12;
            const int FLD_SALARYTYPE = 13;
            const int FLD_MEMBERID = 14;
            const int FLD_ALERTSTATUS = 15;
            const int FLD_ISREMOVED = 16;
            const int FLD_CREATORID = 17;
            const int FLD_UPDATORID = 18;
            const int FLD_CREATEDATE = 19;
            const int FLD_UPDATEDATE = 20;

            MemberJobCartAlertSetup memberJobCartAlertSetup = new MemberJobCartAlertSetup();

            memberJobCartAlertSetup.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberJobCartAlertSetup.AlertTitle = reader.IsDBNull(FLD_ALERTTITLE) ? string.Empty : reader.GetString(FLD_ALERTTITLE);
            memberJobCartAlertSetup.IsEmailAlert = reader.IsDBNull(FLD_ISEMAILALERT) ? false : reader.GetBoolean(FLD_ISEMAILALERT);
            memberJobCartAlertSetup.IsSMSAlert = reader.IsDBNull(FLD_ISSMSALERT) ? false : reader.GetBoolean(FLD_ISSMSALERT);
            memberJobCartAlertSetup.JobTitle = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
            memberJobCartAlertSetup.City = reader.IsDBNull(FLD_CITY) ? string.Empty : reader.GetString(FLD_CITY);
            memberJobCartAlertSetup.CountryId = reader.IsDBNull(FLD_COUNTRYID) ? 0 : reader.GetInt32(FLD_COUNTRYID);
            memberJobCartAlertSetup.StateId = reader.IsDBNull(FLD_STATEID) ? 0 : reader.GetInt32(FLD_STATEID);
            memberJobCartAlertSetup.SkillSet = reader.IsDBNull(FLD_SKILLSET) ? string.Empty : reader.GetString(FLD_SKILLSET);
            memberJobCartAlertSetup.EmploymentLength = reader.IsDBNull(FLD_EMPLOYMENTLENGTH) ? string.Empty : reader.GetString(FLD_EMPLOYMENTLENGTH);
            memberJobCartAlertSetup.SalaryRangeFrom = reader.IsDBNull(FLD_SALARYRANGEFROM) ? 0 : reader.GetDecimal(FLD_SALARYRANGEFROM);
            memberJobCartAlertSetup.SalaryRangeTo = reader.IsDBNull(FLD_SALARYRANGETO) ? 0 : reader.GetDecimal(FLD_SALARYRANGETO);
            memberJobCartAlertSetup.SalaryRangeCurrencyLookupId = reader.IsDBNull(FLD_SALARYRANGECURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_SALARYRANGECURRENCYLOOKUPID);
            memberJobCartAlertSetup.SalaryType = reader.IsDBNull(FLD_SALARYTYPE) ? 0 : reader.GetInt32(FLD_SALARYTYPE);
            memberJobCartAlertSetup.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberJobCartAlertSetup.AlertStatus = reader.IsDBNull(FLD_ALERTSTATUS) ? 0 : reader.GetInt32(FLD_ALERTSTATUS);
            memberJobCartAlertSetup.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberJobCartAlertSetup.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberJobCartAlertSetup.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberJobCartAlertSetup.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberJobCartAlertSetup.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return memberJobCartAlertSetup;
        }
    }
}