﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class StateDataAccess : BaseDataAccess, IStateDataAccess
    {
        #region Constructors

        public StateDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<State> CreateEntityBuilder<State>()
        {
            return (new StateBuilder()) as IEntityBuilder<State>;
        }

        #endregion

        #region  Methods

        State IStateDataAccess.Add(State state)
        {
            const string SP = "dbo.State_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Name", DbType.Binary, state.Name);
                Database.AddInParameter(cmd, "@StateCode", DbType.Binary, state.StateCode);
                Database.AddInParameter(cmd, "@CountryId", DbType.Int32, state.CountryId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        state = CreateEntityBuilder<State>().BuildEntity(reader);
                    }
                    else
                    {
                        state = null;
                    }
                }

                if (state == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("State already exists. Please specify another state.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this state.");
                            }
                    }
                }

                return state;
            }
        }

        State IStateDataAccess.Update(State state)
        {
            const string SP = "dbo.State_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, state.Id);
                Database.AddInParameter(cmd, "@Name", DbType.Binary, state.Name);
                Database.AddInParameter(cmd, "@StateCode", DbType.Binary, state.StateCode);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        state = CreateEntityBuilder<State>().BuildEntity(reader);
                    }
                    else
                    {
                        state = null;
                    }
                }

                if (state == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("State already exists. Please specify another state.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this state.");
                            }
                    }
                }

                return state;
            }
        }

        State IStateDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.State_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<State>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<State> IStateDataAccess.GetAll()
        {
            const string SP = "dbo.State_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<State>().BuildEntities(reader);
                }
            }
        }

        IList<State> IStateDataAccess.GetAllByCountryId(int countryId)
        {
            const string SP = "dbo.State_GetAllStateByCountryId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CountryId", DbType.Int32, countryId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<State>().BuildEntities(reader);
                }
            }
        }

        bool IStateDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.State_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a state which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this state.");
                        }
                }
            }
        }

        Int32 IStateDataAccess.GetStateIdByStateName(string stateName)
        {
            if (stateName==null)
            {
                stateName = ""; ;
            }

            const string SP = "dbo.State_GetStateIdByStateName";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@stateName", DbType.String, stateName);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    Int32 stateId = 0;

                    while (reader.Read())
                    {
                        stateId = reader.GetInt32(0);
                    }

                    return stateId;
                }
            }
        }

        string  IStateDataAccess.GetStateNameById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.State_GetStateNameById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        return reader.IsDBNull(0) ? string.Empty : reader.GetString(0);
                    }
                    return string.Empty;
                }
            }
        }
        #endregion
    }
}