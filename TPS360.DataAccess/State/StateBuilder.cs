﻿using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class StateBuilder : IEntityBuilder<State>
    {
        IList<State> IEntityBuilder<State>.BuildEntities(IDataReader reader)
        {
            List<State> states = new List<State>();

            while (reader.Read())
            {
                states.Add(((IEntityBuilder<State>)this).BuildEntity(reader));
            }

            return (states.Count > 0) ? states : null;
        }

        State IEntityBuilder<State>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_NAME = 1;
            const int FLD_STATECODE = 2;
            const int FLD_COUNTRYID = 3;

            State state = new State();

            state.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            state.Name = reader.IsDBNull(FLD_NAME) ? string.Empty : reader.GetString(FLD_NAME);
            state.StateCode = reader.IsDBNull(FLD_STATECODE) ? string.Empty : reader.GetString(FLD_STATECODE);
            state.CountryId = reader.IsDBNull(FLD_COUNTRYID) ? 0 : reader.GetInt32(FLD_COUNTRYID);

            return state;
        }
    }
}