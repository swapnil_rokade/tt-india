﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class ZipcodeDataAccess : BaseDataAccess, IZipCodeCityDataAccess
    {
        #region Constructors

        public ZipcodeDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<State> CreateEntityBuilder<State>()
        {
            return null;
        }

        #endregion

        #region  Methods      

        IList<string> IZipCodeCityDataAccess.GetAllCity(string prefixText)
        {
            const string SP = "dbo.ZipCodeCity_GetAll";
            if (prefixText ==null)
            {
                throw new ArgumentNullException("prefixText");
            }

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Keyword", DbType.String, prefixText);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                     
                    IList<string> cityValues = new List<string>();
                    while (reader.Read())
                    {
                        cityValues.Add(reader.GetString(0));
                    }
                    return cityValues;
                }
            }
        }

        IList<string> IZipCodeCityDataAccess.GetAllByCountryId(int countryId)
        {
            const string SP = "dbo.State_GetAllStateByCountryId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CountryId", DbType.Int32, countryId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return null;
                }
            }
        }

       
        
        #endregion
    }
}