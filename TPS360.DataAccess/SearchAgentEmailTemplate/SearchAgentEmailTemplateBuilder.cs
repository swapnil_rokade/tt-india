﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class SearchAgentEmailTemplateBuilder : IEntityBuilder<SearchAgentEmailTemplate>
    {
        IList<SearchAgentEmailTemplate> IEntityBuilder<SearchAgentEmailTemplate>.BuildEntities(IDataReader reader)
        {
            List<SearchAgentEmailTemplate> Template = new List<SearchAgentEmailTemplate>();

            while (reader.Read())
            {
                Template.Add(((IEntityBuilder<SearchAgentEmailTemplate>)this).BuildEntity(reader));
            }

            return (Template.Count > 0) ? Template : null;
        }

        SearchAgentEmailTemplate IEntityBuilder<SearchAgentEmailTemplate>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_SENDERID = 1;
            const int FLD_JOBPOSTINGID = 2;
            const int FLD_SUBJECT = 3;
            const int FLD_EMAILBODY = 4;
            const int FLD_ISREMOVED = 5;
            const int FLD_CREATORID = 6;
            const int FLD_UPDATORID = 7;
            const int FLD_CREATEDATE = 8;
            const int FLD_UPDATEDATE = 9;
            SearchAgentEmailTemplate template = new SearchAgentEmailTemplate();

            template.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            template.SenderId = reader.IsDBNull(FLD_SENDERID) ? 0 : reader.GetInt32(FLD_SENDERID);
            template.JobPostingID = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);
            template.Subject = reader.IsDBNull(FLD_SUBJECT) ? string.Empty : reader.GetString(FLD_SUBJECT);
            template.EmailBody = reader.IsDBNull(FLD_EMAILBODY) ? string.Empty : reader.GetString(FLD_EMAILBODY);
            template.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            template.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            template.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            template.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            template.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            return template;
        }
    }
}