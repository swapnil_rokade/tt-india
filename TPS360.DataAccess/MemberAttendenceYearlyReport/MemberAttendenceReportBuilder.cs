﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberAttendenceReportBuilder : IEntityBuilder<MemberAttendenceYearlyReport>
    {
        IList<MemberAttendenceYearlyReport> IEntityBuilder<MemberAttendenceYearlyReport>.BuildEntities(IDataReader reader)
        {
            List<MemberAttendenceYearlyReport> MemberAttendenceYearlyReports = new List<MemberAttendenceYearlyReport>();

            while (reader.Read())
            {
                MemberAttendenceYearlyReports.Add(((IEntityBuilder<MemberAttendenceYearlyReport>)this).BuildEntity(reader));
            }

            return (MemberAttendenceYearlyReports.Count > 0) ? MemberAttendenceYearlyReports : null;
        }

        MemberAttendenceYearlyReport IEntityBuilder<MemberAttendenceYearlyReport>.BuildEntity(IDataReader reader)
        {
            const int FLD_LETTER = 0;
            const int FLD_MEANING = 1;
            const int FLD_JANUARY = 2;
            const int FLD_FEBRUARY = 3;
            const int FLD_MARCH = 4;
            const int FLD_APRIL = 5;
            const int FLD_MAY = 6;
            const int FLD_JUNE = 7;
            const int FLD_JULY = 8;
            const int FLD_AUGUST = 9;
            const int FLD_SEPTEMBER = 10;
            const int FLD_OCTOBER = 11;
            const int FLD_NOVEMBER = 12;
            const int FLD_DECEMBER = 13;

            MemberAttendenceYearlyReport MemberAttendenceYearlyReport = new MemberAttendenceYearlyReport();

            MemberAttendenceYearlyReport.Letter = reader.IsDBNull(FLD_LETTER) ? string.Empty : reader.GetString(FLD_LETTER);
            MemberAttendenceYearlyReport.Meaning = reader.IsDBNull(FLD_MEANING) ? string.Empty : reader.GetString(FLD_MEANING);
            MemberAttendenceYearlyReport.January = reader.IsDBNull(FLD_JANUARY) ? 0 : reader.GetInt32(FLD_JANUARY);
            MemberAttendenceYearlyReport.February = reader.IsDBNull(FLD_FEBRUARY) ? 0 : reader.GetInt32(FLD_FEBRUARY);
            MemberAttendenceYearlyReport.March = reader.IsDBNull(FLD_MARCH) ? 0 : reader.GetInt32(FLD_MARCH);
            MemberAttendenceYearlyReport.April = reader.IsDBNull(FLD_APRIL) ? 0 : reader.GetInt32(FLD_APRIL);
            MemberAttendenceYearlyReport.May = reader.IsDBNull(FLD_MAY) ? 0 : reader.GetInt32(FLD_MAY);
            MemberAttendenceYearlyReport.June = reader.IsDBNull(FLD_DECEMBER) ? 0 : reader.GetInt32(FLD_JUNE);
            MemberAttendenceYearlyReport.July = reader.IsDBNull(FLD_JUNE) ? 0 : reader.GetInt32(FLD_JULY);
            MemberAttendenceYearlyReport.August = reader.IsDBNull(FLD_JULY) ? 0 : reader.GetInt32(FLD_AUGUST);
            MemberAttendenceYearlyReport.September = reader.IsDBNull(FLD_AUGUST) ? 0 : reader.GetInt32(FLD_SEPTEMBER);
            MemberAttendenceYearlyReport.October = reader.IsDBNull(FLD_SEPTEMBER) ? 0 : reader.GetInt32(FLD_OCTOBER);
            MemberAttendenceYearlyReport.November = reader.IsDBNull(FLD_OCTOBER) ? 0 : reader.GetInt32(FLD_NOVEMBER);
            MemberAttendenceYearlyReport.December = reader.IsDBNull(FLD_NOVEMBER) ? 0 : reader.GetInt32(FLD_DECEMBER);
            

            return MemberAttendenceYearlyReport;
        }
    }
}