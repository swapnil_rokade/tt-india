﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class CustomRoleBuilder : BaseEntityBuilder, IEntityBuilder<CustomRole>
    {
        IList<CustomRole> IEntityBuilder<CustomRole>.BuildEntities(IDataReader reader)
        {
            List<CustomRole> customRoles = new List<CustomRole>();

            while (reader.Read())
            {
                customRoles.Add(((IEntityBuilder<CustomRole>)this).BuildEntity(reader));
            }

            return (customRoles.Count > 0) ? customRoles : null;
        }

        CustomRole IEntityBuilder<CustomRole>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_NAME = 1;
            const int FLD_DESCRIPTION = 2;
            const int FLD_ISSYSTEMDEFAULT = 3;
            const int FLD_ISREMOVED = 4;
            const int FLD_CREATORID = 5;
            const int FLD_UPDATORID = 6;
            const int FLD_CREATEDATE = 7;
            const int FLD_UPDATEDATE = 8;

            CustomRole customRole = new CustomRole();

            customRole.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            customRole.Name = reader.IsDBNull(FLD_NAME) ? string.Empty : reader.GetString(FLD_NAME);
            if (reader.FieldCount > 2)
            {
                customRole.Description = reader.IsDBNull(FLD_DESCRIPTION) ? string.Empty : reader.GetString(FLD_DESCRIPTION);
                customRole.IsSystemDefault = reader.IsDBNull(FLD_ISSYSTEMDEFAULT) ? false : reader.GetBoolean(FLD_ISSYSTEMDEFAULT);
                customRole.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
                customRole.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
                customRole.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
                customRole.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
                customRole.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

                customRole.Creator = GetMember(customRole.CreatorId);
                customRole.Updator = GetMember(customRole.UpdatorId);
            }

            return customRole;
        }

        
    }
}