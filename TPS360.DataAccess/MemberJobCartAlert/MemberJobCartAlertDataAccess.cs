﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class MemberJobCartAlertDataAccess : BaseDataAccess, IMemberJobCartAlertDataAccess
    {
        #region Constructors

        public MemberJobCartAlertDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberJobCartAlert> CreateEntityBuilder<MemberJobCartAlert>()
        {
            return (new MemberJobCartAlertBuilder()) as IEntityBuilder<MemberJobCartAlert>;
        }

        #endregion

        #region  Methods

        MemberJobCartAlert IMemberJobCartAlertDataAccess.Add(MemberJobCartAlert memberJobCartAlert)
        {
            const string SP = "dbo.MemberJobCartAlert_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberJobCartAlertSetupId", DbType.Int32, memberJobCartAlert.MemberJobCartAlertSetupId);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, memberJobCartAlert.JobPostingId);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberJobCartAlert.IsRemoved);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberJobCartAlert.CreatorId);                

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberJobCartAlert = CreateEntityBuilder<MemberJobCartAlert>().BuildEntity(reader);
                    }
                    else
                    {
                        memberJobCartAlert = null;
                    }
                }

                if (memberJobCartAlert == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberJobCartAlert already exists. Please specify another memberJobCartAlert.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this memberJobCartAlert.");
                            }
                    }
                }

                return memberJobCartAlert;
            }
        }

        MemberJobCartAlert IMemberJobCartAlertDataAccess.Update(MemberJobCartAlert memberJobCartAlert)
        {
            const string SP = "dbo.MemberJobCartAlert_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberJobCartAlert.Id);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberJobCartAlert.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberJobCartAlert.UpdatorId);               

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberJobCartAlert = CreateEntityBuilder<MemberJobCartAlert>().BuildEntity(reader);
                    }
                    else
                    {
                        memberJobCartAlert = null;
                    }
                }

                if (memberJobCartAlert == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberJobCartAlert already exists. Please specify another memberJobCartAlert.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this memberJobCartAlert.");
                            }
                    }
                }

                return memberJobCartAlert;
            }
        }

        MemberJobCartAlert IMemberJobCartAlertDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberJobCartAlert_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberJobCartAlert>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberJobCartAlert> IMemberJobCartAlertDataAccess.GetAll()
        {
            const string SP = "dbo.MemberJobCartAlert_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberJobCartAlert>().BuildEntities(reader);
                }
            }
        }

        IList<MemberJobCartAlert> IMemberJobCartAlertDataAccess.GetAllByMemberJobCartAlertSetupId(int memberJobCartAlertSetupId)
        {
            if (memberJobCartAlertSetupId < 1)
            {
                throw new ArgumentNullException("memberJobCartAlertSetupId");
            }

            const string SP = "dbo.MemberJobCartAlert_GetAllMemberJobCartAlertByMemberJobCartAlertSetupId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberJobCartAlertSetupId", DbType.Int32, memberJobCartAlertSetupId);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberJobCartAlert>().BuildEntities(reader);
                }
            }
        }

        IList<MemberJobCartAlert> IMemberJobCartAlertDataAccess.GetAllByJobPostingId(int jobPostingId)
        {
            if (jobPostingId < 1)
            {
                throw new ArgumentNullException("jobPostingId");
            }

            const string SP = "dbo.MemberJobCartAlert_GetAllMemberJobCartAlertByJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberJobCartAlert>().BuildEntities(reader);
                }
            }
        }

        bool IMemberJobCartAlertDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberJobCartAlert_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a memberJobCartAlert which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this memberJobCartAlert.");
                        }
                }
            }
        }

        bool IMemberJobCartAlertDataAccess.RemoveAllByMemberJobCartAlertSetupId(int memberJobCartAlertSetupId)
        {
            const string SP = "dbo.MemberJobCartAlert_RemoveAllByMemberJobCartAlertSetupId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);

                Database.AddInParameter(cmd, "@MemberJobCartAlertSetupId", DbType.Int32, memberJobCartAlertSetupId);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_SUCCESS_UPDATE:
                            {
                                return true;

                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this company status.");
                            }
                    }
                }
            }
        }

        bool IMemberJobCartAlertDataAccess.DeleteByMemberJobCartAlertSetupId(int memberJobCartAlertSetupId)
        {
            if (memberJobCartAlertSetupId < 1)
            {
                throw new ArgumentNullException("memberJobCartAlertSetupId");
            }

            const string SP = "dbo.MemberJobCartAlert_DeleteMemberJobCartAlertByMemberJobCartAlertSetupId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberJobCartAlertSetupId", DbType.Int32, memberJobCartAlertSetupId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a memberJobCartAlert which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this memberJobCartAlert.");
                        }
                }
            }
        }

        #endregion
    }
}