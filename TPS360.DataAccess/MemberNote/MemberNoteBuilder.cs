﻿using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberNoteBuilder : IEntityBuilder<MemberNote>
    {
        IList<MemberNote> IEntityBuilder<MemberNote>.BuildEntities(IDataReader reader)
        {
            List<MemberNote> memberNotes = new List<MemberNote>();

            while (reader.Read())
            {
                memberNotes.Add(((IEntityBuilder<MemberNote>)this).BuildEntity(reader));
            }

            return (memberNotes.Count > 0) ? memberNotes : null;
        }

        MemberNote IEntityBuilder<MemberNote>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_MEMBERID = 1;
            const int FLD_COMMONNOTEID = 2;

            MemberNote memberNote = new MemberNote();

            memberNote.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberNote.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberNote.CommonNoteId = reader.IsDBNull(FLD_COMMONNOTEID) ? 0 : reader.GetInt32(FLD_COMMONNOTEID);

            return memberNote;
        }
    }
}