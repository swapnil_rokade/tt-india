﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class MemberNoteDataAccess : BaseDataAccess, IMemberNoteDataAccess
    {
        #region Constructors

        public MemberNoteDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberNote> CreateEntityBuilder<MemberNote>()
        {
            return (new MemberNoteBuilder()) as IEntityBuilder<MemberNote>;
        }

        #endregion

        #region  Methods

        MemberNote IMemberNoteDataAccess.Add(MemberNote memberNote)
        {
            const string SP = "dbo.MemberNote_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberNote.MemberId);
                Database.AddInParameter(cmd, "@CommonNoteId", DbType.Int32, memberNote.CommonNoteId);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, memberNote.JobPostingId );
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberNote = CreateEntityBuilder<MemberNote>().BuildEntity(reader);
                    }
                    else
                    {
                        memberNote = null;
                    }
                }

                if (memberNote == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberNote already exists. Please specify another memberNote.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this memberNote.");
                            }
                    }
                }

                return memberNote;
            }
        }

        MemberNote IMemberNoteDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberNote_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberNote>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberNote> IMemberNoteDataAccess.GetAll()
        {
            const string SP = "dbo.MemberNote_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberNote>().BuildEntities(reader);
                }
            }
        }

        bool IMemberNoteDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberNote_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a memberNote which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this memberNote.");
                        }
                }
            }
        }

        IList<MemberNote> IMemberNoteDataAccess.GetAllByMemberId(int id)
        {
            const string SP = "dbo.[MemberNote_GetByMemberId]";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, id);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberNote>().BuildEntities(reader);
                }
            }
        }
        
        #endregion
    }
}