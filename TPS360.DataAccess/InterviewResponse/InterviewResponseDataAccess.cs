﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :   InterviewResponseDataAccess.cs
    Description         :   This page is used Call the Stored Proceedures for Interviewer Feed back.
    Created By          :   Prasanth
    Created On          :   15/Oct/2015
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
  
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TPS360.Common.Shared;
using TPS360.Common.Helper;
using System.Collections;
using System.Text;
using TPS360.Common.BusinessEntities;
namespace TPS360.DataAccess
{
    internal sealed class InterviewResponseDataAccess : BaseDataAccess, IInterviewResponseDataAccess
    {
        #region Constructors

        public InterviewResponseDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<InterviewResponse> CreateEntityBuilder<InterviewResponse>()
        {
            return (new InterviewResponseBuilder()) as IEntityBuilder<InterviewResponse>;
        }

        #endregion

        #region  Methods
        void IInterviewResponseDataAccess.Add(InterviewResponse InterviewResponse)
        {
            //const string SP = "dbo.InterviewResponse_Create";
            //using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            //{

            //    Database.AddInParameter(cmd, "@InterviewId", DbType.Int32, InterviewResponse.InterviewId);
            //    Database.AddInParameter(cmd, "@QuestionBank_Id", DbType.Int32, InterviewResponse.QuestionBank_id);
            //    Database.AddInParameter(cmd, "@Response", DbType.String, InterviewResponse.Response);
            //    Database.AddInParameter(cmd, "@InterviewerEmail", DbType.String, InterviewResponse.InterviewerEmail);
            //    Database.AddInParameter(cmd, "@Rationale", DbType.String, InterviewResponse.Rationale);
            //    Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, InterviewResponse.CreatorId);
            //    Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, InterviewResponse.UpdatorId);
                
            //    Database.ExecuteReader(cmd);

            //}
            try
            { 
            

            const string SP = "dbo.InterviewResponse_CreateFromSchedule";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {

                Database.AddInParameter(cmd, "@InterviewId", DbType.Int32, InterviewResponse.InterviewId);
                Database.AddInParameter(cmd, "@QuestinBankId", DbType.Int32, InterviewResponse.QuestionBank_id);
                Database.AddInParameter(cmd, "@InterviewerEmail", DbType.String, InterviewResponse.InterviewerEmail);
                Database.ExecuteReader(cmd);

            }
            }
            catch (System.Exception ex)
            {
                string test = ex.Message;
            }



        }


        void IInterviewResponseDataAccess.Update(InterviewResponse InterviewResponse)
        {
            const string SP = "dbo.InterviewResponse_Update";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@InterviewResponse_Id", DbType.Int32, InterviewResponse.InterviewResponse_Id);
                 
                Database.AddInParameter(cmd, "@Response", DbType.String, InterviewResponse.Response);
                Database.AddInParameter(cmd, "@Rationale", DbType.String, InterviewResponse.Rationale);
                

                Database.ExecuteReader(cmd);
            }

        }



        //void IInterviewResponseDataAccess.Update(InterviewResponse InterviewResponse)
        //{
        //    const string SP = "dbo.InterviewResponse_Update";
        //    using (DbCommand cmd = Database.GetStoredProcCommand(SP))
        //    {
        //        Database.AddInParameter(cmd, "@InterviewResponse_Id", DbType.Int32, InterviewResponse.InterviewResponse_Id);
        //        Database.AddInParameter(cmd, "@InterviewId", DbType.Int32, InterviewResponse.InterviewId);
        //        Database.AddInParameter(cmd, "@QuestionBank_Id", DbType.Int32, InterviewResponse.QuestionBank_id);
        //        Database.AddInParameter(cmd, "@Response", DbType.String, InterviewResponse.Response);
        //        Database.AddInParameter(cmd, "@Rationale", DbType.String, InterviewResponse.Rationale);
        //        Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, InterviewResponse.CreatorId);
        //        Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, InterviewResponse.UpdatorId);
                
        //        Database.ExecuteReader(cmd);
        //     }

        //}


        //Check this Functionallity later 0123456789
        IList<InterviewResponse> IInterviewResponseDataAccess.GetAll()
        {
            const string SP = "dbo.InterviewerFeedback_GetAll";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<InterviewResponse>().BuildEntities(reader);
                }
            }

        }


        //Check this Functionality Later 0123456789
        void IInterviewResponseDataAccess.DeleteById(int Id)
        {
            if (Id < 1)
            {
                throw new ArgumentNullException("Id");
            }

            const string SP = "dbo.InterviewerFeedback_DeleteById";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, Id);
                Database.ExecuteNonQuery(cmd);
            }

        }
        //Check this Functionality Later 0123456789
        InterviewResponse IInterviewResponseDataAccess.GetById(int Id)
        {
            if (Id < 1)
            {
                throw new ArgumentNullException("Id");
            }

            const string SP = "dbo.InterviewerFeedback_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, Id);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<InterviewResponse>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }

        }

        IList<InterviewResponse> IInterviewResponseDataAccess.GetByInterviewId(int InterviewId)
        {
            if (InterviewId < 0)
            {
                throw new ArgumentNullException("InterviewId");
            }
            const string SP = "dbo.InterviewerResponse_GetByInterviewID";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@InterviewId", DbType.Int32, InterviewId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {

                    return CreateEntityBuilder<InterviewResponse>().BuildEntities(reader);

                }
            }
        }


        IList<InterviewResponse> IInterviewResponseDataAccess.GetByInterviewId_Email(int InterviewId, string Email)
        {
            if (InterviewId < 0)
            {
                throw new ArgumentNullException("InterviewId");
            }
            const string SP = "dbo.InterviewResponse_GetBy_InterviewId_InterviewerMailid";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Interviewid", DbType.Int32, InterviewId);
                Database.AddInParameter(cmd, "@InterviewerEmail", DbType.String, Email);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {

                    return CreateEntityBuilder<InterviewResponse>().BuildEntities(reader);

                }
            }
        }

        //IList<GenericLookup> IInterviewResponseDataAccess.InterviewAssessment_GetBy_InterviewId_InterviewerEmail(int InterviewId, string Email)
        //{
        //    if (InterviewId < 0)
        //    {
        //        throw new ArgumentNullException("InterviewId");
        //    }
        //    const string SP = "InterviewAssessment_GetBy_InterviewId_InterviewerEmail";

        //    Database.AddInParameter(cmd, "@Interviewid", DbType.Int32, InterviewId);
        //    Database.AddInParameter(cmd, "@InterviewerEmail", DbType.String, Email);
        //    using (IDataReader reader = Database.ExecuteReader(cmd))
        //    {
        //               return CreateEntityBuilder<GenericLookup>().BuildEntities(reader);
        //    }
        //}

        PagedResponse<InterviewResponse> IInterviewResponseDataAccess.GetPaged(PagedRequest request)
        {


            const string SP = "dbo.InterviewResponse_GetPaged";

            string whereClause = string.Empty;



            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "InterviewId")) 
                        {
                            if (value.Trim() != "0")
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }

                                sb.Append(" [I].interviewid ");
                                sb.Append(" = ");
                                sb.Append("'");
                                sb.Append(value);
                                sb.Append("'");
                               

                            }
                        }




                        if (StringHelper.IsEqual(column, "InterviewerEmail"))//0123456789
                        {
                            if (value.Trim() != "")
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append(" [I].InterviewerEmail ");
                                sb.Append(" = ");
                                sb.Append("'");
                                sb.Append(value);
                                sb.Append("'");
                            }
                        }


                        if (StringHelper.IsEqual(column, "AssessmentId"))
                        {
                            if (value.Trim() != "")
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append(" [G].Id");
                                sb.Append(" = ");
                                sb.Append("'");
                                sb.Append(value);
                                sb.Append("'");
                            }
                        }

                    }

                   }
                    whereClause = sb.ToString();
                
            }
            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "QB.Question";
                request.SortOrder = "DESC";
            }


            //whereClause = "' [I].InterviewerEmail like ''%gpkprashanth83@gmail.com%'' and  [I].interviewid  = 128' ";//0123456789
            //request.RowPerPage = 10;

            object[] paramValues = new object[] {	request.PageIndex,
                                                        request.RowPerPage,
                                                        StringHelper.Convert(whereClause),
                                                        StringHelper.Convert(request.SortColumn),
                                                        StringHelper.Convert(request.SortOrder)
                                                    };

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<InterviewResponse> response = new PagedResponse<InterviewResponse>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<InterviewResponse>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }

        }
 #endregion
    }
}
