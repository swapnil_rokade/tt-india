﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class MemberSubmissionDataAccess : BaseDataAccess, IMemberSubmissionDataAccess
    {
        #region Constructors

        public MemberSubmissionDataAccess(Context context) : base(context)
        {

        }

        protected override IEntityBuilder<MemberSubmission> CreateEntityBuilder<MemberSubmission>()
        {
            return (new MemberSubmissionBuider()) as IEntityBuilder<MemberSubmission>;
        }

        #endregion

        #region  Methods

        void IMemberSubmissionDataAccess.Add(MemberSubmission memberSubmission,string MemberId)
        {
            const string SP = "dbo.MemberSubmissions_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId",     DbType.AnsiString , MemberId  );
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, memberSubmission.JobPostingId);
                Database.AddInParameter(cmd, "@IsRemoved",    DbType.Boolean, memberSubmission.IsRemoved);
                Database.AddInParameter(cmd, "@CreatorId",    DbType.Int32, memberSubmission.CreatorId);
                Database.AddInParameter(cmd, "@ReceiverEmail", DbType.AnsiString, memberSubmission.ReceiverEmail);//12129
                Database.AddInParameter(cmd, "@MemberEmailDetailId", DbType.Int32, memberSubmission.MemberEmailDetailId);
                Database.AddInParameter(cmd, "@DateSubmitted", DbType.DateTime , NullConverter .Convert (memberSubmission.SubmittedDate) );
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32  , memberSubmission.CompanyId  );
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberSubmission.UpdatorId );
                Database.ExecuteNonQuery(cmd);
            }
        }

        MemberSubmission IMemberSubmissionDataAccess.Update(MemberSubmission memberSubmission)
        {
            const string SP = "dbo.MemberSubmissions_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberSubmission.Id );
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberSubmission.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberSubmission.UpdatorId);
                Database.AddInParameter(cmd, "@DateSubmitted", DbType.DateTime, NullConverter.Convert(memberSubmission.SubmittedDate));
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, memberSubmission.CompanyId);
                Database.AddInParameter(cmd, "@ReceiverEmail", DbType.AnsiString, memberSubmission.ReceiverEmail);//12129
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberSubmission.CreatorId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberSubmission = CreateEntityBuilder<MemberSubmission>().BuildEntity(reader);
                    }
                    else
                    {
                        memberSubmission = null;
                    }
                }
                return memberSubmission;
            }
        }

        MemberSubmission IMemberSubmissionDataAccess.GetByMemberIdAndJobPostingId(int memberid, int jobpostingid)
        {
            const string SP = "dbo.MemberSubmissions_GetByMemberIdAndJobPostingId";
            MemberSubmission memberSubmission = new MemberSubmission();
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberid );
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobpostingid );

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberSubmission = CreateEntityBuilder<MemberSubmission>().BuildEntity(reader);
                    }
                    else
                    {
                        memberSubmission = null;
                    }
                }
                return memberSubmission;
            }

        }

        #endregion
    }
}

