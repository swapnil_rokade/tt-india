﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data;
using TPS360.Common;
using System.Data.SqlClient;
using TPS360.Common .Shared ;
using TPS360.Common .BusinessEntities ;
using TPS360.Common.Helper;
namespace TPS360.DataAccess
{
    class UserAccessApp : BaseDataAccess, IUserAccessApp
    {
       

        public UserAccessApp(TPS360.Common.Shared.Context context)
            : base(context)
        { }
        #region IUserAccessApp Members

        public int EntryofUserAccessApp(string uname, string sessionId, string AppName, string domainname, string MacId,int allowedusers,string RequestedIp)
        {
            if ((CheckUserAllowedToLogin(uname, RequestedIp)) > 0)
            {
                                            //string sp = "GetNoofUserAllowed";
                                            //int usrsallowed = 0;
                                            //string TPS360LV = new TPS360ConfigElement().Value;
                                            //try
                                            //{
                                            //    using (SqlConnection l_connection = new SqlConnection())
                                            //    {
                                            //        l_connection.ConnectionString = TPS360LV;
                                            //        l_connection.Open();
                                            //        using (DbCommand cmd = l_connection.CreateCommand())
                                            //        {
                                            //            cmd.CommandText = sp;
                                            //            cmd.CommandType = CommandType.StoredProcedure;
                                            //            cmd.CommandTimeout = 600;
                                            //            DbParameter param = cmd.CreateParameter();
                                            //            param.DbType = DbType.AnsiString;
                                            //            param.ParameterName = "@DomainName";
                                            //            param.Value = domainname;
                                            //            cmd.Parameters.Add(param);
                                            //            using (SqlDataReader reader = (SqlDataReader)cmd.ExecuteReader(CommandBehavior.SingleRow))
                                            //            {
                                            //                if (reader.Read())
                                            //                {
                                            //                    usrsallowed = (int)reader[0];
                                            //                }
                                            //            }
                                            //        }
                                            //    }
                                            //}
                                            //catch { }

                int usrsallowed = allowedusers;
            string sp_name = "sp_ApplicationAccessDetailsEntry";
            int result = 0;
            using (DbCommand cmd = Database.GetStoredProcCommand(sp_name))
            {
                try
                {
                    //AddOutputParameter(cmd);
                    Database.AddInParameter(cmd, "@UserId", DbType.AnsiString, uname);
                    Database.AddInParameter(cmd, "@SessionId", DbType.AnsiString, sessionId);
                    Database.AddInParameter(cmd, "@AppName", DbType.AnsiString, AppName);
                    Database.AddInParameter(cmd, "@Domain", DbType.AnsiString, domainname);
                    Database.AddInParameter(cmd, "@MachineID", DbType.AnsiString, MacId);
                    Database.AddInParameter(cmd, "@NoofUserAllowed", DbType.Int32, usrsallowed);
                    //return Database.ExecuteNonQuery(cmd);

                    using (IDataReader reader = Database.ExecuteReader(cmd))
                    {
                        if (reader.Read())
                        {
                            result = (int)reader[0];
                        }
                    }

                }
                catch {  }
                //UpdateAspStateTempSessions(uname,sessionId,AppName,domainname);
                return result;
            }
            }
            else
	            {
                    return -5;//User login not permitted from IP <user's IP address>
	            }
        }

        int CheckUserAllowedToLogin(string uname, string RequestedIp)
        {
            string spName = "sp_useraccessrule";
            int IsUsersAllowed = 0;
            try
            {
                using (DbCommand cmd = Database.GetStoredProcCommand(spName))
                {
                    try
                    {
                        //AddOutputParameter(cmd);
                        Database.AddInParameter(cmd, "@useremail", DbType.AnsiString, uname);
                        Database.AddInParameter(cmd, "@userip", DbType.AnsiString, RequestedIp);
                        using (IDataReader reader = Database.ExecuteReader(cmd))
                        {
                            if (reader.Read())
                            {
                                IsUsersAllowed = (int)reader[0];
                            }
                        }

                    }
                    catch { IsUsersAllowed = 0; }
                }
            }
            catch { IsUsersAllowed = 0; }
            return IsUsersAllowed;
        }

        public void UpdateAspStateTempSessions(string uname,string sessionId,string AppName,string domainname)
        {
            string sp_name = "sp_UpdateAspStateTempSessions";
            using (DbCommand cmd = Database.GetStoredProcCommand(sp_name))
            {
                try
                {
                    //AddOutputParameter(cmd);
                    Database.AddInParameter(cmd, "@UserID", DbType.AnsiString, uname);
                    Database.AddInParameter(cmd, "@SessionID", DbType.AnsiString, sessionId);
                    Database.AddInParameter(cmd, "@AppName", DbType.AnsiString, AppName);
                    Database.AddInParameter(cmd, "@DomainName", DbType.AnsiString, domainname);
                    
                   Database.ExecuteNonQuery(cmd);
                }
                catch { }
            }
        }

        public void UpdateUserAccessApp(string uname, string AppName, string domainname)
        {
            string sp_name = "sp_ApplicationAccessDetailsUpdate";
            using (DbCommand cmd = Database.GetStoredProcCommand(sp_name))
            {
                try
                {
                    //AddOutputParameter(cmd);
                    Database.AddInParameter(cmd, "@UserId", DbType.AnsiString, uname);
                    Database.AddInParameter(cmd, "@AppName", DbType.AnsiString, AppName);
                    Database.AddInParameter(cmd, "@Domain", DbType.AnsiString, domainname);
                    Database.ExecuteNonQuery(cmd);
                }
                catch { }
            }
        }

        public int DeleteUserAccessApp(string uname, string AppName, string domainname)
        {
            string sp_name = "sp_ApplicationAccessDetailsDelete";
            int returnvalue=0;
            using (DbCommand cmd = Database.GetStoredProcCommand(sp_name))
            {
                try
                {
                    //AddOutputParameter(cmd);
                    Database.AddInParameter(cmd, "@UserId", DbType.AnsiString, uname);
                    Database.AddInParameter(cmd, "@AppName", DbType.AnsiString, AppName);
                    Database.AddInParameter(cmd, "@Domain", DbType.AnsiString, domainname);
                    returnvalue=Database.ExecuteNonQuery(cmd);

                }
                catch { }
            }
            return returnvalue;
        }

        public void EntryUserAccessAppReset(string uname, string AppName, string domainname)
        {
            string sp_name = "sp_ApplicationAccessDetailsResetTPS";
            using (DbCommand cmd = Database.GetStoredProcCommand(sp_name))
            {
                try
                {
                    Database.AddInParameter(cmd, "@UserId", DbType.AnsiString, uname);
                    Database.AddInParameter(cmd, "@AppName", DbType.AnsiString, AppName);
                    Database.AddInParameter(cmd, "@Domain", DbType.AnsiString, domainname);
                    Database.ExecuteNonQuery(cmd);

                }
                catch { }
            }
        }

        public int IsAvailableSessionId(string SessionId)
        {
            int result = 0;
            try
            {
                string sp_name = "sp_IsAvailableSessionId";
                using (DbCommand cmd = Database.GetStoredProcCommand(sp_name))
                {
                    try
                    {
                        Database.AddInParameter(cmd, "@_SessionId", DbType.AnsiString, SessionId);
                        using (IDataReader reader = Database.ExecuteReader(cmd))
                        {
                            if (reader.Read())
                            {
                                result = (int)reader[0];
                            }
                        }
                    }
                    catch { }
                }
            }
            catch { }
            return result;
        }

        #endregion

        protected override IEntityBuilder<T> CreateEntityBuilder<T>()
        {
            throw new NotImplementedException();
        }

        #region IUserAccessApp Members


        public int InsertSessionOutTime(string uname, int time, string SessionID, string AppName, string DomainName)
        {
            string sp_name = "InsertMemberSessionOutTime";
            using (DbCommand cmd = Database.GetStoredProcCommand(sp_name))
            {
                try
                {
                    //AddOutputParameter(cmd);
                    Database.AddInParameter(cmd, "@UserId", DbType.AnsiString, uname);
                    Database.AddInParameter(cmd, "@Sessionouttime", DbType.Int32, time);
                    Database.AddInParameter(cmd, "@SessionID", DbType.AnsiString, SessionID);
                    Database.AddInParameter(cmd, "@AppName", DbType.AnsiString, AppName);
                    Database.AddInParameter(cmd, "@DomainName", DbType.AnsiString, DomainName);
                    return Database.ExecuteNonQuery(cmd);


                }
                catch { }
            }
            return 0;
        }

        public int GetSessionTimeOutValue(string uname, string AppName, string DomainName)
        {
            string sp_name = "GetSessionTimeoutValue";
            int result = 0;
            using (DbCommand cmd = Database.GetStoredProcCommand(sp_name))
            {
                try
                {
                    //AddOutputParameter(cmd);
                    Database.AddInParameter(cmd, "@UserId", DbType.AnsiString, uname);
                    Database.AddInParameter(cmd, "@AppName", DbType.AnsiString, AppName);
                    Database.AddInParameter(cmd, "@DomainName", DbType.AnsiString, DomainName);
                    using (IDataReader reader = Database.ExecuteReader(cmd))
                    {
                        if (reader.Read())
                        {
                            result = (int)reader[0];
                        }
                    }

                }
                catch { }
            }
            return result;
        }

        PagedResponse<CurrentSessions> IUserAccessApp.CurrentSessions_GetPaged(PagedRequest request)
        {


            const string SP = "dbo.CurrentSessions_GetPaged";
            string whereClause = string.Empty;



            request.SortColumn = "[M].[FirstName]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<CurrentSessions > response = new PagedResponse<CurrentSessions >();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = (new CurrentSessionsBuilder()).BuildCurrentSessionEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        #endregion
    }
}
