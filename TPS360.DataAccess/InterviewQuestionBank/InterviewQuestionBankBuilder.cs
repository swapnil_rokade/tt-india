﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TPS360.Common.BusinessEntities;
using System.Data;

namespace TPS360.DataAccess
{
    internal sealed class InterviewQuestionBankBuilder : IEntityBuilder<InterviewQuestionBank>
    {
        IList<InterviewQuestionBank> IEntityBuilder<InterviewQuestionBank>.BuildEntities(IDataReader reader)
        {
            List<InterviewQuestionBank> interFeedback = new List<InterviewQuestionBank>();

            while (reader.Read())
            {
                interFeedback.Add(((IEntityBuilder<InterviewQuestionBank>)this).BuildEntity(reader));
            }

            return (interFeedback.Count > 0) ? interFeedback : null;
        }

        InterviewQuestionBank IEntityBuilder<InterviewQuestionBank>.BuildEntity(IDataReader reader) 
        {


            const int FLD_INTERVIEWQUESTIONBANK_ID = 0 ;
            const int FLD_INTERVIEWID = 1;
            const int FLD_QUESTIONBANKTYPE_ID = 2;
            const int FLD_QUESTIONBANKTYPE_NAME = 3; 
 
            const int FLD_CREATEDATE = 4;
            const int FLD_UPDATEDATE = 5;
            const int FLD_CREATORID = 6;
            const int FLD_UPDATORID = 7;
              


            InterviewQuestionBank InterviewQuestionBank = new InterviewQuestionBank();

            InterviewQuestionBank.InterviewQuestionBank_Id = reader.IsDBNull(FLD_INTERVIEWQUESTIONBANK_ID) ? 0 : reader.GetInt32(FLD_INTERVIEWQUESTIONBANK_ID);
            InterviewQuestionBank.InterviewId = reader.IsDBNull(FLD_INTERVIEWID) ? 0 : reader.GetInt32(FLD_INTERVIEWID);
            InterviewQuestionBank.QuestionBankType_Id = reader.IsDBNull(FLD_QUESTIONBANKTYPE_ID) ? 0 : reader.GetInt32(FLD_QUESTIONBANKTYPE_ID);
            InterviewQuestionBank.QuestionBankType = reader.IsDBNull(FLD_QUESTIONBANKTYPE_NAME) ? string.Empty : reader.GetString(FLD_QUESTIONBANKTYPE_NAME);
            InterviewQuestionBank.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            InterviewQuestionBank.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            InterviewQuestionBank.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            InterviewQuestionBank.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            InterviewQuestionBank.InterviewId = reader.IsDBNull(FLD_INTERVIEWID) ? 0 : reader.GetInt32(FLD_INTERVIEWID);


            return InterviewQuestionBank;
        }


      





        #region IEntityBuilder<InterviewQuestionBank> InterviewQuestionBank


        public InterviewQuestionBank BuildEntity(IDataReader dataReader)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

}
