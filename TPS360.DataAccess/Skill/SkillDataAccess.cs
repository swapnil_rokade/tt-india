﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class SkillDataAccess : BaseDataAccess, ISkillDataAccess
    {
        #region Constructors

        public SkillDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<Skill> CreateEntityBuilder<Skill>()
        {
            return (new SkillBuilder()) as IEntityBuilder<Skill>;
        }

        #endregion

        #region  Methods

        Skill ISkillDataAccess.Add(Skill skill)
        {
            const string SP = "dbo.Skill_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Name", DbType.AnsiString, skill.Name);
                Database.AddInParameter(cmd, "@Description", DbType.AnsiString, StringHelper.Convert(skill.Description));
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, skill.IsRemoved);
                Database.AddInParameter(cmd, "@ParentId", DbType.Int32, skill.ParentId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, skill.CreatorId);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        skill = CreateEntityBuilder<Skill>().BuildEntity(reader);
                    }
                    else
                    {
                        skill = null;
                    }
                }

                if (skill == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    //switch (returnCode)
                    //{
                    //    case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                    //        {
                    //            throw new ArgumentException("Skill already exists. Please specify another skill.");
                    //        }
                    //    default:
                    //        {
                    //            throw new SystemException("An unexpected error has occurred while creating this skill.");
                    //        }
                    //}

                    if (returnCode == SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA)
                    {
                        skill = null;
                    }

                }

                return skill;
            }
        }

        Skill ISkillDataAccess.Update(Skill skill)
        {
            const string SP = "dbo.Skill_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, skill.Id);
                Database.AddInParameter(cmd, "@Name", DbType.AnsiString, skill.Name);
                Database.AddInParameter(cmd, "@Description", DbType.AnsiString, StringHelper.Convert(skill.Description));
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, skill.IsRemoved);
                Database.AddInParameter(cmd, "@ParentId", DbType.Int32, skill.ParentId);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, skill.UpdatorId);
               
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        skill = CreateEntityBuilder<Skill>().BuildEntity(reader);
                    }
                    else
                    {
                        skill = null;
                    }
                }

                if (skill == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Skill already exists. Please specify another skill.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this skill.");
                            }
                    }
                }

                return skill;
            }
        }

        Skill ISkillDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.Skill_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<Skill>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<Skill> ISkillDataAccess.GetAll()
        {
            const string SP = "dbo.Skill_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<Skill>().BuildEntities(reader);
                }
            }
        }

        IList<Skill> ISkillDataAccess.GetAllParent()
        {
            const string SP = "dbo.Skill_GetAllParent";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<Skill>().BuildEntities(reader);
                }
            }
        }

        PagedResponse<Skill> ISkillDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.Skill_GetPaged";
            string whereClause = string.Empty;

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "ParentId";
            }

            request.SortColumn = "[S].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<Skill> response = new PagedResponse<Skill>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<Skill>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        bool ISkillDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.Skill_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a skill which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this skill.");
                        }
                }
            }
        }

        IList<Skill> ISkillDataAccess.GetAllSkillByParentId(int parentId, string searchSkill)
        {            
            if (parentId < 1)
            {
                throw new ArgumentNullException("parentId");
            }

            const string SP = "dbo.Skill_GetAllByParentId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@ParentId", DbType.Int32, parentId);
                Database.AddInParameter(cmd, "@SearchSkill", DbType.AnsiString, searchSkill);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<Skill>().BuildEntities(reader);
                }
            }
        }

        Int32 ISkillDataAccess.GetSkillIdBySkillName(string skill)
        {
            if (string.IsNullOrEmpty(skill))
            {
                throw new ArgumentNullException("skill");
            }

            const string SP = "dbo.Skill_GetSkillIdBySkillName";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@SkillName", DbType.String, skill); // Changed in parameter to "@SkillName" wrt Stored proc.

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    Int32 skillId = 0;

                    while (reader.Read())
                    {
                        skillId = reader.GetInt32(0);
                    }

                    return skillId;
                }
            }
        }

        IList<Skill> ISkillDataAccess.GetAllBySearch(string keyword, int count)
        {
            const string SP = "dbo.Skill_GetPaged";
            string whereClause = string.Empty;

            whereClause = "[S].Name like '" + keyword + "%'";
            object[] paramValues = new object[] {	0,
													count ,
													StringHelper.Convert(whereClause),
													StringHelper.Convert("[S].[Name]"),
                                                    StringHelper.Convert("Asc")
												};
            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<Skill>().BuildEntities(reader);
                }
            }
        }

        string ISkillDataAccess.GetAndAddSkillIds(string skillxml,int CreatorID)
        {
            const string SP = "dbo.Skill_GetAndAddSkillIds";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@XmlDocument", DbType.AnsiString, skillxml);
                Database.AddInParameter(cmd, "@CreatorID", DbType.AnsiString, CreatorID );
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return reader.IsDBNull(0) ? string.Empty : reader.GetString(0);
                    }
                    else
                    {
                        return string.Empty;
                    }
                }
            }
        }
        #endregion
    }
}