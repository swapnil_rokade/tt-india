﻿using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class OccupationalSeriesBuilder : IEntityBuilder<OccupationalSeries>
    {
        IList<OccupationalSeries> IEntityBuilder<OccupationalSeries>.BuildEntities(IDataReader reader)
        {
            List<OccupationalSeries> OccupationalSeriess = new List<OccupationalSeries>();

            while (reader.Read())
            {
                OccupationalSeriess.Add(((IEntityBuilder<OccupationalSeries>)this).BuildEntity(reader));
            }

            return (OccupationalSeriess.Count > 0) ? OccupationalSeriess : null;
        }

        OccupationalSeries IEntityBuilder<OccupationalSeries>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_SERIESTITLE = 1;
            const int FLD_GROUPID = 2;

            OccupationalSeries OccupationalSeries = new OccupationalSeries();

            OccupationalSeries.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            OccupationalSeries.SeriesTitle = reader.IsDBNull(FLD_SERIESTITLE) ? string.Empty : reader.GetString(FLD_SERIESTITLE);
            OccupationalSeries.GroupId = reader.IsDBNull(FLD_GROUPID) ? 0 : reader.GetInt32(FLD_GROUPID);
            return OccupationalSeries;
        }
    }
}