﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName:OccupationalSeriesDataAccess.cs
    Description: This page is used for Countries  functionality
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            July-7-2009           Gopala Swamy J         Misc:Added 1) GetStateIdByStateCode() 2) GetDepartmentIdByDepartmentCode() to assist 
                                                                Resume parser team
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/


using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class OccupationalSeriesDataAccess : BaseDataAccess, IOccupationalSeriesDataAccess
    {
        #region Constructors

        public OccupationalSeriesDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<OccupationalSeries> CreateEntityBuilder<OccupationalSeries>()
        {
            return (new OccupationalSeriesBuilder()) as IEntityBuilder<OccupationalSeries>;
        }


        #endregion
        IList<OccupationalSeries> IOccupationalSeriesDataAccess.GetAllBySearch(string keyword, int count)
        {
            const string SP = "dbo.OccupationalSeries_GetPaged";
            string whereClause = string.Empty;

            whereClause = "[D].Name like '" + keyword + "%'";
            object[] paramValues = new object[] {	0,
													count ,
													StringHelper.Convert(whereClause),
													StringHelper.Convert("[D].[SeriesTitle]"),
                                                    StringHelper.Convert("Asc")
												};
            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<OccupationalSeries>().BuildEntities(reader);
                }
            }
        }

        IList<OccupationalSeries> IOccupationalSeriesDataAccess.GetAll()
        {
            const string SP = "dbo.OccupationalSeries_GetAll";
            string whereClause = string.Empty;

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<OccupationalSeries>().BuildEntities(reader);
                }
            }
        }
        OccupationalSeries IOccupationalSeriesDataAccess.GetbyId(int id)
        {

            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.OccupationalSeries_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<OccupationalSeries>()).BuildEntity(reader);
                    }

                    else
                    {
                        return null;
                    }
                }

            }
        }
    }
}
