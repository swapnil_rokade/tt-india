﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Collections;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class MemberPendingJoinersDataAccess : BaseDataAccess, IMemberPendingJoinersDataAccess
    {
                #region Constructors

        public MemberPendingJoinersDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberPendingJoiners> CreateEntityBuilder<MemberPendingJoiners>()
        {
            return (new MemberPendingJoinersBuilder ()) as IEntityBuilder<MemberPendingJoiners>;
        }

        #endregion

        #region  Methods

        PagedResponse<MemberPendingJoiners> IMemberPendingJoinersDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.PendingJoiners_GetPaged";
            string whereClause = string.Empty;
            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");
                        if (!StringHelper.IsBlank(column))
                        {
                            value = value.Replace("'", "''");
                            if (StringHelper.IsEqual(column, "MemberId"))
                            {
                                sb.Append("([MHD].[CreatorId]");
                                sb.Append(" = ");
                                sb.Append(value);
                                sb.Append(" or [MJC].[CreatorId]=");
                                sb.Append(value);
                                sb.Append(" ) ");
                            }
                        }
                    }
                }
                whereClause = sb.ToString();
            }
            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[MHD].[JoiningDate]";
            }
            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "desc";
            }
            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberPendingJoiners > response = new PagedResponse<MemberPendingJoiners >();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<MemberPendingJoiners>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        #endregion
    }
}
