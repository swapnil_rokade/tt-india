﻿using System;
using System.Data;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberPendingJoinersBuilder : IEntityBuilder<MemberPendingJoiners>
    {
        IList<MemberPendingJoiners> IEntityBuilder<MemberPendingJoiners>.BuildEntities(IDataReader reader)
        {
            List<MemberPendingJoiners> MemberPendingJoiners = new List<MemberPendingJoiners>();

            while (reader.Read())
            {
                MemberPendingJoiners.Add(((IEntityBuilder<MemberPendingJoiners>)this).BuildEntity(reader));
            }

            return (MemberPendingJoiners.Count > 0) ? MemberPendingJoiners : null;
        }

        MemberPendingJoiners IEntityBuilder<MemberPendingJoiners>.BuildEntity(IDataReader reader)
        {
            const int FLD_MEMBERID = 0;
            const int FLD_CANDIDATENAME = 1;
            const int FLD_JOBPOSTINGID = 2;
            const int FLD_JOBPOSTINGCODE = 3;
            const int FLD_JOBTITLE = 4;
            const int FLD_EDATAOFJOINING = 5;
            const int FLD_ACTIVERECRUITERID = 6;
            const int FLD_ACTIVERECRUITERNAME = 7;
            MemberPendingJoiners mp = new MemberPendingJoiners();
            mp.MemberID = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            mp.CandidateName = reader.IsDBNull(FLD_CANDIDATENAME) ? string.Empty : reader.GetString(FLD_CANDIDATENAME);
            mp.JobPostingID = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);
            mp.JobPostingCode = reader.IsDBNull(FLD_JOBPOSTINGCODE) ? string.Empty : reader.GetString(FLD_JOBPOSTINGCODE);
            mp.JobTitle = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
            mp.ExpectedDateOfJoining = reader.IsDBNull(FLD_EDATAOFJOINING) ? DateTime.MinValue : reader.GetDateTime(FLD_EDATAOFJOINING);
            mp.ActiveRecruiterID = reader.IsDBNull(FLD_ACTIVERECRUITERID) ? 0 : reader.GetInt32(FLD_ACTIVERECRUITERID);
            mp.ActiveRecruiterName = reader.IsDBNull(FLD_ACTIVERECRUITERNAME) ? string.Empty : reader.GetString(FLD_ACTIVERECRUITERNAME);

            return mp;
        }
    }
}
