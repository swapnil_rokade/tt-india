﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class MemberActivityTypeDataAccess : BaseDataAccess, IMemberActivityTypeDataAccess
    {
        #region Constructors

        public MemberActivityTypeDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberActivityType> CreateEntityBuilder<MemberActivityType>()
        {
            return (new MemberActivityTypeBuilder()) as IEntityBuilder<MemberActivityType>;
        }

        #endregion

        #region  Methods

        MemberActivityType IMemberActivityTypeDataAccess.Add(MemberActivityType memberActivityType)
        {
            const string SP = "dbo.MemberActivityType_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Name", DbType.AnsiString, memberActivityType.Name);
                Database.AddInParameter(cmd, "@Description", DbType.AnsiString, memberActivityType.Description);
                Database.AddInParameter(cmd, "@ActivityType", DbType.Int32, memberActivityType.ActivityType);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberActivityType.IsRemoved);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberActivityType.CreatorId);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberActivityType = CreateEntityBuilder<MemberActivityType>().BuildEntity(reader);
                    }
                    else
                    {
                        memberActivityType = null;
                    }
                }

                if (memberActivityType == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberActivityType already exists. Please specify another memberActivityType.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this memberActivityType.");
                            }
                    }
                }

                return memberActivityType;
            }
        }

        MemberActivityType IMemberActivityTypeDataAccess.Update(MemberActivityType memberActivityType)
        {
            const string SP = "dbo.MemberActivityType_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberActivityType.Id);
                Database.AddInParameter(cmd, "@Name", DbType.AnsiString, memberActivityType.Name);
                Database.AddInParameter(cmd, "@Description", DbType.AnsiString, memberActivityType.Description);
                Database.AddInParameter(cmd, "@ActivityType", DbType.Int32, memberActivityType.ActivityType);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberActivityType.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberActivityType.UpdatorId);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberActivityType = CreateEntityBuilder<MemberActivityType>().BuildEntity(reader);
                    }
                    else
                    {
                        memberActivityType = null;
                    }
                }

                if (memberActivityType == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberActivityType already exists. Please specify another memberActivityType.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this memberActivityType.");
                            }
                    }
                }

                return memberActivityType;
            }
        }

        MemberActivityType IMemberActivityTypeDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberActivityType_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberActivityType>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        MemberActivityType IMemberActivityTypeDataAccess.GetByActivityType(int ActivityType)
        {
            if (ActivityType < 1)
            {
                throw new ArgumentNullException("ActivityType");
            }

            const string SP = "dbo.MemberActivityType_GetByActivityType";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@ActivityType", DbType.Int32, ActivityType);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberActivityType>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberActivityType> IMemberActivityTypeDataAccess.GetAll()
        {
            const string SP = "dbo.MemberActivityType_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberActivityType>().BuildEntities(reader);
                }
            }
        }

        bool IMemberActivityTypeDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberActivityType_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a memberActivityType which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this memberActivityType.");
                        }
                }
            }
        }

        PagedResponse<MemberActivityType> IMemberActivityTypeDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.MemberActivityType_GetPaged";
            string whereClause = string.Empty;

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "Name";
            }

            request.SortColumn = "[MAT].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberActivityType> response = new PagedResponse<MemberActivityType>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<MemberActivityType>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        #endregion
    }
}