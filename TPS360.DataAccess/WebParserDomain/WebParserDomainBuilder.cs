﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class WebParserDomainBuilder : IEntityBuilder<WebParserDomain>
    {
        IList<WebParserDomain> IEntityBuilder<WebParserDomain>.BuildEntities(IDataReader reader)
        {
            List<WebParserDomain> webParserDomains = new List<WebParserDomain>();

            while (reader.Read())
            {
                webParserDomains.Add(((IEntityBuilder<WebParserDomain>)this).BuildEntity(reader));
            }

            return (webParserDomains.Count > 0) ? webParserDomains : null;
        }

        WebParserDomain IEntityBuilder<WebParserDomain>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_DOMAINNAME = 1;
            const int FLD_DOMAINURL = 2;
            const int FLD_FULLNAME = 3;
            const int FLD_CURRENTPOSITION = 4;
            const int FLD_ADDRESS1 = 5;
            const int FLD_EMAIL = 6;
            const int FLD_OFFICEPHONE = 7;
            const int FLD_HOMEPHONE = 8;
            const int FLD_CELLPHONE = 9;
            const int FLD_TOTALEXPERIENCEYEARS = 10;
            const int FLD_CURRENTCOMPANY = 11;
            const int FLD_OBJECTIVE = 12;
            const int FLD_SUMMARY = 13;
            const int FLD_SKILL = 14;
            const int FLD_EDUCATION = 15;
            const int FLD_WORKAUTHORIZATION = 16;
            const int FLD_JOBTYPE = 17;
            const int FLD_RELOCATION = 18;
            const int FLD_WILLINGTOTRAVEL = 19;
            const int FLD_YEARLYSALARYRATE = 20;
            const int FLD_HOURLYSALARRATE = 21;
            const int FLD_SECURITYCLEARANCE = 22;
            const int FLD_COPYPASTERESUME = 23;
            const int FLD_WHOLERESUME = 24;
            const int FLD_WORDESUME = 25;   
            const int FLD_DOCUMENTURL = 26;
            const int FLD_REPLACEKEYWORD = 27;

            const int FLD_ADDRESS2 = 28;
            const int FLD_CITY = 29;
            const int FLD_STATE = 30;
            const int FLD_COUNTRY = 31;
            const int FLD_ZIPCODE = 32;
            const int FLD_DATEOFBIRTH = 33;
            const int FLD_GENDER = 34;
            const int FLD_MARITALSTATUS = 35;
            const int FLD_ISOMITEMPTYLINE = 36;

            const int FLD_ISREMOVED = 37;
            const int FLD_CREATORID = 38;
            const int FLD_UPDATORID = 39;
            const int FLD_CREATEDATE = 40;
            const int FLD_UPDATEDATE = 41;

            WebParserDomain webParserDomain = new WebParserDomain();

            webParserDomain.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            webParserDomain.DomainName = reader.IsDBNull(FLD_DOMAINNAME) ? string.Empty : reader.GetString(FLD_DOMAINNAME);
            webParserDomain.DomainUrl = reader.IsDBNull(FLD_DOMAINURL) ? string.Empty : reader.GetString(FLD_DOMAINURL);
            webParserDomain.FullName = reader.IsDBNull(FLD_FULLNAME) ? string.Empty : reader.GetString(FLD_FULLNAME);
            webParserDomain.CurrentPosition = reader.IsDBNull(FLD_CURRENTPOSITION) ? string.Empty : reader.GetString(FLD_CURRENTPOSITION);
            webParserDomain.Address1 = reader.IsDBNull(FLD_ADDRESS1) ? string.Empty : reader.GetString(FLD_ADDRESS1);
            webParserDomain.Email = reader.IsDBNull(FLD_EMAIL) ? string.Empty : reader.GetString(FLD_EMAIL);
            webParserDomain.OfficePhone = reader.IsDBNull(FLD_OFFICEPHONE) ? string.Empty : reader.GetString(FLD_OFFICEPHONE);
            webParserDomain.HomePhone = reader.IsDBNull(FLD_HOMEPHONE) ? string.Empty : reader.GetString(FLD_HOMEPHONE);
            webParserDomain.CellPhone = reader.IsDBNull(FLD_CELLPHONE) ? string.Empty : reader.GetString(FLD_CELLPHONE);
            webParserDomain.TotalExperienceYears = reader.IsDBNull(FLD_TOTALEXPERIENCEYEARS) ? string.Empty : reader.GetString(FLD_TOTALEXPERIENCEYEARS);
            webParserDomain.CurrentCompany = reader.IsDBNull(FLD_CURRENTCOMPANY) ? string.Empty : reader.GetString(FLD_CURRENTCOMPANY);
            webParserDomain.Objective = reader.IsDBNull(FLD_OBJECTIVE) ? string.Empty : reader.GetString(FLD_OBJECTIVE);
            webParserDomain.Summary = reader.IsDBNull(FLD_SUMMARY) ? string.Empty : reader.GetString(FLD_SUMMARY);
            webParserDomain.Skill = reader.IsDBNull(FLD_SKILL) ? string.Empty : reader.GetString(FLD_SKILL);
            webParserDomain.Education = reader.IsDBNull(FLD_EDUCATION) ? string.Empty : reader.GetString(FLD_EDUCATION);
            webParserDomain.WorkAuthorization = reader.IsDBNull(FLD_WORKAUTHORIZATION) ? string.Empty : reader.GetString(FLD_WORKAUTHORIZATION);
            webParserDomain.JobType = reader.IsDBNull(FLD_JOBTYPE) ? string.Empty : reader.GetString(FLD_JOBTYPE);
            webParserDomain.Relocation = reader.IsDBNull(FLD_RELOCATION) ? string.Empty : reader.GetString(FLD_RELOCATION);
            webParserDomain.WillingToTravel = reader.IsDBNull(FLD_WILLINGTOTRAVEL) ? string.Empty : reader.GetString(FLD_WILLINGTOTRAVEL);
            webParserDomain.YearlySalaryRate = reader.IsDBNull(FLD_YEARLYSALARYRATE) ? string.Empty : reader.GetString(FLD_YEARLYSALARYRATE);
            webParserDomain.HourlySalarRate = reader.IsDBNull(FLD_HOURLYSALARRATE) ? string.Empty : reader.GetString(FLD_HOURLYSALARRATE);
            webParserDomain.SecurityClearance = reader.IsDBNull(FLD_SECURITYCLEARANCE) ? string.Empty : reader.GetString(FLD_SECURITYCLEARANCE);

            webParserDomain.CopyPasteResume = reader.IsDBNull(FLD_COPYPASTERESUME) ? string.Empty : reader.GetString(FLD_COPYPASTERESUME);
            webParserDomain.WholeResume = reader.IsDBNull(FLD_WHOLERESUME) ? string.Empty : reader.GetString(FLD_WHOLERESUME);
            webParserDomain.WordResume = reader.IsDBNull(FLD_WORDESUME) ? string.Empty : reader.GetString(FLD_WORDESUME);
            webParserDomain.DocumentUrl = reader.IsDBNull(FLD_DOCUMENTURL) ? string.Empty : reader.GetString(FLD_DOCUMENTURL);
            webParserDomain.ReplaceKeyword = reader.IsDBNull(FLD_REPLACEKEYWORD) ? string.Empty : reader.GetString(FLD_REPLACEKEYWORD);

            webParserDomain.Address2 = reader.IsDBNull(FLD_ADDRESS2) ? string.Empty : reader.GetString(FLD_ADDRESS2);
            webParserDomain.City = reader.IsDBNull(FLD_CITY) ? string.Empty : reader.GetString(FLD_CITY);
            webParserDomain.State = reader.IsDBNull(FLD_STATE) ? string.Empty : reader.GetString(FLD_STATE);
            webParserDomain.Country = reader.IsDBNull(FLD_COUNTRY) ? string.Empty : reader.GetString(FLD_COUNTRY);
            webParserDomain.ZipCode = reader.IsDBNull(FLD_ZIPCODE) ? string.Empty : reader.GetString(FLD_ZIPCODE);
            webParserDomain.DateOfBirth = reader.IsDBNull(FLD_DATEOFBIRTH) ? string.Empty : reader.GetString(FLD_DATEOFBIRTH);
            webParserDomain.Gender = reader.IsDBNull(FLD_GENDER) ? string.Empty : reader.GetString(FLD_GENDER);
            webParserDomain.MaritalStatus = reader.IsDBNull(FLD_MARITALSTATUS) ? string.Empty : reader.GetString(FLD_MARITALSTATUS);
            webParserDomain.IsOmitEmptyLine = reader.IsDBNull(FLD_ISOMITEMPTYLINE) ? false : reader.GetBoolean(FLD_ISOMITEMPTYLINE);

            webParserDomain.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            webParserDomain.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            webParserDomain.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            webParserDomain.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            webParserDomain.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return webParserDomain;
        }
    }
}