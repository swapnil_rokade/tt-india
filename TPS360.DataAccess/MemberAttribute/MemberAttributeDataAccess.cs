﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class MemberAttributeDataAccess : BaseDataAccess, IMemberAttributeDataAccess
    {
        #region Constructors

        public MemberAttributeDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberAttribute> CreateEntityBuilder<MemberAttribute>()
        {
            return (new MemberAttributeBuilder()) as IEntityBuilder<MemberAttribute>;
        }

        #endregion

        #region  Methods

        MemberAttribute IMemberAttributeDataAccess.Add(MemberAttribute memberAttribute)
        {
            const string SP = "dbo.MemberAttribute_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Comment", DbType.AnsiString, memberAttribute.Comment);
                Database.AddInParameter(cmd, "@Score", DbType.Decimal, memberAttribute.Score);
                Database.AddInParameter(cmd, "@AppraisalAttributeId", DbType.Int32, memberAttribute.AppraisalAttributeId);
                Database.AddInParameter(cmd, "@AppraisalRatingId", DbType.Int32, memberAttribute.AppraisalRatingId);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberAttribute.MemberId);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberAttribute.IsRemoved);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberAttribute.CreatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberAttribute = CreateEntityBuilder<MemberAttribute>().BuildEntity(reader);
                    }
                    else
                    {
                        memberAttribute = null;
                    }
                }

                if (memberAttribute == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberAttribute already exists. Please specify another memberAttribute.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this memberAttribute.");
                            }
                    }
                }

                return memberAttribute;
            }
        }

        MemberAttribute IMemberAttributeDataAccess.Update(MemberAttribute memberAttribute)
        {
            const string SP = "dbo.MemberAttribute_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberAttribute.Id);
                Database.AddInParameter(cmd, "@Comment", DbType.AnsiString, memberAttribute.Comment);
                Database.AddInParameter(cmd, "@Score", DbType.Decimal, memberAttribute.Score);
                Database.AddInParameter(cmd, "@AppraisalAttributeId", DbType.Int32, memberAttribute.AppraisalAttributeId);
                Database.AddInParameter(cmd, "@AppraisalRatingId", DbType.Int32, memberAttribute.AppraisalRatingId);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberAttribute.MemberId);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberAttribute.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberAttribute.UpdatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberAttribute = CreateEntityBuilder<MemberAttribute>().BuildEntity(reader);
                    }
                    else
                    {
                        memberAttribute = null;
                    }
                }

                if (memberAttribute == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberAttribute already exists. Please specify another memberAttribute.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this memberAttribute.");
                            }
                    }
                }

                return memberAttribute;
            }
        }

        MemberAttribute IMemberAttributeDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberAttribute_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberAttribute>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberAttribute> IMemberAttributeDataAccess.GetAll()
        {
            const string SP = "dbo.MemberAttribute_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberAttribute>().BuildEntities(reader);
                }
            }
        }

        bool IMemberAttributeDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberAttribute_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a memberAttribute which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this memberAttribute.");
                        }
                }
            }
        }

        #endregion
    }
}