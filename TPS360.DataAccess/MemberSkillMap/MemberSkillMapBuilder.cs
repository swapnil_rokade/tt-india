﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberSkillMapBuilder : IEntityBuilder<MemberSkillMap>
    {
        IList<MemberSkillMap> IEntityBuilder<MemberSkillMap>.BuildEntities(IDataReader reader)
        {
            List<MemberSkillMap> memberSkillMaps = new List<MemberSkillMap>();

            while (reader.Read())
            {
                memberSkillMaps.Add(((IEntityBuilder<MemberSkillMap>)this).BuildEntity(reader));
            }

            return (memberSkillMaps.Count > 0) ? memberSkillMaps : null;
        }

        MemberSkillMap IEntityBuilder<MemberSkillMap>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_PROFICIENCYLOOKUPID = 1;
            const int FLD_YEARSOFEXPERIENCE = 2;
            const int FLD_LASTUSED = 3;
            const int FLD_COMMENT = 4;
            const int FLD_ISREMOVED = 5;
            const int FLD_MEMBERID = 6;
            const int FLD_SKILLID = 7;
            const int FLD_CREATORID = 8;
            const int FLD_UPDATORID = 9;
            const int FLD_CREATEDATE = 10;
            const int FLD_UPDATEDATE = 11;

            MemberSkillMap memberSkillMap = new MemberSkillMap();

            memberSkillMap.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberSkillMap.ProficiencyLookupId = reader.IsDBNull(FLD_PROFICIENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_PROFICIENCYLOOKUPID);
            memberSkillMap.YearsOfExperience = reader.IsDBNull(FLD_YEARSOFEXPERIENCE) ? 0 : reader.GetInt32(FLD_YEARSOFEXPERIENCE);
            memberSkillMap.LastUsed = reader.IsDBNull(FLD_LASTUSED) ? DateTime.MinValue : reader.GetDateTime(FLD_LASTUSED);
            memberSkillMap.Comment = reader.IsDBNull(FLD_COMMENT) ? string.Empty : reader.GetString(FLD_COMMENT);
            memberSkillMap.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberSkillMap.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberSkillMap.SkillId = reader.IsDBNull(FLD_SKILLID) ? 0 : reader.GetInt32(FLD_SKILLID);
            memberSkillMap.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberSkillMap.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberSkillMap.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberSkillMap.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return memberSkillMap;
        }
    }
}