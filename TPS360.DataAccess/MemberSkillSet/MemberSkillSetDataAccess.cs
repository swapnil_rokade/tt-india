﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class MemberSkillSetDataAccess : BaseDataAccess, IMemberSkillSetDataAccess
    {
        #region Constructors

        public MemberSkillSetDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberSkillSet> CreateEntityBuilder<MemberSkillSet>()
        {
            return (new MemberSkillSetBuilder()) as IEntityBuilder<MemberSkillSet>;
        }

        #endregion

        #region  Methods

        MemberSkillSet IMemberSkillSetDataAccess.Add(MemberSkillSet memberSkillSet)
        {
            const string SP = "dbo.MemberSkillSet_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Proficiency", DbType.AnsiString, StringHelper.Convert(memberSkillSet.Proficiency));
                Database.AddInParameter(cmd, "@Description", DbType.AnsiString, StringHelper.Convert(memberSkillSet.Description));
                Database.AddInParameter(cmd, "@YearsOfExperience", DbType.AnsiString, StringHelper.Convert(memberSkillSet.YearsOfExperience));
                Database.AddInParameter(cmd, "@LastUsed", DbType.DateTime, NullConverter.Convert(memberSkillSet.LastUsed));
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberSkillSet.IsRemoved);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberSkillSet.MemberId);
                Database.AddInParameter(cmd, "@SkillId", DbType.Int32, memberSkillSet.SkillId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberSkillSet.CreatorId);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberSkillSet = CreateEntityBuilder<MemberSkillSet>().BuildEntity(reader);
                    }
                    else
                    {
                        memberSkillSet = null;
                    }
                }

                if (memberSkillSet == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberSkillSet already exists. Please specify another memberSkillSet.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this memberSkillSet.");
                            }
                    }
                }

                return memberSkillSet;
            }
        }

        MemberSkillSet IMemberSkillSetDataAccess.Update(MemberSkillSet memberSkillSet)
        {
            const string SP = "dbo.MemberSkillSet_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberSkillSet.Id);
                Database.AddInParameter(cmd, "@Proficiency", DbType.AnsiString, StringHelper.Convert(memberSkillSet.Proficiency));
                Database.AddInParameter(cmd, "@Description", DbType.AnsiString, StringHelper.Convert(memberSkillSet.Description));
                Database.AddInParameter(cmd, "@YearsOfExperience", DbType.AnsiString, StringHelper.Convert(memberSkillSet.YearsOfExperience));
                Database.AddInParameter(cmd, "@LastUsed", DbType.DateTime, NullConverter.Convert(memberSkillSet.LastUsed));
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberSkillSet.IsRemoved);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberSkillSet.MemberId);
                Database.AddInParameter(cmd, "@SkillId", DbType.Int32, memberSkillSet.SkillId);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberSkillSet.UpdatorId);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberSkillSet = CreateEntityBuilder<MemberSkillSet>().BuildEntity(reader);
                    }
                    else
                    {
                        memberSkillSet = null;
                    }
                }

                if (memberSkillSet == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberSkillSet already exists. Please specify another memberSkillSet.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this memberSkillSet.");
                            }
                    }
                }

                return memberSkillSet;
            }
        }

        MemberSkillSet IMemberSkillSetDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberSkillSet_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberSkillSet>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberSkillSet> IMemberSkillSetDataAccess.GetAll()
        {
            const string SP = "dbo.MemberSkillSet_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberSkillSet>().BuildEntities(reader);
                }
            }
        }

        bool IMemberSkillSetDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberSkillSet_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a memberSkillSet which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this memberSkillSet.");
                        }
                }
            }
        }

        #endregion
    }
}