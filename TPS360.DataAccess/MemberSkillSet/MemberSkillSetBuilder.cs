﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberSkillSetBuilder : IEntityBuilder<MemberSkillSet>
    {
        IList<MemberSkillSet> IEntityBuilder<MemberSkillSet>.BuildEntities(IDataReader reader)
        {
            List<MemberSkillSet> memberSkillSets = new List<MemberSkillSet>();

            while (reader.Read())
            {
                memberSkillSets.Add(((IEntityBuilder<MemberSkillSet>)this).BuildEntity(reader));
            }

            return (memberSkillSets.Count > 0) ? memberSkillSets : null;
        }

        MemberSkillSet IEntityBuilder<MemberSkillSet>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_PROFICIENCY = 1;
            const int FLD_DESCRIPTION = 2;
            const int FLD_YEARSOFEXPERIENCE = 3;
            const int FLD_LASTUSED = 4;
            const int FLD_ISREMOVED = 5;
            const int FLD_MEMBERID = 6;
            const int FLD_SKILLID = 7;
            const int FLD_CREATORID = 8;
            const int FLD_UPDATORID = 9;
            const int FLD_CREATEDATE = 10;
            const int FLD_UPDATEDATE = 11;

            MemberSkillSet memberSkillSet = new MemberSkillSet();

            memberSkillSet.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberSkillSet.Proficiency = reader.IsDBNull(FLD_PROFICIENCY) ? string.Empty : reader.GetString(FLD_PROFICIENCY);
            memberSkillSet.Description = reader.IsDBNull(FLD_DESCRIPTION) ? string.Empty : reader.GetString(FLD_DESCRIPTION);
            memberSkillSet.YearsOfExperience = reader.IsDBNull(FLD_YEARSOFEXPERIENCE) ? string.Empty : reader.GetString(FLD_YEARSOFEXPERIENCE);
            memberSkillSet.LastUsed = reader.IsDBNull(FLD_LASTUSED) ? DateTime.MinValue : reader.GetDateTime(FLD_LASTUSED);
            memberSkillSet.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberSkillSet.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberSkillSet.SkillId = reader.IsDBNull(FLD_SKILLID) ? 0 : reader.GetInt32(FLD_SKILLID);
            memberSkillSet.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberSkillSet.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberSkillSet.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberSkillSet.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return memberSkillSet;
        }
    }
}