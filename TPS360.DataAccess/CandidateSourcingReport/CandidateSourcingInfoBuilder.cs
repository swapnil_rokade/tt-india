﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CandidateSourcingInfoBuilder.cs
    Description: This page is used to build candidate entities.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date               Author           Modification
    ------------------------------------------------------------------------------------------------------------------------------    
                                                                    
-------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class CandidateSourcingInfoBuilder : IEntityBuilder<CandidateSourcingInfo>
    {
        IList<CandidateSourcingInfo> IEntityBuilder<CandidateSourcingInfo>.BuildEntities(IDataReader reader)
        {
            List<CandidateSourcingInfo> Candidates = new List<CandidateSourcingInfo>();

            while (reader.Read())
            {
               
                Candidates.Add(((IEntityBuilder<CandidateSourcingInfo>)this).BuildEntity(reader));
            }

            return (Candidates.Count > 0) ? Candidates : null;
        }
        public IList<DynamicDictionary> BuildEntities(IDataReader reader, IList<string> CheckedList)
        {
            List<DynamicDictionary> re = new List<DynamicDictionary>();
            while (reader.Read())
            {

                re.Add(BuildEntityForReport(reader, CheckedList));

            }

            return (re.Count > 0) ? re : null;
        }
        DynamicDictionary BuildEntityForReport(IDataReader reader, IList<string> CheckedList)
        {
            DynamicDictionary newDyn = new DynamicDictionary();
            foreach (string s in CheckedList)
            {
                try
                {
                    string m = "";
                    //if (s == "Name")
                    //    m = reader["FirstName"].ToString () + " " + reader["LastName"].ToString ();
                    //else 
                    m = reader[s].ToString();
                    if (s == "ResumeSource")
                    {
                        m = TPS360.Common.EnumHelper.GetDescription((TPS360.Common.Shared.ResumeSource)Convert.ToInt32(m));
                    }

                    newDyn.properties.Add(s, m);
                }
                catch
                {
                }
            }
            return newDyn;


        }
        CandidateSourcingInfo IEntityBuilder<CandidateSourcingInfo>.BuildEntity(IDataReader reader)
        {
           
                const int FLD_ID = 0;
                const int FLD_CANDIDNAME = 1;
                const int FLD_REQUISITION = 2;
                const int FLD_ACCOUNT = 3;
                const int FLD_LOCATION = 4;
                const int FLD_RESUMESOURCE = 5;
                const int FLD_CURRENTSTATUS = 6;
                const int FLD_SOURCEDDATE = 7;                
                const int FLD_CLIENTSUBMISSIONDATE = 8;
                const int FLD_OFFEREDDATE = 9;
                const int FLD_JOINEDDATE = 10;
                const int FLD_RECRUITER = 11;
                const int FLD_LEAD = 12;
                const int FLD_JOBPOSTINGCODE = 13;
                const int FLD_REMARKS = 14;
                const int FLD_STATUS = 15;

                // 0.1

                CandidateSourcingInfo CandidateSourcingInfo = new CandidateSourcingInfo();
                CandidateSourcingInfo.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
                CandidateSourcingInfo.CandidateName = reader.IsDBNull(FLD_CANDIDNAME) ? string.Empty : reader.GetString(FLD_CANDIDNAME);
                CandidateSourcingInfo.Requisition = reader.IsDBNull(FLD_REQUISITION) ? string.Empty : reader.GetString(FLD_REQUISITION);
                CandidateSourcingInfo.Account = reader.IsDBNull(FLD_ACCOUNT) ? string.Empty : reader.GetString(FLD_ACCOUNT);
                CandidateSourcingInfo.Location = reader.IsDBNull(FLD_LOCATION) ? string.Empty : reader.GetString(FLD_LOCATION);
                CandidateSourcingInfo.ResumeSourceName = reader.IsDBNull(FLD_RESUMESOURCE) ? string.Empty : reader.GetString(FLD_RESUMESOURCE);
                CandidateSourcingInfo.CurrentStatus = reader.IsDBNull(FLD_CURRENTSTATUS) ? string.Empty : reader.GetString(FLD_CURRENTSTATUS);
                CandidateSourcingInfo.SourcedDate = reader.IsDBNull(FLD_SOURCEDDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_SOURCEDDATE);
                CandidateSourcingInfo.ClientSubmissionDate = reader.IsDBNull(FLD_CLIENTSUBMISSIONDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CLIENTSUBMISSIONDATE);
                CandidateSourcingInfo.OfferedDate = reader.IsDBNull(FLD_OFFEREDDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_OFFEREDDATE);
                CandidateSourcingInfo.Joineddate = reader.IsDBNull(FLD_JOINEDDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_JOINEDDATE);
                CandidateSourcingInfo.Recruiter = reader.IsDBNull(FLD_RECRUITER) ? string.Empty : reader.GetString(FLD_RECRUITER);

                CandidateSourcingInfo.Lead = reader.IsDBNull(FLD_LEAD) ? string.Empty : reader.GetString(FLD_LEAD);
                CandidateSourcingInfo.JobPostingCode = reader.IsDBNull(FLD_JOBPOSTINGCODE) ? string.Empty : reader.GetString(FLD_JOBPOSTINGCODE);

                CandidateSourcingInfo.Remarks = reader.IsDBNull(FLD_REMARKS) ? string.Empty : reader.GetString(FLD_REMARKS);
                CandidateSourcingInfo.Status = reader.IsDBNull(FLD_STATUS) ? 0 : reader.GetInt32(FLD_STATUS);


                return CandidateSourcingInfo;
           

        }

             
       
    }
}
