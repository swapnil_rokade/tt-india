﻿using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class CompanyNoteBuilder : IEntityBuilder<CompanyNote>
    {
        IList<CompanyNote> IEntityBuilder<CompanyNote>.BuildEntities(IDataReader reader)
        {
            List<CompanyNote> companyNotes = new List<CompanyNote>();

            while (reader.Read())
            {
                companyNotes.Add(((IEntityBuilder<CompanyNote>)this).BuildEntity(reader));
            }

            return (companyNotes.Count > 0) ? companyNotes : null;
        }

        CompanyNote IEntityBuilder<CompanyNote>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_COMPANYID = 1;
            const int FLD_COMMONNOTEID = 2;

            CompanyNote companyNote = new CompanyNote();

            companyNote.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            companyNote.CompanyId = reader.IsDBNull(FLD_COMPANYID) ? 0 : reader.GetInt32(FLD_COMPANYID);
            companyNote.CommonNoteId = reader.IsDBNull(FLD_COMMONNOTEID) ? 0 : reader.GetInt32(FLD_COMMONNOTEID);

            using (Context ctx = new Context())
            {
                ICommonNoteDataAccess da = new CommonNoteDataAccess(ctx);
                companyNote.CommonNote = da.GetById(companyNote.CommonNoteId);
            }

            return companyNote;
        }
    }
}