﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class MemberJobCartDetailDataAccess : BaseDataAccess, IMemberJobCartDetailDataAccess
    {
        #region Constructors

        public MemberJobCartDetailDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberJobCartDetail> CreateEntityBuilder<MemberJobCartDetail>()
        {
            return (new MemberJobCartDetailBuilder()) as IEntityBuilder<MemberJobCartDetail>;
        }

        #endregion

        #region  Methods

        MemberJobCartDetail IMemberJobCartDetailDataAccess.Add(MemberJobCartDetail memberJobCartDetail)
        {
            const string SP = "dbo.MemberJobCartDetail_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@AddedDate", DbType.DateTime, NullConverter.Convert(memberJobCartDetail.AddedDate));
                Database.AddInParameter(cmd, "@SelectionStepLookupId", DbType.Int32, memberJobCartDetail.SelectionStepLookupId);
                Database.AddInParameter(cmd, "@AddedBy", DbType.Int32, memberJobCartDetail.AddedBy);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberJobCartDetail.IsRemoved);
                Database.AddInParameter(cmd, "@MemberJobCartId", DbType.Int32, memberJobCartDetail.MemberJobCartId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberJobCartDetail.CreatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberJobCartDetail = CreateEntityBuilder<MemberJobCartDetail>().BuildEntity(reader);
                    }
                    else
                    {
                        memberJobCartDetail = null;
                    }
                }

                if (memberJobCartDetail == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberJobCartDetail already exists. Please specify another memberJobCartDetail.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this memberJobCartDetail.");
                            }
                    }
                }

                return memberJobCartDetail;
            }
        }

        MemberJobCartDetail IMemberJobCartDetailDataAccess.Update(MemberJobCartDetail memberJobCartDetail)
        {
            const string SP = "dbo.MemberJobCartDetail_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberJobCartDetail.Id);
                Database.AddInParameter(cmd, "@AddedDate", DbType.DateTime, NullConverter.Convert(memberJobCartDetail.AddedDate));                
                Database.AddInParameter(cmd, "@AddedBy", DbType.Int32, memberJobCartDetail.AddedBy);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberJobCartDetail.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberJobCartDetail.UpdatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberJobCartDetail = CreateEntityBuilder<MemberJobCartDetail>().BuildEntity(reader);
                    }
                    else
                    {
                        memberJobCartDetail = null;
                    }
                }

                if (memberJobCartDetail == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberJobCartDetail already exists. Please specify another memberJobCartDetail.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this memberJobCartDetail.");
                            }
                    }
                }

                return memberJobCartDetail;
            }
        }

        MemberJobCartDetail IMemberJobCartDetailDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberJobCartDetail_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberJobCartDetail>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberJobCartDetail> IMemberJobCartDetailDataAccess.GetAll()
        {
            const string SP = "dbo.MemberJobCartDetail_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberJobCartDetail>().BuildEntities(reader);
                }
            }
        }

        bool IMemberJobCartDetailDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberJobCartDetail_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a memberJobCartDetail which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this memberJobCartDetail.");
                        }
                }
            }
        }

        bool IMemberJobCartDetailDataAccess.DeleteAllByJobCartId(int memberJobCartId)
        {
            if (memberJobCartId < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberJobCartDetail_DeleteAllByMemberJobCartId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberJobCartId", DbType.Int32, memberJobCartId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a memberJobCartDetail which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this memberJobCartDetail.");
                        }
                }
            }
        }

        bool IMemberJobCartDetailDataAccess.RemoveAllByJobCartId(int memberJobCartId)
        {
            if (memberJobCartId < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberJobCartDetail_RemoveAllByJobCartId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberJobCartId", DbType.Int32, memberJobCartId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a memberJobCartDetail which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this memberJobCartDetail.");
                        }
                }
            }
        }

        IList<MemberJobCartDetail> IMemberJobCartDetailDataAccess.GetAllByMemberJobCartId(int memberJobCartId)
        {
            if (memberJobCartId < 1)
            {
                throw new ArgumentNullException("memberJobCartId");
            }

            const string SP = "dbo.MemberJobCartDetail_GetAllMemberJobCartDetailByMemberJobCartId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberJobCartId", DbType.Int32, memberJobCartId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberJobCartDetail>().BuildEntities(reader);
                }
            }
        }

        MemberJobCartDetail IMemberJobCartDetailDataAccess.GetByMemberJobCartIdAndSelectionStepId(int memberJobCartId, int selectionStepId)
        {
            if (memberJobCartId < 1)
            {
                throw new ArgumentNullException("MemberJobCartId");
            }
            if (selectionStepId < 1)
            {
                throw new ArgumentNullException("SelectionStepId");
            }

            const string SP = "dbo.MemberJobCartDetail_GetMemberJobCartDetailByMemberJobCartIdAndSelectionStepId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberJobCartId", DbType.Int32, memberJobCartId);
                Database.AddInParameter(cmd, "@SelectionStepId", DbType.Int32, selectionStepId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberJobCartDetail>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        MemberJobCartDetail IMemberJobCartDetailDataAccess.GetCurrentByMemberJobCartId(int memberJobCartId)
        {
            if (memberJobCartId < 1)
            {
                throw new ArgumentNullException("MemberJobCartId");
            }
            
            const string SP = "dbo.MemberJobCartDetail_GetCurrentMemberJobCartDetailByMemberJobCartId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberJobCartId", DbType.Int32, memberJobCartId);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberJobCartDetail>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }
        #endregion
    }
}