﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberJobCartDetailBuilder : IEntityBuilder<MemberJobCartDetail>
    {
        IList<MemberJobCartDetail> IEntityBuilder<MemberJobCartDetail>.BuildEntities(IDataReader reader)
        {
            List<MemberJobCartDetail> memberJobCartDetails = new List<MemberJobCartDetail>();

            while (reader.Read())
            {
                memberJobCartDetails.Add(((IEntityBuilder<MemberJobCartDetail>)this).BuildEntity(reader));
            }

            return (memberJobCartDetails.Count > 0) ? memberJobCartDetails : null;
        }

        MemberJobCartDetail IEntityBuilder<MemberJobCartDetail>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_ADDEDDATE = 1;
            const int FLD_SELECTIONSTEPLOOKUPID = 2;
            const int FLD_ADDEDBY = 3;
            const int FLD_ISREMOVED = 4;
            const int FLD_MEMBERJOBCARTID = 5;
            const int FLD_CREATORID = 6;
            const int FLD_UPDATORID = 7;
            const int FLD_CREATEDATE = 8;
            const int FLD_UPDATEDATE = 9;

            MemberJobCartDetail memberJobCartDetail = new MemberJobCartDetail();

            memberJobCartDetail.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberJobCartDetail.AddedDate = reader.IsDBNull(FLD_ADDEDDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_ADDEDDATE);
            memberJobCartDetail.SelectionStepLookupId = reader.IsDBNull(FLD_SELECTIONSTEPLOOKUPID) ? 0 : reader.GetInt32(FLD_SELECTIONSTEPLOOKUPID);
            memberJobCartDetail.AddedBy = reader.IsDBNull(FLD_ADDEDBY) ? 0 : reader.GetInt32(FLD_ADDEDBY);
            memberJobCartDetail.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberJobCartDetail.MemberJobCartId = reader.IsDBNull(FLD_MEMBERJOBCARTID) ? 0 : reader.GetInt32(FLD_MEMBERJOBCARTID);
            memberJobCartDetail.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberJobCartDetail.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberJobCartDetail.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberJobCartDetail.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return memberJobCartDetail;
        }
    }
}