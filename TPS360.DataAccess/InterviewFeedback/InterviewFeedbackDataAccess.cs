﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: InterviewFeedbackDataAccess.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    0.1              27/Dec/2015          Prasanth Kumar G      Introduced function GetByInterviewId_InterviewerEmail
    0.2              24/Dec/2015            pravin khot          Introduced by function GetInterviewFeedbackReport,GetInterviewAssesReportPrint,GetInterviewAssesReportPrint
                                                                  GetByInterviewIdDetail,GetByInterviewIdAllEmail
 ---------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TPS360.Common.Shared;
using TPS360.Common.Helper;
using System.Collections;
using System.Text;
using TPS360.Common.BusinessEntities;
namespace TPS360.DataAccess
{
    internal sealed class InterviewFeedbackDataAccess : BaseDataAccess,IInterviewFeedbackDataAccess
    {
         #region Constructors

        public InterviewFeedbackDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<InterviewFeedback> CreateEntityBuilder<InterviewFeedback>()
        {
            return (new InterviewFeedbackBuilder()) as IEntityBuilder<InterviewFeedback>;
        }

        #endregion

        #region  Methods
        void  IInterviewFeedbackDataAccess.Add(InterviewFeedback interviewFeedback) 
        {
            const string SP = "dbo.InterviewFeedback_Create";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP)) 
            {
                Database.AddInParameter(cmd, "@Title", DbType.String, interviewFeedback.Title);
                Database.AddInParameter(cmd, "@InterviewFeedback", DbType.String, interviewFeedback.Feedback);
                Database.AddInParameter(cmd, "@InterviewRound", DbType.Int32, interviewFeedback.InterviewRound);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, interviewFeedback.CreatorId);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, interviewFeedback.UpdatorId);
                Database.AddInParameter(cmd, "@InterviewId", DbType.Int32, interviewFeedback.InterviewId);
                Database.ExecuteReader(cmd);
             
            }
        
        }

        void IInterviewFeedbackDataAccess.Update(InterviewFeedback interviewFeedback)
        {
            const string SP = "dbo.InterviewFeedback_Update";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, interviewFeedback.Id);
                Database.AddInParameter(cmd, "@Title", DbType.String, interviewFeedback.Title);
                Database.AddInParameter(cmd, "@InterviewFeedback", DbType.String, interviewFeedback.Feedback);
                Database.AddInParameter(cmd, "@InterviewRound", DbType.Int32, interviewFeedback.InterviewRound);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, interviewFeedback.UpdatorId);
                

                Database.ExecuteReader(cmd);

            }

        }

        IList<InterviewFeedback> IInterviewFeedbackDataAccess.GetAll()
        {
            const string SP = "dbo.InterviewFeedback_GetAll";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<InterviewFeedback>().BuildEntities(reader);
                }
            }

        }

        void IInterviewFeedbackDataAccess.DeleteById(int Id)
        {
            if (Id < 1)
            {
                throw new ArgumentNullException("Id");
            }

            const string SP = "dbo.InterviewFeedback_DeleteById";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, Id);
                Database.ExecuteNonQuery(cmd);  
            }

        }

        InterviewFeedback IInterviewFeedbackDataAccess.GetById(int Id)
        {
            if (Id < 1)
            {
                throw new ArgumentNullException("Id");
            }

            const string SP = "dbo.InterviewFeedback_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, Id);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<InterviewFeedback>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }

        }

        //*******************************Code added by pravin khot************************
        IList<InterviewFeedback> IInterviewFeedbackDataAccess.GetByInterviewIdDetail(int InterviewId)
        {
            if (InterviewId < 0)
            {
                throw new ArgumentNullException("InterviewId");
            }
            const string SP = "dbo.InterviewerFeedbackDetail_GetByInterviewId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@InterviewId", DbType.Int32, InterviewId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {

                    return CreateEntityBuilder<InterviewFeedback>().BuildEntities(reader);

                }
            }
        }

        //******************************End************************************************

        //*******************************Code added by pravin khot************************
        IList<InterviewFeedback> IInterviewFeedbackDataAccess.GetByInterviewIdAllEmail(int InterviewId)
        {
            if (InterviewId < 0)
            {
                throw new ArgumentNullException("InterviewId");
            }
            const string SP = "dbo.InterviewerFeedbackDetail_GetByInterviewIdAllEmail";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@InterviewId", DbType.Int32, InterviewId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {

                    return CreateEntityBuilder<InterviewFeedback>().BuildEntities(reader);

                }
            }
        }

        //******************************End************************************************

        IList<InterviewFeedback> IInterviewFeedbackDataAccess.GetByInterviewId(int InterviewId) 
        {
            if (InterviewId < 0) 
            {
                throw new ArgumentNullException("InterviewId");
            }
            const string SP = "dbo.InterviewFeedback_GetByInterviewId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@InterviewId", DbType.Int32, InterviewId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                   
                        return CreateEntityBuilder<InterviewFeedback>().BuildEntities(reader);
               
                }
            }
        }

        PagedResponse<InterviewFeedback> IInterviewFeedbackDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.InterviewFeedback_GetPaged";
            
            string whereClause = string.Empty;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<InterviewFeedback> response = new PagedResponse<InterviewFeedback>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<InterviewFeedback>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }



        //************************************code added by pravin khot on 24/Dec/2015*******************************
        PagedResponse<InterviewFeedback> IInterviewFeedbackDataAccess.GetInterviewFeedbackReport(PagedRequest request)
        {
            const string SP = "dbo.InterviewFeedbackReport_GetReport";

            string whereClause = string.Empty;
            string Min = string.Empty;
            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "InterviewType"))
                        {

                            if (value != string.Empty)
                            {
                                if (value != "Please Select")
                                {
                                    //if (sb.Length > 0)
                                    //{
                                    sb.Append(" and ");
                                    //}
                                    sb.Append(" [Ifeed].[EmailId]");
                                    sb.Append(" = ");
                                    sb.Append("'");
                                    sb.Append(value);
                                    sb.Append("'");
                                }
                            }

                        }
                        if (StringHelper.IsEqual(column, "StartDate"))
                        {
                            if (value != DateTime.MinValue.ToString())
                            {
                                //if (sb.Length > 0)
                                //{
                                sb.Append(" and ");
                                //}
                                sb.Append(" [IV].[StartDateTime]");
                                sb.Append(" >= ");
                                value = "Convert(DateTime,'" + Convert.ToDateTime(value).ToString("dd/MM/yyyy") + "',103) ";
                                sb.Append(value);
                            }
                        }
                        if (StringHelper.IsEqual(column, "EndDate"))
                        {
                            if (value != DateTime.MinValue.ToString())
                            {
                                //if (sb.Length > 0)
                                //{
                                sb.Append(" and ");
                                //}
                                sb.Append(" [IV].[StartDateTime]");
                                sb.Append(" < ");
                                value = "Convert(DateTime,'" + Convert.ToDateTime(value).AddDays(1).ToString("dd/MM/yyyy") + "',103) ";
                                sb.Append(value);

                            }
                        }


                        if (StringHelper.IsEqual(column, "Cid"))
                        {
                            if (value != string.Empty)
                            {
                                //if (sb.Length > 0)
                                //{
                                sb.Append(" and ");
                                //}
                                sb.Append(" [IV].[MemberId]");
                                sb.Append(" = ");
                               // sb.Append("'");     // commented by pravin khot
                                sb.Append(value);
                                // sb.Append("'");       // commented by pravin khot

                            }
                        }

                        if (StringHelper.IsEqual(column, "CName"))
                        {
                            if (value != string.Empty)
                            {
                                //if (sb.Length > 0)
                                //{
                                sb.Append(" and ");
                                //}
                                sb.Append(" [C].[CandidateName] like '%");
                                sb.Append(value);
                                sb.Append("%'");
                            }
                        }
                    }
                }
                whereClause = sb.ToString();
            }
            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = " [IV].[StartDateTime]";
                request.SortOrder = "DESC";
            }

            request.SortColumn = request.SortColumn;


            object[] paramValues = new object[] {	request.PageIndex,
                                                    request.RowPerPage,
                                                    StringHelper.Convert(whereClause),
                                                    StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
                                                };

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<InterviewFeedback> response = new PagedResponse<InterviewFeedback>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {

                    response.Response = (new InterviewFeedbackBuilder()).BuildInterviewFeedbackReport(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }

        }

        //************************************code added by pravin khot on 24/Dec/2015*******************************
        PagedResponse<InterviewFeedback> IInterviewFeedbackDataAccess.GetInterviewFeedbackReportPrint(PagedRequest request)
        {
            const string SP = "dbo.InterviewFeedbackReport_GetReportPrint";

            string whereClause = string.Empty;
            string Min = string.Empty;
            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "Cid"))
                        {
                            if (value != string.Empty)
                            {
                                request.SortColumn = value;

                                //if (sb.Length > 0)
                                //{
                                //    sb.Append(" and ");
                                //}
                                //sb.Append(" [IV].[MemberId] like '%");
                                //sb.Append(value);
                                //sb.Append("%'");
                            }
                        }

                        if (StringHelper.IsEqual(column, "CName"))
                        {
                            if (value != string.Empty)
                            {
                                whereClause = value;

                                //if (sb.Length > 0)
                                //{
                                //    sb.Append(" and ");
                                //}
                                //sb.Append(" [C].[CandidateName] like '%");
                                //sb.Append(value);
                                //sb.Append("%'");
                            }
                        }
                    }
                }

            }
            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = " ";
                request.SortOrder = " ";
            }

            request.SortColumn = request.SortColumn;


            object[] paramValues = new object[] {	request.PageIndex,
                                                    request.RowPerPage,
                                                    StringHelper.Convert(whereClause),
                                                    StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
                                                };

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<InterviewFeedback> response = new PagedResponse<InterviewFeedback>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {

                    response.Response = (new InterviewFeedbackBuilder()).BuildInterviewFeedbackReportPrint(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }

        }
        //************************************code added by pravin khot on 24/Dec/2015*******************************
        PagedResponse<InterviewFeedback> IInterviewFeedbackDataAccess.GetInterviewAssesReportPrint(PagedRequest request)
        {
            const string SP = "dbo.InterviewFeedbackReport_GetReportPrintAssesment";

            string whereClause = string.Empty;
            string Min = string.Empty;
            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");


                        if (StringHelper.IsEqual(column, "Cid"))
                        {
                            if (value != string.Empty)
                            {
                                request.SortColumn = value;
                                //if (sb.Length > 0)
                                //{
                                //    sb.Append(" and ");
                                //}
                                //sb.Append(" [IV].[MemberId] like '%");
                                //sb.Append(value);
                                //sb.Append("%'");
                            }
                        }

                        if (StringHelper.IsEqual(column, "CName"))
                        {
                            if (value != string.Empty)
                            {
                                whereClause = value;
                                //if (sb.Length > 0)
                                //{
                                //    sb.Append(" and ");
                                //}
                                //sb.Append(" [C].[CandidateName] like '%");
                                //sb.Append(value);
                                //sb.Append("%'");
                            }
                        }
                    }
                }
                //whereClause = sb.ToString();
            }
            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = " ";
                request.SortOrder = " ";
            }

            request.SortColumn = request.SortColumn;


            object[] paramValues = new object[] {	request.PageIndex,
                                                    request.RowPerPage,
                                                    StringHelper.Convert(whereClause),
                                                    StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
                                                };

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<InterviewFeedback> response = new PagedResponse<InterviewFeedback>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {

                    response.Response = (new InterviewFeedbackBuilder()).BuildInterviewFeedbackReportPrintAssessment(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }

        }


        //Function introduced by Prasanth on 27/Dec/2015
        InterviewFeedback IInterviewFeedbackDataAccess.GetByInterviewId_InterviewerEmail(int InterviewId, string Email)
        {
            if (InterviewId < 0)
            {
                throw new ArgumentNullException("InterviewId");
            }
            const string SP = "dbo.InterviewFeedback_GetByInterviewId_InterviwerEmail";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@InterviewId", DbType.Int32, InterviewId);
                Database.AddInParameter(cmd, "@InterviewerEmail", DbType.String, Email);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<InterviewFeedback>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        #endregion
    }
}
