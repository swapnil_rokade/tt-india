﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class MemberEducationDataAccess : BaseDataAccess, IMemberEducationDataAccess
    {
        #region Constructors

        public MemberEducationDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberEducation> CreateEntityBuilder<MemberEducation>()
        {
            return (new MemberEducationBuilder()) as IEntityBuilder<MemberEducation>;
        }

        #endregion

        #region  Methods

        MemberEducation IMemberEducationDataAccess.Add(MemberEducation memberEducation)
        {
            const string SP = "dbo.MemberEducation_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);

                Database.AddInParameter(cmd, "@LevelOfEducationLookupId", DbType.Int32, memberEducation.LevelOfEducationLookupId);
                Database.AddInParameter(cmd, "@DegreeTitle", DbType.AnsiString, StringHelper.Convert(memberEducation.DegreeTitle));
                Database.AddInParameter(cmd, "@FieldOfStudyLookupId", DbType.Int32, memberEducation.FieldOfStudyLookupId);
                Database.AddInParameter(cmd, "@MajorSubjects", DbType.AnsiString, StringHelper.Convert(memberEducation.MajorSubjects));
                Database.AddInParameter(cmd, "@GPA", DbType.AnsiString, StringHelper.Convert(memberEducation.GPA));
                Database.AddInParameter(cmd, "@GpaOutOf", DbType.AnsiString, StringHelper.Convert(memberEducation.GpaOutOf));
                Database.AddInParameter(cmd, "@AttendedFrom", DbType.DateTime, NullConverter.Convert(memberEducation.AttendedFrom));
                Database.AddInParameter(cmd, "@AttendedTo", DbType.DateTime, NullConverter.Convert(memberEducation.AttendedTo));
                Database.AddInParameter(cmd, "@DegreeObtainedYear", DbType.AnsiString, StringHelper.Convert(memberEducation.DegreeObtainedYear));
                Database.AddInParameter(cmd, "@InstituteName", DbType.AnsiString, StringHelper.Convert(memberEducation.InstituteName));
                Database.AddInParameter(cmd, "@InstituteCity", DbType.AnsiString, StringHelper.Convert(memberEducation.InstituteCity));
                Database.AddInParameter(cmd, "@InstituteCountryId", DbType.Int32, memberEducation.InstituteCountryId);
                Database.AddInParameter(cmd, "@BriefDescription", DbType.AnsiString, StringHelper.Convert(memberEducation.BriefDescription));
                Database.AddInParameter(cmd, "@IsHighestEducation", DbType.Boolean, memberEducation.IsHighestEducation);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberEducation.IsRemoved);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberEducation.MemberId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberEducation.CreatorId);
                Database.AddInParameter(cmd, "@StateId", DbType.Int32, memberEducation.StateId);  //11065

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberEducation = CreateEntityBuilder<MemberEducation>().BuildEntity(reader);
                    }
                    else
                    {
                        memberEducation = null;
                    }
                }

                if (memberEducation == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member education already exists. Please specify another member education.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this member education.");
                            }
                    }
                }

                return memberEducation;
            }
        }

        MemberEducation IMemberEducationDataAccess.Update(MemberEducation memberEducation)
        {
            const string SP = "dbo.MemberEducation_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberEducation.Id);
                Database.AddInParameter(cmd, "@LevelOfEducationLookupId", DbType.Int32, memberEducation.LevelOfEducationLookupId);
                Database.AddInParameter(cmd, "@DegreeTitle", DbType.AnsiString, StringHelper.Convert(memberEducation.DegreeTitle));
                Database.AddInParameter(cmd, "@FieldOfStudyLookupId", DbType.Int32, memberEducation.FieldOfStudyLookupId);
                Database.AddInParameter(cmd, "@MajorSubjects", DbType.AnsiString, StringHelper.Convert(memberEducation.MajorSubjects));
                Database.AddInParameter(cmd, "@GPA", DbType.AnsiString, StringHelper.Convert(memberEducation.GPA));
                Database.AddInParameter(cmd, "@GpaOutOf", DbType.AnsiString, StringHelper.Convert(memberEducation.GpaOutOf));
                Database.AddInParameter(cmd, "@AttendedFrom", DbType.DateTime, NullConverter.Convert(memberEducation.AttendedFrom));
                Database.AddInParameter(cmd, "@AttendedTo", DbType.DateTime, NullConverter.Convert(memberEducation.AttendedTo));
                Database.AddInParameter(cmd, "@DegreeObtainedYear", DbType.AnsiString, StringHelper.Convert(memberEducation.DegreeObtainedYear));
                Database.AddInParameter(cmd, "@InstituteName", DbType.AnsiString, StringHelper.Convert(memberEducation.InstituteName));
                Database.AddInParameter(cmd, "@InstituteCity", DbType.AnsiString, StringHelper.Convert(memberEducation.InstituteCity));
                Database.AddInParameter(cmd, "@InstituteCountryId", DbType.Int32, memberEducation.InstituteCountryId);
                Database.AddInParameter(cmd, "@BriefDescription", DbType.AnsiString, StringHelper.Convert(memberEducation.BriefDescription));
                Database.AddInParameter(cmd, "@IsHighestEducation", DbType.Boolean, memberEducation.IsHighestEducation);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberEducation.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberEducation.UpdatorId);
                Database.AddInParameter(cmd, "@StateId", DbType.Int32, memberEducation.StateId);  //11065
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberEducation = CreateEntityBuilder<MemberEducation>().BuildEntity(reader);
                    }
                    else
                    {
                        memberEducation = null;
                    }
                }

                if (memberEducation == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberEducation already exists. Please specify another member education.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this member education.");
                            }
                    }
                }

                return memberEducation;
            }
        }

        MemberEducation IMemberEducationDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberEducation_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberEducation>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        MemberEducation IMemberEducationDataAccess.GetByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberEducation_GetMemberEducationByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberEducation>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }
        //evan
        string IMemberEducationDataAccess.GetLevelofEducationByLookupId(int lookupId)
        {
            if (lookupId < 1)
            {
                throw new ArgumentNullException("lookupId");
            }

            const string SP = "dbo.GenericLookup_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, lookupId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return reader["Name"].ToString();
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberEducation> IMemberEducationDataAccess.GetAll()
        {
            const string SP = "dbo.MemberEducation_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberEducation>().BuildEntities(reader);
                }
            }
        }

        IList<MemberEducation> IMemberEducationDataAccess.GetAllMemberEducationByMemberId(int memberId)
        {
            const string SP = "dbo.MemberEducation_GetMemberEducationByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberEducation>().BuildEntities(reader);
                }
            }
        }

        MemberEducation IMemberEducationDataAccess.GetHighestEducationByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberEducation_GetMemberHighestEducationByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberEducation>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }


        PagedResponse<MemberEducation> IMemberEducationDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.MemberEducation_GetPaged";
            string whereClause = string.Empty;

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "IsHighestEducation";
            }

            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "DESC";
            }

            request.SortColumn = "[ME].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberEducation> response = new PagedResponse<MemberEducation>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<MemberEducation>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        PagedResponse<MemberEducation> IMemberEducationDataAccess.GetPagedByMemberId(PagedRequest request)
        {
            const string SP = "dbo.MemberEducation_GetPaged";
            string whereClause = string.Empty;

            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "MemberId"))
                        {
                            sb.Append("[ME].[MemberId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }                        
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "IsHighestEducation";
            }

            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "DESC";
            }

            request.SortColumn = "[ME].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberEducation> response = new PagedResponse<MemberEducation>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<MemberEducation>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        bool IMemberEducationDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberEducation_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member education which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member education.");
                        }
                }
            }
        }

        bool IMemberEducationDataAccess.DeleteByMemberId(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberEducation_DeleteMemberEducationByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member education which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member education.");
                        }
                }
            }
        }
        #endregion
    }
}