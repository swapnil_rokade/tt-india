﻿using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class CandidateReportDashboardBuilder : IEntityBuilder<CandidateReportDashboard>
    {
        IList<CandidateReportDashboard> IEntityBuilder<CandidateReportDashboard>.BuildEntities(IDataReader reader)
        {
            List<CandidateReportDashboard> candidateReportDashboard = new List<CandidateReportDashboard>();

            while (reader.Read())
            {
                candidateReportDashboard.Add(((IEntityBuilder<CandidateReportDashboard>)this).BuildEntity(reader));
            }

            return (candidateReportDashboard.Count > 0) ? candidateReportDashboard : null;
        }

        CandidateReportDashboard IEntityBuilder<CandidateReportDashboard>.BuildEntity(IDataReader reader)
        {
            const int FLD_NEWAPPLICANT = 0;
            const int FLD_UPDATEDAPPLICANT = 1;
            const int FLD_BYINTERVIEW = 2;
            const int FLD_BYINDUSTRY = 3;
            const int FLD_BYFUNCTIONALCATEGORY = 4;
            const int FLD_BYWORKPERMIT = 5;
            const int FLD_BYGENDER = 6;
            const int FLD_BYMARITALSTATUS = 7;
            const int FLD_BYEDUCATION = 8;
            const int FLD_BYTAKENTESTS = 9;
            const int FLD_BYBROADCASTEDRESUME = 10;
            const int FLD_BYREFERRED = 11;
            const int FLD_BYJOBAGENT= 12;
            const int FLD_BYLOCATION = 13;
            const int FLD_BYASSIGNEDMANAGER = 14;

            CandidateReportDashboard candidateReportDashboard = new CandidateReportDashboard();
            int i = 0;
            while (reader.Read())
            {
                if (i == FLD_NEWAPPLICANT)
                {
                    candidateReportDashboard.NewApplicant = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                }
                else if (i == FLD_UPDATEDAPPLICANT)
                {
                    candidateReportDashboard.UpdatedApplicant = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                }
                else if (i == FLD_BYINTERVIEW)
                {
                    candidateReportDashboard.ByInterview = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                }
                else if (i == FLD_BYINDUSTRY)
                {
                    candidateReportDashboard.ByIndustry = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                }
                else if (i == FLD_BYFUNCTIONALCATEGORY)
                {
                    candidateReportDashboard.ByFunctionalCategory = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                }
                else if (i == FLD_BYWORKPERMIT)
                {
                    candidateReportDashboard.ByWorkPermit = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                }
                else if (i == FLD_BYGENDER)
                {
                    candidateReportDashboard.ByGender = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                }
                else if (i == FLD_BYMARITALSTATUS)
                {
                    candidateReportDashboard.ByMaritalStatus = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                }
                else if (i == FLD_BYEDUCATION)
                {
                    candidateReportDashboard.ByEducation = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                }
                else if (i == FLD_BYTAKENTESTS)
                {
                    candidateReportDashboard.ByPerformedTests = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                }
                else if (i == FLD_BYBROADCASTEDRESUME)
                {
                    candidateReportDashboard.ByBroadCastedResume = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                }
                else if (i == FLD_BYREFERRED)
                {
                    candidateReportDashboard.ByReferred = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                }
                else if (i == FLD_BYJOBAGENT)
                {
                    candidateReportDashboard.ByJobAgent = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                }
                else if (i == FLD_BYLOCATION)
                {
                    candidateReportDashboard.ByLocation = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                }
                else if (i == FLD_BYASSIGNEDMANAGER)
                {
                    candidateReportDashboard.ByAssignedManager = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                }
                i++;
            }

            return candidateReportDashboard;
        }
    }
}