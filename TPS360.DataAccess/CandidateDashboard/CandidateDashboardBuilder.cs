﻿using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using System;

namespace TPS360.DataAccess
{
    internal sealed class CandidateDashboardBuilder : IEntityBuilder<CandidateDashboard>
    {
        IList<CandidateDashboard> IEntityBuilder<CandidateDashboard>.BuildEntities(IDataReader reader)
        {
            List<CandidateDashboard> candidateDashboard = new List<CandidateDashboard>();

            while (reader.Read())
            {
                candidateDashboard.Add(((IEntityBuilder<CandidateDashboard>)this).BuildEntity(reader));
            }

            return (candidateDashboard.Count > 0) ? candidateDashboard : null;
        }

        CandidateDashboard IEntityBuilder<CandidateDashboard>.BuildEntity(IDataReader reader)
        {
            const int FLD_JOBSADDEDSINCELASTLOGIN = 0;
            const int FLD_JOBSINJOBCART = 1;
            const int FLD_JOBSAPPLIED = 2;
            const int FLD_JOBSSUBMITTED = 3;
            const int FLD_INTERVIEWPENDING = 4;
            const int FLD_INTERVIEWCOMPLETED = 5;
            const int FLD_INTERVIEWFEEDBACKED = 6;
            const int FLD_TESTPENDING = 7;
            const int FLD_TESTCOMPLETED = 8;
            const int FLD_HOTLIST = 9;
            const int FLD_CREATEDATE = 10;
            const int FLD_UPDATEDATE = 11;

            CandidateDashboard candidateDashboard = new CandidateDashboard();
            candidateDashboard.JobsAddedSinceLastLogin = reader.IsDBNull(FLD_JOBSADDEDSINCELASTLOGIN) ? 0 : reader.GetInt32(FLD_JOBSADDEDSINCELASTLOGIN);
            candidateDashboard.JobsInJobCart = reader.IsDBNull(FLD_JOBSINJOBCART) ? 0 : reader.GetInt32(FLD_JOBSINJOBCART);
            candidateDashboard.JobsApplied = reader.IsDBNull(FLD_JOBSAPPLIED) ? 0 : reader.GetInt32(FLD_JOBSAPPLIED);
            candidateDashboard.JobsSubmitted = reader.IsDBNull(FLD_JOBSSUBMITTED) ? 0 : reader.GetInt32(FLD_JOBSSUBMITTED);
            candidateDashboard.InterviewsPending = reader.IsDBNull(FLD_INTERVIEWPENDING) ? 0 : reader.GetInt32(FLD_INTERVIEWPENDING);
            candidateDashboard.InterviewsCompleted = reader.IsDBNull(FLD_INTERVIEWCOMPLETED) ? 0 : reader.GetInt32(FLD_INTERVIEWCOMPLETED);
            candidateDashboard.InterviewsFeedbacked = reader.IsDBNull(FLD_INTERVIEWFEEDBACKED) ? 0 : reader.GetInt32(FLD_INTERVIEWFEEDBACKED);
            candidateDashboard.TestsPending = reader.IsDBNull(FLD_TESTPENDING) ? 0 : reader.GetInt32(FLD_TESTPENDING);
            candidateDashboard.TestsCompleted = reader.IsDBNull(FLD_TESTCOMPLETED) ? 0 : reader.GetInt32(FLD_TESTCOMPLETED);
            candidateDashboard.HotList = reader.IsDBNull(FLD_HOTLIST) ? 0 : reader.GetInt32(FLD_HOTLIST);
            candidateDashboard.RegistrationDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            candidateDashboard.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            return candidateDashboard;
        }
    }
}