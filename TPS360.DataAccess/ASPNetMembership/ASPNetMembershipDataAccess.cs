﻿using System;
using System.Data;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class ASPNetMembershipDataAccess : BaseDataAccess, IASPNetMembershipDataAccess
    {
        #region Constructors

        public ASPNetMembershipDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<ApplicationWorkflowMap> CreateEntityBuilder<ApplicationWorkflowMap>()
        {
            return (new ApplicationWorkflowMapBuilder()) as IEntityBuilder<ApplicationWorkflowMap>;
        }
        
        #endregion

        #region  Methods

        bool IASPNetMembershipDataAccess.LockUser(Guid userId)
        {
            const string SP = "dbo.aspnet_Membership_LockUser";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);

                Database.AddInParameter(cmd, "@UserId", DbType.Guid, userId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_LOCK:
                        {
                            return true;
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while locking this user.");
                        }
                }
            }
        }

        #endregion
    }
}