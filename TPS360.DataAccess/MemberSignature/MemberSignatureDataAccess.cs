﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class MemberSignatureDataAccess : BaseDataAccess, IMemberSignatureDataAccess
    {
        #region Constructors

        public MemberSignatureDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberSignature> CreateEntityBuilder<MemberSignature>()
        {
            return (new MemberSignatureBuilder()) as IEntityBuilder<MemberSignature>;
        }

        #endregion

        #region  Methods

        MemberSignature IMemberSignatureDataAccess.Add(MemberSignature memberSignature)
        {
            const string SP = "dbo.MemberSignature_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Signature", DbType.AnsiString, StringHelper.Convert(memberSignature.Signature));
                Database.AddInParameter(cmd, "@Logo", DbType.AnsiString, StringHelper.Convert(memberSignature.Logo));
                Database.AddInParameter(cmd, "@Active", DbType.Boolean, memberSignature.Active);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberSignature.IsRemoved);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberSignature.MemberId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberSignature.CreatorId);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberSignature = CreateEntityBuilder<MemberSignature>().BuildEntity(reader);
                    }
                    else
                    {
                        memberSignature = null;
                    }
                }

                if (memberSignature == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member signature already exists. Please specify another member signature.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this member signature.");
                            }
                    }
                }

                return memberSignature;
            }
        }

        MemberSignature IMemberSignatureDataAccess.Update(MemberSignature memberSignature)
        {
            const string SP = "dbo.MemberSignature_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberSignature.Id);
                Database.AddInParameter(cmd, "@Signature", DbType.AnsiString, StringHelper.Convert(memberSignature.Signature));
                Database.AddInParameter(cmd, "@Logo", DbType.AnsiString, StringHelper.Convert(memberSignature.Logo));
                Database.AddInParameter(cmd, "@Active", DbType.Boolean, memberSignature.Active);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberSignature.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberSignature.UpdatorId);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberSignature = CreateEntityBuilder<MemberSignature>().BuildEntity(reader);
                    }
                    else
                    {
                        memberSignature = null;
                    }
                }

                if (memberSignature == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member signature already exists. Please specify another member signature.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this member signature.");
                            }
                    }
                }

                return memberSignature;
            }
        }

        MemberSignature IMemberSignatureDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberSignature_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberSignature>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        MemberSignature IMemberSignatureDataAccess.GetByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberSignature_GetActiveMemberSignatureByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberSignature>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberSignature> IMemberSignatureDataAccess.GetAllByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberSignature_GetAllMemberSignatureByMemberId";
            
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberSignature>().BuildEntities(reader);
                }
            }
            
        }

        IList<MemberSignature> IMemberSignatureDataAccess.GetAll()
        {
            const string SP = "dbo.MemberSignature_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberSignature>().BuildEntities(reader);
                }
            }
        }

        bool IMemberSignatureDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberSignature_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member signature which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member signature.");
                        }
                }
            }
        }

        bool IMemberSignatureDataAccess.DeleteByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberSignature_DeleteMemberSignatureByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member signature which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member signature.");
                        }
                }
            }
        }

        #endregion
    }
}