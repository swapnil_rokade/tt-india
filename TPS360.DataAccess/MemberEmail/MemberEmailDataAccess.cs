﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberEmailDataAccess.cs
    Description: This is the .cs file used for data accesse in email pages.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Dec-05-2008           Jagadish            Defect id: 9358; changed 'Or' to 'And' in the Query
    0.2            Dec-26-2008           Jagadish            Defect id: 9290; Changes made in method Getpaged().
-------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;
using System.Collections;
using System.Text;

namespace TPS360.DataAccess
{
    internal sealed class MemberEmailDataAccess : BaseDataAccess, IMemberEmailDataAccess
    {
        #region Constructors

        public MemberEmailDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberEmail> CreateEntityBuilder<MemberEmail>()
        {
            return (new MemberEmailBuilder()) as IEntityBuilder<MemberEmail>;
        }

        #endregion

        #region  Methods
     

        MemberEmail IMemberEmailDataAccess.Add(MemberEmail memberEmail)
        {
            const string SP = "dbo.MemberEmail_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@SenderId", DbType.Int32, memberEmail.SenderId);
                Database.AddInParameter(cmd, "@SenderEmail", DbType.AnsiString, memberEmail.SenderEmail);
                Database.AddInParameter(cmd, "@ReceiverId", DbType.Int32, memberEmail.ReceiverId);
                Database.AddInParameter(cmd, "@ReceiverEmail", DbType.AnsiString, memberEmail.ReceiverEmail);
                Database.AddInParameter(cmd, "@EmailTypeLookupId", DbType.Int32, memberEmail.EmailTypeLookupId);
                Database.AddInParameter(cmd, "@Subject", DbType.AnsiString, memberEmail.Subject);
                Database.AddInParameter(cmd, "@EmailBody", DbType.AnsiString, memberEmail.EmailBody);
                Database.AddInParameter(cmd, "@Status", DbType.Int32, memberEmail.Status);
                Database.AddInParameter(cmd, "@ParentId", DbType.Int32, memberEmail.ParentId);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberEmail.IsRemoved);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberEmail.CreatorId);
                Database.AddInParameter(cmd, "@BCC", DbType.AnsiString, memberEmail .BCC );
                Database.AddInParameter(cmd, "@CC", DbType.AnsiString, memberEmail .CC );
                Database.AddInParameter(cmd, "@SentDate", DbType .DateTime   ,memberEmail .SentDate);
                Database.AddInParameter(cmd, "@AttachedFileNames", DbType.AnsiString, memberEmail.AttachedFileNames );
                Database.AddInParameter(cmd, "@NoOfAttachments", DbType.Int32 , memberEmail.NoOfAttachedFiles );
              
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberEmail = CreateEntityBuilder<MemberEmail>().BuildEntity(reader);
                    }
                    else
                    {
                        memberEmail = null;
                    }
                }

                if (memberEmail == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member email already exists. Please specify another member email.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this member email.");
                            }
                    }  
                }
              
                return memberEmail;
            }
        }

        IList<MemberEmail> IMemberEmailDataAccess.GetByEmailAddressAndCreatedDate(string emailAddress, string createdDate)
        {


            const string SP = "dbo.MemberEmail_GetByEmailAddressAndCreatedDate";
  
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@emailAddress", DbType.String, emailAddress );
                Database.AddInParameter(cmd, "@createdDate", DbType.String , createdDate );

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberEmail>().BuildEntities(reader);
                }
            }
        }
        bool IMemberEmailDataAccess.ValidateSenderEmailID(string PrimaryEmail)
        {
            const string SP = "dbo.MemberEmail_SenderIdValidate";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                //AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@PrimaryEmail", DbType.String ,PrimaryEmail );

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        //return  CreateEntityBuilder<MemberEmail>().BuildEntity(reader);
                        return true;
                    }
                    else
                    {
                        return  false ;
                    }
                }

           
            }
        }

        MemberEmail IMemberEmailDataAccess.Update(MemberEmail memberEmail)
        {
            const string SP = "dbo.MemberEmail_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberEmail.Id);
                Database.AddInParameter(cmd, "@SenderId", DbType.Int32, memberEmail.SenderId);
                Database.AddInParameter(cmd, "@SenderEmail", DbType.AnsiString, memberEmail.SenderEmail);
                Database.AddInParameter(cmd, "@ReceiverId", DbType.Int32, memberEmail.ReceiverId);
                Database.AddInParameter(cmd, "@ReceiverEmail", DbType.AnsiString, memberEmail.ReceiverEmail);
                Database.AddInParameter(cmd, "@EmailTypeLookupId", DbType.Int32, memberEmail.EmailTypeLookupId);
                Database.AddInParameter(cmd, "@Subject", DbType.AnsiString, memberEmail.Subject);
                Database.AddInParameter(cmd, "@EmailBody", DbType.AnsiString, memberEmail.EmailBody);
                Database.AddInParameter(cmd, "@Status", DbType.Int32, memberEmail.Status);
                Database.AddInParameter(cmd, "@ParentId", DbType.Int32, memberEmail.ParentId);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberEmail.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberEmail.UpdatorId);
                Database.AddInParameter(cmd, "@AttachedFileNames", DbType.AnsiString, memberEmail.AttachedFileNames);
                Database.AddInParameter(cmd, "@NoOfAttachments", DbType.Int32, memberEmail.NoOfAttachedFiles);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberEmail = CreateEntityBuilder<MemberEmail>().BuildEntity(reader);
                    }
                    else
                    {
                        memberEmail = null;
                    }
                }

                if (memberEmail == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member email already exists. Please specify another member email.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this member email.");
                            }
                    }
                }

                return memberEmail;
            }
        }

        MemberEmail IMemberEmailDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberEmail_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberEmail>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        MemberEmail IMemberEmailDataAccess.GetMemberEmailSenderByMemberIdSubjectDate(Int32 memberId,string subject, DateTime date)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "[dbo].[MemberEmailSender_GetByMemberIdSubjectDate]";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                Database.AddInParameter(cmd, "@Subject", DbType.String, subject);
                Database.AddInParameter(cmd, "@Date", DbType.DateTime, date);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberEmail>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        MemberEmail IMemberEmailDataAccess.GetMemberEmailReceiverByMemberIdSubjectDate(Int32 memberId, string subject, DateTime date)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "[dbo].[MemberEmailReceiver_GetByMemberIdSubjectDate]";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                Database.AddInParameter(cmd, "@Subject", DbType.String, subject);
                Database.AddInParameter(cmd, "@Date", DbType.DateTime, date);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberEmail>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberEmail> IMemberEmailDataAccess.Synchronise(string fromdate, string todate, string subject, string toemail, string uid)
        {
            const string SP = "dbo.OutMail_Sync";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@fromdate", DbType.String, fromdate);
                Database.AddInParameter(cmd, "@todate", DbType.String, todate);
                Database.AddInParameter(cmd, "@subject", DbType.String, subject);
                Database.AddInParameter(cmd, "@toemail", DbType.String, toemail);
                Database.AddInParameter(cmd, "@uid", DbType.String, uid);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    //if (reader.Read())
                    //{
                        return CreateEntityBuilder<MemberEmail>().BuildEntities(reader);
                    //}
                    //else
                    //{
                    //    return null;
                    //}
                }
            }
        }

        IList<MemberEmail> IMemberEmailDataAccess.GetAll()
        {
            const string SP = "dbo.MemberEmail_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberEmail>().BuildEntities(reader);
                }
            }
        }

        IList<MemberEmail> IMemberEmailDataAccess.GetByEmailAddress(string emailAddress)
        {
            const string SP = "dbo.MemberEmail_GetByEmailAddress";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@EmailAddress", DbType.AnsiString, emailAddress);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberEmail>().BuildEntities(reader);
                }
            }
        }

        PagedResponse<MemberEmail> IMemberEmailDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.MemberEmail_GetPaged";
            bool flag = false;
            string whereClause = string.Empty;
            string CompanyId = string.Empty;
            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");
                        
                        if (StringHelper.IsEqual(column, "EmailTypeLookupId"))
                        {
                            if (sb.ToString() != "") sb.Append(" And ");
                            if (Convert.ToInt32(value) == 390)
                                sb.Append(" [ME].[EmailTypeLookupId] in (390,392,393)");
                            else
                                sb.Append(" [ME].[EmailTypeLookupId] = 391");
                        }
                        //if (sb.ToString() != "")
                        //{
                        //    if (StringHelper.IsEqual(column, "EmailTypeLookupId"))
                        //    {
                        //        sb.Append(" And [ME].[EmailTypeLookupId]");
                        //        sb.Append(" = ");
                        //        sb.Append(value);

                        //    }
                        //}
                        //else
                        //{
                        //    if (StringHelper.IsEqual(column, "EmailTypeLookupId"))
                        //    {
                        //        sb.Append(" [ME].[EmailTypeLookupId]");
                        //        sb.Append(" = ");
                        //        sb.Append(value);

                        //    }
                        //}
                        if (sb.ToString() != "")
                        {
                            if (StringHelper.IsEqual(column, "SenderId"))
                            {
                                sb.Append(" And [ME].[SenderId]");
                                sb.Append(" = ");
                                sb.Append(value);
                            }
                        }
                        else
                        {
                            if (StringHelper.IsEqual(column, "SenderId"))
                            {
                                sb.Append(" [ME].[SenderId]");
                                sb.Append(" = ");
                                sb.Append(value);
                            }

                        }
                        if (sb.ToString() != "")
                        {
                            if (StringHelper.IsEqual(column, "ReceiverId"))
                            {
                                sb.Append(" And ([ME].[ReceiverId]");
                                sb.Append(" = ");
                                sb.Append(value);
                                sb.Append (" OR ';'+ receiveremail+';' like (select '%;'+primaryemail+';%' from member where id= ");
                                sb.Append (value );
                                sb.Append (" )");
                                sb.Append(" OR ';'+ cc+';' like (select '%;'+primaryemail+';%' from member where id= ");
                                sb.Append(value);
                                sb.Append(" )");
                                sb.Append(" OR ';'+ bcc+';' like (select '%;'+primaryemail+';%' from member where id= ");
                                sb.Append(value);
                                sb.Append(" ))");
                            }
                        }
                        else
                        {
                            if (StringHelper.IsEqual(column, "ReceiverId"))
                            {
                                sb.Append(" ([ME].[ReceiverId]");
                                sb.Append(" = ");
                                sb.Append(value);
                                sb.Append(" OR ';'+ receiveremail+';' like (select '%;'+primaryemail+';%' from member where id= ");
                                sb.Append(value);
                                sb.Append(" )");
                                sb.Append(" OR ';'+ cc+';' like (select '%;'+primaryemail+';%' from member where id= ");
                                sb.Append(value);
                                sb.Append(" )");
                                sb.Append(" OR ';'+ bcc+';' like (select '%;'+primaryemail+';%' from member where id= ");
                                sb.Append(value);
                                sb.Append(" ))");
                            }

                        }
                        if (sb.ToString() != "")
                        {
                            if (StringHelper.IsEqual(column, "companyId"))
                            {
                                if (Convert.ToInt32 (value )>0)
                                {
                                    //sb.Append(" And ([ReceiverId] in (select memberid from companycontact where companyid=");
                                    //sb.Append(value);
                                    //sb.Append(")");
                                    //sb.Append(" OR [ME].[SenderId] in (select memberid from companycontact where companyid=");
                                    //sb.Append(value);
                                    //sb.Append("))");
                                    CompanyId = value;
                                }
                            }
                        }
                        else
                        {
                            if (StringHelper.IsEqual(column, "companyId"))
                            {
                                if (Convert.ToInt32(value) > 0)
                                {
                                    //sb.Append(" [ME].[ReceiverId] in (select memberid from companycontact where companyid=");
                                    //sb.Append(value);
                                    //sb.Append(")");
                                    //sb.Append(" Or [ME].[SenderId] in (select memberid from companycontact where companyid=");
                                    //sb.Append(value);
                                    //sb.Append(")");
                                    CompanyId = value;
                                }
                            }
                        }
                        if (sb.ToString() != "")
                        {
                            if (StringHelper.IsEqual(column, "company"))
                            {
                                if (Convert.ToInt32(value) > 0)
                                {
                                    //sb.Append(" And ([ReceiverId] in (select memberid from companycontact where companyid=");
                                    //sb.Append(value);
                                    //sb.Append("))");
                                    CompanyId = value;
                                }
                            }
                        }
                        else
                        {
                            if (StringHelper.IsEqual(column, "company"))
                            {
                                if (Convert.ToInt32(value) > 0)
                                {
                                    //sb.Append(" [ME].[ReceiverId] in (select memberid from companycontact where companyid=");
                                    //sb.Append(value);
                                    //sb.Append(")");
                                    CompanyId = value;
                                }
                            }
                        }
                        if (sb.ToString() != "")
                        {
                            if (StringHelper.IsEqual(column, "comId"))
                            {
                                if (Convert.ToInt32(value) > 0)
                                {
                                    //sb.Append(" And [ME].[SenderId] in (select memberid from companycontact where companyid=");
                                    //sb.Append(value);
                                    //sb.Append(")");
                                    CompanyId = value;
                                }
                            }
                        }
                        else
                        {
                            if (StringHelper.IsEqual(column, "comId"))
                            {
                                if (Convert.ToInt32(value) > 0)
                                {
                                    //sb.Append("[ME].[SenderId] in (select memberid from companycontact where companyid=");
                                    //sb.Append(value);
                                    //sb.Append(")");
                                    CompanyId = value;
                                }
                            }
                        }
                        if (sb.ToString() != "")
                        {
                            if (StringHelper.IsEqual(column, "senId"))
                            {
                                sb.Append(" And ([ME].[ReceiverId]");
                                sb.Append(" = ");
                                sb.Append(value);
                                sb.Append(" OR ';'+ receiveremail+';' like (select '%;'+primaryemail+';%' from member where id= ");
                                sb.Append(value);
                                sb.Append(" )");
                                sb.Append(" OR ';'+ cc+';' like (select '%;'+primaryemail+';%' from member where id= ");
                                sb.Append(value);
                                sb.Append(" )");
                                sb.Append(" OR ';'+ bcc+';' like (select '%;'+primaryemail+';%' from member where id= ");
                                sb.Append(value);
                                sb.Append(" )");
                                sb.Append(" OR [ME].[SenderId]");
                                sb.Append(" = ");
                                sb.Append(value);
                                sb.Append(")");
                            }
                        }
                        else
                        {
                            if (StringHelper.IsEqual(column, "senId"))
                            {
                                sb.Append(" [ME].[ReceiverId]");
                                sb.Append(" = ");
                                sb.Append(value);
                                sb.Append(" OR ';'+ receiveremail+';' like (select '%;'+primaryemail+';%' from member where id= ");
                                sb.Append(value);
                                sb.Append(" )");
                                sb.Append(" OR ';'+ cc+';' like (select '%;'+primaryemail+';%' from member where id= ");
                                sb.Append(value);
                                sb.Append(" )");
                                sb.Append(" OR ';'+ bcc+';' like (select '%;'+primaryemail+';%' from member where id= ");
                                sb.Append(value);
                                sb.Append(" )");
                                sb.Append(" Or [ME].[SenderId]");
                                sb.Append(" = ");
                                sb.Append(value);
                            }
                        }
                        if (sb.ToString() != "")
                        {
                            if (StringHelper.IsEqual(column, "RecId"))
                            {
                                sb.Append(" Or [ME].[SenderId]");
                                sb.Append(" = ");
                                sb.Append(value);
                            }
                        }
                        else
                        {
                            if (StringHelper.IsEqual(column, "RecId"))
                            {
                                sb.Append(" [ME].[SenderId]");
                                sb.Append(" = ");
                                sb.Append(value);
                            }
                        }
                        // Defect # 9566


                        if (StringHelper.IsEqual(column, "AnyKey"))
                        {
                            if (value != "")
                            {
                                // 0.2 starts here
                                //whereClause = string.Empty;
                                //whereClause = BuildAnyKeyWordQuery(value);
                                //flag = true;
                                //break; 
                                if (sb.ToString() != "")
                                {
                                    sb.Append(" And");
                                    sb.Append(BuildAnyKeyWordQuery(value));
                                }
                                else
                                {
                                    sb.Append(BuildAnyKeyWordQuery(value));
                                }
                                // 0.2 ends here
                            }
                        }
                        
                    }
                }
                if (!flag)
                {
                    whereClause = sb.ToString();
                }
            }


            
            if (StringHelper.IsBlank(request.SortColumn))
            {
                //request.SortColumn = "CreateDate";
                request.SortColumn = "SentDate";
                request.SortOrder = "desc";
            }

            request.SortColumn = "[ME].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder),
                                                    StringHelper.Convert(CompanyId )
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberEmail> response = new PagedResponse<MemberEmail>();
                try
                {
                    using (IDataReader reader = Database.ExecuteReader(cmd))
                    {
                        response.Response = CreateEntityBuilder<MemberEmail>().BuildEntities(reader);

                        if ((reader.NextResult()) && (reader.Read()))
                        {
                            response.TotalRow = reader.GetInt32(0);
                        }
                    }

                    return response;
                }
                catch (Exception ex)
                {
                    return response;
                }
            }
        }


        private string BuildAnyKeyWordQuery(string anyKeyWords)
        {
            StringBuilder sbWhereClause = new StringBuilder();

            string[] keyWords = StringHelper.BuildKeyWordArray(anyKeyWords);

            for (int i = 0; i < keyWords.Length; i++)
            {
                if (sbWhereClause.ToString() == String.Empty)
                {
                    sbWhereClause.Append(" ( [ME].[SenderEmail] LIKE '%" + keyWords[i].Trim() + "%'");
                    sbWhereClause.Append(" OR [ME].[ReceiverEmail] LIKE '%" + keyWords[i].Trim() + "%' ");
                    sbWhereClause.Append(" OR [ME].[Subject] LIKE '%" + keyWords[i].Trim() + "%' ");
                    sbWhereClause.Append(" OR Contains([ME].[EmailBody], '\"" + keyWords[i].Trim() + "\"') ) ");
                }
                else
                {
                    sbWhereClause.Append(" OR ");
                    sbWhereClause.Append(" ( [ME].[SenderEmail] LIKE '%" + keyWords[i].Trim() + "%'");
                    sbWhereClause.Append(" OR [ME].[ReceiverEmail] LIKE '%" + keyWords[i].Trim() + "%' ");
                    sbWhereClause.Append(" OR [ME].[Subject] LIKE '%" + keyWords[i].Trim() + "%' ");
                    sbWhereClause.Append(" OR Contains([ME].[EmailBody], '\"" + keyWords[i].Trim() + "\"') ) ");
                }
            }

            return sbWhereClause.ToString();
        }

        bool IMemberEmailDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberEmail_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member email which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member email.");
                        }
                }
            }
        }

        string IMemberEmailDataAccess.GetMemberEmailTypeById (int Id)
        {
            if (Id < 1)
            {
                throw new ArgumentNullException("Id");
            }
            const string SP = "dbo.MemberEmail_GetTypeById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, Id);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        return reader.IsDBNull(0) ? string.Empty : reader.GetString(0);
                    }
                    return string.Empty;
                }
            }

        }
       

        #endregion
    }
}