﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberEmailBuilder : IEntityBuilder<MemberEmail>
    {
        IList<MemberEmail> IEntityBuilder<MemberEmail>.BuildEntities(IDataReader reader)
        {
            List<MemberEmail> memberEmails = new List<MemberEmail>();

            while (reader.Read())
            {
                memberEmails.Add(((IEntityBuilder<MemberEmail>)this).BuildEntity(reader));
            }

            return (memberEmails.Count > 0) ? memberEmails : null;
        }

        MemberEmail IEntityBuilder<MemberEmail>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_SENDERID = 1;
            const int FLD_SENDEREMAIL = 2;
            const int FLD_RECEIVERID = 3;
            const int FLD_RECEIVEREMAIL = 4;
            const int FLD_EMAILTYPELOOKUPID = 5;
            const int FLD_SUBJECT = 6;
            const int FLD_EMAILBODY = 7;
            const int FLD_STATUS = 8;
            const int FLD_PARENTID = 9;
            const int FLD_ISREMOVED = 10;
            const int FLD_CREATORID = 11;
            const int FLD_UPDATORID = 12;
            const int FLD_CREATEDATE = 13;
            const int FLD_UPDATEDATE = 14;
            const int FLD_SENTDATE = 15;
            const int FLD_BCC = 16;
            const int FLD_CC = 17;
            const int FLD_ATTACHEDFILENAMES = 18;
            const int FLD_NOOFATTACHMENTS = 19;
            MemberEmail memberEmail = new MemberEmail();

            memberEmail.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberEmail.SenderId = reader.IsDBNull(FLD_SENDERID) ? 0 : reader.GetInt32(FLD_SENDERID);
            memberEmail.SenderEmail = reader.IsDBNull(FLD_SENDEREMAIL) ? string.Empty : reader.GetString(FLD_SENDEREMAIL);
            memberEmail.ReceiverId = reader.IsDBNull(FLD_RECEIVERID) ? 0 : reader.GetInt32(FLD_RECEIVERID);
            memberEmail.ReceiverEmail = reader.IsDBNull(FLD_RECEIVEREMAIL) ? string.Empty : reader.GetString(FLD_RECEIVEREMAIL);
            memberEmail.EmailTypeLookupId = reader.IsDBNull(FLD_EMAILTYPELOOKUPID) ? 0 : reader.GetInt32(FLD_EMAILTYPELOOKUPID);
            memberEmail.Subject = reader.IsDBNull(FLD_SUBJECT) ? string.Empty : reader.GetString(FLD_SUBJECT);
            memberEmail.EmailBody = reader.IsDBNull(FLD_EMAILBODY) ? string.Empty : reader.GetString(FLD_EMAILBODY);
            memberEmail.Status = reader.IsDBNull(FLD_STATUS) ? 0 : reader.GetInt32(FLD_STATUS);
            memberEmail.ParentId = reader.IsDBNull(FLD_PARENTID) ? 0 : reader.GetInt32(FLD_PARENTID);
            memberEmail.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberEmail.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberEmail.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberEmail.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberEmail.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            memberEmail.SentDate = reader.IsDBNull(FLD_SENTDATE) ? string.Empty : reader.GetDateTime(FLD_SENTDATE).ToString();
            memberEmail.BCC = reader.IsDBNull(FLD_BCC) ? string.Empty : reader.GetString(FLD_BCC);
            memberEmail.CC  = reader.IsDBNull(FLD_CC ) ? string.Empty : reader.GetString(FLD_CC);
            memberEmail.AttachedFileNames  = reader.IsDBNull(FLD_ATTACHEDFILENAMES ) ? string.Empty : reader.GetString(FLD_ATTACHEDFILENAMES );
            memberEmail.NoOfAttachedFiles  = reader.IsDBNull(FLD_NOOFATTACHMENTS ) ? 0 : reader.GetInt32(FLD_NOOFATTACHMENTS );
            return memberEmail;
        }
    }
}