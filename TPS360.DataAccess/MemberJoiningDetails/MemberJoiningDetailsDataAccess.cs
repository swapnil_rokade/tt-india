﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Collections;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class MemberJoiningDetailsDataAccess : BaseDataAccess, IMemberJoiningDetailsDataAccess
    {
                #region Constructors

        public MemberJoiningDetailsDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberJoiningDetail> CreateEntityBuilder<MemberJoiningDetail>()
        {
            return (new MemberJoiningDetailsBuilder()) as IEntityBuilder<MemberJoiningDetail>;
        }

        #endregion

        #region  Methods

        void IMemberJoiningDetailsDataAccess.Add(MemberJoiningDetail MemberJoiningDetail, string MemberId)
        {
            const string SP = "dbo.MemberJoiningDetails_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.AnsiString  , MemberId );
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, MemberJoiningDetail.JobPostingId);
                Database.AddInParameter(cmd, "@OfferedSalary", DbType.AnsiString, MemberJoiningDetail.OfferedSalary);
                Database.AddInParameter(cmd, "@BillableSalary", DbType.AnsiString , MemberJoiningDetail.BillableSalary );
                Database.AddInParameter(cmd, "@BillingRate", DbType.AnsiString , MemberJoiningDetail.BillingRate );
                Database.AddInParameter(cmd, "@Revenue", DbType.AnsiString , MemberJoiningDetail.Revenue );
                Database.AddInParameter(cmd, "@JoiningDate", DbType.DateTime, NullConverter.Convert(MemberJoiningDetail.JoiningDate));
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, MemberJoiningDetail.IsRemoved);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, MemberJoiningDetail.CreatorId);
                Database.AddInParameter(cmd, "@OfferedSalaryPayCycle", DbType.Int32, MemberJoiningDetail.OfferedSalaryPayCycle);
                Database.AddInParameter(cmd, "@OfferedSalaryCurrency", DbType.Int32, MemberJoiningDetail.OfferedSalaryCurrency);
                Database.AddInParameter(cmd, "@BillableSalaryCurrency", DbType.Int32, MemberJoiningDetail.BillableSalaryCurrency);
                Database.AddInParameter(cmd, "@RevenueCurrency", DbType.Int32, MemberJoiningDetail.RevenueCurrency);
                Database.AddInParameter(cmd, "@PSID", DbType.AnsiString, MemberJoiningDetail.PSID);
                Database.AddInParameter(cmd, "@Location", DbType.Int32 , MemberJoiningDetail.Location );
                Database.AddInParameter(cmd, "@OtherLocation", DbType.AnsiString, MemberJoiningDetail.OtherLocation );
                Database.ExecuteNonQuery(cmd);
            }
        }

        MemberJoiningDetail IMemberJoiningDetailsDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberJoiningDetails_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberJoiningDetail>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        MemberJoiningDetail IMemberJoiningDetailsDataAccess.GetByMemberIdAndJobPosstingID(int memberId, int jobpostingid)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }
            if (jobpostingid  < 1)
            {
                throw new ArgumentNullException("jobpostingid");
            }
            const string SP = "dbo.MemberJoiningDetails_GetByMemberIdAndJobPostingID";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobpostingid );

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberJoiningDetail>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }           
        }

        bool IMemberJoiningDetailsDataAccess.DeleteJoiningDetialsById(int HiringDetailsID)
        {
            if (HiringDetailsID < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberJoiningDetails_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, HiringDetailsID);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member Joining Detail which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member Joining detail.");
                        }
                }
            }
        }

        #endregion
    }
}
