﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :   InterviewPanelDataAccess.cs
    Description         :   This page is used Call the Stored Proceedures for Interview Panel.
    Created By          :   Pravin
    Created On          :   20/Nov/2015
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
  
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TPS360.Common.Shared;
using TPS360.Common.Helper;
using System.Collections;
using System.Text;
using TPS360.Common.BusinessEntities;
namespace TPS360.DataAccess
{
    internal sealed class InterviewPanelDataAccess : BaseDataAccess, IInterviewPanelDataAccess
    {
         #region Constructors

        public InterviewPanelDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<InterviewPanel> CreateEntityBuilder<InterviewPanel>()
        {
            return (new InterviewPanelBuilder()) as IEntityBuilder<InterviewPanel>;
        }
      
        #endregion

        #region  Methods

        InterviewPanel IInterviewPanelDataAccess.Add(InterviewPanel InterviewPanel) 
        {
            const string SP = "dbo.InterviewPanel_Create";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP)) 
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@InterviewPanelType_Id", DbType.Int32, InterviewPanel.InterviewPanelType_Id);
                Database.AddInParameter(cmd, "@Interviewer_Id", DbType.Int32, InterviewPanel.Interviewer_Id);
                Database.AddInParameter(cmd, "@Interview_Mode", DbType.String, InterviewPanel.Interview_Mode);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, InterviewPanel.CreatorId);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, InterviewPanel.UpdatorId);
                Database.ExecuteNonQuery(cmd);
                return InterviewPanel;         
            }
        
        }
        InterviewPanel IInterviewPanelDataAccess.AddOtherInterviewer(InterviewPanel InterviewPanel)
        {
            const string SP = "dbo.InterviewPanelOtherInterviewer_Create";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@InterviewPanelType_Id", DbType.Int32, InterviewPanel.InterviewPanelType_Id);
                Database.AddInParameter(cmd, "@OtherInterviewers", DbType.String, InterviewPanel.OtherInterviewers);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, InterviewPanel.CreatorId);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, InterviewPanel.UpdatorId);
                Database.ExecuteNonQuery(cmd);
                return InterviewPanel;
            }

        }
        InterviewPanel IInterviewPanelDataAccess.Update(InterviewPanel InterviewPanel)
        {
            const string SP = "dbo.InterviewPanel_Update";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                
                Database.AddInParameter(cmd, "@InterviewPanel_Id", DbType.Int32, InterviewPanel.InterviewPanel_Id);
                Database.AddInParameter(cmd, "@InterviewPanelType_Id", DbType.Int32, InterviewPanel.InterviewPanelType_Id);
                Database.AddInParameter(cmd, "@Interviewer_Id", DbType.Int32, InterviewPanel.Interviewer_Id);
                Database.AddInParameter(cmd, "@Interview_Mode", DbType.String, InterviewPanel.Interview_Mode);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, InterviewPanel.CreatorId);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, InterviewPanel.UpdatorId);
                Database.ExecuteNonQuery(cmd);
                return InterviewPanel;
                         
            }
        }
        InterviewPanel IInterviewPanelDataAccess.UpdateOtherInterviewer(InterviewPanel InterviewPanel)
        {
            const string SP = "dbo.InterviewPanel_UpdateOtherInterviewer";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {

                Database.AddInParameter(cmd, "@InterviewPanel_Id", DbType.Int32, InterviewPanel.InterviewPanel_Id);
                Database.AddInParameter(cmd, "@InterviewPanelType_Id", DbType.Int32, InterviewPanel.InterviewPanelType_Id);
                Database.AddInParameter(cmd, "@OtherInterviewers", DbType.String, InterviewPanel.OtherInterviewers);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, InterviewPanel.CreatorId);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, InterviewPanel.UpdatorId);
                Database.ExecuteNonQuery(cmd);
                return InterviewPanel;

            }
        }


      
        IList<InterviewPanel> IInterviewPanelDataAccess.GetAll()
        {
            const string SP = "dbo.InterviewPanel_GetAll";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<InterviewPanel>().BuildEntities(reader);
                }
            }

        }

        IList<InterviewPanel> IInterviewPanelDataAccess.GetAll(string SortExpression)
        {
            const string SP = "dbo.InterviewPanel_All";
                     
            string SortColumn = "[Q].[CreateDate]";
            string SortOrder = "desc";
            string[] part = (string.IsNullOrEmpty(SortExpression)) ? null : SortExpression.Split(' ');

            if (part != null && part.Length > 0)
            {
                SortColumn = part[0];

                if (part.Length > 1 && part[1] != null)
                {
                    SortOrder = part[1];
                }
                else
                {
                    SortOrder = "asc";
                }
            }

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                
                Database.AddInParameter(cmd, "@SortColumn", DbType.String, SortColumn);
                Database.AddInParameter(cmd, "@SortOrder", DbType.String, SortOrder);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<InterviewPanel>().BuildEntities(reader);
                }
            }
        }

        bool IInterviewPanelDataAccess.ChkEmailId(int InterviewerId, int panelid)  
        {
            if (panelid < 1)
            {
                throw new ArgumentNullException("Id");
            }
            else
            {
                try
                {
                    const string SP = "dbo.InterviewPanel_ChkEmailId";
                    using (DbCommand cmd = Database.GetStoredProcCommand(SP))
                    {
                        Database.AddInParameter(cmd, "@InterviewerId", DbType.Int32, InterviewerId);
                        Database.AddInParameter(cmd, "@panelid", DbType.Int32, panelid);
                        Database.ExecuteNonQuery(cmd);
                        return true;
                     }
                }
                catch 
                {
                    return false;
                }
            }

        }

        bool IInterviewPanelDataAccess.ClientChkEmailId(int InterviewerId, int panelid)
        {
            if (panelid < 1)
            {
                throw new ArgumentNullException("panelid");
            }
            else
            {
                try
                {
                    const string SP = "dbo.InterviewPanel_ClientChkEmailId";
                    using (DbCommand cmd = Database.GetStoredProcCommand(SP))
                    {

                        Database.AddInParameter(cmd, "@InterviewerId", DbType.Int32, InterviewerId);
                        Database.AddInParameter(cmd, "@panelid", DbType.Int32, panelid);
                        Database.ExecuteNonQuery(cmd);
                        return true;
                    }
                }
                catch 
                {
                    return false;
                }
            }

        }

        IList<InterviewPanel> IInterviewPanelDataAccess.OtherChkEmailId(int panelid)
        {
            const string SP = "dbo.InterviewPanel_OtherChkEmailId";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {

               Database.AddInParameter(cmd, "@panelid", DbType.Int32, panelid);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<InterviewPanel>().BuildEntities(reader);
                }
            }

        }
        IList<InterviewPanel> IInterviewPanelDataAccess.ScheduleOtherChkEmailId(int RequisitionId)
        {
            const string SP = "dbo.InterviewSchedule_OtherChkEmailId";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {

                Database.AddInParameter(cmd, "@RequisitionId", DbType.Int32, RequisitionId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<InterviewPanel>().BuildEntities(reader);
                }
            }

        }
 
        bool IInterviewPanelDataAccess.DeleteById(int Id)
        {
            if (Id < 1)
            {
                throw new ArgumentNullException("Id");
            }

            const string SP = "dbo.InterviewPanel_DeleteById";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, Id);
                Database.ExecuteNonQuery(cmd);
                return true;
            }

        }

        bool IInterviewPanelDataAccess.UpdateDeleteById(int InterviewPanelids)
        {
            if (InterviewPanelids < 1)
            {
                throw new ArgumentNullException("Id");
            }

            const string SP = "dbo.InterviewPanel_UpdateDeleteById";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@InterviewPanelids", DbType.Int32, InterviewPanelids);
                Database.ExecuteNonQuery(cmd);
                return true;
            }          
        
        }
        
        InterviewPanel IInterviewPanelDataAccess.GetById(int Id)
        {
            if (Id < 1)
            {
                throw new ArgumentNullException("Id");
            }

            const string SP = "dbo.InterviewPanel_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, Id);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<InterviewPanel>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }

        }      
       
        //Check this Functionality Later 0123456789
        PagedResponse<InterviewPanel> IInterviewPanelDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.InterviewerFeedback_GetPaged";
            
            string whereClause = string.Empty;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<InterviewPanel> response = new PagedResponse<InterviewPanel>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<InterviewPanel>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }
        #endregion
    }
}
