﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberManagerBuilder : IEntityBuilder<MemberManager>
    {
        IList<MemberManager> IEntityBuilder<MemberManager>.BuildEntities(IDataReader reader)
        {
            List<MemberManager> memberManagers = new List<MemberManager>();

            while (reader.Read())
            {
                memberManagers.Add(((IEntityBuilder<MemberManager>)this).BuildEntity(reader));
            }

            return (memberManagers.Count > 0) ? memberManagers : null;
        }

        MemberManager IEntityBuilder<MemberManager>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_MEMBERID = 1;
            const int FLD_MANAGERID = 2;
            const int FLD_ISPRIMARYMANAGER = 3;
            const int FLD_UNASSIGNEDBY = 4;
            const int FLD_UNASSIGNDATE = 5;
            const int FLD_ISREMOVED = 6;
            const int FLD_CREATORID = 7;
            const int FLD_UPDATORID = 8;
            const int FLD_CREATEDATE = 9;
            const int FLD_UPDATEDATE = 10;

            MemberManager memberManager = new MemberManager();

            memberManager.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberManager.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberManager.ManagerId = reader.IsDBNull(FLD_MANAGERID) ? 0 : reader.GetInt32(FLD_MANAGERID);
            memberManager.IsPrimaryManager = reader.IsDBNull(FLD_ISPRIMARYMANAGER) ? false : reader.GetBoolean(FLD_ISPRIMARYMANAGER);
            memberManager.UnAssignedBy = reader.IsDBNull(FLD_UNASSIGNEDBY) ? 0 : reader.GetInt32(FLD_UNASSIGNEDBY);
            memberManager.UnAssignDate = reader.IsDBNull(FLD_UNASSIGNDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UNASSIGNDATE);
            memberManager.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberManager.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberManager.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberManager.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberManager.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return memberManager;
        }
    }
}