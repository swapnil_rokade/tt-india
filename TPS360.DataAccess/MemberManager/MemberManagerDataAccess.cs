﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;
using System.Collections;

namespace TPS360.DataAccess
{
    internal sealed class MemberManagerDataAccess : BaseDataAccess, IMemberManagerDataAccess
    {
        #region Constructors

        public MemberManagerDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberManager> CreateEntityBuilder<MemberManager>()
        {
            return (new MemberManagerBuilder()) as IEntityBuilder<MemberManager>;
        }

        #endregion

        #region  Methods

        MemberManager IMemberManagerDataAccess.Add(MemberManager memberManager)
        {
            const string SP = "dbo.MemberManager_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberManager.MemberId);
                Database.AddInParameter(cmd, "@ManagerId", DbType.Int32, memberManager.ManagerId);
                Database.AddInParameter(cmd, "@IsPrimaryManager", DbType.Boolean, memberManager.IsPrimaryManager);
                Database.AddInParameter(cmd, "@UnAssignedBy", DbType.Int32, memberManager.UnAssignedBy);
                Database.AddInParameter(cmd, "@UnAssignDate", DbType.DateTime, NullConverter.Convert(memberManager.UnAssignDate));
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberManager.IsRemoved);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberManager.CreatorId);                

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberManager = CreateEntityBuilder<MemberManager>().BuildEntity(reader);
                    }
                    else
                    {
                        memberManager = null;
                    }
                }

                if (memberManager == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberManager already exists. Please specify another memberManager.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this memberManager.");
                            }
                    }
                }

                return memberManager;
            }
        }

        MemberManager IMemberManagerDataAccess.Update(MemberManager memberManager)
        {
            const string SP = "dbo.MemberManager_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberManager.Id);
                Database.AddInParameter(cmd, "@IsPrimaryManager", DbType.Boolean, memberManager.IsPrimaryManager);
                Database.AddInParameter(cmd, "@UnAssignedBy", DbType.Int32, memberManager.UnAssignedBy);
                Database.AddInParameter(cmd, "@UnAssignDate", DbType.DateTime, NullConverter.Convert(memberManager.UnAssignDate));
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberManager.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberManager.UpdatorId);                

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberManager = CreateEntityBuilder<MemberManager>().BuildEntity(reader);
                    }
                    else
                    {
                        memberManager = null;
                    }
                }

                if (memberManager == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberManager already exists. Please specify another memberManager.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this memberManager.");
                            }
                    }
                }

                return memberManager;
            }
        }

        MemberManager IMemberManagerDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberManager_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberManager>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        MemberManager IMemberManagerDataAccess.GetByMemberIdAndManagerId(int memberId, int managerId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            if (managerId < 1)
            {
                throw new ArgumentNullException("managerId");
            }

            const string SP = "dbo.MemberManager_GetMemberManagerByMemberIdAndManagerId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                Database.AddInParameter(cmd, "@ManagerId", DbType.Int32, managerId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberManager>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        MemberManager IMemberManagerDataAccess.GetPrimaryByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberManager_GetPrimaryByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberManager>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberManager> IMemberManagerDataAccess.GetAllByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberManager_GetAllMemberManagerByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberManager>().BuildEntities(reader);
                }
            }
        }

        IList<MemberManager> IMemberManagerDataAccess.GetAll()
        {
            const string SP = "dbo.MemberManager_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberManager>().BuildEntities(reader);
                }
            }
        }

        bool IMemberManagerDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberManager_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a memberManager which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this memberManager.");
                        }
                }
            }
        }

        bool IMemberManagerDataAccess.DeleteByMemberIdAndManagerId(int memberId, int managerId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            if (managerId < 1)
            {
                throw new ArgumentNullException("managerId");
            }

            const string SP = "dbo.MemberManager_DeleteByMemberIdAndManagerId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                Database.AddInParameter(cmd, "@ManagerId", DbType.Int32, managerId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a memberManager which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this memberManager.");
                        }
                }
            }
        }

        bool IMemberManagerDataAccess.DeleteAllMemberManagerByMemberId(int memberId )
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberManager_DeleteAllMemberManagerByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                 
                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a memberManager which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this memberManager.");
                        }
                }
            }
        }

        ArrayList IMemberManagerDataAccess.GetListByMemberId(int memberId)
        {
            if (memberId < 0)
            {
                throw new ArgumentException("memberId");
            }

            const string SP = "dbo.MemberManager_GetAllMemberManagerListByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList memberManagerList = new ArrayList();

                    while (reader.Read())
                    {
                        memberManagerList.Add(new { Id = reader.GetInt32(0), Name = reader.GetString(1) });
                    }

                    return memberManagerList;
                }
            }
        }
        PagedResponse<Member> IMemberManagerDataAccess.GetPagedByMemberId(int MemberID, PagedRequest request)
        {
            if (MemberID < 1)
            {
                throw new ArgumentNullException("MemberID");
            }

            string whereClause = string.Empty;
            whereClause = "MemberId = " + MemberID;
            if (request.SortColumn == null) request.SortColumn = "[M].[FirstName]";
            else if (request.SortColumn == string.Empty) request.SortColumn = "[M].[FirstName]";
            else request.SortColumn = "[MD].[" + request.SortColumn + "]";

            if (request.SortOrder == null) request.SortOrder = "ASC";
            else if (request.SortOrder == string.Empty) request.SortOrder = "ASC";
            object[] paramValues = new object[] {request .PageIndex,
													request .RowPerPage   ,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request .SortColumn  ),
                                                    StringHelper.Convert(request.SortOrder),
												};
            const string SP = "dbo.MemberManager_GetPagedMemberManagerByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<Member> response = new PagedResponse<Member>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {

                    response.Response = ((new MemberBuilder()) as IEntityBuilder<Member>).BuildEntities(reader); //CreateEntityBuilder<Member>().BuildEntities(reader); //CreateEntityBuilder<MemberDocument>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        string IMemberManagerDataAccess.GetPrimaryManagerNameByMemberId(int memberId)
        {


            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }
            const string SP = "dbo.MemberManager_GetPrimaryManagerNameByMemberId";

              using (DbCommand cmd = Database.GetStoredProcCommand(SP))
              {
                  Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                  using (IDataReader reader = Database.ExecuteReader(cmd))
                  {
                      while (reader.Read())
                      {
                          return reader.IsDBNull(0) ? string.Empty : reader.GetString(0);
                      }
                      return string.Empty;
                  }
              }

        }

        
        #endregion
    }
}