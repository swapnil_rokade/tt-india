﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class GenericLookupBuilder : IEntityBuilder<GenericLookup>
    {
        IList<GenericLookup> IEntityBuilder<GenericLookup>.BuildEntities(IDataReader reader)
        {
            List<GenericLookup> genericLookups = new List<GenericLookup>();

            while (reader.Read())
            {
                genericLookups.Add(((IEntityBuilder<GenericLookup>)this).BuildEntity(reader));
            }

            return (genericLookups.Count > 0) ? genericLookups : null;
        }

        GenericLookup IEntityBuilder<GenericLookup>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_NAME = 1;
            const int FLD_DESCRIPTION = 2;
            const int FLD_TYPE = 3;
            const int FLD_SORTORDER = 4;
            const int FLD_CREATORID = 5;
            const int FLD_UPDATORID = 6;
            const int FLD_CREATEDATE = 7;
            const int FLD_UPDATEDATE = 8;

            GenericLookup genericLookup = new GenericLookup();

            genericLookup.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            genericLookup.Name = reader.IsDBNull(FLD_NAME) ? string.Empty : reader.GetString(FLD_NAME);
            genericLookup.Description = reader.IsDBNull(FLD_DESCRIPTION) ? string.Empty : reader.GetString(FLD_DESCRIPTION);
            genericLookup.Type = reader.IsDBNull(FLD_TYPE) ? 0 : reader.GetInt32(FLD_TYPE);
            genericLookup.SortOrder = reader.IsDBNull(FLD_SORTORDER) ? 0 : reader.GetInt32(FLD_SORTORDER);
            genericLookup.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            genericLookup.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            genericLookup.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            genericLookup.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return genericLookup;
        }
    }
}