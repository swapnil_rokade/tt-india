﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class MemberHiringProcessDataAccess : BaseDataAccess, IMemberHiringProcessDataAccess
    {
        #region Constructors

        public MemberHiringProcessDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberHiringProcess> CreateEntityBuilder<MemberHiringProcess>()
        {
            return (new MemberHiringProcessBuilder()) as IEntityBuilder<MemberHiringProcess>;
        }

        #endregion

        #region  Methods

        MemberHiringProcess IMemberHiringProcessDataAccess.Add(MemberHiringProcess memberHiringProcess)
        {
            const string SP = "dbo.MemberHiringProcess_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberHiringProcess.MemberId);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, memberHiringProcess.JobPostingId);
                Database.AddInParameter(cmd, "@InterviewLevelId", DbType.Int32, memberHiringProcess.InterviewLevelId );
                Database.AddInParameter(cmd, "@InterviewerId", DbType.Int32, memberHiringProcess.InterviewerId );
                Database.AddInParameter(cmd, "@InterviewerNote", DbType.AnsiString, StringHelper.Convert(memberHiringProcess.InterviewerNote));
                Database.AddInParameter(cmd, "@InterviewerDate", DbType.DateTime, NullConverter.Convert(memberHiringProcess.InterviewerDate));
                Database.AddInParameter(cmd, "@InterviewTotal", DbType.Decimal, memberHiringProcess.InterviewTotal);
                Database.AddInParameter(cmd, "@InterviewSubmittedStages", DbType.Int32, memberHiringProcess.InterviewSubmittedStages);
                Database.AddInParameter(cmd, "@ApplicantMatchingPercent", DbType.Int32, memberHiringProcess.ApplicantMatchingPercent);
                Database.AddInParameter(cmd, "@InterviewRemarks",DbType.AnsiString,memberHiringProcess.InterviewRemarks);


                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberHiringProcess = CreateEntityBuilder<MemberHiringProcess>().BuildEntity(reader);
                    }
                    else
                    {
                        memberHiringProcess = null;
                    }
                }

                if (memberHiringProcess == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member hiring process already exists. Please specify another member hiring process.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this member hiring process.");
                            }
                    }
                }

                return memberHiringProcess;
            }
        }

        MemberHiringProcess IMemberHiringProcessDataAccess.Update(MemberHiringProcess memberHiringProcess)
        {
            const string SP = "dbo.MemberHiringProcess_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberHiringProcess.Id);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberHiringProcess.MemberId);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, memberHiringProcess.JobPostingId);
                Database.AddInParameter(cmd, "@InterviewLevelId", DbType.Int32, memberHiringProcess.InterviewLevelId );
                Database.AddInParameter(cmd, "@InterviewerId", DbType.Int32, memberHiringProcess.InterviewerId);
                Database.AddInParameter(cmd, "@InterviewerNote", DbType.AnsiString, StringHelper.Convert(memberHiringProcess.InterviewerNote));
                Database.AddInParameter(cmd, "@InterviewerDate", DbType.DateTime, NullConverter.Convert(memberHiringProcess.InterviewerDate));
                Database.AddInParameter(cmd, "@InterviewTotal", DbType.Decimal, memberHiringProcess.InterviewTotal);
                Database.AddInParameter(cmd, "@InterviewSubmittedStages", DbType.Int32, memberHiringProcess.InterviewSubmittedStages);
                Database.AddInParameter(cmd, "@ApplicantMatchingPercent", DbType.Int32, memberHiringProcess.ApplicantMatchingPercent);
                Database.AddInParameter(cmd, "@InterviewRemarks", DbType.AnsiString, memberHiringProcess.InterviewRemarks);


                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberHiringProcess = CreateEntityBuilder<MemberHiringProcess>().BuildEntity(reader);
                    }
                    else
                    {
                        memberHiringProcess = null;
                    }
                }

                if (memberHiringProcess == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberHiringProcess already exists. Please specify another member hiring process.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this member hiring process.");
                            }
                    }
                }

                return memberHiringProcess;
            }
        }

        MemberHiringProcess IMemberHiringProcessDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberHiringProcess_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberHiringProcess>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberHiringProcess> IMemberHiringProcessDataAccess.GetByMemberIdJobPostingId(int memberId, int jobPostingId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }
            if (jobPostingId < 1)
            {
                throw new ArgumentNullException("jobPostingId");
            }

            const string SP = "dbo.MemberHiringProcess_GetByMemberIdAndJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberHiringProcess>()).BuildEntities (reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberHiringProcess> IMemberHiringProcessDataAccess.GetAll()
        {
            const string SP = "dbo.MemberHiringProcess_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberHiringProcess>().BuildEntities(reader);
                }
            }
        }

        bool IMemberHiringProcessDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberHiringProcess_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member hiring process which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member hiring process.");
                        }
                }
            }
        }

        MemberHiringProcess IMemberHiringProcessDataAccess.GetByMemberIdJobPostingIdAndInterviewLevelId(int memberId, int jobPostingId, int interviewlevelid)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }
            if (jobPostingId < 1)
            {
                throw new ArgumentNullException("jobPostingId");
            }

            if (interviewlevelid < 1)
            {
                throw new ArgumentNullException("InterviewLevelId");
            }

            const string SP = "dbo.MemberHiringProcess_GetByMemberIdJobPostingIdAndInterviewLevelID";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingId);
                Database.AddInParameter(cmd, "@InterviewLevelId", DbType.Int32, interviewlevelid);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberHiringProcess>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        #endregion
    }
}