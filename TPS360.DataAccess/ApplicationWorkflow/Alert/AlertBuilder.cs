﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class AlertBuilder : IEntityBuilder<Alert>
    {
        IList<Alert> IEntityBuilder<Alert>.BuildEntities(IDataReader reader)
        {
            List<Alert> alerts = new List<Alert>();

            while (reader.Read())
            {
                alerts.Add(((IEntityBuilder<Alert>)this).BuildEntity(reader));
            }

            return (alerts.Count > 0) ? alerts : null;
        }

        Alert IEntityBuilder<Alert>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_TITLE = 1;
            const int FLD_DESCRIPTION = 2;
            const int FLD_CATEGORYLOOKUPID = 3;
            const int FLD_WORKFLOWTITLEID = 4;
            const int FLD_ISREMOVED = 5;
            const int FLD_CREATORID = 6;
            const int FLD_UPDATORID = 7;
            const int FLD_CREATEDATE = 8;
            const int FLD_UPDATEDATE = 9;

            Alert alert = new Alert();

            alert.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            alert.Title = reader.IsDBNull(FLD_TITLE) ? string.Empty : reader.GetString(FLD_TITLE);
            alert.Description = reader.IsDBNull(FLD_DESCRIPTION) ? string.Empty : reader.GetString(FLD_DESCRIPTION);
            alert.CategoryLookupId = reader.IsDBNull(FLD_CATEGORYLOOKUPID) ? 0 : reader.GetInt32(FLD_CATEGORYLOOKUPID);
            alert.WorkflowTitleId = reader.IsDBNull(FLD_WORKFLOWTITLEID) ? 0 : reader.GetInt32(FLD_WORKFLOWTITLEID);
            alert.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? 0 : reader.GetInt32(FLD_ISREMOVED);
            alert.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            alert.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            alert.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            alert.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return alert;
        }
    }
}