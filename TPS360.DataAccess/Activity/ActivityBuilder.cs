﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class ActivityBuilder : IEntityBuilder<Activity>
    {//Building
        IList<Activity> IEntityBuilder<Activity>.BuildEntities(IDataReader reader)
        {
            List<Activity> activitys = new List<Activity>();

            while (reader.Read())
            {
                activitys.Add(((IEntityBuilder<Activity>)this).BuildEntity(reader));
            }

            return (activitys.Count > 0) ? activitys : null;
        }

        Activity IEntityBuilder<Activity>.BuildEntity(IDataReader reader)
		{
			const int FLD_ACTIVITYID = 0;
			const int FLD_STARTDATETIMEUTC = 1;
			const int FLD_DURATION = 2;
			const int FLD_SUBJECT = 3;
			const int FLD_ACTIVITYDESCRIPTION = 4;
			const int FLD_ALLDAYEVENT = 5;
			const int FLD_LOCATION = 6;
			const int FLD_STATUS = 7;
			const int FLD_ENABLEREMINDER = 8;
			const int FLD_REMINDERINTERVAL = 9;
			const int FLD_SHOWTIMEAS = 10;
			const int FLD_IMPORTANCE = 11;
			//const int FLD__TS = 12;
			const int FLD_RECURRENCEID = 12;
			const int FLD_ORIGINALSTARTDATETIMEUTC = 13;
			const int FLD_VARIANCEID = 14;
            const int FLD_UPDATEDATE = 15;
            Activity activity = new Activity();

			activity.ActivityID = reader.IsDBNull(FLD_ACTIVITYID) ? 0 : reader.GetInt32(FLD_ACTIVITYID);
			activity.StartDateTimeUtc = reader.IsDBNull(FLD_STARTDATETIMEUTC) ? DateTime.MinValue : reader.GetDateTime(FLD_STARTDATETIMEUTC);
			activity.Duration = reader.IsDBNull(FLD_DURATION) ? 0 : reader.GetInt32(FLD_DURATION);
			activity.Subject = reader.IsDBNull(FLD_SUBJECT) ? string.Empty : reader.GetString(FLD_SUBJECT);
			activity.ActivityDescription = reader.IsDBNull(FLD_ACTIVITYDESCRIPTION) ? string.Empty : reader.GetString(FLD_ACTIVITYDESCRIPTION);
			activity.AllDayEvent = reader.IsDBNull(FLD_ALLDAYEVENT) ? false : reader.GetBoolean(FLD_ALLDAYEVENT);
			activity.Location = reader.IsDBNull(FLD_LOCATION) ? string.Empty : reader.GetString(FLD_LOCATION);
			activity.Status = reader.IsDBNull(FLD_STATUS) ? 0 : reader.GetInt32(FLD_STATUS);
			activity.EnableReminder = reader.IsDBNull(FLD_ENABLEREMINDER) ? false : reader.GetBoolean(FLD_ENABLEREMINDER);
            activity.ReminderInterval = reader.IsDBNull(FLD_REMINDERINTERVAL) ? 0 : reader.GetInt32(FLD_REMINDERINTERVAL);
			activity.ShowTimeAs = reader.IsDBNull(FLD_SHOWTIMEAS) ? 0 : reader.GetInt32(FLD_SHOWTIMEAS);
			activity.Importance = reader.IsDBNull(FLD_IMPORTANCE) ? 0 : reader.GetInt32(FLD_IMPORTANCE);

			//activity._ts = reader.IsDBNull(FLD__TS) ? 0 : reader.GetByte[](FLD__TS);

			activity.RecurrenceID = reader.IsDBNull(FLD_RECURRENCEID) ? 0 : reader.GetInt32(FLD_RECURRENCEID);
			activity.OriginalStartDateTimeUtc = reader.IsDBNull(FLD_ORIGINALSTARTDATETIMEUTC) ? DateTime.MinValue : reader.GetDateTime(FLD_ORIGINALSTARTDATETIMEUTC);
			activity.VarianceID = reader.IsDBNull(FLD_VARIANCEID) ? Guid.Empty : reader.GetGuid(FLD_VARIANCEID);
            if (reader.FieldCount > 15)
                activity.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            return activity;
		}
    }
}