﻿using System;
using System.Data;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System.Collections.Generic;

namespace TPS360.DataAccess
{
    internal sealed class ActivityDataAccess : BaseDataAccess, IActivityDataAccess
    {
        #region Constructors

        public ActivityDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<Activity> CreateEntityBuilder<Activity>()
        {
            return (new ActivityBuilder()) as IEntityBuilder<Activity>;
        }

        #endregion

        #region  Methods

        Activity IActivityDataAccess.Add(Activity activity)
        {
            const string SP = "dbo.Activity_Add";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                //AddOutputParameter(cmd);             

                Database.AddInParameter(cmd, "@AllDayEvent", DbType.Boolean, activity.AllDayEvent);
                Database.AddInParameter(cmd, "@ActivityDescription", DbType.String, activity.ActivityDescription);
                Database.AddInParameter(cmd, "@Duration", DbType.Int32, activity.Duration);
                Database.AddInParameter(cmd, "@Location", DbType.String, activity.Location);
                Database.AddInParameter(cmd, "@StartDateTimeUtc", DbType.DateTime, activity.StartDateTimeUtc);
                Database.AddInParameter(cmd, "@Subject", DbType.String, activity.Subject);
                Database.AddInParameter(cmd, "@EnableReminder", DbType.Boolean, activity.EnableReminder);
                Database.AddInParameter(cmd, "@ReminderInterval", DbType.Int32, activity.ReminderInterval);
                Database.AddInParameter(cmd, "@ShowTimeAs", DbType.Int32, activity.ShowTimeAs);
                Database.AddInParameter(cmd, "@Importance", DbType.Int32, activity.Importance);
                Database.AddInParameter(cmd, "@Status", DbType.Int32, activity.Status);
                Database.AddInParameter(cmd, "@RecurrenceKey", DbType.Int32, activity.RecurrenceID);
                Database.AddInParameter(cmd, "@ResourceName", DbType.Int32, activity.ResourceName);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        activity.ActivityID = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                    }
                    else if (reader.NextResult() && reader.Read())
                    {
                        activity.ActivityID = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                    }
                }

                if (activity == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Activity already exists. Please specify another activity.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this activity.");
                            }
                    }
                }

                return activity;
            }
        }

        Activity IActivityDataAccess.AddForInterview(Activity activity)
        {
            const string SP = "dbo.Activity_AddForInterview";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                //AddOutputParameter(cmd);             

                Database.AddInParameter(cmd, "@AllDayEvent", DbType.Boolean, activity.AllDayEvent);
                Database.AddInParameter(cmd, "@ActivityDescription", DbType.String, activity.ActivityDescription);
                Database.AddInParameter(cmd, "@Duration", DbType.Int32, activity.Duration);
                Database.AddInParameter(cmd, "@Location", DbType.String, activity.Location);
                Database.AddInParameter(cmd, "@StartDateTimeUtc", DbType.DateTime, activity.StartDateTimeUtc);
                Database.AddInParameter(cmd, "@Subject", DbType.String, activity.Subject);
                Database.AddInParameter(cmd, "@EnableReminder", DbType.Boolean, activity.EnableReminder);
                Database.AddInParameter(cmd, "@ReminderInterval", DbType.Int32, activity.ReminderInterval);
                Database.AddInParameter(cmd, "@ShowTimeAs", DbType.Int32, activity.ShowTimeAs);
                Database.AddInParameter(cmd, "@Importance", DbType.Int32, activity.Importance);
                Database.AddInParameter(cmd, "@Status", DbType.Int32, activity.Status);
                Database.AddInParameter(cmd, "@RecurrenceKey", DbType.Int32, activity.RecurrenceID);
                Database.AddInParameter(cmd, "@ResourceName", DbType.Int32, activity.ResourceName);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, activity.IsRemoved);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        activity.ActivityID = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                    }
                    else if (reader.NextResult() && reader.Read())
                    {
                        activity.ActivityID = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                    }
                }

                if (activity == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Activity already exists. Please specify another activity.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this activity.");
                            }
                    }
                }

                return activity;
            }
        }

        Activity IActivityDataAccess.Update(Activity activity)
        {
            const string SP = "dbo.Activity_Upd";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@DataKey", DbType.Int32, activity.ActivityID);
                Database.AddInParameter(cmd, "@AllDayEvent", DbType.Boolean, activity.AllDayEvent);
                Database.AddInParameter(cmd, "@ActivityDescription", DbType.String, activity.ActivityDescription);
                Database.AddInParameter(cmd, "@Duration", DbType.Int32, activity.Duration);
                Database.AddInParameter(cmd, "@Location", DbType.String, activity.Location);
                Database.AddInParameter(cmd, "@StartDateTimeUtc", DbType.DateTime, activity.StartDateTimeUtc);
                Database.AddInParameter(cmd, "@Subject", DbType.String, activity.Subject);
                Database.AddInParameter(cmd, "@EnableReminder", DbType.Boolean, activity.EnableReminder);
                Database.AddInParameter(cmd, "@ReminderInterval", DbType.Int32, activity.ReminderInterval);
                Database.AddInParameter(cmd, "@ShowTimeAs", DbType.Int32, activity.ShowTimeAs);
                Database.AddInParameter(cmd, "@Importance", DbType.Int32, activity.Importance);
                Database.AddInParameter(cmd, "@Status", DbType.Int32, activity.Status);
                Database.AddInParameter(cmd, "@RecurrenceKey", DbType.Int32, activity.RecurrenceID);                
                Database.AddInParameter(cmd, "@VarianceKey", DbType.Guid, activity.VarianceID);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return CreateEntityBuilder<Activity>().BuildEntity(reader);
                    }
                    else
                    {
                       return null;
                    }
                }

                if (activity == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Activity already exists. Please specify another activity.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this activity.");
                            }
                    }
                }

                return activity;
            }
        }

        Activity IActivityDataAccess.UpdateForInterview(Activity activity)
        {
            const string SP = "dbo.Activity_UpdForInterview";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@DataKey", DbType.Int32, activity.ActivityID);
                Database.AddInParameter(cmd, "@AllDayEvent", DbType.Boolean, activity.AllDayEvent);
                Database.AddInParameter(cmd, "@ActivityDescription", DbType.String, activity.ActivityDescription);
                Database.AddInParameter(cmd, "@Duration", DbType.Int32, activity.Duration);
                Database.AddInParameter(cmd, "@Location", DbType.String, activity.Location);
                Database.AddInParameter(cmd, "@StartDateTimeUtc", DbType.DateTime, activity.StartDateTimeUtc);
                Database.AddInParameter(cmd, "@Subject", DbType.String, activity.Subject);
                Database.AddInParameter(cmd, "@EnableReminder", DbType.Boolean, activity.EnableReminder);
                Database.AddInParameter(cmd, "@ReminderInterval", DbType.Int32, activity.ReminderInterval);
                Database.AddInParameter(cmd, "@ShowTimeAs", DbType.Int32, activity.ShowTimeAs);
                Database.AddInParameter(cmd, "@Importance", DbType.Int32, activity.Importance);
                Database.AddInParameter(cmd, "@Status", DbType.Int32, activity.Status);
                Database.AddInParameter(cmd, "@RecurrenceKey", DbType.Int32, activity.RecurrenceID);
                Database.AddInParameter(cmd, "@VarianceKey", DbType.Guid, activity.VarianceID);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, activity.IsRemoved);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return CreateEntityBuilder<Activity>().BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }

                if (activity == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Activity already exists. Please specify another activity.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this activity.");
                            }
                    }
                }

                return activity;
            }
        }


        Activity IActivityDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.Activity_GetByActivityID";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@ActivityID", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<Activity>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        //IList<Activity> IActivityDataAccess.GetAll()
        //{
        //    const string SP = "dbo.Activity_GetAll";

        //    using (DbCommand cmd = Database.GetStoredProcCommand(SP))
        //    {
        //        using (IDataReader reader = Database.ExecuteReader(cmd))
        //        {
        //            return CreateEntityBuilder<Activity>().BuildEntities(reader);
        //        }
        //    }
        //}

        bool IActivityDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.Activity_DeleteByActivityID";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@ActivityID", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a activity which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this activity.");
                        }
                }
            }
        }

        IList<Activity> IActivityDataAccess.GetAllActivityByResourceId(int resourceID)
        {
            if (resourceID < 1)
            {
                throw new ArgumentNullException("resourceID");
            }

            const string SP = "dbo.Activity_GetAllActivityByResourceID";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@ResourceID", DbType.Int32, resourceID);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<Activity>().BuildEntities(reader);
                }
            }
        }
        IList<Activity> IActivityDataAccess.GetAllActivityByResourceIdAndDate(int resourceID,string Date)
        {
            if (resourceID < 1)
            {
                throw new ArgumentNullException("resourceID");
            }

            const string SP = "dbo.Activity_GetAllActivityByResourceIDAndDate";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@ResourceID", DbType.Int32, resourceID);
                Database.AddInParameter(cmd, "@Date", DbType.String , Date );

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<Activity>().BuildEntities(reader);
                }
            }
        }

        #endregion
    }
}