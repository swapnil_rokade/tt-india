﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName:OccupationalGroupDataAccess.cs
    Description: This page is used for Countries  functionality
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            July-7-2009           Gopala Swamy J         Misc:Added 1) GetStateIdByStateCode() 2) GetOccupationalGroupIdByOccupationalGroupCode() to assist 
                                                                Resume parser team
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/


using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class OccupationalGroupDataAccess : BaseDataAccess, IOccupationalGroupDataAccess
    {
        #region Constructors

        public OccupationalGroupDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<OccupationalGroup> CreateEntityBuilder<OccupationalGroup>()
        {
            return (new OccupationalGroupBuilder()) as IEntityBuilder<OccupationalGroup>;
        }

        #endregion

        #region  Methods

        IList<OccupationalGroup> IOccupationalGroupDataAccess.GetAllBySearch(string keyword, int count)
        {
            const string SP = "OccupationalGroup_GetPaged";
            string whereClause = string.Empty;

            whereClause = "[D].GroupTitle like '" + keyword + "%'";
            object[] paramValues = new object[] {	0,
													count ,
													StringHelper.Convert(whereClause),
													StringHelper.Convert("[D].[GroupTitle]"),
                                                    StringHelper.Convert("Asc")
												};
            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<OccupationalGroup>().BuildEntities(reader);
                }
            }
        }
        IList<OccupationalGroup> IOccupationalGroupDataAccess.GetAll()
        {
            const string SP = "OuccupationalGroup_GetAll";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<OccupationalGroup>().BuildEntities(reader);
                }
            }
        }












        #endregion
    }
}