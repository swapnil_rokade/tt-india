﻿using System.Data;
using System.Collections.Generic;
using System;
using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class DepartmentBuilder : IEntityBuilder<Department>
    {
        IList<Department> IEntityBuilder<Department>.BuildEntities(IDataReader reader)
        {
            List<Department> Departments = new List<Department>();

            while (reader.Read())
            {
                Departments.Add(((IEntityBuilder<Department>)this).BuildEntity(reader));
            }

            return (Departments.Count > 0) ? Departments : null;
        }

        Department IEntityBuilder<Department>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_NAME = 1;
   

            Department Department = new Department();

            Department.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            Department.Name = reader.IsDBNull(FLD_NAME) ? string.Empty : reader.GetString(FLD_NAME);
           
            return Department;
        }
    }
}