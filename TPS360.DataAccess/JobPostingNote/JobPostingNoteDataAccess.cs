﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System.Collections;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class JobPostingNoteDataAccess : BaseDataAccess, IJobPostingNoteDataAccess
    {
        #region Constructors

        public JobPostingNoteDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<JobPostingNote> CreateEntityBuilder<JobPostingNote>()
        {
            return (new JobPostingNoteBuilder()) as IEntityBuilder<JobPostingNote>;
        }

      

        #endregion

        #region  Methods

        JobPostingNote IJobPostingNoteDataAccess.Add(JobPostingNote jobPostingNote)
        {
            const string SP = "dbo.JobPostingNote_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingNote.JobPostingId);
                Database.AddInParameter(cmd, "@CommonNoteId", DbType.Int32, jobPostingNote.CommonNoteId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        jobPostingNote = CreateEntityBuilder<JobPostingNote>().BuildEntity(reader);
                    }
                    else
                    {
                        jobPostingNote = null;
                    }
                }

                if (jobPostingNote == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Job posting note already exists. Please specify another company note.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this company note.");
                            }
                    }
                }

                return jobPostingNote;
            }
        }

        JobPostingNote IJobPostingNoteDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.JobPostingNote_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<JobPostingNote>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<JobPostingNote> IJobPostingNoteDataAccess.GetAll()
        {
            const string SP = "dbo.JobPostingNote_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<JobPostingNote>().BuildEntities(reader);
                }
            }
        }

        IList<JobPostingNote> IJobPostingNoteDataAccess.GetAllByJobPostingId(int jobPostingId)
        {
            const string SP = "dbo.JobPostingNote_GetAllJobPostingNoteByJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<JobPostingNote>().BuildEntities(reader);
                }
            }
        }


        bool IJobPostingNoteDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.JobPostingNote_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a job posting note which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this company note.");
                        }
                }
            }
        }

        bool IJobPostingNoteDataAccess.DeleteByJobPostingId(int jobPostingId)
        {
            if (jobPostingId < 1)
            {
                throw new ArgumentNullException("jobPostingId");
            }

            const string SP = "dbo.JobPostingNote_DeleteJobPostingNoteByJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a job posting note which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this company note.");
                        }
                }
            }
        }

        #endregion
    }


    internal sealed class ReqNotesDataAccess : BaseDataAccess, IReqNotesDataAccess
    {

        public ReqNotesDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<RequisitionNotesEntry> CreateEntityBuilder<RequisitionNotesEntry>()
        {
            return (new ReqNotesBuilder()) as IEntityBuilder<RequisitionNotesEntry>;
        }

        PagedResponse<RequisitionNotesEntry> IReqNotesDataAccess.GetAllReqNotesById(PagedRequest pagereq)
        {
            const string SP = "dbo.GetJobPostingNotesbyId_GetPaged";
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            if (pagereq.Conditions.Count > 0)
            {
                string column = null;
                string value = null;
              
                foreach (DictionaryEntry entry in pagereq.Conditions)
                {

                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        if (column.ToLower ()== "JobPostingId".ToLower ())
                        {
                            sb.Append("[J].[JobpostingId]=" + value.ToString());
                        }

                    }
                }
            }
                object[] paramValues = new object[] {	pagereq.PageIndex,
													pagereq.RowPerPage,
													StringHelper.Convert(sb.ToString()),
													StringHelper.Convert(pagereq.SortColumn),
                                                    StringHelper.Convert(pagereq.SortOrder)
												};

                using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
                {
                    PagedResponse<RequisitionNotesEntry> response = new PagedResponse<RequisitionNotesEntry>();

                    using (IDataReader reader = Database.ExecuteReader(cmd))
                    {
                        response.Response = CreateEntityBuilder<RequisitionNotesEntry>().BuildEntities(reader);

                        if ((reader.NextResult()) && (reader.Read()))
                        {
                            response.TotalRow = reader.GetInt32(0);
                        }
                    }

                    return response;
                }



   




           
        }
    }
}