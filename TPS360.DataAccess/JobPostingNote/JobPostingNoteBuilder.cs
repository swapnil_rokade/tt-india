﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class JobPostingNoteBuilder : IEntityBuilder<JobPostingNote>
    {
        IList<JobPostingNote> IEntityBuilder<JobPostingNote>.BuildEntities(IDataReader reader)
        {
            List<JobPostingNote> jobPostingNotes = new List<JobPostingNote>();

            while (reader.Read())
            {
                jobPostingNotes.Add(((IEntityBuilder<JobPostingNote>)this).BuildEntity(reader));
            }

            return (jobPostingNotes.Count > 0) ? jobPostingNotes : null;
        }


        JobPostingNote IEntityBuilder<JobPostingNote>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_JOBPOSTINGID = 1;
            const int FLD_COMMONNOTEID = 2;

            JobPostingNote jobPostingNote = new JobPostingNote();

            jobPostingNote.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            jobPostingNote.JobPostingId = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);
            jobPostingNote.CommonNoteId = reader.IsDBNull(FLD_COMMONNOTEID) ? 0 : reader.GetInt32(FLD_COMMONNOTEID);

            return jobPostingNote;
        }

      
    }


    internal sealed class ReqNotesBuilder : IEntityBuilder<RequisitionNotesEntry>
    {

        IList<RequisitionNotesEntry> IEntityBuilder<RequisitionNotesEntry>.BuildEntities(IDataReader reader)
        {
            List<RequisitionNotesEntry> ReqNotesEntry = new List<RequisitionNotesEntry>();

            while (reader.Read())
            {
                ReqNotesEntry.Add(((IEntityBuilder<RequisitionNotesEntry>)this).BuildEntity(reader));
            }

            return (ReqNotesEntry.Count > 0) ? ReqNotesEntry : null;
        }

        RequisitionNotesEntry IEntityBuilder<RequisitionNotesEntry>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_NoteDetail = 1;
            const int FLD_CreatedDate = 2;
            const int FLD_Creator = 3;

            RequisitionNotesEntry ReqNote = new RequisitionNotesEntry();
            ReqNote.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            ReqNote.CreateDate = reader.IsDBNull(FLD_CreatedDate) ? DateTime .MinValue : reader.GetDateTime(FLD_CreatedDate);
            ReqNote.NoteDetail = reader.IsDBNull(FLD_NoteDetail) ? "" : reader.GetString(FLD_NoteDetail);
            ReqNote.CreatorName = reader.IsDBNull(FLD_Creator) ? "" : reader.GetString(FLD_Creator);

            return ReqNote;
        }
    }

}