﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class CompanyDocumentDataAccess : BaseDataAccess, ICompanyDocumentDataAccess
    {
        #region Constructors

        public CompanyDocumentDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<CompanyDocument> CreateEntityBuilder<CompanyDocument>()
        {
            return (new CompanyDocumentBuilder()) as IEntityBuilder<CompanyDocument>;
        }

        #endregion

        #region  Methods

        CompanyDocument ICompanyDocumentDataAccess.Add(CompanyDocument companyDocument)
        {
            const string SP = "dbo.CompanyDocument_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Title", DbType.AnsiString, companyDocument.Title);
                Database.AddInParameter(cmd, "@FileName", DbType.AnsiString, companyDocument.FileName);
                Database.AddInParameter(cmd, "@Description", DbType.AnsiString, companyDocument.Description);
                Database.AddInParameter(cmd, "@DocumentType", DbType.Int32, companyDocument.DocumentType);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, companyDocument.IsRemoved);
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyDocument.CompanyId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, companyDocument.CreatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        companyDocument = CreateEntityBuilder<CompanyDocument>().BuildEntity(reader);
                    }
                    else
                    {
                        companyDocument = null;
                    }
                }

                if (companyDocument == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Company document already exists. Please specify another company document.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this company document.");
                            }
                    }
                }

                return companyDocument;
            }
        }

        CompanyDocument ICompanyDocumentDataAccess.Update(CompanyDocument companyDocument)
        {
            const string SP = "dbo.CompanyDocument_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, companyDocument.Id);
                Database.AddInParameter(cmd, "@Title", DbType.AnsiString, companyDocument.Title);
                Database.AddInParameter(cmd, "@FileName", DbType.AnsiString, companyDocument.FileName);
                Database.AddInParameter(cmd, "@Description", DbType.AnsiString, companyDocument.Description);
                Database.AddInParameter(cmd, "@DocumentType", DbType.Int32, companyDocument.DocumentType);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, companyDocument.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, companyDocument.UpdatorId );

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        companyDocument = CreateEntityBuilder<CompanyDocument>().BuildEntity(reader);
                    }
                    else
                    {
                        companyDocument = null;
                    }
                }

                if (companyDocument == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Company document already exists. Please specify another company document.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this company document.");
                            }
                    }
                }

                return companyDocument;
            }
        }

        CompanyDocument ICompanyDocumentDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.CompanyDocument_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<CompanyDocument>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<CompanyDocument> ICompanyDocumentDataAccess.GetAllByCompanyId(int companyId)
        {
            const string SP = "dbo.CompanyDocument_GetAllCompanyDocumentByCompanyId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CompanyDocument>().BuildEntities(reader);
                }
            }
        }

        IList<CompanyDocument> ICompanyDocumentDataAccess.GetAll()
        {
            const string SP = "dbo.CompanyDocument_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CompanyDocument>().BuildEntities(reader);
                }
            }
        }

        bool ICompanyDocumentDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.CompanyDocument_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a company document which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this company document.");
                        }
                }
            }
        }
        CompanyDocument ICompanyDocumentDataAccess.GetAllByCompanyIDTypeAndFileName(int companyId, string type, string FileName)
        {
            if (companyId < 1)
            {
                throw new ArgumentNullException("companyId");
            }

            if (type == string.Empty)
            {
                throw new ArgumentNullException("type");
            }

            const string SP = "dbo.CompanyDocument_GetCompanyDocumentByCompanyIdTypeAndFileName";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyId );
                Database.AddInParameter(cmd, "@Type", DbType.String, type);
                Database.AddInParameter(cmd, "@FileName", DbType.String, FileName);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<CompanyDocument>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }
        bool ICompanyDocumentDataAccess.DeleteByCompanyId(int companyId)
        {
            if (companyId < 1)
            {
                throw new ArgumentNullException("companyId");
            }

            const string SP = "dbo.CompanyDocument_DeleteByCompanyId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a company document which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this company document.");
                        }
                }
            }
        }


        PagedResponse<CompanyDocument>  ICompanyDocumentDataAccess.GetPagedByCompanyId(int companyId, PagedRequest request)
        {
           
            string whereClause = string.Empty;
            whereClause = "CompanyID = " + companyId;

            object[] paramValues = new object[] {request .PageIndex,
													request .RowPerPage   ,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request .SortColumn  ),
                                                    StringHelper.Convert(request.SortOrder),
												};
            const string SP = "dbo.CompanyDocument_GetPagedCompanyDocumentByCompanyId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<CompanyDocument > response = new PagedResponse<CompanyDocument >();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<CompanyDocument >().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }
        #endregion
    }
}