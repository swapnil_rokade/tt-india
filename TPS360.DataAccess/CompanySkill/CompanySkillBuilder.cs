﻿using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class CompanySkillBuilder : IEntityBuilder<CompanySkill>
    {
        IList<CompanySkill> IEntityBuilder<CompanySkill>.BuildEntities(IDataReader reader)
        {
            List<CompanySkill> companySkills = new List<CompanySkill>();

            while (reader.Read())
            {
                companySkills.Add(((IEntityBuilder<CompanySkill>)this).BuildEntity(reader));
            }

            return (companySkills.Count > 0) ? companySkills : null;
        }

        CompanySkill IEntityBuilder<CompanySkill>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_COMPANYID = 1;
            const int FLD_SKILLID = 2;

            CompanySkill companySkill = new CompanySkill();

            companySkill.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            companySkill.CompanyId = reader.IsDBNull(FLD_COMPANYID) ? 0 : reader.GetInt32(FLD_COMPANYID);
            companySkill.SkillId = reader.IsDBNull(FLD_SKILLID) ? 0 : reader.GetInt32(FLD_SKILLID);

            return companySkill;
        }
    }
}