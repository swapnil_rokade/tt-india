﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class CompanySkillDataAccess : BaseDataAccess, ICompanySkillDataAccess
    {
        #region Constructors

        public CompanySkillDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<CompanySkill> CreateEntityBuilder<CompanySkill>()
        {
            return (new CompanySkillBuilder()) as IEntityBuilder<CompanySkill>;
        }

        #endregion

        #region  Methods

        CompanySkill ICompanySkillDataAccess.Add(CompanySkill companySkill)
        {
            const string SP = "dbo.CompanySkill_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, companySkill.CompanyId);
                Database.AddInParameter(cmd, "@SkillId", DbType.Int32, companySkill.SkillId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        companySkill = CreateEntityBuilder<CompanySkill>().BuildEntity(reader);
                    }
                    else
                    {
                        companySkill = null;
                    }
                }

                if (companySkill == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("CompanySkill already exists. Please specify another companySkill.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this companySkill.");
                            }
                    }
                }

                return companySkill;
            }
        }        

        CompanySkill ICompanySkillDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.CompanySkill_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<CompanySkill>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<CompanySkill> ICompanySkillDataAccess.GetAll()
        {
            const string SP = "dbo.CompanySkill_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CompanySkill>().BuildEntities(reader);
                }
            }
        }

        IList<CompanySkill> ICompanySkillDataAccess.GetAllByCompanyId(int companyId)
        {
            const string SP = "dbo.CompanySkill_GetAllByCompanyId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CompanySkill>().BuildEntities(reader);
                }
            }
        }

        bool ICompanySkillDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.CompanySkill_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a companySkill which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this companySkill.");
                        }
                }
            }
        }

        bool ICompanySkillDataAccess.DeleteByCompanyId(int companyId)
        {
            if (companyId < 1)
            {
                throw new ArgumentNullException("companyId");
            }

            const string SP = "dbo.CompanySkill_DeleteByCompanyId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                    {
                        return true;
                    }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                    {
                        throw new ArgumentException("Cannot delete a companySkill which has association.");
                    }
                    default:
                    {
                        throw new SystemException("An unexpected error has occurred while deleting this companySkill.");
                    }
                }
            }
        }

        #endregion
    }
}