﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberSourceHistoryBuilder : IEntityBuilder<MemberSourceHistory>
    {
        IList<MemberSourceHistory> IEntityBuilder<MemberSourceHistory>.BuildEntities(IDataReader reader)
        {
            List<MemberSourceHistory> memberSourceHistory  = new List<MemberSourceHistory>();

            while (reader.Read())
            {
                memberSourceHistory.Add(((IEntityBuilder<MemberSourceHistory>)this).BuildEntity(reader));
            }

            return (memberSourceHistory.Count > 0) ? memberSourceHistory : null;
        }

        MemberSourceHistory IEntityBuilder<MemberSourceHistory>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_MEMBERID = 1;
            const int FLD_CREATORID = 2;
            const int FLD_UPDATORID = 3;
            const int FLD_CREATEDATE = 4;
            const int FLD_UPDATEDATE = 5;
            const int FLD_ISREMOVED = 6;
            const int FLD_SOURCE = 7;
            const int FLD_EXPIREDATE = 8;
            const int FLD_SOURCEDESCRIPTION = 9;
            const int FLD_USERID = 10;
            const int FLD_USERNAME = 11;
            const int FLD_SOURCENAME = 12; 
            const int FLD_SOURCEDESCRIPTIONNAME = 13;
           

            MemberSourceHistory memberSourceHistory = new MemberSourceHistory();
            memberSourceHistory.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberSourceHistory.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberSourceHistory.CreatorId = reader.IsDBNull(FLD_CREATORID) ?  0 : reader.GetInt32(FLD_CREATORID);
            memberSourceHistory.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ?0 :   reader.GetInt32(FLD_UPDATORID);
            memberSourceHistory.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberSourceHistory.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            memberSourceHistory.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberSourceHistory.Source = reader.IsDBNull(FLD_SOURCE) ? 0 :   reader.GetInt32(FLD_SOURCE);
            memberSourceHistory.ExpireDate = reader.IsDBNull(FLD_EXPIREDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_EXPIREDATE);
            memberSourceHistory.SourceDescription = reader.IsDBNull(FLD_SOURCEDESCRIPTION) ? string.Empty : reader.GetString(FLD_SOURCEDESCRIPTION);
            memberSourceHistory.UserId = reader.IsDBNull(FLD_USERNAME) ? 0 : reader.GetInt32(FLD_USERID);
            memberSourceHistory.SourceName = reader.IsDBNull(FLD_SOURCENAME) ? string.Empty : reader.GetString(FLD_SOURCENAME);
            memberSourceHistory.SourceDescriptionName = reader.IsDBNull(FLD_SOURCEDESCRIPTIONNAME) ? string.Empty : reader.GetString(FLD_SOURCEDESCRIPTIONNAME);
            return memberSourceHistory;
        }
    }
}