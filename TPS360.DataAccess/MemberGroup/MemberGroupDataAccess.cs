﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberGroupDataAccess.cs
    Description: This page is used for Member Group Data Access
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date             Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Jan-28-2009           Shivanand           Defect ID:8879; In the method "GetPagedByMemberIdandGroupType()", SortColumn in the request object is formatted with table alias.
    0.2            Dec-8-2009            Sandeesh            Defect id: 11988 -To return a proper generic list for the method -GetAllNameByManagerId
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/


using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class MemberGroupDataAccess : BaseDataAccess, IMemberGroupDataAccess
    {
        #region Constructors

        public MemberGroupDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberGroup> CreateEntityBuilder<MemberGroup>()
        {
            return (new MemberGroupBuilder()) as IEntityBuilder<MemberGroup>;
        }

        #endregion

        #region  Methods

        MemberGroup IMemberGroupDataAccess.Add(MemberGroup memberGroup)
        {
            const string SP = "dbo.MemberGroup_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Name", DbType.AnsiString, StringHelper.Convert(memberGroup.Name));
                Database.AddInParameter(cmd, "@Description", DbType.AnsiString, StringHelper.Convert(memberGroup.Description));
                Database.AddInParameter(cmd, "@GroupType", DbType.Int32, memberGroup.GroupType);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberGroup.IsRemoved);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberGroup.CreatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberGroup = CreateEntityBuilder<MemberGroup>().BuildEntity(reader);
                    }
                    else
                    {
                        memberGroup = null;
                    }
                }

                if (memberGroup == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member group already exists. Please specify another member group.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this member group.");
                            }
                    }
                }

                return memberGroup;
            }
        }

        MemberGroup IMemberGroupDataAccess.Update(MemberGroup memberGroup)
        {
            const string SP = "dbo.MemberGroup_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberGroup.Id);
                Database.AddInParameter(cmd, "@Name", DbType.AnsiString, StringHelper.Convert(memberGroup.Name));
                Database.AddInParameter(cmd, "@Description", DbType.AnsiString, StringHelper.Convert(memberGroup.Description));
                Database.AddInParameter(cmd, "@GroupType", DbType.Int32, memberGroup.GroupType);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberGroup.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberGroup.UpdatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberGroup = CreateEntityBuilder<MemberGroup>().BuildEntity(reader);
                    }
                    else
                    {
                        memberGroup = null;
                    }
                }

                if (memberGroup == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member group already exists. Please specify another member group.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this member group.");
                            }
                    }
                }

                return memberGroup;
            }
        }

        MemberGroup IMemberGroupDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberGroup_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberGroup>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberGroup> IMemberGroupDataAccess.GetAll()
        {
            const string SP = "dbo.MemberGroup_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberGroup>().BuildEntities(reader);
                }
            }
        }

        IList<MemberGroup> IMemberGroupDataAccess.GetAll(int groupType)
        {
            const string SP = "dbo.MemberGroup_GetAllByGroupType";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@GroupType", DbType.Int32, groupType);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberGroup>().BuildEntities(reader);
                }
            }
        }

        IList<MemberGroup> IMemberGroupDataAccess.GetAllByMemberId(int memberId)
        {
            const string SP = "dbo.MemberGroup_GetAllByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberGroup>().BuildEntities(reader);
                }
            }
        }

        PagedResponse<MemberGroup> IMemberGroupDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.MemberGroup_GetPaged";
            string whereClause = string.Empty;

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "Name";
            }

            request.SortColumn = "[M].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberGroup> response = new PagedResponse<MemberGroup>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<MemberGroup>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        PagedResponse<MemberGroup> IMemberGroupDataAccess.GetPagedByMemberId(PagedRequest request)
        {
            const string SP = "dbo.MemberGroup_GetPaged";
            string whereClause = string.Empty;

            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "MemberId"))
                        {
                            //sb.Append("[M].[CreatorId]");
                            //sb.Append(" = ");
                            //sb.Append(value);
                            sb.Append("[M].[Id]");
                            sb.Append(" IN ");
                            sb.Append(" ( SELECT DISTINCT MemberGroupId FROM MemberGroupManager WHERE MemberId= ");
                            sb.Append(value);
                            sb.Append(" ) ");
                        }
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "Name";
            }

            request.SortColumn = "[M].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberGroup> response = new PagedResponse<MemberGroup>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<MemberGroup>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        PagedResponse<MemberGroup> IMemberGroupDataAccess.GetPagedByMemberIdandGroupType(PagedRequest request)
        {
            const string SP = "dbo.MemberGroup_GetPaged";
            string whereClause = string.Empty;

            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "SearchKey"))
                        {
                            if (!StringHelper.IsBlank(sb))
                            {
                                sb.Append(" AND ( [M].[Name] like '" + value + "%')");
                            }
                            else
                            {
                                sb.Append(" ([M].[Name] like '" + value + "%')");
                            }
                        }
                        if (StringHelper.IsEqual(column, "GroupType"))
                        {
                            sb.Append("[M].[GroupType]");
                            sb.Append(" = ");
                            sb.Append(value);                            
                        }

                        if (StringHelper.IsEqual(column, "MemberId"))
                        {
                            if (value != "0")
                            {
                                if (!StringHelper.IsBlank(sb))
                                {
                                    sb.Append(" AND ");
                                    sb.Append(" [M].[Id]");
                                    sb.Append(" IN ");
                                    sb.Append(" ( SELECT DISTINCT MemberGroupId FROM MemberGroupManager WHERE MemberId= ");
                                    sb.Append(value);
                                    sb.Append(" ) ");
                                }

                                else
                                {
                                    sb.Append(" [M].[Id]");
                                    sb.Append(" IN ");
                                    sb.Append(" ( SELECT DISTINCT MemberGroupId FROM MemberGroupManager WHERE MemberId= ");
                                    sb.Append(value);
                                    sb.Append(" ) ");

                                }
                            }
                        }                        
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[M].[Name]";   //0.1
            }

           // request.SortColumn = "[M].[" + request.SortColumn + "]";  // 0.1

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberGroup> response = new PagedResponse<MemberGroup>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<MemberGroup>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        bool IMemberGroupDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberGroup_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member group which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member group.");
                        }
                }
            }
        }
        MemberGroup IMemberGroupDataAccess.GetByName(string name)
        {
            const string SP = "dbo.MemberGroup_GetByName";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Name", DbType.AnsiString , name );

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberGroup>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }


        ArrayList IMemberGroupDataAccess.GetAllNameByManagerId(int managerId)
        {
            if (managerId < 0)
            {
                throw new ArgumentException("managerId");
            }

            const string SP = "dbo.MemberGroup_GetAllMemberGroupByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, managerId);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList memberGroupList = new ArrayList();

                    while (reader.Read())
                    {
                       // memberGroupList.Add(new { Id = reader.GetInt32(0), Name = reader.GetString(1) });//0.2
                         memberGroupList.Add(new MemberGroup() { Id = reader.GetInt32(0), Name = reader.GetString(1) });//0.2
                    }

                    return memberGroupList;
                }
            }
        }

        int IMemberGroupDataAccess.GetCountByGroupType(int groupType)
        {
            const string SP = "dbo.MemberGroup_GetCountByGroupType";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@GroupType", DbType.Int32, groupType);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        return reader == null ? 0 : Convert.ToInt32(reader.GetValue(0).ToString());
                    } return 0;

                }
            }
        }


        int IMemberGroupDataAccess.GetCountByManagerIDAndGroupType(int ManagerID, int groupType)
        {
            if (ManagerID < 0)
            {
                throw new ArgumentException("ManagerID");
            }
            if (groupType < 0)
            {
                throw new ArgumentException("groupType");
            }

            const string SP = "dbo.MemberGroup_GetGountMemberGroupByMemberIdandGroupType";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@ManagerID", DbType.Int32, ManagerID);
                Database.AddInParameter(cmd, "@GroupType", DbType.Int32, groupType);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        return reader == null ? 0 :Convert .ToInt32(reader.GetValue(0).ToString());
                    }

                    return 0;
                }
            }
        }
        ArrayList IMemberGroupDataAccess.GetAllNameByManagerId(int memberId,int groupType)
        {
            if (memberId < 0)
            {
                throw new ArgumentException("memberId");
            }
            if (groupType < 0)
            {
                throw new ArgumentException("groupType");
            }

            const string SP = "dbo.MemberGroup_GetAllMemberGroupByMemberIdandGroupType";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                Database.AddInParameter(cmd, "@GroupType", DbType.Int32, groupType);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList memberGroupList = new ArrayList();

                    while (reader.Read())
                    {
                      //memberGroupList.Add(new { Id = reader.GetInt32(0), Name = reader.GetString(1) });//0.2
                        memberGroupList.Add(new MemberGroup() { Id = reader.GetInt32(0), Name = reader.GetString(1) });//0.2
                    }

                    return memberGroupList;
                }
            }
        }

        bool IMemberGroupDataAccess.GetMemberGroupAccessByCreatorIdAndGroupId(int CreatorId, int GroupId)
        {
            if (CreatorId < 1)
            {
                throw new ArgumentNullException("CreatorId");
            }
            if (GroupId < 1)
            {
                throw new ArgumentNullException("GroupId");
            }
            const string SP = "dbo.MemberGroup_GetMemberGroupAccessByCreatorIdandGroupId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, CreatorId);
                Database.AddInParameter(cmd, "@GroupId", DbType.AnsiString, GroupId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        return reader.IsDBNull(0) ? false : reader.GetBoolean(0);
                    }

                    return false;
                }
            }
        }
        #endregion
    }
}