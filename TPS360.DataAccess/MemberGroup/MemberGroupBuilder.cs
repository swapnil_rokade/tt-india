﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberGroupBuilder : IEntityBuilder<MemberGroup>
    {
        IList<MemberGroup> IEntityBuilder<MemberGroup>.BuildEntities(IDataReader reader)
        {
            List<MemberGroup> memberGroups = new List<MemberGroup>();

            while (reader.Read())
            {
                memberGroups.Add(((IEntityBuilder<MemberGroup>)this).BuildEntity(reader));
            }

            return (memberGroups.Count > 0) ? memberGroups : null;
        }

        MemberGroup IEntityBuilder<MemberGroup>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_NAME = 1;
            const int FLD_DESCRIPTION = 2;
            const int FLD_GROUPTYPE = 3;
            const int FLD_ISREMOVED = 4;
            const int FLD_CREATORID = 5;
            const int FLD_UPDATORID = 6;
            const int FLD_CREATEDATE = 7;
            const int FLD_UPDATEDATE = 8;
            const int FLD_CANDIDATECOUNT = 9;
            const int FLD_MANAGERNAME = 10;
            MemberGroup memberGroup = new MemberGroup();

            memberGroup.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberGroup.Name = reader.IsDBNull(FLD_NAME) ? string.Empty : reader.GetString(FLD_NAME);
            memberGroup.Description = reader.IsDBNull(FLD_DESCRIPTION) ? string.Empty : reader.GetString(FLD_DESCRIPTION);
            memberGroup.GroupType = reader.IsDBNull(FLD_GROUPTYPE) ? 0 : reader.GetInt32(FLD_GROUPTYPE);
            memberGroup.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberGroup.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberGroup.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberGroup.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberGroup.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            if (reader.FieldCount > FLD_CANDIDATECOUNT)
            {
                memberGroup.CandidateCount = reader.IsDBNull(FLD_CANDIDATECOUNT) ? 0 : reader.GetInt32(FLD_CANDIDATECOUNT);
                memberGroup.ManagerName = reader.IsDBNull(FLD_MANAGERNAME) ? string.Empty : reader.GetString(FLD_MANAGERNAME);
            }
            return memberGroup;
        }
    }
}