using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal interface IEntityBuilder<T> where T : BaseEntity
    {
        IList<T> BuildEntities(IDataReader dataReader);

        T BuildEntity(IDataReader dataReader);
    }

    internal abstract class BaseDataAccess : MarshalByRefObject, IDisposable
    {
        #region Instance Variables

        private bool _isDisposed;
        private Context _context;

        private Database _db;
        private Database _ATdb;

        #endregion

        #region Properties

        protected virtual Context Context
        {
            [DebuggerStepThrough()]
            get
            {
                if (_context == null)
                {
                    _context = new Context();
                }

                return _context;
            }
        }

        protected virtual Database Database
        {
            [DebuggerStepThrough()]
            get
            {
                if (_db == null)
                {
                    string type = typeof(Database).ToString();

                    if (Context.Contains(type))
                    {
                        _db = (Database)Context[type];

                    }
                    else
                    {
                        _db = DatabaseFactory.CreateDatabase();

                        Context[type] = _db as IDisposable;
                    }
                }

                return _db;
            }
        }


        protected virtual Database ATDatabase
        {
            [DebuggerStepThrough()]
            get
            {
                if (_ATdb == null)
                {
                    string type = typeof(Database).ToString();

                    if (Context.Contains(type))
                    {
                        _ATdb = (Database)Context[type];

                    }
                    else
                    {
                        _ATdb = DatabaseFactory.CreateDatabase("TPS360TrailDB");

                        Context[type] = _ATdb as IDisposable;
                    }
                }

                return _ATdb;
            }
        }


        #endregion

        #region Constructer & Destructer

        protected BaseDataAccess(Context context)
        {
            _context = context;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                }
            }

            _isDisposed = true;
        }

        ~BaseDataAccess()
        {
            Dispose(false);
        }

        #endregion

        #region Protected Methods

        [DebuggerStepThrough()]
        protected abstract IEntityBuilder<T> CreateEntityBuilder<T>() where T : BaseEntity;

        //[DebuggerStepThrough()]
        protected virtual int GetReturnCodeFromParameter(DbCommand command)
        {
            return (int)Database.GetParameterValue(command, SqlConstants.PRM_RETURN_CODE);
        }

        [DebuggerStepThrough()]
        protected virtual int GetNewIdFromParameter(DbCommand command, string paramName)
        {
            return (int)Database.GetParameterValue(command, string.Concat("@" + paramName));
        }

        [DebuggerStepThrough()]
        protected virtual int GetTotalRowFromParameter(DbCommand command)
        {
            return (int)Database.GetParameterValue(command, SqlConstants.PRM_TOTAL_ROW);
        }

        [DebuggerStepThrough()]
        protected virtual int GetTotalPageFromParameter(DbCommand command)
        {
            return (int)Database.GetParameterValue(command, SqlConstants.PRM_TOTAL_PAGE);
        }

        [DebuggerStepThrough()]
        protected virtual int GetCountFromParameter(DbCommand command)
        {
            return (int)Database.GetParameterValue(command, SqlConstants.PRM_COUNT);
        }

        [DebuggerStepThrough()]
        protected void AddOutputParameter(DbCommand command)
        {
            Database.AddOutParameter(command, "@ReturnCode", DbType.Int32, 4);
        }
        
        #endregion
    }
}