﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class JobPostingDocumentBuilder : IEntityBuilder<JobPostingDocument>
    {
        IList<JobPostingDocument> IEntityBuilder<JobPostingDocument>.BuildEntities(IDataReader reader)
        {
            List<JobPostingDocument> jobPostingDocuments = new List<JobPostingDocument>();

            while (reader.Read())
            {
                jobPostingDocuments.Add(((IEntityBuilder<JobPostingDocument>)this).BuildEntity(reader));
            }

            return (jobPostingDocuments.Count > 0) ? jobPostingDocuments : null;
        }

        JobPostingDocument IEntityBuilder<JobPostingDocument>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_TITLE = 1;
            const int FLD_FILENAME = 2;
            const int FLD_DOCUMENTTYPE = 3;
            const int FLD_JOBPOSTINGID = 4;

            JobPostingDocument jobPostingDocument = new JobPostingDocument();

            jobPostingDocument.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            jobPostingDocument.Title = reader.IsDBNull(FLD_TITLE) ? string.Empty : reader.GetString(FLD_TITLE);
            jobPostingDocument.FileName = reader.IsDBNull(FLD_FILENAME) ? string.Empty : reader.GetString(FLD_FILENAME);
            jobPostingDocument.DocumentType = reader.IsDBNull(FLD_DOCUMENTTYPE) ? 0 : reader.GetInt32(FLD_DOCUMENTTYPE);
            jobPostingDocument.JobPostingId = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);

            return jobPostingDocument;
        }
    }
}