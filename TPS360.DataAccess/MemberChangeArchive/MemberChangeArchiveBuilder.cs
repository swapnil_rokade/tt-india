﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class MemberChangeArchiveBuilder : IEntityBuilder<MemberChangeArchive>
    {
        IList<MemberChangeArchive> IEntityBuilder<MemberChangeArchive>.BuildEntities(IDataReader reader)
        {
            List<MemberChangeArchive> memberChangeArchives = new List<MemberChangeArchive>();

            while (reader.Read())
            {
                memberChangeArchives.Add(((IEntityBuilder<MemberChangeArchive>)this).BuildEntity(reader));
            }

            return (memberChangeArchives.Count > 0) ? memberChangeArchives : null;
        }

        MemberChangeArchive IEntityBuilder<MemberChangeArchive>.BuildEntity(IDataReader reader)
		{
				const int FLD_ID = 0;
				const int FLD_MEMBERID = 1;
				const int FLD_CHANGEDOBJECT = 2;
				const int FLD_ISREMOVE = 3;
				const int FLD_CREATORID = 4;
				const int FLD_UPDATORID = 5;
				const int FLD_CREATEDATE = 6;
				const int FLD_UPDATEDATE = 7;

            MemberChangeArchive memberChangeArchive = new MemberChangeArchive();

			memberChangeArchive.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
			memberChangeArchive.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberChangeArchive.ChangedObject = (byte[])(reader.GetValue(FLD_CHANGEDOBJECT));
            //memberChangeArchive.ChangedObject = (reader.IsDBNull(FLD_CHANGEDOBJECT) ? null : );
			memberChangeArchive.IsRemove = reader.IsDBNull(FLD_ISREMOVE) ? false : reader.GetBoolean(FLD_ISREMOVE);
			memberChangeArchive.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
			memberChangeArchive.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
			memberChangeArchive.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
			memberChangeArchive.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
		
            return memberChangeArchive;
		}
    }
}