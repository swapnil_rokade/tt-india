﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberExperienceDataAccess.cs
    Description: This page is used for dataaccess of MemberExperience.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.         Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Dec-22-2008         Shivanand           Defect #9511; In the method Add() & Update(),new Inparameters added [ContactName,CompanyEmail,CompanyPhone,CompanyWebsite].
    0.2             July-8-2009         Gopala Swamy J      Defect #10847, Added new Parameter @StateId 
    0.3             Sep-18-2009         Gopala Swamy J      Defect #11375,Added new DeleteByMemberId()
    ------------------------------------------------------------------------------------------------------------------------------
*/
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class MemberExperienceDataAccess : BaseDataAccess, IMemberExperienceDataAccess
    {
        #region Constructors

        public MemberExperienceDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberExperience> CreateEntityBuilder<MemberExperience>()
        {
            return (new MemberExperienceBuilder()) as IEntityBuilder<MemberExperience>;
        }

        #endregion

        #region  Methods

        MemberExperience IMemberExperienceDataAccess.Add(MemberExperience memberExperience)
        {
            const string SP = "dbo.MemberExperience_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);

                Database.AddInParameter(cmd, "@CompanyName", DbType.AnsiString, StringHelper.Convert(memberExperience.CompanyName));
                Database.AddInParameter(cmd, "@PositionName", DbType.AnsiString, StringHelper.Convert(memberExperience.PositionName));
                Database.AddInParameter(cmd, "@City", DbType.AnsiString, StringHelper.Convert(memberExperience.City));
                Database.AddInParameter(cmd, "@DateFrom", DbType.DateTime,  NullConverter.Convert(memberExperience.DateFrom));
                
                    Database.AddInParameter(cmd, "@DateTo", DbType.DateTime, NullConverter.Convert(memberExperience.DateTo));
               
                Database.AddInParameter(cmd, "@IsTillDate", DbType.Boolean, memberExperience.IsTillDate);
                Database.AddInParameter(cmd, "@Responsibilities", DbType.AnsiString, StringHelper.Convert(memberExperience.Responsibilities));
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberExperience.IsRemoved);
                Database.AddInParameter(cmd, "@CountryLookupId", DbType.Int32, memberExperience.CountryLookupId);
                Database.AddInParameter(cmd, "@IndustryCategoryLookupId", DbType.Int32, memberExperience.IndustryCategoryLookupId);
                Database.AddInParameter(cmd, "@FunctionalCategoryLookupId", DbType.Int32, memberExperience.FunctionalCategoryLookupId);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberExperience.MemberId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberExperience.CreatorId);
  // 0.1 starts
                Database.AddInParameter(cmd, "@ContactName", DbType.AnsiString, StringHelper.Convert(memberExperience.ContactName));
                Database.AddInParameter(cmd, "@CompanyEmail", DbType.AnsiString, StringHelper.Convert(memberExperience.CompanyEmail));
                Database.AddInParameter(cmd, "@CompanyPhone", DbType.AnsiString, StringHelper.Convert(memberExperience.CompanyPhone));
                Database.AddInParameter(cmd, "@CompanyWebsite", DbType.AnsiString, StringHelper.Convert(memberExperience.CompanyWebsite));
 // 0.1 ends
                Database.AddInParameter(cmd, "@StateId", DbType.AnsiString, memberExperience.StateId); //0.2

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberExperience = CreateEntityBuilder<MemberExperience>().BuildEntity(reader);
                    }
                    else
                    {
                        memberExperience = null;
                    }
                }

                if (memberExperience == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member experience already exists. Please specify another member experience.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this member experience.");
                            }
                    }
                }

                return memberExperience;
            }
        }

        MemberExperience IMemberExperienceDataAccess.Update(MemberExperience memberExperience)
        {
            const string SP = "dbo.MemberExperience_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);

                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberExperience.Id);

                Database.AddInParameter(cmd, "@CompanyName", DbType.AnsiString, StringHelper.Convert(memberExperience.CompanyName));
                Database.AddInParameter(cmd, "@PositionName", DbType.AnsiString, StringHelper.Convert(memberExperience.PositionName));
                Database.AddInParameter(cmd, "@City", DbType.AnsiString, StringHelper.Convert(memberExperience.City));
                Database.AddInParameter(cmd, "@DateFrom", DbType.DateTime, NullConverter.Convert(memberExperience.DateFrom));
                
                
                    Database.AddInParameter(cmd, "@DateTo", DbType.DateTime, NullConverter.Convert(memberExperience.DateTo));
                

                Database.AddInParameter(cmd, "@IsTillDate", DbType.Boolean, memberExperience.IsTillDate);
                Database.AddInParameter(cmd, "@Responsibilities", DbType.AnsiString, StringHelper.Convert(memberExperience.Responsibilities));
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberExperience.IsRemoved);
                Database.AddInParameter(cmd, "@CountryLookupId", DbType.Int32, memberExperience.CountryLookupId);
                Database.AddInParameter(cmd, "@IndustryCategoryLookupId", DbType.Int32, memberExperience.IndustryCategoryLookupId);
                Database.AddInParameter(cmd, "@FunctionalCategoryLookupId", DbType.Int32, memberExperience.FunctionalCategoryLookupId);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberExperience.UpdatorId);

   // 0.1 starts
                Database.AddInParameter(cmd, "@ContactName", DbType.AnsiString, StringHelper.Convert(memberExperience.ContactName));
                Database.AddInParameter(cmd, "@CompanyEmail", DbType.AnsiString, StringHelper.Convert(memberExperience.CompanyEmail));
                Database.AddInParameter(cmd, "@CompanyPhone", DbType.AnsiString, StringHelper.Convert(memberExperience.CompanyPhone));
                Database.AddInParameter(cmd, "@CompanyWebsite", DbType.AnsiString, StringHelper.Convert(memberExperience.CompanyWebsite));
  // 0.1 ends
                Database.AddInParameter(cmd, "@StateId", DbType.AnsiString, memberExperience.StateId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberExperience = CreateEntityBuilder<MemberExperience>().BuildEntity(reader);
                    }
                    else
                    {
                        memberExperience = null;
                    }
                }

                if (memberExperience == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member experience already exists. Please specify another member experience.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this member experience.");
                            }
                    }
                }

                return memberExperience;
            }
        }

        MemberExperience IMemberExperienceDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberExperience_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberExperience>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberExperience> IMemberExperienceDataAccess.GetAll()
        {
            const string SP = "dbo.MemberExperience_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberExperience>().BuildEntities(reader);
                }
            }
        }

        IList<MemberExperience> IMemberExperienceDataAccess.GetAllByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberExperience_GetAllMemberExperienceByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberExperience>().BuildEntities(reader);
                }
            }
        }

        PagedResponse<MemberExperience> IMemberExperienceDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.MemberExperience_GetPaged";
            string whereClause = string.Empty;

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "CompanyName";
            }

            request.SortColumn = "[ME].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberExperience> response = new PagedResponse<MemberExperience>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<MemberExperience>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        PagedResponse<MemberExperience> IMemberExperienceDataAccess.GetPagedByMemberId(PagedRequest request)
        {
            const string SP = "dbo.MemberExperience_GetPaged";
            string whereClause = string.Empty;

            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "MemberId"))
                        {
                            sb.Append("[ME].[MemberId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "CompanyName";
            }

            request.SortColumn = "[ME].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberExperience> response = new PagedResponse<MemberExperience>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<MemberExperience>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        bool IMemberExperienceDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberExperience_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member experience which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member experience.");
                        }
                }
            }
        }

        //0.3 starts here

        bool IMemberExperienceDataAccess.DeleteByMemberId(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberExperience_DeleteMemberExperienceByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member experience which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member experience.");
                        }
                }
            }
        }
        //0.3  end  here
        #endregion
    }
}