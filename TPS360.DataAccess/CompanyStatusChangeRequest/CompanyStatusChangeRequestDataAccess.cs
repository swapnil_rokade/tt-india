﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class CompanyStatusChangeRequestDataAccess : BaseDataAccess, ICompanyStatusChangeRequestDataAccess
    {
        #region Constructors

        public CompanyStatusChangeRequestDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<CompanyStatusChangeRequest> CreateEntityBuilder<CompanyStatusChangeRequest>()
        {
            return (new CompanyStatusChangeRequestBuilder()) as IEntityBuilder<CompanyStatusChangeRequest>;
        }

        #endregion

        #region  Methods

        CompanyStatusChangeRequest ICompanyStatusChangeRequestDataAccess.Add(CompanyStatusChangeRequest companyStatusChangeRequest)
        {
            const string SP = "dbo.CompanyStatusChangeRequest_Create";

            string checkList = string.Empty;

            if (companyStatusChangeRequest.CheckList != null && companyStatusChangeRequest.CheckList.Count > 0)
            {
                checkList = string.Join("|", StringHelper.ToArray(companyStatusChangeRequest.CheckList));
            }
            
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@LastStatus", DbType.Int32, companyStatusChangeRequest.LastStatus);
                Database.AddInParameter(cmd, "@RequestedStatus", DbType.Int32, companyStatusChangeRequest.RequestedStatus);
                Database.AddInParameter(cmd, "@CheckList", DbType.String, NullConverter.Convert(checkList));
                Database.AddInParameter(cmd, "@Notes", DbType.AnsiString, StringHelper.Convert(companyStatusChangeRequest.Notes));
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyStatusChangeRequest.CompanyId);
                Database.AddInParameter(cmd, "@RequestorId", DbType.Int32, companyStatusChangeRequest.RequestorId);
                Database.AddInParameter(cmd, "@ApproveStatus", DbType.Int32, companyStatusChangeRequest.ApproveStatus);
                Database.AddInParameter(cmd, "@ApproveStatusChangerId", DbType.Int32, NullConverter.Convert(companyStatusChangeRequest.ApproveStatusChangerId));
                Database.AddInParameter(cmd, "@ApproveStatusChangeDate", DbType.DateTime, NullConverter.Convert(companyStatusChangeRequest.ApproveStatusChangeDate));
                Database.AddInParameter(cmd, "@ApproveStatusComments", DbType.AnsiString, StringHelper.Convert(companyStatusChangeRequest.ApproveStatusComments));
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        companyStatusChangeRequest = CreateEntityBuilder<CompanyStatusChangeRequest>().BuildEntity(reader);
                    }
                    else
                    {
                        companyStatusChangeRequest = null;
                    }
                }

                if (companyStatusChangeRequest == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Company status change request already exists. Please specify another company status change request.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this company status change request.");
                            }
                    }
                }

                return companyStatusChangeRequest;
            }
        }

        CompanyStatusChangeRequest ICompanyStatusChangeRequestDataAccess.Update(CompanyStatusChangeRequest companyStatusChangeRequest)
        {
            const string SP = "dbo.CompanyStatusChangeRequest_Update";

            string checkList = string.Empty;

            if (companyStatusChangeRequest.CheckList != null && companyStatusChangeRequest.CheckList.Count > 0)
            {
                checkList = string.Join("|", StringHelper.ToArray(companyStatusChangeRequest.CheckList));
            }

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, companyStatusChangeRequest.Id);
                Database.AddInParameter(cmd, "@CheckList", DbType.String, NullConverter.Convert(checkList));
                Database.AddInParameter(cmd, "@Notes", DbType.AnsiString, StringHelper.Convert(companyStatusChangeRequest.Notes));
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        companyStatusChangeRequest = CreateEntityBuilder<CompanyStatusChangeRequest>().BuildEntity(reader);
                    }
                    else
                    {
                        companyStatusChangeRequest = null;
                    }
                }

                if (companyStatusChangeRequest == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Company status change request already exists. Please specify another company status change request.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this company status change request.");
                            }
                    }
                }

                return companyStatusChangeRequest;
            }
        }

        bool ICompanyStatusChangeRequestDataAccess.Verify(int companyId, ApproveStatus approveStatus, string comments, int changerId)
        {
            const string SP = "dbo.CompanyStatusChangeRequest_Verify";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyId);
                Database.AddInParameter(cmd, "@ApproveStatus", DbType.Int32, approveStatus);
                Database.AddInParameter(cmd, "@ApproveStatusComments", DbType.AnsiString, comments);
                Database.AddInParameter(cmd, "@ApproveStatusChangerId", DbType.Int32, changerId);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_SUCCESS_UPDATE:
                        {
                            return true;
                        }
                        default:
                        {
                            throw new SystemException("An unexpected error has occurred while updating this company status change request.");
                        }
                    }
                }
            }
        }

        CompanyStatusChangeRequest ICompanyStatusChangeRequestDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.CompanyStatusChangeRequest_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<CompanyStatusChangeRequest>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        CompanyStatusChangeRequest ICompanyStatusChangeRequestDataAccess.GetByCompanyId(int companyId)
        {
            const string SP = "dbo.CompanyStatusChangeRequest_GetByCompanyId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return CreateEntityBuilder<CompanyStatusChangeRequest>().BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<CompanyStatusChangeRequest> ICompanyStatusChangeRequestDataAccess.GetAllByCompanyId(int companyId)
        {
            const string SP = "dbo.CompanyDocument_GetAllByCompanyId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CompanyStatusChangeRequest>().BuildEntities(reader);
                }
            }
        }

        IList<CompanyStatusChangeRequest> ICompanyStatusChangeRequestDataAccess.GetAll()
        {
            const string SP = "dbo.CompanyStatusChangeRequest_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CompanyStatusChangeRequest>().BuildEntities(reader);
                }
            }
        }

        bool ICompanyStatusChangeRequestDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.CompanyStatusChangeRequest_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a company status change request which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this company status change request.");
                        }
                }
            }
        }

        bool ICompanyStatusChangeRequestDataAccess.DeleteByCompanyId(int companyId)
        {
            if (companyId < 1)
            {
                throw new ArgumentNullException("companyId");
            }

            const string SP = "dbo.CompanyStatusChangeRequest_DeleteByCompanyId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a company status change request which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this company status change request.");
                        }
                }
            }
        }

        #endregion
    }
}