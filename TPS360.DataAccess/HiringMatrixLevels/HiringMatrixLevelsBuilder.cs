﻿using System.Data;
using System.Collections.Generic;
using System;
using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class HiringMatrixLevelsBuilder : IEntityBuilder<HiringMatrixLevels>
    {
        IList<HiringMatrixLevels> IEntityBuilder<HiringMatrixLevels>.BuildEntities(IDataReader reader)
        {
            List<HiringMatrixLevels> hiringMatrixLevels = new List<HiringMatrixLevels>();

            while (reader.Read())
            {
                hiringMatrixLevels.Add(((IEntityBuilder<HiringMatrixLevels>)this).BuildEntity(reader));
            }

            return (hiringMatrixLevels.Count > 0) ? hiringMatrixLevels : null;
        }

        HiringMatrixLevels IEntityBuilder<HiringMatrixLevels>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_NAME = 1;
            const int FLD_SORTORDER = 2;
            const int FLD_CREATORID = 3;
            const int FLD_UPDATORID = 4;
            const int FLD_CREATEDATE = 5;
            const int FLD_UPDATEDATE = 6;
            const int FLD_COUNT = 7;


            HiringMatrixLevels hiringMatrixLevels = new HiringMatrixLevels();

            hiringMatrixLevels.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            hiringMatrixLevels.Name = reader.IsDBNull(FLD_NAME) ? string.Empty : reader.GetString(FLD_NAME);
            hiringMatrixLevels.SortingOrder = reader.IsDBNull(FLD_SORTORDER) ? 0 : reader.GetInt32(FLD_SORTORDER);
            hiringMatrixLevels.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            hiringMatrixLevels.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            hiringMatrixLevels.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            hiringMatrixLevels.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            if(reader .FieldCount >FLD_COUNT )
            hiringMatrixLevels.Count = reader.IsDBNull(FLD_COUNT) ? 0 : reader.GetInt32(FLD_COUNT);
            return hiringMatrixLevels;
        }
    }
}