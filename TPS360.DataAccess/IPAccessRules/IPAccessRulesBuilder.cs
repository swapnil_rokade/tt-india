﻿using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class IPAccessRulesBuilder : IEntityBuilder<IPAccessRules>
    {
        IList<IPAccessRules> IEntityBuilder<IPAccessRules>.BuildEntities(IDataReader reader)
        {
            List<IPAccessRules> IPAccessRuless = new List<IPAccessRules>();

            while (reader.Read())
            {
                IPAccessRuless.Add(((IEntityBuilder<IPAccessRules>)this).BuildEntity(reader));
            }

            return (IPAccessRuless.Count > 0) ? IPAccessRuless : null;
        }

        IPAccessRules IEntityBuilder<IPAccessRules>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_MemberId = 1;
            const int FLD_ALLOWEDIP = 2;
            const int FLD_ALLOWEDIPTO = 3;
            const int FLD_MEMBERNAME = 4;

            IPAccessRules IPAccessRules = new IPAccessRules();

            IPAccessRules.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            IPAccessRules.MemberId  = reader.IsDBNull(FLD_MemberId ) ? string .Empty  : reader.GetString  (FLD_MemberId );
            IPAccessRules.AllowedIP  = reader.IsDBNull(FLD_ALLOWEDIP ) ? string.Empty : reader.GetString(FLD_ALLOWEDIP );
            IPAccessRules.AllowedIPTo = reader.IsDBNull(FLD_ALLOWEDIPTO) ? string.Empty : reader.GetString(FLD_ALLOWEDIPTO);
            if (reader.FieldCount > FLD_MEMBERNAME)
                IPAccessRules.UserName = reader.IsDBNull(FLD_MEMBERNAME) ? string.Empty : reader.GetString(FLD_MEMBERNAME);
            return IPAccessRules;
        }
    }
}