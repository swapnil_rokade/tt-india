﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TPS360.Common.BusinessEntities;
using System.Data;

namespace TPS360.DataAccess
{
    internal sealed class MailQueueBuilder : IEntityBuilder<MailQueue>
    {
        IList<MailQueue> IEntityBuilder<MailQueue>.BuildEntities(IDataReader reader)
        {
            List<MailQueue> mailqueue = new List<MailQueue>();

            while (reader.Read())
            {
                mailqueue.Add(((IEntityBuilder<MailQueue>)this).BuildEntity(reader));
            }

            return (mailqueue.Count > 0) ? mailqueue : null;
        }

        MailQueue IEntityBuilder<MailQueue>.BuildEntity(IDataReader reader) 
        {
            const int FLD_ID = 0;
            const int FLD_SENDERID = 1;
            const int FLD_RECEIVEREMAILID = 2;
            const int FLD_SUBJECT = 3;
            const int FLD_EMAILBODY = 4;
            const int FLD_BCC = 5;
            const int FLD_CC = 6;
            const int FLD_ATTACHEDFILENAMES = 7;
            const int FLD_NOOFATTACHMENTS = 8;

            MailQueue mailqueue = new MailQueue();
            mailqueue.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            mailqueue.SenderId = reader.IsDBNull(FLD_SENDERID) ? 0 : reader.GetInt32(FLD_SENDERID);
            mailqueue.ReceiverEmailId = reader.IsDBNull(FLD_RECEIVEREMAILID) ? string.Empty : reader.GetString(FLD_RECEIVEREMAILID);
            mailqueue.Subject = reader.IsDBNull(FLD_SUBJECT) ? string.Empty : reader.GetString(FLD_SUBJECT);
            mailqueue.EmailBody = reader.IsDBNull(FLD_EMAILBODY) ? string.Empty : reader.GetString(FLD_EMAILBODY);
            mailqueue.bcc = reader.IsDBNull(FLD_BCC) ? string.Empty : reader.GetString(FLD_BCC);
            mailqueue.cc = reader.IsDBNull(FLD_CC) ? string.Empty : reader.GetString(FLD_CC);
            mailqueue.AttachedFileNames = reader.IsDBNull(FLD_ATTACHEDFILENAMES) ? string.Empty : reader.GetString(FLD_ATTACHEDFILENAMES);
            mailqueue.NoOfAttachments = reader.IsDBNull(FLD_NOOFATTACHMENTS) ? 0 : reader.GetInt32(FLD_NOOFATTACHMENTS);
            
            return mailqueue;
        }

        #region IEntityBuilder<MailQueue> Members


        public MailQueue BuildEntity(IDataReader dataReader)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
