﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TPS360.Common.Shared;
using TPS360.Common.Helper;
using System.Collections;
using System.Text;
using TPS360.Common.BusinessEntities;
namespace TPS360.DataAccess
{
    internal sealed class MailQueueDataAccess : BaseDataAccess,IMailQueueDataAccess
    {
         #region Constructors

        public MailQueueDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MailQueue> CreateEntityBuilder<MailQueue>()
        {
            return (new MailQueueBuilder()) as IEntityBuilder<MailQueue>;
        }

        #endregion

        #region  Methods
        int  IMailQueueDataAccess.Add(MailQueue mailqueue) 
        {
            const string SP = "dbo.MailQueue_Create";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP)) 
            {
                Database.AddInParameter(cmd, "@SenderId", DbType.Int32, mailqueue.SenderId);
                Database.AddInParameter(cmd, "@ReceiverEmail", DbType.String, mailqueue.ReceiverEmailId);
                Database.AddInParameter(cmd, "@Subject", DbType.String, mailqueue.Subject);
                Database.AddInParameter(cmd, "@EmailBody", DbType.String, mailqueue.EmailBody);
                Database.AddInParameter(cmd, "@BCC", DbType.String, mailqueue.bcc);
                Database.AddInParameter(cmd, "@CC", DbType.String, mailqueue.cc);
                Database.AddInParameter(cmd, "@AttachedFileNames", DbType.String, mailqueue.AttachedFileNames);
                Database.AddInParameter(cmd, "@NoOfAttachments", DbType.Int32, mailqueue.NoOfAttachments);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                    }
                    return 0;
                }
                return 0;
            }
        
        }

        IList<MailQueue> IMailQueueDataAccess.GetAllMailInQueue()
        {
            const string SP = "dbo.MailQueue_GetAll";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MailQueue>().BuildEntities(reader);
                }
            }

        }

        bool IMailQueueDataAccess.DeleteMailQueueById(int Id,out int MemberEmailId)
        {
            if (Id < 1)
            {
                throw new ArgumentNullException("Id");
            }
            int tempMemberEmailId = 0;
            const string SP = "dbo.MailQueue_DeleteById";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, Id);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {

                        tempMemberEmailId = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                    }

                }
                
                MemberEmailId = tempMemberEmailId;
                
                int returncode = GetReturnCodeFromParameter(cmd);

                switch (returncode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member email.");
                        }


                }

                
            }

        }
        void IMailQueueDataAccess.UpdateFileNamesInMailQueue(string fileNames, int MailQueueId,int NoOfAttachments)
        {
            const string SP = "dbo.MailQueue_UpdateFilenamesInMailQueue";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP)) 
            {
                Database.AddInParameter(cmd, "@FileNames", DbType.String, fileNames);
                Database.AddInParameter(cmd, "@MailQueueId", DbType.Int32, MailQueueId);
                Database.AddInParameter(cmd, "@NoOfAttachments", DbType.Int32, NoOfAttachments);
                Database.ExecuteNonQuery(cmd);
            }
        }
        void IMailQueueDataAccess.UpdateMailbodyForInterviewTemplate(string EmailBody, int MemberEmailId) 
        {
            const string SP = "dbo.MailQueue_UpdateEmailBodyForInterviewMailTemplate";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@EmailBody", DbType.String, EmailBody);
                Database.AddInParameter(cmd, "@MemberEmailId", DbType.Int32, MemberEmailId);
            
                Database.ExecuteNonQuery(cmd);
            }
        }
        #endregion
    }
}
