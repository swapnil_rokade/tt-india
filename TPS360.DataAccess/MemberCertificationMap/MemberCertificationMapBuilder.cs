﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberCertificationMapBuilder : IEntityBuilder<MemberCertificationMap>
    {
        IList<MemberCertificationMap> IEntityBuilder<MemberCertificationMap>.BuildEntities(IDataReader reader)
        {
            List<MemberCertificationMap> memberCertificationMaps = new List<MemberCertificationMap>();

            while (reader.Read())
            {
                memberCertificationMaps.Add(((IEntityBuilder<MemberCertificationMap>)this).BuildEntity(reader));
            }

            return (memberCertificationMaps.Count > 0) ? memberCertificationMaps : null;
        }

        MemberCertificationMap IEntityBuilder<MemberCertificationMap>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_CERTIFICATIONNAME = 1;
            const int FLD_VALIDFROM = 2;
            const int FLD_VALIDTO = 3;
            const int FLD_ISSUINGATHORITY = 4;
            const int FLD_ISREMOVED = 5;
            const int FLD_MEMBERID = 6;
            const int FLD_CREATORID = 7;
            const int FLD_UPDATORID = 8;
            const int FLD_CREATEDATE = 9;
            const int FLD_UPDATEDATE = 10;

            MemberCertificationMap memberCertificationMap = new MemberCertificationMap();

            memberCertificationMap.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberCertificationMap.CerttificationName  = reader.IsDBNull(FLD_CERTIFICATIONNAME ) ? string.Empty  : reader.GetString (FLD_CERTIFICATIONNAME );
            memberCertificationMap.ValidFrom  = reader.IsDBNull(FLD_VALIDFROM ) ? DateTime .MinValue   : reader.GetDateTime   (FLD_VALIDFROM );
            memberCertificationMap.ValidTo  = reader.IsDBNull(FLD_VALIDTO ) ? DateTime .MinValue   : reader.GetDateTime   (FLD_VALIDTO );
            memberCertificationMap.IssuingAthority  = reader.IsDBNull(FLD_ISSUINGATHORITY ) ? string.Empty : reader.GetString(FLD_ISSUINGATHORITY );
            memberCertificationMap.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberCertificationMap.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberCertificationMap.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberCertificationMap.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberCertificationMap.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberCertificationMap.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            return memberCertificationMap;
        }
    }
}