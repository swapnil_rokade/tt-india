using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Data.SqlClient;
using System.Collections;

public static class DailyReport
{
    private static readonly string strEmployeeId = String.Empty;
    //public DailyReport(string p_strEmployeeId)
    //{
    //    strEmployeeId = p_strEmployeeId;
    //}
    private static readonly Hashtable objCandidate = new Hashtable();
    private static readonly Hashtable objConsultant = new Hashtable();
    private static readonly Hashtable objCompany = new Hashtable();
    private static readonly Hashtable objLeads = new Hashtable();
    private static readonly Hashtable objRequisition = new Hashtable();
    private static readonly Hashtable objEmployee = new Hashtable();
    public static string GetReport(string strEmployeeId,DateTime dtReport)
    {
        string strMessage = String.Empty;
        try
        {
            using (SqlConnection objSqlCon = new SqlConnection("context connection=true;"))
            {
                //strMessage = "Trying to connect";
                objSqlCon.Open();
                //strMessage += "Connection is opened";
                
                StringBuilder strBody = new StringBuilder();
                strBody.Append(@"<table cellpadding=0 cellspacing=0 border=0 width=985>");
                strBody.Append("<tr>");
                strBody.Append("<td class=JSBorderLine>");
                GetEmployeeInformation(ref strBody, strEmployeeId, objSqlCon);
                strBody.Append("</td>");
                strBody.Append("</tr>");

                string strActivityReport = LoadActivities(objSqlCon, strEmployeeId, dtReport);

                strBody.Append("<tr>");
                strBody.Append("<td class=JSBorderLine>");
                LoadEmployeeWorkStatus(ref strBody);
                strBody.Append("</td>");
                strBody.Append("</tr>");

                strBody.Append("<tr>");
                strBody.Append("<td class=JSBorderLine>");
                LoadOpenTaskList(ref strBody, strEmployeeId, objSqlCon, dtReport);
                strBody.Append("</td>");
                strBody.Append("</tr>");

                strBody.Append("<tr>");
                strBody.Append("<td class=JSBorderLine>");
                LoadCompletedTaskList(ref strBody, strEmployeeId, objSqlCon, dtReport);
                strBody.Append("</td>");
                strBody.Append("</tr>");

                strBody.Append("<tr>");
                strBody.Append("<td class=JSBorderLine>");
                strBody.Append(strActivityReport);
                strBody.Append("</td>");
                strBody.Append("</tr>");

                strBody.Append("<tr>");
                strBody.Append("<td class=JSBorderLine>");
                LoadEmployeeTimeSheet(ref strBody, strEmployeeId, objSqlCon, dtReport);
                strBody.Append("</td>");
                strBody.Append("</tr>");
                strBody.Append("</table>");

                return strBody.ToString();
            }
        }
        catch (Exception ex)
        {
            return strMessage+ex.Message;
        }

    }

    private static void GetEmployeeInformation(ref StringBuilder strReport, string strEmployeeId, SqlConnection objSqlCon)
    {
        try
        {
            string strSqlQuery = @"select EI.varchrFirstName,EI.varchrLastName,EW.varchrPosition,EI.varchrCity,EI.chrState
        from Employee_Info EI left join Employee_WorkDetails EW on EI.Id=EW.Employee_Info_Id where EI.ReferenceUserID='" + strEmployeeId + "'";
            SqlCommand objSqlCmd = new SqlCommand(strSqlQuery, objSqlCon);
            SqlDataReader objSqlDR = objSqlCmd.ExecuteReader();
            if (objSqlDR.Read())
            {
                strReport.Append("<table width='100%' border='0' cellspacing='1' cellpadding='5'>");
                strReport.Append("<tr>");
                strReport.Append("<td align='left' class='JSFormHeading'>Name:</td>");
                strReport.Append("<td align='left' class='NewFormValue'>");
                strReport.Append(objSqlDR["varchrFirstName"].ToString() + " " + objSqlDR["varchrLastName"].ToString());
                strReport.Append("</td>");
                strReport.Append("<td align='left' class='JSFormHeading'>Job Title:</td>");
                strReport.Append("<td align='left' class='NewFormValue'>");
                strReport.Append(objSqlDR["varchrPosition"].ToString());
                strReport.Append("</td>");
                strReport.Append("<td align='left' class='JSFormHeading'>Location:</td>");
                strReport.Append("<td align='left' class='NewFormValue'>");
                strReport.Append(GetCurrentLocation(objSqlDR["varchrCity"].ToString(), objSqlDR["chrState"].ToString()));
                strReport.Append("</td>");
                strReport.Append("</tr>");
                strReport.Append("</table>");
            }
            objSqlDR.Close();
        }
        catch { }

    }

    private static string GetCurrentLocation(string strCity, string strState)
    {
        string strCityState = "";

        if (strCity != "")
        {
            if (strState != "")
            {
                strCityState = strCity + ", " + strState;
            }
            else
            {
                strCityState = strCity;
            }
        }
        else
        {
            strCityState = strState;
        }
        return strCityState;
    }

    private static void LoadEmployeeWorkStatus(ref StringBuilder strReport)
    {
        strReport.Append(@"<table width='100%' border='0' cellspacing='1' cellpadding='5'>
                        <tr>
                            <td colspan='6' align='left' class='JSFormHeading'>Work Status</td>
                        </tr>
                        <tr>
                            <td colspan='6' height='5' bgcolor='#ffcc00'>
                                <spacer height=1 width=1 type=block>
                            </td>
                        </tr>
                        <tr>
                            <td class='JSFormHeading' style='background-color: #ff9900; text-align: center'>Candidates</td>
                            <td class='JSFormHeading' style='background-color: #cc99ff; text-align: center'>Consultants</td>
                            <td class='JSFormHeading' style='background-color: #808000; text-align: center'>Company</td>
                            <td class='JSFormHeading' style='background-color: #666699; text-align: center'>Leads</td>
                            <td class='JSFormHeading' style='background-color: #ff9900; text-align: center'>Requisition</td>
                            <td class='JSFormHeading' style='background-color: #cc99ff; text-align: center'>Employee</td>
                        </tr>");

        strReport.Append("<tr>");
        strReport.Append("<td class='NewFormValue' align='center'>");
        strReport.Append(objCandidate.Count.ToString());
        strReport.Append("</td>");
        strReport.Append("<td class='NewFormValue' align='center'>");
        strReport.Append(objConsultant.Count.ToString());
        strReport.Append("</td>");
        strReport.Append("<td class='NewFormValue' align='center'>");
        strReport.Append(objCompany.Count.ToString());
        strReport.Append("</td>");
        strReport.Append("<td class='NewFormValue' align='center'>");
        strReport.Append(objLeads.Count.ToString());
        strReport.Append("</td>");
        strReport.Append("<td class='NewFormValue' align='center'>");
        strReport.Append(objRequisition.Count.ToString());
        strReport.Append("</td>");
        strReport.Append("<td class='NewFormValue' align='center'>");
        strReport.Append(objEmployee.Count.ToString());
        strReport.Append("</td>");
        strReport.Append("</tr>");
        strReport.Append("</table>");
    }

    private static void LoadOpenTaskList(ref StringBuilder strReport, string strEmployeeId, SqlConnection objSqlCon, DateTime dtReport)
    {
        strReport.Append(@"<table width=100% border=0 cellspacing=1 cellpadding=5>
                        <tr>
                            <td colspan=4 align=left class=JSFormHeading>Open Tasks</td>
                        </tr>
                        <tr>
                            <td colspan='4' height='5' bgcolor='#ffcc00'>
                                <spacer height=1 width=1 type=block>
                            </td>
                        </tr>");
        string strSqlQuery = "select * from Employee_ToDo where varchrStatus ='Open' and varchrAssignTo='" + strEmployeeId + "'";
        SqlCommand objSqlCmd = new SqlCommand(strSqlQuery, objSqlCon);
        SqlDataReader objSqlDR = objSqlCmd.ExecuteReader();
        int intCount = 0;
        while (objSqlDR.Read())
        {
            if (intCount == 0)
            {
                strReport.Append(@"<tr>
                                       <td align=left class=JSFormHeading>Subject</td>
                                       <td align=left class=JSFormHeading>Description</td>
                                       <td align=left class=JSFormHeading>Start Date</td>
                                       <td align=left class=JSFormHeading>End Date</td>
                                   </tr>");
            }
            intCount++;
            strReport.Append("<tr>");
            strReport.Append("<td align=left class=NewFormValue>");
            strReport.Append(objSqlDR["varchrSubject"].ToString());
            strReport.Append("</td>");
            strReport.Append("<td align=left class=NewFormValue>");
            strReport.Append(objSqlDR["varchrDescription"].ToString());
            strReport.Append("</td>");
            strReport.Append("<td align=left class=NewFormValue>");
            strReport.Append(CheckNullDate(Convert.ToDateTime(objSqlDR["dttmEndDate"].ToString()).ToShortDateString()));
            strReport.Append("</td>");
            strReport.Append("<td align=left class=NewFormValue>");
            strReport.Append(CheckNullDate(Convert.ToDateTime(objSqlDR["dttmEndDate"].ToString()).ToShortDateString()));
            strReport.Append("</td>");
            strReport.Append("</tr>");
        }
        objSqlDR.Close();
        if (intCount == 0)
        {
            strReport.Append(@"<tr>
                               <td colspan='4' align='left' class='NewFormValue' style='color:red;'>No Open Task</td>
                               </tr>");
        }
        strReport.Append("</table>");
    }

    private static void LoadCompletedTaskList(ref StringBuilder strReport, string strEmployeeId, SqlConnection objSqlCon,DateTime dtReport)
    {
        strReport.Append(@"<table width=100% border=0 cellspacing=1 cellpadding=5>
                        <tr>
                            <td colspan=4 align=left class=JSFormHeading>Completed Tasks</td>
                        </tr>
                        <tr>
                            <td colspan='4' height='5' bgcolor='#ffcc00'>
                                <spacer height=1 width=1 type=block>
                            </td>
                        </tr>");
        string strSqlQuery = "select * from Employee_ToDo where varchrStatus ='Completed' and varchrAssignTo='" + strEmployeeId + "' and [dttmCompleteDate]>='" + dtReport.ToShortDateString()+"'";
        SqlCommand objSqlCmd = new SqlCommand(strSqlQuery, objSqlCon);
        SqlDataReader objSqlDR = objSqlCmd.ExecuteReader();
        int intCount = 0;
        while (objSqlDR.Read())
        {
            if (intCount == 0)
            {
                strReport.Append(@"<tr>
                                       <td align=left class=JSFormHeading>Subject</td>
                                       <td align=left class=JSFormHeading>Description</td>
                                       <td align=left class=JSFormHeading>Start Date</td>
                                       <td align=left class=JSFormHeading>End Date</td>
                                   </tr>");
            }
            intCount++;
            strReport.Append("<tr>");
            strReport.Append("<td align=left class=NewFormValue>");
            strReport.Append(objSqlDR["varchrSubject"].ToString());
            strReport.Append("</td>");
            strReport.Append("<td align=left class=NewFormValue>");
            strReport.Append(objSqlDR["varchrDescription"].ToString());
            strReport.Append("</td>");
            strReport.Append("<td align=left class=NewFormValue>");
            strReport.Append(CheckNullDate(Convert.ToDateTime(objSqlDR["dttmEndDate"].ToString()).ToShortDateString()));
            strReport.Append("</td>");
            strReport.Append("<td align=left class=NewFormValue>");
            strReport.Append(CheckNullDate(Convert.ToDateTime(objSqlDR["dttmEndDate"].ToString()).ToShortDateString()));
            strReport.Append("</td>");
            strReport.Append("</tr>");
        }
        objSqlDR.Close();
        if (intCount == 0)
        {
            strReport.Append(@"<tr>
                               <td colspan='4' align='left' class='NewFormValue' style='color:red;'>No Completed Task</td>
                               </tr>");
        }
        strReport.Append("</table>");
    }

    private static void LoadEmployeeTimeSheet(ref StringBuilder strReport, string strEmployeeId, SqlConnection objSqlCon, DateTime dtReport)
    {
        try
        {
            strReport.Append(@"<table width=100% border=0 cellspacing=1 cellpadding=5>
                        <tr>
                            <td colspan='3' width='100%' align='left' class='JSFormHeading'>");
            GetTimeSheetHeader(ref strReport,objSqlCon, strEmployeeId, dtReport);
            strReport.Append(@"</td>
                        </tr>
                        <tr>
                            <td colspan='3' height='5' bgcolor='#ffcc00'>
                                <spacer height=1 width=1 type=block>
                            </td>
                        </tr>");
            string strSqlQuery = @"SELECT convert(varchar,[dttmLoginTime],120) as dttmLoginTime
            ,case convert(varchar,[dttmLogoutTime],101) when '01/01/1800' then '' else convert(varchar,[dttmLogoutTime],120) end as dttmLogoutTime
            ,[varchrWorkDetails]
            ,[dttmDateEntered]
            ,[Employee_Info_Id]
            ,[Id]
            FROM [TimeSheet]
            where Employee_Info_Id = '" + strEmployeeId + "' and dttmDateEntered >='" + dtReport.ToShortDateString() + "' and [varchrWorkDetails]!='' order by dttmLoginTime desc";
            SqlCommand objSqlCmd = new SqlCommand(strSqlQuery, objSqlCon);
            SqlDataReader objSqlDR = objSqlCmd.ExecuteReader();
            int intCount = 0;
            while (objSqlDR.Read())
            {
                if (intCount == 0)
                {
                    strReport.Append(@"<tr>
                                       <td width='20%' align='left' class='JSFormField'>Login Time</td>
                                       <td width='20%' align='left' class='JSFormField'>Logout Time</td>
                                       <td width='60%' align='left' class='JSFormField'>Report</td>
                                   </tr>");
                }
                intCount++;
                strReport.Append("<tr>");
                strReport.Append("<td align=left class=NewFormValue>");
                strReport.Append(objSqlDR["dttmLoginTime"].ToString());
                strReport.Append("</td>");
                strReport.Append("<td align=left class=NewFormValue>");
                strReport.Append(objSqlDR["dttmLogoutTime"].ToString());
                strReport.Append("</td>");
                strReport.Append("<td align=left class=NewFormValue>");
                strReport.Append(objSqlDR["varchrWorkDetails"].ToString());
                strReport.Append("</td>");
                strReport.Append("</tr>");
            }
            if (intCount == 0)
            {
                strReport.Append(@"<tr>
                               <td colspan='3' align='left' class='NewFormValue' style='color:red;'>No Daily Report Submitted</td>
                               </tr>");
            }
            strReport.Append("</table>");
            objSqlDR.Close();
        }
        catch { }
    }

    private static void GetTimeSheetHeader(ref StringBuilder strReport,SqlConnection objSqlCon,string strEmployeeId,DateTime dtReport)
    {
        try
        {
            strReport.Append(@"<table width=100% border=0 cellspacing=0 cellpadding=0>
                               <tr class='JSFormHeading'>
                               <td width='20%' align='left' style='color:white;'>Submitted Daily Report</td>");
            strReport.Append("<td width='20%' align='right' style='color:white;'>First Login :&nbsp;</td>");
            strReport.Append("<td width='20%' align='left' style='color:white;'>");

            string strLoginTime = GetFirstLoginTime(dtReport, objSqlCon, strEmployeeId);
            strReport.Append(strLoginTime);
            strReport.Append("</td>");
            strReport.Append("<td width='20%' align='right' style='color:white;'>Last Logout :&nbsp;</td>");

            strReport.Append("<td width='20%' align='left' style='color:white;'>");
            string strLogoutTime = GetLastLogoutTime(dtReport, objSqlCon, strEmployeeId);
            strReport.Append(strLogoutTime);
            strReport.Append("</td>");

            strReport.Append("</tr></table>");
        }
        catch { }
    }

    private static string GetFirstLoginTime(DateTime dt, SqlConnection objSqlCon,string strEmployeeId)
    {
        //LogIn time
        //string strQuery = "SELECT TOP 1 dttmActivityDate FROM [UserHistory] WHERE intOwnerId='" + strEmployeeId + "' AND varchrActivity='Logged In' AND CONVERT(VARCHAR,dttmActivityDate,101)= '" + String.Format("{0:MM/dd/yyyy}", dt) + "' order by dttmActivityDate";
        string strQuery = @"SELECT TOP 1 convert(varchar,[dttmLoginTime],120) as FirstLoginTime
                            FROM [TimeSheet]
                            WHERE Employee_Info_Id='" + strEmployeeId + @"' AND
                            dttmDateEntered >='"+dt.ToShortDateString()+@"'
                            ORDER BY [dttmLoginTime]";
        return Reports.GetSingleValue(strQuery, objSqlCon);
    }

    private static string LoadActivities(SqlConnection objSqlCon, string strEmployeeId, DateTime dtReport)
    {
        StringBuilder strReport = new StringBuilder();
        try
        {
            strReport.Append(@"<table width='100%' border='0' cellspacing='1' cellpadding='5'>
                        <tr>
                            <td colspan='4' align='left' class='JSFormHeading'>Employee Activity Report</td>
                        </tr>
                        <tr>
                            <td colspan='4' height='5' bgcolor='#ffcc00'>
                                <spacer height=1 width=1 type=block>
                            </td>
                        </tr>
                        <tr>
                            <td class='JSFormHeading' style='text-align: left'>Time(EST)</td>
                            <td class='JSFormHeading' style='text-align: left'>Type</td>
                            <td class='JSFormHeading' style='text-align: left'>Name</td>
                            <td class='JSFormHeading' style='text-align: left'>Description</td>
                        </tr>");
            string query = @"SELECT * 
                             FROM [UserHistory] 
                             WHERE intOwnerId='" + strEmployeeId + "' AND CONVERT(VARCHAR,dttmActivityDate,110)= '" + GetDBDateFormat(dtReport) +
                            "' order by dttmActivityDate DESC";
            SqlCommand SqlCmd = new SqlCommand(query, objSqlCon);
            SqlDataAdapter ad = new SqlDataAdapter(SqlCmd);
            DataTable dtActivity = new DataTable();
            ad.Fill(dtActivity);
            if (dtActivity.Rows.Count > 0)
            {
                foreach (DataRow dtRow in dtActivity.Rows)
                {
                    strReport.Append("<tr>");
                    strReport.Append("<td align=left class=NewFormValue>");
                    strReport.Append(GetShortTime(dtRow["dttmActivityDate"].ToString()));
                    strReport.Append("</td>");
                    strReport.Append("<td align=left class=NewFormValue>");
                    string strUserName = String.Empty;
                    strReport.Append(GetUserTypeName(dtRow["intUserId"].ToString(), objSqlCon, ref strUserName));
                    strReport.Append("</td>");
                    strReport.Append("<td align=left class=NewFormValue>");
                    strReport.Append(strUserName);
                    strReport.Append("</td>");
                    strReport.Append("<td align=left class=NewFormValue>");
                    strReport.Append(dtRow["varchrActivity"].ToString());
                    strReport.Append("</td>");
                    strReport.Append("</tr>");
                }
            }
            else
            {
                strReport.Append("<tr>");
                strReport.Append("<td align=left class=NewFormValue colspan='4'>");
                strReport.Append("<font color='red'>No Activity for Today</font>");
                strReport.Append("</td>");
                strReport.Append("</tr>");
            }
            strReport.Append("</table>");
        }
        catch (Exception ex)
        {
            strReport.Append("<font color='red'>Error:"+ex.Message+"</font>");
        }

        return strReport.ToString();
    }

    private static string GetDBDateFormat(DateTime dt)
    {
        string strRetval = string.Empty;
        if (dt.Month.ToString().Length > 1)
            strRetval = dt.Month.ToString();
        else
            strRetval = "0" + dt.Month.ToString();

        if (dt.Day.ToString().Length > 1)
            strRetval += "-" + dt.Day.ToString();
        else
            strRetval += "-0" + dt.Day.ToString();

        return strRetval + "-" + dt.Year.ToString();

    }

    private static string GetShortTime(string strDateTime)
    {
        try
        {
            DateTime dt = DateTime.Parse(strDateTime.Trim());
            return dt.ToShortTimeString();
        }
        catch
        {
            return strDateTime;
        }
    }

    private static string GetUserTypeName(string strUserId, SqlConnection objSqlCon, ref string strUserName)
    {
        SqlCommand objSqlCmd=null;
        SqlDataReader objSqlDR=null;
        try
        {
            string strSqlQuery = "select varchrFirstName,varchrLastName,UA.smlintGroupID,UA.Id from dbo.Employee_Info EI left join dbo.UserAuthorization UA on EI.ReferenceUserID=UA.Id Where UA.Id = '" + strUserId + "'";
            objSqlCmd = new SqlCommand(strSqlQuery, objSqlCon);
            objSqlDR = objSqlCmd.ExecuteReader();
            if (objSqlDR.Read())
            {
                strUserName = objSqlDR["varchrFirstName"].ToString();
                if (objSqlDR["varchrLastName"].ToString() != String.Empty)
                {
                    strUserName = strUserName + " " + objSqlDR["varchrLastName"].ToString();
                }
                if (objSqlDR["smlintGroupID"].ToString() == "22")
                {
                    if (!objConsultant.Contains(objSqlDR["Id"].ToString()))
                    {
                        objConsultant.Add(objSqlDR["Id"].ToString(), objSqlDR["Id"].ToString());
                    }
                    return "Consultant";
                }
                else
                {
                    if (!objEmployee.Contains(objSqlDR["Id"].ToString()))
                    {
                        objEmployee.Add(objSqlDR["Id"].ToString(), objSqlDR["Id"].ToString());
                    }
                    return "Employee";
                }
            }

            objSqlDR.Close();

            strSqlQuery = "select varchrFirstName,varchrLastName,Id from dbo.JobSeekerInfo Where Id = '" + strUserId + "'";
            objSqlCmd.CommandText = strSqlQuery; ;
            objSqlDR = objSqlCmd.ExecuteReader();
            if (objSqlDR.Read())
            {
                strUserName = objSqlDR["varchrFirstName"].ToString();
                if (objSqlDR["varchrLastName"].ToString() != String.Empty)
                {
                    strUserName = strUserName + " " + objSqlDR["varchrLastName"].ToString();
                }

                if (!objCandidate.Contains(objSqlDR["Id"].ToString()))
                {
                    objCandidate.Add(objSqlDR["Id"].ToString(), objSqlDR["Id"].ToString());
                }
                return "Candidate";
            }

            objSqlDR.Close();

            strSqlQuery = "select varchrPositionID,Id from dbo.JobPosting Where Id = '" + strUserId + "'";
            objSqlCmd.CommandText = strSqlQuery; ;
            objSqlDR = objSqlCmd.ExecuteReader();
            if (objSqlDR.Read())
            {
                strUserName = objSqlDR["varchrPositionID"].ToString();

                if (!objRequisition.Contains(objSqlDR["Id"].ToString()))
                {
                    objRequisition.Add(objSqlDR["Id"].ToString(), objSqlDR["Id"].ToString());
                }
                return "Requisition";
            }

            objSqlDR.Close();

            strSqlQuery = "select varchrLeadDescription,Id from dbo.Company_Leads Where Id = '" + strUserId + "'";
            objSqlCmd.CommandText = strSqlQuery; ;
            objSqlDR = objSqlCmd.ExecuteReader();
            if (objSqlDR.Read())
            {
                strUserName = objSqlDR["varchrLeadDescription"].ToString();

                if (!objLeads.Contains(objSqlDR["Id"].ToString()))
                {
                    objLeads.Add(objSqlDR["Id"].ToString(), objSqlDR["Id"].ToString());
                }
                return "Lead";
            }
            objSqlDR.Close();

            //strQuery = "select varchrCompanyName from dbo.VendorInfo Where Id = '" + strUserId + "'";
            //objSqlCmd.CommandText = strQuery; ;
            //objSqlDR = objSqlCmd.ExecuteReader();
            //if (objSqlDR.Read())
            //{
            //    strUserName = objSqlDR["varchrCompanyName"].ToString();
            //    //obj
            //    if (!objLeads.Contains(objSqlDR["Id"].ToString()))
            //    {
            //        objLeads.Add(objSqlDR["Id"].ToString(), objSqlDR["Id"].ToString());
            //    }
            //    return "Supplier";
            //}

            //objSqlDR.Close();

            strSqlQuery = "select varchrCompanyName,bitProspective,Id from dbo.ClientInfo Where Id = '" + strUserId + "'";
            objSqlCmd.CommandText = strSqlQuery; ;
            objSqlDR = objSqlCmd.ExecuteReader();
            if (objSqlDR.Read())
            {
                strUserName = objSqlDR["varchrCompanyName"].ToString();

                if (!objCompany.Contains(objSqlDR["Id"].ToString()))
                {
                    objCompany.Add(objSqlDR["Id"].ToString(), objSqlDR["Id"].ToString());
                }
                if (objSqlDR["bitProspective"].ToString().ToLower() == "true")
                {
                    return "Prospective Client";
                }
                else
                {
                    return "Client";
                }
            }

        }
        catch { }
        finally
        {
            if (objSqlDR != null)
            {
                objSqlDR.Close();
                objSqlDR.Dispose();
            }

            if (objSqlCmd != null)
            {
                objSqlCmd.Dispose();
            }
        }
        return String.Empty;
    }

    private static string GetLastLogoutTime(DateTime dt, SqlConnection objSqlCon, string strEmployeeId)
    {
        //string strQuery = "SELECT TOP 1 dttmActivityDate FROM [UserHistory] WHERE intOwnerId='" + strEmployeeId + "' AND varchrActivity='Log Out' AND CONVERT(VARCHAR,dttmActivityDate,101)= '" + String.Format("{0:MM/dd/yyyy}", dt) + "' order by dttmActivityDate DESC";
        string strQuery = @"SELECT TOP 1 convert(varchar,[dttmLogoutTime],120) as LastLogoutTime
                            FROM [TimeSheet]
                            WHERE Employee_Info_Id='" + strEmployeeId + @"' AND
                            dttmDateEntered >='" + dt.ToShortDateString() + @"'
                            ORDER BY [dttmLogoutTime] DESC";
        return Reports.GetSingleValue(strQuery, objSqlCon);
        //if (strDt != string.Empty)
        //{
        //    DateTime dtLogIn = DateTime.Parse(strDt);
        //    return dtLogIn.ToShortTimeString();
        //}
        //return strDt;
    }

    private static string CheckNullDate(string DateField)
    {
        string strReturnDate = string.Empty;
        if (DateField != "1/1/1800")
            strReturnDate = DateField;
        return strReturnDate;
    }

    //public static string Body
    //{
    //    get { return strBody.ToString(); }
    //}
}

