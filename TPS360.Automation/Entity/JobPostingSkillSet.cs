﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Automation
{
    internal sealed class JobPostingSkillSet : BaseEntity
    {
        #region Properties

        public int JobPostingId
        {
            get;
            set;
        }

        public int SkillId
        {
            get;
            set;
        }

        public int ProficiencyLookupId
        {
            get;
            set;
        }

        public int NoOfYearsExp
        {
            get;
            set;
        }

        public bool AssessmentRequired
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public JobPostingSkillSet()
            : base()
        {
        }

        #endregion
    }
}