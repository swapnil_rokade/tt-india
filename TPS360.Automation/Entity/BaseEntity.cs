using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

namespace TPS360.Automation
{
    [Serializable()]
    internal abstract class BaseEntity : ICloneable
    {
        [XmlIgnore()]
        public virtual int Id
        {
            get;
            set;
        }

        [XmlIgnore()]
        public int CreatorId
        {
            get;
            set;
        }

        [XmlIgnore()]
        public int UpdatorId
        {
            get;
            set;
        }

        [XmlIgnore()]
        public virtual bool IsNew
        {
            [DebuggerStepThrough()]
            get
            {
                return (Id <= 0);
            }
        }

        [XmlIgnore()]
        public DateTime CreateDate
        {
            get;
            set;
        }

        [XmlIgnore()]
        public DateTime UpdateDate
        {
            get;
            set;
        }

        protected BaseEntity()
        {
            CreateDate = UpdateDate = DateTime.Now;
        }

        object ICloneable.Clone()
        {
            return Clone();
        }

        public virtual BaseEntity Clone()
        {
            using (MemoryStream ms = new MemoryStream())
            {
                (new BinaryFormatter()).Serialize(ms, this);

                ms.Seek(0, SeekOrigin.Begin);

                return (new BinaryFormatter()).Deserialize(ms) as BaseEntity;
            }
        }
    }
}