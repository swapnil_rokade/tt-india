﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Automation
{
    internal sealed class GenericLookup : BaseEntity
    {
        #region Properties

        public string Name
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public int Type
        {
            get;
            set;
        }

        public int SortOrder
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public GenericLookup()
            : base()
        {
        }

        #endregion
    }
}