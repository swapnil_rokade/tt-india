﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Automation
{
    internal sealed class Skill : BaseEntity
    {
        #region Properties
        
        public string Name
        {
            get;
            set;
        }
        
        public string Description
        {
            get;
            set;
        }
        
        public bool IsRemoved
        {
            get;
            set;
        }
        
        public int ParentId
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public Skill()
            : base()
        {
        }

        #endregion
    }
}
