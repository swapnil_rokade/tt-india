﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;


namespace TPS360.Automation
{
    internal sealed class SkillBuilder : IEntityBuilder<Skill>
    {
        IList<Skill> IEntityBuilder<Skill>.BuildEntities(SqlDataReader reader)
        {
            List<Skill> skills = new List<Skill>();

            while (reader.Read())
            {
                skills.Add(((IEntityBuilder<Skill>)this).BuildEntity(reader));
            }

            return (skills.Count > 0) ? skills : null;
        }

        Skill IEntityBuilder<Skill>.BuildEntity(SqlDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_NAME = 1;
            const int FLD_DESCRIPTION = 2;
            const int FLD_ISREMOVED = 3;
            const int FLD_PARENTID = 4;
            const int FLD_CREATORID = 5;
            const int FLD_UPDATORID = 6;
            const int FLD_CREATEDATE = 7;
            const int FLD_UPDATEDATE = 8;

            Skill skill = new Skill();

            skill.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            skill.Name = reader.IsDBNull(FLD_NAME) ? string.Empty : reader.GetString(FLD_NAME);
            skill.Description = reader.IsDBNull(FLD_DESCRIPTION) ? string.Empty : reader.GetString(FLD_DESCRIPTION);
            skill.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            skill.ParentId = reader.IsDBNull(FLD_PARENTID) ? 0 : reader.GetInt32(FLD_PARENTID);
            skill.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            skill.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            skill.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            skill.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return skill;
        }
    }
}
