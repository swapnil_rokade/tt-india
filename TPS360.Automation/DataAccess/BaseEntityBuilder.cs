using System.Collections.Generic;
using System.Runtime.Remoting.Contexts;

namespace TPS360.Automation
{
    internal abstract class BaseEntityBuilder
    {
        #region Instance Variables

        private Context _context = new Context();
        
        #endregion

        #region Properties

        #endregion

        #region Constructer & Destructer

        protected BaseEntityBuilder()
        {
           
        }        

        #endregion

        #region Protected Methods

        #endregion
    }
}