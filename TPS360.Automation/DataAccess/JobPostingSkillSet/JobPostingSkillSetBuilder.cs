﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;


namespace TPS360.Automation
{
    internal sealed class JobPostingSkillSetBuilder : IEntityBuilder<JobPostingSkillSet>
    {
        IList<JobPostingSkillSet> IEntityBuilder<JobPostingSkillSet>.BuildEntities(SqlDataReader reader)
        {
            List<JobPostingSkillSet> jobPostingSkillSets = new List<JobPostingSkillSet>();

            while (reader.Read())
            {
                jobPostingSkillSets.Add(((IEntityBuilder<JobPostingSkillSet>)this).BuildEntity(reader));
            }

            return (jobPostingSkillSets.Count > 0) ? jobPostingSkillSets : null;
        }

        JobPostingSkillSet IEntityBuilder<JobPostingSkillSet>.BuildEntity(SqlDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_JOBPOSTINGID = 1;
            const int FLD_SKILLID = 2;
            const int FLD_PROFICIENCYLOOKUPID = 3;
            const int FLD_NOOFYEARSEXP = 4;
            const int FLD_ASSESSMENTREQUIRED = 5;
            const int FLD_CREATEDATE = 6;
            const int FLD_UPDATEDATE = 7;

            JobPostingSkillSet jobPostingSkillSet = new JobPostingSkillSet();

            jobPostingSkillSet.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            jobPostingSkillSet.JobPostingId = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);
            jobPostingSkillSet.SkillId = reader.IsDBNull(FLD_SKILLID) ? 0 : reader.GetInt32(FLD_SKILLID);
            jobPostingSkillSet.ProficiencyLookupId = reader.IsDBNull(FLD_PROFICIENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_PROFICIENCYLOOKUPID);
            jobPostingSkillSet.NoOfYearsExp = reader.IsDBNull(FLD_NOOFYEARSEXP) ? 0 : reader.GetInt32(FLD_NOOFYEARSEXP);
            jobPostingSkillSet.AssessmentRequired = reader.IsDBNull(FLD_ASSESSMENTREQUIRED) ? false : reader.GetBoolean(FLD_ASSESSMENTREQUIRED);
            jobPostingSkillSet.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            jobPostingSkillSet.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return jobPostingSkillSet;
        }
    }
}
