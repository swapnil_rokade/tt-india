﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;

using System.Data.SqlClient;

namespace TPS360.Automation
{
    internal sealed class JobPostingGeneralQuestionDataAccess:BaseDataAccess
    {
        #region Constructors

        public JobPostingGeneralQuestionDataAccess()
        {
        }

        protected override IEntityBuilder<JobPostingGeneralQuestion> CreateEntityBuilder<JobPostingGeneralQuestion>()
        {
            return (new JobPostingGeneralQuestionBuilder()) as IEntityBuilder<JobPostingGeneralQuestion>;
        }

        #endregion

        #region Method
        public IList<JobPostingGeneralQuestion> GetAllByJobPostingId(int jobPostingId, string questionType)
        {
            if (jobPostingId < 1)
            {
                throw new ArgumentNullException("jobPostingId");
            }

            const string SP = "JobPostingGeneralQuestion_GetJobPostingGeneralQuestionByJobPostingId";

            using (SqlConnection objSqlCon = new SqlConnection("context connection=true;"))
            {
                objSqlCon.Open();
                using (SqlCommand cmd = new SqlCommand(SP, objSqlCon))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@jobPostingId", SqlDbType.Int);
                    cmd.Parameters["@jobPostingId"].Value = jobPostingId;                   
                    cmd.Parameters.Add("@QuestionType",SqlDbType.VarChar);
                    cmd.Parameters["@QuestionType"].Value = questionType;

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        return CreateEntityBuilder<JobPostingGeneralQuestion>().BuildEntities(reader);
                    }
                }
            }
        }        
        #endregion
    }
}
