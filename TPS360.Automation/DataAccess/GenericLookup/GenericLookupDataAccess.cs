﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;

using System.Data.SqlClient;


namespace TPS360.Automation
{
    internal sealed class GenericLookupDataAccess : BaseDataAccess
    {
        #region Constructors

        public GenericLookupDataAccess()
        {
        }

        protected override IEntityBuilder<GenericLookup> CreateEntityBuilder<GenericLookup>()
        {
            return (new GenericLookupBuilder()) as IEntityBuilder<GenericLookup>;
        }

        #endregion

        #region  Methods

        public GenericLookup GetById(int id)
        {
            const string SP = "GenericLookup_GetById";

            using (SqlConnection objSqlCon = new SqlConnection("context connection=true;"))
            {
                objSqlCon.Open();
                using (SqlCommand cmd = new SqlCommand(SP, objSqlCon))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@Id", SqlDbType.Int);
                    cmd.Parameters["@Id"].Value = id;

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return CreateEntityBuilder<GenericLookup>().BuildEntity(reader);
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }

        #endregion
    }
}