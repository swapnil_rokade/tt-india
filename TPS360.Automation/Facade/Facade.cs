using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Data.SqlClient;

namespace TPS360.Automation
{
    public static class Facade
    {
        #region Entity Bussiness Layer

        #region JobPosting

        internal static JobPosting GetJobPostingById(int id)
        {
            JobPostingDataAccess newDataAccess = new JobPostingDataAccess();
            return newDataAccess.GetById(id);
        }

        internal static IList<JobPosting> GetAllJobPostingByMemberId(int memberId)
        {
            JobPostingDataAccess newDataAccess = new JobPostingDataAccess();
            return newDataAccess.GetAllByMemberId(memberId);
        }

        #endregion

        #region JobPostingSkillSet
        internal static IList<JobPostingSkillSet> GetAllJobPostingSkillSetByjobPostingId(int jobPostingId)
        {
            JobPostingSkillSetDataAccess newDataAccess = new JobPostingSkillSetDataAccess();
            return newDataAccess.GetAllByJobPostingId(jobPostingId);
        }
        #endregion

        #region JobPostingAssessment
        internal static IList<JobPostingAssessment> GetAllJobPostingAssessmentByjobPostingId(int jobPostingId)
        {
            JobPostingAssessmentDataAccess newDataAccess = new JobPostingAssessmentDataAccess();
            return newDataAccess.GetAllByJobPostingId(jobPostingId);
        }
        #endregion

        #region JobPostingGeneralQuestion
        internal static IList<JobPostingGeneralQuestion> GetAllJobPostingGeneralQuestionByjobPostingId(int jobPostingId, String questionType)
        {
            JobPostingGeneralQuestionDataAccess newDataAccess = new JobPostingGeneralQuestionDataAccess();
            return newDataAccess.GetAllByJobPostingId(jobPostingId, questionType);
        }
        #endregion

        #region GenericLookup

        internal static GenericLookup GetGenericLookupById(int id)
        {
            GenericLookupDataAccess newDataAccess = new GenericLookupDataAccess();
            return newDataAccess.GetById(id);
        }

        #endregion

        #region Skill
        internal static Skill GetSkillById(int id)
        {
            SkillDataAccess newDataAccess = new SkillDataAccess();
            return newDataAccess.GetById(id);
        }
        #endregion

        #region State
        internal static State GetStateById(int id)
        {
            StateDataAccess newDataAccess = new StateDataAccess();
            return newDataAccess.GetById(id);
        }
        #endregion

        #region TestMaster
        internal static TestMaster GetTestMasterById(int id)
        {
            TestMasterDataAccess newDataAccess = new TestMasterDataAccess();
            return newDataAccess.GetById(id);
        }
        #endregion        

        #endregion

        #region Report Generator

        //[return: SqlFacet(MaxSize = -1)]
        //[Microsoft.SqlServer.Server.SqlFunction(DataAccess = DataAccessKind.Read)]
        //public static string GetDailyReport(string strEmployeeId)
        //{
        //    //DailyReport newRpt = new DailyReport(strEmployeeId);
        //    return DailyReport.GetReport(strEmployeeId, DateTime.Now);
        //}

        [return: SqlFacet(MaxSize = -1)]
        [Microsoft.SqlServer.Server.SqlFunction(DataAccess = DataAccessKind.Read)]
        public static string GetJobReport(int jobId,string candidateName,bool isSingle)
        {
            string strJobDetail = JobReport.GetReport(jobId, string.Empty);
            if (isSingle)
            {
                return GetReportHeader() + strJobDetail + GetReportFooter();
            }
            else
            {
                return strJobDetail;
            }
        }

        [SqlFunction]
        public static string GetReportHeader()
        {
            string strHeader = @"
<html>
<head>
<style>
.JSBorderLine
{
	background-color:#D3D3D3;
}
.JSFormHeading
{
	BACKGROUND-COLOR: #006699; font-weight:bold; Color:white;font-family:Verdana; 
	font-size:12px;
}

.JSFormField
{
	BACKGROUND-COLOR: #e1f2fb;
    font-weight:bold; 
    color:Black;
    font-family:Verdana; 
	font-size:11px;
}

.NewFormValue
{
	BACKGROUND-COLOR: white;
	font-weight:normal;
	color:Black;
	font-family:Verdana; 
	font-size:11px;
}
</style>
</head>
<body>
";
            return strHeader;
        }

        [SqlFunction]
        public static string GetReportFooter()
        {
            return "</body></html>";
        }

        [return: SqlFacet(MaxSize = -1)]
        [Microsoft.SqlServer.Server.SqlFunction(DataAccess = DataAccessKind.Read)]
        public static string GetAlertForBirthDay(string candidateName, string candidateEmail)
        {
            string strJobDetail = WorkFlowAlert.GetAlertForBirthdayNotification(candidateName, candidateEmail);
            return GetReportHeader() + strJobDetail + GetReportFooter();
        }

        #endregion  Report Generator

        #region Mail Account

        public static bool CreateEmailAccount(string emailId, string smtp,string connectionString)
        {
            DBMailAccount newMailAccount = new DBMailAccount();
            return newMailAccount.Create(emailId, smtp, connectionString);
        }

        public static bool SetDefaultEmailAccount(int Id, string connectionString)
        {
            DBMailAccount newMailAccount = new DBMailAccount();
            return newMailAccount.SetDefault(Id, connectionString);
        }

        public static int GetDefaultEmailAccountId(string connectionString)
        {
            DBMailAccount newMailAccount = new DBMailAccount();
            return newMailAccount.GetDefaultAccountId(connectionString);
        }

        public static bool DeleteEmailAccount(int Id, string connectionString)
        {
            DBMailAccount newMailAccount = new DBMailAccount();
            return newMailAccount.Delete(Id, connectionString);
        }

        public static DataTable GetAllEmailAccount(string connectionString)
        {
            DBMailAccount newMailAccount = new DBMailAccount();
            return newMailAccount.GetAll(connectionString);
        }

        #endregion

        #region Job & Schedule

        public static bool CreateJobSchedule(string JobType, string SqlCommandText, DateTime StartDateTime, DateTime EndDateTime, bool Active, string DbConnectionString)
        {
            DBJobAgent newJobAgent = new DBJobAgent();
            return newJobAgent.Create(JobType, SqlCommandText, StartDateTime, EndDateTime, Active, DbConnectionString);
        }

        public static DataTable GetAllJobSchedule(string DbConnectionString)
        {
            DBJobAgent newJobAgent = new DBJobAgent();
            return newJobAgent.GetAll(DbConnectionString);
        }

        public static bool DeleteJobSchedule(string JobId, string DbConnectionString)
        {
            DBJobAgent newJobAgent = new DBJobAgent();
            return newJobAgent.Delete(JobId,DbConnectionString);
        }

        public static bool UpdateJobSchedule(string JobId, string JobType, string SqlCommandText, DateTime StartDateTime, DateTime EndDateTime, bool Active, string DbConnectionString)
        {
            DBJobAgent newJobAgent = new DBJobAgent();
            return newJobAgent.Update(JobId,JobType, SqlCommandText, StartDateTime, EndDateTime, Active, DbConnectionString);
        }

        public static bool ChangeConfiguration(bool enable, string DbConnectionString)
        {
            DBJobAgent newJobAgent = new DBJobAgent();
            return newJobAgent.ChangeConfiguration(enable, DbConnectionString);
        }


        #endregion
    }
}