﻿using System;
using System.Collections.Generic;

namespace TPS360.Automation
{
    static class MiscUtil
    {
        public static string GetLookupNameById(int id)
        {
            if (id > 0)
            {
                GenericLookup genericLookUp = Facade.GetGenericLookupById(id);
                if (genericLookUp != null)
                {
                    return genericLookUp.Name;
                }
            }
            return string.Empty;
        }

        public static string GetBenifitsById(string benifitsLookupIds)
        {
            string benifits = string.Empty;
            int benifitLookupId = 0;
            GenericLookup genericlookUp = null;
            string[] benifitIds = benifitsLookupIds.Split(new char[] { '!' });

            foreach (string bs in benifitIds)
            {
                Int32.TryParse(bs, out benifitLookupId);
                if (benifitLookupId > 0)
                {
                    genericlookUp = Facade.GetGenericLookupById(benifitLookupId);
                    if (genericlookUp != null)
                    {
                        if (string.IsNullOrEmpty(benifits))
                        {
                            benifits = genericlookUp.Name;
                        }
                        else
                        {
                            benifits = benifits + ", " + genericlookUp.Name;
                        }
                    }
                }
            }

            return benifits;
        }

        public static string GetRequisitionSkillSetList(int jobPostingId)
        {
            string skills = string.Empty;
            int skillId = 0;
            IList<JobPostingSkillSet> jobPostingSkillSetList = Facade.GetAllJobPostingSkillSetByjobPostingId(jobPostingId);

            if (jobPostingSkillSetList != null)
            {
                foreach (JobPostingSkillSet jobPostingSkill in jobPostingSkillSetList)
                {
                    skillId = jobPostingSkill.SkillId;
                    Skill skill = Facade.GetSkillById(skillId);

                    if (skill != null)
                    {
                        if (string.IsNullOrEmpty(skills))
                        {
                            skills = skill.Name;
                        }
                        else
                        {
                            skills = skills + ", " + skill.Name;
                        }
                    }
                }
            }

            return skills;
        }

        public static string GetSkillNameById(int skillId)
        {
            string skillName = string.Empty;

            Skill skill = Facade.GetSkillById(skillId);
            skillName = (skill != null ? skill.Name : string.Empty);

            return skillName;
        }

        public static string GetTestNameById(int testId)
        {
            string testName = string.Empty;

            TestMaster testMaster = Facade.GetTestMasterById(testId);
            testName = (testMaster != null ? testMaster.Name : string.Empty);

            return testName;
        }
    }
}
