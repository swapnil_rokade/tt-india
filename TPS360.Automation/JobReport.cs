using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Data.SqlClient;
using System.Collections;
using System.Web;

namespace TPS360.Automation
{
    static class JobReport
    {
        private static readonly bool _showJobDescription=true;
        private static readonly bool _showClientJobDescription = true;
        private static readonly bool _showAssessment = true;
        private static readonly bool _showScreeningQuestion = true;
        private static readonly string emailHeaderStyle = "style='background-color:#dcdde0;font-weight:bold'";

        public static string GetReport(int jobpostingId,string candidateName)
        {
            string strMessage = String.Empty;
            try
            {
                //JobPosting jobPosting = Facade.GetJobPostingById(jobpostingId);
                return GetJobPostingDetailHTML(jobpostingId, candidateName);
            }
            catch (Exception ex)
            {
                return strMessage + ex.Message;
            }

        }

        public static string GetJobPostingDetailHTML(int jobPostingId, string candidateName)
        {
            StringBuilder jobPostingHTMLText = new StringBuilder();
            try
            {
                jobPostingHTMLText.Append("<table width=700px align=center border=0 cellspacing=0 cellpadding=5>");

                //Introduction
                jobPostingHTMLText.Append("    <tr>");
                jobPostingHTMLText.Append("        <td><div align=justify>");
                jobPostingHTMLText.Append("            <br/><br/>Dear &nbsp;" + candidateName + ", <br/><br/>");
                jobPostingHTMLText.Append("            I came across your resume while searching various internet job boards or in our internal database.  I believe the following job ");
                jobPostingHTMLText.Append("            is a good match with your profile.");
                jobPostingHTMLText.Append("        </td>");
                jobPostingHTMLText.Append("    </tr>");

                //Middle Part: Contains All Job Posting Information
                jobPostingHTMLText.Append(GetJobPostingInformation(jobPostingId));

                //Last Part of Email: Contains employee signature and offer
                jobPostingHTMLText.Append("    <tr>");
                jobPostingHTMLText.Append("        <td><div align=justify>");

                jobPostingHTMLText.Append("<br/>If you are interested in pursuing the above job opportunity,"
                            + " please forward a word version"
                            + " of your resume by email with salary expectations and other details.");
                jobPostingHTMLText.Append("         </td>");
                jobPostingHTMLText.Append("    </tr>");

                //Email footer
                //jobPostingHTMLText.Append(GetEmailFooter());

                jobPostingHTMLText.Append("</table>");
            }
            catch (Exception ex)
            {
                jobPostingHTMLText.Append(ex.Message.ToString());
            }
            return jobPostingHTMLText.ToString();
        }

        private static string GetJobPostingInformation(int jobPostingId)
        {
            StringBuilder jobPostingHTML = new StringBuilder();
            JobPosting jobPosting = Facade.GetJobPostingById(jobPostingId);

            if (jobPosting != null)
            {
                // Requisition Details
                jobPostingHTML.Append("    <tr>");
                jobPostingHTML.Append("        <td>");
                jobPostingHTML.Append("            " + GetJobPostingBasicDetails(jobPosting) + "");
                jobPostingHTML.Append("        </td>");
                jobPostingHTML.Append("    </tr>");

                if (_showJobDescription)
                {
                    // Job Description
                    jobPostingHTML.Append("    <tr>");
                    jobPostingHTML.Append("        <td>");
                    jobPostingHTML.Append("            " + GetJobDescription(jobPosting) + "");
                    jobPostingHTML.Append("        </td>");
                    jobPostingHTML.Append("    </tr>");
                }

                if (_showClientJobDescription)
                {
                    // Client Job Description
                    jobPostingHTML.Append("    <tr>");
                    jobPostingHTML.Append("        <td>");
                    jobPostingHTML.Append("            " + GetClientJobDescription(jobPosting) + "");
                    jobPostingHTML.Append("        </td>");
                    jobPostingHTML.Append("    </tr>");
                }

                // Skill Set
                jobPostingHTML.Append("    <tr>");
                jobPostingHTML.Append("        <td>");
                jobPostingHTML.Append("            " + GetJobPostingSkillSet(jobPosting.Id) + "");
                jobPostingHTML.Append("        </td>");
                jobPostingHTML.Append("    </tr>");

                if (_showAssessment)
                {
                    //Required Assessments
                    jobPostingHTML.Append("    <tr>");
                    jobPostingHTML.Append("        <td>");
                    jobPostingHTML.Append("            " + GetJobPostingAssessment(jobPosting.Id) + "");
                    jobPostingHTML.Append("        </td>");
                    jobPostingHTML.Append("    </tr>");
                }

                if (_showScreeningQuestion)
                {
                    // Required General Screening Questions
                    jobPostingHTML.Append("    <tr>");
                    jobPostingHTML.Append("        <td>");
                    jobPostingHTML.Append("            " + GetJobPostingGeneralQuestion(jobPosting.Id,jobPosting.JobDescription ) + "");
                    jobPostingHTML.Append("        </td>");
                    jobPostingHTML.Append("    </tr>");
                }
            }
            return jobPostingHTML.ToString();
        }

        private static string GetJobPostingBasicDetails(JobPosting jobPosting)
        {
            StringBuilder jobPostingDetailsHTML = new StringBuilder();
            string startDate = string.Empty;

            //Start Date
            if (string.IsNullOrEmpty(jobPosting.StartDate))
            {
                startDate = "ASAP";
            }
            else
            {
                startDate = jobPosting.StartDate.ToString();
            }

            //Education
            string education = string.Empty;
            //education = MiscUtil.GetLookupNameById(jobPosting.RequiredDegreeLookupId);
     

            //Salary
            string salary = jobPosting.PayRate + jobPosting.PayCycle;

            //Benefits
            string benefits = MiscUtil.GetBenifitsById(jobPosting.OtherBenefits);
            string duration = string.Empty;
            //Employement Duration
            //duration = MiscUtil.GetLookupNameById(jobPosting.JobDurationLookupId) + " (" + jobPosting.JobDurationMonth + " Months)";

            //Travel Required
            string travel = "";
            if (jobPosting.TravelRequiredPercent.ToString() == "0")
            {
                travel = "None";
            }
            else if (jobPosting.TravelRequiredPercent.ToString() == "25")
            {
                travel = jobPosting.TravelRequiredPercent.ToString() + "%";
            }

            jobPostingDetailsHTML.Append("<table cellpadding=0 cellspacing=0 border=0 width=100%>");
            jobPostingDetailsHTML.Append("<tr>");
            jobPostingDetailsHTML.Append("    <td colspan=2 width=98%>");
            jobPostingDetailsHTML.Append("        <table width=100% border=0 cellspacing=1 cellpadding=5>");
            jobPostingDetailsHTML.Append("            <tr>");
            jobPostingDetailsHTML.Append("                <td height=5 " + emailHeaderStyle + ">Requisition Details</td>");
            jobPostingDetailsHTML.Append("            </tr>");
            jobPostingDetailsHTML.Append("        </table>");
            jobPostingDetailsHTML.Append("    </td>");
            jobPostingDetailsHTML.Append("</tr>");

            jobPostingDetailsHTML.Append("<tr>");
            jobPostingDetailsHTML.Append("    <td colspan=2 valign=top align=left width=98%>");
            jobPostingDetailsHTML.Append("        <table width=100% border=0 cellspacing=1 cellpadding=5>");
            jobPostingDetailsHTML.Append("            <tr>");
            jobPostingDetailsHTML.Append("                <td width=21%>Job Title</td>");
            jobPostingDetailsHTML.Append("                <td>" + jobPosting.JobTitle.ToString() + "</td>");
            jobPostingDetailsHTML.Append("                <td width=21%>No. of Openings</td>");
            jobPostingDetailsHTML.Append("                <td>" + jobPosting.NoOfOpenings.ToString() + "</td>");
            jobPostingDetailsHTML.Append("            </tr>");
            jobPostingDetailsHTML.Append("            <tr>");
            jobPostingDetailsHTML.Append("                <td width=21%>City</td>");
            jobPostingDetailsHTML.Append("                <td>" + jobPosting.City.ToString() + "</td>");
            jobPostingDetailsHTML.Append("                <td width=21%>State</td>");
            //jobPostingDetailsHTML.Append("                <td>" + MiscUtil.GetStateNameById(jobPosting.StateId, facade) + "</td>");
            jobPostingDetailsHTML.Append("            </tr>");
            jobPostingDetailsHTML.Append("            <tr>");
            jobPostingDetailsHTML.Append("                <td width=21%>Start Date</td>");
            jobPostingDetailsHTML.Append("                <td nowrap=nowrap>" + startDate + "</td>");
            jobPostingDetailsHTML.Append("                <td width=21%>Experience Required (yrs)</td>");
            jobPostingDetailsHTML.Append("                <td nowrap=nowrap><b>Min</b>&nbsp;" + jobPosting.MaxExpRequired.ToString() + "&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<b>Max</b>&nbsp;" + jobPosting.MaxExpRequired.ToString() + "</td>");
            jobPostingDetailsHTML.Append("            </tr>");
            jobPostingDetailsHTML.Append("            <tr>");
            jobPostingDetailsHTML.Append("                <td width=21%>Educational Qualification</td>");
            jobPostingDetailsHTML.Append("                <td>" + education + "</td>");
            jobPostingDetailsHTML.Append("                <td width=21%>Salary</td>");
            jobPostingDetailsHTML.Append("                <td>" + salary + "</td>");
            jobPostingDetailsHTML.Append("            </tr>");
            jobPostingDetailsHTML.Append("            <tr>");
            jobPostingDetailsHTML.Append("                <td width=21% valign=top align=left>Benefits</td>");
            jobPostingDetailsHTML.Append("                <td colspan=3><div align=justify>" + benefits + "</div></td>");
            jobPostingDetailsHTML.Append("            </tr>");
            jobPostingDetailsHTML.Append("            <tr>");
            jobPostingDetailsHTML.Append("                <td width=21%>Employment Length</td>");
            jobPostingDetailsHTML.Append("                <td>" + duration + "</td>");
            jobPostingDetailsHTML.Append("                <td width=21%>Travel Required</td>");
            jobPostingDetailsHTML.Append("                <td>" + travel + "</td>");
            jobPostingDetailsHTML.Append("            </tr>");
            jobPostingDetailsHTML.Append("        </table>");
            jobPostingDetailsHTML.Append("    </td>");
            jobPostingDetailsHTML.Append("</tr>");
            jobPostingDetailsHTML.Append("</table>");

            return jobPostingDetailsHTML.ToString();
        }

        private static string GetJobDescription(JobPosting jobPosting)
        {
            StringBuilder jobDescriptionHTML = new StringBuilder();
            jobDescriptionHTML.Append("<table cellpadding=0 cellspacing=0 border=0 width=100%>");
            jobDescriptionHTML.Append("<tr>");
            jobDescriptionHTML.Append("    <td>");
            jobDescriptionHTML.Append("        <table width=100% border=0 cellspacing=1 cellpadding=5>");
            jobDescriptionHTML.Append("            <tr>");
            jobDescriptionHTML.Append("                <td colspan=2 height=5 " + emailHeaderStyle + ">Job Description</td>");
            jobDescriptionHTML.Append("            </tr>");
            jobDescriptionHTML.Append("            <tr>");
            jobDescriptionHTML.Append("                <td colspan=2 valign=top>");
            jobDescriptionHTML.Append("                        <div align=justify>" + jobPosting.JobDescription.ToString() + "</div>");
            jobDescriptionHTML.Append("                </td>");
            jobDescriptionHTML.Append("            </tr>");
            jobDescriptionHTML.Append("        </table>");
            jobDescriptionHTML.Append("    </td>");
            jobDescriptionHTML.Append("</tr>");
            jobDescriptionHTML.Append("</table>");
            return jobDescriptionHTML.ToString();
        }

        private static string GetClientJobDescription(JobPosting jobPosting)
        {
            StringBuilder jobDescriptionHTML = new StringBuilder();
            jobDescriptionHTML.Append("<table cellpadding=0 cellspacing=0 border=0 width=100%>");
            jobDescriptionHTML.Append("<tr>");
            jobDescriptionHTML.Append("    <td>");
            jobDescriptionHTML.Append("        <table width=100% border=0 cellspacing=1 cellpadding=5>");
            jobDescriptionHTML.Append("            <tr>");
            jobDescriptionHTML.Append("                <td colspan=2 height=5 " + emailHeaderStyle + ">Client Job Description</td>");
            jobDescriptionHTML.Append("            </tr>");
            jobDescriptionHTML.Append("            <tr>");
            jobDescriptionHTML.Append("                <td colspan=2 valign=top>");
            jobDescriptionHTML.Append("                        <div align=justify>" + (jobPosting.ClientJobDescription.ToString()) + "</div>");
            jobDescriptionHTML.Append("                </td>");
            jobDescriptionHTML.Append("            </tr>");
            jobDescriptionHTML.Append("        </table>");
            jobDescriptionHTML.Append("    </td>");
            jobDescriptionHTML.Append("</tr>");
            jobDescriptionHTML.Append("</table>");
            return jobDescriptionHTML.ToString();
        }

        private static string GetJobPostingSkillSet(int jobPostingId)
        {
            StringBuilder skillSetHTML = new StringBuilder();

            skillSetHTML.Append("<table cellpadding=0 cellspacing=0 border=0 width=100%>");
            skillSetHTML.Append("<tr>");
            skillSetHTML.Append("    <td>");
            skillSetHTML.Append("        <table width=100% border=0 cellspacing=1 cellpadding=5>");
            skillSetHTML.Append("            <tr>");
            skillSetHTML.Append("                <td colspan=3 height=5 " + emailHeaderStyle + ">Required Skills</td>");
            skillSetHTML.Append("            </tr>");
            skillSetHTML.Append("            <tr>");
            skillSetHTML.Append("                <td width=50%>Skill</td>");
            skillSetHTML.Append("                <td width=25% align=center>Experience (Yrs)</td>");
            skillSetHTML.Append("                <td width=25% align=center>Proficiency</td>");
            skillSetHTML.Append("            </tr>");

            IList<JobPostingSkillSet> jobPostingSkillSetList = Facade.GetAllJobPostingSkillSetByjobPostingId(jobPostingId);

            if (jobPostingSkillSetList != null)
            {
                foreach (JobPostingSkillSet jobPostingSkillSet in jobPostingSkillSetList)
                {
                    if (jobPostingSkillSet != null)
                    {
                        skillSetHTML.Append("<tr>");
                        skillSetHTML.Append("    <td>" + MiscUtil.GetSkillNameById(jobPostingSkillSet.SkillId) + "</td>");
                        skillSetHTML.Append("    <td align=center>" + jobPostingSkillSet.NoOfYearsExp.ToString() + "</td>");
                        skillSetHTML.Append("    <td align=center>" + MiscUtil.GetLookupNameById(jobPostingSkillSet.ProficiencyLookupId) + "</td>");
                        skillSetHTML.Append("</tr>");
                    }
                }
            }

            skillSetHTML.Append("        </table>");
            skillSetHTML.Append("    </td>");
            skillSetHTML.Append("</tr>");
            skillSetHTML.Append("</table>");
            return skillSetHTML.ToString();
        }

        private static string GetJobPostingAssessment(int jobPostingId)
        {
            StringBuilder assessmentsHTML = new StringBuilder();

            assessmentsHTML.Append("<table cellpadding=0 cellspacing=0 border=0 width=100%>");
            assessmentsHTML.Append("<tr>");
            assessmentsHTML.Append("    <td>");
            assessmentsHTML.Append("        <table width=100% border=0 cellspacing=1 cellpadding=5>");
            assessmentsHTML.Append("            <tr>");
            assessmentsHTML.Append("                <td height=5 " + emailHeaderStyle + ">Required Assessment Tests</td>");
            assessmentsHTML.Append("            </tr>");

            IList<JobPostingAssessment> jobPostingAssessmentList = Facade.GetAllJobPostingAssessmentByjobPostingId(jobPostingId);

            if (jobPostingAssessmentList.Count > 0)
            {
                foreach (JobPostingAssessment jobPostingAssessment in jobPostingAssessmentList)
                {
                    if (jobPostingAssessment != null)
                    {
                        assessmentsHTML.Append("<tr>");
                        assessmentsHTML.Append("    <td align=left>" + MiscUtil.GetTestNameById(jobPostingAssessment.TestMasterId) + "</td>");
                        assessmentsHTML.Append("</tr>");
                    }
                }
            }

            assessmentsHTML.Append("        </table>");
            assessmentsHTML.Append("    </td>");
            assessmentsHTML.Append("</tr>");
            assessmentsHTML.Append("</table>");

            return assessmentsHTML.ToString();
        }

        private static string GetJobPostingGeneralQuestion(int jobPostingId, string questionType)
        {
            StringBuilder questionsHTML = new StringBuilder();

            questionsHTML.Append("<table cellpadding=0 cellspacing=0 border=0 width=100%>");
            questionsHTML.Append("<tr>");
            questionsHTML.Append("    <td>");
            questionsHTML.Append("        <table width=100% border=0 cellspacing=1 cellpadding=5>");
            questionsHTML.Append("            <tr>");
            questionsHTML.Append("                <td colspan=3 height=5 " + emailHeaderStyle + ">Required General Screening Questions</td>");
            questionsHTML.Append("            </tr>");

            IList<JobPostingGeneralQuestion> jobPostingGeneralQuestionList = Facade.GetAllJobPostingGeneralQuestionByjobPostingId(jobPostingId, questionType);

            if (jobPostingGeneralQuestionList != null)
            {
                foreach (JobPostingGeneralQuestion jobPostingGeneralQuestion in jobPostingGeneralQuestionList)
                {
                    if (jobPostingGeneralQuestion != null)
                    {
                        questionsHTML.Append("<tr>");
                        questionsHTML.Append("   <td colspan=3 align=left>" + jobPostingGeneralQuestion.Question + "</td>");
                        questionsHTML.Append("</tr>");
                    }
                }
            }

            questionsHTML.Append("        </table>");
            questionsHTML.Append("    </td>");
            questionsHTML.Append("</tr>");
            questionsHTML.Append("</table>");

            return questionsHTML.ToString();
        }

    }
}

