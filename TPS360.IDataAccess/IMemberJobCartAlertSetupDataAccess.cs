﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberJobCartAlertSetupDataAccess

    public interface IMemberJobCartAlertSetupDataAccess
    {
        MemberJobCartAlertSetup Add(MemberJobCartAlertSetup memberJobCartAlertSetup);

        MemberJobCartAlertSetup Update(MemberJobCartAlertSetup memberJobCartAlertSetup);

        MemberJobCartAlertSetup GetById(int id);

        MemberJobCartAlertSetup GetByMemberId(int id);

        IList<MemberJobCartAlertSetup> GetAllByMemberId(int memberId);

        IList<MemberJobCartAlertSetup> GetAll();

        bool DeleteById(int id);

        bool DeleteByMemberId(int memberId);
    }

    #endregion
}