﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IInterviewPanelDataAccess.cs
    Description: This is the DataAccess  library page.
    Created By: Pravin khot
    Created On: 20/nov/2015
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    
*/
using System.Collections.Generic;
using TPS360.Common.Shared;
using TPS360.Common.BusinessEntities;



namespace TPS360.DataAccess
{
    #region IInterviewPanelDataAccess

    public interface IInterviewPanelDataAccess
    {
        InterviewPanel Add(InterviewPanel InterviewPanel);
        InterviewPanel AddOtherInterviewer(InterviewPanel InterviewPanel);

        InterviewPanel Update(InterviewPanel InterviewPanel);
        InterviewPanel UpdateOtherInterviewer(InterviewPanel InterviewPanel);

        InterviewPanel GetById(int id);

        IList<InterviewPanel> GetAll();
        IList<InterviewPanel> GetAll(string SortExpression);
        PagedResponse<InterviewPanel> GetPaged(PagedRequest request);


        bool DeleteById(int InterviewPanelMaster_ById);
        bool UpdateDeleteById(int InterviewPanelids);
        bool ChkEmailId(int InterviewerId, int panelid);
        bool ClientChkEmailId(int InterviewerId, int panelid);
        IList<InterviewPanel> OtherChkEmailId(int panelid);
        IList<InterviewPanel> ScheduleOtherChkEmailId(int RequisitionId);
    }

    #endregion
}