﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberSignatureDataAccess

    public interface IMemberSignatureDataAccess
    {
        MemberSignature Add(MemberSignature memberSignature);

        MemberSignature Update(MemberSignature memberSignature);

        MemberSignature GetById(int id);

        MemberSignature GetByMemberId(int memberId);

        IList<MemberSignature> GetAllByMemberId(int memberId);

        IList<MemberSignature> GetAll();

        bool DeleteById(int id);

        bool DeleteByMemberId(int memberId);
    }

    #endregion
}