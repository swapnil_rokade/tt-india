﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberChangeArchiveDataAccess

    public interface IMemberChangeArchiveDataAccess
    {
        MemberChangeArchive Add(MemberChangeArchive memberChangeArchive);

        MemberChangeArchive Update(MemberChangeArchive memberChangeArchive);

        MemberChangeArchive GetById(int id);

        IList<MemberChangeArchive> GetAllByMemberId(int memberId);

        IList<MemberChangeArchive> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}