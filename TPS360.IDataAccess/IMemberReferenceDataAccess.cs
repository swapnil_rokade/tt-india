﻿/*-------------------------------------------------------------------------------------------------------------   FileName: IMemberReferenceDataAccess.cs
   Description: 
   Created By: 
   Created On:
   Modification Log:
   ------------------------------------------------------------------------------------------------------------   Ver.No.             Date                  Author              Modification
   ------------------------------------------------------------------------------------------------------------    0.1             Feb-03-2008             Sandeesh            Defect Fix - ID: 9376; Implemented sorting.
*/
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IMemberReferenceDataAccess

    public interface IMemberReferenceDataAccess
    {
        MemberReference Add(MemberReference memberReference);

        MemberReference Update(MemberReference memberReference);

        MemberReference GetById(int id);

        IList<MemberReference> GetAll();
        
        IList<MemberReference> GetAllByMemberId(int memberId);

        //0.1 Start

        IList<MemberReference> GetAllByMemberId(int memberId, string sortExpression);
        //0.1 End 
        PagedResponse<MemberReference> GetPaged(PagedRequest request);

        string  GetEmailById(int id);

        bool DeleteById(int id);
    }

    #endregion
}