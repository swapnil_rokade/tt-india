﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IResourceDataAccess

    public interface IResourceDataAccess
    {
        Resource Add(Resource resource);

        Resource Update(Resource resource);

        Resource GetByResourceID(int resourceID);

        IList<Resource> GetAll();

        bool DeleteByResourceID(int resourceID);

        Resource GetByResourceName(string resourceName);
    }

    #endregion
}