﻿using TPS360.Common.BusinessEntities;
using System.Collections.Generic;

namespace TPS360.DataAccess
{
    #region IActivityDataAccess

    public interface IActivityDataAccess
    {
        Activity Add(Activity activity);

        Activity AddForInterview(Activity activity);

        Activity Update(Activity activity);

        Activity UpdateForInterview(Activity activity);

        Activity GetById(int id);

        //IList<Activity> GetAll();

        bool DeleteById(int id);
        IList<Activity> GetAllActivityByResourceIdAndDate(int resourceID, string Date);
        IList<Activity> GetAllActivityByResourceId(int resourceID);
    }

    #endregion
}