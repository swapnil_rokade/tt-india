﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;


namespace TPS360.DataAccess
{
    #region IMemberSubmissionDataAccess

    public interface IMemberSubmissionDataAccess
    {
        void  Add(MemberSubmission memberSubmission,string MemberId);

        MemberSubmission Update(MemberSubmission memberSubmission);

        MemberSubmission GetByMemberIdAndJobPostingId(int memberid, int jobpostingid);
    }
    #endregion
}
