﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System;

namespace TPS360.DataAccess
{
    #region IMemberDailyReportDataAccess

    public interface IMemberDailyReportDataAccess
    {
        MemberDailyReport Add(MemberDailyReport memberDailyReport);

        MemberDailyReport Update(MemberDailyReport memberDailyReport);

        MemberDailyReport GetById(int id);

        MemberDailyReport GetByLoginTimeAndMemberId(DateTime loginTime,int memberId);

        IList<MemberDailyReport> GetAll();

        PagedResponse<MemberDailyReport> GetPaged(PagedRequest request);

        bool DeleteById(int id);

        bool UpdateAllByLoginTimeAndMemberId(DateTime loginTime, int memberId);
    }

    #endregion
}