﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ISiteSettingDataAccess

    public interface ISiteSettingDataAccess
    {
        SiteSetting Add(SiteSetting siteSetting);

        SiteSetting Update(SiteSetting siteSetting);

        SiteSetting GetById(int id);

        IList<SiteSetting> GetAll();

        bool DeleteById(int id);

        SiteSetting GetBySettingType(int settingType);
    }

    #endregion
}