﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using System.Collections;
using System.Data;

namespace TPS360.DataAccess
{
    #region IWebParserDomainDataAccess

    public interface IWebParserDomainDataAccess
    {
        WebParserDomain Add(WebParserDomain memberManager);

        WebParserDomain Update(WebParserDomain memberManager);

        WebParserDomain GetById(int id);

        IList<WebParserDomain> GetAll();

        bool DeleteById(int id);

        DataTable GetAllName();
    }

    #endregion
}