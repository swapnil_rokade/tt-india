﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IInterviewFeedbackDataAccess.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    0.1              27/Dec/2015          Prasanth Kumar G      Introduced GetByInterviewId_InterviewerEmail
     0.2             24/Dec/2015          pravin khot            Introduced by GetByInterviewIdDetail,GetByInterviewIdAllEmail,GetInterviewFeedbackReport,GetInterviewFeedbackReportPrint,GetInterviewAssesReportPrint
 
 ---------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System.Collections.Generic;
using TPS360.Common.Shared;
using TPS360.Common.BusinessEntities;



namespace TPS360.DataAccess
{
    #region IInterviewFeedbackDataAccess

    public interface IInterviewFeedbackDataAccess
    {
        void Add(InterviewFeedback interviewFeedback);

        void Update(InterviewFeedback interviewFeedback);

        InterviewFeedback GetById(int id);

        IList<InterviewFeedback> GetAll();

        PagedResponse<InterviewFeedback> GetPaged(PagedRequest request);

        IList<InterviewFeedback> GetByInterviewId(int InterviewId);
        //Line introduced by Prasanth on 27/Dec/2015
        InterviewFeedback GetByInterviewId_InterviewerEmail(int InterviewId, string InterviewerEmail);

        void DeleteById(int id);        
        IList<InterviewFeedback> GetByInterviewIdDetail(int InterviewId); //code added by pravin khot on 22/Dec/2015
        IList<InterviewFeedback> GetByInterviewIdAllEmail(int InterviewId); //code added by pravin khot on 22/Dec/2015
        PagedResponse<InterviewFeedback> GetInterviewFeedbackReport(PagedRequest request);//code added by pravin khot on 24/dec/2015
        PagedResponse<InterviewFeedback> GetInterviewFeedbackReportPrint(PagedRequest request);//code added by pravin khot on 29/dec/2015
        PagedResponse<InterviewFeedback> GetInterviewAssesReportPrint(PagedRequest request);//code added by pravin khot on 29/dec/2015
    }

    #endregion
}