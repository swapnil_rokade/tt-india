﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IActionLogDataAccess

    public interface IActionLogDataAccess
    {
        ActionLog Add(ActionLog actionLog);

        ActionLog Update(ActionLog actionLog);

        ActionLog GetById(int id);

        IList<ActionLog> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}