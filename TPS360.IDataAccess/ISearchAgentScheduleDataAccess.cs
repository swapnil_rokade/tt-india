﻿using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System.Collections;
using System.Data;
using System;

namespace TPS360.DataAccess
{
    #region IJobPostingDataAccess
    public interface ISearchAgentScheduleDataAccess
    {
        SearchAgentSchedule Add(SearchAgentSchedule SearchAgent);

        SearchAgentSchedule Update(SearchAgentSchedule SearchAgent);

        SearchAgentSchedule GetById(int id);

        SearchAgentSchedule GetByJobPostingId(int JobPostingId);

        IList<SearchAgentSchedule> GetAll();

        bool DeleteByJobPostingId(int JobPostingId);

        void AddCandidateForMail(SearchAgentMailToCandidate candidate);

    }
    #endregion
}
