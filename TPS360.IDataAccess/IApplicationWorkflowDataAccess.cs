﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IApplicationWorkflowDataAccess

    public interface IApplicationWorkflowDataAccess
    {
        ApplicationWorkflow Add(ApplicationWorkflow applicationWorkflow);

        ApplicationWorkflow Update(ApplicationWorkflow applicationWorkflow);

        ApplicationWorkflow GetById(int id);

        ApplicationWorkflow GetByTitle(string title);

        ApplicationWorkflow GetByMemberId(int memberId);

       
        IList<ApplicationWorkflow> GetAll();

        bool DeleteById(int id);

      
    }

    #endregion
}