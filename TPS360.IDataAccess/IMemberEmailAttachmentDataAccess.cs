﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberEmailAttachmentDataAccess

    public interface IMemberEmailAttachmentDataAccess
    {
        MemberEmailAttachment Add(MemberEmailAttachment memberEmailAttachment);

        MemberEmailAttachment Update(MemberEmailAttachment memberEmailAttachment);

        MemberEmailAttachment GetById(int id);

        IList<MemberEmailAttachment> GetAllByMemberEmailId(int memberEmailId);

        IList<MemberEmailAttachment> GetAll();

        bool DeleteById(int id);

        bool DeleteByMemberEmailId(int memberEmailId);
    }

    #endregion
}