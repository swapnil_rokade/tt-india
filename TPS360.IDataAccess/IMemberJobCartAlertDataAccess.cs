﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberJobCartAlertDataAccess

    public interface IMemberJobCartAlertDataAccess
    {
        MemberJobCartAlert Add(MemberJobCartAlert memberJobCartAlert);

        MemberJobCartAlert Update(MemberJobCartAlert memberJobCartAlert);

        MemberJobCartAlert GetById(int id);

        IList<MemberJobCartAlert> GetAll();

        IList<MemberJobCartAlert> GetAllByMemberJobCartAlertSetupId(int memberJobCartAlertSetupId);

        IList<MemberJobCartAlert> GetAllByJobPostingId(int jobPostingId);
        
        bool DeleteById(int id);

        bool RemoveAllByMemberJobCartAlertSetupId(int memberJobCartAlertSetupId);

        bool DeleteByMemberJobCartAlertSetupId(int memberJobCartAlertSetupId);
    }

    #endregion
}