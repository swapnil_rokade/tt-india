﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IJobPostingGeneralQuestionDataAccess

    public interface IJobPostingGeneralQuestionDataAccess
    {
        JobPostingGeneralQuestion Add(JobPostingGeneralQuestion jobPostingGeneralQuestion);

        JobPostingGeneralQuestion Update(JobPostingGeneralQuestion jobPostingGeneralQuestion);

        JobPostingGeneralQuestion GetById(int id);

        IList<JobPostingGeneralQuestion> GetAll();        

        bool DeleteById(int id);

        IList<JobPostingGeneralQuestion> GetAllByJobPostingId(int jobPostingId, string questionType);

        bool DeleteByJobPostingId(int jobPostingId);
    }

    #endregion
}