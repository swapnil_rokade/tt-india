﻿
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System.Collections;

namespace TPS360.DataAccess
{
    #region IMemberGroupDataAccess

    public interface IMemberGroupDataAccess
    {
        MemberGroup Add(MemberGroup memberGroup);

        MemberGroup Update(MemberGroup memberGroup);

        MemberGroup GetById(int id);

        IList<MemberGroup> GetAll();

        IList<MemberGroup> GetAll(int groupType);

        IList<MemberGroup> GetAllByMemberId(int memberId);

        PagedResponse<MemberGroup> GetPaged(PagedRequest request);

        PagedResponse<MemberGroup> GetPagedByMemberId(PagedRequest request);

        PagedResponse<MemberGroup> GetPagedByMemberIdandGroupType(PagedRequest request);

        bool DeleteById(int id);

        ArrayList GetAllNameByManagerId(int memberId);

        ArrayList GetAllNameByManagerId(int memberId, int groupType);
        int GetCountByManagerIDAndGroupType(int ManagerID, int groupType);
        int GetCountByGroupType(int groupType);
        MemberGroup GetByName(string name);
        bool GetMemberGroupAccessByCreatorIdAndGroupId(int CreatorId, int GroupId);
    }

    #endregion
}