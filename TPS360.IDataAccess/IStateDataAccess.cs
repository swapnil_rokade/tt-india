﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using System;

namespace TPS360.DataAccess
{
    #region IStateDataAccess

    public interface IStateDataAccess
    {
        State Add(State state);

        State Update(State state);

        State GetById(int id);

        IList<State> GetAll();

        IList<State> GetAllByCountryId(int countryId);

        bool DeleteById(int id);

        Int32 GetStateIdByStateName(string stateName);

        string GetStateNameById(int id);
    }

    #endregion
}