﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IDuplicateMemberDataAccess.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author                      Modification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    0.1              Sept-10-2009           Sukanta Ghorui              Added GetDuplicateRecords method
 ---------------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Collections;
using System.Collections.Generic;
using TPS360.Common.Shared;
using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IDuplicateMemberDataAccess

    public interface IDuplicateMemberDataAccess
    {
        IList<DuplicateMember> GetDuplicateRecords(bool checkFirstName, string strFirstName,
                bool checkMiddleName, string strMiddleName,
                bool checkLastName, string strLastName,
                bool checkEmailId, string strEmailId,
                bool checkDOB, DateTime? DOB,
                bool checkSSN, string strSSN);
    }

    #endregion

}