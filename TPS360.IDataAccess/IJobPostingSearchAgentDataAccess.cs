﻿using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System.Collections;
using System.Data;
using System;

namespace TPS360.DataAccess
{
    #region IJobPostingDataAccess

    public interface IJobPostingSearchAgentDataAccess
    {

        JobPostingSearchAgent Add(JobPostingSearchAgent jobPosting);

        JobPostingSearchAgent Update(JobPostingSearchAgent jobPosting);

        JobPostingSearchAgent GetById(int id);

        JobPostingSearchAgent GetByJobPostingId(int JobPostingId);

        void UpdateByJobPostingIdAndIsRemoved(int JobpostingId, bool IsRemoved);

        bool DeleteByJobPostingId(int JobPostingId);
    }

    #endregion
}
