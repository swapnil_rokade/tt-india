﻿using System;

namespace TPS360.DataAccess
{
    #region IASPNetMembership

    public interface IASPNetMembershipDataAccess
    {
        bool LockUser(Guid userId);
    }

    #endregion
}