﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberObjectiveAndSummaryDataAccess

    public interface IMemberObjectiveAndSummaryDataAccess
    {
        MemberObjectiveAndSummary Add(MemberObjectiveAndSummary memberObjectiveAndSummary);

        MemberObjectiveAndSummary Update(MemberObjectiveAndSummary memberObjectiveAndSummary);

        MemberObjectiveAndSummary GetById(int id);

        MemberObjectiveAndSummary GetByMemberId(int memberId);

        IList<MemberObjectiveAndSummary> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}