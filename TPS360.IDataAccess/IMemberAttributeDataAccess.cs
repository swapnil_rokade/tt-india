﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberAttributeDataAccess

    public interface IMemberAttributeDataAccess
    {
        MemberAttribute Add(MemberAttribute memberAttribute);

        MemberAttribute Update(MemberAttribute memberAttribute);

        MemberAttribute GetById(int id);

        IList<MemberAttribute> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}