﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region ICompanyStatusChangeRequestDataAccess

    public interface ICompanyStatusChangeRequestDataAccess
    {
        CompanyStatusChangeRequest Add(CompanyStatusChangeRequest companyStatusChangeRequest);

        CompanyStatusChangeRequest Update(CompanyStatusChangeRequest companyStatusChangeRequest);

        bool Verify(int companyId, ApproveStatus approveStatus, string comments, int changerId);

        CompanyStatusChangeRequest GetById(int id);

        CompanyStatusChangeRequest GetByCompanyId(int companyId);

        IList<CompanyStatusChangeRequest> GetAllByCompanyId(int companyId);

        IList<CompanyStatusChangeRequest> GetAll();

        bool DeleteById(int id);

        bool DeleteByCompanyId(int companyId);
    }

    #endregion
}