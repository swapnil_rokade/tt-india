﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: ICommonNoteDataAccess.cs
    Description: This is the class library page.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author             Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              Jan-09-2008           Jagadish            Defect ID: 9073; Added an overload for method 'GetAllByMemberId'.
    0.2              Feb-2-2009            Gopala Swamy        Defect ID: 9061 ;Added new parameter "string sortExpression" to method called "GetAllByProjectId"
-------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ICommonNoteDataAccess

    public interface ICommonNoteDataAccess
    {
        CommonNote Add(CommonNote commonNote);

        CommonNote Update(CommonNote commonNote);

        CommonNote GetById(int id);

        IList<CommonNote> GetAllByMemberId(int memberId);

        IList<CommonNote> GetAllByMemberId(int memberId, string SortExpression); //0.1

        IList<CommonNote> GetAllByCompanyId(int companyId, string SortExpression);

        IList<CommonNote> GetAllByCampaignId(int campaignId);

        IList<CommonNote> GetAllByProjectId(int projectId, string sortExpression);//0.2

        IList<CommonNote> GetAll();

        bool DeleteById(int id);

        CommonNote GetByCompanyIdAndNoteCategoryLookUpId(int companyId, int noteCategoryLookUpId);
    }

    #endregion
}