﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IJobPostingSkillSetDataAccess.cs
    Description: This is the class library page.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              Nov-10-2008           Jagadish            Defect ID: 9184; (Kaizen) Added a method declaration called 'DeleteByJobPostingID'
                                                               to delete jobposting skills having same JobPostingId.
-------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IJobPostingSkillSetDataAccess

    public interface IJobPostingSkillSetDataAccess
    {
        JobPostingSkillSet Add(JobPostingSkillSet jobPostingSkillSet);

        JobPostingSkillSet Update(JobPostingSkillSet jobPostingSkillSet);

        JobPostingSkillSet GetById(int id);

        JobPostingSkillSet GetBySkillId(int skillId);

        JobPostingSkillSet GetByJobPostingIdAndSkillId(int jobPostingId, int skillId);

        IList<JobPostingSkillSet> GetAll();

        bool DeleteById(int id);        

        bool DeleteByJobPostingID(int id); // 0.1

        IList<JobPostingSkillSet> GetAllByJobPostingId(int jobPostingId);

    }

    #endregion
}