﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System;

namespace TPS360.DataAccess
{
    #region ISkillDataAccess

    public interface ISkillDataAccess
    {
        Skill Add(Skill skill);

        Skill Update(Skill skill);

        Skill GetById(int id);

        IList<Skill> GetAll();

        IList<Skill> GetAllParent();

        PagedResponse<Skill> GetPaged(PagedRequest request);

        bool DeleteById(int id);

        IList<Skill> GetAllSkillByParentId(int parentId, string search);

        Int32 GetSkillIdBySkillName(string skill);

        IList<Skill> GetAllBySearch(string keyword, int count);

        string GetAndAddSkillIds(string skillxml, int CreatorID);

    }

    #endregion
}