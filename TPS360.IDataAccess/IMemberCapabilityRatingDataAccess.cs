﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberCapabilityRatingDataAccess

    public interface IMemberCapabilityRatingDataAccess
    {
        MemberCapabilityRating Add(MemberCapabilityRating memberCapabilityRating);

        MemberCapabilityRating Update(MemberCapabilityRating memberCapabilityRating);

        MemberCapabilityRating GetById(int id);

        IList<MemberCapabilityRating> GetAll();

        bool DeleteById(int id);

        IList<MemberCapabilityRating> GetAllByMemberIdAndPositionId(int memberId, int positionId);

        bool DeleteByMemberIdAndPositionId(int memberId, int positionId);

        MemberCapabilityRating GetByMemberIdAndPositionIdAndCapabilityTypeAndPositionCapabilityMapId(int memberId, int positionId, int capabilityType, int positionCapabilityMapId);
    }

    #endregion
}