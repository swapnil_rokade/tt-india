﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName:ICountryDataAccess.cs
    Description: This page is used for Countries  functionality
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            July-7-2009           Gopala Swamy J         Misc:Added 1) GetStateIdByStateCode() 2) GetCountryIdByCountryCode() to assist 
                                                                Resume parser team
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using System;

namespace TPS360.DataAccess
{
    #region IDepartmentDataAccess

    public interface IDepartmentDataAccess
    {
        Department Add(Department country);

        Department Update(Department country);

        Department GetById(int id);

        IList<Department> GetAll();

        bool DeleteById(int id);

        Int32 GetDepartmentIdByDepartmentName(string DepartmentName);

        IList<Department> GetAllBySearch(string keyword, int count);

    }

    #endregion
}