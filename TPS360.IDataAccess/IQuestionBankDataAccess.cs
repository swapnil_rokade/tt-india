﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IQuestionBankDataAccess.cs
    Description: This is the DataAccess  library page.
    Created By: Prasanth Kumar G
    Created On: 15/Jul/2015
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    
*/
using System.Collections.Generic;
using TPS360.Common.Shared;
using TPS360.Common.BusinessEntities;



namespace TPS360.DataAccess
{
    #region IQuestionBankDataAccess

    public interface IQuestionBankDataAccess
    {
        QuestionBank Add(QuestionBank QuestionBank);

        QuestionBank Update(QuestionBank QuestionBank);

        QuestionBank GetById(int id);

        IList<QuestionBank> GetAll();
        IList<QuestionBank> GetAll(string SortExpression);
        PagedResponse<QuestionBank> GetPaged(PagedRequest request);

     
        bool DeleteById(int id);
    }

    #endregion
}