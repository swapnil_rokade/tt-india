﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberSkillSetDataAccess

    public interface IMemberSkillSetDataAccess
    {
        MemberSkillSet Add(MemberSkillSet memberSkillSet);

        MemberSkillSet Update(MemberSkillSet memberSkillSet);

        MemberSkillSet GetById(int id);

        IList<MemberSkillSet> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}