﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:MemberPortal.master.cs
    Description:This is one of the master page for displaying Links to "My Account","Logout","Home"
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Jan-13-2009          Gopala Swamy          Enhancement  id:8675; Set Property " ctlLeftPanel.CurrentSiteMapType" to Employee Portal menu 
 *  0.2            Jun-29-2009              Veda             Defect ID:10511; Modified Default Sitemap 
-------------------------------------------------------------------------------------------------------------------------------------------       
*/


using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Web.Security;
using AjaxControlToolkit;

namespace TPS360.Web.UI
{
    public partial class MemberPortalMaster : BaseMasterPage
    {
        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //try
            //{
            //    if (Facade.IsAvailableSessionId(Session.SessionID) != 1)
            //    {
            //        FormsAuthentication.SignOut();
            //        FormsAuthentication.RedirectToLoginPage();
            //        return;
            //    }
            //}
            //catch { }
            Response.Expires = -1;  
            if (!IsPostBack)
            {
                if (Request.Url.ToString().ToLower().Contains("email"))
                {
                    lblTitle.Text = "Email History";
                }

                if (IsUserEmployee)
                {
                    ctlLeftPanel.CurrentSiteMapType = (int)SiteMapType.EmployeePortalMenu;
                    ctlLeftPanel.EnablePermissionChecking = false;
                }
                else if (IsUserCandidate)
                {
                    ctlLeftPanel.CurrentSiteMapType = (int)SiteMapType.CandidateCareerPortalMenu;
                    ctlLeftPanel.EnablePermissionChecking = false;
                }
                else if (IsUserConsultant)
                {
                    ctlLeftPanel.EnablePermissionChecking = false;
                }
                else if (IsUserAdmin)
                {
                    ctlLeftPanel.CurrentSiteMapType = (int)SiteMapType.EmployeePortalMenu;
                    ctlLeftPanel.EnablePermissionChecking = false;
                }
                if (!MiscUtil.ReturnDashboard(Request.RawUrl, CurrentMember.Id, Facade))
                {
                    Helper.Url.Redirect(UrlConstants.Dashboard.DEFAULT_PAGE);
                }
            }
        }

        #endregion
    }
}