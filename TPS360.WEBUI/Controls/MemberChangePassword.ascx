﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MemberChangePassword.ascx.cs"
    Inherits="TPS360.Web.UI.ControlMemberChangePassword" %>
    <asp:Panel ID ="pnlChangePassword" runat ="server" DefaultButton ="btnSubmit">

    <div class="TabPanelHeader" id="divHeader" runat ="server"  >
        Change Password
    </div>    
<div>
    <div class="TableRow" style="text-align: center">
        <asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
        <asp:HiddenField ID="hfMemberId" runat="server" Value="0" />
    </div>
    <div class="TableRow">
        <div class="TableFormLeble">
            <asp:Label EnableViewState="false" ID="lblOldPassword" runat="server" Text="Old Password"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:TextBox EnableViewState="false" ID="txtOldPassword" runat="server" CssClass="CommonTextBox" autocomplete="off" TextMode="Password"></asp:TextBox>
            <span class="RequiredField">*</span>
        </div>
    </div>
    <div class="TableRow">
        <div class="TableFormValidatorContent" style=" margin-left : 42%">
            <asp:RequiredFieldValidator ID="rfvOldPassword" runat="server" ControlToValidate="txtOldPassword"
                ErrorMessage="Please enter old password." EnableViewState="False" Display="Dynamic">
            </asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="TableRow">
        <div class="TableFormLeble">
            <asp:Label EnableViewState="false" ID="lblNewPassword" runat="server" Text="New Password"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:TextBox EnableViewState="false" ID="txtNewPassword" runat="server" CssClass="CommonTextBox" TextMode="Password"></asp:TextBox>
            <span class="RequiredField">*</span>
        </div>
    </div>
    <div class="TableRow">
        <div class="TableFormValidatorContent"  style=" margin-left : 42%">
            <asp:RequiredFieldValidator ID="rfvNewPassword" runat="server" ControlToValidate="txtNewPassword"
                ErrorMessage="Please enter new password." EnableViewState="False" Display="Dynamic">
            </asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="revNewPassword" runat="server" ControlToValidate="txtNewPassword"
                Display="Dynamic" 
                ErrorMessage="Please enter at least 8 characters." EnableViewState="false"
                ValidationExpression="^[\s\S]{8,}$"></asp:RegularExpressionValidator>
        </div>
    </div>
    <div class="TableRow">
        <div class="TableFormLeble">
            <asp:Label EnableViewState="false" ID="lblConfirmPassword" runat="server" Text="Confirm Password"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:TextBox EnableViewState="false" ID="txtConfirmPassword" runat="server" CssClass="CommonTextBox" TextMode="Password"></asp:TextBox>
            <span class="RequiredField">*</span>
        </div>
    </div>
    <div class="TableRow">
        <div class="TableFormValidatorContent"  style=" margin-left : 42%">
            <asp:RequiredFieldValidator ID="rfvConfirmPassword" runat="server" ControlToValidate="txtConfirmPassword"
                ErrorMessage="Please retype new password." EnableViewState="False" Display="Dynamic">
            </asp:RequiredFieldValidator>
            <asp:CompareValidator ID="cvNewPassword" runat="server" ControlToValidate="txtConfirmPassword"
                Type="String" Operator="Equal" ControlToCompare="txtNewPassword" ErrorMessage="Passwords must match."
                EnableViewState="False" Display="Dynamic"></asp:CompareValidator>
        </div>
    </div>
    <br />
    <div class="TableRow" style="text-align: center;">
        <asp:Button ID="btnSubmit" CssClass="CommonButton" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
    </div>
</div>
</asp:Panel>