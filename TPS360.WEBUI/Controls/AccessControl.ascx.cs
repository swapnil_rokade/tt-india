﻿using System;
using System.Web.Security;

using TPS360.Common.Helper;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;
using System.Web.UI.WebControls;

namespace TPS360.Web.UI
{
    public partial class ctlAccessControl : BaseControl
    {
        #region Member Variables

        private static Guid _userId;
        private static int _memberId = 0;

        #endregion

        #region Properties

        public Guid UserId
        {
            set { _userId = value; }
        }

        #endregion

        #region Methods

        private void GetMemberId()
        {
            //From Member Profile
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                _memberId =Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);

            }
            //From Member Login
            else
            {
                _memberId = base.CurrentMember.Id;
            }
        }

        private void PrepareView()
        {
            ControlHelper.PopulateEnumIntoList(ddlAccessStatus, typeof(AccessStatus));
        }

        private void PrepareEditView()
        {
            if (_memberId > 0)
            {
                Member member = Facade.GetMemberById(_memberId);
                if (member != null)
                {
                    ddlAccessStatus.SelectedValue = member.Status.ToString();
                }
            }
        }

        private void UpdateAccessControl()
        {
            if (IsValid)
            {
                try
                {
                    if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
                    {
                        _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
                    }

                    Member member = Facade.GetMemberById(_memberId);

                    if (member != null)
                    {
                        _userId = member.UserId;
                    }
                    else
                    {
                        return;
                    }

                    if (ddlAccessStatus.SelectedIndex == 0)
                    {
                        MembershipUser thisUser = Membership.GetUser(_userId);
                        if (thisUser.UnlockUser())
                        {
                            member.UpdatorId = base.CurrentMember.Id;
                            member.Status = (int)AccessStatus.Enabled;
                            Facade.UpdateMemberStatus(member);
                            MiscUtil.ShowMessage(lblMessage, "User has been unlocked successfully.", false);
                        }
                    }
                    else if (ddlAccessStatus.SelectedIndex == 1)
                    {
                        if (Facade.LockUser(_userId))
                        {
                            member.UpdatorId = base.CurrentMember.Id;
                            member.Status = (int)AccessStatus.Blocked;
                            Facade.UpdateMemberStatus(member);
                            MiscUtil.ShowMessage(lblMessage, "User has been locked successfully.", false);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                }
            }
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            PrepareView();
            if (!IsPostBack)
            {
                GetMemberId();               
                PrepareEditView();
            }
            if (IsPostBack)
            {
                MiscUtil.LoadAllControlState(Request, this);
            }
        }
        
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            UpdateAccessControl();
        }

        #endregion
    }
}