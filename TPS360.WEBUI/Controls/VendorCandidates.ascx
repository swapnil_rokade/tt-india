<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VendorCandidates.ascx.cs"
    Inherits="VendorCandidates" %>

<script>
    function HeaderCheckBoxClicked(chkAllItem) {
        $("table#tlbTemplate tbody tr").each(function() {
            $(this).find(":checkbox").attr('checked', chkAllItem.checked);
        });
        SelectRowCheckbox();

    }

    function SelectRowCheckbox(param1, param2) {



        // debugger;
        var ischeckall = true;
        var HeaderCheckbox = document.getElementById('chkAllItem');
        var elements = HeaderCheckbox.form.elements;

        for (var i = 0; i < elements.length; i++) {
            if ((elements[i].type == 'checkbox') && (elements[i].id != HeaderCheckbox.id)) {
                if (elements[i].checked == true) {
                    var hndId = elements[i + 1];

                    if (IsNumeric(hndId.value)) {
                        ADDID(hndId.value, '<%= hdnSelectedIDS.ClientID %>');
                    }

                }
                else {
                    var hndId = elements[i + 1];
                    if (IsNumeric(hndId.value)) {
                        RemoveID(hndId.value, '<%= hdnSelectedIDS.ClientID %>');
                    }
                }
            }
        }
        HeaderCheckbox.checked = ischeckall;
    }
</script>

<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<asp:Panel ID="pnlSettings" runat="server">
</asp:Panel>
<asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
<asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>
<asp:HiddenField ID="hdnSelectedIDS" runat="server" />
<asp:TextBox ID="SortColumn" runat="server" EnableViewState="true" Visible="false" />
<asp:TextBox ID="SortOrder" runat="server" EnableViewState="true" Visible="false" />
<asp:Panel ID="widgetBody" runat="server">
    <asp:ObjectDataSource ID="odsCandidateList" runat="server" SelectMethod="GetPaged"
        TypeName="TPS360.Web.UI.CandidateDataSource" SelectCountMethod="GetListCount"
        EnablePaging="True" SortParameterName="sortExpression">
        <SelectParameters>
            <asp:Parameter Name="memberId" Type="Int32" />
            <asp:Parameter Name="IsVendor" Type="Boolean" DefaultValue="false" />
            <asp:Parameter Name="IsVendorContact" Type="Boolean" DefaultValue="true" />
            <asp:Parameter Name="DateFrom" Type="DateTime" />
            <asp:Parameter Name="DateTo" Type="DateTime" />
            <asp:Parameter Name="SortOrder" DefaultValue="asc" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <div class="TabPanelHeader">
        Vendor Candidates
    </div>
    <asp:ListView ID="lsvCandidateList" runat="server" DataSourceID="odsCandidateList"
        EnableViewState="true" DataKeyNames="Id" OnItemDataBound="lsvCandidateList_ItemDataBound"
        OnItemCommand="lsvCandidateList_ItemCommand" OnPreRender="lsvCandidateList_PreRender">
        <LayoutTemplate>
            <table id="tlbTemplate" class="Grid" cellspacing="0" border="0">
                <tr runat="server" id="trRecentApplicant">
                    <th style="min-width: 100px; white-space: nowrap;">
                        <asp:LinkButton ID="btnCreatedate" runat="server" ToolTip="Sort By CreatedDate" CommandName="Sort"
                            CommandArgument="[C].[CreateDate]" Text="Date Created" />
                    </th>
                    <th style="min-width: 100px; white-space: nowrap;">
                        <asp:LinkButton ID="btnName" runat="server" ToolTip="Sort By Name" CommandName="Sort"
                            CommandArgument="[C].[FirstName]" Text=" Candidate Name" />
                    </th>
                    <th>
                        <asp:LinkButton ID="btnSource" runat="server" ToolTip="Sort By Source" CommandName="Sort"
                            CommandArgument="[M].[FirstName]" Text="Source" />
                    </th>
                </tr>
                <tr id="itemPlaceholder" runat="server">
                </tr>
                <tr class="Pager">
                    <td colspan="3">
                        <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                    </td>
                </tr>
            </table>
        </LayoutTemplate>
        <EmptyDataTemplate>
            <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server"
                style="width: 100%; margin: 0px 0px;">
                <tr>
                    <td>
                        No candidates available.
                    </td>
                </tr>
            </table>
        </EmptyDataTemplate>
        <ItemTemplate>
            <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                <td>
                    <asp:Label ID="lblCreatedDate" runat="server" />
                </td>
                <td>
                    <asp:HyperLink ID="lnkCandidateName" Target="_blank" runat="server"></asp:HyperLink>
                </td>
                <td>
                    <asp:LinkButton ID="lblSource" runat="server" />
                </td>
            </tr>
        </ItemTemplate>
    </asp:ListView>
</asp:Panel>
