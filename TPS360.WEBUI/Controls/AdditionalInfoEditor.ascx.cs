﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: AdditionalInfoEditor.ascx.cs
    Description: This is the user control page used in resume builder to allow user to provide additional information.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Sep-11-2008          Jagadish            Defect id: 8702; Range validator to validate date of birth has added.
    0.2             Nov-24-2008          Jagadish            Defect id: 9314; (Kaizen Issue) Removed the fields country of birth,city of birth,country of residence,Gender,
                                                             marital status,the Permanent Address section and all salary fields from the Additional Info tab and added a method PopulateSiteSettings()
                                                             only for the Consultant resume builder.
    0.3             Dec-10-2008          Shivanand           Defect id: 9494; In the method PrepareEditView(), textbox "txtSSN" is populated from memberDetail object.
                                                                           In the method BuildMemberDetail(), SSNPAN field of memberDetail object is asigned with value from textbox "txtSSN".
    0.4             Dec-15-2008          Shivanand           Defect id: 9533; Current company fields are hidden for consultant role.
    0.5             Dec-22-2008          Shivanand           Defect id: 9511; _memberId is declared as static,new private members declared "_experienceId" & "_memberExperience".
                                                                            Property included "CurrentMemberExperience".
                                                                            New methods added "SaveMemberExperience()" & BuildMemberExperience().
                                                                            In the method Page_Load(), "divEmplyerInfo" is made visible for consultant role.
                                                                            In the method btnSaveMember_Click(), the method SaveMemberExperience() is called.
                                                                            New method added "chkDurationTillAddInfo_CheckedChanged()" to handle checkbox event.
    0.6             Feb-18-2009         Jagadish             Defect id: 9827; Changes made in method "chkIsPermanentAddress_Click()".
    0.7             Mar-03-2009         Jagadish             Defect id: 10038; Changes made in the method 'Page_Load()'.
    0.8             April-06-2009       Nagarathna V.B       Defect Id:9545; changes made in "BuildMemberDetail" check for the SSN duplication.
    0.9             Apr-21-2009         Shivanand            Defect #10169; In the method PrepareEditView(),textboxes txtExpectedMaxYearlySalary,txtExpectedMaxMonthlySalary & txtExpectedMaxHourlySalary are assigned to values
                                                                            from memberExtendedInfo object and in method BuildMemberExInfo(), these textbox values are used to build entity "memberExtendedInfo"
    1.0             Apr-22-2009         Shivanand            Defect #10218; Changes made in the methods SaveMember() & SaveMemberExperience().
    1.1             Apr-27-2009         Nagarathna V.B       Defect #10406 : changes are made in "BuildMemberExInfo()" 
    1.2             Sep-13-2009         Gopala Swamy J       Defect #11576 : 
    1.3             Nov-23-2009         Rajendra             Defect #11694 : Changes are made in PrepareEditView() and chkIsPermanentAddress_Click() and added new method chkAddress();
    1.4             DEC-03-2009         Rajendra             Defect #11694(Rework): Changes are made in Page_Load();
    1.5             Jan-27-2010         Nagarathna.V.B       Defect #11825,11826: commented "chkIsPermanentAddress_Click" in page_load event.
 *  1.6             Apr-21-2010         Sudarshan.R.         Defect ID:12683 Changes made to load default country from Admin Settings
------------------------------------------------------------------------------------------------------------------------------------------- 
*/
using System.Linq;
using System.Text;
using System;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using System.Collections;
using System.Collections.Generic;
using TPS360.Web.UI.Helper;
using System.Web.UI.WebControls; 
using System.Text.RegularExpressions;
using System.Configuration;

namespace TPS360.Web.UI
{
    public partial class ControlsAdditionalInfoEditor : BaseControl
    {
        #region Private variable

        private static int _memberId = 0; 
        Member _member;
        MemberDetail _memberDetail;
        MemberExtendedInformation _memberExtendedInfo;
        private static string _memberrole = string.Empty;

        private static Member memberArchiveInfo = null;
        private static MemberDetail memberDetailArchiveInfo = null;
        private static MemberExtendedInformation memberExtInfoArchiveInfo = null;
        private static Hashtable hashTableArchiveInfo = null;
        private static int _experienceId = 0;
        MemberExperience _memberExperience; 

        #endregion

        #region Properties

        private Member CurrentMemberInfo
        {
            get
            {
                if (_member == null)
                {
                    //From Member Profile
                    if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
                    {
                        _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
                    }
                    //From Member Login
                    else
                    {
                        _memberId = base.CurrentMember.Id;
                    }

                    if (_memberId > 0)
                    {
                       
                        _member = Facade.GetMemberById(_memberId);
                    }

                    if (_member == null)
                    {
                        _member = new Member();
                    }
                    else
                    {
                        memberArchiveInfo = (Member)_member.Clone();
                    }
                }
                return _member;
            }
        }

        private MemberDetail CurrentMemberDetails
        {
            get
            {
                if (_memberDetail == null)
                {
                    if (_memberId > 0)
                    {
                        _memberDetail = Facade.GetMemberDetailByMemberId(_memberId);
                    }
                    else
                    {
                        _memberDetail = Facade.GetMemberDetailByMemberId(base.CurrentMember.Id);
                    }

                    if (_memberDetail == null)
                    {
                        _memberDetail = new MemberDetail();
                    }
                    else
                    {
                        memberDetailArchiveInfo = (MemberDetail)_memberDetail.Clone();
                    }
                }

                return _memberDetail;
            }
        }

        private MemberExtendedInformation CurrentMemberExInfo
        {
            get
            {
                if (_memberExtendedInfo == null)
                {
                    if (_memberId > 0)
                    {
                        _memberExtendedInfo = Facade.GetMemberExtendedInformationByMemberId(_memberId);
                    }
                    else
                    {
                        _memberExtendedInfo = Facade.GetMemberExtendedInformationByMemberId(base.CurrentMember.Id);
                    }

                    if (_memberExtendedInfo == null)
                    {
                        _memberExtendedInfo = new MemberExtendedInformation();
                    }
                    else
                    {
                        memberExtInfoArchiveInfo = (MemberExtendedInformation)_memberExtendedInfo.Clone();
                    }
                }

                return _memberExtendedInfo;
            }
        }

        public string MemberRole
        {
            set { _memberrole = value; }
            get { return _memberrole; }
        }

        public int WorkFlowId
        {
            get
            {
                return (int)(ViewState[this.ClientID + "_WorkFlowId"] ?? 0);
            }
            set
            {
                ViewState[this.ClientID + "_WorkFlowId"] = value;
            }
        }

        public bool IsInformationArchived
        {
            get
            {
                return (bool)(ViewState[this.ClientID + "_IsInformationArchived"] ?? false);
            }
            set
            {
                ViewState[this.ClientID + "_IsInformationArchived"] = value;
            }
        }

        MemberExperience CurrentMemberExperience
        {
            get
            {                
                if (_memberExperience == null)
                {
                    if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_ID]))
                    {
                        _experienceId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_ID]);
                    }


                    if (_experienceId > 0)
                    {
                        _memberExperience = Facade.GetMemberExperienceById(_experienceId);
                    }

                    else if (_memberId > 0)
                    {
                        IList<MemberExperience> lstMemberExperience = Facade.GetAllMemberExperienceByMemberId(_memberId);
                        if (lstMemberExperience != null)
                        {
                            _memberExperience = lstMemberExperience[0];
                        }
                        else
                        {
                            _memberExperience = new MemberExperience();
                        }                        
                    }                    
                }
                return _memberExperience;
            }
        }

        #endregion

        #region Methods

        private void SaveMember()
        {
            if (IsValid)
            {
                try
                {
                    Member member = BuildMember();
                    MemberDetail memberDetail = BuildMemberDetail();
                    MemberExtendedInformation memberExInfo = BuildMemberExInfo();

                    if (member.IsNew)
                    {
                        Facade.AddMember(member);
                        MiscUtil.ShowMessage(lblMessage, "Additional information has been added successfully.", false);
                    }
                    else
                    {
                        Facade.UpdateMember(member);
                        MiscUtil.ShowMessage(lblMessage, "Additional information has been updated successfully.", false);
                        if (IsInformationArchived)
                        {
                            hashTableArchiveInfo = new Hashtable();
                            if (memberArchiveInfo != member)
                            {
                                if (memberArchiveInfo != null && member != null)
                                {
                                    if (memberArchiveInfo.PermanentAddressLine1 != member.PermanentAddressLine1)
                                    {
                                        hashTableArchiveInfo.Add("PermamentAddressLine1", memberArchiveInfo.PermanentAddressLine1);
                                    }
                                    if (memberArchiveInfo.PermanentAddressLine2 != member.PermanentAddressLine2)
                                    {
                                        hashTableArchiveInfo.Add("PermamentAddressLine2", memberArchiveInfo.PermanentAddressLine2);
                                    }
                                    if (memberArchiveInfo.PermanentCountryId != member.PermanentCountryId)
                                    {
                                        hashTableArchiveInfo.Add("PermamentCountryId", memberArchiveInfo.PermanentCountryId);
                                    }
                                    if (memberArchiveInfo.PermanentStateId != member.PermanentStateId)
                                    {
                                        hashTableArchiveInfo.Add("PermamentStateId", memberArchiveInfo.PermanentStateId);
                                    }
                                    if (memberArchiveInfo.PermanentCity != member.PermanentCity)
                                    {
                                        hashTableArchiveInfo.Add("PermamentCity", memberArchiveInfo.PermanentCity);
                                    }
                                    if (memberArchiveInfo.PermanentZip != member.PermanentZip)
                                    {
                                        hashTableArchiveInfo.Add("PermamentZip", memberArchiveInfo.PermanentZip);
                                    }
                                    if (memberArchiveInfo.PermanentPhone != member.PermanentPhone)
                                    {
                                        hashTableArchiveInfo.Add("PermanentPhone", member.PermanentPhone + " Ext. " + member.PermanentPhoneExt);
                                    }
                                    if (memberArchiveInfo.PermanentMobile != member.PermanentMobile)
                                    {
                                        hashTableArchiveInfo.Add("PermanentMobile", member.PermanentMobile);
                                    }
                                    if (memberArchiveInfo.DateOfBirth != member.DateOfBirth)
                                    {
                                        hashTableArchiveInfo.Add("DateOfBirth", member.DateOfBirth);
                                    }
                                }
                            }

                            if (memberDetailArchiveInfo != memberDetail)
                            {

                                if (memberDetailArchiveInfo != null && memberDetail != null)
                                {
                                    if (memberDetailArchiveInfo.CountryIdOfBirth != memberDetail.CountryIdOfBirth)
                                    {
                                        hashTableArchiveInfo.Add("CountryIdOfBirth", memberDetail.CountryIdOfBirth);
                                    }
                                    if (memberDetailArchiveInfo.CountryIdOfCitizenship != memberDetail.CountryIdOfCitizenship)
                                    {
                                        hashTableArchiveInfo.Add("CountryIdOfResidence", memberDetail.CountryIdOfCitizenship);
                                    }
                                    if (memberDetailArchiveInfo.GenderLookupId != memberDetail.GenderLookupId)
                                    {
                                        hashTableArchiveInfo.Add("Gender", memberDetail.GenderLookupId);
                                    }
                                    if (memberDetailArchiveInfo.MaritalStatusLookupId != memberDetail.MaritalStatusLookupId)
                                    {
                                        hashTableArchiveInfo.Add("MaritalStatus", memberDetail.MaritalStatusLookupId);
                                    }
                                }
                            }
                            if (memberExtInfoArchiveInfo != memberExInfo)
                            {
                                if (memberExtInfoArchiveInfo != null && memberExInfo != null)
                                {
                                    if (memberExtInfoArchiveInfo.CurrentYearlyRate != memberExInfo.CurrentYearlyRate)
                                    {
                                        hashTableArchiveInfo.Add("CurrentYearlySalary", memberExInfo.CurrentYearlyRate);
                                    }
                                    if (memberExtInfoArchiveInfo.CurrentYearlyCurrencyLookupId != memberExInfo.CurrentYearlyCurrencyLookupId)
                                    {
                                        hashTableArchiveInfo.Add("CurrentYearlyCurrencyLookupId", memberExInfo.CurrentYearlyCurrencyLookupId);
                                    }
                                    if (memberExtInfoArchiveInfo.CurrentMonthlyRate != memberExInfo.CurrentMonthlyRate)
                                    {
                                        hashTableArchiveInfo.Add("CurrentMonthlySalary", memberExInfo.CurrentMonthlyRate);
                                    }
                                    if (memberExtInfoArchiveInfo.CurrentMonthlyCurrencyLookupId != memberExInfo.CurrentMonthlyCurrencyLookupId)
                                    {
                                        hashTableArchiveInfo.Add("CurrentMonthlyCurrencyLookupId", memberExInfo.CurrentMonthlyCurrencyLookupId);
                                    }
                                    if (memberExtInfoArchiveInfo.CurrentHourlyRate != memberExInfo.CurrentHourlyRate)
                                    {
                                        hashTableArchiveInfo.Add("CurrentHourlySalary", memberExInfo.CurrentHourlyRate);
                                    }
                                    if (memberExtInfoArchiveInfo.CurrentHourlyCurrencyLookupId != memberExInfo.CurrentHourlyCurrencyLookupId)
                                    {
                                        hashTableArchiveInfo.Add("CurrentHourlyCurrencyLookupId", memberExInfo.CurrentHourlyCurrencyLookupId);
                                    }
                                    if (memberExtInfoArchiveInfo.ExpectedYearlyRate != memberExInfo.ExpectedYearlyRate)
                                    {
                                        hashTableArchiveInfo.Add("ExpectedYearlySalary", memberExInfo.ExpectedYearlyRate);
                                    }
                                    if (memberExtInfoArchiveInfo.ExpectedYearlyCurrencyLookupId != memberExInfo.ExpectedYearlyCurrencyLookupId)
                                    {
                                        hashTableArchiveInfo.Add("ExpectedYearlyCurrencyLookupId", memberExInfo.ExpectedYearlyCurrencyLookupId);
                                    }
                                    if (memberExtInfoArchiveInfo.ExpectedMonthlyRate != memberExInfo.ExpectedMonthlyRate)
                                    {
                                        hashTableArchiveInfo.Add("ExpectedMonthlySalary", memberExInfo.ExpectedMonthlyRate);
                                    }
                                    if (memberExtInfoArchiveInfo.ExpectedMonthlyCurrencyLookupId != memberExInfo.ExpectedMonthlyCurrencyLookupId)
                                    {
                                        hashTableArchiveInfo.Add("ExpectedMonthlyCurrencyLookupId", memberExInfo.ExpectedMonthlyCurrencyLookupId);
                                    }
                                    if (memberExtInfoArchiveInfo.ExpectedHourlyRate != memberExInfo.ExpectedHourlyRate)
                                    {
                                        hashTableArchiveInfo.Add("ExpectedHourlySalary", memberExInfo.ExpectedHourlyRate);
                                    }
                                    if (memberExtInfoArchiveInfo.ExpectedHourlyCurrencyLookupId != memberExInfo.ExpectedHourlyCurrencyLookupId)
                                    {
                                        hashTableArchiveInfo.Add("ExpectedHourlyCurrencyLookupId", memberExInfo.ExpectedHourlyCurrencyLookupId);
                                    }
                                    if (memberExtInfoArchiveInfo.SalaryTypeLookupId != memberExInfo.SalaryTypeLookupId)
                                    {
                                        hashTableArchiveInfo.Add("SalaryPaymentType", memberExInfo.SalaryTypeLookupId);
                                    }
                                    if (memberExtInfoArchiveInfo.LastEmployer != memberExInfo.LastEmployer)
                                    {
                                        hashTableArchiveInfo.Add("CurrentCompany", memberExInfo.LastEmployer);
                                    }
                                    if (memberExtInfoArchiveInfo.SecurityClearance != memberExInfo.SecurityClearance)
                                    {
                                        hashTableArchiveInfo.Add("SecurityClearance", memberExInfo.SecurityClearance);
                                    }
                                    if (memberExtInfoArchiveInfo.PassportStatus != memberExInfo.PassportStatus)
                                    {
                                        hashTableArchiveInfo.Add("PassportStatus", memberExInfo.PassportStatus);
                                    }
                                    if (memberExtInfoArchiveInfo.WorkAuthorizationLookupId != memberExInfo.WorkAuthorizationLookupId)
                                    {
                                        hashTableArchiveInfo.Add("WorkPermitStatus", memberExInfo.WorkAuthorizationLookupId);
                                    }
                                    if (memberExtInfoArchiveInfo.WillingToTravel != memberExInfo.WillingToTravel)
                                    {
                                        hashTableArchiveInfo.Add("WillingToTravel", memberExInfo.WillingToTravel);
                                    }
                                    if (memberExtInfoArchiveInfo.JobTypeLookupId != memberExInfo.JobTypeLookupId)
                                    {
                                        hashTableArchiveInfo.Add("JobLength", memberExInfo.JobTypeLookupId);
                                    }
                                }

                                MemberChangeArchive memberChangeArchive = new MemberChangeArchive();
                                memberChangeArchive.ChangedObject = ObjectEncrypter.Encrypt<Hashtable>(hashTableArchiveInfo);
                                memberChangeArchive.MemberId = member.Id;
                                memberChangeArchive.CreatorId = base.CurrentMember.Id;

                                Facade.AddMemberChangeArchive(memberChangeArchive);
                            }
                        }
                    }
                    if (memberDetail.IsNew)
                    {
                        Facade.AddMemberDetail(memberDetail);
                    }
                    else
                    {
                        Facade.UpdateMemberDetail(memberDetail);
                    }
                    if (memberExInfo.IsNew)
                    {
                        Facade.AddMemberExtendedInformation(memberExInfo);
                    }
                    else
                    {
                        Facade.UpdateMemberExtendedInformation(memberExInfo);
                    }
                }
                catch (ArgumentException ex)
                {
                    MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                }
            }
        }
        private void PrepareView()
        {
            MiscUtil.PopulateCategory(ddlCategory, Facade);

            ListControl [] li={ddlCountryOfBirth,ddlCountryOfResidence};
            MiscUtil.PopulateCountry(li, Facade);
                       StringBuilder strLookuptypes = new StringBuilder();
            strLookuptypes.Append(((int)(LookupType.MaritalStatus)).ToString() + ",");
            strLookuptypes.Append(((int)(LookupType.Currency)).ToString() + ",");
            strLookuptypes.Append(((int)(LookupType.WorkAuthorizationType)).ToString() + ",");
            strLookuptypes.Append(((int)(LookupType.WorkSchedule)).ToString() + ",");
//            strLookuptypes.Append(((int)(LookupType.TaxTermType)).ToString() + ",");
            strLookuptypes.Append(((int)(LookupType.EmploymentDurationType)).ToString() + ",");
            strLookuptypes.Append(((int)(LookupType.IDProof )).ToString() + ",");
            strLookuptypes.Append(((int)(LookupType.Gender)).ToString() + ",");

            strLookuptypes.Append(((int)(LookupType.Industry)).ToString() + ",");
            strLookuptypes.Append(((int)(LookupType.JobFunction)).ToString());

            IList<GenericLookup> lst = Facade.GetAllGenericLookupByLookupType(strLookuptypes.ToString());
            PopulateGenericLookup(lst.Where(x => x.Type == (int)LookupType.MaritalStatus).ToList(), ddlMaritalStatus);
            ddlMaritalStatus.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));

            var currency = from s in lst
                           where s.Type == ((int)(LookupType.Currency))
                           select s;
            
            var ccurrency = currency.Skip(2).OrderBy(f => f.Name).ToList();
            var ss = currency.Take(2).ToList();
            ccurrency = ss.Concat(ccurrency).ToList();
            PopulateGenericLookup(ccurrency.ToList(), ddlCurrentYearlyCurrency);
            PopulateGenericLookup(ccurrency.ToList(), ddlExpectedYearlyCurrency);
            
            ddlCurrentYearlyCurrency.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT_CURRENCY, "0"));
            ddlExpectedYearlyCurrency.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT_CURRENCY, "0"));
         

            PopulateGenericLookup(lst.Where(x => x.Type == (int)LookupType.WorkAuthorizationType).ToList(), ddlWorkPermitStatus);
            PopulateGenericLookup(lst.Where(x => x.Type == (int)LookupType.WorkSchedule).ToList(), ddlWorkSchedule);
            ddlWorkPermitStatus.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));


            PopulateGenericLookup(lst.Where(x => x.Type == (int)LookupType.EmploymentDurationType).ToList(), ddlJobType);
            if (ddlJobType.GetType().Name != "CheckBoxList")
            {
                ddlJobType.Items.Insert(0, new ListItem("No Preference", "0"));    // 8971
            }
            PopulateGenericLookup(lst.Where(x => x.Type == (int)LookupType.Gender).ToList(), ddlGender);
            ddlGender.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));


            //PopulateGenericLookup(lst.Where(x => x.Type == (int)LookupType.Industry).ToList(), ddlIndustry);
            //ddlIndustry.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));

            //PopulateGenericLookup(lst.Where(x => x.Type == (int)LookupType.JobFunction).ToList(), ddlJobFunction);
            //ddlJobFunction.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));


            MiscUtil.PopulateIndustry(ddlIndustry, Facade);
            ddlIndustry.Items[0].Text = "Please Select";

            MiscUtil.PopulateJobFunction(ddlJobFunction, Facade);
            ddlJobFunction.Items[0].Text = "Please Select";



            PopulateGenericLookup(lst.Where(x => x.Type == (int)LookupType.IDProof).ToList(), ddlIDProof);
            ddlIDProof.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
            txtCurrentCompany.Text = string.Empty;
            ApplyDefaultsFromSiteSetting();
        }

        void PopulateGenericLookup(IList<GenericLookup> glookup, ListControl control)
        {
            control.DataSource = glookup;
            control.DataTextField = "Name";
            control.DataValueField = "Id";
            control.DataBind();
        }
        private void ApplyDefaultsFromSiteSetting()
        {
            if (SiteSetting != null)
            {
                ddlExpectedYearlyCurrency.SelectedValue = SiteSetting[DefaultSiteSetting.Currency.ToString()].ToString();
                ddlCurrentYearlyCurrency.SelectedValue = SiteSetting[DefaultSiteSetting.Currency.ToString()].ToString();
                if (ddlCurrentYearlyCurrency.SelectedItem.Text == "INR")
                {
                    lblCurrentSalaryCurrency.Text = "(Lacs)";
                    lblCurrentSalaryCurrency.Visible = true;
                }
                else
                {
                    lblCurrentSalaryCurrency.Text = "";
                }
                if (ddlExpectedYearlyCurrency.SelectedItem.Text == "INR")
                {

                    lblExpectedSalaryCurrency.Text = "(Lacs)";
                    lblExpectedSalaryCurrency.Visible = true;
                }
                else
                {
                    lblExpectedSalaryCurrency.Text = "";
                }
            }
        }
     
        private void PrepareEditView()
        {
            Member member = CurrentMemberInfo;
            MemberDetail memberDetail = CurrentMemberDetails;
            MemberExtendedInformation memberExtendedInfo = CurrentMemberExInfo;

            if (CurrentMemberInfo.DateOfBirth.ToShortDateString() != DateTime.MinValue.ToShortDateString())
            {            
                wdcDateOfBirth.Value = CurrentMemberInfo.DateOfBirth.ToShortDateString();
            }

            txtCityOfBirth.Text =MiscUtil .RemoveScript ( memberDetail.CityOfBirth,string .Empty );
            txtCityOfBirth.ToolTip =MiscUtil .RemoveScript ( memberDetail.CityOfBirth,string .Empty );
            if (!String.IsNullOrEmpty(Convert.ToString(memberDetail.CountryIdOfBirth)))
            {
                ddlCountryOfBirth.SelectedValue = Convert.ToString(memberDetail.CountryIdOfBirth);
            }
            else
            {
                ApplyDefaultsFromSiteSetting();
            }

            if (!String.IsNullOrEmpty(Convert.ToString(memberDetail.CountryIdOfCitizenship)))
            {
                ddlCountryOfResidence.SelectedValue = Convert.ToString(memberDetail.CountryIdOfCitizenship);
            }
            else
            {
                ApplyDefaultsFromSiteSetting();
            }

            if (CurrentMemberDetails.IsCurrentSameAsPrimaryAddress)
            {
                chkIsPermanentAddress.Checked = true;
                
                txtLocation.Text =MiscUtil .RemoveScript ( member.PermanentAddressLine1,string .Empty );
                txtPermanentAddress2.Text = MiscUtil .RemoveScript (member.PermanentAddressLine2,string .Empty );
                txtPermanentCity.Text = MiscUtil .RemoveScript (member.PermanentCity,string .Empty );
                uclCountryState .SelectedCountryId =member.PermanentCountryId;
                uclCountryState.SelectedStateId = member.PermanentStateId;
                txtPermanentZipCode.Text = MiscUtil .RemoveScript (member.PermanentZip,string .Empty );
                txtPermanentPhone.Text = MiscUtil .RemoveScript (member.PrimaryPhone,string .Empty );
                txtPermamentPhoneExt.Text = MiscUtil .RemoveScript (member.PrimaryPhoneExtension,string .Empty );
                txtPermanentMobile.Text = MiscUtil .RemoveScript (member.CellPhone,string .Empty );

                Session.Add("chkAddress", true); 
                chkAddress(); 
                            
            }
            else
            {
                chkIsPermanentAddress.Checked = false;

                txtLocation.Text = MiscUtil .RemoveScript (memberDetail.CurrentAddressLine1,string .Empty );
                txtPermanentAddress2.Text = MiscUtil .RemoveScript (memberDetail.CurrentAddressLine2,string .Empty );
                txtPermanentCity.Text =MiscUtil .RemoveScript ( memberDetail.CurrentCity,string .Empty );
                if (Convert.ToInt32(memberDetail.CurrentCountryId) > 0)
                {
                    uclCountryState.SelectedCountryId = memberDetail.CurrentCountryId;
                    uclCountryState.SelectedStateId = memberDetail.CurrentStateId;
                }
                else
                {
                    uclCountryState.SelectedCountryId = 0;
                }
                uclCountryState.SelectedStateId = memberDetail.CurrentStateId;
                txtPermanentZipCode.Text =MiscUtil .RemoveScript ( memberDetail.CurrentZip,string .Empty );

                txtPermanentPhone.Text = MiscUtil .RemoveScript (member.PermanentPhone,string .Empty );
                txtPermamentPhoneExt.Text =MiscUtil .RemoveScript ( member.PermanentPhoneExt,string .Empty );
                txtPermanentMobile.Text = MiscUtil .RemoveScript (member.PermanentMobile,string .Empty );

                Session.Add("chkAddress", false); 
                chkAddress(); 
            }

            ddlGender.SelectedValue = Convert.ToString(memberDetail.GenderLookupId);
            ddlMaritalStatus.SelectedValue = Convert.ToString(memberDetail.MaritalStatusLookupId);

            ddlIndustry.SelectedValue = Convert.ToString(memberDetail.CandidateIndustryLookupID);
            ddlJobFunction.SelectedValue = Convert.ToString(memberDetail.JobFunctionLookupID);


            txtCurrentYearlySalary.Text =(memberExtendedInfo.CurrentYearlyRate==0?"": Convert.ToString(memberExtendedInfo.CurrentYearlyRate));

           if(memberExtendedInfo.ExpectedSalaryPayCycle!=0) ddlExpectedSalaryCycle.SelectedValue = memberExtendedInfo.ExpectedSalaryPayCycle.ToString() ;
          if(memberExtendedInfo.CurrentSalaryPayCycle!=0)  ddlCurrentSalaryCycle.SelectedValue = memberExtendedInfo.CurrentSalaryPayCycle.ToString ();
            txtExpectedYearlySalary.Text =( memberExtendedInfo.ExpectedYearlyRate==0?"":Convert.ToString(memberExtendedInfo.ExpectedYearlyRate));
            if(memberExtendedInfo.ExpectedYearlyCurrencyLookupId >0)ddlExpectedYearlyCurrency.SelectedValue = Convert.ToString(memberExtendedInfo.ExpectedYearlyCurrencyLookupId);
            if(memberExtendedInfo.CurrentYearlyCurrencyLookupId>0)ddlCurrentYearlyCurrency.SelectedValue = Convert.ToString(memberExtendedInfo.CurrentYearlyCurrencyLookupId);
            if (ddlCurrentYearlyCurrency.SelectedItem.Text == "INR")
            {
                lblCurrentSalaryCurrency.Text = "(Lacs)";
                lblCurrentSalaryCurrency.Visible = true;
            }
            else
            {
                lblCurrentSalaryCurrency.Text = "";
            }
            txtCurrentCTCNote.Text = (memberExtendedInfo.CurrentSalaryNote == "" ? "" : memberExtendedInfo.CurrentSalaryNote);
            if (ddlExpectedYearlyCurrency.SelectedItem.Text == "INR")
            {

                lblExpectedSalaryCurrency.Text = "(Lacs)";
                lblExpectedSalaryCurrency.Visible = true;
            }
            else
            {
                lblExpectedSalaryCurrency.Text = "";

            }
            txtExpectedCTCNote.Text = (memberExtendedInfo.ExpectedSalaryNote == "" ? "" : memberExtendedInfo.ExpectedSalaryNote);

            ddlJobType.SelectedValue = Convert.ToString(memberExtendedInfo.JobTypeLookupId);
            txtCurrentCompany.Text = MiscUtil .RemoveScript (memberExtendedInfo.LastEmployer,string .Empty );
            txtJobTitleA.Text = MiscUtil .RemoveScript (memberExtendedInfo.CurrentPosition,string .Empty );
            ControlHelper.SelectListByValue(ddlWorkSchedule, memberExtendedInfo.WorkScheduleLookupId.ToString ());
           // txtExperience.Text = MiscUtil .RemoveScript (memberExtendedInfo.TotalExperienceYears,string .Empty );
            uclExperience.Experience = memberExtendedInfo.TotalExperienceYears;
            //txtCategory.Text = MiscUtil.RemoveScript(memberExtendedInfo.Category, string.Empty);
            ControlHelper.SelectListByValue(ddlCategory , memberExtendedInfo.Category);
            txtwebsite.Text = MiscUtil.RemoveScript(memberExtendedInfo.Website, string.Empty);
            txtLinkedInProfile.Text = MiscUtil.RemoveScript(memberExtendedInfo.LinkedinProfile, string.Empty);
            //txtExpectedMaxYearlySalary.Text =(memberExtendedInfo.ExpectedYearlyMaxRate==0?"": Convert.ToString(memberExtendedInfo.ExpectedYearlyMaxRate));  
            if (memberExtendedInfo.WillingToTravel == true)
            {
                rdoTravelYes.Checked = true;
                rdoTravelNo.Checked = false;
            }
            else
            {
                rdoTravelNo.Checked = true;
                rdoTravelYes.Checked = false;
            }

            if (memberExtendedInfo.SecurityClearance == true)
            {
                rdoSecurityClearanceYes.Checked = true;
                rdoSecurityClearanceNo.Checked = false;
            }
            else
            {
                rdoSecurityClearanceNo.Checked = true;
                rdoSecurityClearanceYes.Checked = false;
            }
            if (memberExtendedInfo.PassportStatus)
            {
                rdoPassportReady.Checked = true;
                rdoPassportNone.Checked = false;
            }
            else
            {
                rdoPassportReady.Checked = false;
                rdoPassportNone.Checked = true;
            }

            ddlWorkPermitStatus.SelectedValue = Convert.ToString(memberExtendedInfo.WorkAuthorizationLookupId);
            if (!(string.IsNullOrEmpty(memberDetail.SSNPAN)) && memberDetail.SSNPAN.Trim().Length ==9)
            {
               txtSSN1.Attributes["value"] = memberDetail.SSNPAN.Substring(0, 3); 
                txtSSN2.Attributes["value"] = memberDetail.SSNPAN.Substring(3, 2); 
                txtSSN3.Text = memberDetail.SSNPAN.Substring(5, 4);
            }
            ControlHelper.SelectListByValue(ddlIDProof, memberExtendedInfo.IdCardLookUpId.ToString());
           txtIdProof .Text = MiscUtil.RemoveScript(memberExtendedInfo.IdCardDetail , string.Empty);
           txtNoticePeriod.Text = (memberExtendedInfo.NoticePeriod == "" ? "" : memberExtendedInfo.NoticePeriod);

        }
      
        private Member BuildMember()
        {
            Member member = CurrentMemberInfo;

            if (!string.IsNullOrEmpty(wdcDateOfBirth.Text)) member.DateOfBirth = Convert.ToDateTime(wdcDateOfBirth.Text.Trim());
            else  member.DateOfBirth = DateTime.MinValue;
            CurrentMemberDetails.IsCurrentSameAsPrimaryAddress = chkIsPermanentAddress.Checked;

            member.PermanentPhone = txtPermanentPhone.Text=MiscUtil .RemoveScript (txtPermanentPhone.Text.Trim());
            txtPermanentPhone.Text = MiscUtil.RemoveScript(txtPermanentPhone.Text.Trim(),string .Empty );
            member.PermanentPhoneExt = txtPermamentPhoneExt.Text=MiscUtil .RemoveScript(txtPermamentPhoneExt.Text);
            txtPermamentPhoneExt.Text = MiscUtil.RemoveScript(txtPermamentPhoneExt.Text, string.Empty);
            member.PermanentMobile = txtPermanentMobile.Text=MiscUtil .RemoveScript(txtPermanentMobile.Text.Trim());
            txtPermanentMobile.Text = MiscUtil.RemoveScript(txtPermanentMobile.Text.Trim(), string.Empty);
            
            member.UpdatorId = base.CurrentMember.Id;

            return member;
        }
        private MemberDetail BuildMemberDetail()
        {
            MemberDetail memberDetail = CurrentMemberDetails;

            if (Convert.ToInt32(ddlMaritalStatus.SelectedValue) > 0)
            {
                memberDetail.MaritalStatusLookupId = Convert.ToInt32(ddlMaritalStatus.SelectedValue);
            }
            else
            {
                memberDetail.MaritalStatusLookupId = 0;
            }
            if (Convert.ToInt32(ddlGender.SelectedValue) > 0)
            {
                memberDetail.GenderLookupId = Convert.ToInt32(ddlGender.SelectedValue);
            }
            else
            {
                memberDetail.GenderLookupId = 0;
            }
                    memberDetail.CityOfBirth = txtCityOfBirth.Text=MiscUtil .RemoveScript(txtCityOfBirth.Text.Trim());
                    txtCityOfBirth.Text = MiscUtil.RemoveScript(txtCityOfBirth.Text.Trim(),string .Empty );
                    if (Convert.ToInt32(ddlCountryOfBirth.SelectedValue) > 0)
                    {
                        memberDetail.CountryIdOfBirth = ddlCountryOfBirth.SelectedValue;
                    }
                    else
                    {
                        memberDetail.CountryIdOfBirth = "0";
                    }
                    if (Convert.ToInt32(ddlCountryOfResidence.SelectedValue) > 0)
                    {
                        memberDetail.CountryIdOfCitizenship = ddlCountryOfResidence.SelectedValue;
                    }
                    else
                    {
                        memberDetail.CountryIdOfCitizenship ="0";
                    }
                   
                    memberDetail.CurrentAddressLine1 = txtLocation.Text=MiscUtil .RemoveScript(txtLocation.Text.Trim());
                    txtLocation.Text = MiscUtil.RemoveScript(txtLocation.Text.Trim(), string.Empty);
                    memberDetail.CurrentAddressLine2 = txtPermanentAddress2.Text=MiscUtil .RemoveScript(txtPermanentAddress2.Text.Trim());
                    txtPermanentAddress2.Text = MiscUtil.RemoveScript(txtPermanentAddress2.Text.Trim(), string.Empty);
                    memberDetail.CurrentCity = txtPermanentCity.Text=MiscUtil .RemoveScript(txtPermanentCity.Text.Trim());
                    txtPermanentCity.Text = MiscUtil.RemoveScript(txtPermanentCity.Text.Trim(), string.Empty);
                    memberDetail.CurrentCountryId = uclCountryState.SelectedCountryId;
                    memberDetail.CurrentStateId = uclCountryState.SelectedStateId;
                    memberDetail.CurrentZip =txtPermanentZipCode.Text= MiscUtil .RemoveScript(txtPermanentZipCode.Text.Trim());
                    txtPermanentZipCode.Text = MiscUtil.RemoveScript(txtPermanentZipCode.Text.Trim(), string.Empty);

                    if (_memberId > 0) memberDetail.MemberId = _memberId;
                    memberDetail.CreatorId = base.CurrentMember.Id;
                    memberDetail.UpdatorId = base.CurrentMember.Id;

                    memberDetail.SSNPAN = MiscUtil .RemoveScript(txtSSN1.Text.Trim()) + MiscUtil .RemoveScript(txtSSN2.Text.Trim()) + MiscUtil .RemoveScript(txtSSN3.Text.Trim()); // 9645  
                    
          
                       if (((memberDetail.SSNPAN != null) && (memberDetail.SSNPAN!=string.Empty) && (memberDetail.MemberId > 0)))
                        {
                            Facade.GetMemberSSNCount(memberDetail.SSNPAN, memberDetail.MemberId);
                        }

                       if (Convert.ToInt32(ddlIndustry.SelectedValue) > 0)
                       {
                           memberDetail.CandidateIndustryLookupID = Convert.ToInt32(ddlIndustry.SelectedValue);
                       }
                       else
                       {
                           memberDetail.CandidateIndustryLookupID = 0;
                       }
                       if (Convert.ToInt32(ddlJobFunction.SelectedValue) > 0)
                       {
                           memberDetail.JobFunctionLookupID = Convert.ToInt32(ddlJobFunction.SelectedValue);
                       }
                       else
                       {
                           memberDetail.JobFunctionLookupID = 0;
                       }

             return memberDetail;
        }
        private MemberExtendedInformation BuildMemberExInfo()
        {
            MemberExtendedInformation memberExInfo = CurrentMemberExInfo;

            string yearlySalary = txtCurrentYearlySalary.Text=MiscUtil .RemoveScript(txtCurrentYearlySalary.Text);
            txtCurrentYearlySalary.Text = MiscUtil .RemoveScript(txtCurrentYearlySalary.Text.Replace(" ", ""),string .Empty );
            memberExInfo.CurrentYearlyRate = Convert.ToDecimal(txtCurrentYearlySalary.Text.Trim ().Replace (" " ,"")==""?"0": MiscUtil .RemoveScript(txtCurrentYearlySalary.Text));
            memberExInfo.CurrentYearlyCurrencyLookupId =Convert.ToInt32(ddlCurrentYearlyCurrency.SelectedValue) == 0 ? Convert.ToInt32((SiteSetting[DefaultSiteSetting.Currency.ToString()] == null ? 0 : SiteSetting[DefaultSiteSetting.Currency.ToString()])) : Convert.ToInt32(ddlCurrentYearlyCurrency.SelectedValue);
            txtExpectedYearlySalary.Text = MiscUtil.RemoveScript(txtExpectedYearlySalary.Text.Replace(" ", ""), string.Empty);
            memberExInfo.ExpectedYearlyRate =(txtExpectedYearlySalary.Text.Trim ().Replace (" " ,"")==string .Empty ?0: Convert.ToDecimal(MiscUtil .RemoveScript(txtExpectedYearlySalary.Text)));
            memberExInfo.ExpectedYearlyCurrencyLookupId = Convert.ToInt32(ddlExpectedYearlyCurrency.SelectedValue)==0 ? Convert.ToInt32((SiteSetting[DefaultSiteSetting.Currency.ToString()] == null ? 0 : SiteSetting[DefaultSiteSetting.Currency.ToString()])) : Convert.ToInt32(ddlExpectedYearlyCurrency.SelectedValue);
            //txtExpectedMaxYearlySalary.Text = MiscUtil.RemoveScript(txtExpectedMaxYearlySalary.Text.Replace(" ", ""), string.Empty);
          // memberExInfo.ExpectedYearlyMaxRate = Convert.ToDecimal(txtExpectedMaxYearlySalary.Text.Trim ().Replace (" " ,"") == "" ? "0" : txtExpectedMaxYearlySalary.Text);
            memberExInfo.CurrentSalaryPayCycle =Convert .ToInt32 ( ddlCurrentSalaryCycle.SelectedValue);
            memberExInfo.ExpectedSalaryPayCycle = Convert .ToInt32 ( ddlExpectedSalaryCycle.SelectedValue);
            memberExInfo.CurrentSalaryNote = MiscUtil.RemoveScript(txtCurrentCTCNote.Text, string.Empty);
            memberExInfo.ExpectedSalaryNote = MiscUtil.RemoveScript(txtExpectedCTCNote.Text, string.Empty);
            memberExInfo.JobTypeLookupId = Convert.ToInt32(ddlJobType.SelectedValue);
            memberExInfo.LastEmployer = txtCurrentCompany.Text=MiscUtil .RemoveScript(txtCurrentCompany.Text);
            txtCurrentCompany.Text = MiscUtil.RemoveScript(txtCurrentCompany.Text, string.Empty);
            memberExInfo.CurrentPosition = txtJobTitleA.Text=MiscUtil .RemoveScript(txtJobTitleA.Text.Trim());
            txtJobTitleA.Text = MiscUtil.RemoveScript(txtJobTitleA.Text.Trim(), string.Empty);
            memberExInfo.TotalExperienceYears = uclExperience.Experience; //txtExperience.Text=MiscUtil .RemoveScript(txtExperience.Text.Trim());
           // txtExperience.Text = MiscUtil.RemoveScript(txtExperience.Text.Trim(), string.Empty);
            //memberExInfo.Category = txtCategory.Text = MiscUtil .RemoveScript(txtCategory.Text.Trim ());
            memberExInfo.Category = ddlCategory.SelectedValue;
            //txtCategory.Text = MiscUtil.RemoveScript(txtCategory.Text.Trim(), string.Empty);
            memberExInfo.Website = txtwebsite.Text = MiscUtil.RemoveScript(txtwebsite.Text.Trim());
            txtwebsite.Text = MiscUtil.RemoveScript(txtwebsite.Text.Trim(), string.Empty);
            memberExInfo.LinkedinProfile = txtLinkedInProfile.Text = MiscUtil.RemoveScript(txtLinkedInProfile.Text.Trim());
            txtLinkedInProfile.Text = MiscUtil.RemoveScript(txtLinkedInProfile.Text.Trim(), string.Empty);
            if (rdoTravelYes.Checked == true) memberExInfo.WillingToTravel = true;
            else  memberExInfo.WillingToTravel = false;
            if (ddlWorkSchedule.SelectedIndex != -1) memberExInfo.WorkScheduleLookupId   = Convert.ToInt32(ddlWorkSchedule.SelectedValue);
            if (rdoSecurityClearanceYes.Checked == true) memberExInfo.SecurityClearance = true;
            else memberExInfo.SecurityClearance = false;
            if (rdoPassportReady.Checked) memberExInfo.PassportStatus = true;
            else memberExInfo.PassportStatus = false;
            memberExInfo.WorkAuthorizationLookupId = Convert.ToInt32(ddlWorkPermitStatus.SelectedValue);
            if (_memberId > 0) memberExInfo.MemberId = _memberId;
            memberExInfo.CreatorId = base.CurrentMember.Id;
            memberExInfo.UpdatorId = base.CurrentMember.Id;
            //memberExInfo.PANNumber = txtPan.Text;
            memberExInfo.IdCardLookUpId = Convert .ToInt32( ddlIDProof.SelectedValue);
            memberExInfo.IdCardDetail = MiscUtil.RemoveScript(txtIdProof.Text.Trim(), string.Empty);
            memberExInfo.NoticePeriod = MiscUtil.RemoveScript(txtNoticePeriod.Text.Trim(), string.Empty);
            return memberExInfo;
        } 
        private void LoadWorkFlowSetting()
        {
            ApplicationWorkflow applicationWorkflow = Facade.GetApplicationWorkflowByTitle(WorkFlowTitle.ATSCandidateInformationArchived.ToString());
            if (applicationWorkflow != null && applicationWorkflow.RequireApproval && _memberrole == ContextConstants.ROLE_CANDIDATE)
             IsInformationArchived = true;
            else IsInformationArchived = false;
        }
        private void SaveMemberExperience()
        {
            try
            {
                MemberExperience memberExperience = CurrentMemberExperience;

                if (memberExperience.IsNew)
                {
                    Facade.AddMemberExperience(memberExperience);
                }
                else
                {
                    Facade.UpdateMemberExperience(memberExperience);
                }
                _experienceId = 0;
               
            }
            catch (ArgumentException ex)
            {
                MiscUtil.ShowMessage(lblMessage, ex.Message, true);
            }
        }
        
        #endregion

        #region Events

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {


         
            chkIsPermanentAddress .Attributes .Add ("onclick", "javascript:Ischecked()");
            ddlCurrentYearlyCurrency.Attributes.Add("onchange", "javascript:ShowYearlyCurrency('"+ddlCurrentYearlyCurrency .ClientID +"','"+lblCurrentSalaryCurrency .ClientID +"')");
            ddlExpectedYearlyCurrency.Attributes.Add("onchange", "javascript:ShowYearlyCurrency('" + ddlExpectedYearlyCurrency.ClientID + "','" + lblExpectedSalaryCurrency.ClientID + "')");
            try
            {
             
                    txtSSN1.Attributes.Add("OnKeyUp", "return fnSSNKepUpSSN1();");
                    txtSSN2.Attributes.Add("OnKeyUp", "return fnSSNKepUpSSN2();");
                    Country country = Facade.GetCountryById(Convert.ToInt32(SiteSetting[DefaultSiteSetting.Country.ToString()].ToString()));
                    if (country != null)
                    {
                        if (country.Name == "India")
                        {
                            lblPermanentZipCode.Text = "PIN Code";
                            revZIPCode.ErrorMessage = "Please enter a valid PIN code";
                            divSSN.Visible = false;
                            divPan.Visible = true;
                        }
                        if (country.CountryCode != "US" && country.Name != "United States")
                        {
                            dvEmploymentType.Visible = false;
                            dvSecurityClearence.Visible = false;
                            dvWorkPermitstatus.Visible = false;
                        }
                        else
                        {
                            dvEmploymentType.Visible = true ;
                            dvSecurityClearence.Visible = true ;
                            dvWorkPermitstatus.Visible = true ;
                        }
                    }

                    string countryId;
                    this.RangeValidatorDOB.MaximumValue = DateTime.Now.Date.ToShortDateString();
                    this.RangeValidatorDOB.MinimumValue = new DateTime(1800, 1, 1).ToShortDateString();
                    if (!IsPostBack)
                    {
                        PrepareView();
                        PrepareEditView();
                        LoadWorkFlowSetting();
                    }
                    if (MemberRole == "Employee")
                    {
                        divSSN.Visible = false;
                    }
             
            }
            catch(Exception ex)
            {
            }
            this.Page.Title =MiscUtil.RemoveScript ( CurrentMemberInfo.FirstName,string .Empty ) + " " +MiscUtil .RemoveScript ( CurrentMemberInfo .MiddleName )+ " "  +MiscUtil .RemoveScript ( CurrentMemberInfo.LastName) + " - " + "Resume Builder";
        }

        #endregion Page Events

        protected void btnSaveMember_Click(object sender, EventArgs e)
        {
            string strModule = Helper.Url.SecureUrl.ToString();
            SaveMember();
            uclCountryState.SelectedCountryId = uclCountryState.SelectedCountryId;
            uclCountryState.SelectedStateId = uclCountryState.SelectedStateId;
        }

        protected void chkIsPermanentAddress_Click(object sender, EventArgs e)
        {
            Member member = CurrentMemberInfo;
            MemberDetail memberDetail = CurrentMemberDetails;
            if (chkIsPermanentAddress.Checked)
            {
                txtLocation.Text =MiscUtil .RemoveScript ( member.PermanentAddressLine1,string .Empty );
                txtPermanentAddress2.Text =MiscUtil .RemoveScript (  member.PermanentAddressLine2,string .Empty );
                txtPermanentCity.Text =MiscUtil .RemoveScript (  member.PermanentCity,string .Empty );
               uclCountryState .SelectedCountryId  =member.PermanentCountryId;
               uclCountryState.SelectedStateId = member.PermanentStateId;
                txtPermanentZipCode.Text = MiscUtil .RemoveScript ( member.PermanentZip,string .Empty );
                txtPermanentPhone.Text = MiscUtil .RemoveScript ( member.PrimaryPhone,string .Empty );
                txtPermamentPhoneExt.Text = MiscUtil .RemoveScript ( member.PrimaryPhoneExtension,string .Empty );
                txtPermanentMobile.Text = MiscUtil .RemoveScript ( member.CellPhone,string .Empty );
                Session.Add("chkAddress", true); 
                chkAddress(); 
            }
            else
            {
                txtLocation.Text = MiscUtil .RemoveScript ( memberDetail.CurrentAddressLine1,string .Empty );
                txtPermanentAddress2.Text = MiscUtil .RemoveScript ( memberDetail.CurrentAddressLine2,string .Empty );
                txtPermanentCity.Text = MiscUtil .RemoveScript ( memberDetail.CurrentCity,string .Empty );
                uclCountryState.SelectedCountryId = memberDetail.CurrentCountryId;
                uclCountryState.SelectedStateId = memberDetail.CurrentStateId;
                txtPermanentZipCode.Text = MiscUtil .RemoveScript ( memberDetail.CurrentZip,string .Empty );
                txtPermanentPhone.Text = MiscUtil .RemoveScript ( member.PermanentPhone,string .Empty );
                txtPermamentPhoneExt.Text = MiscUtil .RemoveScript ( member.PermanentPhoneExt,string .Empty );
                txtPermanentMobile.Text = MiscUtil.RemoveScript(member.PermanentMobile, string.Empty);
                Session.Add("chkAddress", false);
                chkAddress();                
            }
        }

        protected void chkAddress()
        {
            if (Session["chkAddress"] != null)
            {
                if (Convert.ToBoolean(Session["chkAddress"]) == true)
                {
                    txtLocation.ReadOnly = true;
                    txtPermanentAddress2.ReadOnly = true;
                    txtPermanentCity.ReadOnly = true;
                    uclCountryState.Enabled = false;
                    txtPermanentZipCode.ReadOnly = true;
                    txtPermanentPhone.ReadOnly = true;
                    txtPermamentPhoneExt.ReadOnly = true;
                    txtPermanentMobile.ReadOnly = true;
                }
                else if(Convert.ToBoolean(Session["chkAddress"]) == false)
                {
                    txtLocation.ReadOnly = false;
                    txtPermanentAddress2.ReadOnly = false;
                    txtPermanentCity.ReadOnly = false;
                    uclCountryState.Enabled = true ;
                    txtPermanentZipCode.ReadOnly = false;
                    txtPermanentPhone.ReadOnly = false;
                    txtPermamentPhoneExt.ReadOnly = false;
                    txtPermanentMobile.ReadOnly = false;
                }
            }
        }

        #endregion

    
}
}
