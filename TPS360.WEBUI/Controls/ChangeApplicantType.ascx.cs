﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:ChangeApplicatType.ascx.cs
    Description: This is the user control page used to Change the Applicant type.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Apr-15-2010          Sudarshan.R.          Defect id: 12646 ; Changes made in LoadExistingType().
     0.2            Apr-21-2010           Prashant         Defect id: 12529    Changes made in btnApplicantTypeSave_Click().
-------------------------------------------------------------------------------------------------------------------------------------------       

>*/

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;

namespace TPS360.Web.UI
{
    public partial class cltChangeApplicantType : BaseControl
    {
        #region Member Variables

        private int _memberId = 1;

        #endregion

        #region Methods

        private void GetMemberId()
        {
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                hfMemberId.Value = Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID];
            }
            _memberId = Int32.Parse(hfMemberId.Value);

        }

        private void LoadExistingType()
        {
            Member memberType = Facade.GetMemberById(_memberId);
            GenericLookup _MemberTypeLookup = Facade.GetGenericLookupById(memberType.MemberType);
            if (_MemberTypeLookup != null)
            {
                ddlEmployeementType.SelectedValue = Convert.ToString( _MemberTypeLookup.Id);
            }
        }

        private void PrepareView()
        {
            MiscUtil.PopulateCandidateType(ddlEmployeementType, Facade);
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            _memberId = Int32.Parse(hfMemberId.Value);
            PrepareView();
            GetMemberId();
         
            MemberManager Manager = Facade.GetMemberManagerByMemberIdAndManagerId(_memberId, CurrentMember.Id);
            if (Manager == null && !IsUserAdmin)
                Helper.Url.Redirect(UrlConstants.Dashboard.DEFAULT_PAGE);
            if (!Page.IsPostBack)
            {
                LoadExistingType();
            }
            if (IsPostBack)
            {
                MiscUtil.LoadAllControlState(Request, this);
            }
            if (Session["msg"] != null)
            {
                MiscUtil.ShowMessage(lblMessage, Session["msg"].ToString(), false);
                Session.Remove("msg");
            }
        }

        protected void btnApplicantTypeSave_Click(object sender, EventArgs e)
        {
            int applicanttype = Int32.Parse(ddlEmployeementType.SelectedValue);
            Member applicant = Facade.GetMemberById(_memberId);
            string msg;
            if (applicant != null)
            {
                applicant.UpdatorId = base.CurrentMember.Id;
                applicant.MemberType = applicanttype;
                Facade.UpdateMember(applicant);
                Facade.UpdateUpdateDateByMemberId(_memberId);
                LoadExistingType();
                msg = "Successfully Changed Candidate Type.";
                //MiscUtil.ShowMessage(lblMessage, msg, false);
               
            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Candidate Type already assigned", true);
                return;
            }
            ScriptManager.RegisterClientScriptBlock(this, typeof(MiscUtil), "candidatetype", ";parent.changeApplicantionType('" + ddlEmployeementType.SelectedItem.Text + "');parent.ShowStatusMessage('" + msg + "',false);", true);
        }
        #endregion

}
}