﻿/* 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: ObjectiveSummaryEditor.ascx.cs
    Description: This is the control which is used to edit Objective and Summary information
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
     0.1             Apr-09-2010          Ganapati Bhat          Defect id: 12584 ; Assigned objectiveAndSummary value in BuildObjectiveAndSummary().
-------------------------------------------------------------------------------------------------------------------------------------------       
*/

using System;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using System.Text.RegularExpressions;
namespace TPS360.Web.UI
{
    public partial class ControlObjectiveSummaryEditor : BaseControl
    {
        #region Member Variables

        MemberObjectiveAndSummary _memberObjectiveAndSummary;
        private static string _memberrole = string.Empty;
       private static MemberObjectiveAndSummary oldObjectiveAndSummary;
       private int statusObjective = 0;
       private int statusSummery = 0;
        #endregion

        #region Properties
       private int Current_CandidateID
       {
           get
           {
               if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
               {
                   return Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
               }
               return 0;
           }
       }
        MemberObjectiveAndSummary CurrentMemberObjectiveAndSummary
        {
            get
            {
                if (_memberObjectiveAndSummary == null)
                {
                   
                    if (Current_CandidateID  > 0)
                    {
                        _memberObjectiveAndSummary = Facade.GetMemberObjectiveAndSummaryByMemberId(Current_CandidateID );
                    }
                    else
                    {
                        _memberObjectiveAndSummary = Facade.GetMemberObjectiveAndSummaryByMemberId(base.CurrentMember.Id);
                    }

                    if (_memberObjectiveAndSummary == null)
                    {
                        _memberObjectiveAndSummary = new MemberObjectiveAndSummary();
                    }
                    else
                    {
                        oldObjectiveAndSummary = (MemberObjectiveAndSummary)_memberObjectiveAndSummary.Clone();
                    }
                }

                return _memberObjectiveAndSummary;
            }
        }

        public string MemberRole
        {
            set { _memberrole = value; }
            get { return _memberrole; }
        }

        #endregion

        #region Methods

        private void PrepareEditView()
        {
            txtObjective.Text =MiscUtil .RemoveScript ( CurrentMemberObjectiveAndSummary.Objective,string .Empty );
            txtSummary.Text =MiscUtil .RemoveScript ( CurrentMemberObjectiveAndSummary.Summary,string .Empty );
           
        }
      
        private void SaveObjectiveSummary()
        {
            if (IsValid)
            {
                try
                {
                    MemberObjectiveAndSummary objectiveAndSummary = BuildObjectiveAndSummary();

                    if (objectiveAndSummary.IsNew)
                    {
                        Facade.AddMemberObjectiveAndSummary(objectiveAndSummary);                          
                        MiscUtil.ShowMessage(lblMessage, "Objective and summary has been added successfully.", false);
                    }
                    else
                    {
                        Facade.UpdateMemberObjectiveAndSummary(objectiveAndSummary);
                        MiscUtil.ShowMessage(lblMessage, "Objective and summary has been updated successfully.", false );
                    }
                    Facade.UpdateUpdateDateByMemberId(Current_CandidateID);
                    if (statusObjective == 2)
                    {
                        MiscUtil.AddActivity(_memberrole, Current_CandidateID , base.CurrentMember.Id, ActivityType.AddObjective, Facade);
                    }
                    else if (statusObjective == 3)
                    {
                        MiscUtil.AddActivity(_memberrole, Current_CandidateID , base.CurrentMember.Id, ActivityType.UpdateObjective, Facade);
                    }
                    
                    if (statusSummery == 2)
                    {
                        MiscUtil.AddActivity(_memberrole,Current_CandidateID ,base.CurrentMember.Id,ActivityType.AddSummery,Facade);
                    }
                    else if (statusSummery == 3)
                    {
                        MiscUtil.AddActivity(_memberrole,Current_CandidateID ,base.CurrentMember.Id,ActivityType.UpdateSummery,Facade);
                    }
                }
                catch (ArgumentException ex)
                {
                    MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                }
            }
        }

        private MemberObjectiveAndSummary BuildObjectiveAndSummary()
        {
            MemberObjectiveAndSummary objectiveAndSummary = CurrentMemberObjectiveAndSummary;
            objectiveAndSummary.Objective =txtObjective .Text = MiscUtil .RemoveScript ( txtObjective.Text.Trim());
            string objective = string.Empty;
            objective = txtObjective.Text = MiscUtil.RemoveScript(txtObjective.Text.Trim(), string.Empty);
            if (!String.IsNullOrEmpty(objective))
            {
                if (objectiveAndSummary.Objective == null || String.IsNullOrEmpty(objectiveAndSummary.Objective.Trim()))
                {
                    statusObjective = 2;
                }
                else
                {
                    statusObjective = 3;
                }
                objectiveAndSummary.Objective =MiscUtil .RemoveScript ( txtObjective.Text.Trim());
            }
            else
            {
                statusObjective = 0;
            }

            
            objectiveAndSummary.Summary =txtSummary .Text = MiscUtil .RemoveScript (txtSummary.Text.Trim());

            string summery = txtSummary.Text = MiscUtil.RemoveScript(txtSummary.Text.Trim(),string .Empty );
            if (!string.IsNullOrEmpty(summery))
            {
                if (objectiveAndSummary.Summary == null || string.IsNullOrEmpty(objectiveAndSummary.Summary.Trim()))
                {
                    statusSummery = 2;
                }
                else
                {
                    statusSummery = 3;
                }
                objectiveAndSummary.Summary =MiscUtil .RemoveScript ( txtSummary.Text.Trim());
            }
            else
            {
                statusSummery = 0;
            
            }

            if (Current_CandidateID  > 0)
            {
                objectiveAndSummary.MemberId = Current_CandidateID ;
            }
            else
            {
                objectiveAndSummary.MemberId = base.CurrentMember.Id;
            }
            objectiveAndSummary.CreatorId = base.CurrentMember.Id;
            objectiveAndSummary.UpdatorId = base.CurrentMember.Id;
            return objectiveAndSummary;
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
                 MiscUtil.LoadAllControlState(Request, this);
                 if (!IsPostBack)
                     PrepareEditView();

                 if (Current_CandidateID > 0)
                 {
                     string name = MiscUtil.GetMemberNameById(Current_CandidateID, Facade);
                     this.Page.Title = name + " - " + "Resume Builder";
                 }
          
        }

        protected void btnSaveObjectiveSummary_Click(object sender, EventArgs e)
        {
            SaveObjectiveSummary();
        }

        #endregion
    }
}