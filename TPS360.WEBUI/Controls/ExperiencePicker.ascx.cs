﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Web.Security;
using System.Text.RegularExpressions;
using System.Linq;
using System.Web.UI;
namespace TPS360.Web.UI
{
   
    
    public partial class ExperiencePicker: BaseControl
    {

        public void Clear()
        {
            ddlExpMonths.SelectedIndex = 0;
            ddlExpYears.SelectedIndex = 0;
        }
        public short YearsTabIndex
        {
            set
            {
               ddlExpYears.TabIndex = value;
            }
        }
        public short MonthTabIndex
        {
            set
            {
              ddlExpMonths.TabIndex = value;
            }
        }

        public string Experience
        {
            get
            {
                return ddlExpYears.Text + "." + ddlExpMonths.Text;
            }
            set
            {
                FillControls();
                string exp = value;
                char[] delim={'.'};
                string[] exparry = exp.Split(delim, StringSplitOptions.RemoveEmptyEntries);
                if (exparry.Length > 0) ddlExpYears.Text = exparry[0];
                if (exparry.Length > 1) ddlExpMonths.Text = exparry[1];
            }
        }

        protected void Page_Load(object sender,EventArgs e)
        {
            FillControls();
        }
        
        public void  FillControls()
        {
            if (ddlExpYears .Items .Count ==0)
            {
                for (int i = 0; i <= 35; i++)
                {
                  ddlExpYears.Items .Add (new ListItem (i.ToString ()));
                }
            }

            if (ddlExpMonths.Items.Count == 0)
            {
                for (int i = 0; i <= 11; i++)
                {
                    ddlExpMonths.Items.Add(new ListItem(i.ToString()));
                }
            }
        }
    }
    
}