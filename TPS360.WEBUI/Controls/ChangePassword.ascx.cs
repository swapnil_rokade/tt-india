﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:ChancgePassword.ascx.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              Sept-24-2009         Nagarathna V.B      Enha#11473;passing current user for send email method call
-------------------------------------------------------------------------------------------------------------------------------------------       

>*/
using System;
using System.Web.Security;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using System.Text;
using System.Net;
using System.IO;
using System.Configuration;
using TPS360.Web.UI.Helper;

namespace TPS360.Web.UI
{
    public partial class ctlChangePassword : BaseControl
    {
        #region Member Variables

        #endregion

        #region Properties

        #endregion

        #region Methods

        public void HideSaveButton()
        {
            btnSubmit.Visible = false;
        }
        public static string GetFileContentFromUrl(string url)
        {
            string fileContent = string.Empty;

            try
            {
                WebRequest request = WebRequest.Create(url);
                WebResponse response = request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);

                fileContent = StringHelper.Convert(reader.ReadToEnd());
                reader.Close();
                response.Close();
            }
            catch (Exception)
            {

            }

            return fileContent;
        }

        private void SendChangePasswordEmail(string mailFrom, string mailTo, string mailBody)
        {
            string sentStatus = string.Empty;
            EmailHelper emailManager = new EmailHelper();
            emailManager.To.Clear();
            emailManager.From = (mailFrom != "") ? mailFrom : "admin@tps360.com";
            emailManager.To.Add(mailTo);
            emailManager.Subject = "New Password For Talentrackr Access.";
            emailManager.Body = mailBody.ToString();
            sentStatus = emailManager.Send(base.CurrentMember.Id);
        }

        private void ChangePassword()
        {
            if (IsValid)
            {
                try
                {
                    if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
                    {
                        int memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);

                        Member member = Facade.GetMemberById(memberId);

                        if (member != null && member.Id > 0)
                        {
                            MembershipUser user = Membership.GetUser(member.UserId);

                            if (user != null && user.UserName != "" && !user.IsLockedOut)
                            {
                                string memberName = user.UserName;
                                string strOldPassword = user.GetPassword();

                                if (txtNewPassword.Text.Trim() == txtConfirmPassword.Text.Trim())
                                {
                                    user.ChangePassword(strOldPassword, txtNewPassword.Text.Trim());
                                    Membership.UpdateUser(user);

                                    //string mailFrom = CurrentUser.UserName;
                                    //string mailTo = user.UserName;

                                    //string changePasswordTemplateUrl = AppSettings.ChangePasswordTemplateUrl;
                                    //string mailBody = GetFileContentFromUrl(changePasswordTemplateUrl);

                                    //memberName = member.FirstName + " " + member.MiddleName + " " + member.LastName;

                                    //StringBuilder stringBuilder = new StringBuilder();

                                    //stringBuilder.Append(mailBody);
                                    //stringBuilder.Replace("[MemberName]", memberName);
                                    //stringBuilder.Replace("[Password]", user.GetPassword());

                                    //SendChangePasswordEmail(mailFrom, mailTo, stringBuilder.ToString());

                                    MiscUtil.ShowMessage(lblMessage, "New password has been saved successfully.", false);

                                    txtNewPassword.Text = String.Empty;
                                    txtConfirmPassword.Text = String.Empty;
                                }
                                else
                                {
                                    MiscUtil.ShowMessage(lblMessage, "Invalid old password", true);
                                }
                            }
                            else
                            {
                                MiscUtil.ShowMessage(lblMessage, "User not found.", true);
                            }
                        }
                        else
                        {
                            MiscUtil.ShowMessage(lblMessage, "Member not found.", true);
                        }
                    }
                    else
                    {
                        MiscUtil.ShowMessage(lblMessage, "Member not found.", true);
                    }
                }
                catch (ArgumentException ex)
                {
                    MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                }
            }
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            ChangePassword();
        }

        #endregion
    }
}