﻿using System;
using System.Collections;
using System.Web;
using System.Web.UI.WebControls;
using TPS360.Providers;
using TPS360.Common.Helper;
using TPS360.Common.Shared;

namespace TPS360.Web.UI
{
    public partial class MenuSelector : BaseControl
    {
        #region Properties

        public TreeView SiteMapTree
        {
            get
            {
                return treeViewSiteMap;
            }
        }

        public SiteMapType[] CurrentSiteMapTypes
        {
            get
            {
                return (SiteMapType[])(ViewState[this.ClientID + "_SiteMapType"] ?? null);
            }
            set
            {
                ViewState[this.ClientID + "_SiteMapType"] = value;
            }
        }

        public bool DisableChacking
        {
            get
            {
                return (bool)(ViewState[this.ClientID + "_DisableChacking"] ?? false);
            }
            set
            {
                ViewState[this.ClientID + "_DisableChacking"] = value;
            }
        }

        public bool ShowLines
        {
            get
            {
                return (bool)(ViewState[this.ClientID + "_ShowLines"] ?? false);
            }
            set
            {
                ViewState[this.ClientID + "_ShowLines"] = value;
            }
        }

        public ArrayList SelectList
        {
            get
            {
                return (ArrayList)(ViewState[this.ClientID + "_SelectList"] ?? null);
            }
            set
            {
                ViewState[this.ClientID + "_SelectList"] = value;
            }
        }

        

        #endregion

        #region Method

        private bool IsInSelectList(int menuId)
        {
            ArrayList selectList = SelectList;

            if ((selectList != null) && (selectList.Count > 0))
            {
                if (selectList.Contains(menuId))
                {
                    return true;
                }
            }

            return false;
        }
        public void BindList()
        {
            SiteMapNodeCollection nodeList = new SiteMapNodeCollection();
            SiteMapNode node = SiteMapDataSource1.Provider.RootNode;
            if (!IsUserAdmin)
            {
                if (CurrentSiteMapTypes != null && CurrentSiteMapTypes.Length > 0)
                {
                    for (int i = 0; i <= CurrentSiteMapTypes.Length - 1; i++)
                    {
                        node = SiteMap.Providers["SqlSiteMaps"].FindSiteMapNodeFromKey(StringHelper.Convert((int)CurrentSiteMapTypes[i]));

                        if (node != null)
                        {
                            if (!base.IsUserAdmin)
                            {
                                if (node.Title == "Application Top Menu")
                                {
                                    SiteMapNodeCollection nodeLists = new SiteMapNodeCollection();
                                    for (int j = 0; j < node.ChildNodes.Count; j++)
                                    {
                                        SiteMapNode nodes = node.ChildNodes[j];
                                        if (nodes.Title != "Admin Access" && nodes.Title != "Admin Data")
                                            nodeLists.Add(nodes);
                                    }
                                    node.ChildNodes = nodeLists;
                                }


                                nodeList.Add(node);
                            }
                            else
                            {
                                nodeList.Add(node);
                            }
                        }


                    }
                }
            }
            if (IsUserAdmin)
            {
                if (CurrentSiteMapTypes != null && CurrentSiteMapTypes.Length > 0)
                {
                    for (int i = 0; i <= CurrentSiteMapTypes.Length - 1; i++)
                    {
                        node = SiteMap.Providers["SqlSiteMap"].FindSiteMapNodeFromKey(StringHelper.Convert((int)CurrentSiteMapTypes[i]));

                        if (node != null)
                        {
                            nodeList.Add(node);
                        }
                    }
                }
            }
            treeViewSiteMap.DataSource = nodeList;
            treeViewSiteMap.DataBind();

            if (DisableChacking)
            {
                treeViewSiteMap.ShowCheckBoxes = TreeNodeTypes.None;
                treeViewSiteMap.Attributes.Remove("onclick");
            }

            if (ShowLines)
            {
                treeViewSiteMap.ShowLines = true;
            }




        }

        

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               
            }
        }

        protected void TreeView1_TreeNodeDataBound(object sender, TreeNodeEventArgs e)
        {
            SiteMapNode nodeFromSiteMap = (SiteMapNode)e.Node.DataItem;


            if (nodeFromSiteMap["Id"] != null)
            {
                e.Node.Value = nodeFromSiteMap["Id"];
            }

            e.Node.SelectAction = TreeNodeSelectAction.None;

            if (!DisableChacking && IsInSelectList(Convert.ToInt32(e.Node.Value)))
            {
                e.Node.Checked = true;
            }
            
        }

        #endregion
    }
}