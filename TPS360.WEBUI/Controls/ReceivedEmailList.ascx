<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReceivedEmailList.ascx.cs"
    Inherits="TPS360.Web.UI.ControlReceivedEmailList" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<asp:UpdatePanel ID="upReceivedList" runat="server">
    <ContentTemplate>
        <asp:ObjectDataSource ID="odsEmailList" runat="server" SelectMethod="GetPagedForReceived"
            OnSelecting="odsEmailList_Selecting" TypeName="TPS360.Web.UI.MemberEmailDataSource"
            SelectCountMethod="GetListCountForReceived" EnablePaging="True" SortParameterName="sortExpression">
            <SelectParameters>
                <asp:Parameter Name="emailTypeLookupId" DefaultValue="" />
                <asp:Parameter Name="senderId" DefaultValue="" />
                <asp:Parameter Name="receiverId" DefaultValue="" />
                <asp:Parameter Name="comId" DefaultValue="" />
                <asp:Parameter Name="AnyKey" DefaultValue="" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <asp:HiddenField ID="hdnSortColumn" runat="server" />
        <asp:HiddenField ID="hdnSortOrder" runat="server" />
        <asp:HiddenField ID="checks" runat="server" Value="" />
        <div class="HeaderDevider">
        </div>
        <div class="TableRow">
            <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
        </div>
        <div class="TableRow">
            <asp:Panel ID="pnlSear" runat="server" DefaultButton="btnSearchEmail">
                <div class="TableFormContent" style="margin-left: 170px;">
                    <asp:TextBox ID="txtSearchEmail" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                    <asp:LinkButton ID="btnSearchEmail" runat="server" CssClass="btn btn-primary" Text="Search Email"
                        OnClick="btnSearchEmail_Click" ><i class="icon-search icon-white"></i> Search</asp:LinkButton>
                </div>
            </asp:Panel>
            <div id="drpselect" class="TableFormContent" style="margin-left: 80px;">
                <asp:Label ID="lblcontent" runat="server" Visible="false"></asp:Label>
                <asp:DropDownList ID="ddlcontact" runat="server" CssClass="CommonDropDownList" Visible="false"
                    OnSelectedIndexChanged="ddlcontact_SelectedIndexChanged" AutoPostBack="true">
                </asp:DropDownList>
            </div>
        </div>
        
        <div class="GridContainer">
            <asp:ListView ID="lsvReceivedEmail" runat="server" DataKeyNames="Id" OnItemDataBound="lsvReceivedEmail_ItemDataBound"
                OnItemCommand="lsvReceivedEmail_ItemCommand" OnPreRender="lsvReceivedEmail_PreRender">
                <LayoutTemplate>
                    <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                        <tr>
                            <th style="width: 5px; white-space: nowrap; width: 120px">
                                <asp:LinkButton ID="btnDateTime" runat="server" ToolTip="Sort By Date And Time" CommandName="Sort"
                                    CommandArgument="SentDate" Text="Date & Time" />
                            </th>
                            <th align="left" id="thClientEmail" runat="server" visible="false">
                                <asp:LinkButton ID="lnkFrom" runat="server" ToolTip="Sort By Sender Email Address"
                                    Width="70%" CommandName="Sort" CommandArgument="SenderEmail" Text="From" />
                            </th>
                            <th style="width: 10%; white-space: nowrap;">
                                <asp:LinkButton ID="btnTo" runat="server" ToolTip="Sort By Receiver Email Address"
                                    Width="70%" CommandName="Sort" CommandArgument="ReceiverEmail" Text="To" />
                            </th>
                            <th style="width: 30%; white-space: nowrap;">
                                <asp:LinkButton ID="btnSubject" runat="server" ToolTip="Sort By Subject" Width="70%"
                                    CommandName="Sort" CommandArgument="Subject" Text="Subject" />
                            </th>
                            <th style="white-space: normal; width: 50%">
                                Body
                            </th>
                        </tr>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        <tr class="Pager">
                            <td colspan="4" id="tdPager" runat="server">
                                <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                            </td>
                        </tr>
                    </table>
                </LayoutTemplate>
                <EmptyDataTemplate>
                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                        <tr>
                            <td>
                                No data was returned.
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <ItemTemplate>
                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                        <td align="left">
                            <asp:Label ID="lblDateTime" runat="server" Style="width: 120px; white-space: nowrap;" />
                            <asp:HiddenField ID="hdfMemberEmailId" runat="server" />
                        </td>
                        <td align="left" id="tdClientEmail" runat="server" visible="false">
                            <asp:Label ID="lblSender" runat="server"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblReceivedFrom" runat="server" />
                        </td>
                        <td align="left">
                            <asp:HyperLink ID="hlnkSubject" runat="server" CssClass="NormalLink" ></asp:HyperLink>
                        </td>
                        <td align="left">
                            <asp:HyperLink ID="hlnkEmailBody" runat="server" CssClass="NormalLink" ></asp:HyperLink>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
