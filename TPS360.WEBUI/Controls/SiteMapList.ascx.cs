﻿using System;
using System.Web.UI.WebControls;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Providers;
using TPS360.Web.UI.Helper;
using System.Collections.Generic;
using System.Web;

namespace TPS360.Web.UI
{
    public partial class ctlSiteMapList : BaseControl
    {
        #region Member Variables

        #endregion

        #region Methods

        private void BindList()
        {
            this.lsvCustomSiteMap.DataBind();
            updPanelCustomSiteMap.Update();
        }
        private void ResetSiteMap()
        {
            SqlSiteMapProvider customProvider = SiteMap.Providers["SqlSiteMap"] as SqlSiteMapProvider;

            if (customProvider != null)
            {
                customProvider.ResetSiteMap();
            }
        }

        #endregion

        #region Events

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtSortColumn.Text = "btnTitle";
                txtSortOrder.Text = "ASC";
                string message = Helper.Url.SecureUrl[UrlConstants.PARAM_MSG];

                if (!StringHelper.IsBlank(message))
                {
                    MiscUtil.ShowMessage(lblMessage, message, false);
                }

                pnlTreeView.Visible = false;
            }
        }
        #endregion

        #region ListView Events

        protected void lsvCustomSiteMap_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            bool _isAdmin = base.IsUserAdmin; 
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                CustomSiteMap customSiteMap = ((ListViewDataItem)e.Item).DataItem as CustomSiteMap;

                if (customSiteMap != null)
                {
                    Label lblName = (Label)e.Item.FindControl("lblName");
                    Label lblDescription = (Label)e.Item.FindControl("lblDescription");
                    Label lblParentMenu = (Label)e.Item.FindControl("lblParentMenu");
                    Label lblLastUpdated = (Label)e.Item.FindControl("lblLastUpdated");
                    ImageButton btnEdit = (ImageButton)e.Item.FindControl("btnEdit");
                    ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");

                    lblName.Text = customSiteMap.Title;
                    lblDescription.Text = customSiteMap.Description;
                    lblParentMenu.Text = customSiteMap.ParentMenu.Title;
                    lblLastUpdated.Text = StringHelper.Convert(UIConstants.SHORT_DATE_FORMAT, customSiteMap.UpdateDate);
                    btnDelete.Visible = _isAdmin;
                    btnDelete.OnClientClick = "return ConfirmDelete('site map')";
                    btnEdit.CommandArgument = btnDelete.CommandArgument = StringHelper.Convert(customSiteMap.Id);
                }
            }
        }
        private void PlaceUpDownArrow()
        {
            try
            {

                LinkButton lnk = (LinkButton)lsvCustomSiteMap.FindControl(txtSortColumn.Text);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", ( txtSortOrder .Text  == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }

        }
        protected void lsvCustomSiteMap_PreRender(object sender, EventArgs e)
             {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvCustomSiteMap  .FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "CustomSiteMapRowPerPage";
            }
            PlaceUpDownArrow();
        }
        protected void lsvCustomSiteMap_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder.Text == "ASC")txtSortOrder.Text = "DESC";
                        else  txtSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn.Text = lnkbutton.ID;
                        txtSortOrder.Text = "ASC";
                    }
                }
            }
            catch
            {
            }
            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);

            if (id > 0)
            {
                if (string.Equals(e.CommandName, "EditItem"))
                {
                    Helper.Url.Redirect(UrlConstants.Admin.SITEMAP_EDITOR_PAGE, string.Empty, UrlConstants.PARAM_ID, StringHelper.Convert(id));
                }
                else if (string.Equals(e.CommandName, "DeleteItem"))
                {
                    try
                    {
                        if (Facade.DeleteCustomSiteMapById(id))
                        {
                            MiscUtil.ShowMessage(lblMessage, "Menu has been successfully deleted.", false);
                            ResetSiteMap();
                            BindList();
                        }
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
            }
        }

        #endregion

        #endregion
    }
}