﻿
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MemberExperienceEditor.ascx.cs"
    Inherits="TPS360.Web.UI.ControlMemberExperienceEditor" %>
<%@ Register Src="~/Controls/CountryStatePicker.ascx" TagName="CountryState" TagPrefix="ucl" %>
<%@ Register Src ="~/Controls/PagerControl.ascx" TagName ="Pager" TagPrefix ="ucl" %>  

<%@ Register Assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.Web.UI.EditorControls" TagPrefix="ig" %>
<%@ Register assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.Web.UI.LayoutControls" tagprefix="ig" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<script type ="text/javascript" language ="javascript"  src="../js/AjaxVariables.js"></script>

		<script type ="text/javascript" language ="javascript" src="../js/AjaxScript.js" ></script>

<asp:TextBox ID ="hdnSortColumn" runat ="server" EnableViewState ="true" Visible ="false"  />
<asp:TextBox ID ="hdnSortOrder" runat ="server" EnableViewState ="true"  Visible ="false" />
<script language="javascript" type="text/javascript">

function ExpandAll(hdnRowIds,imgAll)
{

var hdnExpandRowIDs=document .getElementById (hdnRowIds);
    if(hdnExpandRowIDs !=null)
    {
        var rows=hdnExpandRowIDs .value.split(',');
        for(var i=0;i<rows.length;i++)
        {
            var IDs=rows[i].split(';');
            if(IDs.length==2)
            {
               ShowOrHide (IDs [0],IDs [1]);
                
            }
         }
    }
    var imgAll=document .getElementById (imgAll );
    if(imgAll .src.indexOf ("collapse_all.gif")>-1)
    {
        imgAll .src ="../Images/expand_all.gif"; 
    }
    else
    {
        imgAll .src ="../Images/collapse_all.gif"; 
    }
}
 function  ShowOrHide(v,img)
 {
 var tdDetail=document .getElementById (v);
  var image=document .getElementById (img);
 if(tdDetail .style.display =="none")
 { tdDetail.style.display="";
  image.src="../Images/collapse_all.gif";
 }
 else 
 { tdDetail .style.display="none";
 image.src="../Images/expand_all.gif";
 }
 
 
 }
    function ValidatedateExp(source, args)
    { 
        //debugger;              
        var chkDurationTill = $get('<%= chkDurationTill.ClientID %>');
        var LblDatevalidate = $get('<%= LblDatevalidate.ClientID %>');
        var hnSDate = $get('<%= hdnSDate.ClientID %>');
        var hnEDate = $get('<%= hdnEDate.ClientID %>');
        LblDatevalidate.innerHTML = "";
        var CVExp = source.id.split('_')[5];
        if(CVExp == "CustomValidatorFromDate")
        {
            var sDate = args.Value.split('/');
            hnSDate.value=sDate;
        }
        else
        {
            var eDate = args.Value.split('/');
            hnEDate.value=eDate;
        } 
        var dateTime = new Date;
        var DateCompare = new Date(dateTime.getFullYear(),dateTime.getMonth(),dateTime.getDate());        
        var StartDate = new Date(hnSDate.value.split(',')[2], hnSDate.value.split(',')[0] - 1, hnSDate.value.split(',')[1]);
        var EndDate = new Date(hnEDate.value.split(',')[2], hnEDate.value.split(',')[0] - 1, hnEDate.value.split(',')[1]);
        
        if (StartDate > DateCompare) 
        {
            LblDatevalidate.innerHTML = "'Employment Duration' From date should not be greater than current date.";
            LblDatevalidate.style.display = "block";
            args.IsValid = false;
            return;
        }
        if(!chkDurationTill.checked)
        {  
            if (EndDate > DateCompare) 
            {
                LblDatevalidate.innerHTML = "'Employment Duration' To date should not be greater than current date.";
                LblDatevalidate.style.display = "block";
                args.IsValid=false;
                return;                
            }
            else if (StartDate > EndDate) 
                 {
                    LblDatevalidate.innerHTML = "'Employment Duration' From date should not be greater than To Date.";
                    LblDatevalidate.style.display = "block";
                    args.IsValid=false;
                 }
        }            
    }
    
    


 function ScrollTopPosition()
      {
    
      $('html').animate({
						scrollTop: -100
					}, 400);
      }
 
</script>
<asp:UpdatePanel ID="upExperience" runat ="server">
<ContentTemplate >


<asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
<asp:HiddenField ID="hfMemberId" runat="server" Value="0" />
<asp:HiddenField ID="hdnSDate" runat="server" Value="0" />
<asp:HiddenField ID="hdnEDate" runat="server" Value="0" />
<asp:HiddenField ID="hdnExpandRowIds" runat ="server" />
<div style =" width :100%">
            
                    <div style="width: 100%" id="divExperienceInputBlock" runat ="server" >
                    <div class="TabPanelHeader" >
                    Add Experience Record
                    </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                <asp:Label EnableViewState="false" ID="lblCompanyName" runat="server" Text="Employer/Company Name"
                                    ></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox EnableViewState="false" ID="txtCompanyName" runat="server" CssClass="CommonTextBox"
                                    Width="350px" TabIndex ="1"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormValidatorContent"  style ="margin-left :42%">   <%--0.6 added enabled property --%>
                                <asp:RequiredFieldValidator ID="rfvCompanyName" runat="server" ControlToValidate="txtCompanyName"
                                    ErrorMessage="Please enter company name." ValidationGroup="Experience" EnableViewState="False"
                                    Display="Dynamic"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                <asp:Label EnableViewState="false" ID="lblJobTitle" runat="server" Text="Job Title"
                                   ></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox EnableViewState="false" ID="txtJobTitle" runat="server" CssClass="CommonTextBox"
                                    Width="350px" TabIndex ="2"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormValidatorContent" style ="margin-left :42%">  <%--0.6 added enabled property --%>
                                <asp:RequiredFieldValidator ID="rfvJobTitle" runat="server" ControlToValidate="txtJobTitle"
                                    ValidationGroup="Experience" ErrorMessage="Please enter a job title." EnableViewState="False"
                                    Display="Dynamic"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                <asp:Label EnableViewState="false" ID="lblFunctionalCategory" runat="server" Text="Category"
                                    ></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:DropDownList EnableViewState="true" ID="ddlFunctionalCategory" runat="server"
                                    CssClass="CommonDropDownList" TabIndex ="3"> 
                                </asp:DropDownList>
                              
                            </div>
                        </div>
                       
                           <div class="TableRow">
                            <div class="TableFormLeble">
                                <asp:Label EnableViewState="false" ID="lblCity" runat="server" Text="City" ></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox EnableViewState="false" TabIndex ="4" ID="txtExperienceCity" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                            </div>
                        </div>
                           <ucl:CountryState ID="uclCountryState" runat="server" FirstOption="Please Select" ApplyDefaultSiteSetting="True"  /> 
                     
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                <asp:Label EnableViewState="false" ID="lblEmploymentDuration" runat="server" Text="Employment Duration"
                                    ></asp:Label>:
                            </div>
                            <div class="TableFormContent" style="white-space: nowrap;">
                                <div style="float: left;">
                                    <%--<igsch:WebDateChooser CssClass="CommonDropDownList" ID="wdcEmpDurationFromDate" Width="95px"
                                        runat="server" Editable="True" NullDateLabel="" AllowNull="true" TabIndex ="7" EnableKeyboardNavigation ="true"    CalendarLayout-HideOtherMonthDays ="true" >
                                    </igsch:WebDateChooser>--%>
                                    <ig:WebDatePicker ID="wdcEmpDurationFromDate"  DropDownCalendarID="ddcEmpDurationFromDate" runat="server"></ig:WebDatePicker>
                                    <ig:WebMonthCalendar  ID="ddcEmpDurationFromDate" AllowNull="true"  runat="server"></ig:WebMonthCalendar>
                                </div>
                               
                                <div style="float:left; padding-left : 5px;">
                                    <asp:Label ID="lblUntil" runat="server" Text="To" ></asp:Label>:
                                </div>
                                <div style="float: left;">
                                    <%--<igsch:WebDateChooser CssClass="CommonDropDownList" Width="95px" ID="wdcEmpDurationToDate"
                                        runat="server" Editable="True" NullDateLabel="" AllowNull="true" TabIndex ="8" EnableKeyboardNavigation ="true"   CalendarLayout-HideOtherMonthDays ="true">
                                    </igsch:WebDateChooser>--%>
                                    <ig:WebDatePicker ID="wdcEmpDurationToDate" runat="server"  DropDownCalendarID="ddcEmpDurationToDate"></ig:WebDatePicker>
                                    <ig:WebMonthCalendar  ID="ddcEmpDurationToDate" AllowNull="true" Focusable="false"  runat="server"></ig:WebMonthCalendar>
                                </div>
                                <div style="float: left; padding-left: 20px;">
                             
                                    <asp:CheckBox ID="chkDurationTill" runat="server"
                                        Text="Till Present"  AutoPostBack="True" 
                                        oncheckedchanged="chkDurationTill_CheckedChanged" TabIndex ="9" style=" display :inline " />
                              
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="TableRow">
                            <div class="TableFormValidatorContent" style=" margin-left :42%">
                             <asp:CustomValidator ID="CustomValidatorFromDate" runat="server"
                                            controltovalidate="wdcEmpDurationFromDate" 
                                            errormessage="" 
                                            ClientValidationFunction="ValidatedateExp" ValidationGroup="Experience"/>
                               <asp:CustomValidator ID="CustomValidatorToDate" runat="server"
                                        controltovalidate="wdcEmpDurationToDate" 
                                        errormessage="" 
                                        ClientValidationFunction="ValidatedateExp" ValidationGroup="Experience"/>
                                    <asp:Label ID="LblDatevalidate" runat="server" Text="" ForeColor="Red"></asp:Label>                      
                            </div>
                        </div>
                       
                        <br />
                     
                        <div>
                            <div class="TableFormLeble">
                                <asp:Label EnableViewState="false" ID="lblProjectDetails" runat="server" Text="Notes"
                                    ></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox EnableViewState="false" ID="txtProjectDetails" runat="server" CssClass="CommonTextBox"
                                    TextMode="MultiLine" Rows="8"  TabIndex ="10" Width="350px"></asp:TextBox>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                            </div>
                            <div class="TableFormContent">
                                <asp:Button ID="btnSave" CssClass="CommonButton" ValidationGroup="Experience" runat="server"
                                    Text="Save" OnClick="btnSave_Click" TabIndex ="11"/>  <%-- 0.5 added UseSubmitBehavior --%>
                            </div>
                          
                        </div>
                    <div class="TabPanelHeader" >
                    Experience History
                    </div>
                    </div>
                    
                    
                    
                    <div class="GridContainer">
                        <asp:ObjectDataSource ID="odsExperienceList" runat="server" SelectMethod="GetPagedByMemberId"
                            TypeName="TPS360.Web.UI.MemberExperienceDataSource" EnablePaging="true" SelectCountMethod="GetListCount"
                            SortParameterName="sortExpression">
                            <SelectParameters>
                                <asp:Parameter Name="memberId" DefaultValue="0" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <asp:ListView ID="lsvExperience" runat="server" DataKeyNames="Id" OnItemDataBound="lsvExperience_ItemDataBound"
                            OnItemCommand="lsvExperience_ItemCommand"   OnPreRender ="lsvExperience_PreRender">
                            <LayoutTemplate>
                                <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                    <tr>
                                        <th style="width: 20px !important; "> <asp:Image ID="imgAll" ImageUrl="../Images/expand_all.gif"  runat ="server"  Visible ="false"  /></th>
                                        <th style="width: 20%; white-space: nowrap;"> <asp:LinkButton ID="btnDuration" runat="server" ToolTip="Sort by Duration" CommandName="Sort" CommandArgument="DateFrom" Text="Duration"  TabIndex ="12"/></th>
                                        <th style="width: 15%; white-space: nowrap;"><asp:LinkButton ID="btnJobTitle" runat="server" ToolTip="Sort by Job Title" CommandName="Sort" CommandArgument="PositionName" Text="Job Title"  TabIndex ="13"/></th>
                                        <th style="width: 20%; white-space: nowrap;"> <asp:LinkButton ID="btnEmployerName" runat="server" ToolTip="Sort By Employer Name" CommandName="Sort" CommandArgument="CompanyName" Text="Employer Name"  TabIndex ="14"/></th>
                                        <th style="width: 15%; white-space: nowrap;"><asp:LinkButton ID="btnLocation" runat="server" ToolTip="Sort by Location" CommandName="Sort" CommandArgument="City" Text="Location"  TabIndex ="15"/> </th>
                                        <th style="width: 20%; white-space: nowrap;"><asp:LinkButton ID="btnFunctionalCategory" runat="server" ToolTip="Sort by Functional Category" CommandName="Sort" CommandArgument="Name" Text="Category" TabIndex ="16" /></th>
                                        <th style="text-align: center; width: 40px !important;" runat ="server" id="thAction">Action</th>
                                    </tr>
                                      <tr  >
                                                <td colspan ="7" style =" padding : 0px 0px 0px 0px; border-style : none none none none "></td>
                                               </tr>
                                    <tr id="itemPlaceholder" runat="server">
                                    </tr>
                                    <tr class="Pager">
                                        <td colspan="7"  runat ="server" id="tdPager">
                                            <ucl:Pager Id="pagerControl" runat ="server" EnableViewState ="true"  />
                                        </td>
                                    </tr>
                                </table>
                            </LayoutTemplate>
                            <EmptyDataTemplate>
                                <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                    <tr>
                                        <td>No Work History available.</td>
                                    </tr>
                                </table>
                            </EmptyDataTemplate>
                            <ItemTemplate>
                                <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>' >
                                    <td style="width: 5%; white-space: nowrap; border-right-style : none ;"><asp:Image ID="imgShowHide" ImageUrl="~/Images/expand_all.gif" runat="server" /></td>
                                    <td style="width: 15%; white-space: normal;  border-right-style : none ; border-left-style :none ;"><asp:Label ID="lblDuration" runat="server" /></td>
                                    <td style="width: 15%; white-space:normal;  border-left-style :none ;"><asp:Label ID="lblJobTitle" runat="server" /></td>
                                    <td style="width: 20%; white-space: normal;"><asp:Label ID="lblEmployerName" runat="server" /></td>
                                    <td style="width: 10%; white-space: normal;  border-left-style :none ;" ><asp:Label ID="lblLocation" runat="server" /></td>
                                    <td style="width: 15%;  border-left-style :none ;"><asp:Label ID="lblFunctionalCategory" runat="server" /></td>
                                    <td style="width: 10%; white-space: nowrap; text-align: center;  border-left-style :none ;" runat ="server" id ="tdAction">
                                        <asp:ImageButton ID="btnEdit" SkinID="sknEditButton" runat="server" CommandName="EditItem"
                                            CausesValidation="false" ToolTip="Edit"  TabIndex ="17" OnClientClick ="ScrollTopPosition()"></asp:ImageButton>
                                        <asp:ImageButton ID="btnDelete" SkinID="sknDeleteButton" runat="server" CommandName="DeleteItem"
                                            ToolTip="Delete"  TabIndex ="17"></asp:ImageButton>
                                    </td>
                                </tr>
                                <tr runat ="server" id="trDetail" style =" display :none " class='<%# Container.DataItemIndex % 2 == 0 ? "Expandrow" : "Expandaltrow" %>'  >
                                    <td colspan="7" style="text-align: left;" >
                                            <strong>Notes:&nbsp;&nbsp;</strong><asp:Label ID="lblProjectDetails" runat="server"/>
                                    </td>
                                </tr> 
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
           
</div>
</ContentTemplate>
</asp:UpdatePanel>