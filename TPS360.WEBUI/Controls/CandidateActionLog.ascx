﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CandidateActionLog.ascx.cs"
    Inherits="TPS360.Web.UI.CandidateActionLog" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
<asp:HiddenField ID="hdnRowPerPageName" runat="server" />
<asp:HiddenField ID="hdnTotalRowCount" runat="server" />
<asp:HiddenField ID="hdnStartRowIndex" runat="server" />
<asp:HiddenField ID="hdnMaxRow" runat="server" />

<script type="text/javascript">



    //onkeydown="if (event.keyCode==13) {event.keyCode=9; return event.keyCode }

    function validate(source, arguments) {

        var hdnOldPageNo = document.getElementById('<%= hdnOldPageNo.ClientID %>');

        var hdntxtSliderClientID = document.getElementById('<%= hdntxtSliderClientID.ClientID %>');
        var txtSlider = document.getElementById(hdntxtSliderClientID.value);
        if (txtSlider != null) {
            if (hdnOldPageNo.value == txtSlider.value) {
                arguments.IsValid = false; return;
            }

            arguments.IsValid = true;
        }

    }




    function entername() {
        var hdnOldPageNo = document.getElementById('<%= hdnOldPageNo.ClientID %>');
        var hdntxtSliderClientID = document.getElementById('<%= hdntxtSliderClientID.ClientID %>');
        var txtSlider = document.getElementById(hdntxtSliderClientID.value);
        if (txtSlider != null)
            hdnOldPageNo.value = txtSlider.value;
    }

</script>

<div>
    <div align="left">
        <asp:HiddenField ID="hfMemberId" runat="server" />
        <asp:HiddenField ID="hfJobPostingId" runat="server"  Value ="0"/>
        <asp:HiddenField ID="hfCreatorId" runat="server" />
    </div>
    <div style="padding-top: 5px">
        <asp:ObjectDataSource ID="odsEventLog" runat="server" SelectMethod="GetPaged" TypeName="TPS360.Web.UI.EventLogDataSource"
            SelectCountMethod="GetListCount" EnablePaging="True" SortParameterName="sortExpression">
            <SelectParameters>
                <asp:Parameter Name="JobPostingId" DefaultValue="0" />
                <asp:Parameter Name="CandidateId" DefaultValue="0" />
                <asp:Parameter Name="CreatorId" DefaultValue="0" />
                <asp:Parameter Name ="ClientId" DefaultValue ="0" />
            </SelectParameters>
        </asp:ObjectDataSource>        
        <asp:UpdatePanel ID="upHiringLog" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <%--<asp:HiddenField ID="hdnShowOrHide" runat ="server" />--%>
                <asp:HiddenField ID="hdnOldPageNo" runat="server" />
                <asp:HiddenField ID="hdntxtSliderClientID" runat="server" />
                <asp:HiddenField ID="hdnSortColumn" runat="server" />
                <asp:HiddenField ID="hdnSortOrder" runat="server" />
                <div align="left">
                     <asp:ImageButton ID="btnExport" CssClass ="btn" SkinID ="sknExportToExcel" runat ="server"  OnClick="btnExport_Click"  />
                    </div>
                <div style="max-height: 450px; overflow: auto">
                    <asp:ListView ID="lsvHiringLog" runat="server" DataKeyNames="Id" OnItemDataBound="lsvHiringLog_ItemDataBound"
                        OnItemCommand="lsvHiringLog_ItemCommand" OnPreRender="lsvHiringLog_PreRender">
                        <LayoutTemplate>
                            <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                <tr>
                                    <th style="width: 125px !important">
                                        <asp:LinkButton ID="btnDate" runat="server" Text="Date & Time" CommandName="Sort"
                                            CommandArgument="[EL].[ActionDate]" />
                                    </th>
                                    <th style=" white-space: nowrap;">
                                        <asp:LinkButton ID="btnUser" runat="server" Text="User" CommandName="Sort" CommandArgument="[M].[FirstName]" />
                                    </th>
                                    <th style=" white-space: nowrap;">
                                        <asp:LinkButton ID="btnRequisition" runat="server" Text="Requisition" CommandName="Sort" CommandArgument="[J].[JobTitle]" />
                                    </th>
                                    <th style=" white-space: nowrap;">
                                        <asp:LinkButton ID="btnAction" runat="server" Text="Action" CommandName="Sort" CommandArgument="[EL].[ActionType]" />
                                    </th>
                                </tr>
                                <tr id="itemPlaceholder" runat="server">
                                </tr>
                                <tr class="Pager">
                                <td colspan="4" id="tdPager" runat="server">
                                    <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                </td>
                            </tr>
                            </table>
                            
                        </LayoutTemplate>
                        <EmptyDataTemplate>
                            <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                <tr>
                                    <td>
                                        No data was returned.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'
                                style="text-align: left">
                                <td>
                                    <asp:Label ID="lblDate" runat="server" />
                                </td>
                                <td >
                                    <asp:Label ID="lblUser" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblRequisition" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblAction" runat="server" />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnExport" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</div>
