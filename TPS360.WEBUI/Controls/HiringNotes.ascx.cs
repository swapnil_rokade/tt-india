﻿using System;
using System.Web.UI.WebControls;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;

namespace TPS360.Web.UI
{
    public partial class HiringNotes : TemplateUserControl
    {
        #region MemberVariables
        CommonNote note = new CommonNote();
        MemberNote membernote = new MemberNote();
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Data != null)
            {
                hfMemberId.Value = Data.ToString();
                lblTitle.Text = MiscUtil.GetMemberNameById(Convert.ToInt32(hfMemberId.Value),Facade ) + " - Hiring Notes";
            }
            if (DataValue != null)
            {
                txtHiringNotes.Text = MiscUtil.RemoveScript(DataValue.ToString().Replace ("<br/>","\n"), "");
                hfNote.Value = DataValue.ToString();
                if(NoteUpdatorName !=null && NoteUpdateTime !=null && NoteUpdatorName.ToString () !=string .Empty && NoteUpdateTime.ToString () !=string .Empty  )
                lblStatus.Text  = "Last updated by " + NoteUpdatorName + " on " + NoteUpdateTime + ".";
            }
            if (JobPostingID != null)
                hfJobPostingId.Value = JobPostingID.ToString();
            hfCurrentMemberId.Value = CurrentMember.Id.ToString ();
        }
       
       
    }
}
