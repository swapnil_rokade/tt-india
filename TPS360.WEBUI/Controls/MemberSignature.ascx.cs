﻿/*
---------------------------------------------------------------------------------------------------------------  
    FileName: MemberSignature.ascx.cs
    Description: This is the control which is used to create the signatures.
    Created By: 
    Created On:
    Modification Log:
    -----------------------------------------------------------------------------------------------------------   
    Ver.No.             Date                Author              MOdification
    -----------------------------------------------------------------------------------------------------------  
    0.1               13-Feb-2009         Sandeesh         Defect id:9387-  Added code to edit the logos in the saved Signatures 
    0.2               17-Feb-2009         Sandeesh         Defect id:9388-  Added code to provide an option for the employees to delete their saved signatures.
    0.3               17-Apr-2009         Gopala Swamy J   Defect Id:10362- Else statement added  
    0.4               20-Apr-2009         Gopala Swamy J   Defect Id:10366- Called function "LoadUploadedSignature()"
    0.5               20-Apr-2009         Gopala Swamy J   Defect Id:10356- commented a line
    0.6               27-Apr-2009         Nagarathna V.B   Defect Id:10404,10403: corrected wrong alert messages.
 *  0.7               11-Mar-2010         Sudarshan R      Defect Id:12237- Enabled logo Upload button in EditMode
 -----------------------------------------------------------------------------------------------------------  
 */

using System;
using TPS360.Web.UI;
using TPS360.Common.Helper;
using TPS360.Common.BusinessEntities;
using System.IO;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using TPS360.Web.UI.Helper;
using FreeTextBoxControls;
namespace TPS360.Web.UI
{
    public partial class ControlsMemberSignature : BaseControl
    {

        #region Member Variables

        private int _memberId = 0;
        private string _userId = string.Empty;

        #endregion

        #region Methods

        private void LoadMemberData()
        {
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
            }
            else
            {
                _memberId = base.CurrentMember.Id;
            }

            if (_memberId > 0)
            {
                hfMemberId.Value = _memberId.ToString();

                Member currentMember = new Member();
                currentMember = Facade.GetMemberById(_memberId);
                hfMemberUserId.Value = currentMember.UserId.ToString();
                _userId = hfMemberUserId.Value;
            }
        }

        private void UploadLogo()
        {
            string strMessage = string.Empty;
            string strFilename;
            if (hfMemberLogoId.Value == string.Empty) //new sign
            {
                    MemberSignature newSignature = new MemberSignature();
                    newSignature.CreateDate = DateTime.Now;
                    newSignature.MemberId = _memberId;
                    newSignature.CreatorId = base.CurrentMember.Id;
                    newSignature.Signature = txtSignature.Text;
                    newSignature.Logo = "";
                    if (chkActive.Checked)
                        newSignature.Active = true;
                    else
                        newSignature.Active = false;
                    newSignature=Facade.AddMemberSignature(newSignature);
                if(newSignature !=null )
                    hfMemberLogoId.Value = newSignature.Id.ToString ();
                    strMessage = "Signature saved successfully";
                    MiscUtil.ShowMessage(lblMessage, strMessage, false);
                  
            }
            else
            {
               
                MemberSignature updatedSignature = new MemberSignature();
                if (chkActive.Checked == true)
                {
                    updatedSignature.Active = true;
                }
                else
                {
                    updatedSignature.Active = false;
                }

                updatedSignature.UpdateDate = DateTime.Now;
                updatedSignature.MemberId = _memberId;
                updatedSignature.Id = Convert.ToInt32(hfMemberLogoId.Value.Trim());
                updatedSignature.Logo = hfMemberLogoName.Value;
                updatedSignature.UpdatorId = base.CurrentMember.Id;
                
                updatedSignature.Signature = txtSignature.Text;
                Facade.UpdateMemberSignature(updatedSignature);
                strMessage = "Signature updated successfully";
                MiscUtil.ShowMessage(lblMessage, strMessage, false);
            }
        }
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            _memberId = Int32.Parse(hfMemberId.Value);
            _userId = hfMemberUserId.Value;
           
            if (!IsPostBack)
            {
                LoadMemberData();
                if (_memberId > 0)
                {
                    IList<MemberSignature> lstSignature = Facade.GetAllMemberSignatureByMemberId(_memberId);
                    if (lstSignature != null)
                    {
                        foreach (MemberSignature sign in lstSignature)
                        {
                            txtSignature.Text = sign.Signature.ToString();
                            hfMemberLogoId.Value = sign.Id.ToString();
                            if (sign.Active)
                                chkActive.Checked = true;
                        }
                    }
                }
            }

            lblMessage.Text = "";
            lblMessage.CssClass = "";
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            UploadLogo();
            btnSave.CausesValidation = true;
            hfMemberLogoName.Value = String.Empty;
            
        }

        #endregion

    }
}
