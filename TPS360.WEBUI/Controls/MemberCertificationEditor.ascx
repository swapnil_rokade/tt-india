﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MemberCertificationEditor.ascx.cs"
    Inherits="TPS360.Web.UI.MemberCertificationEditor" %>
<%@ Register Assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.Web.UI.EditorControls" TagPrefix="ig" %>
<%@ Register assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.Web.UI.LayoutControls" tagprefix="ig" %>

<%@ Register assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.Web.UI.LayoutControls" tagprefix="ig" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>

<script type="text/javascript" language="javascript">
  

     function ValidatedateLicense(source, args)
    {    
        var LblDatevalidate = document .getElementById  ('ctl00_cphCandidateMaster_uwtResumeBuilder__ctl6_ucntrlCertification_LblDatevalidate');
        LblDatevalidate.innerHTML = "";

        //debugger;
        var CVEduDate = source.id.substring(source.id.lastIndexOf('_') + 1, source.id.length);
        switch(CVEduDate)
        {
            case "CustomValidatorFromDate":
            strStartDate = args.Value.split('-');
            strEndDate = $get('ctl00_cphCandidateMaster_uwtResumeBuilder__ctl6_ucntrlCertification_wdcValidTo').value.split('-');            
            break;
            
            case "CustomValidatorToDate":
            strStartDate = $get('ctl00_cphCandidateMaster_uwtResumeBuilder__ctl6_ucntrlCertification_wdcValidFrom').value.split('-');
            strEndDate = args.Value.split('-');            
            break;                      
        }
        var dateTime = new Date;
        DateCompare = new Date(dateTime.getFullYear(),dateTime.getMonth(),dateTime.getDate());
        strStartDate = new Date(strStartDate[0], strStartDate[1] - 1, strStartDate[2]);
        strEndDate = new Date(strEndDate[0], strEndDate[1] - 1, strEndDate[2]);
                        
        if (strStartDate > DateCompare) 
        {
            LblDatevalidate.innerHTML = "'Duration' Start date should not be greater than current date.";
            LblDatevalidate.style.display = "block";
            args.IsValid=false;
        }
        else 
        {
           
           
                if (strStartDate > strEndDate) 
                {
                    LblDatevalidate.innerHTML = "'Duration' Start date should not be greater than End Date.";
                    LblDatevalidate.style.display = "block";
                    args.IsValid=false;
                }
                else 
                {
                    LblDatevalidate.innerHTML = "";
                    args.IsValid=true;
                }
            
        }
    }
       
</script>

<asp:UpdatePanel ID="upCertification" runat="server">
    <ContentTemplate>
        <div style="width: 100%">
            <div id="dvCertification" runat="server" style="width: 100%">
                <div class="TabPanelHeader">
                    Add License/Certification
                </div>
                <div class="TableRow" style="text-align: center">
                    <asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
                    <asp:HiddenField ID="hfMemberId" runat="server" Value="0" />
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label EnableViewState="false" ID="lblCertification" runat="server" Text="License/Certification"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <asp:TextBox ID="txtCertificationName" runat="server" CssClass="CommonTextBox" EnableViewState="false"></asp:TextBox>
                        <span class="RequiredField">*</span>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:RequiredFieldValidator ID="rfvCertification" runat="server" ControlToValidate="txtCertificationName"
                                ErrorMessage="Please enter License/Certification." EnableViewState="False" Display="Dynamic"
                                ValidationGroup="CertificationInfo"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label EnableViewState="false" ID="lblValidFrom" runat="server" Text="Valid From"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <table>
                            <tr>
                                <td>
                                   <%-- <igsch:WebDateChooser ID="wdcValidFrom" runat="server" Editable="true" EnableKeyboardNavigation="true"
                                        NullDateLabel="" CalendarLayout-MinDate="1/1/1900 12:00:00 AM" CalendarLayout-HideOtherMonthDays="true">
                                    </igsch:WebDateChooser>--%>
                                    <ig:WebDatePicker ID="wdcValidFrom"  DropDownCalendarID="ddcValidFrom" runat="server"></ig:WebDatePicker>
                                    <ig:WebMonthCalendar  ID="ddcValidFrom" AllowNull="true"  runat="server"></ig:WebMonthCalendar>
                                </td>
                                <td>
                                    <asp:Label EnableViewState="false" ID="lblValidTo" runat="server" Text="To"></asp:Label>:
                                </td>
                                <td>
                                  <%--  <igsch:WebDateChooser ID="wdcValidTo" runat="server" Editable="true" EnableKeyboardNavigation="true"
                                        NullDateLabel="" CalendarLayout-MinDate='<%#Eval("wdcValidFrom") %>' CalendarLayout-HideOtherMonthDays="true">
                                    </igsch:WebDateChooser>--%>
                                    <ig:WebDatePicker ID="wdcValidTo"  DropDownCalendarID="ddcValidTo" runat="server"></ig:WebDatePicker>
                                    <ig:WebMonthCalendar  ID="ddcValidTo" AllowNull="true"  runat="server"></ig:WebMonthCalendar>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:CustomValidator ID="CustomValidatorFromDate" runat="server" Display="Dynamic"
                            ControlToValidate="wdcValidFrom" ErrorMessage="" ClientValidationFunction="ValidatedateLicense"
                            ValidationGroup="CertificationInfo" />
                        <asp:CustomValidator ID="CustomValidatorToDate" runat="server" Display="Dynamic"
                            ControlToValidate="wdcValidTo" ErrorMessage="" ClientValidationFunction="ValidatedateLicense"
                            ValidationGroup="CertificationInfo" />
                        <asp:Label ID="LblDatevalidate" runat="server" Text="" ForeColor="Red"></asp:Label>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label EnableViewState="false" ID="lblIssuer" runat="server" Text="Issuing Authority"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <asp:TextBox ID="txtIssuer" runat="server" CssClass="CommonTextBox" EnableViewState="false"></asp:TextBox>
                    </div>
                    <div class="TableRow" style="text-align: center">
                        <asp:Button ID="btnSave" CssClass="CommonButton" runat="server" Text="Save" ValidationGroup="CertificationInfo"
                            OnClick="btnSave_Click" UseSubmitBehavior="true" />
                    </div>
                </div>
                <div class="TabPanelHeader">
                    List of License/Certifications
                </div>
            </div>
            <div style="width: 100%">
                <asp:HiddenField ID="hdnSortColumnName" runat="server" EnableViewState="true" />
                <asp:HiddenField ID="hdnSortColumnOrder" runat="server" EnableViewState="true" />
                <asp:TextBox ID="hdnSortColumn" runat="server" EnableViewState="true" Visible="false" />
                <asp:TextBox ID="hdnSortOrder" runat="server" EnableViewState="true" Visible="false" />
                <div class="GridContainer" style="text-align: left;">
                    <asp:ObjectDataSource ID="odsCertificationList" runat="server" SelectMethod="GetPagedByMemberId"
                        TypeName="TPS360.Web.UI.MemberCertificationDataSource" EnablePaging="true" SelectCountMethod="GetListCount"
                        SortParameterName="sortExpression" OnInit="odsCertificationList_Init"></asp:ObjectDataSource>
                    <asp:ListView ID="lsvCertifications" runat="server" DataKeyNames="Id" DataSourceID="odsCertificationList"
                        OnItemDataBound="lsvCertifications_ItemDataBound" OnPreRender="lsvCertifications_PreRender"
                        OnItemCommand="lsvCertifications_ItemCommand" EnableViewState="true">
                        <LayoutTemplate>
                            <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                <tr>
                                    <th>
                                        <asp:LinkButton ID="btnCertification" runat="server" CommandName="Sort" ToolTip="Sort By Skill"
                                            CommandArgument="CertificationName" Text="License/Certification" />
                                    </th>
                                    <th style="width: 30%; white-space: nowrap;">
                                        <asp:LinkButton ID="btnValid" runat="server" ToolTip="Sort By Validity" CommandName="Sort"
                                            CommandArgument="Valid" Text="Valid" />
                                    </th>
                                    <th style="width: 30%; white-space: nowrap;">
                                        <asp:LinkButton ID="btnAuthoriy" runat="server" CommandName="Sort" CommandArgument="IssuingAthority"
                                            Text="Issuing  Athority" ToolTip="Sort By Issuing Athority" />
                                    </th>
                                    <th style="text-align: center; width: 40px !important;" id="thAction" runat="server">
                                        Action
                                    </th>
                                </tr>
                                <tr id="itemPlaceholder" runat="server">
                                </tr>
                                <tr class="Pager">
                                    <td colspan="4" runat="server" id="tdPager">
                                        <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                    </td>
                                </tr>
                            </table>
                        </LayoutTemplate>
                        <EmptyDataTemplate>
                            <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                <tr>
                                    <td>
                                        No License/Certification data available.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                <td>
                                    <asp:Label ID="lblCertification" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblValid" runat="server" />
                                </td>
                                <td style="white-space: nowrap;">
                                    <asp:Label ID="lblIssuingAthority" runat="server" />
                                </td>
                                <td style="text-align: center;" id="tdAction" runat="server">
                                    <asp:ImageButton ID="btnEdit" SkinID="sknEditButton" CausesValidation="false" runat="server"
                                        CommandName="EditItem"></asp:ImageButton>
                                    <asp:ImageButton ID="btnDelete" Visible="true" SkinID="sknDeleteButton" runat="server"
                                        CommandName="DeleteItem"></asp:ImageButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
