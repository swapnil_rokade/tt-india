﻿ <%-- 
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :   InterviewerFeedbac.ascx
    Description         :   This page is used to fill up Interviewer Feed back .
    Created By          :   Prasanth
    Created On          :   20/Jul/2015
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1                 23/Dec/2015         Prasanth Kumar G    Bugfix in Upload Document
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------        
--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterviewerFeedback.ascx.cs" Inherits="TPS360.Web.UI.ControlsInterviewerFeedback" %>

 
 <script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>
 <script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>
 

<script type="text/javascript">
    function UploadFile(fileUpload) {
        if (fileUpload.value != '') {
           document.getElementById("<%=btnUpload.ClientID %>").click();
           
        }
    }
</script>

 
 
 
  <asp:UpdatePanel ID="updPnlInterviewfeedback" runat="server" >
            <ContentTemplate>
 
     <div class="TableRow" style="text-align: center">
            <asp:Label ID="lblMessage" runat="server" EnableViewState="false" Style="z-index: 9999"></asp:Label>
     </div>
     
 
    <div class="FormLeftColumn">
     
        <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label ID="Label1" runat="server" Text="First Name"></asp:Label>:
            </div>
            <div class="TableFormContent">
            <asp:TextBox ID="txtFirstName" runat="server" CssClass="CommonTextBox"  ValidationGroup ="NoteValidation" ></asp:TextBox>
                <span class="RequiredField">*</span>
                <asp:RequiredFieldValidator ID ="rfvFirstName" runat ="server" Display ="Dynamic"  ErrorMessage ="Please Enter First Name." ControlToValidate ="txtFirstName" ValidationGroup ="NoteValidation"></asp:RequiredFieldValidator> 
            </div>
        </div>
       
        <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label ID="Label2" runat="server" Text="Last Name"></asp:Label>:
            </div>
            <div class="TableFormContent">
            <asp:TextBox ID="txtLastName" runat="server" CssClass="CommonTextBox"  ValidationGroup ="NoteValidation" ></asp:TextBox>
                <span class="RequiredField">*</span>
                 <asp:RequiredFieldValidator ID ="rsvLastName" runat ="server" Display ="Dynamic"  ErrorMessage ="Please Enter Last Name." ControlToValidate ="txtLastName" ValidationGroup ="NoteValidation"></asp:RequiredFieldValidator> 
            </div>
        </div>
        
        <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label ID="Label3" runat="server" Text="Email"></asp:Label>:
            </div>
            <div class="TableFormContent">
            <asp:TextBox ID="txtEmail" runat="server" CssClass="CommonTextBox"  ValidationGroup ="NoteValidation" Enabled="false" ></asp:TextBox>
             
            </div>
        </div>
       
        <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label ID="Label4" runat="server" Text="Employee ID"></asp:Label>:
            </div>
            <div class="TableFormContent">
            <asp:TextBox ID="txtEmployeeid" runat="server" CssClass="CommonTextBox"  ValidationGroup ="NoteValidation" ></asp:TextBox>
                <span class="RequiredField">*</span>
                <asp:RequiredFieldValidator ID ="rfvEmployeeId" runat ="server" Display ="Dynamic"  ErrorMessage ="Please Enter Employye ID." ControlToValidate ="txtEmployeeid" ValidationGroup ="NoteValidation"></asp:RequiredFieldValidator> 
            </div>
        </div>
      
      
      </div>
      
    <div class="FormRightColumn">
     
       
        <div class ="TableRow">
            <div  class ="TableFormValidatorContent" style="text-align:center">
              
            </div>
        </div>
        
        <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label ID="lblRound" runat="server" Text="Round"></asp:Label>:
            </div>
            <div class="TableFormContent">
            <asp:DropDownList ID="ddlInterviewRounds" runat="server" CssClass="CommonDropDownList" ValidationGroup ="NoteValidation" ></asp:DropDownList>
                 
            </div>
        </div>
        
        <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label ID="lblFeedback" runat="server" Text="Final Comments"></asp:Label>:
            </div>
            <div class="TableFormContent">
            <asp:TextBox ID="txtNotes" runat="server" CssClass="CommonTextBox" TextMode="MultiLine" Rows="6" ValidationGroup ="NoteValidation" ></asp:TextBox>
                <span class="RequiredField">*</span>
                <asp:RequiredFieldValidator ID ="rfvNotes" runat ="server" Display ="Dynamic"  ErrorMessage ="Please enter comments." ControlToValidate ="txtNotes" ValidationGroup ="NoteValidation"></asp:RequiredFieldValidator> 
            </div>
            
            
        </div>
        
        <div class ="TableRow">
            <div  class ="TableFormValidatorContent" style="text-align:center">
                
            </div>
        </div>
        
             <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label ID="Label5" runat="server" Text="Overall Feedback"></asp:Label>:
            </div>
            <div class="TableFormContent">
            <asp:DropDownList ID="DdlOverallFeedback" runat="server" CssClass="CommonDropDownList" ValidationGroup ="NoteValidation" >
            <asp:ListItem Value = "0" Text = "Please Select"/>
            <asp:ListItem Value = "1" Text = "Selected"/>
            <asp:ListItem Value = "2" Text = "Rejected"/>
            <asp:ListItem Value = "3" Text = "On hold"/>
            </asp:DropDownList>
                <span class="RequiredField">*</span>
            <asp:RequiredFieldValidator ID="RfvOverallFeedback" runat="server" ControlToValidate="DdlOverallFeedback" ErrorMessage="Please Select ."
            InitialValue="0" Display="Dynamic" ValidationGroup="NoteValidation"></asp:RequiredFieldValidator>
            </div>
             
        </div>    
        <div class="TableRow" runat="server" id="rowDocumentType">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblDocumentType" runat="server" Text="Document Type"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <asp:DropDownList ID="ddlDocumentType" runat="server" 
                            CssClass="CommonDropDownList">
                        </asp:DropDownList>
                      
                    </div>
         </div>
         
    
    
        <div id="divUploadFile" enableviewstate="true">
                <div class="TableRow">
                    <div class="TableFormLeble">
                        Upload Document:</div>
                    <div class="TableFormContent">
                         <asp:FileUpload ID="fuDocument" runat="server"  size="40"   Style="float: left;" EnableViewState="true"
                            CssClass="CommonButton" />
                           
                          <asp:Button ID="btnUpload" Text="Upload" runat="server" Style="display: none" 
                             onclick="btnUpload_Click" />
                              
                    </div>
                   
                </div>
                <div class="TableRow">
                                    <asp:RegularExpressionValidator Visible="true" ID="revDocument" runat="server" ErrorMessage="Please Upload JPGs,BMPs,GIFs,DOCs/DOCXs,RTFs,TXTs,ZIPs,XLS,XLSX,CSV,PPT,PPTX and PDFs onlys."
                                        ValidationExpression="(.*\.[jJ][pP][gG])|(.*\.[dD][oO][cC][xX])|(.*\.[dD][oO][cC])|(.*\.[rR][tT][fF])|(.*\.[tT][xX][tT])|(.*\.[zZ][iI][pP])|(.*\.[pP][dD][fF])|(.*\.[xX][lL][sS][xX])|(.*\.[xX][lL][sS])|(.*\.[cC][sS][vV])|(.*\.[pP][pP][tT][xX])|(.*\.[pP][pP][tT]|(.*\.[gG][iI][fF])|(.*\.[bB][mM][pP])|(.*\.[pP][nN][gG]))"
                                        ControlToValidate="fuDocument" ValidationGroup="UploadDocument"></asp:RegularExpressionValidator>
                                        
                                </div>
                <div class="TableRow">
                                    <asp:RegularExpressionValidator Visible="false" ID="revPhoto" runat="server" ErrorMessage="Please Upload JPEGs,JPGs , GIFs ,PDFs and ZIPs onlya."
                                        ValidationExpression="(.*\.[jJ][pP][eE][gG])|(.*\.[pP][dD][fF])|(.*\.[jJ][pP][gG])|(.*\.[gG][iI][fF])|(.*\.[zZ][iI][pP])|(.*\.[xX][lL][sS][xX])|(.*\.[xX][lL][sS])|(.*\.[cC][sS][vV])"
                                        ControlToValidate="fuDocument" ValidationGroup="UploadDocument"></asp:RegularExpressionValidator>
                                </div>
                <div class="TableRow">
                                    <asp:RegularExpressionValidator Visible="false" ID="revVideo" runat="server" ErrorMessage="Please Upload WMV,MPG and AVI onlya."
                                        ValidationExpression="(.*\.[wW][mM][vV])|(.*\.[mM][pP][gG])|(.*\.[aA][vV][iI])|(.*\.[xX][lL][sS][xX])|(.*\.[xX][lL][sS])|(.*\.[cC][sS][vV])"
                                        ControlToValidate="fuDocument" ValidationGroup="UploadDocument"></asp:RegularExpressionValidator>
                                </div>
            </div>                            
        <div class="TableRow">
                    <div class="TableFormValidatorContent" style="clear: both; color: #888888; margin-left: 42%">
                        Supported File Formats: .doc, .docx, .pdf, .rtf, .txt
                    </div>
                </div>
                
  
          <div class="TableRow" runat="server" id="Div1">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblFile" runat="server" Text="Selected file"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <asp:Label ID="LblFileName" runat="server" Text=""></asp:Label>:
                    </div>
         </div>       
     
         
    </div>
       <br />
         <div class="TableRow" style="text-align: center">
                    <asp:HiddenField ID="hdnInterviewFeebackId" runat ="server" />
                    <asp:Label ID="Label6" runat="server" EnableViewState="false"></asp:Label>
                    <asp:HiddenField ID="hfMemberId" runat="server" Value="0" />
                    <asp:HiddenField ID="hfMemberDocumentId" runat="server" />
                    <asp:HiddenField ID="hfMemberDocumentName" runat="server" />
                    <asp:HiddenField ID="hfDocumentType" runat="server" />
                    <asp:HiddenField ID = "hfFilePath" runat="server" />  <%--line introduced by Prasanth on 29/Dec/2015--%>
                    <asp:HiddenField ID = "hfFileName" runat="server" />  <%--line introduced by Prasanth on 29/Dec/2015--%>
                    <asp:HiddenField ID = "hfDocumentName" runat="server" />  <%--line introduced by Prasanth on 29/Dec/2015--%>
                    
                     
        </div>  
        
     
        <div class="TableRow"> 
            <div class="TableFormLeble">
                <asp:Label ID="lblTitle" runat="server" Text="Title"  visible="false"></asp:Label>
            </div>
            <div class="TableFormContent">
            <asp:TextBox  visible="false" ID="txtTitle" runat="server" CssClass="CommonTextBox"  ValidationGroup ="NoteValidation" ></asp:TextBox>
                
            </div>
        </div>
        
        </ContentTemplate>
         <Triggers>
        <asp:PostBackTrigger ControlID="btnUpload" />
    </Triggers>
 </asp:UpdatePanel>
   
   
   
   