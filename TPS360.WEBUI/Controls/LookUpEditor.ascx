﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LookUpEditor.ascx.cs"
    Inherits="TPS360.Web.UI.ControlLookUpEditor" %>
 <div class="TabPanelHeader" >
        Add Lookup Value
    </div>
   
                    <asp:UpdatePanel ID="pnlDynamic" runat="server">
                        <ContentTemplate>
                            <div>
                                <div class="TableRow" style="text-align: center">
                                    <asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormLeble">
                                        <asp:Label EnableViewState="false" ID="lblLookupName" runat="server" Text="Lookup Name"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:TextBox EnableViewState="false" ID="txtLookupName" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                                        <span class="RequiredField">*</span>
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormValidatorContent" style=" margin-left :42%">
                                        <asp:RequiredFieldValidator ID="rfvLookupName" runat="server" ControlToValidate="txtLookupName"
                                            ErrorMessage="Please enter lookup name." EnableViewState="False" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormLeble">
                                        <asp:Label EnableViewState="false" ID="lblType" runat="server" Text="Type"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:DropDownList EnableViewState="false" ID="ddlType" CssClass="CommonDropDownList" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormValidatorContent" style="padding-left: 41%;">
                                        <asp:RequiredFieldValidator ID="rvfType" InitialValue="0" runat="server" ControlToValidate="ddlType"
                                            ErrorMessage="Please select type." EnableViewState="False" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormLeble">
                                        <asp:Label EnableViewState="false" ID="lblLookupDescription" runat="server" Text="Description"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:TextBox EnableViewState="false" ID="txtLookupDescription" TextMode="MultiLine" Rows="4" Width="300px"
                                            runat="server" CssClass="CommonTextBox" ></asp:TextBox>
                                    </div>
                                </div>
                                <div class="TableRow" style="text-align: center">
                                    <div class="TableFormLeble">
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:Button ID="btnSave" ToolTip="Save" CssClass="CommonButton" runat="server" Text="Save"
                                            OnClick="btnSave_Click" />
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
              