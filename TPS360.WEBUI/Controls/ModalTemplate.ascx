﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ModalTemplate.ascx.cs"
    Inherits="ModalTemplate" %>
<div id="master-container">
    <div id="user-screen" class="modal-container screen">
        <div class="modal-backdrop">
            <div class="modal-border">
                <div class="modal">
                    <div class="header">
                        <span class="title">
                            <asp:Label ID="lbModalTitle" runat="server"></asp:Label></span> <a class="button close"
                                title="Close" onclick="javascript:$find('MPEStatus').hide();" id="imgClose" runat="server">
                            </a>
                    </div>
                    <div class="content" style="width: 400px; height: 250px; display: table-cell; overflow: auto"
                        runat="server" id="divContent">
                        <asp:PlaceHolder ID="ModalPlaceHolder" runat="server"></asp:PlaceHolder>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
