﻿using System;
using System.Web.UI.WebControls;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;

namespace TPS360.Web.UI
{
    public partial class HiringMatrixSource : TemplateUserControl
    {
        #region MemberVariables
        CommonNote note = new CommonNote();
        MemberNote membernote = new MemberNote();
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {

            MiscUtil.PopulateMemberListByRole(ddlEmployee , ContextConstants.ROLE_EMPLOYEE, Facade);
            ddlEmployee.Items.RemoveAt(0);
            if (Data != null)
            {
                hfMemberId.Value = Data.ToString();
                lblTitle.Text = MiscUtil.GetMemberNameById(Convert.ToInt32(hfMemberId.Value),Facade ) + " - Edit Candidate Source";

            }
           
                //txtHiringNotes.Text = MiscUtil.RemoveScript(DataValue.ToString(), "");
               // hfSourceId.Value = DataValue.ToString();
                TPS360 .Common .BusinessEntities. MemberJobCart job = Facade.GetMemberJobCartByMemberIdAndJobPostringId(Convert.ToInt32(hfMemberId.Value),Convert .ToInt32 (JobPostingID .ToString ()));
                ControlHelper.SelectListByValue(ddlEmployee, job.SourceId==0?job.UpdatorId.ToString () : job.SourceId .ToString () );
           
            if (JobPostingID != null)
                hfJobPostingId.Value = JobPostingID.ToString();
            hfCurrentMemberId.Value = CurrentMember.Id.ToString ();
        }
       
       
    }
}
