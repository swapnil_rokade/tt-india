﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ResumeAttachmentModal.ascx.cs"
    Inherits="TPS360.Web.UI.ResumeAttachmentModal" %>
<%@ Register Src="~/Controls/HtmlEditor.ascx" TagName="HtmlEditor" TagPrefix="ucl" %>

<link href="../Style/TabStyle.css" type="text/css" rel="Stylesheet" />

<script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>

<script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

<script type="text/javascript">
 
 
  function CLCheckUnCheckAllRowLevelCheckboxes(cntrlHeaderCheckbox,posCheckbox)
    {

      var cntrlGridView = document.getElementById("tlbTemplate");
      var chkBoxAll=document.getElementById(cntrlHeaderCheckbox);
      var hndID="";
      var chkBox="";
      var checked=0;
      var rowNum=0;
      if (cntrlHeaderCheckbox!=null&&cntrlHeaderCheckbox.checked!=null)
      {
            var rowLength=0;
            rowLength=cntrlGridView.rows.length;
            for(var i=1;i<rowLength;i++)
            {
                var myrow=cntrlGridView.rows[i];
                var mycel=myrow.getElementsByTagName("td")[posCheckbox];
                if (mycel.childNodes[0].value=="on")
                {
                    chkBox=mycel.childNodes[0];
                }
                else 
                {
                    chkBox=mycel.childNodes[1];
                }
                try
                {
                    if (mycel.childNodes[2].value==undefined)
                    {
                        hndID=mycel.childNodes[3];
                    }
                    else 
                    {
                        hndID=mycel.childNodes[2];
                    }
                }
                catch(e ) 
                {
                }
                if(chkBox!=null)
                {
                    if(chkBox.checked==null)
                    {
                        chkBox=chkBox.nextSibling;}
                        if(chkBox!=null)
                        {
                            if(chkBox.checked!=null)
                            {
                                    chkBox.checked=cntrlHeaderCheckbox.checked;
                                    rowNum ++;
                            }
                        }
                    }
                    if(chkBox !=null )
                    {
                        if(chkBox.checked!=null&&chkBox.checked==true)
                        {
                            if  (chkBox.id.indexOf('chkItemCandidate')>-1)
                            {
                                if(hndID != null)ADDID(hndID.value,'<%= hdnSelRes.ClientID%>');
                                checked ++;
                               
                            }
                        }
                        else    
                        {
                            if  (chkBox.id.indexOf('chkItemCandidate')>-1)
                            {
                                if(hndID != null)RemoveID(hndID.value,'<%= hdnSelRes.ClientID%>');
                            }
                        }
                   }
                }
           }

           if(checked==rowNum)
            {
                if(chkBoxAll !=null )
                {
                    chkBoxAll.checked=true;
                }
            }
            else
            {
                if(chkBoxAll !=null )
                {
                    chkBoxAll.checked=false;
                }
            }
   }
   function CheckUnCheckGridViewHeaderCheckboxs(dgrName,chkboxName,posRowChkbox,p)
{

var chkBoxAll=document.getElementById(chkboxName);
var tbl=document.getElementById(dgrName).getElementsByTagName("tbody")[0];
var checked=0;
var rowNum=0;
var rowLength=tbl.rows.length;
var chkBox="";
var hndID="";
for(var i=1;i<rowLength;i++)
{
var myrow=tbl.rows[i];
var mycel=myrow.getElementsByTagName("td")[posRowChkbox];

if (mycel.childNodes[0].value=="on")
{
chkBox=mycel.childNodes[0];
}
else 
{
chkBox=mycel.childNodes[1];
}
try
{
if (mycel.childNodes[2].value==undefined)
{

hndID=mycel.childNodes[3];
}
else 
{
hndID=mycel.childNodes[2];
}
}
catch(e ) 
{
}

if(hndID!=null )
{
if(hndID.value==null)
{
try{
hndID=hndID.childNodes[3];

}
catch (e)
{
}
}
else
{
   
}
}

if(chkBox!=null)
{

if(chkBox.checked==null)
{

chkBox=chkBox.childNodes[1];}
if(chkBox!=null)
{

if(chkBox.checked!=null)
{
rowNum++;}

if(chkBox.checked!=null&&chkBox.checked==true)
{;if(hndID != null)ADDID(hndID.value,'<%= hdnSelRes.ClientID%>');

checked++;}
else{if(hndID != null)RemoveID(hndID.value,'<%= hdnSelRes.ClientID%>');
}

}}}

if(checked==rowNum)
{
chkBoxAll.checked=true;}
else
{
chkBoxAll.checked=false;}}

function RemoveID(ID,hndCtrl)
{

var hdnSelectedIDS=document .getElementById (hndCtrl); 

var arrID=hdnSelectedIDS.value.split(',');
var IDS="";
for(var i=0;i<arrID .length;i++)
{
    if(arrID [i]!=ID)
    {
       if(IDS !="")IDS +=",";
        IDS +=arrID [i];
    }
    hdnSelectedIDS .value=IDS;
    
}

}

function ADDID(ID,hndCtrl)
{
var idAvailable=false ;
var hdnSelectedIDS=document .getElementById (hndCtrl);
var arrID=hdnSelectedIDS.value.split(',');
for(var i=0;i<arrID .length;i++)
{

    if(arrID [i]==ID)
    {
       idAvailable= true ;
        break ;
    }
    
}
if(!idAvailable )
{
       if(hdnSelectedIDS .value!="")hdnSelectedIDS .value +=",";
     hdnSelectedIDS .value= hdnSelectedIDS .value  + ID;
}

}
function minimixeModel()
{
 var pan=document .getElementById ("ctl00_cphHomeMaster_pnlResume");
    pan.style.width="650px";
        pan.style.height="450px";
  pan.style.left="16%";
  
                      pan.style.top="11.5%";
    var pan1=document .getElementById ("ctl00_cphHomeMaster_pnl");
    pan1.style.width="650px";
        pan.style.height="450px";
         pan1.style.left="16%";
                pan1.style.top="11.5%";
}
function maximizeModel()
{
 var pan=document .getElementById ("ctl00_cphHomeMaster_pnlResume");
    pan.style.width="800px";
        pan.style.height="525px";

                pan.style.left="8.5%";
                      pan.style.top="5.4%";
    var pan1=document .getElementById ("ctl00_cphHomeMaster_pnl");
    pan1.style.width="800px";
        pan1.style.height="525px";
          pan1.style.left="8.5%";
                pan1.style.top= "5.4%";
}
function ValidatorChange()
{
var cntrl=document .getElementById ('<%=ddlDocType.ClientID %>');
var doc=document .getElementById ('<%=revDocument.ClientID %>');
var photo=document .getElementById ('<%=revPhoto .ClientID %>');
var video=document .getElementById ('<%=revVideo .ClientID %>');
try
{

if(cntrl.options[cntrl.selectedIndex].text=="Photo")
{
ValidatorEnable(photo, true ); 
ValidatorEnable(doc , false  );
ValidatorEnable(video , false  );
}
else if (cntrl.options[cntrl.selectedIndex].text=="Video Resume")
{
ValidatorEnable(photo, false  ); 
ValidatorEnable(doc , false  );
ValidatorEnable(video , true   );
}
else 
{
ValidatorEnable(photo, false  ); 
ValidatorEnable(doc , true   );
ValidatorEnable(video , false  );
}
}
catch (e)
{
}

}

    

</script>

<div class="GridContainer" style="width: 100%;">
    <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
    <div class="SpecializedTitle" style="font-weight: bold; float: left; padding-bottom: 10px;">
        <asp:UpdatePanel ID="up" runat="server">
            <ContentTemplate>
                <asp:HiddenField ID="hfMemberId" runat="server" EnableViewState="true" />
                <asp:HiddenField ID="hdnSelectedIDS" runat="server" />
                <asp:HiddenField ID="hdnTmpFolder" runat="server" />
                <asp:HiddenField ID="hdnSelId" runat="server" />
                <asp:HiddenField ID="hdnSelRes" runat="server" />
                <asp:HiddenField ID="hdnSortColumn" runat="server" />
                <asp:HiddenField ID="hdnSortOrder" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div style="float: left; width: 100%;">
        <asp:UpdatePanel ID="upModel" runat="server">
            <ContentTemplate>
                <div class="tabbable">
                    <!-- Only required for left/right tabs -->
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab1" data-toggle="tab">Select Existing Resume</a></li>
                        <li><a href="#tab2" data-toggle="tab">Copy/Paste Resume</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1">
                            <div class="TabContentHolder" style =" padding-top : 0px;">
                                <asp:UpdatePanel ID="upPanel" runat="server">
                                    <ContentTemplate>
                                    <div class="TabPanelHeader">Attach Resume Documents </div>
                                        <asp:TextBox ID="SortColumn" runat="server" EnableViewState="true" Visible="false" />
                                        <asp:TextBox ID="SortOrder" runat="server" EnableViewState="true" Visible="false" />
                                        <asp:ObjectDataSource ID="odsDocumentList" runat="server" SelectMethod="GetPagedByMemberId"
                                            OnSelecting="odsDocumentList_Selecting" TypeName="TPS360.Web.UI.MemberDocumentsDataSource"
                                            SelectCountMethod="GetListCount" EnablePaging="True" SortParameterName="sortExpression">
                                            <SelectParameters>
                                                <asp:Parameter Name="MemberID" DefaultValue="0" />
                                                <asp:Parameter Name="SortColumn" DefaultValue="[G].[Name]" />
                                                <asp:Parameter Name="SortOrder" DefaultValue="ASC" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                        <div style="overflow: auto; max-height: 150px;">
                                            <asp:ListView ID="lsvExistingResume" runat="server" DataKeyNames="Id" OnItemDataBound="lsvExistingResume_ItemDataBound"
                                                OnItemCommand="lsvExistingResume_ItemCommand" OnPreRender="lsvExistingResume_PreRender">
                                                <LayoutTemplate>
                                                    <table id="tlbTemplate" class="Grid" cellspacing="0" border="0">
                                                        <tr runat="server" id="trresuem">
                                                            <th style="width: 30px !important; white-space: nowrap;">
                                                                <input id="chkAllItem" name="chkAllItem" type="checkbox" onclick="CLCheckUnCheckAllRowLevelCheckboxes(this,0)" />
                                                            </th>
                                                            <th style="width: 35%; white-space: nowrap;">
                                                                <asp:LinkButton ID="btnDocumentFileName" runat="server" ToolTip="Sort By Document Name"
                                                                    CommandName="Sort" CommandArgument="FileName" Text="File"></asp:LinkButton>
                                                            </th>
                                                            <th style="width: 35%; white-space: nowrap;">
                                                                <asp:LinkButton ID="btnDocumentTitle" runat="server" ToolTip="Sort By Document Title"
                                                                    CommandName="Sort" CommandArgument="Title" Text="Title" />
                                                            </th>
                                                            <th style="width: 25%; white-space: nowrap;">
                                                                <asp:LinkButton ID="btnDocumentType" runat="server" ToolTip="Sort By Document Type"
                                                                    CommandName="Sort" CommandArgument="[G].[Name]" Text="Type" />
                                                            </th>
                                                        </tr>
                                                        <tr id="itemPlaceholder" runat="server">
                                                        </tr>
                                                        </tr>
                                                    </table>
                                                </LayoutTemplate>
                                                <EmptyDataTemplate>
                                                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                                        <tr>
                                                            <td>
                                                                No resume found.
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </EmptyDataTemplate>
                                                <ItemTemplate>
                                                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                                        <td align="left">
                                                            <asp:CheckBox runat="server" ID="chkItemCandidate" OnClick="CheckUnCheckGridViewHeaderCheckboxs('tlbTemplate','chkAllItem',0,1)" />
                                                            <asp:HiddenField ID="hdnID" runat="server" />
                                                        </td>
                                                        <td align="left">
                                                            <asp:Label ID="lblFileName" runat="server" />
                                                        </td>
                                                        <td align="left">
                                                            <asp:Label ID="lblTitle" runat="server"></asp:Label>
                                                        </td>
                                                        <td align="left">
                                                            <asp:Label ID="lblType" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </div>
                                        <div class="TableRow" style="text-align: center; padding-top: 10px">
                                            <asp:Button ID="btnAttach" runat="server" Text="Attach" CssClass="CommonButton" OnClick="btnAttach_Click" />
                                        </div>
                                        <br />
                                        <div class="TabPanelHeader ">Upload New Resume </div>
                                        <div style="padding-left: 35px">
                                            <div class="TableRow">
                                                <div class="TableFormLeble">
                                                    <asp:Label ID="lblDocPath" runat="server" Text="Document Path"></asp:Label>:
                                                </div>
                                                <div class="TableFormContent">
                                                    <asp:FileUpload ID="fuDocPath" runat="server" />
                                                    <span class="RequiredField">*</span>
                                                </div>
                                            </div>
                                            <div class="TableRow">
                                                <div class="TableFormValidatorContent" style="margin-left: 42%">
                                                    <asp:UpdatePanel ID="pnlValidator" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <div class="TableRow">
                                                                <asp:RequiredFieldValidator ID="rfvDocumentUpload" runat="server" ErrorMessage="Please select a file to upload."
                                                                    ControlToValidate="fuDocPath" ValidationGroup="UploadDocument" Display="Dynamic"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="TableRow">
                                                                <asp:RegularExpressionValidator Enabled="false" ID="revDocument" runat="server" ErrorMessage="Please Upload JPGs, JPEGs, BMPs, PNGs, GIFs, TIFFs, DOCs/DOCXs, RTFs, TXTs, ZIPs, XLS/XLXs, CSVs, PPTs/PPTXs and PDFs only."
                                                                    ValidationExpression="(.*\.[jJ][pP][gG])|(.*\.[jJ][pP][eE][gG])|(.*\.[bB][mM][pP])|(.*\.[pP][nN][gG])|(.*\.[tT][iI][fF])|(.*\.[gG][iI][fF])|(.*\.[dD][oO][cC][xX])|(.*\.[dD][oO][cC])|(.*\.[rR][tT][fF])|(.*\.[tT][xX][tT])|(.*\.[zZ][iI][pP])|(.*\.[pP][dD][fF])|(.*\.[xX][lL][sS][xX])|(.*\.[xX][lL][sS])|(.*\.[cC][sS][vV])|(.*\.[pP][pP][tT][xX])|(.*\.[pP][pP][tT])"
                                                                    ControlToValidate="fuDocPath" ValidationGroup="UploadDocument" Display="Dynamic"></asp:RegularExpressionValidator>
                                                            </div>
                                                            <div class="TableRow">
                                                                <asp:RegularExpressionValidator Enabled="false" ID="revPhoto" runat="server" ErrorMessage="Please Upload JPEGs,JPGs,BMPs,PNGs,TIFFs ,GIFs ,PDFs and ZIPs only."
                                                                    ValidationExpression="(.*\.[jJ][pP][eE][gG])|(.*\.[pP][dD][fF])|(.*\.[jJ][pP][gG])|(.*\.[bB][mM][pP])|(.*\.[pP][nN][gG])|(.*\.[tT][iI][fF])|(.*\.[gG][iI][fF])|(.*\.[zZ][iI][pP])"
                                                                    ControlToValidate="fuDocPath" ValidationGroup="UploadDocument" Display="Dynamic"></asp:RegularExpressionValidator>
                                                            </div>
                                                            <div class="TableRow">
                                                                <asp:RegularExpressionValidator Enabled="false" ID="revVideo" runat="server" ErrorMessage="Please Upload WMV,MPG and AVI only."
                                                                    ValidationExpression="(.*\.[wW][mM][vV])|(.*\.[mM][pP][gG])|(.*\.[aA][vV][iI])"
                                                                    ControlToValidate="fuDocPath" ValidationGroup="UploadDocument" Display="Dynamic"></asp:RegularExpressionValidator>
                                                            </div>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="btnUpload" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                            <div class="TableRow">
                                                <div class="TableFormLeble">
                                                    <asp:Label ID="lblDocTitle" runat="server" Text="Document Title"></asp:Label>:
                                                </div>
                                                <div class="TableFormContent">
                                                    <asp:TextBox ID="txtDocTitle" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                                                    <span class="RequiredField">*</span>
                                                </div>
                                            </div>
                                            <div class="TableRow">
                                                <div class="TableFormValidatorContent" style="margin-left: 42%">
                                                    <asp:RequiredFieldValidator ID="rfvDocumentTitle" runat="server" ValidationGroup="UploadDocument"
                                                        ControlToValidate="txtDocTitle" Display="Dynamic" ErrorMessage="Please enter document title."></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="TableRow">
                                                <div class="TableFormLeble">
                                                    <asp:Label ID="lblDocType" runat="server" Text="Document Type"></asp:Label>:
                                                </div>
                                                <div class="TableFormContent">
                                                    <asp:DropDownList ID="ddlDocType" runat="server" CssClass="CommonDropDownList">
                                                    </asp:DropDownList>
                                                    <span class="RequiredField">*</span>
                                                </div>
                                            </div>
                                            <div class="TableRow">
                                                <div class="TableFormValidatorContent" style="margin-left: 42%">
                                                    <asp:CompareValidator ID="cvDocumentType" ValidationGroup="UploadDocument" runat="server"
                                                        ControlToValidate="ddlDocType" ErrorMessage="Please select document type." Operator="NotEqual"
                                                        ValueToCompare="0" Display="Dynamic"></asp:CompareValidator>
                                                </div>
                                            </div>
                                            <div class="TableRow" style="text-align: center; padding-top: 10px">
                                                <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="CommonButton" OnClick="btnUpload_Click"
                                                    ValidationGroup="UploadDocument" OnClientClick="ValidatorChange()" />
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab2" style="width: 100%; overflow: auto;">
                        <asp:UpdatePanel ID="upCopyPasted" runat ="server" >
                        <ContentTemplate >
                        
                            <div class="TabContentHolder">
                                <asp:UpdatePanel ID="upCopyPaste" runat="server">
                                    <ContentTemplate>
                                        <div class="TableRow" style="padding-bottom: 10px;">
                                            <asp:LinkButton ID="lnkRemoveContact" runat="server" Text="Remove Contact Info" OnClick="lnkRemoveContact_Click"
                                                Font-Underline="false" Visible ="false" ></asp:LinkButton>
                                        </div>
                                        <div class="TableRow" style="text-align: left">
                                            <div class="TableFormContent">
                                                <ucl:HtmlEditor ID="txtCopyPasteResume" runat="server" />
                                                
                                            </div>
                                        </div>
                                        <div class="HeaderDevider">
                                        </div>
                                        <div class="TableRow">
                                            <div class="TableFormLeble">
                                                <asp:Label ID="lblOutputType" runat="server" Text="Output Type"></asp:Label>:
                                            </div>
                                            <div class="TableFormContent">
                                                <asp:DropDownList ID="ddlOutputType" runat="server" CssClass="CommonDropDownList"
                                                    AutoPostBack="false" EnableViewState="true">
                                                    <asp:ListItem Value="1" Selected="True">Word Document (.doc)</asp:ListItem>
                                                    <asp:ListItem Value="2">PDF (.pdf)</asp:ListItem>
                                                    <asp:ListItem Value="3">Text File (.txt)</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="TableRow">
                                            <div class="TableFormLeble">
                                                <asp:Label ID="lblDocmentTitle" runat="server" Text="Document Title"></asp:Label>:
                                            </div>
                                            <div class="TableFormContent">
                                                <asp:TextBox ID="txtDocmentTitle" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                                                <span class="RequiredField">*</span>
                                            </div>
                                        </div>
                                        <div class="TableRow">
                                            <div class="TableFormValidatorContent" style="margin-left: 42%">
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Do not include any of these characters in title: \ / : * ? < > |"
                                                    ValidationExpression="^([a-zA-Z0-9_\s\-]*)$" ControlToValidate="txtDocmentTitle"
                                                    ValidationGroup="UploadDocuments" Display="Dynamic"></asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="UploadDocuments"
                                                    ControlToValidate="txtDocmentTitle" Display="Dynamic" ErrorMessage="Please enter document title."></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="TableRow" style="text-align: center; padding-top: 10px">
                                            <asp:Button ID="btnAttachAsDoc" runat="server" Text="Attach As Document" CssClass="CommonButton"
                                                OnClick="btnAttachAsDoc_Click" ValidationGroup="UploadDocuments" />
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>

