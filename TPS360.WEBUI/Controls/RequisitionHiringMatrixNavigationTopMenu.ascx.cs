﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: RequisitionHiringMatrixNavigationTopMenu.ascx.cs
    Description: This is the user control page used for requisition hiring matrix navigation top menu
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date          Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Mar-10-2009        Yogeesh Bhat        Defect ID: 10060; Changes made in RenderCurrentJobPosting()
-------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using System.Collections.Generic;
using System.Collections;
using System.Web.UI.WebControls;
using System.Web;
using System.Web.UI.HtmlControls;
namespace TPS360.Web.UI
{
    public partial class cltRequisitionHiringMatrixNavigationTopMenu : ATSBaseControl
    {
       

        #region Member Variables
        #endregion

        #region Methods
        public int HiringMatrixInterviewLevel
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_TAB]))
                {
                    return Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_TAB]);
                }
                return 0;
            }
            
        }
        public int SelectedLevel
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl["SelectedLevel"]))
                {
                    return Convert.ToInt32(Helper.Url.SecureUrl["SelectedLevel"]);
                }
                return 0;
            }
        }
        public string MemberIds
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID ]))
                {
                    return Convert.ToString(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID ]);
                }
                return string.Empty;
            }
        }
        //public void RenderCurrentJobPosting()
        //{
        //    JobPosting currentJobPosting = base.CurrentJobPosting;
        //    lblTitle.Visible = false;                                                                           //0.1
        //    ControlHelper.SetHyperLink(lnkTitle, UrlConstants.Requisition.JOB_POSTING_INTERNAL_PREVIEW_PAGE, string.Empty, currentJobPosting.JobTitle + "&nbsp;&nbsp;" + currentJobPosting.JobPostingCode, UrlConstants.PARAM_JOB_ID, StringHelper.Convert(currentJobPosting.Id));  //0.1
        //}

        //public void SetTabText()
        //{
        //    int[] hiringMatrixCount = Facade.GetHiringMatrixMemberCount(base.CurrentJobPostingId);
        //    IList <HiringMatrixLevels > hiringMatrixLevels=Facade .GetAllHiringMatrixLevels ();
        //    for (int i = 0; i < hiringMatrixLevels.Count ; i++)
        //    {
        //        try
        //        {
        //            uwtRequisitionHiringMatrixNavigationTopMenu.Tabs[i].Text = hiringMatrixLevels[i].Name + " (" + hiringMatrixCount[i].ToString() + ")";
        //        }
        //        catch
        //        {
        //        }
        //    }
        //    uwtRequisitionHiringMatrixNavigationTopMenu.Tabs[hiringMatrixLevels.Count].Text = "Rejected (" + Facade.GetRejectCandidateCount(base .CurrentJobPostingId ) + ")";
        //}

        //private int TabLevel()
        //{
        //    if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_TAB ]))
        //    {
        //        return Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_TAB]);
        //    }
        //    return 0;
        //}

        #endregion

        #region Events
      
        protected override void OnPreRender(EventArgs e)
        {
            if (!IsPostBack)
            {
              
            }
        } 
        protected void Page_Load(object sender, EventArgs e)
        {
            #region comment
            if (!IsPostBack)
            {
                MiscUtil.EventLogForRequisition(TPS360.Common.Shared.EventLogForRequisition.HiringMatrixOpened, CurrentJobPostingId, CurrentMember.Id, Facade);

                if (Session["PageFrom"] != null)
                {

                    if (SelectedLevel > 0)
                    {
                        HiringMatrixLevels level = Facade.GetHiringMatrixLevelsById(SelectedLevel );
                        if (level != null)
                        {
                            var webConfig = System .Web .Configuration . WebConfigurationManager.OpenWebConfiguration("~");
                            var submit = webConfig.AppSettings.Settings["Submission"];
                            var offer = webConfig.AppSettings.Settings["Offer"];
                            var join = webConfig.AppSettings.Settings["Join"];

                            if (level.Name == offer.Value )
                            {
                                Session["PageFrom"] = null;
                                uclHiringDetails.JobPostingId = CurrentJobPostingId;
                                uclHiringDetails.BulkAction = "BulkAction";
                                uclHiringDetails.MemberID = MemberIds;
                                uclHiringDetails.StatusId = level.Id;
                                mpeHiringDetails.Enabled = true;
                                mpeHiringDetails.Show();
                            }
                            else if (level.Name == join.Value )
                            {
                                Session["PageFrom"] = null;
                                uclJoiningDetails.JobPostingId = CurrentJobPostingId;
                                uclJoiningDetails.BulkAction = "BulkAction";
                                uclJoiningDetails.MemberID = MemberIds;
                                uclJoiningDetails.StatusId = level.Id;
                                mpeJoiningDetails.Enabled = true;
                                mpeJoiningDetails.Show();
                            }
                            
                        }
                    }
                }


            }
            //HireLevel.Value = HiringMatrixInterviewLevel.ToString();
            //if (!IsPostBack)
            //{
            //    int i = 0;
            //    int level = TabLevel();
            //    int selectedtab = 0;
            //    IList<HiringMatrixLevels> hiringMatrixLevels = Facade.GetAllHiringMatrixLevels();
            //    bool isLevelSelected = false  ;
            //    bool isRejectionSelected = false;
            //    if (hiringMatrixLevels != null)
            //    {
            //        foreach (HiringMatrixLevels levels in hiringMatrixLevels)
            //        {
            //            Infragistics.WebUI.UltraWebTab.Tab tab = new Infragistics.WebUI.UltraWebTab.Tab();
            //            if (HiringMatrixInterviewLevel == levels.Id)
            //            {
            //                   tab = uwtRequisitionHiringMatrixNavigationTopMenu.Tabs .GetTab (i);
            //                   isLevelSelected = true ;
            //            }
            //            if (HiringMatrixInterviewLevel == -1)
            //            {

            //                isRejectionSelected = true;
            //            }
            //            tab.Text = levels.Name;
            //            tab.Key = levels.Id.ToString();
            //            if (level == levels.Id) selectedtab = levels.SortingOrder;
            //            if (HiringMatrixInterviewLevel != levels.Id)
            //              uwtRequisitionHiringMatrixNavigationTopMenu.Tabs.Insert(i, tab);

                       

            //            i++;
            //            if (Helper.Url.SecureUrl[UrlConstants.PARAM_TAB].ToString() == levels.Id.ToString())
            //                this.Page.Title = levels.Name + " - " + CurrentJobPosting .JobTitle +" - " + CurrentJobPosting .JobPostingCode ;
            //          hdnPageTitle .Value =this .Page .Title ;
            //        }
            //        if (!isLevelSelected && !isRejectionSelected )
            //        {
            //            Infragistics.WebUI.UltraWebTab.Tab lasttab = uwtRequisitionHiringMatrixNavigationTopMenu.Tabs.GetTab(i);
            //            uwtRequisitionHiringMatrixNavigationTopMenu.Tabs.Remove(lasttab);
            //            uwtRequisitionHiringMatrixNavigationTopMenu.SelectedTab = i+1;
            //            this.Page.Title = "Requisition Status & Notes";
            //        }
            //        else if (!isLevelSelected && isRejectionSelected)
            //        {
            //            Infragistics.WebUI.UltraWebTab.Tab lasttab = uwtRequisitionHiringMatrixNavigationTopMenu.Tabs.GetTab(i);
            //            uwtRequisitionHiringMatrixNavigationTopMenu.Tabs.Remove(lasttab);
            //            uwtRequisitionHiringMatrixNavigationTopMenu.SelectedTab = i;
            //            //uwtRequisitionHiringMatrixNavigationTopMenu.Tabs[i].Text += " (" +Facade .GetRejectCandidateCount ()+ ")";
            //            this.Page.Title = "Requisition Rejected Candidates";
            //            hdnPageTitle.Value = "Requisition Rejected Candidates"; 
            //        }
            //        else
            //        {
            //            Infragistics.WebUI.UltraWebTab.Tab lasttab = uwtRequisitionHiringMatrixNavigationTopMenu.Tabs.GetTab(i);
            //            ASP.controls_requisitionstatusreport_ascx uclStatusReport = (ASP.controls_requisitionstatusreport_ascx)lasttab.ContentPane.FindControl("uclStatusReport");
            //            if (uclRequisitionHiringMatrix != null) lasttab.ContentPane.Controls.Remove(uclStatusReport);

            //        }
               
            //        if (level > 0)
            //        {
            //            uwtRequisitionHiringMatrixNavigationTopMenu.SelectedTab = selectedtab;// Facade.GetHiringMatrixLevelsById(level).SortingOrder;
            //        }
            //        if (selectedtab == 0)
            //            MiscUtil.EventLogForRequisition(TPS360.Common.Shared.EventLogForRequisition.HiringMatrixOpened, CurrentJobPostingId, CurrentMember.Id, Facade);
            //    }
            //    RenderCurrentJobPosting();
            //    SetTabText();
            //    if (Session ["PageFrom"] !=null )
            //    {
            //        if (uwtRequisitionHiringMatrixNavigationTopMenu.Tabs[uwtRequisitionHiringMatrixNavigationTopMenu.SelectedTab].Text.StartsWith("Offered"))
            //        {
            //            Session["PageFrom"] = null;
            //            uclHiringDetails.JobPostingId = CurrentJobPostingId;
            //            uclHiringDetails.BulkAction = "BulkAction";
            //            uclHiringDetails.MemberID = MemberIds;
            //            mpeHiringDetails.Enabled = true;
            //            mpeHiringDetails.Show();
                        
            //        }
            //        if (uwtRequisitionHiringMatrixNavigationTopMenu.Tabs[uwtRequisitionHiringMatrixNavigationTopMenu.SelectedTab].Text.StartsWith("Joined"))
            //        {
            //            Session["PageFrom"] = null;
            //            uclJoiningDetails.JobPostingId = CurrentJobPostingId;
            //            uclJoiningDetails.BulkAction = "BulkAction";
            //            uclJoiningDetails.MemberID = MemberIds;
            //            mpeJoiningDetails.Enabled = true;
            //            mpeJoiningDetails.Show();
            //        }
            //    }
            //} 
            //this.Page.Title = hdnPageTitle.Value;
            #endregion
            
        }
        
        //protected void uwtRequisitionHiringMatrixNavigationTopMenu_TabClick(object sender, Infragistics.WebUI.UltraWebTab.WebTabEvent e)
        //{
        //    Helper.Url.Redirect(UrlConstants.ATS.ATS_PRESELECTED_INTERVIEW_HIRINGMATRIX, string.Empty, UrlConstants.PARAM_JOB_ID, StringHelper.Convert(base.CurrentJobPostingId), UrlConstants.PARAM_TAB, e.Tab.Key == "StatusReport" ? "0" : (e.Tab.Key == "Rejection" ? "-1" : e.Tab.Key));
        //}

        #endregion
       
    }
}