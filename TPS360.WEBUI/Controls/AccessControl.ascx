﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AccessControl.ascx.cs"
    Inherits="TPS360.Web.UI.ctlAccessControl" %>
    <asp:UpdatePanel ID="upAccessControl" runat ="server" >
    <ContentTemplate >
    
<div>
    <div class="TableRow" style="text-align: center;">
        <asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
    </div>
    <div class="TableRow">
        <div class="TableFormLeble">
            <asp:Label EnableViewState="false" ID="lblChangeAccessStatus" runat="server" Text="Change Access Status"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:DropDownList EnableViewState="false" ID="ddlAccessStatus" runat="server" CssClass="CommonDropDownList"
                ValidationGroup="AccessControl" Width="300">
            </asp:DropDownList>
            <span class="RequiredField">*</span>
        </div>
    </div>   
        <div class="TableFormValidatorContent" style="text-align:center;">
            <asp:CompareValidator ID="cvAccessStatus" runat="server" ControlToValidate="ddlAccessStatus"
                Type="Integer" Operator="NotEqual" ValueToCompare="0" ErrorMessage="Please select access status."
                EnableViewState="False" Display="Dynamic" ValidationGroup="AccessControl"></asp:CompareValidator>
        </div>
     
        <br />
        <div class="TableRow" style="text-align: center;">
            <asp:Button ID="btnUpdate" CssClass="CommonButton" runat="server" Text="Update" OnClick="btnUpdate_Click"
                ValidationGroup="AccessControl" />
        </div>
</div>
</ContentTemplate>
    </asp:UpdatePanel>