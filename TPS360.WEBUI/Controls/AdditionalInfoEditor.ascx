﻿
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AdditionalInfoEditor.ascx.cs"  
    Inherits="TPS360.Web.UI.ControlsAdditionalInfoEditor" %>
    <%@ Register Src="~/Controls/CountryStatePicker.ascx" TagName="CountryState" TagPrefix="ucl" %>
<%@ Register Assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.Web.UI.EditorControls" TagPrefix="ig" %>
<%@ Register assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.Web.UI.LayoutControls" tagprefix="ig" %>
    <%@ Register Src ="~/Controls/ExperiencePicker.ascx" TagName ="Experience" TagPrefix ="ucl" %>
    <script type ="text/javascript" language ="javascript"  src="../js/AjaxVariables.js"></script>
    		<script type ="text/javascript" language ="javascript" src="../js/AjaxScript.js" ></script>
<script language="javascript" type="text/javascript">
Sys.Application.add_load(function() {
      $('input[rel=Numeric]').keypress(function (event){return allownumeric(event,$(this));});
      });
      
    function fnValidateSalary(source,args) 
    {
      var CVCurrency= source.id.substring(source.id.lastIndexOf('_')+1,source.id.length);  // changed to generalise the function.
       switch(CVCurrency)
       {
            case "CustomValidatorCYCurrency":
            var txtValidCurrency = $get('<%= txtCurrentYearlySalary.ClientID %>').value.trim();            
            break;

            case "CustomValidatorEYCurrency":
            var txtValidCurrency = $get('<%= txtExpectedYearlySalary.ClientID %>').value.trim();
           
            break;
         
        }
       
       if((txtValidCurrency != "" && txtValidCurrency > 0 && !isNaN(txtValidCurrency)) || (txtValidMaxCurrency != "" && txtValidMaxCurrency > 0 && !isNaN(txtValidMaxCurrency)))   // 0.8
       {
            if(args.Value == 0)
            args.IsValid = false;
            else
            args.IsValid = true; 
       }       
    }
    function fnValidateSSN(source, args)
    {
           var regExpSSN1 =/[0-9]{3}/;   
           var regExpSSN2 =/[0-9]{2}/;   
           var regExpSSN3 =/[0-9]{4}/;   
           
        
        if( $get('<%= txtSSN1.ClientID %>').value.search(regExpSSN1)==-1 || $get('<%= txtSSN2.ClientID %>').value.search(regExpSSN2)==-1 || $get('<%= txtSSN3.ClientID %>').value.search(regExpSSN3)==-1)  // 9645
        
          {            
            $get('<%= lblSSNErrorMsg.ClientID %>').innerHTML="Invalid SSN (Ex: 123-45-6789)"; 
            args.IsValid = false;           
          }
          else
          {
            $get('<%= lblSSNErrorMsg.ClientID %>').innerHTML=""; 
            args.IsValid = true;
          } 
          
           var cvSSN= source.id.substring(source.id.lastIndexOf('_')+1,source.id.length);        
           switch(cvSSN)
           {
                case "cvSSN1":
                if( $get('<%= txtSSN1.ClientID %>').value.length ==3)
                {
                    $get('<%= txtSSN2.ClientID %>').focus();
                }
                break;   
                
                case "cvSSN2" :
                if( $get('<%= txtSSN2.ClientID %>').value.length ==2)
                {
                    $get('<%= txtSSN3.ClientID %>').focus();
                }
                break;                             
           }          
    }
      
    
    // Function to trim the string
    function fnTrim(str)
      { 
         for(var i=0;i<=str.length;i++)    
         {    
            if(str.charAt(i) == " ")
            {       
                str=str.replace(" ","");  
                i=0;       
            }
         }          
       return str;
      }
    
    function fnSSNKepUpSSN1()
     {
        if( $get('<%= txtSSN1.ClientID %>').value.length ==3)
        {
            $get('<%= txtSSN2.ClientID %>').focus();
        }
        
     }
     
     function fnSSNKepUpSSN2()
     {
        if( $get('<%= txtSSN2.ClientID %>').value.length ==2)
        {
            $get('<%= txtSSN3.ClientID %>').focus();
        }
        
     }
     
     function fnValidateYearlySalaryRange(source,args)
     {
         var txtFromSalary = $get('<%= txtExpectedYearlySalary.ClientID %>').value.trim();      
                   
          
         txtFromSalary= fnTrim(txtFromSalary);
         txtMaxSalary=fnTrim(txtMaxSalary);
          
          txtFromSalary=parseFloat(txtFromSalary);
          txtMaxSalary=parseFloat(txtMaxSalary);
          
          if((txtFromSalary =="0" && txtMaxSalary =="0") || (isNaN(txtFromSalary) && isNaN(txtMaxSalary)))
          {
            args.IsValid = true;             
          }  
                  
          else if(  (isNaN(txtFromSalary) && !isNaN(txtMaxSalary)) || isNaN(txtMaxSalary) && !isNaN(txtFromSalary) )
          { 
            args.IsValid = false; 
          }
          
          else if((txtFromSalary =="0" && txtMaxSalary !="0") || (txtMaxSalary == "0" && txtFromSalary !="0"))
          { 
            args.IsValid = false; 
          }
          
          else if (txtMaxSalary < txtFromSalary)
          { 
            args.IsValid = false;
          }
     }
     
     function check()
     {
     var isPost=document .getElementById ('<%=PostBack.ClientID%>');
     isPost .value="PostBack";
     }
     function Ischecked()
     {
     var isCheck=document .getElementById ('<%=Ischeck.ClientID%>');
     isCheck .value="Checked";
     }

</script>
<asp:UpdatePanel ID ="upAdditional" runat ="server" >
<ContentTemplate>

<div >
    <div class="TableRow" style="text-align: center">
        <asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
        <asp:HiddenField ID ="PostBack" runat ="server" Value ="" />
        <asp:HiddenField ID ="Ischeck" runat ="server" Value ="" />
    </div>
    <div class="TableRow" style="width:100%;">
        <div class="FormLeftColumn" style="width: 58%;float:left">
        <div>
            <div class="TableRow">
                <div class="TableFormLeble"style =" width : 30%">
                    <asp:Label EnableViewState="false" ID="lblJobTitle" runat="server" Text="Current Position"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox EnableViewState="true" ID="txtJobTitleA" runat="server" CssClass="CommonTextBox"
                        ValidationGroup="AdditionalInfo" TabIndex ="1"  onmouseover  ="this.title=this.value;" Width ="60%" ></asp:TextBox>
                </div>
            </div>
            <div class="TableRow" runat="server" id="divCurrentCompany"> 
                <div class="TableFormLeble" style =" width : 30%">
                    <asp:Label EnableViewState="false" ID="lblEmployerType" runat="server" Text="Current Company"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox EnableViewState="true" ID="txtCurrentCompany" runat="server" CssClass="CommonTextBox"
                        ValidationGroup="AdditionalInfo" TabIndex ="2" onmouseover  ="this.title=this.value;"  Width ="60%" ></asp:TextBox>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormLeble" style =" width : 30%">
                    <asp:Label EnableViewState="false" ID="lblExperience" runat="server" Text="Total Experience"></asp:Label>:
                </div>
                <div class="TableFormContent">
                
                 <ucl:Experience id="uclExperience" runat="server"></ucl:Experience>
                    <asp:TextBox EnableViewState="true" ID="txtExperience" runat="server" CssClass="CommonTextBox"
                        ValidationGroup="AdditionalInfo" Width="60px" MaxLength="5" TabIndex ="3" onmouseover  ="this.title=this.value;" Visible ="false" ></asp:TextBox>
                    
                </div>
            </div>
            <div class="TableRow"  style =" width :100%" >
                <div class="TableFormValidatorContent" style =" margin-left :30%">    <%--1.0--%>  
                    <asp:RegularExpressionValidator EnableViewState="false" runat="server" ID="revExperience"
                        ControlToValidate="txtExperience" ErrorMessage="Invalid format" ValidationExpression="[0-9]+(\.)?([0-9]+)?"  
                        SetFocusOnError="true" Display="Dynamic"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormLeble" style =" width : 30%">
                    <asp:Label EnableViewState="false" ID="lblCategory" runat="server" Text="Category"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <%--<asp:TextBox EnableViewState="false" ID="txtCategory" runat="server" CssClass="CommonTextBox"
                        ValidationGroup="AdditionalInfo" Width="150px" TabIndex ="4" onmouseover  ="this.title=this.value;"></asp:TextBox>--%>
                        <asp:DropDownList ID="ddlCategory" runat ="server" CssClass ="CommonDropDownList" ></asp:DropDownList>
                </div>
            </div>
            
            
                      <div class="TableRow" id="divCYS" runat="server"> 
                <div class="TableFormLeble" style =" width : 30%">
                    <asp:Label EnableViewState="false" ID="lblCurrentYearlySalary" runat="server" Text="Current CTC"></asp:Label>:
                </div>
                <div class ="TableFormContent">
                <div style="width: 40px; float: left;">
                    <asp:TextBox EnableViewState="false" ID="txtCurrentYearlySalary" Width="40px" runat="server"
                        CssClass="CommonTextBox" TabIndex ="16" rel="Numeric" onmouseover  ="this.title=this.value;"></asp:TextBox>
                </div>
                <div style=" float: left; text-align: left; padding-left: 15px;">
                    <asp:DropDownList EnableViewState="true" ID="ddlCurrentYearlyCurrency" runat="server"
                        CssClass ="CommonDropDownList" TabIndex ="17" Width ="70px">
                    </asp:DropDownList>
                    <asp:DropDownList ID ="ddlCurrentSalaryCycle" runat="server" CssClass ="CommonDropDownList"  TabIndex ="18" Width ="70px">
                   <asp:ListItem Value ="4" Selected ="True" >Yearly</asp:ListItem>
                      <asp:ListItem Value ="3">Monthly</asp:ListItem>
                         <asp:ListItem Value ="2">Daily</asp:ListItem>
                    <asp:ListItem Value ="1">Hourly</asp:ListItem>
                 
                 
                   
                    </asp:DropDownList>
                    <asp:Label ID="lblCurrentSalaryCurrency" runat ="server" ></asp:Label>
                </div>
                </div>
            </div>
            <div class="TableRow"  style =" width :100%" >
                <div class="TableFormValidatorContent" style=" margin-left :30%">
                    <asp:RegularExpressionValidator runat="server" ID="revCurrentYearlySalary" ControlToValidate="txtCurrentYearlySalary"
                        ErrorMessage="Invalid Current Salary <br/>" ValidationExpression="([0-9]+\.[0-9]+|[0-9]+)"
                        EnableViewState="false" SetFocusOnError="true" Display="Dynamic" ValidationGroup="AdditionalInfo"></asp:RegularExpressionValidator>                    
                </div>
                <div class="TableFormValidatorContent" style=" margin-left :30%">
                    <asp:RequiredFieldValidator runat="server" ID="rfvCurrentYearlySalary" ControlToValidate="txtCurrentYearlySalary"
                        EnableViewState="false" ErrorMessage="Invalid Current Salary <br/>" SetFocusOnError="true"
                        Display="Dynamic" ValidationGroup="AdditionalInfo" 
                        InitialValue="<%#String.Empty%>" Visible="False"></asp:RequiredFieldValidator>                    
                </div>
                <div class="TableFormValidatorContent" style=" margin-left :30%">
                    <asp:CustomValidator ID="CustomValidatorCYCurrency" runat="server" controltovalidate="ddlCurrentYearlyCurrency" EnableViewState="false" SetFocusOnError="true" Display="Dynamic"
                    errormessage="Please select currency type. <br/>" ClientValidationFunction="fnValidateSalary" ValidationGroup="AdditionalInfo"/>
                </div>
        
            </div> 

            <div class="TableRow" runat="server" id="divCCN" visible="false"> 
                <div class="TableFormLeble" style =" width : 30%">
                    <asp:Label EnableViewState="false" ID="lblCurrentSalaryNote" runat="server" Text="Current CTC Note"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox EnableViewState="true" ID="txtCurrentCTCNote" runat="server" CssClass="CommonTextBox"
                        ValidationGroup="AdditionalInfo" TabIndex ="21" onmouseover  ="this.title=this.value;"  Width ="60%" ></asp:TextBox>
                </div>
            </div>

            <div class="TableRow" id="divEYS" runat="server" > 
                <div class="TableFormLeble"  style =" width : 30%">
                    <asp:Label EnableViewState="false" ID="lblExpectedYearlySalary" runat="server" Text="Expected CTC"></asp:Label>:
                </div>
                 <div class ="TableFormContent">
                <div style="width: 40px; float: left;">
                    <asp:TextBox EnableViewState="false" ID="txtExpectedYearlySalary" rel="Numeric" onmouseover  ="this.title=this.value;" Width="40px" runat="server" CssClass="CommonTextBox" TabIndex ="19"></asp:TextBox>                    
                                  
                </div>                
                <%--<div style="float: left;padding-left: 10px;">To
                    <asp:TextBox EnableViewState="false" ID="txtExpectedMaxYearlySalary" onmouseover  ="this.title=this.value;" Width="40px" runat="server" CssClass="CommonTextBox" TabIndex ="20"></asp:TextBox>                 --%>
                   <div style=" float: left; text-align: left; padding-left: 15px;">
                   <asp:DropDownList EnableViewState="true" ID="ddlExpectedYearlyCurrency" runat="server" CssClass="CommonDropDownList" TabIndex ="22" Width ="70px">
                    </asp:DropDownList>  
                    
                     <asp:DropDownList ID ="ddlExpectedSalaryCycle" runat="server" CssClass ="CommonDropDownList"  TabIndex ="22" Width ="70px" >
                    <asp:ListItem Value ="4" Selected ="True" >Yearly</asp:ListItem>
                     <asp:ListItem Value ="3">Monthly</asp:ListItem>
                        <asp:ListItem Value ="2">Daily</asp:ListItem>
                    <asp:ListItem Value ="1">Hourly</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Label ID="lblExpectedSalaryCurrency" runat ="server" ></asp:Label>
                </div>
               </div> 
            </div>
            <div class="TableRow"  style =" width :100%" >
                <div class="TableFormValidatorContent" style=" margin-left :30%">
                    <asp:RegularExpressionValidator runat="server" ID="revExpectedYearlySalary" ControlToValidate="txtExpectedYearlySalary"
                        ErrorMessage="Invalid Expected Salary <br/>" ValidationExpression="^\d{0,100}(\.\d{1,100})?$"
                        EnableViewState="false" SetFocusOnError="true" Display="Dynamic" ValidationGroup="AdditionalInfo">
                    </asp:RegularExpressionValidator>   
                    <%-- <asp:RegularExpressionValidator runat="server" ID="revExpectedYearlyMaxSalary" 
                        ErrorMessage="Invalid Max Salary <br/>" ValidationExpression="^\d{0,100}(\.\d{1,100})?$"
                        EnableViewState="false" SetFocusOnError="true" Display="Dynamic" ValidationGroup="AdditionalInfo">
                    </asp:RegularExpressionValidator> --%>
                                         
                </div>
                <div class="TableFormValidatorContent" style=" margin-left :30%">
                    <asp:RequiredFieldValidator runat="server" ID="rfvExpectedYearlySalary" ControlToValidate="txtExpectedYearlySalary"
                        EnableViewState="false" ErrorMessage="Invalid Expected Salary <br/>" SetFocusOnError="true"
                        Display="Dynamic" ValidationGroup="AdditionalInfo" InitialValue="0" Visible="False">
                    </asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator runat="server" ID="rfvExpectedYearlyMaxSalary" 
                        EnableViewState="false" ErrorMessage="Invalid Max Salary <br/>" SetFocusOnError="true"
                        Display="Dynamic" ValidationGroup="AdditionalInfo" InitialValue="0" Visible="False">
                    </asp:RequiredFieldValidator>
                </div>
                <div class="TableFormValidatorContent" style=" margin-left :30%">
                    <asp:CustomValidator ID="CustomValidatorEYCurrency" runat="server" controltovalidate="ddlExpectedYearlyCurrency" EnableViewState="false" SetFocusOnError="true" Display="Dynamic"
                    errormessage="Please select currency type." ClientValidationFunction="fnValidateSalary" ValidationGroup="AdditionalInfo"/>
                </div>
                
                <div class="TableFormValidatorContent" style=" margin-left :30%">
                    <asp:CustomValidator ID="CVMaxYearlySalary" runat="server"  EnableViewState="false" Display="Dynamic"
                    errormessage="Invalid salary range" ClientValidationFunction="fnValidateYearlySalaryRange" ValidationGroup="AdditionalInfo" ValidateEmptyText="true"/>
                </div>
          
            </div>

            <div class="TableRow" runat="server" id="divECN"> 
                <div class="TableFormLeble" style =" width : 30%">
                    <asp:Label EnableViewState="false" ID="lblExpectedCTCNote" runat="server" Text="Salary Notes"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox EnableViewState="true" ID="txtExpectedCTCNote" runat="server" CssClass="CommonTextBox"
                        ValidationGroup="AdditionalInfo" TabIndex ="23" onmouseover  ="this.title=this.value;"  Width ="60%" ></asp:TextBox>
                </div>
            </div>

            <div class="TableRow" id ="dvEmploymentType" runat ="server" >
                <div class="TableFormLeble"  style =" width : 30%">
                    <asp:Label EnableViewState="false" ID="lblJobType" runat="server" Text="Employment Type"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:DropDownList EnableViewState="true" ID="ddlJobType" runat="server" CssClass="CommonDropDownList" TabIndex ="24">
                    </asp:DropDownList>                                        
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormLeble"  style =" width : 30%"> 
                    <asp:Label EnableViewState="false" ID="lblWorkSchedule" runat="server" Text="Work Schedule"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:DropDownList EnableViewState="true" ID="ddlWorkSchedule" runat="server" CssClass="CommonDropDownList" TabIndex ="25">
                    </asp:DropDownList>                                        
                </div>
            </div>
            
               <div class="TableRow">
                <div class="TableFormLeble"  style =" width : 30%"> 
                    <asp:Label EnableViewState="false" ID="lblWebsite" runat="server" Text="Website"></asp:Label>:
                </div>
                <div class="TableFormContent">
                  <asp:TextBox ID ="txtwebsite" runat ="server" CssClass ="CommonTextBox"  Width ="60%" ></asp:TextBox>                           
                </div>
            </div>
            
            
               <div class="TableRow">
                <div class="TableFormLeble"  style =" width : 30%"> 
                    <asp:Label EnableViewState="false" ID="lblLinkedInProfile" runat="server" Text="LinkedIn Profile"  ></asp:Label>:
                </div>
                <div class="TableFormContent">
                  <asp:TextBox ID ="txtLinkedInProfile" runat ="server" CssClass ="CommonTextBox" Width ="60%"></asp:TextBox>                           
                </div>
            </div>
            
            
            <div class="TableRow">
                <div class="TableFormLeble" style =" width : 30%"> 
                    <asp:Label EnableViewState="false" ID="lblIndustry" runat="server" Text="Industry"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:DropDownList EnableViewState="true" ID="ddlIndustry" runat="server" CssClass="CommonDropDownList"
                        ValidationGroup="AdditionalInfo" TabIndex ="11">
                    </asp:DropDownList>
                </div>
            </div>
            
            <div class="TableRow">
                <div class="TableFormLeble" style =" width : 30%"> 
                    <asp:Label EnableViewState="false" ID="lblJobFunction" runat="server" Text="Job Function"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:DropDownList EnableViewState="true" ID="ddlJobFunction" runat="server" CssClass="CommonDropDownList"
                        ValidationGroup="AdditionalInfo" TabIndex ="12">
                    </asp:DropDownList>
                </div>
            </div>
            
            
          
        </div>
        </div>
        <div class="FormRightColumn" style="width: 42%; vertical-align: top;float:left ;  ">
  
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label EnableViewState="false" ID="lblDateOfBirth" runat="server" Text="Date of Birth"></asp:Label>:
                </div>
                <div class="TableFormContent">
                 <%--   <igsch:WebDateChooser ID="wdcDateOfBirth" runat="server" Editable="True" NullDateLabel="" CalendarLayout-MinDate="1/1/1900 12:00:00 AM" EnableKeyboardNavigation ="true"  TabIndex ="6" >
                    <CalendarLayout HideOtherMonthDays="True">
                        </CalendarLayout>
                    </igsch:WebDateChooser>--%>
                    <ig:WebDatePicker ID="wdcDateOfBirth"  DropDownCalendarID="ddcDateOfBirth" runat="server"></ig:WebDatePicker>
                    <ig:WebMonthCalendar  ID="ddcDateOfBirth" AllowNull="true"  runat="server"></ig:WebMonthCalendar>
                     </div>
            </div>
            <div class ="TableRow">
                 <div class="TableFormValidatorContent" style=" margin-left :42%">
                    <asp:RangeValidator EnableViewState="false" ID="RangeValidatorDOB" runat="server" ControlToValidate="wdcDateOfBirth" 
                                        ErrorMessage="Please select 'Date of Birth' less than current date." Type="Date"
                                        SetFocusOnError="true" Display="Dynamic"></asp:RangeValidator>
                 </div> 
            </div>
            <div id="divRemovingFields" runat="server"> 
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label EnableViewState="false" ID="lblCityOfBirth" runat="server" Text="City of Birth"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox EnableViewState="false" ID="txtCityOfBirth" runat="server" CssClass="CommonTextBox"
                        ValidationGroup="AdditionalInfo" Width="90px" TabIndex ="7" onmouseover  ="this.title=this.value;"></asp:TextBox>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label EnableViewState="false" ID="lblCountryOfBirth" runat="server" Text="Citizenship"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:DropDownList  ID="ddlCountryOfBirth" runat="server" CssClass="CommonDropDownList" EnableViewState ="true" 
                        ValidationGroup="AdditionalInfo" TabIndex ="8">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label EnableViewState="false" ID="lblCountryOfResidence" runat="server" Text="Country of Residence"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:DropDownList EnableViewState="true" ID="ddlCountryOfResidence" runat="server"
                        CssClass="CommonDropDownList" ValidationGroup="AdditionalInfo" TabIndex ="9">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label EnableViewState="false" ID="lblGender" runat="server" Text="Gender"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:DropDownList EnableViewState="true" ID="ddlGender" runat="server" CssClass="CommonDropDownList"
                        ValidationGroup="AdditionalInfo" TabIndex ="10">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label EnableViewState="false" ID="lblMaritalStatus" runat="server" Text="Marital Status"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:DropDownList EnableViewState="true" ID="ddlMaritalStatus" runat="server" CssClass="CommonDropDownList"
                        ValidationGroup="AdditionalInfo" TabIndex ="11">
                    </asp:DropDownList>
                </div>
            </div>
            </div>
            <div class="TableRow" runat="server" id="divSSN">
                <div class="TableFormLeble">
                    <asp:Label EnableViewState="false" ID="lblSSN" runat="server" Text="SSN"></asp:Label>:
                </div>
          
                <div class="TableFormContent">
                     <asp:TextBox EnableViewState="false" ID="txtSSN1"  Width="20px" MaxLength="3" TextMode="Password" runat="server" CssClass="CommonTextBox" TabIndex ="12">
                     </asp:TextBox>
                       <b>-</b>
                        
                    <asp:TextBox EnableViewState="false" ID="txtSSN2" Width="15px" MaxLength="2" TextMode="Password" runat="server" CssClass="CommonTextBox" TabIndex ="13">
                    </asp:TextBox>
                     <b>-</b>
                    <asp:TextBox EnableViewState="false" ID="txtSSN3" Width="30px"  onmouseover  ="this.title=this.value;" MaxLength="4" runat="server" CssClass="CommonTextBox" TabIndex ="14">
                    </asp:TextBox>
                     </div>                
 
     
          </div> 
            <div class ="TableRow"  style =" width :100%" >
              <div class="TableFormValidatorContent" style=" margin-left :42%">
                <asp:CustomValidator ID="cvSSN1" runat="server" controltovalidate="txtSSN1" EnableViewState="false" SetFocusOnError="true" Display="Dynamic"
                     ClientValidationFunction="fnValidateSSN"  ValidationGroup="AdditionalInfo"/>
                     <asp:CustomValidator ID="cvSSN2" runat="server" controltovalidate="txtSSN2" EnableViewState="false" SetFocusOnError="true" Display="Dynamic"
                       ClientValidationFunction="fnValidateSSN" ValidationGroup="AdditionalInfo"/>
                       
               <asp:CustomValidator ID="cvSSN3" runat="server" controltovalidate="txtSSN3" EnableViewState="false" SetFocusOnError="true" Display="Dynamic"
                       ClientValidationFunction="fnValidateSSN" ValidationGroup="AdditionalInfo"/> 
              <asp:Label ID="lblSSNErrorMsg" runat="server" Text="" ForeColor="Red"></asp:Label>
              </div> 
          </div>
            <div class="TableRow" id ="dvWorkPermitstatus" runat ="server" >
                <div class="TableFormLeble">
                    <asp:Label EnableViewState="false" ID="lblWorkPermitStatus" runat="server" Text="Work Permit Status"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:DropDownList EnableViewState="true" ID="ddlWorkPermitStatus" runat="server"
                        CssClass="CommonDropDownList" ValidationGroup="AdditionalInfo" TabIndex ="15">
                    </asp:DropDownList>
                </div>
            </div>            
            <div class="TableRow">
                <div class="TableFormLeble"  >
                    <asp:Label EnableViewState="false" ID="lblPassportStatus" runat="server" Text="Passport Status"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:RadioButton ID="rdoPassportReady" runat="server" Text="Ready" GroupName="Passport"
                        ValidationGroup="AdditionalInfo" Checked="true" TabIndex ="24" style=" display:inline " />
                    <asp:RadioButton ID="rdoPassportNone" runat="server" Text="None" GroupName="Passport"
                        Checked="true" ValidationGroup="AdditionalInfo" TabIndex ="25" style=" display:inline "/>
                </div>
            </div>
              <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label EnableViewState="false" ID="lblWillingToTravel" runat="server" Text="Willing to Travel"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:RadioButton ID="rdoTravelYes" style=" display:inline " GroupName="W2T" runat="server" Text="Yes" TabIndex ="5" />
                    <asp:RadioButton ID="rdoTravelNo" style=" display:inline " GroupName="W2T" runat="server" Checked="true" Text="No" TabIndex ="5"/>
                </div>
            </div>
            <div class="TableRow" id ="divPan" runat ="server" visible="false" >
                <div class="TableFormLeble" >
                    <asp:Label EnableViewState="false" ID="lblPan" runat="server" Text="ID Proof"></asp:Label>:
                </div>
                <div class="TableFormContent">
                <asp:DropDownList ID="ddlIDProof" runat ="server" CssClass ="CommonDropDownList" ></asp:DropDownList>
                <div style =" margin-left : 43%; padding-top : 3px;">
                    <asp:TextBox EnableViewState="false" ID="txtIdProof" runat="server" CssClass="CommonTextBox"
                        ValidationGroup="AdditionalInfo" TabIndex ="29" onmouseover  ="this.title=this.value;"></asp:TextBox>
                        </div>
                </div>
            </div>
            <div class="TableRow" runat="server" id="divNoticePeriod"> 
                <div class="TableFormLeble">
                    <asp:Label EnableViewState="false" ID="lblNoticePeriod" runat="server" Text="Notice Period"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox EnableViewState="true" ID="txtNoticePeriod" runat="server" CssClass="CommonTextBox"
                        ValidationGroup="AdditionalInfo" TabIndex ="30" onmouseover  ="this.title=this.value;"  Width ="40%" ></asp:TextBox>
                </div>
            </div>
            <div class="TableRow" id ="dvSecurityClearence" runat ="server" >
                <div class="TableFormLeble" >
                    <asp:Label EnableViewState="false" ID="lblSecurityClearance" runat="server" Text="Security Clearance"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:RadioButton ID="rdoSecurityClearanceYes" GroupName="SClearence" runat="server"
                        Text="Yes" TabIndex ="26" style=" display:inline "/>&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:RadioButton ID="rdoSecurityClearanceNo" GroupName="SClearence" runat="server"
                        Checked="true" Text="No" TabIndex ="27" style=" display:inline "/>
                </div>
            </div>
        </div>
    </div>
    <div class="TableRow" id="divPA" runat="server"> 
        <br />
        <legend class="TabPanelHeader">
            Permanent Address
        </legend>
        <div class="FormLeftColumn" style="width: 47%;">
            <div class="TableRow">
                <div class="TableFormLeble">
                </div>
                <div class="TableFormContent">
                    <asp:CheckBox ID="chkIsPermanentAddress" CssClass ="ColumnSelection" runat="server" AutoPostBack="true" Text="Same as Current Address?"
                        OnCheckedChanged="chkIsPermanentAddress_Click" TabIndex ="28" />
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label EnableViewState="false" ID="lblLocation" runat="server" Text="Permanent Address1"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox EnableViewState="false" ID="txtLocation" runat="server" CssClass="CommonTextBox"
                        ValidationGroup="AdditionalInfo" TabIndex ="29" onmouseover  ="this.title=this.value;"></asp:TextBox>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label EnableViewState="false" ID="lblPermanentAddress2" runat="server" Text="Permanent Address2"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox EnableViewState="false" ID="txtPermanentAddress2" runat="server" CssClass="CommonTextBox"
                        ValidationGroup="AdditionalInfo" TabIndex ="30" onmouseover  ="this.title=this.value;"></asp:TextBox>
                </div>
            </div>
             <ucl:CountryState ID="uclCountryState" runat="server" FirstOption="Please Select" ApplyDefaultSiteSetting="True"  /> 
                     
        </div>
        <div class="FormLeftColumn" style="width: 52%; vertical-align: top;">
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label EnableViewState="false" ID="lblPermanentCity" runat="server" Text="City"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox EnableViewState="false" ID="txtPermanentCity" runat="server" CssClass="CommonTextBox"
                        ValidationGroup="AdditionalInfo" TabIndex ="33" onmouseover  ="this.title=this.value;"></asp:TextBox>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label EnableViewState="false" ID="lblPermanentZipCode" runat="server" Text="Zip Code"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox EnableViewState="false" ID="txtPermanentZipCode" runat="server" CssClass="CommonTextBox"
                        ValidationGroup="AdditionalInfo" MaxLength="10"  onmouseover  ="this.title=this.value;" TabIndex ="34"></asp:TextBox>
                </div>
            </div>              
            <div class="TableRow"  style =" width :100%" > 
            <div class="TableFormValidatorContent" style=" margin-left :42%">
                <asp:RegularExpressionValidator ID="revZIPCode" runat="server" ControlToValidate="txtPermanentZipCode"
                    Display="Dynamic" ErrorMessage="Please enter a valid zip code" ValidationExpression="^[0-9-]*$"></asp:RegularExpressionValidator><%--(\d{6}(-\d{4})?)|(\d{5}(-\d{4})?)--%>
            </div>
           </div>
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label EnableViewState="false" ID="lblPermamentPhone" runat="server" Text="Permanent Phone"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox EnableViewState="false" ID="txtPermanentPhone" runat="server" CssClass="CommonTextBox"
                        ValidationGroup="AdditionalInfo" Width="120px" TabIndex ="35" onmouseover  ="this.title=this.value;" MaxLength ="15" rel="Integer"></asp:TextBox>
                    <asp:TextBox EnableViewState="false" ID="txtPermamentPhoneExt" runat="server" CssClass="CommonTextBox"
                        ValidationGroup="AdditionalInfo" Width="20px" TabIndex ="36" onmouseover  ="this.title=this.value;"  MaxLength ="5" rel="Integer"></asp:TextBox>
                </div>
            </div>
            <div class="TableRow"  style =" width :100%" >
                <div class="TableFormValidatorContent" style=" margin-left :42%">
                    <asp:RegularExpressionValidator ID="revParmanentPhone" runat="server" Display="Dynamic"
                        ControlToValidate="txtPermanentPhone" EnableViewState="false" ErrorMessage="Please enter valid phone number."
                        ValidationExpression="^([\+][0-9]{1,10}([ \-])?)?([\(]{1}[0-9]{1,5}[\)])?([0-9 \-]{1,32})((x|X)?[0-9]{2,4}?)$"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="TableRow"  style =" width :100%" >
                <div class="TableFormValidatorContent" style=" margin-left :42%">
                    <asp:RegularExpressionValidator ID="revParmanentPhoneEx" runat="server" Display="Dynamic"
                        ControlToValidate="txtPermamentPhoneExt" EnableViewState="false" ErrorMessage="Please enter extension number in 2-4 digits."
                        ValidationExpression="^[0-9]{2,4}$"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label EnableViewState="false" ID="lblPermanentMobile" runat="server" Text="Permanent Mobile"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox EnableViewState="false" ID="txtPermanentMobile" runat="server" CssClass="CommonTextBox"
                        ValidationGroup="AdditionalInfo" TabIndex ="37" onmouseover  ="this.title=this.value;"  MaxLength ="15" rel="Integer"></asp:TextBox>
                </div>
            </div>
            <div class="TableRow"  style =" width :100%" >
                <div class="TableFormValidatorContent" style=" margin-left :42%">
                    <asp:RegularExpressionValidator ID="revPermanentMobile" runat="server" Display="Dynamic"
                        ControlToValidate="txtPermanentMobile" EnableViewState="false" ErrorMessage="Please enter valid mobile number."
                        ValidationExpression="^([\+][0-9]{1,10}([ \-])?)?([\(]{1}[0-9]{1,5}[\)])?([0-9 \-]{1,32})((x|X)?[0-9]{2,4}?)$"></asp:RegularExpressionValidator>
                </div>
            </div>
        </div>
    </div>
    <div class="TableRow" style="text-align: center;">
        <asp:Button ID="btnSave" CssClass="CommonButton" runat="server" Text="Save" OnClick="btnSaveMember_Click"
            ValidationGroup="AdditionalInfo" TabIndex ="38" OnClientClick ="check()"/>            
    </div>
    <br />
    <input type="hidden" runat="server" id="hdnSSN1" value="" /> 
    <input type="hidden" runat="server" id="hdnSSN2" value="" />
</div>


</ContentTemplate>
</asp:UpdatePanel>

