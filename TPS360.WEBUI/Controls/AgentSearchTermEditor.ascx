﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AgentSearchTermEditor.ascx.cs" Inherits="TPS360.Web.UI.AgentSearchTermEditor"   %>

    <%@ Register Src ="~/Controls/ConfirmationWindow.ascx" TagName ="Confirms" TagPrefix ="uc1" %>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
    <script type ="text/javascript" language ="javascript"  src="../js/AjaxVariables.js"></script>
		<script type ="text/javascript" language ="javascript" src="../js/AjaxScript.js" ></script>
     <style type="text/css">  
        .CalendarCSS  
        {  
            background-color:Gray  ;  
            color:Blue ;  
            border-color : Maroon ;
            }  
            .custom-calendar .ajax__calendar_container

{

 background-color:#ffc; /* pale yellow */

 border:solid 1px #666;

}

.custom-calendar .ajax__calendar_title

{

 background-color:#cf9; /* pale green */

 height:20px;

 color:#333;

}

.custom-calendar .ajax__calendar_prev,

.custom-calendar .ajax__calendar_next

{

 background-color:#aaa; /* darker gray */

 height:20px;

 width:20px;

}

.custom-calendar .ajax__calendar_today

{

 background-color:#cff;  /* pale blue */

 height:20px;

}

.custom-calendar .ajax__calendar_days table thead tr td

{

 background-color:#ff9; /* dark yellow */

 color:#333;

}

.custom-calendar .ajax__calendar_day

{

 color:#333; /* normal day - darker gray color */

}

.custom-calendar .ajax__calendar_other .ajax__calendar_day

{

 display :none ;
 color:#666; /* day not actually in this month - lighter gray color */

}
.calendarDropdown
{
	margin-left : -18px;
 margin-bottom :-3px;
}
    </style>  
    <script language ="javascript" type ="text/javascript" >

 function HotListSelected(source, eventArgs)
        {
           hdnSelectedHotList=document .getElementById ('<%=hdnSelectedHotList.ClientID %>');
            hdnSelectedHotListText=document .getElementById ('<%=hdnSelectedHotListText.ClientID %>');
            hdnSelectedHotListText.value=eventArgs.get_text();
            hdnSelectedHotList.value=eventArgs.get_value();  
        }
         function fnValidateSalary(source,args) 
        {
          // debugger;        
           
           var txtSalaryRangeFromValue = $get('<%= txtSalaryRangeFrom.ClientID %>').value.trim();       
           var ddlPaymentTypeValue= $get('<%= ddlPaymentType.ClientID %>').value;
                  
           if(txtSalaryRangeFromValue != "" && !isNaN(txtSalaryRangeFromValue) && ddlPaymentTypeValue <1 && txtSalaryRangeFromValue > 0 )
           {               
                if(args.Value == 0)
                args.IsValid = false;
                else
                args.IsValid = true;           
           } 
        }
        function fnValidateSalaryTo(source,args) 
        {
          // debugger;            
           
           var txtSalaryRangeToValue = $get('<%= txtSalaryRangeTo.ClientID %>').value.trim();      
           var ddlPaymentTypeValue= $get('<%= ddlPaymentType.ClientID %>').value;
                      
           if(txtSalaryRangeToValue != "" && !isNaN(txtSalaryRangeToValue) && ddlPaymentTypeValue <1 && txtSalaryRangeToValue > 0 )
           {               
                if(args.Value == 0)
                args.IsValid = false;
                else
                args.IsValid = true;           
           }  
        }  
        function fnValidateCurrency(source,args) 
        {
          // debugger;            
            
           var txtSalaryRangeFromValue = $get('<%= txtSalaryRangeFrom.ClientID %>').value.trim();       
           var ddlCurrency= $get('<%= ddlCurrency.ClientID %>').value;
                      
           if(txtSalaryRangeFromValue != "" && !isNaN(txtSalaryRangeFromValue) && ddlCurrency <1 && txtSalaryRangeFromValue > 0 )
           {              
                if(args.Value == 0)
                args.IsValid = false;
                else
                args.IsValid = true;           
           }  
            

                                                                 
        }
        
        function fnValidateCurrencyTo(source,args) 
        {
          // debugger;            
          
           var txtSalaryRangeToValue = $get('<%= txtSalaryRangeTo.ClientID %>').value.trim();      
           var ddlCurrency= $get('<%= ddlCurrency.ClientID %>').value;
                      
           if(txtSalaryRangeToValue != "" && !isNaN(txtSalaryRangeToValue) && ddlCurrency <1 && txtSalaryRangeToValue > 0 )
           {
                if(args.Value == 0)
                args.IsValid = false;
                else
                args.IsValid = true;                         
           } 
        }  
    </script>
    <asp:UpdatePanel id="updatepanelbuttonss" runat="server"  >
    <ContentTemplate>
<div id ="div" runat ="server" visible ="false" >
<uc1:Confirms ID="uclConfirms" runat="server" ></uc1:Confirms></div>
</ContentTemplate>
</asp:UpdatePanel>


<div class="GridContainer" style="width: 100%;" >
<asp:HiddenField ID="hdnSelectedHotList" runat ="server" />
      <asp:HiddenField ID="hdnSelectedHotListText" runat ="server" />
      <asp:HiddenField ID ="hdnJobPostingSearchAgentId" runat ="server" />
      <asp:HiddenField ID ="hdnPostback" runat ="server" />
      <div style ="text-align :left ;">
     
<div style ="text-indent:25px;" >
        The search fields below will be used to find candidates to email the requisition details. To clear all fields and reload search values from the requisition, click "Reset Search Terms".</div>
      </div>
      <div class="TableRow">
            <div class="TableFormLeble" style =" text-align :left ; margin-left :13.5%; width :7.5%;">
                <asp:Label ID="lblKeyword" runat="server" Text="Keywords"></asp:Label>:
            </div>
            <div class="TableFormContent" style ="vertical-align :top ; text-align :left ;">
                <asp:TextBox ID="txtKeyword" runat="server" CssClass="SearchTextbox" Width="340px" EnableViewState="true"> </asp:TextBox>
            </div>
        </div>
                        <div style ="  width :50%; float :left ;" >
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label ID="lblJobTitle" runat="server" Text="Current Job Title"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:TextBox ID="txtJobTitle" Width="180px" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label ID="lblWorkCity" runat="server" Text="City"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:TextBox ID="txtWorkCity" Width="180px" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                                </div>
                            </div>
                          
                            
                              <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label ID="lblState" runat="server" Text="State"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                   <asp:HiddenField ID="hdnSelectedState" runat ="server" Value ="" />
                                    <asp:DropDownList ID="ddlState" runat="server" CssClass="CommonDropDownList" Width="130px" EnableViewState ="true"  >
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label ID="lblCountry" runat="server" Text="Country"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:DropDownList ID="ddlCountry" runat="server"  CssClass="CommonDropDownList" Width="130px" >
                                    </asp:DropDownList>
                                </div>
                            </div>
                            
                            
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label ID="lblYearsofExp" runat="server" Text="Years of Experience"></asp:Label>:
                                </div>
                                <div style ="vertical-align :text-top ;float :left ;">
                                    <asp:Label ID="Label3" runat="server" Text="MIN"></asp:Label><span >:</span>
            <asp:TextBox ID="txtMinExp" Width="20px" runat="server" CssClass="CommonTextBox" 

MaxLength="2"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Label ID="Label4" runat="server" Text="MAX"></asp:Label><span >:</span>
             <asp:TextBox ID="txtMaxExp" Width="20px" runat="server" CssClass="CommonTextBox" MaxLength="2" ></asp:TextBox>
                                </div>
                            </div>
                         
                            <div class="TableRow">
                                <div class="TableFormValidatorContent" style=" margin-left :42%">
                                    <asp:RegularExpressionValidator ID="revMinExp" runat="server" ErrorMessage="MIN field should be integer." ControlToValidate="txtMinExp" Display="Dynamic" SetFocusOnError="True" ValidationExpression="^\d*[0-9]$" ValidationGroup="valSearchAgents">
                                    </asp:RegularExpressionValidator>
                                </div>
                                <div class="TableFormValidatorContent" style=" margin-left :42%">
                                    <asp:RegularExpressionValidator ID="revMaxExp" runat="server" ErrorMessage="MAX field should be integer." ControlToValidate="txtMaxExp" Display="Dynamic" SetFocusOnError="True" ValidationExpression="^\d*[0-9]$" ValidationGroup="valSearchAgents">
                                    </asp:RegularExpressionValidator>
                                </div>
                                <div class="TableFormValidatorContent" style=" margin-left :42%">
                                    <asp:CompareValidator ID="cmpValidExp" ControlToValidate="txtMaxExp" ControlToCompare="txtMinExp" runat="server" 

SetFocusOnError="true" Display="Dynamic" EnableViewState="false" 
                                        ErrorMessage="Max exp. should be greater than min exp." ValidationGroup="valSearchAgents" 

Operator="GreaterThan" type="Integer" />
                                </div> 
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble" ">
                                    <asp:Label ID="lblSalaryRange" runat="server" Text="Salary Range"></asp:Label>:
                              
                                </div>
                                <div style ="vertical-align :text-top ;  width ="400px;" align="left"  >
                                    <asp:Label ID="lblSalaryRangeFrom" runat="server" Text="From"></asp:Label><span >:</span>
                                    <asp:TextBox ID="txtSalaryRangeFrom" Width="50px" runat="server" CssClass="CommonTextBox" ValidationGroup ="valSearchAgents"></asp:TextBox>
                                    <asp:Label ID="lblSalaryRangeTo" runat="server" Text="To"></asp:Label><span >:</span>
                                    <asp:TextBox ID="txtSalaryRangeTo" Width="50px" runat="server" CssClass="CommonTextBox" ValidationGroup ="valSearchAgents"></asp:TextBox>
                                    <div class ="TableRow">
                                   <div class="TableFormLeble" ">
                                    <asp:Label ID ="lbl" runat ="server" ></asp:Label>
                                </div>
                                   <div style =" float :left ;">
                                      <asp:DropDownList ID="ddlCurrency" runat="server" CssClass="CommonDropDownList" Width="102px">
                                        </asp:DropDownList>
                                        <asp:DropDownList ID="ddlPaymentType" runat="server" CssClass="CommonDropDownList" Width ="60px" >
                                        </asp:DropDownList> 
                                        <asp:Label id="lblSalaryRate" runat ="server" ></asp:Label>
                               </div>
                               </div>
                                </div>
                                <div class="TableRow">
                                        <asp:Label ID="lblRangeError" runat="server" EnableViewState="false" ForeColor="Red" Font-Names="Verdana" 

Visible="false"></asp:Label>
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormValidatorContent" style=" margin-left :42%">
                                    <asp:RegularExpressionValidator runat="server" ID="revSalaryRangeFrom" ControlToValidate="txtSalaryRangeFrom"
                                        ErrorMessage="Please enter valid Min Salary" ValidationExpression="^\d*[0-9]\d*(\.\d+)?$"
                                        EnableViewState="false" SetFocusOnError="true" Display="Dynamic" ValidationGroup="valSearchAgents">
                                    </asp:RegularExpressionValidator>                                                                
                                </div>                                        
                                 <div class="TableFormValidatorContent" style=" margin-left :42%">
                                    <asp:RegularExpressionValidator runat="server" ID="revSalaryRangeTo" ControlToValidate="txtSalaryRangeTo"
                                        ErrorMessage="Please enter valid Max Salary" ValidationExpression="^\d*[0-9]\d*(\.\d+)?$"
                                        EnableViewState="false" SetFocusOnError="true" Display="Dynamic" ValidationGroup="valSearchAgents">
                                    </asp:RegularExpressionValidator>                                                                
                                </div>
                                
                                <div class="TableFormValidatorContent" style=" margin-left :42%">
                                    <asp:CompareValidator ID="cmpValYearlySalary" ControlToValidate="txtSalaryRangeTo" 

ControlToCompare="txtSalaryRangeFrom" runat="server" SetFocusOnError="true" Display="Dynamic" EnableViewState="false" 
                                        ErrorMessage="Max salary should be greater than min salary" ValidationGroup="valSearchAgents" 

Operator="GreaterThanEqual" type="Double" />
                                </div>                                        
                            </div>    
                                                          
                            <div class="TableRow">                                        
                                
                                    <div class="TableFormValidatorContent" style=" margin-left :42%">
                                        <asp:CustomValidator ID="cvCurrency" runat="server" controltovalidate="ddlCurrency" EnableViewState="false" SetFocusOnError="true" Display="Dynamic" Errormessage="Please select Currency type." ClientValidationFunction="fnValidateCurrency" ValidationGroup="valSearchAgents"/>
                                    </div>
                                    <div class="TableFormValidatorContent" style=" margin-left :42%">
                                        <asp:CustomValidator ID="cvPaymentType" runat="server" controltovalidate="ddlPaymentType" EnableViewState="false" SetFocusOnError="true" Display="Dynamic" Errormessage="Please select Salary Type." ClientValidationFunction="fnValidateSalary" ValidationGroup="valSearchAgents"/>
                                    </div>
                                    
                                    <div class="TableFormValidatorContent" style=" margin-left :42%">
                                        <asp:CustomValidator ID="cvCurrencyTO" runat="server" controltovalidate="ddlCurrency" EnableViewState="false" SetFocusOnError="true" Display="Dynamic" Visible="false" Errormessage="Please select Currency type." ClientValidationFunction="fnValidateCurrencyTo" ValidationGroup="valSearchAgents"/>
                                    </div>
                                    
                                    <div class="TableFormValidatorContent" style=" margin-left :42%">
                                        <asp:CustomValidator 
                                            ID="cvPaymentTypeTo" 
                                            runat="server" 
                                            controltovalidate="ddlPaymentType" 
                                            EnableViewState="false" 
                                            SetFocusOnError="true" 
                                            Display="Dynamic"  
                                            Visible="false"
                                            Errormessage="Please select Salary Type." 
                                            ClientValidationFunction="fnValidateSalaryTo" 
                                            ValidationGroup="valSearchAgents"/>
                                    </div>                                            
                           </div>
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label ID="lblSearchInHotList" runat="server" Text="Hot List"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:DropDownList ID="ddlHotList" runat="server" CssClass="CommonDropDownList" Width="187px"></asp:DropDownList>
                                     <asp:TextBox ID="txtSearch_HotList" runat ="server" CssClass ="CommonTextBox" AutoComplete="OFF" Visible ="false" Width ="200px"></asp:TextBox>
                                     <div id="divSearch_HotList" style ="z-index :9999"></div>
                                     <ajaxToolkit:AutoCompleteExtender ID="autoSeachHotList"
                                                                     runat ="server"  
                                                                     ServicePath="~/AjaxExtender.asmx"  
                                                                     TargetControlID="txtSearch_HotList" 
                                                                     MinimumPrefixLength="1" 
                                                                     ServiceMethod="GetHotListItems"   
                                                                     EnableCaching="True"   
                                                                     CompletionInterval="0" 
                                                                     CompletionListCssClass="AutoCompleteBox" 
                                                                     FirstRowSelected="True"
                                                                     OnClientItemSelected ="HotListSelected"
                                                                      ></ajaxToolkit:AutoCompleteExtender>
                                                                     
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label ID="lblEducation" runat="server" Text="Education"></asp:Label>:
                                </div>
                                <div class ="TableFormContent">
                                <div style="border: solid 1px #7f7f7f;border-collapse:collapse;color: #333333; width:205px; height :80px; overflow 

:auto ">
                                <asp:CheckBoxList ID ="chkEducationList" runat ="server" AutoPostBack ="false" EnableViewState ="true" ></asp:CheckBoxList>
                                </div>
                                </div> 
                                </div> 
                        </div>
                        <div class="FormRightColumn" style =" width :50%">
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label ID="lblJobType" runat="server" Text="Employment Type"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:DropDownList ID="ddlJobType" runat="server" CssClass="CommonDropDownList" Width="200px">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label ID="lblResumeLastUpdate" runat="server" Text="Resume Last Updated"></asp:Label>:
                                </div>
                               <div class="TableFormContent">
                                    <asp:DropDownList ID="ddlResumeLastUpdate" runat="server" CssClass="CommonDropDownList">
                                    </asp:DropDownList>                                            
                                </div>                                         
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label ID="Label2" runat="server" Text="Candidate Type">Candidate Type</asp:Label>:
                                </div>
                               <div class="TableFormContent">
                                    <asp:DropDownList ID="ddlApplicantType" runat="server" CssClass="CommonDropDownList">
                                    </asp:DropDownList>                                            
                                </div>                                         
                            </div>
                             <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label EnableViewState="false" ID="lblWorkSchedule" runat="server" Text="Work Schedule"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:DropDownList EnableViewState="true" ID="ddlWorkSchedule" runat="server" CssClass="CommonDropDownList">
                                    </asp:DropDownList>                                        
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label ID="lblWorkStatus" runat="server" Text="Work Status"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <div style="border: solid 1px #7f7f7f;border-collapse:collapse;color: #333333; width:205px; height :80px; overflow :auto ">
                                    <asp:CheckBoxList ID="lstWorkStatus" runat="server"></asp:CheckBoxList>
                               </div>                                        
                         
                            </div>
                            
                              <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label ID="lblHiringStatus" runat="server" Text="Hiring Status"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <div style="border: solid 1px #7f7f7f;border-collapse:collapse;color: #333333; width:205px; height :80px; overflow :auto ">
                                    <asp:CheckBoxList ID="chkHiringStatus" runat="server"></asp:CheckBoxList></div> 
                               </div>                                        
                            </div>
                            
                            
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label ID="lblAvailability" runat="server" Text="Available After"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                   <asp:TextBox   
    ID="txtJoiningDate"  
    runat="server"  Width ="100px"
    >  
</asp:TextBox>  
   <ajaxToolkit:CalendarExtender   
    ID="wdcJoiningDate"  
    runat="server"  
    TargetControlID="txtJoiningDate"    
    CssClass="custom-calendar"   
     PopupButtonID ="imgShow"    EnableViewState ="true"  
       
    >  
</ajaxToolkit:CalendarExtender>  


<asp:ImageButton ID="imgShow" runat ="server"  CssClass ="calendarDropdown"  ImageUrl ="~/Images/downarrow.gif" />                            
                                </div>
                            </div>
                           <%-- <div class ="TableRow">
    <div class ="TableFormValidatorContent" style =" margin-left : 42%">
        <asp:RangeValidator ID="gvDate" runat="server" Display ="Dynamic"  ErrorMessage="Enter valid date and Joining date should be greater than or equal to current date." ControlToValidate="txtJoiningDate" Type="Date" ValidationGroup ="ATSPreciseSearch"  ></asp:RangeValidator>
    </div>
</div>--%>
                            <div class="TableRow" id ="dvSecurityClearence" runat ="server" >
                                <div class="TableFormLeble">
                                    <asp:Label ID="lblSecurityClearance" runat="server" Text="Security Clearance"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:RadioButtonList ID="optSecurityClearance" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text ="Yes" Value ="1"  ></asp:ListItem>
                                    <asp:ListItem Text ="No" Value ="0" Selected ="True"  ></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </div>
                        </div> 
                      <div class="TableRow" ALIGN="center" >
                      <asp:Button ID="btnSave" runat ="server" Text ="Save" CssClass ="CommonButton" 
                              OnClick="btnSave_Click" ValidationGroup ="valSearchAgents" />
                      <asp:Button ID ="btnRemove" runat ="server" Text ="Reset Search Terms" 
                              CssClass ="CommonButton" OnClick="btnRemove_Click" />
                      </div> 


</div>

