﻿<%--
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberHotList.ascx
    Description: This is the control which is used to add candidate to hotlist 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Jan-22-2009          Jagadish              Defect id: 9787; Added Lable 'lblCandidateOrConsultant'.
-------------------------------------------------------------------------------------------------------------------------------------------
--%>

<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MemberHotList.ascx.cs"
    Inherits="TPS360.Web.UI.ControlMemberHotList" %>
    <%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<div>
<asp:HiddenField ID="hdnMemberId" runat="server" />
<asp:HiddenField ID="hdnMemberRole" runat="server" />
    <div class="TableRow" style="text-align: left;">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>
    <div class="TabPanelHeader" >
          Add Candidate to Hot List
    </div>
     
    <div class="TableRow" style="width: 100%;">
        <div class="TableFormLeble" style="text-align: right;">
            <asp:Label ID="lblAddHotList" runat="server" Text="Add to Hot List"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:DropDownList ID="ddlHotList" runat="server" CssClass="CommonDropDownList" ValidationGroup="HotListAdd">
            </asp:DropDownList>
            &nbsp;
            <asp:Button ID="btnAdd" CssClass="CommonButton" Text="Add" runat="server" OnClick="btnAdd_Click" ValidationGroup ="HotListAdd" />
        </div>
    </div>
    <div class="TableRow">
        <div class="TableFormValidatorContent" style =" margin-left :42%">
            <asp:CompareValidator ID="cvLevelOfEducation" runat="server" ControlToValidate="ddlHotList"
                Type="Integer" Operator="NotEqual" ValueToCompare="0" ErrorMessage="Please select hot list."
                EnableViewState="False" Display="Dynamic" ValidationGroup="HotListAdd"></asp:CompareValidator>
        </div>
    </div>
    
    
        <%--<div class="TableRow" style="text-align: left; font-weight: bold;">
       <b><asp:Label ID="lblMemberName" Text="" runat="server"></asp:Label></b> has been added
        to the following Hot Lists:
    </div>
    <br />--%>
    <div class="TabPanelHeader" >
           Current Hot Lists 
    </div>
    <div class="GridContainer" style="text-align: left;">
    <asp:ObjectDataSource ID="odsHotList"  runat="server" SelectMethod="GetPaged"
                                 TypeName="TPS360.Web.UI.MemberGroupMapDataSource"
                                SelectCountMethod="GetListCount" EnablePaging="True" SortParameterName="sortExpression">
                               <SelectParameters >
                                <asp:Parameter Name ="MemberId" DefaultValue ="" />
                               </SelectParameters>
                                </asp:ObjectDataSource>
        <asp:ListView ID="lsvCandidateHotList" runat="server" DataKeyNames="Id" DataSourceID ="odsHotList" OnItemCommand="lsvCandidateHotList_ItemCommand"
            OnItemDataBound="lsvCandidateHotList_ItemDataBound">
            <LayoutTemplate>
                <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                    <tr>
                        <th style="width: 75%; white-space: nowrap; text-align: left;">
                            Hot List
                        </th>
                        <th style="width: 25%; white-space: nowrap; text-align: left;">
                            Date Added
                        </th>
                        <th id ="thhotlist" runat ="server"  style="width: 140px; white-space: nowrap; text-align: left;">
                            Remove from Hot List
                        </th>
                    </tr>
                    <tr id="itemPlaceholder" runat="server">
                    </tr>
                     <tr class="Pager">
                                                <td colspan="3">
                                                    <ucl:Pager ID="pagerControl_Requisition" runat="server" EnableViewState="true" />
                                                </td>
                                            </tr>
                </table>
            </LayoutTemplate>
            <EmptyDataTemplate>
                <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                    <tr>
                        <td>
                            No data was returned.
                        </td>
                    </tr>
                </table>
            </EmptyDataTemplate>
            <ItemTemplate>
                <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                    <td>
                        <asp:Label ID="lblHotList" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="lblDateAdded" runat="server" />
                    </td>
                    <td id ="tdhotlist" runat ="server"  style="text-align: center;">
                        <asp:ImageButton ID="btnRemove" Visible="true" SkinID="sknDeleteButton" runat="server"
                            CommandName="DeleteItem"></asp:ImageButton>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
    </div>
    <br />
   
</div>
