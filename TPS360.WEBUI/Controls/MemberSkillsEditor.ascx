﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MemberSkillsEditor.ascx.cs" Inherits="TPS360.Web.UI.MemberSkillsEditor"   %>

<%@ Register Assembly ="AjaxControlToolkit" Namespace ="AjaxControlToolkit" TagPrefix ="ajaxToolkit" %>

    <%@ Register Src ="~/Controls/PagerControl.ascx" TagName ="Pager" TagPrefix ="ucl" %>
    <script type="text/javascript" language="javascript" src="../Scripts/Common.js"></script>
<script type="text/javascript" language="javascript">
  function  HeaderSkillCheckBoxClicked(chkAllItem)
    {
         $("table#tlbTemplateskill tbody tr").each(function(){
            $(this).find(":checkbox").attr('checked',chkAllItem .checked);
        });
        SelectRowCheckboxRejected();

    }
     function IsNumeric(input) {
        return (input - 0) == input && input.length > 0;
    }

 function SelectRowCheckboxRejected(param1, param2) {
 
 
  
        // debugger;
        var ischeckall = true;
        var HeaderCheckbox = document.getElementById('chkAllItem');
        var elements = HeaderCheckbox.form.elements;

        for (var i = 0; i < elements.length; i++) {
            if ((elements[i].type == 'checkbox') && (elements[i].id != HeaderCheckbox.id)) {
                if (elements[i].checked == true) {
                    var hndId = elements[i + 1];
                   
                    if (IsNumeric(hndId.value)) {
                        ADDID(hndId.value, '<%= hdnSelectedIDS.ClientID %>');
                    }

                }
                else {
                    var hndId = elements[i + 1];
                    if (IsNumeric(hndId.value)) {
                        RemoveID(hndId.value, '<%= hdnSelectedIDS.ClientID %>');
                    }
                }
            }
        }
       // HeaderCheckbox.checked = ischeckall;
    }
function SkillsSelected(source, eventArgs)
{
try
{
    hdnSelectedSkill=document .getElementById ("ctl00_cphCandidateMaster_uwtResumeBuilder__ctl3_ucntrlskills_hdnSelectedSkill");
    hdnSelectedSkillText=document .getElementById ("ctl00_cphCandidateMaster_uwtResumeBuilder__ctl3_ucntrlskills_hdnSelectedSkillText");
    hdnSelectedSkillText.value=eventArgs.get_text();
    hdnSelectedSkill.value=eventArgs.get_value(); 
    }
    catch (e)
    {
    }
}

</script>

<asp:UpdatePanel ID="upSkill" runat ="server" >
<ContentTemplate >

<asp:HiddenField ID="hdnSelectedIDS" runat="server" />
<asp:HiddenField ID="hdnSelectedSkillText" runat ="server" />
<asp:HiddenField ID="hdnSelectedSkill" runat ="server" />
<asp:TextBox ID ="hdnSortColumn" runat ="server" Visible ="false"   />
<asp:TextBox ID ="hdnSortOrder" runat ="server"  Visible ="false" />
<asp:HiddenField ID="hdnSortColumnName" runat ="server" />
<asp:HiddenField ID="hdnSortColumnOrder" runat ="server" />
<div id ="dvSkills" runat ="server" style =" width :100%" > 
    <div class="TableRow" style="text-align: center">
        <asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
        <asp:HiddenField ID="hfMemberId" runat="server" Value="0" />
    </div>
    
    <div class="TabPanelHeader" >
        Add Skill
    </div>
    <div class="TableRow">
        <div class="TableFormLeble">
            <asp:Label EnableViewState="false" ID="lblSkillName" runat="server" Text="Skill"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <div id="divSkill" runat ="server" ></div>
            <asp:TextBox ID="txtSkillName" runat ="server" CssClass="CommonTextBox" AutoComplete="OFF"  EnableViewState ="true"></asp:TextBox><span class="RequiredField">*</span>
            <ajaxToolkit:AutoCompleteExtender ID="txtSkillName_AutoCompleteExtender"  
                runat="server" 
                ServicePath="~/AjaxExtender.asmx" 
                TargetControlID="txtSkillName" 
                MinimumPrefixLength="1" 
                ServiceMethod="GetSkills"   
                EnableCaching="true"    
                CompletionListElementID ="divSkill"  
                CompletionInterval="0" 
                CompletionListCssClass="AutoCompleteBox" 
                FirstRowSelected="True" 
                OnClientItemSelected ="SkillsSelected" />   
                   
               
        </div>
    </div>
    <div class="TableRow">
        <div class="TableFormValidatorContent" style=" margin-left :42%">  
            <asp:RequiredFieldValidator ID="rfvSkill" runat="server" ControlToValidate="txtSkillName" 
            ErrorMessage="Please enter Skill." EnableViewState="False" Display="Dynamic"
            ValidationGroup="SkillInfo"></asp:RequiredFieldValidator>
        </div>
    </div>

    <div class="TableRow">
        <div class="TableFormLeble">
             <asp:Label EnableViewState="false" ID="lblYearsUsed" runat="server" Text="Years Used"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:DropDownList EnableViewState="true" ID="ddlYearsUsed" runat="server" Width ="100px"  
            CssClass="CommonDropDownListx" AutoPostBack ="false"   >
            </asp:DropDownList>
        </div>
    </div>
    <div class="TableRow">
        <div class="TableFormLeble">
             <asp:Label EnableViewState="false" ID="lblLastUsed" runat="server" Text="Last Used"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:DropDownList EnableViewState="true" ID="ddlLastUsed" runat="server" Width ="100px"
            CssClass="CommonDropDownListx" AutoPostBack ="false"   >
            </asp:DropDownList>
        </div>
    </div>
    <div class="TableRow" style="text-align: center">
        <asp:Button ID="btnSave" CssClass ="CommonButton" runat ="server" Text ="Save" ValidationGroup="SkillInfo" OnClick ="btnSave_Click" />
    </div>
    <div class="TabPanelHeader" >
        List of Skills
    </div>
    </div>
    
     <div>
        <div class="GridContainer" style="text-align: left;">
         <asp:LinkButton ID="btnRemove" runat="server" Text="Delete" OnClick="btnRemove_Click"
                            CssClass="btn btn-danger" style=" margin-bottom : 5px;"  OnClientClick ="return ConfirmDelete('skill(s)');" CausesValidation="true" />
         <asp:ObjectDataSource ID="odsSkillsList" runat="server" SelectMethod="GetPagedByMemberId"
            TypeName="TPS360.Web.UI.MemberSkillsDataSource" EnablePaging="true" SelectCountMethod="GetListCount"
            SortParameterName="sortExpression" OnInit="odsSkillsList_Init"></asp:ObjectDataSource>
            <asp:ListView ID="lsvSkills" runat="server" DataKeyNames="Id"  DataSourceID="odsSkillsList"
            OnItemDataBound="lsvSkills_ItemDataBound"  OnPreRender ="lsvSkills_PreRender"
            OnItemCommand="lsvSkills_ItemCommand" EnableViewState="true">
            <LayoutTemplate>
                <table id="tlbTemplateskill" class="Grid" cellspacing="0" border="0" style ="width :100%">
                    <tr style ="width :100%" enableviewstate ="true" runat ="server"  >
                        <th style="width: 20px !important;  " id="thCheck" runat ="server" >
                                        <input id="chkAllItem" name="chkAllItem" type="checkbox" onclick="HeaderSkillCheckBoxClicked(this)" />
                        </th>
                        <th enableviewstate ="true" > <asp:LinkButton ID="btnSkill" runat="server" CommandName="Sort" ToolTip="Sort By Skill" CommandArgument="SkillId" Text="Skill" /></th>
                        <th style=" width : 80px;"> <asp:LinkButton ID="btnYearsUsed" runat="server" ToolTip="Sort By Years Used" CommandName="Sort" CommandArgument="YearsOfExperience" Text="Years Used" /></th>
                        <th style=" width : 70px;"> <asp:LinkButton ID="btnLastUsed" runat="server" CommandName="Sort" CommandArgument="LastUsed" Text="Last Used" ToolTip="Sort By Last Used" /> </th>
                        <th style=" width:45px !important " runat ="server" id ="thAction"> Action </th> 
                    </tr>
                    <tr id="itemPlaceholder" runat="server">
                    </tr>
                    <tr class="Pager">
                        <td colspan="5" runat ="server" id="tdPager">
                           <ucl:Pager Id="pagerControl" runat ="server" EnableViewState ="true"  />
                           
                        </td>
                    </tr>
                </table>
            </LayoutTemplate>
            <EmptyDataTemplate>
                <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                    <tr>
                        <td>
                            No Skills data available.
                        </td>
                    </tr>
                </table>
            </EmptyDataTemplate>
            <ItemTemplate>
                <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                  <td  style="width: 20px;" runat ="server" id ="tdCheck">
                                    <asp:CheckBox runat="server" ID="chkItemSkill" OnClick="SelectRowCheckboxRejected()" />
                                    <asp:HiddenField ID="hfSkillId"  runat="server" />
                                </td>
                    <td> <asp:Label ID="lblSkill" runat="server" /></td>
                    <td> <asp:Label ID="lblYearsUsed" runat="server" /> </td>
                    <td>  <asp:Label ID="lblLastUsed" runat="server" /> </td>
                    <td style="text-align: center;" runat ="server" id ="tdAction">
                        <asp:ImageButton ID="btnEdit" SkinID="sknEditButton" CausesValidation="false" runat="server"
                            CommandName="EditItem"></asp:ImageButton>
                        <asp:ImageButton ID="btnDelete" Visible="true" SkinID="sknDeleteButton" runat="server"
                            CommandName="DeleteItem"></asp:ImageButton>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
    </div>
    
    </div>
</ContentTemplate>
</asp:UpdatePanel>