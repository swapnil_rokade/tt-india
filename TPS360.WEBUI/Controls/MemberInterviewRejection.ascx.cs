﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberInterviewRejection.ascx.cs
    Description: This is the user control page used for member interview functionalities
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Sep-29-2008           Yogeesh Bhat        Defect ID: 8734; Added PagerCommand() method for pager events
    0.2            Oct-01-2008           Yogeesh Bhat        Defect ID: 8733; Added delete functionlity in lsvInterviewSchedule_ItemCommand()
    0.3            Oct-06-2008           Yogeesh Bhat        Defect ID: 8731; Added Update, Cancel functionality for external interviewers
                                                             in btnAdd_Click() method;
    0.4            Oct-14-2008           Yogeesh Bhat        Defect ID: 8893; Added new method, ddlJobPosting_SelectedIndexChanged()
    0.5            Nov-04-2008           Jagadish            Defect ID: 9029; Added code to make First name, Last name, Email id text fields empty and cancel button invisible
                                                             when user selects edit mode in 'Schedule list & reports' tab.
    0.6            Nov-28-2008           Jagadish            Defect ID: 9199; Added the message when email id not exist.
    0.7            Dec-02-2008           Anand Dixit         Defect ID: 8955; Included Conversion from UTC to Local time.
    0.8            Jan-05-2009           Jagadish            Defect ID: 9522; Included Conversion from UTC to Local time.
    0.9            Jan-15-2009           Yogeesh Bhat        Defect ID: 9705: Changes made in PrepareView() method
    1.0            Jan-16-2009           Gopala Swamy        Defect ID: 9728: Changes made in lsvInterviewSchedule_ItemDataBound() method
    1.1            Jan-16-2009           Jagadish            Defect Id: 9704: Changed the control Dropdown to WebDateTimeEdit.
    1.2            Jan-21-2009           Jagadish            Defect Id: 9717: Modification to #9704. Added editable dropdown.
    1.3            Feb-12-2009           Yogeesh Bhat        Defect Id: 9246: Changes made in btnSave_Click(); Added new method GetInterviewerSchedule()
    1.4            Feb-13-2009           Nagarathna          Defect ID:9697:changes made in lsvExternalInterviewers_ItemCommand(),lsvInterviewSchedule_ItemCommand; 
 *  1.5            Sept-24-2009         Nagarathna V.B      Enha#11473;passing current user for send email method call
 *  1.6            Jan-19-2010          Gopala Swamy J        Defect Id:12901;Changed Style attribute and put events called "OnTextChanged"
 *  1.7            Apr-19-2010          Sudarshan.R.          Defect Id:12651; changes made to stop btnAdd click event firing on page refresh.
 *  1.8            Apr-22-2010          Sudarshan.R.          Defect Id:12712 ; Changes made in Page_Load.
-------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Common;
using System.Web;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Web.UI;
using Microsoft.Office.Interop.Outlook;
namespace TPS360.Web.UI
{
    public partial class ControlMemberInterviewRejection : BaseControl
    {
        #region Member Variables

        private static int _memberId = 0;
        private static int _interviewId = 0;
        private static string _memberrole = string.Empty;
        private static bool isNew = true;
        private static bool IsAccessToDelete = false;
        private bool _mailsetting = false;
        private string _sessionKey;
        #endregion

        #region Properties

        public string MemberRole
        {
            set { _memberrole = value; }
            get { return _memberrole; }
        }

        public int MemberId
        {
            set { _memberId = value; }
        }
        public string TempDirectory
        {
            get
            {
                return (string)Helper.Session.Get("tempDir");
            }
            set
            {
                Helper.Session.Set("tempDir", value);
            }
        }
        public string PreviewDir
        {
            get;
            set;
        }
        string CandidateId
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
                {
                    return Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID];

                }
                ////From Member Login
                else
                {
                    if (!StringHelper.IsBlank(Helper.Url.SecureUrl["CanId"]))
                    {
                        return Helper.Url.SecureUrl["CanId"];

                    }
                    else return "0";
                }

            }
        }

        int InterId
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_INTERVIEWSCHEDULE_ID]))
                {
                    return Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_INTERVIEWSCHEDULE_ID]);

                }
                ////From Member Login
                else
                {
                    return 0;
                }

            }
        }

        int JobPostingId
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID]))
                {
                    return Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID]);

                }
                ////From Member Login
                else
                {
                    return 0;
                }

            }
        }

        public bool isMainApplication
        {
            get
            {
                switch (ApplicationSource)
                {
                    case ApplicationSource.LandTApplication:
                        return false;
                        break;
                    case ApplicationSource.MainApplication:
                        return true;
                        break;
                    default:
                        return true;
                        break;
                }
                return true;
            }
        }




        #endregion

        #region Methods



        private void BindInterviewList()
        {
            lsvInterviewRejection.DataSource = Facade.GetRejectCandidateByMemberId(Convert.ToInt32(CandidateId));
            lsvInterviewRejection.DataBind();
        }
        private void PrepareView()
        {
            BindInterviewList();
        }
        public string SessionKey
        {
            get
            {
                return _sessionKey;
            }
            set
            {
                _sessionKey = value;
            }
        }
        
        private void PlaceUpDownArrow()
        {

            try
            {
                LinkButton lnk = (LinkButton)lsvInterviewRejection.FindControl(txtSortColumn.Text);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }

        }
        
        #endregion

        #region Events

        #region Page event

        protected void Page_Load(object sender, EventArgs e)
        {

            
            //cmpStartDate.ValueToCompare = DateTime.Now.ToShortDateString();
            

            if (!IsPostBack)
            {
                
                PrepareView();
                
            }

            
            
            //if (Request.RawUrl.Contains("TPS360Overview.aspx"))
            //{
            //    if (tdPager != null) tdPager.ColSpan = 9;
            //}
            //else
            //{
            //    if (tdPager != null) tdPager.ColSpan = 10;
            //}
            //if (tdPager != null)
            //{
            //    if (IsAccessToDelete || IsUserAdmin)
            //        tdPager.ColSpan = 10;
            //    else
            //        tdPager.ColSpan = 9;
            //}
            
            string pagesize = "";
            pagesize = (Request.Cookies["InterviewRowPerPage"] == null ? "" : Request.Cookies["InterviewRowPerPage"].Value);
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvInterviewRejection.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
            }
            
            if (IsPostBack)
            {
                _sessionKey = "InterviewEmailFileUpload";
            }

        }

        #endregion

        #region ListView Events

        protected void lsvInterviewRejection_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            bool _IsAdmin = base.IsUserAdmin;
            try 
            {
                if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
                {
                    RejectCandidate candidate = ((ListViewDataItem)e.Item).DataItem as RejectCandidate;

                    if (candidate != null)
                    {
                        System.Web.UI.HtmlControls.HtmlTableCell tdPager = (System.Web.UI.HtmlControls.HtmlTableCell)lsvInterviewRejection.FindControl("tdPager");
                        tdPager.ColSpan = 5;
                        
                        Label lblDateTime = (Label)e.Item.FindControl("lblDateTime");
                        LinkButton lblJobTitle = (LinkButton)e.Item.FindControl("lblJobTitle");
                        //Label lblTitle = (Label)e.Item.FindControl("lblTitle");
                        Label lblRejectedBy = (Label)e.Item.FindControl("lblRejectedBy");
                        Label lblReason = (Label)e.Item.FindControl("lblReason");
                        
                        lblDateTime.Text = candidate.UpdateDate.ToString();

                        lblJobTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + candidate.JobPostingID.ToString() + "&FromPage=JobDetail" + "','700px','570px'); return false;");

                        SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.Requisition.DIR + UrlConstants.Requisition.JOB_POSTING_PREVIEW, string.Empty, UrlConstants.PARAM_JOB_ID, candidate.JobPostingID.ToString());
                        if (candidate.JobTitle.Length > 16)
                        {
                            lblJobTitle.Text = candidate.JobTitle.Substring(0, 16);
                            lblJobTitle.ToolTip = candidate.JobTitle;
                        }
                        else
                            lblJobTitle.Text = candidate.JobTitle;
                        //lblJobTitle.OnClientClick = "window.open('" + url + "')";
                        lblRejectedBy.Text = candidate.RejectorName;

                        if (candidate.RejectDetails.Length > 13)
                        {
                            lblReason.Text = candidate.RejectDetails.Substring(0, 13) + "...";
                            lblReason.ToolTip = candidate.RejectDetails;
                        }
                        else
                            lblReason.Text = candidate.RejectDetails;

                        //lblTitle.Text = candidate.InterviewTitle;
                        //if (lblTitle.Text.Length > 18)
                        //{
                        //    lblTitle.Text = lblTitle.Text.Substring(0, 18) + "...";
                        //    lblTitle.ToolTip = candidate.InterviewTitle;
                        //}
                    }
                }
            }
            catch 
            { 
            
            }
            
        }
        protected void lsvInterviewRejection_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvInterviewRejection.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                else
                {
                    lsvInterviewRejection.DataSource = null;
                    lsvInterviewRejection.DataBind();
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "InterviewRowPerPage";
            }
            else
            {
                lsvInterviewRejection.DataSource = null;
                lsvInterviewRejection.DataBind();
            }
            PlaceUpDownArrow();
            //if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
            //{
            //    LinkButton btnClient = (LinkButton)lsvInterviewSchedule.FindControl("btnClient");
            //    if (btnClient != null) btnClient.Text = "BU";
            //}
        }
        protected void lsvInterviewRejection_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);
            try
            {

                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                        else txtSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn.Text = lnkbutton.ID;
                        txtSortOrder.Text = "ASC";
                    }
                }
            }
            catch
            {
            }
        }

        #endregion

        #endregion

    }
}
