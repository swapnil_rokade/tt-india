<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EmailFileUpload.ascx.cs"
    Inherits="TPS360.Web.UI.ctlEmailFileUpload" %>

<script language="JavaScript" src="../Scripts/attachment.js" type="text/javascript"></script>

<script type="text/javascript" language="javascript">

function getattachment()
{
getattachments();
}
</script>

<asp:HiddenField ID="hdnTmpFolder" runat="server" />
<asp:HiddenField ID="hdnClear" runat="server" />
<asp:UpdatePanel ID="pnlEmailEditorTab" runat="server" UpdateMode="Conditional">
    <Triggers>
        <asp:PostBackTrigger ControlID="btnUpload" />
    </Triggers>
    <ContentTemplate>
        <table width="70%" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td align="left" style="white-space: nowrap;">
                </td>
                <td align="left" style="white-space: nowrap;">
                    <div class="TableRow">
                        <div class="TableFormLeble" style="text-align: left;">
                            Attachment to upload</div>
                    </div>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="left" style="white-space: nowrap;">
                    <asp:TextBox EnableViewState="false" ID="txtTitle" Visible="false" runat="server"
                        CssClass="CommonTextBox"></asp:TextBox><br />
                </td>
                <td align="left" style="white-space: nowrap;" colspan="2">
                    <asp:FileUpload ID="filSource" Width="200px" runat="server" CssClass="CommonTextBox"
                        EnableViewState="false" />
                    <asp:Button ID="btnUpload" runat="server" CssClass="CommonButton" Font-Size="7" Text="Attach"
                        OnClick="UploadClicked" EnableViewState="false" ValidationGroup="EmailFileUpload"
                        UseSubmitBehavior="false" />
                </td>
            </tr>
            <tr>
                <td align="left" colspan="3">
                    <asp:RequiredFieldValidator ID="RFVFileBrowse" runat="server" ErrorMessage="Please upload the file."
                        ValidationGroup="EmailFileUpload" Display="Dynamic" ControlToValidate="filSource"
                        Font-Size="11px"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revfuDocument" runat="server" ErrorMessage="Invalid File Format"
                        ControlToValidate="filSource" ValidationExpression="^(?!.*\.exe$).*$" Display="Dynamic"
                        ValidationGroup="EmailFileUpload"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td style="height: 10" colspan="3">
                </td>
            </tr>
            <tr id="tdAttachFile" runat="server">
                <td align="left" colspan="3">
                    <div class="TableRow">
                        <div class="TableFormLeble" style="text-align: left;">
                            <asp:Label ID="lblUploadedFilesText" runat="server" EnableViewState="false" Text="Attach Files:"></asp:Label>
                        </div>
                    </div>
                </td>
            </tr>
            <tr id="tdUploadedFiles" runat="server">
                <td align="left" colspan="3">
                    <asp:GridView ID="gvFiles" CssClass="grid" CellSpacing="1" CellPadding="3" GridLines="None"
                        AutoGenerateColumns="False" runat="server" OnRowDataBound="SourceRowDataBound"
                        OnRowCommand="SourceRowCommand">
                        <RowStyle CssClass="gridRow"></RowStyle>
                        <AlternatingRowStyle CssClass="gridAltRow" />
                        <Columns>
                            <asp:TemplateField HeaderText="File Name">
                                <HeaderStyle Width="80%" CssClass="gridHeader" HorizontalAlign="Left" />
                                <ItemStyle Wrap="False" Width="80%" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkFileName" CssClass="FileTitle" Target="_blank" runat="server"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <HeaderStyle Width="20%" CssClass="gridHeader" />
                                <ItemStyle HorizontalAlign="Left" Wrap="False" Width="20%" />
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnDelete" Visible="true" SkinID="sknDeleteButton" runat="server"
                                        CommandName="DeleteFile"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            Did not find any Attached file.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <style type="text/css">
            .FileTitle
            {
                text-transform: capitalize;
            }
        </style>
    </ContentTemplate>
</asp:UpdatePanel>
