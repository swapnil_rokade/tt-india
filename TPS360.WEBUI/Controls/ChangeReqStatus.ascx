﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ChangeReqStatus.ascx.cs" Inherits="Controls_ChangeReqStatus" %>
<asp:UpdatePanel ID="upChangeStatus" runat ="server" >
<ContentTemplate >

<asp:HiddenField ID="hdCurrentJobPostingID"  runat ="server" />
    <div class="TableRow" style="text-align:left">
        <asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
    </div>
    <div style="text-align:center;padding-bottom:30px" id="divMessage" runat ="server" ><asp:Label ID ="lblNote" runat ="server"><b>Note:</b>only the original creator of the requisition is allowed to change the status.
</asp:Label></div>
    
    <div class="TableRow">
        <div class="TableFormLeble">
              <asp:Label EnableViewState="false" ID="lblJobStatus" runat="server" Text="Change Req Status"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:DropDownList ID="ddlJobStatus" runat="server" CssClass="CommonDropDownList" TabIndex ="1"
                >
            </asp:DropDownList>
           
        </div>
    </div>
    <div class="TableRow" >
        <div class="TableFormLeble" >
        </div>
        <div class="TableFormContent">
             <asp:Button ID="btnChangeJobStatus" CssClass="CommonButton" runat="server" Text="Save" TabIndex ="2" OnClick="btnChangeJobStatus_Click"   ValidationGroup ="re" />
        </div> 
        </div> 


</ContentTemplate>
</asp:UpdatePanel>