﻿
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CheckedDropDownList.ascx.cs"
    Inherits="TPS360.Web.UI.CheckedDropDownList" EnableViewState ="true"  %>
<style>
label
{
display:inline !important;
}
</style>
<link href= "../assets/CheckedDropdownList/css/jquery-ui-1.8.13.custom.css" rel ="Stylesheet" />
<link href ="../assets/CheckedDropdownList/css/ui.dropdownchecklist.themeroller.css" rel ="Stylesheet" />
<script type="text/javascript" src="../assets/CheckedDropdownList/js/jquery-ui-1.8.13.custom.min.js"></script>
<script src="../assets/CheckedDropdownList/js/ui.dropdownchecklist-1.4-min.js" type ="text/javascript" ></script>

 <asp:HiddenField ID="hdnSelectedList" runat ="server"  EnableViewState ="true"  />
 
 <div style =" display : inline-block ;width : 200px; white-space : normal " >
 
            <asp:DropDownList ID="ddlList" runat="server" CssClass="s4" EnableViewState ="true"   data-placeholder="Choose a State..." 
                Width="158px" TabIndex="25">
            </asp:DropDownList>
       
</div> 
<script type="text/javascript"> 
   Sys.Application.add_load(function() { 
     
    
      $(".s4").dropdownchecklist( {  forceMultiple: true
    , onComplete: function(selector) {
        var values = "";
        var allChecked=true ;
        for( i=0; i < selector.options.length; i++ ) {
            
            if (selector.options[i].selected && (selector.options[i].value != "")&& (selector.options[i].value != "0")) {
                if ( values != "" ) values += ",";
                values += selector.options[i].value;
            }
           
        }
     
	  	$('#<%=hdnSelectedList.ClientID %> ').val(values);
	  	alert ($('#<%=hdnSelectedList.ClientID %> ').val());
    } 
	
    });
  });
    </script>
    