﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LeftPanel.ascx.cs" Inherits="TPS360.Web.UI.LeftPanel" EnableViewState="true"%>
            <asp:ListView ID="lvNavigationItems" OnItemDataBound="lvNavigationItems_ItemDataBound" runat="server">
                <LayoutTemplate>
                        <li id="itemPlaceholder" runat="server" />
                </LayoutTemplate>
                <ItemTemplate>
                <li id="lnkLeftMenuItem" runat ="server" ></li>
                        
                </ItemTemplate>
     </asp:ListView> 

<asp:SiteMapDataSource runat="server" ID="LeftMenuSiteMap" ShowStartingNode="false" />