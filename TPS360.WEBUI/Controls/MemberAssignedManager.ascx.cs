﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberAssignedManager.ascx.cs
    Description: This is the user control page used for assigning manager
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Oct-10-2008           Yogeesh Bhat        Defect ID: 8908; Changes made in Page_Load() method.
                                                             (checked drop down list value (ddlEmployee) for current member id)
    0.2            May-05-2009           Gopala Swamy J      Defect Id:10407;Commented a line
    0.3            Sep-16-2009           Veda                Defect Id:11504;Blank data grid is displaying  
 * 0.4             Jan-4-2010            Basavaraj A         Defect id:11994 ; changes made to refresh Primary Manager 
-------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;

namespace TPS360.Web.UI
{
    public partial class cltMemberAssignedManager : BaseControl
    {
        #region Member Variables
        private bool Isdelete = false;
        private int _memberId = 0;
        #endregion

        #region Methods

        private void GetMemberId()
        {
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                hfMemberId.Value  = Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID];
            }
            _memberId = int.Parse(hfMemberId.Value);
        }

        private void LoadAssignedManagers()
        {
            hfPrimaryManagerId.Value = "0";
            IList<MemberManager> memberManagerList = Facade.GetAllMemberManagerByMemberId(_memberId);
            lsvAssignedManager.DataSource = memberManagerList;
            lsvAssignedManager.DataBind();
            if (lsvAssignedManager == null && lsvAssignedManager.Items.Count == 0)
                btnUpdate.Visible = false;
        }

        private void PrepareView()
        {
            MiscUtil.PopulateMemberListByRole(ddlEmployee, ContextConstants.ROLE_EMPLOYEE, Facade);
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            ddlEmployee.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            GetMemberId();
            PrepareView();
            
                MemberManager primaryManager = Facade.GetMemberManagerPrimaryByMemberId(_memberId);
                if (primaryManager != null)
                {
                    if (CurrentMember.Id != primaryManager.ManagerId && !IsUserAdmin)
                    {
                        divAssndManager.Visible = false;
                        btnAssign.Visible = false;
                        btnUpdate.Visible = false;
                    }
                }
                else
                {
                    if (!IsUserAdmin)
                    {
                        divAssndManager.Visible = false;
                        btnAssign.Visible = false;
                        btnUpdate.Visible = false;
                    }
                }
                MemberManager Manager = Facade.GetMemberManagerByMemberIdAndManagerId(_memberId, CurrentMember .Id );
                if (Manager == null && !IsUserAdmin)
                Helper.Url.Redirect(UrlConstants.Dashboard.DEFAULT_PAGE);
            if (!Page.IsPostBack)
            {
                LoadAssignedManagers();
                //for (int i = 0; i < ddlEmployee.Items.Count; i++)
                //{
                //    if (ddlEmployee.Items[i].Value == _memberId.ToString())
                //    {
                //        ddlEmployee.Items.RemoveAt(i);
                //        break;
                //    }
                //}
            }
            if (IsPostBack)
            {
                MiscUtil.LoadAllControlState(Request,this);
            }
            for (int i = 0; i < ddlEmployee.Items.Count; i++)
            {
                if (ddlEmployee.Items[i].Value == _memberId.ToString())
                {
                    ddlEmployee.Items.RemoveAt(i);
                    break;
                }
            }
        }

        protected void btnAssign_Click(object sender, EventArgs e)
        {
            int managerid = Int32.Parse(ddlEmployee.SelectedValue);
            
           MemberManager  Manager = Facade.GetMemberManagerByMemberIdAndManagerId(_memberId, managerid);
           if (Manager == null)
           {
               MemberManager newManager = new MemberManager();
               newManager.CreatorId = base.CurrentMember.Id;
               newManager.UpdatorId = base.CurrentMember.Id;
               newManager.ManagerId = managerid;
               newManager.MemberId = _memberId;
               if (hfPrimaryManagerId.Value == "0")
                   newManager.IsPrimaryManager = true;
               else
                   newManager.IsPrimaryManager = false;
               Facade.AddMemberManager(newManager);
               Facade.UpdateUpdateDateByMemberId(_memberId);
               LoadAssignedManagers();
               MiscUtil.ShowMessage(lblMessage, "Successfully assigned manager", false);
               ddlEmployee.SelectedIndex = 0;
           }
           else
           {
               LoadAssignedManagers();
               MiscUtil.ShowMessage(lblMessage, "Manager already assigned", true);
           }
           for (int i = 0; i < ddlEmployee.Items.Count; i++)
           {
               if (ddlEmployee.Items[i].Value == _memberId.ToString())
               {
                   ddlEmployee.Items.RemoveAt(i);
                   break;
               }
           }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            int membermanagerid = Request.Form["rdoPrimaryManager"] != null ? Int32.Parse(Request.Form["rdoPrimaryManager"]) : 0;
            int previousPrimaryManagerId = Int32.Parse(hfPrimaryManagerId.Value);
            if (membermanagerid != 0)
            {
                if (membermanagerid != previousPrimaryManagerId)
                {
                    MemberManager currentManager = null;
                    //Update previous manager
                    if (previousPrimaryManagerId != 0)
                    {
                        currentManager = Facade.GetMemberManagerById(previousPrimaryManagerId);
                        if (currentManager != null)
                        {
                            currentManager.IsPrimaryManager = false;
                            currentManager.UpdatorId = base.CurrentMember.Id;
                            Facade.UpdateMemberManager(currentManager);
                        }
                    }

                    //Change to current manager
                    currentManager = Facade.GetMemberManagerById(membermanagerid);
                    if (currentManager != null)
                    {
                        currentManager.IsPrimaryManager = true;
                        currentManager.UpdatorId = base.CurrentMember.Id;
                        Facade.UpdateMemberManager(currentManager);
                        if (CurrentMember.Id != currentManager .ManagerId  && !IsUserAdmin)
                        {
                            divAssndManager.Visible = false;
                            btnAssign.Visible = false;
                            btnUpdate.Visible = false;
                        }
                    }
                    Facade.UpdateUpdateDateByMemberId(_memberId);
                    MiscUtil.ShowMessage(lblMessage, "Successfully update primary manager",false );
                }
                else
                {                    
                    MiscUtil.ShowMessage(lblMessage, "Primary manager is not changed to update", true);
                }
            }
            else
            {                 
                MiscUtil.ShowMessage(lblMessage, "Please select primary manager", true);
            }
            LoadAssignedManagers();
        }
        protected void lsvAssignedManager_PreRender(object sender, EventArgs e)
        {
            System.Web.UI.HtmlControls.HtmlTableCell thUnAssign = (System.Web.UI.HtmlControls.HtmlTableCell)lsvAssignedManager.FindControl("thUnAssign");
            if (thUnAssign != null)
                thUnAssign.Visible = Isdelete;
            foreach (ListViewDataItem list in lsvAssignedManager .Items )
            {
                System.Web.UI.HtmlControls.HtmlTableCell tdUnAssign = (System.Web.UI.HtmlControls.HtmlTableCell)list.FindControl("tdUnAssign");
                if (tdUnAssign != null)
                    tdUnAssign.Visible = Isdelete;
            }

        }
        protected void lsvAssignedManager_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
   
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                MemberManager memberManager = ((ListViewDataItem)e.Item).DataItem as MemberManager;

                if (memberManager != null)
                {
                    Label lblManagerName = (Label)e.Item.FindControl("lblManagerName");
                    Label lblAssignedDate = (Label)e.Item.FindControl("lblAssignedDate");
                    Label lblPrimaryManager = (Label)e.Item.FindControl("lblPrimaryManager");
                    ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");
                    string member = Facade.GetMemberNameById(memberManager.ManagerId);
                    if (member != string .Empty )
                    {
                        lblManagerName.Text = member;
                        if (memberManager.IsPrimaryManager)
                        {
                            lblPrimaryManager.Text = "<input id='rdoPrimaryManager' checked name='rdoPrimaryManager' type='radio' value='" + memberManager.Id + "' />" + member ;
                            hfPrimaryManagerId.Value = memberManager.Id.ToString();
                        }
                        else
                        {
                            lblPrimaryManager.Text = "<input id='rdoPrimaryManager' name='rdoPrimaryManager' type='radio' value='" + memberManager.Id + "' />" + member;
                        }
                    }
                    lblAssignedDate.Text = memberManager.CreateDate.ToShortDateString();
                    
                        if (memberManager.IsPrimaryManager)
                            btnDelete.Visible = false ;
                        else
                        {
                            btnDelete.Visible = true ;
                            Isdelete = true;
                        }
                        btnDelete.OnClientClick = "return ConfirmDelete('Manager')";
                        btnDelete.CommandArgument = StringHelper.Convert(memberManager.Id);
                }
            }
            
        }

        protected void lsvAssignedManager_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);

            if (id > 0)
            {
                if (string.Equals(e.CommandName, "DeleteItem"))
                {
                    try
                    {
                        if (Facade.DeleteMemberManagerById(id))
                        {
                            LoadAssignedManagers();
                            MiscUtil.ShowMessage(lblMessage, "Manager is successfully un-assigned.", false);
                        }
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
            }
            //Response.Redirect(Request.RawUrl);
        }

        #endregion
    }
}
