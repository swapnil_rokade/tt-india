﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: OverviewMenu.ascx
    Description: This is the user control page used to display overview menu.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date             Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Mar-5-2009           Jagadish            Defect id: 10012; Applied scrolling effect for control 'tabList'.
-------------------------------------------------------------------------------------------------------------------------------------------       
--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OverviewMenu.ascx.cs" Inherits="TPS360.Web.UI.OverviewMenu" EnableViewState="true"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
    <div style="height: 19px;width:99%">
    <div style="float:left;height: 19px;width:2%;background-color:#7DBEFF;">
    <span id="Span1" onmouseover="startOverView_scroll_left('<%= tabList.ClientID %>')" class="ScrollStyleSubmenu"  onmouseout="stopOverView_scrolling();"></span>    
    </div>
    <div style="float:left;height: 19px;width:96%;background-color:#7DBEFF;">
                    <ul id="tabList" runat="server" class="overviewtab" >
                        <asp:Repeater ID="rptTopMenu" runat="server" OnItemDataBound="rptTopMenu_ItemDataBound">
                            <ItemTemplate>
                                <li runat="server" id="liId" rel="home">
                                <a style="cursor: hand; cursor: pointer;" runat="server" id="lnkMenuName">
                                    <asp:Label ID="lblMenuName" runat="server"></asp:Label>
                                </a></li>
                            </ItemTemplate>
                        </asp:Repeater>               
                    </ul>  
    </div>
    <div style="float:left;clear:right;height: 19px;width:2%;background-color:#7DBEFF;">
    <span id="Span2" onmouseover="startOverView_scroll_right('<%= tabList.ClientID %>')" class="ScrollStyleSubmenu"  onmouseout="stopOverView_scrolling();"></span>
    </div>
    
    </div>

    <div id="divScriptHolder" runat="server">
    </div>
    <asp:SiteMapDataSource runat="server" ID="MenuSiteMap" ShowStartingNode="false" />

    
    