﻿
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultipleItemPicker.ascx.cs"
    Inherits="TPS360.Web.UI.MultipleItemPicker" %>

<link href ="../assets/css/chosen.css" rel ="Stylesheet" />
<asp:HiddenField ID="hdnSelectedListItems" runat ="server" EnableViewState ="true"  />

            <asp:ListBox ID="ddlItem" runat="server" CssClass="chzn-select"  multiple=""   SelectionMode ="Multiple" 
                AutoPostBack="false" Width="158px" TabIndex="26">
            </asp:ListBox>
<script src="../assets/js/chosen.jquery.js" type ="text/javascript" ></script>
   <script type="text/javascript"> 
   Sys.Application.add_load(function() { 
   $(".chzn-select").chosen();
  $(".chzn-select-deselect").chosen({allow_single_deselect:true});
  $(".chzn-select").chosen().change(function (){
    $('#<%=hdnSelectedListItems.ClientID %>').val($(this).val());
   });
  });
    </script>

