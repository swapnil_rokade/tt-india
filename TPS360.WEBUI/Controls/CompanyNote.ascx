﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CompanyNote.ascx.cs" Inherits="TPS360.Web.UI.Controls_CompanyNote" %>
        <%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<asp:UpdatePanel ID="updPnlDynamic" runat="server">
    <ContentTemplate>
        <div>
            <asp:Label ID="lblMessage" EnableViewState="False" runat="server"></asp:Label>
        </div>
        <div class="TabPanelHeader">
           Add Note
        </div>
        
       <asp:TextBox ID="txtSortColumn" runat ="server" Visible ="false"  ></asp:TextBox>
<asp:TextBox ID="txtSortOrder" runat ="server" Visible ="false"  ></asp:TextBox>
        <asp:Panel ID="pnlMaster" runat="server">
            <div class="TableRow">
                <div class="TableFormLeble" style="padding-left: 0%">
                    <asp:Label ID="lblNoteType" runat="server" Text="Note Category"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:DropDownList ID="ddlNoteType" runat="server" CssClass="CommonDropDownList" ValidationGroup="NoteType"
                        Width="180px">
                    </asp:DropDownList>
                   
                </div>
            </div>
          
            <div class="TableRow">
                <div class="TableFormLeble" style="padding-left: 0%">
                    <asp:Label ID="lblNotes" runat="server" Text="Note"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" Rows="6" Width="350px" CssClass="CommonTextBox" ValidationGroup="NoteType"></asp:TextBox>
                    <span class="RequiredField">*</span>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormValidatorContent"  style =" margin-left :42%" >
                    <asp:RequiredFieldValidator ID="rfvNotes" runat="server" ControlToValidate="txtNotes"
                        ErrorMessage="Please enter Notes ." EnableViewState="False" Display="Dynamic"
                        ValidationGroup="Notes">
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="TableRow" style="text-align: center;">
                  <asp:Button ID="btnSubmitNotes" CssClass="CommonButton" runat="server" Text="Save" ValidationGroup="Notes" OnClick="btnSubmitNotes_Click" />
                
             </div>
        </asp:Panel>
        <br />
        <div class="TabPanelHeader">
        List of Notes 
        </div>
         <asp:Panel ID="pnlGrid" runat="server">
            <div class="GridContainer">
            <asp:ObjectDataSource ID="odsNotesList" runat="server" EnablePaging="false" SelectMethod="GetAllByCompanyID"
            TypeName="TPS360.Web.UI.CommonNoteDataSource" SortParameterName="sortExpression">
            <SelectParameters>
                <asp:Parameter Name="companyId" DefaultValue="0" />
            </SelectParameters>            
        </asp:ObjectDataSource>
                <asp:ListView ID="lsvCompanyNotes" runat="server" DataKeyNames="Id" OnItemDataBound="lsvCompanyNotes_ItemDataBound"
                    OnItemCommand="lsvCompanyNotes_ItemCommand" OnPreRender ="lsvCompanyNotes_PreRender" DataSourceID ="odsNotesList">
                    <LayoutTemplate>
                        <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                        <tr >
                        <th style="white-space: nowrap; width :125px !important">
                            <asp:LinkButton ID="btnDateTime" runat="server" ToolTip="Sort by create date" CommandName="Sort"
                                CommandArgument="[C].[CreateDate]" Text="Date & Time" />
                        </th>
                        <th style="white-space: nowrap; ">
                            <asp:LinkButton ID="btnManager" runat="server" CommandName="Sort" CommandArgument="[M].[FirstName]"
                                Text="Manager" />
                        </th>
                        <th style="white-space: nowrap; ">
                            <asp:LinkButton ID="btnNoteCategory" runat="server" CommandName="Sort" CommandArgument="[G].[Name]"
                                Text="Note Category" />
                        </th>
                        <th style="white-space:normal; "> Note
                    
                        </th>
                        <th style="text-align: center; " runat ="server" id="thAction" visible="false">
                            Action
                        </th>
                    </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                               <tr class="Pager">
                                                <td colspan="4">
                                                    <ucl:Pager ID="pagerControl_Requisition" runat="server" EnableViewState="true" NoOfRowsCookie="MemberCompanyNoteRowPerPage"/>
                                                </td>
                                            </tr>
                        </table>
                    </LayoutTemplate>
                    <EmptyDataTemplate>
                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                            <tr>
                                <td>
                                    No notes found.
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <ItemTemplate>
                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                            <td align="left">
                                <asp:Label ID="lblCreatedDate" runat="server"  />
                            </td>
                            <td align="left">
                               <asp:Label ID="lblCreatedBy" runat="server" Width ="80px" /> 
                            </td>
                            <td align="left">
                                <asp:Label ID="lblNoteType" runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblNote" runat="server" />
                            </td>
                            <td style="text-align: left; white-space: nowrap;" id ="tdAction" runat ="server" visible="false" >
                                <asp:ImageButton ID="btnEdit" SkinID="sknEditButton" runat="server" CommandName="EditItem"></asp:ImageButton>
                                <asp:ImageButton ID="btnDelete" SkinID="sknDeleteButton" runat="server" CommandName="DeleteItem" Visible="true"></asp:ImageButton>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </div>
            <br />
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>

