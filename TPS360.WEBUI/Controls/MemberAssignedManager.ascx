﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MemberAssignedManager.ascx.cs"
    Inherits="TPS360.Web.UI.cltMemberAssignedManager" %>
    
<div>
 
    <div class="TableRow" style="text-align: center">
        <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
        <asp:HiddenField ID="hfMemberId" runat="server" Value="0" />
        <asp:HiddenField ID="hfPrimaryManagerId" runat="server" Value="0" />
    </div>
    <div id="divAssndManager" runat ="server" >
    <div class="TabPanelHeader" >
          Assign Manager
    </div>
    <div class="TableRow" style="text-align:center">
        
            <asp:DropDownList EnableViewState="false" ID="ddlEmployee" Width="200px" CssClass="CommonDropDownList"
                runat="server">
            </asp:DropDownList>
            <asp:Button ID="btnAssign" CssClass="CommonButton" runat="server" Text="Add" ValidationGroup="AssignManager"
                OnClick="btnAssign_Click"/>
      
    </div>
    
    <div class="TableRow" >
        <div class="TableFormValidatorContent" style ="margin-left :38.5%">
            <asp:CompareValidator ID="cvEmployee" runat="server" ControlToValidate="ddlEmployee"
                Type="Integer" Operator="NotEqual" ValueToCompare="0" ErrorMessage="Please select manager."
                EnableViewState="False" Display="Dynamic" ValidationGroup="AssignManager"></asp:CompareValidator>
        </div>
    </div>
    </div>
    <div class="TabPanelHeader" >
          List of Assigned Managers
    </div>
    <div class="GridContainer">
        <asp:ListView ID="lsvAssignedManager" runat="server" DataKeyNames="Id" OnItemDataBound="lsvAssignedManager_ItemDataBound"
            OnItemCommand="lsvAssignedManager_ItemCommand" OnPreRender ="lsvAssignedManager_PreRender">
            <LayoutTemplate>
                <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                    <tr>
                        <th style="width: 35%; white-space: nowrap;">
                            Manager
                        </th>
                        <th style="width: 15%; text-align: center; white-space: nowrap;">
                            Date Assigned
                        </th>
                        <th style="width: 20%; text-align: center; white-space: nowrap;">
                            Primary Manager
                        </th>
                        <th id ="thUnAssign" style="text-align: center; white-space: nowrap; width: 10%;">
                            Un-Assign Manager
                        </th>
                    </tr>
                    <tr id="itemPlaceholder" runat="server">
                    </tr>
                </table>
            </LayoutTemplate>
            <EmptyDataTemplate>
                <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                    <tr>
                        <td style="color: Red">
                            No manager assigned.
                        </td>
                    </tr>
                </table>
            </EmptyDataTemplate>
            <ItemTemplate>
                <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                    <td>
                        <asp:Label  ID="lblManagerName" runat="server" />
                    </td>
                    <td style="text-align: center;">
                        <asp:Label  ID="lblAssignedDate" runat="server" />
                    </td>
                    <td style="white-space: nowrap; text-align: left;">
                        <asp:Label  ID="lblPrimaryManager" runat="server" />
                    </td>
                    <td id ="tdUnAssign" style="text-align: center;">
                        <asp:ImageButton ID="btnDelete" SkinID="sknDeleteButton" runat="server" CommandName="DeleteItem">
                        </asp:ImageButton>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
    </div>
    <br />
    <div class="TableRow" style="text-align: center">
        <asp:Button ID="btnUpdate" CssClass="CommonButton" runat="server" Text="Update" OnClick="btnUpdate_Click"
            CausesValidation="false" EnableViewState ="true" />
    </div>
</div>
