﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RequisitionNotes.ascx.cs" Inherits="RequisitionNotes" %>
<%@ Register Src ="~/Controls/PagerControl.ascx" TagName ="Pager" TagPrefix ="ucl" %>

<asp:HiddenField ID="hdnRowPerPageName" runat ="server" />
 <asp:HiddenField ID="hdnTotalRowCount" runat ="server" />
 <asp:HiddenField ID="hdnStartRowIndex" runat ="server" />
 <asp:HiddenField ID="hdnMaxRow" runat ="server" />
 <script>
   function setSelectedRowPerPage(ddlRowPerPage)
   {
      var hdnSelectedRowPerPage=document .getElementById ('<%= hdnSelectedRowPerPage.ClientID %>');
      if(hdnSelectedRowPerPage !=null && ddlRowPerPage !=null)
      {
        hdnSelectedRowPerPage .value=ddlRowPerPage .options[ddlRowPerPage .selectedIndex].value;
   
      }
   }
   function PageChanged(txtPageNumber)
   {
   var hdnPageNumber=document .getElementById ('<%=hdnPageNumber.ClientID %>');
   if(hdnPageNumber !=null && txtPageNumber !=null )
    hdnPageNumber .value=txtPageNumber .value;
   }
 </script>
 <asp:UpdatePanel ID="upNotes" runat ="server" >
 <ContentTemplate >
 
 <asp:HiddenField ID="hdnCurrentJobPostingId" runat ="server" />
 <div class="TableRow" style="text-align:left">
        <asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
    </div>

    <div class="TableRow">
        <div class="TabPanelHeader" style =" margin-bottom : 0px;">
            Add Note
        </div>
        <div class="TableFormContent">
            <asp:TextBox ID="txtAdditionalNote" runat="server" CssClass="CommonTextBox" TabIndex ="6" TextMode="MultiLine" Style="resize: none;
                        float: left" Rows="5" Width="655px" ValidationGroup ="NoteValidation" ></asp:TextBox>
            
        </div>
    </div>
   
    <div class ="TableRow">
        <div  class ="TableFormValidatorContent">
            <asp:RequiredFieldValidator ID ="rfvNotes" runat ="server" Display ="Dynamic"  ErrorMessage ="Please Add Notes." ControlToValidate ="txtAdditionalNote" ValidationGroup ="NoteValidation"></asp:RequiredFieldValidator> 
        </div>
    </div>
     <div class ="TableRow">
            <div class="TableFormLeble" style="width:20%">
            </div>
            <div class="TableFormContent" style="margin-left:45%">
                <asp:Button ID="btnSaveNote" CssClass="CommonButton" runat="server" Text="Save" TabIndex ="7" OnClick="btnSaveNote_Click"  ValidationGroup ="NoteValidation" />
                </div>
    </div>
    
   <asp:ObjectDataSource ID="odsReqNotes" runat="server" SelectMethod="GetPaged"
        TypeName="TPS360.Web.UI.RequisitionNotesDataSource" SelectCountMethod="GetListCount"
        EnablePaging="True" SortParameterName="sortExpression">
        <SelectParameters>
            <asp:Parameter Name="JobPostingId" DefaultValue="0" />
        </SelectParameters>
    </asp:ObjectDataSource>
    
    <div class="TableRow" style="width:100%">
       <div class="TableRow">
        <div class="TabPanelHeader" style=" margin-bottom : 5px;">
            Notes
        </div>                                     
  </div> 
   <asp:HiddenField ID="hdnSortColumn" runat ="server" />
<asp:HiddenField ID="hdnSortOrder" runat ="server" />
<asp:HiddenField ID="hdnSelectedRowPerPage" runat ="server" />
<asp:HiddenField ID="hdnPageNumber" runat ="server" />
        <div  style="text-align:left;width:100%; max-height : 270px; overflow :auto ;  float:left;">
       
            <asp:ListView ID="lsvAddedNote" runat="server"  DataKeyNames="Id"  OnItemDataBound="lsvAddedNote_ItemDataBound" OnPreRender ="lsvAddedNote_PreRender" EnableViewState ="true"
             OnItemCommand="lsvAddedNote_ItemCommand"  >
                <LayoutTemplate>
                    <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                        <tr>
                            <th>
                                <asp:LinkButton ID="btnDate" runat="server" Text="Date & Time" CommandName="Sort" CommandArgument="[C].[CreateDate]" />
                            </th>
                            <th>
                               <asp:LinkButton ID="btnAdded" runat="server" Text="Added By" CommandName="Sort" CommandArgument="[M].[FirstName]" />
                            </th>                            
                            <th> Note </th>
                        </tr>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        
                        
                        <tr class="Pager">
                            <td colspan="3">
                                <ucl:Pager id="Pager" runat="Server" NoOfRowsCookie="ReqNotes" />
                            </td>
                        </tr>
                    </table>
                </LayoutTemplate>
                <EmptyDataTemplate>
                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                        <tr>
                            <td>
                                No notes added.
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <ItemTemplate>
                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                        <td> <asp:Label ID="lblDateTime" runat="server" /> </td>
                        <td> <asp:Label ID="lblAddedBy" runat="server"  /> </td>
                        <td> <asp:Label ID="lblNote" runat="server" />     </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </div>
    </div>

</ContentTemplate>
 </asp:UpdatePanel>