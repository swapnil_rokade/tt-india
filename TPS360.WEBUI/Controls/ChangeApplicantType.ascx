﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ChangeApplicantType.ascx.cs" 
Inherits="TPS360.Web.UI.cltChangeApplicantType" %>
<div>

    <div class="TableRow" style="text-align: center">
        <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
        <asp:HiddenField ID="hfMemberId" runat="server" Value="0" />
        <asp:HiddenField ID="hfPrimaryManagerId" runat="server" Value="0" />
    </div>
    <div class="TableRow">
            <asp:DropDownList EnableViewState="false" ID="ddlEmployeementType" Width="200px" CssClass="CommonDropDownList"
                runat="server">
            </asp:DropDownList>
            <asp:Button ID="btnApplicantTypeSave" CssClass="CommonButton" runat="server" 
                Text="Save" ValidationGroup="ApplicantType" 
                onclick="btnApplicantTypeSave_Click"/>
    </div>
    <div class="TableRow">
        <div class="TableFormValidatorContent">
            <asp:CompareValidator ID="cvEmployee" runat="server" ControlToValidate="ddlEmployeementType"
                Type="Integer" Operator="NotEqual" ValueToCompare="0" ErrorMessage="Please select Candidate type."
                EnableViewState="False" Display="Dynamic" ValidationGroup="ApplicantType"></asp:CompareValidator>
        </div>
    </div>
</div>
