﻿<%------------------------------------------------------------------------------------------------------------------------------------
    FileName: Controls/AllConsultantTimeSheetList.ascx
    Created By: 
    Created On:
    Modification Log:
    -----------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date          Author           Modification
    -----------------------------------------------------------------------------------------------------------------------------------
    0.1            Jan-19-2010   Basavaraj Angadi Defect Id:12901;Changed Style attribute
---------------------------------------------------------------------------------------------------------------------------------------

--%>

<%@ control Language="C#" AutoEventWireup="true" CodeFile="ConfirmationWindow.ascx.cs"
    Inherits="TPS360.Web.UI.ConfirmationWindow" %>
     <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<script >
function hidePopup()
{

  $find('<%= mpeMsg.ClientID %>').hide(); 
  
}
</script>

        <asp:Button ID="btnD" runat="server" Text="" Style="display: none" Width="0" Height="0" />
        <asp:Button ID="btnD2" runat="server" Text="" Style="display: none" Width="0" Height="0" />
        <asp:Panel ID="pnlMsg" runat="server" CssClass="mp" style="display: none" Width="550px">
            <asp:Panel ID="pnlMsgHD" runat="server" CssClass="mpHd">
                &nbsp;Message
            </asp:Panel>
            <asp:GridView ID="grvMsg" runat="server" ShowHeader="false" Width="100%" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <table>
                                <tr>
                                    <td style =" border-bottom-style :none ">
                                        <asp:Image ID="imgErr" runat="server" ImageUrl="~/App_Themes/default/mpImgs/err.png"
                                            Visible=' <%# (((Message)Container.DataItem).MessageType == enmMessageType.Error) ? true : false %>' />
                                        <asp:Image ID="imgSuc" runat="server" ImageUrl="~/App_Themes/default/mpImgs/suc.png"
                                            Visible=' <%# (((Message)Container.DataItem).MessageType == enmMessageType.Success) ? true : false %>' />
                                        <asp:Image ID="imgAtt" runat="server" ImageUrl="~/App_Themes/default/mpImgs/att.png"
                                            Visible=' <%# (((Message)Container.DataItem).MessageType == enmMessageType.Attention) ? true : false %>' />
                                        <asp:Image ID="imgInf" runat="server" ImageUrl="~/App_Themes/default/mpImgs/inf.png"
                                            Visible=' <%# (((Message)Container.DataItem).MessageType == enmMessageType.Info) ? true : false %>' />
                                    </td>
                                    <td style =" border-bottom-style :none;   white-space :normal ">
                                        <%# Eval("MessageText")%>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <div class="mpClose">
                <asp:Button ID="btnOK" runat="server" Text="OK" CausesValidation="false" CssClass ="CommonButton" Width="60px"
                  />
                <asp:Button ID="btnPostOK" runat="server" Text="OK" CssClass ="CommonButton" CausesValidation="false" OnClick="btnPostOK_Click" OnClientClick ="hidePopup()"
                    Visible="false" Width="60px" />
<%--                <asp:Button ID="btnPostCancel" runat="server" Text="Cancel" CausesValidation ="true" OnClick ="btnPostCancel_Click" 
                     OnClientClick ="hidePopup()" Width="60px" CssClass ="CommonButton" />--%>
                     
    <%--                 <input type ="button" value ="Cancel" class ="CommonButton" causesvalidation ="true"  onclick ="hidePopup()" />--%>
     <asp:Button ID="btnPostCancel" runat="server" Text="Cancel" class ="CommonButton"
                    Width="60px" />
                     
                    <%-- <input type ="button" id ="BTN" value ="Cancel" class ="CommonButton" causesvalidation ="true"  onclick ="hidePopup()" />--%>
            </div>
        </asp:Panel>

        <ajaxToolkit:ModalPopupExtender  ID="mpeMsg" runat="server" TargetControlID="btnD" 
            PopupControlID="pnlMsg" BackgroundCssClass="mpBg" 
            DropShadow="true" OkControlID="btnOK"  CancelControlID="btnPostCancel"   >

        </ajaxToolkit:ModalPopupExtender>
