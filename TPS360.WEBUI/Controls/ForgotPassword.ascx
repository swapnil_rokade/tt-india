﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ForgotPassword.ascx.cs"
    Inherits="TPS360.Web.UI.ForgotPassword" %>

<script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>

    <script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

    <br />
    <br />
    <br />
    <div class="MidBodyBoxRow">
        <div class="MainBox" style="width:600px;height: 10px">
            <div class="MasterBoxContainer2">
                <div class="section-header">
                    Reset Forgotten Password</div>
                <div class="ChildBoxContainer2" style="height:175px;">
                    <div class="BoxContainer">
                        <div id="divEmail" runat="server">
                            <div class="TableHeading" style="height: 40px;margin-bottom:10px;">
                            <div class="alert alert-info">
                                Enter your email address to be sent password reset instructions.</div>
      </div>
                            <div style="width: 98%;">
                                <div class="TableRow">
                                    <div class="TableFormLeble ">
                                        <asp:Label ID="lblEmail" Text="Email Address:" runat="server"></asp:Label></div>
                                    <div class="TableFormContent">
                                        <asp:TextBox ID="txt_UserEmailName" runat="server" CssClass=" commontextbox " AutoCompleteType="Disabled "
                                            TabIndex="0" AutoComplete="OFF"> </asp:TextBox></div>
                                </div>
                                <div class=" TableFormValidatorContent" style="margin-left: 42%">
                                    <label id="lblAvailableuser" style="color: Red">
                                    </label>
                                    <asp:RegularExpressionValidator ID="revTxtMail" runat="server" ControlToValidate="txt_UserEmailName"
                                        Display="Dynamic" ErrorMessage="Please enter valid email address." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                        ValidationGroup="EmailValidation"></asp:RegularExpressionValidator>
                                    <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="txt_UserEmailName"
                                        EnableViewState="false" Display="Dynamic" SetFocusOnError="true" ErrorMessage="Email Address is required."
                                        ToolTip="Email Address is required." ValidationGroup="EmailValidation"></asp:RequiredFieldValidator>
                                </div>
                                <div class="TableRow">
                                    <div class="TableRow">
                                        <div class="TableFormLeble">
                                        </div>
                                        <div class="TableFormContent">
                                            <asp:Button ID="btnPasswordReset" runat="server" Text="Send Reset Email"
                                                OnClick="btnPasswordReset_Click" ValidationGroup="EmailValidation" TabIndex="2"
                                                UseSubmitBehavior="true" CssClass="btn" /></div>
                                    </div>
                                </div>
                                <div class="TableFormContent" style="text-align:center">
                                <a href="Login.aspx">← Back to Log In</a>
                                </div>
                            </div>
                        </div>
                        <div id="divContinue" runat="server" visible="false">
                            <div class="well">
                                <asp:Label ID="lblConfirmHeading" runat="server" Width="95%" Font-Bold="true"></asp:Label></div>
                            <div style="width: 98%;">
                                <div class="TableRow">
                                    <div class="TableFormLeble">
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:Button ID="btnContinue" runat="server" Text="Continue"
                                            OnClick="btnContinue_Click" TabIndex="3" UseSubmitBehavior="true" CssClass="btn" /></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HyperLink ID="lnkMail" runat="server" Visible="false"></asp:HyperLink>

    <script language="javascript" type="text/javascript">
        document.getElementById('"ctl00_cphHomeMaster_txt_UserEmailName').focus();
    </script>

