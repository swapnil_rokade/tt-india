﻿using System;
using System.Web.Security;

using TPS360.Common.Helper;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;
using System.Web.UI.WebControls;

namespace TPS360.Web.UI
{
    public partial class SmallPagerControl : BaseControl
    {
   
        protected void PagerCommand(object sender, DataPagerCommandEventArgs e)
        {
           
            DataPager pager = e.Item.Pager;
            int newSartRowIndex;

            switch (e.CommandName)
            {
                case "Next":
                    {
                        newSartRowIndex = pager.StartRowIndex + pager.MaximumRows >= pager.TotalRowCount ? pager.StartRowIndex : pager.StartRowIndex + pager.MaximumRows;
                        break;
                    }
                case "Previous":
                    {
                        newSartRowIndex = pager.StartRowIndex - pager.MaximumRows < 0 ? pager.StartRowIndex : pager.StartRowIndex - pager.MaximumRows;
                        break;
                    }
                case "Last":
                    {
                        newSartRowIndex = ((pager.TotalRowCount / pager.MaximumRows) * pager.MaximumRows) >= pager.TotalRowCount ? (((pager.TotalRowCount / pager.MaximumRows) - 1) * (pager.MaximumRows)) : ((pager.TotalRowCount / pager.MaximumRows) * pager.MaximumRows);
                        break;
                    }
                case "First":
                    {
                        newSartRowIndex = 0;
                        break;
                    }
                default:
                    {
                        newSartRowIndex = 0;
                        break;
                    }
            }

            e.NewMaximumRows = e.Item.Pager.MaximumRows;
            e.NewStartRowIndex = newSartRowIndex;
           
        }

        protected void CurrentPageChanged(object sender, EventArgs e)
        {

            TextBox txtCurrentPage = sender as TextBox;
            if (txtCurrentPage.Text.Trim() == string.Empty) txtCurrentPage.Text = "1";
            int selectedpage = 0;
            Int32.TryParse(txtCurrentPage.Text, out selectedpage);

            if (selectedpage > 0 && Convert.ToDouble((pager.TotalRowCount) / Convert.ToDouble(pager.PageSize)) > selectedpage - 1)
            {
                int startRowIndex = (int.Parse(txtCurrentPage.Text) - 1) * pager.MaximumRows;
                pager.SetPageProperties(startRowIndex, pager.MaximumRows, true);

            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Please enter valid page number", true);
            }
        }

        protected void ddlRowPerPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            TextBox txtCurrentPage = sender as TextBox;
            DropDownList ddlRowPerPage = sender as DropDownList;
            int startRowIndex = 0;
            pager.PageSize = Convert.ToInt32(ddlRowPerPage.SelectedValue);
            Response.Cookies[hdnRowPerPageName .Value].Value = pager.PageSize.ToString();
            pager.SetPageProperties(startRowIndex, Convert.ToInt32(ddlRowPerPage.SelectedValue), true);
           
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
    }
}