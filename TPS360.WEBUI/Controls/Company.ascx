﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: Company.ascx
    Description: This is the user control page used for company registration
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date             Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1                 Jan-30-2009      Kalyani P           Defect id: 9045; Commented company logo dispaly panel.
    0.2                 Apr-06-2009      Jagadish            Defect id: 10191; Restricted zip code text field to 6 characters.
    0.3                 July-2-2009      Gopala Swamy J      Defect id: 10732; Made correct validation for Zipcode
    0.4                 Apr-30-2010      Ganapati Bhat       Defect id: 12733; Commented lines to disable Toll free number extension
-------------------------------------------------------------------------------------------------------------------------------------------       
--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Company.ascx.cs" Inherits="TPS360.Web.UI.cltCompany" %>
<%@ Register Src="~/Controls/CountryStatePicker.ascx" TagName="CountryState" TagPrefix="ucl" %>
<%@ Register Assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.Web.UI.EditorControls" TagPrefix="ig" %>
<%@ Register assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.Web.UI.LayoutControls" tagprefix="ig" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/MultipleItemPicker.ascx" TagName="MultipleSelection"
    TagPrefix="ucl" %>

<script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

<script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>

<style type="text/css">
    .CalendarCSS
    {
        background-color: Gray;
        color: Blue;
        border-color: Maroon;
    }
    .custom-calendar .ajax__calendar_container
    {
        background-color: #ffc; /* pale yellow */
        border: solid 1px #666;
    }
    .custom-calendar .ajax__calendar_title
    {
        background-color: #cf9; /* pale green */
        height: 20px;
        color: #333;
    }
    .custom-calendar .ajax__calendar_prev, .custom-calendar .ajax__calendar_next
    {
        background-color: #aaa; /* darker gray */
        height: 20px;
        width: 20px;
    }
    .custom-calendar .ajax__calendar_today
    {
        background-color: #cff; /* pale blue */
        height: 20px;
    }
    .custom-calendar .ajax__calendar_days table thead tr td
    {
        background-color: #ff9; /* dark yellow */
        color: #333;
    }
    .custom-calendar .ajax__calendar_day
    {
        color: #333; /* normal day - darker gray color */
    }
    .custom-calendar .ajax__calendar_other .ajax__calendar_day
    {
        display: none;
        color: #666; /* day not actually in this month - lighter gray color */
    }
    .calendarDropdown
    {
        margin-left: -18px;
        margin-bottom: -3px;
    }
</style>

<script type="text/javascript">
function thisCountry_OnChange(countryId,StateId,HdnId)
{
var coun=document .getElementById (countryId );
var divein=document .getElementById ('ctl00_ctl00_cphHomeMaster_cphCompanyMaster_ctlCompany_divEINNumber');
if(coun.options[coun.selectedIndex].text=='United States')
  divein .style.display='';
  else  divein .style.display='none';
return Country_OnChange(countryId ,stateid ,HdnId );
}

Sys.Application.add_load(function() {
      $('input[rel=Numeric]').keypress(function (event){return allownumeric(event,$(this));});
     });
 
</script>

<div>
    <div class="TableRow" style="text-align: center">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>
    <asp:Panel ID="pnlEditor" runat="server" DefaultButton="btnSave">
        <div class="TabPanelHeader" id="divCompanyHeader" runat="server">
            Editor
        </div>
        <div class="FormLeftColumn" style="width: 55%;">
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label ID="lblCompanyName" runat="server" Text="Company Name"></asp:Label>:
                </div>
                <div class="TableFormContent" >
                    <asp:TextBox ID="txtCompanyName" runat="server" CssClass="CommonTextBox" EnableViewState="true" FocusOnInitialization="True"></asp:TextBox><span
                        class="RequiredField">*</span>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                    <asp:RequiredFieldValidator ID="rfvCompanyName" runat="server" ControlToValidate="txtCompanyName"
                        ValidationGroup="Company" ErrorMessage="Please enter company name." EnableViewState="False"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label ID="lblIndustryType" runat="server" Text="Industry Type"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:DropDownList ID="ddlIndustryType" CssClass="CommonDropDownList" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label ID="lblCompanySize" runat="server" Text="Company Size"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:DropDownList ID="ddlCompanySize" CssClass="CommonDropDownList" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="TableRow" id="divOwnerType" runat="server">
                <div class="TableFormLeble">
                    <asp:Label ID="lblOwnerType" runat="server" Text="Owner Type"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:DropDownList ID="ddlOwnerType" CssClass="CommonDropDownList" runat="server"
                        Width="155px">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label ID="lblOfficePhone" runat="server"  Text="Office Phone"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox ID="txtOfficePhone" runat="server" ValidationGroup="Company" CssClass="CommonTextBox"  MaxLength ="15" ></asp:TextBox>&nbsp;Ext.&nbsp;
                    <asp:TextBox ID="txtOfficePhoneExtension" ValidationGroup="Company" Width="30px" runat="server" CssClass="CommonTextBox"  MaxLength ="4" rel="Integer"></asp:TextBox>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                    <asp:RegularExpressionValidator ID="revOfficePhone" runat="server" Display="Dynamic"
                        ControlToValidate="txtOfficePhone" EnableViewState="false" ValidationGroup="Company" ErrorMessage="Please enter 0-9,(),+- only."
                        ValidationExpression="^[0-9'()+-]{1,20}$"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                    <asp:RegularExpressionValidator ID="revOfficePhoneExtension" runat="server" Display="Dynamic"
                        ControlToValidate="txtOfficePhoneExtension" EnableViewState="false" ErrorMessage="Please enter extension number in 2-4 digits."
                        ValidationGroup="Company" ValidationExpression="^[0-9]{2,4}$"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label ID="lblTollFreePhone" runat="server" Text="Toll Free Phone"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox ID="txtTollFreePhone" ValidationGroup="Company" runat="server" CssClass="CommonTextBox"  MaxLength ="15" ></asp:TextBox>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                   <%-- <asp:RegularExpressionValidator ID="revTollFreePhone" ValidationGroup="Company"  runat="server" Display="Dynamic"
                        ControlToValidate="txtTollFreePhone" EnableViewState="false" ErrorMessage="Please enter 0-9,(),+- only."
                        ValidationExpression="^([\+][0-9]{1,10}([ \-])?)?([\(]{1}[0-9]{1,5}[\)])?([0-9 \-]{1,32})((x|X)?[0-9]{2,4}?)$"></asp:RegularExpressionValidator>
                --%>  
                 <asp:RegularExpressionValidator ID="revTollFreePhone" ValidationGroup="Company"  runat="server" Display="Dynamic"
                        ControlToValidate="txtTollFreePhone" EnableViewState="false" ErrorMessage="Please enter 0-9,(),+- only."
                        ValidationExpression="^[0-9'()+-]{1,20}$"></asp:RegularExpressionValidator>
                      
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label ID="lblFaxNumber" runat="server" Text="Fax Number"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox ID="txtFaxNumber" runat="server" ValidationGroup="Company" CssClass="CommonTextBox"  MaxLength ="15"></asp:TextBox>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormValidatorContent"  style="margin-left: 42%">
                    <asp:RegularExpressionValidator ID="revFaxNumber" ValidationGroup="Company"  runat="server" Display="Dynamic"
                        ControlToValidate="txtFaxNumber" EnableViewState="false" ErrorMessage="Please enter 0-9,(),+- only."
                        ValidationExpression="^[0-9'()+-]{1,20}$"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label ID="lblEmail" runat="server" Text="Company Email"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox ID="txtPrimaryEmail" ValidationGroup="Company" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                    <asp:RegularExpressionValidator ID="rfvPrimaryEmail" ValidationGroup="Company" runat="server" ControlToValidate="txtPrimaryEmail"
                        EnableViewState="false" ErrorMessage="Please enter valid email address." Display="Dynamic"
                        ValidationExpression="\w+([-+.!#$%&*+-/=?^_`{|}~'\\\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    <%--^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$--%>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label ID="lblWebAddress" runat="server" Text="Web Address"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox ID="txtWebAddress" runat="server" ValidationGroup="Company" CssClass="CommonTextBox"></asp:TextBox>
                </div>
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:RegularExpressionValidator ID="rfvWebAddress" ValidationGroup="Company" runat="server" ErrorMessage="Please enter the valid web address."
                            ControlToValidate="txtWebAddress" EnableViewState="false" Display="Dynamic" ValidationExpression="^((ht|f)tp(s?)\:\/\/|~/|/)?([\w]+:\w+@)?([a-zA-Z]{1}([\w\-]+\.)+([\w]{2,5}))(:[\d]{1,5})?((/?\w+/)+|/?)(\w+\.[\w]{3,4})?((\?\w+=\w+)?(&\w+=\w+)*)?">
                        </asp:RegularExpressionValidator>
                    </div>
                </div>
            </div>
        </div>
        <div class="FormRightColumn" style="width: 45%;">
            <div class="TableRow">
                <div class="TableFormLeble" style="width: 30%">
                    <asp:Label ID="lblAddress1" runat="server" Text="Address 1"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox ID="txtAddress1" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormLeble" style="width: 30%">
                    <asp:Label ID="lblAddress2" runat="server" Text="Address 2"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox ID="txtAddress2" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormLeble" style="width: 30%">
                    <asp:Label ID="lblCity" runat="server" Text="City"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox ID="txtCity" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                </div>
            </div>
            <asp:UpdatePanel ID="upnCountryState" runat="server">
                <ContentTemplate>
                    <ucl:CountryState ID="uclCountryState" runat="server" FirstOption="Please Select"
                        TableFormLabel_Width="30%" />
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="TableRow">
                <div class="TableFormLeble" style="width: 30%">
                    <asp:Label ID="lblZip" runat="server" Text="Zip/Postal Code"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox ID="txtZip" Width="70px" runat="server" CssClass="CommonTextBox" MaxLength="6"></asp:TextBox>
                    <%--0.2--%>
                    <div class="TableFormValidatorContent" style="margin-left: 29%">
                        <asp:RegularExpressionValidator ID="revZIPCode" runat="server" ControlToValidate="txtZip"
                            Display="Dynamic" ErrorMessage="Please enter a valid zip code" ValidationExpression="^[0-9-]*$"
                            ValidationGroup="Company"></asp:RegularExpressionValidator><%--(\d{6}(-\d{4})?)|(\d{5}(-\d{4})?)--%>
                    </div>
                </div>
            </div>
            <%--<div class="TableRow">
                        <div class="TableFormLeble" style="width: 30%">
                            Contract Start Date:</div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtStartDate" runat="server" Width="100px">  
                            </asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="wdcStartDate" runat="server" TargetControlID="txtStartDate"
                                CssClass="custom-calendar" PopupButtonID="imgShow" EnableViewState="true">
                            </ajaxToolkit:CalendarExtender>
                            <asp:ImageButton ID="imgShow" runat="server" CssClass="calendarDropdown" ImageUrl="~/Images/downarrow.gif" />
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble" style="width: 30%">
                            Contract End Date:
                        </div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtEndDate" runat="server" Width="100px" FocusOnInitialization="True">  
                            </asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="wdcEndDate" runat="server" TargetControlID="txtEndDate"
                                CssClass="custom-calendar" PopupButtonID="imgShows" EnableViewState="true">
                            </ajaxToolkit:CalendarExtender>
                            <asp:ImageButton ID="imgShows" runat="server" CssClass="calendarDropdown" ImageUrl="~/Images/downarrow.gif" />
                        </div>
                    </div>
                    <div>
                    <asp:CompareValidator ID="cmbstartwithendcontract" runat="server" Operator="LessThanEqual"
                            Type="Date" ControlToValidate="wdcStartDate" ErrorMessage="Start date should not be after the 
                            End date"
                            Display="Dynamic" ControlToCompare="wdcEndDate" /> </div>--%>
            <div class="TableRow" id="dvStartDate" runat="server">                
                <div class="TableFormLeble" style="width: 30%">
                    <%--Added For LandT--%>
                    <asp:Label ID="lblStartDate" runat="server" Text="Contract Start Date"></asp:Label>:
                </div>
                <div class="TableFormContent" style="white-space: nowrap">
                    <div>
                        <div style="float: left" id="divDate" runat="server">
                            <%--<igsch:WebDateChooser ID="wdcStartDate" runat="server" NullDateLabel="" EnableKeyboardNavigation="True"
                                AutoCloseUp="true" TabIndex="4" Editable="True">
                                <CalendarLayout HideOtherMonthDays="True">
                                </CalendarLayout>
                            </igsch:WebDateChooser>--%>
                            <ig:WebDatePicker ID="wdcStartDate"  DropDownCalendarID="ddcStartDate" runat="server"></ig:WebDatePicker>
                            <ig:WebMonthCalendar  ID="ddcStartDate" AllowNull="true"  runat="server"></ig:WebMonthCalendar>
                        </div>
                    </div>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormValidatorContent" style="margin-left: 30%">
                    <asp:CompareValidator ID="cmpStartDate" runat="server" Operator="GreaterThanEqual"
                        Type="Date" ControlToValidate="wdcStartDate" ErrorMessage="Start date should not be before the current date"
                        Display="Dynamic" />
                </div>
            </div>
            <div class="TableRow" id="dvendDate" runat="server">                
                <div class="TableFormLeble" style="width: 30%">
                    <%--Added For LandT--%>
                    <asp:Label ID="lblEndDate" runat="server" Text="Contract End Date"></asp:Label>:
                </div>
                <div class="TableFormContent" style="white-space: nowrap">
                    <div>
                        <div style="float: left" id="div1" runat="server">
                          <%--  <igsch:WebDateChooser ID="wdcEndDate" runat="server" NullDateLabel="" EnableKeyboardNavigation="True"
                                AutoCloseUp="true" TabIndex="4" Editable="True">
                                <CalendarLayout HideOtherMonthDays="True">
                                </CalendarLayout>
                            </igsch:WebDateChooser>--%>
                            <ig:WebDatePicker ID="wdcEndDate"  DropDownCalendarID="ddcEndDate" runat="server"></ig:WebDatePicker>
                                        <ig:WebMonthCalendar  ID="ddcEndDate" AllowNull="true"  runat="server"></ig:WebMonthCalendar> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormValidatorContent" style="margin-left: 30%">
                    <asp:CompareValidator ID="cmpEndDate" runat="server" Operator="GreaterThanEqual"
                        Type="Date" ControlToValidate="wdcEndDate" ErrorMessage="End date should not be before the current date"
                        Display="Dynamic" />
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormValidatorContent" style="margin-left: 30%">
                    <asp:CompareValidator ID="cmbstartwithendcontract" runat="server" Operator="LessThanEqual"
                        Type="Date" ControlToValidate="wdcStartDate" ErrorMessage="Start date should not be after the End date"
                        Display="Dynamic" ControlToCompare="wdcEndDate" />
                </div>
            </div>
            <div class="TableRow" id="dvservicefee" runat="server">
                <div class="TableFormLeble" style="width: 30%">
                    <asp:Label ID="lblServiceFee" runat="server" Text="Service Fee:"></asp:Label>
                </div>
                <div class="TableFormContent">
                    <asp:TextBox ID="txtServiceFee" runat="server" CssClass="CommonTextBox" rel="Numeric"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="TableRow">
            <div class="TableFormLeble" style="width: 23%">
                <asp:Label ID="lblCompanyInformation" runat="server" Text="About the Account"></asp:Label>:
            </div>
            <div class="TableFormContent">
                <asp:TextBox ID="txtCompanyInformation" TextMode="MultiLine" Rows="4" Width="75%"
                    runat="server" CssClass="CommonTextBox"></asp:TextBox>
            </div>
        </div>
        <div class="TableRow" style="text-align: center; padding-top: 8px;">
            <asp:Button ID="btnSave" CssClass="CommonButton" runat="server" Text="Save" OnClick="btnSave_Click"
                ValidationGroup="Company" />
        </div>
    </asp:Panel>
</div>
