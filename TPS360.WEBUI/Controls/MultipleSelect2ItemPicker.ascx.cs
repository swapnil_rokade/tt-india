﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MultipleSelect2ItemPicker.ascx.cs
    Description: 
    Created By: pravin khot (from dispark)
    Created On: 26/Feb/2016
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Web.Security;
using System.Text.RegularExpressions;
using System.Linq;
using System.Web.UI;
namespace TPS360.Web.UI
{


    public partial class MultipleSelect2ItemPicker : BaseControl
    {
        public ListBox ListItem
        {
            get
            {
                return ddlItem;
            }
        }

        public string SelectedItems
        {
            get
            {
                //string value = "";
                //foreach (ListItem item in ddlItem.Items)
                //{
                //    if (item.Selected)
                //    {
                //        if (value != string.Empty) value += ",";
                //        value += item.Value;
                //    }
                //}

                //return value;
                return hdnSelectedListItems.Value;
            }
            set
            {
                char [] delim={','};
                string[] list = value.Split(delim, StringSplitOptions.RemoveEmptyEntries);
                foreach (ListItem item in ddlItem.Items)
                {
                    if (list.Contains(item.Value.Trim()))
                        item.Selected = true;
                     
                }
            }
        }
        private bool _Enabled = true;
        public bool Enabled
        {
            set
            {
                _Enabled = value;
                ddlItem.Enabled = value;// ddlCountry.Enabled = value;
              
            }
        }

        public object DataSource
        {
            set
            {
                ddlItem.DataSource = value ;
            }
        }
        public void DataBind()
        {
            ddlItem.DataBind();
        }
        public string DataValueField
        {
            set
            {
                ddlItem.DataValueField = value;
            }
        }

        public string DataTextField
        {
            set
            {
                ddlItem.DataTextField = value;
            }
        }
       

        private string _FirstOption;
        public string FirstOption
        {
            set
            {
                _FirstOption = value;

            }
            get { return _FirstOption; }
        }

      
               
  


        protected void Page_Load(object sender,EventArgs e)
        {
        }

       
     
     

        
    }
    
}