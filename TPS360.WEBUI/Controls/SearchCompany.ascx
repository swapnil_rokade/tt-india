﻿
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SearchCompany.ascx.cs"
    Inherits="TPS360.Web.UI.SearchCompany" %>
    <%@ Register Src ="~/Controls/CountryStatePicker.ascx" TagName ="CountryState" TagPrefix ="ucl" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/DateRangePicker.ascx" TagName = "DateRangePicker" TagPrefix ="ucl" %>
<script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>
<script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>
<script>
document.body.onclick = function(e)
  {
  $('#<%= pnlSearchBoxContent.ClientID %>').css({'overflow-y':'','overflow-x':''});
  $('#<%= pnlSearchBoxContent.ClientID %> div:first').css({'overflow-y':'','overflow-x':''});
  }

</script>
<asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
<asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>
<asp:TextBox ID="SortColumn" runat="server" EnableViewState="true" Visible="false" />
<asp:TextBox ID="SortOrder" runat="server" EnableViewState="true" Visible="false" />
<asp:ObjectDataSource ID="odsCompanyList" runat="server" SelectMethod="GetPaged"
    OnSelecting="odsCompanyList_Selecting" TypeName="TPS360.Web.UI.CompanyDataSource"
    SelectCountMethod="GetListCount" EnablePaging="True" SortParameterName="sortExpression">
    <SelectParameters>
        <asp:ControlParameter ControlID="txtName" Name="companyName" PropertyName="Text"
            Type="String" />
        <asp:ControlParameter ControlID="ddlIndustryType" Name="industryType" PropertyName="SelectedValue"
            Type="String" />
        <asp:ControlParameter ControlID="uclCountryState" Name="country" PropertyName="SelectedCountryId"
            Type="String" />
        <asp:ControlParameter ControlID="uclCountryState" Name="state" PropertyName="SelectedStateId"
            Type="String" />
        <asp:ControlParameter ControlID="txtContactName" Name="companyContact" PropertyName="Text"
            Type="String" />
        <asp:ControlParameter ControlID="txtCity" Name="city" PropertyName="Text" Type="String" />
        <asp:ControlParameter ControlID="dtCreateDate" Name="fromCreateDate" PropertyName="StartDate"
            Type="String" />
        <asp:ControlParameter ControlID="dtCreateDate" Name="toCreateDate" PropertyName="EndDate"
            Type="String" />
        <asp:ControlParameter ControlID="dtUpdateDate" Name="fromUpdateDate" PropertyName="StartDate"
            Type="String" />
        <asp:ControlParameter ControlID="dtUpdateDate" Name="toUpdateDate" PropertyName="EndDate"
            Type="String" />
        <asp:Parameter Name="SortOrder" DefaultValue="asc" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:UpdatePanel ID="updPanelCompany" runat="server">
    <ContentTemplate>
        <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
        <asp:Panel ID="pnlSearchBoxBody" runat="server" CssClass="well well-small nomargin">
            <asp:Panel ID="pnlSearchRegion" runat="server">
                <asp:CollapsiblePanelExtender ID="cpeExperience" runat="server" TargetControlID="pnlSearchBoxContent"
                    ExpandControlID="pnlSearchBoxHeader" CollapseControlID="pnlSearchBoxHeader" CollapsedImage="~/Images/expand-plus.png"
                    ExpandedImage="~/Images/collapse-minus.png" Collapsed="true" ImageControlID="imgSearchBoxToggle"
                    SuppressPostBack="True" >
                </asp:CollapsiblePanelExtender>
                <asp:Panel ID="pnlSearchBoxHeader" runat="server">
                    <div class="SearchBoxContainer">
                        <div class="SearchTitleContainer">
                            <div class="ArrowContainer">
                                <asp:ImageButton ID="imgSearchBoxToggle" runat="server" ImageUrl="~/Images/expand-plus.png"
                                    AlternateText="collapse" />
                            </div>
                            <div class="TitleContainer">
                                Filter Options
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlSearchBoxContent" runat="server"  CssClass ="filter-section" DefaultButton="btnSearch"
                    Height="0">
                    <div class="TableRow spacer" style="">
                        <div class="FormLeftColumn" style="width: 56%">
                            <div class="TableRow" style="white-space: nowrap">
                                <div class="TableFormLeble" style="width: 27%;">
                                    <asp:Label EnableViewState="true" ID="lblcompanyName" runat="server" Text="Account Name"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:TextBox ID="txtName" runat="server" autocomplete="off" CssClass="SearchTextbox"
                                        Width="130" />
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble" style="width: 27%;">
                                    <asp:Label EnableViewState="false" ID="Label7" runat="server" Text="City"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:TextBox ID="txtCity" runat="server" autocomplete="off" CssClass="SearchTextbox"
                                        Width="130" />
                                </div>
                            </div>
                             <ucl:CountryState ID="uclCountryState" runat ="server" FirstOption="All" ApplyDefaultSiteSetting ="False" TableFormLabel_Width ="27%" />
                        </div>
                        <div class="FormRightColumn" style="width: 43%">
                            <div class="TableRow">
                                <div class="TableFormLeble" style="width: 30%;">
                                    <asp:Label EnableViewState="false" ID="Label15" runat="server" Text="Contact Name"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:TextBox ID="txtContactName" runat="server" autocomplete="off" CssClass="SearchTextbox"
                                        Width="130" />
                                </div>
                            </div>
                            <div class="TableRow" style="white-space: nowrap">
                                <div class="TableFormLeble" style="width: 30%;">
                                    <asp:Label EnableViewState="false" ID="Label6" runat="server" Text="Industry Type"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:DropDownList ID="ddlIndustryType" CssClass="CommonDropDownList" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <asp:Panel ID="pnlHideSearchByEmployee" runat="server">
                                <div class="TableRow" style="white-space: nowrap">
                                    <div class="TableFormLeble" style="width: 30%;">
                                        <asp:Label EnableViewState="false" ID="Label8" runat="server" Text="By User"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:DropDownList ID="ddlEmployee" CssClass="CommonDropDownList" runat="server" EnableViewState="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </asp:Panel>
                            <div class="TableRow" style="white-space: nowrap">
                                <div class="TableFormLeble" style="width: 30%;">
                                    <asp:Label EnableViewState="false" ID="Label11" runat="server" Text="Date Created From"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                      <ucl:DateRangePicker ID="dtCreateDate" runat ="server"  />
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble" style="width: 30%;">
                                    <asp:Label EnableViewState="false" ID="Label10" runat="server" Text="Date Updated From"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                          <ucl:DateRangePicker ID="dtUpdateDate" runat ="server"  />
                                </div>
                            </div>
                        </div>
                        <div class="TableRow" style="text-align: center; padding-top: 5px; padding-bottom: 5px">
                            <asp:LinkButton ID="btnSearch" runat="server" Text="Search" AlternateText="Search" CssClass="btn btn-primary"
                                OnClick="btnSearch_Click" CausesValidation="false" ><i class="icon-search icon-white"></i> Search</asp:LinkButton>
                            <asp:Button ID="btnClear" runat="server" Text="Clear" AlternateText="Clear" CssClass="btn"
                                OnClick="btnClear_Click" EnableViewState="false" CausesValidation="false" />
                        </div>
                    </div>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
        <br />
        <asp:Panel ID="pnlGridRegion" runat="server">
            <div class="GridContainer" style="overflow: auto">
                <asp:ListView ID="lsvCompany" runat="server" DataKeyNames="Id" OnItemDataBound="lsvCompany_ItemDataBound"
                    OnItemCommand="lsvCompany_ItemCommand" OnPreRender="lsvCompany_PreRender">
                    <LayoutTemplate>
                        <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                            <tr>
                               
                                <th>
                                    <asp:LinkButton ID="btnCompanyName" runat="server" CommandName="Sort" CommandArgument="[C].[CompanyName]"
                                        Text="Account Name" />
                                </th>
                                <th>Location
                                </th>
                       
                                <th >
                                    Office Phone
                                </th>
                                <th >
                                    <asp:LinkButton ID="btnPrimaryContact" runat="server" CommandName="Sort" CommandArgument="[CC].[FirstName]"
                                        Text="Primary Contact" ToolTip="Sort By Primary Contact"></asp:LinkButton>
                                </th>
                              
                                <th >
                                    <asp:LinkButton ID="btnEmail" runat="server" CommandName="Sort" ToolTip="Sort By Email" CommandArgument="[CC].[Email]"
                                        Text="Email"></asp:LinkButton>
                                </th>
                                <th id="thAction" runat="server" style="text-align: center; white-space: nowrap;
                                    width: 55px !important;">
                                    Action
                                </th>
                            </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                            <tr class="Pager">
                                <td colspan="6" id="tdPager" runat="server">
                                    <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <EmptyDataTemplate>
                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                            <tr>
                                <td>
                                    No data found.
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <ItemTemplate>
                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                           
                            <td>
                                <asp:HyperLink ID="lnkCompanyName" runat="server"  />
                            </td>
                            <td>
                                <asp:Label ID="lblLocation" runat="server"  />
                            </td>
                          
                            <td>
                                <asp:Label ID="lblOfficePhone" runat="server"/>
                            </td>
                            <td>
                                <asp:Label ID="lblPrimaryContact" runat="server"  />
                            </td>
                        
                            <td>
                                <asp:LinkButton ID="lnkPrimaryEmail" runat="server"></asp:LinkButton>
                            </td>
                            <td  id="tdAction"   runat="server" style="text-align: center; white-space: nowrap;
                                    width: 60px !important;">
                                <asp:ImageButton ID="btnEdit" SkinID="sknEditButton" runat="server" CommandName="EditItem"
                                    CausesValidation="false"></asp:ImageButton>
                                <asp:ImageButton ID="btnDelete" SkinID="sknDeleteButton" runat="server" CommandName="DeleteItem"
                                    Visible="true"></asp:ImageButton>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
