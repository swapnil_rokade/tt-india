﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI;
using TPS360.Common.Shared;
using System.Collections.Generic;
using TPS360.Web.UI.Helper;
using TPS360.Common.Helper;
public partial class RequisitionAssignedManagers : RequisitionBaseControl 
{

    public JobPosting Job_Posting
    {
        get;
        set;
    }
    private int _jobpostingid;
    public int JobPostingId
    {
        set
        {
            _jobpostingid = value;
            hdnJobPostingId.Value = value.ToString();
           Job_Posting =  Facade.GetJobPostingById(JobPostingId);
           if (Job_Posting.CreatorId == CurrentMember.Id)
            {
                MiscUtil.PopulateMemberListByRole(lstRecruiters, ContextConstants.ROLE_EMPLOYEE, Facade);
                lstRecruiters.Items.RemoveAt(0);
                divRecruiters.Visible = true;
            }
            else divRecruiters.Visible = false;
            PopulateHiringTeamMembers();

        }
        get
        {
           
                return Convert.ToInt32(hdnJobPostingId.Value == "" ? "0" : hdnJobPostingId.Value);
            
        }
    }
  
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack && JobPostingId >0) Job_Posting = Facade.GetJobPostingById(JobPostingId);
        if (!IsPostBack)
        {
            Label lbModalTitle = (Label)this.Page.Master.FindControl("lbModalTitle");
            if (lbModalTitle != null) lbModalTitle.Text = Job_Posting.JobTitle + " - Assigned Team";
        }
      
    }

    protected void btnAddRecruiters_Click(object sender, EventArgs e)
    {
        AddSelectedMemberToJobPostingHiringTeam(lstRecruiters, EmployeeType.Internal);
        PopulateHiringTeamMembers();
        ScriptManager.RegisterClientScriptBlock(lblMessage , typeof(MiscUtil), "RefreshCount", "parent.RefreshList();", true);
      //  AjaxControlToolkit.ModalPopupExtender mpeManagers = null;
        //    if(Request .Url .AbsoluteUri .ToLower ().Contains ("hiringmatrix.aspx"))
        //        mpeManagers = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclHiringMatrix").FindControl("mpeManagers");
        //    else mpeManagers = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("uclRequisition").FindControl("mpeManagers");
        //if (mpeManagers != null) mpeManagers.Show();
    }
    protected void lsvAssignedTeam_PreRender(object sender, EventArgs e)
    {
        HtmlTableCell thAction = (HtmlTableCell)lsvAssignedTeam.FindControl("thAction");
        if (thAction != null)
        {
            thAction.Visible = Job_Posting.CreatorId == CurrentMember.Id;
        }
    }
    protected void lsvAssignedTeam_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
        {
            JobPostingHiringTeam jobPostingHiringTeam = ((ListViewDataItem)e.Item).DataItem as JobPostingHiringTeam;

            if (jobPostingHiringTeam != null)
            {
                Label lblAssignedDate = (Label)e.Item.FindControl("lblAssignedDate");
                Label lblMemberName = (Label)e.Item.FindControl("lblMemberName");
                Label lblMemberEmail = (Label)e.Item.FindControl("lblMemberEmail");

                ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");

                lblAssignedDate.Text = jobPostingHiringTeam.CreateDate.ToShortDateString();
                lblMemberName.Text = jobPostingHiringTeam.Name;
                lblMemberEmail.Text  = jobPostingHiringTeam.PrimaryEmail;
              
                HtmlTableCell tdAction = (HtmlTableCell)e.Item.FindControl("tdAction");

                if (Job_Posting.CreatorId != CurrentMember.Id)
                {
                    tdAction.Visible = false;
                }
                else
                {

                    if (jobPostingHiringTeam.MemberId == Job_Posting.CreatorId)
                        btnDelete.Visible = false;
                    else
                        btnDelete.Visible = true;
                    btnDelete.OnClientClick = "return ConfirmDelete('Hiring Team Member')";
                    btnDelete.CommandArgument = StringHelper.Convert(jobPostingHiringTeam.Id);
                }
                
            }
        }
    }

    protected void lsvAssignedTeam_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        int id;

        int.TryParse(e.CommandArgument.ToString(), out id);

        if (id > 0)
        {
            if (string.Equals(e.CommandName, "DeleteItem"))
            {
                try
                {
                    if (Facade.DeleteJobPostingHiringTeamById(id))
                    {
                        PopulateHiringTeamMembers();
                        MiscUtil.ShowMessage(lblMessage, "Hiring team member has been successfully deleted.", false);
                        ScriptManager.RegisterClientScriptBlock(lblMessage, typeof(MiscUtil), "RefreshCount", "parent.RefreshList();", true);
                    }
                }
                catch (ArgumentException ex)
                {
                    MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                }
            }
        }
       
    }
    private void PopulateHiringTeamMembers()
    {

        lsvAssignedTeam.DataSource = Facade.GetAllJobPostingHiringTeamByJobPostingId(JobPostingId );
        lsvAssignedTeam.DataBind();
    }

    private void AddSelectedMemberToJobPostingHiringTeam(ListBox listBox, EmployeeType employeeType)
    {
        bool status = false;
        int countSelected = 0;
        int countAdded = 0;
        int memberId = 0;
        string message = string.Empty;
        //if (listBox.SelectedIndex > 0) 
        {
            for (int i = 0; i <= listBox.Items.Count - 1; i++)
            {
                if (listBox.Items[i].Selected)
                {
                    countSelected++;
                    Int32.TryParse(listBox.Items[i].Value, out memberId);
                    if (memberId > 0)
                    {
                        JobPostingHiringTeam jobPostingHiringTeam = Facade.GetJobPostingHiringTeamByMemberId(memberId, JobPostingId  );

                        if (jobPostingHiringTeam == null)
                        {
                           
                            jobPostingHiringTeam = new JobPostingHiringTeam();
                            jobPostingHiringTeam.EmployeeType = employeeType.ToString();
                            jobPostingHiringTeam.JobPostingId = JobPostingId  ;
                            jobPostingHiringTeam.MemberId = memberId;
                            jobPostingHiringTeam.CreatorId = base.CurrentMember.Id;
                            Facade.AddJobPostingHiringTeam(jobPostingHiringTeam);
                            countAdded++;
                        }
                    }
                }
            }
            if (countSelected > 0)
            {
                if (countAdded > 0)
                {
                    int countNotAdded = countSelected - countAdded;
                    if (countNotAdded == 0)
                    {
                        MiscUtil.EventLogForRequisition(EventLogForRequisition.RequisitionAssigned, JobPostingId, CurrentMember.Id, Facade);

                        message = "Successfully added all the selected members.";
                        status = false ;
                    }
                    else
                    {
                        MiscUtil.EventLogForRequisition(EventLogForRequisition.RequisitionAssigned, JobPostingId, CurrentMember.Id, Facade);

                        message = "Successfully added " + countAdded + " of " + countSelected + " selected members.</br>The remaining members are already in the list</b></font>";
                        status = false;
                    }
                }
                else
                {
                    message = "The selected member(s) are already in the list";
                    status = true;
                }
            }
            else
            {
                message = "Select at least one member to add.";
                status = true;
            }
        }
        // else
        // {
        //     message = "Please Select a member to add";
        //  }

        MiscUtil.ShowMessage(lblMessage, message, status );
    }

}
