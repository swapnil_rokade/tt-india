﻿<%-- -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CareerRequisitionList.aspx
    Description: using carrer page
    Created By: pravin khot
    Created On:  17/Jan/2016
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date           Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
  
-------------------------------------------------------------------------------------------------------------------------------------------       

--%>

<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CareerRequisitionList.ascx.cs"
    Inherits="TPS360.Web.UI.CareerRequisitionList" %>
<%@ Register Src="~/Controls/DateRangePicker.ascx" TagName="DateRangePicker" TagPrefix="ucl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/ModalTemplate.ascx" TagName="Template" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/RequisitionAssignedManagers.ascx" TagName="Managers"
    TagPrefix="ucl" %>
<%@ Register Src="~/Controls/CountryStatePicker.ascx" TagName="CountryState" TagPrefix="ucl" %>
<%@ Register Src ="~/Controls/MultipleItemPicker.ascx" TagName ="MultipleSelection" TagPrefix="ucl" %>
<script src="../js/SearchCandidateSub.js" type="text/javascript"></script>

<%@ Register Src="~/Controls/ConfirmationWindow.ascx" TagName="Confirm" TagPrefix="ucl" %>
<script src="../js/CompanyRequisition.js" type ="text/javascript" ></script>
<%@ Register Assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.Web.UI.EditorControls" TagPrefix="ig" %>
<%@ Register assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.Web.UI.LayoutControls" tagprefix="ig" %>

<style>
    .dropdown-menu:after
    {
        border-bottom: 7px solid  #FFFFFF;
        border-left: 7px solid transparent;
        border-right: 7px solid transparent;
        content: "";
        display: inline-block;
        left: 80%;
        position: absolute;
        top: -6px;
    }
    .dropdown-menu::before
    {
        position: absolute;
        top: -7px;
        left: 80%;
        display: inline-block;
        border-right: 7px solid transparent;
        border-bottom: 7px solid #CCC;
        border-left: 7px solid transparent;
        border-bottom-color: rgba(0, 0, 0, 0.2);
        content: '';
    }
    .pnlHide
    {
    }
    .pnlHide:first-child
    {
        overflow: hidden;
    }
</style>

<script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

<script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>

<script type="text/javascript">
document.body.onclick = function(e)
  {
  $('#ctl00_cphHomeMaster_uclRequisition_pnlSearchBoxContent').css({'overflow-y':'inherit','overflow-x':'inherit'});
  $('#ctl00_cphHomeMaster_uclRequisition_pnlSearchBoxContent div:first').css({'overflow-y':'inherit','overflow-x':'inherit'});
  }
    //To Keep the scroll bar in the Exact Place
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoadedReq);
    function PageLoadedReq(sender, args) {
        try {
        var hdnScroll = document.getElementById('ctl00_cphHomeMaster_uclRequisition_hdnScrollPos');
        var bigDiv = document.getElementById('bigDiv');
        bigDiv.scrollLeft = hdnScroll.value;
        var background = $find("mpeModal")._backgroundElement;
        background.onclick = function()
            {
                CloseModal(); 
            }
        }
        catch (e) {
        }
    }
    function Redirect(a) {       
        window.location = a; 
    }
    function onShownManagers() {
        var background = $find("MPEManagers")._backgroundElement;
        background.onclick = function() { $find("MPEManagers").hide(); }
    }
    function SetScrollPosition() {
        var hdnScroll = document.getElementById('ctl00_cphHomeMaster_uclRequisition_hdnScrollPos');
        var bigDiv = document.getElementById('bigDiv');
        hdnScroll.value = bigDiv.scrollLeft;
    }
  
    
    function RefreshList()
    { 
        var hdnDoPostPack=document .getElementById ('<%=hdnDoPostPack.ClientID %>');
        hdnDoPostPack .value="1";
        __doPostBack('ctl00_cphHomeMaster_uclRequisition_upcandidateList',''); return false ;
    }
    $('#ctl00_cphHomeMaster_uclRequisition_lsvJobPosting_lnkNoOfOpening').mouseover(function() {
        //$('#ctl00_cphHomeMaster_uclRequisition_lsvJobPosting_lnkNoOfOpening').append('<div>Handler for .mouseover() called.</div>');
        console.log('asdsaweqw');
        alert('asdasd');
    });
    
     


  function ValidateRequisition(source, args)
      {
        alert ('d');
       
      }    
      
    function HotList_Selected(source, eventArgs) {
        hdnSelectedHotListValue = document.getElementById("ctl00_cphHomeMaster_ctrlEntireInternalDatabase_hdnSelectedHotListValue");
        hdnSelectedHotListText = document.getElementById("ctl00_cphHomeMaster_ctrlEntireInternalDatabase_hdnSelectedHot_ListText");
        hdnSelectedHotListText.value = eventArgs.get_text();
        hdnSelectedHotListValue.value = eventArgs.get_value();

    }
   
    var n = 10;
    function monitorClick(e) {
        try {
            var evt = (e) ? e : event;

            var theElem = (evt.srcElement) ? evt.srcElement : evt.target;

            while (theElem != null) {
                if (!$('#'+theElem .id).hasClass('active-result') && theElem.id.indexOf('spanTitle') &&  theElem.id.indexOf('txtSearch')&& theElem.id.indexOf('refReq') < 0 && theElem.id.indexOf('refHot') < 0 && theElem.id.indexOf('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_ddlRequisition') && theElem.id.indexOf('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_txtRequisition') && theElem.id.indexOf('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_ddlJobSeekerGroup') && theElem.id.indexOf('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_TxtHotList') && theElem.id.indexOf('btnAddToHotList') && theElem.id.indexOf('list') && theElem.id.indexOf('icon') && n != 0) {
                    n = 10;
                    var divReqs = document.getElementById('divReqs');
                    if (divReqs != null) {
                        $(divReqs).closest('.btn-group').removeClass('open');
                        divReqs.style.display = "none";
                    }
                    var divHotListdiv = document.getElementById('divHotListdiv');
                    if (divHotListdiv != null) {
                        $(divHotListdiv).closest('.btn-group').removeClass('open');
                        divHotListdiv.style.display = "none";
                    }
                    return true;
                }
                else {
                    n = 10;
                    return true;
                }
                theElem = theElem.offsetParent;
            }
        }
        catch (e) {
        } n = 10;
        return true;
    }
    document.onclick = monitorClick;
    function DropdownChange(divId) {
        n = 0;
        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) reqDiv.style.display = "";
    }
    function HideContextMenu(divId) {
        if (n == 0) {
            var reqDiv = document.getElementById(divId);
        }
        if (reqDiv != null) reqDiv.style.display = "none";
    }
    function DropdownClicked(divId) {
        n = 0;
        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) reqDiv.style.display = "";
    }

    function ShowDiv(divId) {
        n = 10;
        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) reqDiv.style.display = "";
    }
    function ShowReqDiv(divId) {

$('.btn-group').removeClass('open');

        var a = document.getElementById("divReqs");
        var b = document.getElementById("divHotListdiv");
        if (a != null) a.style.display = "none";
        if (b != null) b.style.display = "none";

        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) {
            $(reqDiv).closest('.btn-group').addClass('open');
            reqDiv.style.display = "";
            reqDiv.parentElement.className = "btn-group open";
        }
        return false;
    }
    function ShowOrHide(v, img) {
        var tdDetail = document.getElementById(v);
        var image = document.getElementById(img);
        if (tdDetail.style.display == "none") {
            tdDetail.style.display = "";
            image.src = "../Images/collapse-minus.png";
        }
        else {
            tdDetail.style.display = "none"; image.src = "../Images/expand-plus.png";
        }

    }

    //******************* Old comment by pravin khot on 19/Jan/2016*******************
//    function ShowOrHideRow(v, img, id, isKeywordPresent) {
//       
//        var tdDetail = document.getElementById(v);    
//       
//        var image = document.getElementById(img);
//        var country = document.getElementById("ctl00_cphHomeMaster_ctrlEntireInternalDatabase_hdnSiteSettingCountry").value;
//        var SearchStringName = document.getElementById("ctl00_cphHomeMaster_ctrlEntireInternalDatabase_hdnHighlightName").value;

//        if (tdDetail.style.display == "none") {
//            // alert(SearchStringName);
//            GetCandidateSearchSub(id, v, country, SearchStringName, isKeywordPresent);
//            tdDetail.style.display = "";
//            image.src = "../Images/collapse-minus.png";
//        }
//        else {
//            tdDetail.style.display = "none"; image.src = "../Images/expand-plus.png";
//        }

//    }

    //*****************************End******************************************

    //*******************New function add by pravin khot on 19/Jan/2016*******************
    function ShowOrHideRow(v, id, isKeywordPresent) {
     
        var tdDetail = document.getElementById(v);
       
      
        if (tdDetail.style.display == "none") {
           
            GetCareerJobSearchSub(id, v, isKeywordPresent);

            tdDetail.style.display = "";
             
        }
        else {
            tdDetail.style.display = "none";
        }
       

    }
    //***************************End*******************************************
    function ShowOrHideCheckBoxClicked(v, img) {
        var tdDetail = document.getElementById(v);
        var image = document.getElementById(img);
        if (tdDetail.style.display == "none") {
            tdDetail.style.display = "";
            image.src = "../Images/collapse-minus.png";
        }
        else {
            tdDetail.style.display = "none"; image.src = "../Images/expand-plus.png";
        }
        SelectRowCheckbox();
        CheckAllRow();
    }
    function CheckAllRow() {
        var DetailArray = document.getElementById('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_hdnDetailRowIds').value.split(',');
        var allExpand = true;
        var AllCollapsed = true;
        for (var i = 0; i < DetailArray.length; i++) {
            var arr = DetailArray[i].split(';');
            var row = document.getElementById(arr[0]);
            if (row != null) {
                if (row.style.display == "none") {
                    allExpand = false;
                }
                else {
                    AllCollapsed = false;
                }
            }

        }

        var headerimg = document.getElementById('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_lsvSearchResult_imgShowHide');
        if (allExpand) headerimg.src = "../Images/collapse-minus.png";
        if (AllCollapsed) headerimg.src = "../Images/expand-plus.png";
    }
    function HotListSelected(source, eventArgs) {
        hdnSelectedHotList = document.getElementById("ctl00_cphHomeMaster_ctrlEntireInternalDatabase_hdnSelectedHotList");
        hdnSelectedHotListText = document.getElementById("ctl00_cphHomeMaster_ctrlEntireInternalDatabase_hdnSelectedHotListText");
        hdnSelectedHotListText.value = eventArgs.get_text();
        hdnSelectedHotList.value = eventArgs.get_value();
    }



    function clickSearchButton(e) {
        // debugger;

        //event = fix(event);
        if (e.keyCode == 13) {
            var btn = $get('<%= btnSearch.ClientID%>');
            if (typeof btn == 'object') {
                btn.click();
            }
        }
    }
    function fix(event) {
        if (!event) event = window.event;
        if (!event) return null;
        if (!event.stopPropagation) { event.stopPropagation = Func('this.cancelBubble = true') }
        if (!event.preventDefault) { event.preventDefault = Func('this.returnValue = true') }
        if (typeof event.layerX == 'undefined' && typeof event.offsetX == 'number') {
            event.layerX = event.offsetX; event.layerY = event.offsetY;
        }
        if (event.target) {
            if (event.target.nodeType == 3) event.target = event.target.parentNode;
        }
        if (!event.target && event.srcElement) {
            event.target = event.srcElement;
            if (event.type == 'onmouseout') {
                event.relatedTarget = event.toElement;
            }
            else if (event.type == 'onmouseover') {
                event.relatedTarget = event.fromElement;
            }
        }
        event.fixed = true;
        return event;
    }


    function IsNumeric(sText) {
        var ValidChars = "0123456789.";
        var IsNumber = true;
        var Char;


        for (i = 0; i < sText.length && IsNumber == true; i++) {
            Char = sText.charAt(i);
            if (ValidChars.indexOf(Char) == -1) {
                IsNumber = false;
            }
        }
        return IsNumber;

    }
    function ShowHideAllDetail(HeaderImage) {

        alert(HeaderImage);
        var image = HeaderImage.src;
        var expand;
        if (image.indexOf("expand-plus.png") >= 0) {
            expand = true;
            HeaderImage.src = "../Images/collapse-minus.png";
        }
        else {
            expand = false;
            HeaderImage.src = "../Images/expand-plus.png";
        }
        var DetailArray = document.getElementById('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_hdnDetailRowIds').value.split(',');
        for (var i = 0; i < DetailArray.length; i++) {
            var arr = DetailArray[i].split(';');
            var row = document.getElementById(arr[0]);
            var img = document.getElementById(arr[1]);
            if (row != null && img != null) {
                img.src = HeaderImage.src;
                if (expand) row.style.display = "";
                else row.style.display = "none";
            }
        }
    }


    function SelectRowCheckbox() {
        var ischeckall = true;
        var HeaderCheckbox = document.getElementById('chkAllItem');
        var elements = HeaderCheckbox.form.elements;

        for (var i = 0; i < elements.length; i++) {
            if ((elements[i].type == 'checkbox') && (elements[i].id != HeaderCheckbox.id)) {


                if (elements[i].checked == true) {
                    var hndId = elements[i + 1];
                    ADDID(hndId.value, 'ctl00_cphHomeMaster_ctrlEntireInternalDatabase_txtSelectedIds');

                }
                else {
                    var hndId = elements[i + 1];
                    RemoveID(hndId.value, 'ctl00_cphHomeMaster_ctrlEntireInternalDatabase_txtSelectedIds');

                }


            }

        }
        HeaderCheckbox.checked = ischeckall;
    }






    function SelectAllCheckbox(HeaderCheckbox) {

        // debugger;
        var isChecked = HeaderCheckbox.checked;
        var elements = HeaderCheckbox.form.elements;

        for (var i = 0; i < elements.length; i++) {
            if ((elements[i].type == 'checkbox') && (elements[i].id != HeaderCheckbox.id)) {
                if (elements[i].id.indexOf('chkCandidate') > -1) {
                    if (elements[i].checked != isChecked) {
                        elements[i].checked = isChecked;
                        if (isChecked == true) {
                            var hndId = elements[i + 1];
                            if (IsNumeric(hndId.value) && hndId.value != "0")
                                ADDID(hndId.value, 'ctl00_cphHomeMaster_ctrlEntireInternalDatabase_txtSelectedIds');

                        }
                        else {
                            var hndId = elements[i + 1];
                            if (IsNumeric(hndId.value) && hndId.value != "0")
                                RemoveID(hndId.value, 'ctl00_cphHomeMaster_ctrlEntireInternalDatabase_txtSelectedIds');
                        }


                    }
                }

            }
        }
    }

    function IsReset() {
        var txtField = document.getElementById('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_txtSelectedIds');
        txtField.value = "";
    }


    function CheckUnCheckRowLevelCheckboxes(cntrlHeaderCheckbox, cntrlGridView, posCheckbox) {
        //debugger
        var ctrlHeaderChkBox = $get(cntrlHeaderCheckbox);
        var ctrlTable = $get(cntrlGridView);
        var rowLength = ctrlTable.rows.length;

        for (var i = 1; i < rowLength; i++) {
            var myrow = ctrlTable.rows[i];
            var mycel = myrow.getElementsByTagName("td")[posCheckbox];

            for (var j = 0; j < mycel.childNodes.length; j++) {
                if (mycel.childNodes[j].type == "checkbox") {
                    mycel.childNodes[j].checked = ctrlHeaderChkBox.checked;
                }
            }
        }
    }

    function CheckUnCheckHeaderCheckbox(triggerControl, cntrlGridView, cntrlHeaderCheckbox, posCheckbox) {
        // debugger
        var ctrlHeaderChkBox = $get(cntrlHeaderCheckbox);

        if (!triggerControl.checked) {
            ctrlHeaderChkBox.checked = triggerControl.checked;
        }
        else {
            var checked = true;
            var ctrlTable = $get(cntrlGridView);
            var rowLength = ctrlTable.rows.length;

            for (var i = 1; i < rowLength; i++) {
                var myrow = ctrlTable.rows[i];
                var mycel = myrow.getElementsByTagName("td")[posCheckbox];

                for (var j = 0; j < mycel.childNodes.length; j++) {
                    if (mycel.childNodes[j].type == "checkbox") {
                        if (!checked || !mycel.childNodes[j].checked) {
                            checked = false;
                        }
                    }
                }
            }

            ctrlHeaderChkBox.checked = checked;
        }
    }



    function fnValidateSalary(source, args) {
        // debugger;        

        var txtSalaryRangeFromValue = $get('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_txtSalaryRangeFrom').value.trim();
        var ddlPaymentTypeValue = $get('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_ddlPaymentType').value;

        if (txtSalaryRangeFromValue != "" && !isNaN(txtSalaryRangeFromValue) && ddlPaymentTypeValue < 1 && txtSalaryRangeFromValue > 0) {
            if (args.Value == 0)
                args.IsValid = false;
            else
                args.IsValid = true;
        }
    }

    function fnValidateSalaryTo(source, args) {
        // debugger;            

        var txtSalaryRangeToValue = $get('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_txtSalaryRangeTo').value.trim();
        var ddlPaymentTypeValue = $get('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_ddlPaymentType').value;

        if (txtSalaryRangeToValue != "" && !isNaN(txtSalaryRangeToValue) && ddlPaymentTypeValue < 1 && txtSalaryRangeToValue > 0) {
            if (args.Value == 0)
                args.IsValid = false;
            else
                args.IsValid = true;
        }
    }

    function fnValidateCurrency(source, args) {
        // debugger;            

        var txtSalaryRangeFromValue = $get('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_txtSalaryRangeFrom').value.trim();
        var ddlCurrency = $get('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_ddlCurrency').value;

        if (txtSalaryRangeFromValue != "" && !isNaN(txtSalaryRangeFromValue) && ddlCurrency < 1 && txtSalaryRangeFromValue > 0) {
            if (args.Value == 0)
                args.IsValid = false;
            else
                args.IsValid = true;
        }



    }

    function fnValidateCurrencyTo(source, args) {
        // debugger;            

        var txtSalaryRangeToValue = $get('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_txtSalaryRangeTo').value.trim();
        var ddlCurrency = $get('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_ddlCurrency').value;

        if (txtSalaryRangeToValue != "" && !isNaN(txtSalaryRangeToValue) && ddlCurrency < 1 && txtSalaryRangeToValue > 0) {
            if (args.Value == 0)
                args.IsValid = false;
            else
                args.IsValid = true;
        }
    }

    function CheckboxUncheck() {
        var frm = document.forms[0];
        for (i = 0; i < frm.length; i++) {
            e = frm.elements[i];
            if (e.type == 'checkbox' && e.name.indexOf(chkCandidate) != -1) {
                e.checked = false;
            }
        }
    }
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoadedReqs);
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endReqs);
    function PageLoadedReqs(sender, args) {
       
    }
    function endReqs(sender, args) {
       
    }






</script>


<asp:HiddenField ID="hdnScrollPos" runat="server" />

<asp:ObjectDataSource ID="odsRequisitionList" runat="server" SelectMethod="GetPagedRequisitionList"
    OnSelecting="odsRequisitionList_Selecting" TypeName="TPS360.Web.UI.JobPostingDataSource"
    SelectCountMethod="GetRequisitionListCount" EnablePaging="True" SortParameterName="sortExpression">
    <SelectParameters>
        <asp:Parameter Name="IsTemplate" DefaultValue="False" Type="Boolean" Direction="Input" />
        <asp:Parameter Name="memberId" />
        <asp:Parameter Name="IsCompanyContact" Type="Boolean" DefaultValue="False" />
        <asp:Parameter Name="CompanyContactId" Type="Int32" DefaultValue="0" />
        <asp:ControlParameter ControlID="txtJobTitle" Name="JobTitle" PropertyName="Text"
            Type="String" />
        <asp:ControlParameter ControlID="txtReqCode" Name="ReqCode" PropertyName="Text" Type="String" />
        <asp:ControlParameter ControlID="ddlJobStatus" Name="jobStatus" PropertyName="SelectedItems"
            Type="String" />
        <asp:ControlParameter ControlID="dtPstedDate" Name="JobPostingFromDate" PropertyName="StartDate"
            Type="String" />
        <asp:ControlParameter ControlID="dtPstedDate" Name="JobPostingToDate" PropertyName="EndDate"
            Type="String" />
        <asp:ControlParameter ControlID="ddlReqCreator" Name="ReqCreator" PropertyName="SelectedValue"
            Type="String" />
        <asp:ControlParameter ControlID="ddlEmployee" Name="employee" PropertyName="SelectedValue"
            Type="String" />
        <asp:ControlParameter ControlID="TxtCity" Name="City" PropertyName="Text" Type="String" />
        <asp:ControlParameter ControlID="uclCountryState" Name="StateID" PropertyName="SelectedStateId"
            Type="String" />
        <asp:ControlParameter ControlID="uclCountryState" Name="CountryID" PropertyName="SelectedCountryId"
            Type="String" />
        <asp:ControlParameter ControlID="ddlEndClient" Name="endClients" PropertyName="SelectedItems"
            Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>


<asp:Panel ID="pnlSearchBoxBody" runat="server"  Visible="false" CssClass="well well-small nomargin"
    DefaultButton="btnSearch">
    <asp:Panel ID="pnlSearchRegion" runat="server">
        <asp:CollapsiblePanelExtender ID="cpeExperience" runat="server" TargetControlID="pnlSearchBoxContent"
            ExpandControlID="pnlSearchBoxHeader" CollapseControlID="pnlSearchBoxHeader" CollapsedImage="~/Images/expand-plus.png"
            ExpandedImage="~/Images/collapse-minus.png" Collapsed="true" ImageControlID="imgSearchBoxToggle"
            SuppressPostBack="True">
        </asp:CollapsiblePanelExtender>
        <asp:Panel ID="pnlSearchBoxHeader" runat="server">
            <div class="SearchBoxContainer">
                <div class="SearchTitleContainer">
                    <div class="ArrowContainer">
                        <asp:ImageButton ID="imgSearchBoxToggle" runat="server" ImageUrl="~/Images/expand-plus.png"
                            AlternateText="collapse" />
                    </div>
                    <div class="TitleContainer">
                        Filter Options
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlSearchBoxContent" runat="server"  CssClass ="filter-section"  Height="0">
            <div class="TableRow spacer">
                <div class="FormLeftColumn" style="width: 50%;">
                    <div class="TableRow">
                        <div class="TableFormLeble" style="width: 40%;">
                            <asp:Label EnableViewState="false" ID="lblJobTitle" runat="server" Text="Job Title"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtJobTitle" runat="server" CssClass="SearchTextbox"></asp:TextBox>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble" style="width: 40%;">
                            <asp:Label EnableViewState="false" ID="lblReqCode" runat="server" Text="Req Code"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtReqCode" runat="server" CssClass="SearchTextbox"></asp:TextBox>
                        </div>
                    </div>
                    <div class="TableRow" style="white-space: nowrap">
                        <div class="TableFormLeble" style="width: 40%;">
                            <asp:Label EnableViewState="false" ID="Label1" runat="server" Text="Date Posted"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <ucl:DateRangePicker ID="dtPstedDate" runat="server" />
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble" style="width: 40%;">
                            <asp:Label EnableViewState="false" ID="lblJobStatusHeader" runat="server" Text="Requisition Status"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                        <ucl:MultipleSelection ID="ddlJobStatus" runat ="server" />
                        
                       <%--     <asp:DropDownList EnableViewState="true" ID="ddlJobStatus" runat="server" CssClass="CommonDropDownList"
                                AutoPostBack="false">
                            </asp:DropDownList>--%>
                        </div>
                    </div>
                    
                    
                   
                </div>
                <div class="FormRightColumn" style="width: 49%">
                    <div class="TableRow" style="white-space: nowrap;">
                    <div class="TableRow">
                        <div class="TableFormLeble" style="width: 40%;">
                            <asp:Label EnableViewState="false" ID="lblCreator" runat="server" Text="Req Creator"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:DropDownList ID="ddlReqCreator" runat="server" CssClass="CommonDropDownList">
                            </asp:DropDownList>
                        </div>
                    </div>
                    
                     <div class="TableRow">
                        <div class="TableFormLeble" style="width: 40%;">
                            <asp:Label EnableViewState="false" ID="lblByEmployee" runat="server" Text="Assigned User"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:DropDownList ID="ddlEmployee" CssClass="CommonDropDownList" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                    
                        <div class="TableFormLeble" style="width: 40%;">
                            <asp:Label EnableViewState="false" ID="lblByLocation" runat="server" Text="City"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:TextBox EnableViewState="false" ID="txtCity" CssClass="SearchTextbox" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <ucl:CountryState ID="uclCountryState" runat="server" FirstOption="Please Select" ShowAndHideState="false"
                        TableFormLabel_Width="40%" />
                    <div id="divBU" runat="server">
                    <div class="TableRow">
                        <div class="TableFormLeble" style="width: 40%;">
                            <asp:Label EnableViewState="true" ID="lblByEndClientsHeader" runat="server" Text="Account"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <%--<asp:DropDownList ID="ddlEndClient" CssClass="CommonDropDownList" runat="server">
                            </asp:DropDownList>--%>
                            <ucl:MultipleSelection ID="ddlEndClient" runat ="server" />
                        </div>
                    </div></div>
                </div>
                <div class="TableRow" style="text-align: center; padding-top: 5px; padding-bottom: 5px">
                    <asp:LinkButton ID="btnSearch" runat="server" Text="Search" AlternateText="Search"
                        CssClass="btn btn-primary" ValidationGroup="RequisitionList" EnableViewState="false"
                        OnClick="btnSearch_Click"><i class="icon-search icon-white"></i> Search</asp:LinkButton>
                    <asp:Button ID="btnClear" runat="server" Text="Clear" AlternateText="Clear" CssClass="btn"
                        EnableViewState="false" OnClick="btnClear_Click" />
                </div>
            </div>
        </asp:Panel>
    </asp:Panel>
</asp:Panel>
<%--<br />--%>
<div>
    <asp:UpdatePanel ID="up" runat="server">
        <ContentTemplate>
            <ajaxToolkit:AlwaysVisibleControlExtender ID="avceMessage" runat="server" TargetControlID="lblMessage">
            </ajaxToolkit:AlwaysVisibleControlExtender>
            <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
<asp:UpdatePanel ID="upcandidateList" runat="server">
    <ContentTemplate>
        <asp:HiddenField ID="hdnDoPostPack" runat="server" />
        <asp:HiddenField ID="hdnSortColumn" runat="server" />
        <asp:HiddenField ID="hdnSortOrder" runat="server" />
        <div class="GridContainer">
            <div style="overflow: inherit" id="bigDiv" runat="server" onscroll='SetScrollPosition()'>
            
               <%-- <asp:ListView ID="lsvJobPosting" runat="server" EnableViewState="true" DataKeyNames="Id"
                    OnItemDataBound="lsvJobPosting_ItemDataBound" OnItemCommand="lsvJobPosting_ItemCommand"
                    OnPreRender="lsvJobPosting_PreRender" DataSourceID="odsRequisitionList">
                    <LayoutTemplate>
                        <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                             <%--<tr id="trHeadLevel" runat="server">
                            
                               <th style="white-space: nowrap; width: 85px !important">
                                    <asp:LinkButton ID="btnPostedDate" runat="server" CommandName="Sort" ToolTip="Sort By Post Date"
                                        CommandArgument="[J].[PostedDate]" Text="Post Date" TabIndex="2" />
                                </th>--%>
                                
                               <%-- <th runat="server"  colspan="10" align="left"  id="thReqCode" style="white-space: nowrap; width: 1200px !important">
                                    Job Title
                                  <%--  <asp:LinkButton ID="lnkReqCode" runat="server" CommandName="Sort" ToolTip="Sort By Req Code"
                                        CommandArgument="[J].[JobPostingCode]" Text="Job Title" TabIndex="3" />--%>
                                <%--</th>--%>
                               <%-- <th style="white-space: nowrap; width: 90px !important">
                                    <asp:LinkButton ID="lnkClientJobId" runat="server" CommandName="Sort" ToolTip="Sort By Client Job ID"
                                        CommandArgument="[J].ClientJobId"  TabIndex="4" />
                                </th>
                                <th style="white-space: nowrap;" runat="server" id="thClient">
                                    <asp:LinkButton ID="lnkClient" runat="server" CommandName="Sort" ToolTip="Sort By Account"
                                        CommandArgument="[C].[CompanyName]"  TabIndex="5" />
                                </th>
                                <th style="white-space: nowrap; min-width: 60px">
                                    <asp:LinkButton ID="btnJobTitle" runat="server" CommandName="Sort" ToolTip="Sort By Job Title"
                                        CommandArgument="[J].[JobTitle]"  TabIndex="6" />
                                </th>
                                <th style="white-space: nowrap; min-width: 65px">
                                    <asp:LinkButton ID="btnCity" runat="server" CommandName="Sort" ToolTip="Sort By Location"
                                        CommandArgument="[J].[City]"  TabIndex="7" />
                                </th>
                                <th  id="thNoOfOpening" runat="server" style="width: 97px !important">
                                    <asp:LinkButton ID="lnkNoOfOpening" runat="server" CommandName="Sort" 
                                       CommandArgument="[J].NoOfOpenings" TabIndex="8" />
                                </th>
                                <th id="thLevels" runat="server" style="width: 40px !important">
                                    <asp:LinkButton ID="lnkLevel" runat="server"  ToolTip="Sort By Total Candidates"
                                        CommandName="Sort" CommandArgument="[1]"></asp:LinkButton>
                                </th>
                                <th style="width: 40px !important">
                                    <asp:LinkButton ID="lnkTeam" runat="server"   ToolTip="Sort By Team" CommandArgument="[JMC].[ManagersCount]"
                                        CommandName="Sort"></asp:LinkButton>
                                </th>
                                <th style="white-space: nowrap; min-width: 50px; width: 105px !important">
                                    <asp:LinkButton ID="btnJobStatus" runat="server" CommandName="Sort"
                                        CommandArgument="[GL].[Name]" TabIndex="7" Width="50%" />
                                </th>--%>
                               <%-- <th style="white-space: nowrap;" runat="server" id="thAssigningManager">
                                    Assigning Manager
                                </th>
                                <th style="text-align: center; white-space: nowrap; width: 45px !important;" id="thAction"
                                    runat="server">
                                    Action
                                </th>
                            </tr>--%>
                           <%-- <tr>
                              <td colspan="10" style="padding: 0px 0px 0px 0px; border-style: none none none none">
                                </td>
                            </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                            <tr class="Pager" visible="false">
                                <td colspan="10" runat="server" id="tdPager">
                                   <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <EmptyDataTemplate>
                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                            <tr>
                                <td>
                                    No data was returned.
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <ItemTemplate>
                      <tr id="row" runat="server"  >--%>
                           <%-- <td>
                                <asp:Label ID="lblPostedDate" runat="server" Width="80px" />
                            </td>--%>
                        <%--  <td  colspan="5"  style="font-weight: normal;  font-family : Arial; font-size:15px" id="tdtitle" runat="server">                                                                            
                               <B>  <asp:Label ID="lnkJobTitle" runat="server" color="blue"/></B>
                                 <asp:HiddenField ID="hfCandidateId" runat="server" />
                            </td>--%>
                            
                          <%--  <td>
                                <asp:Label ID="lblClientJobId" runat="server" />
                            </td>
                            <td runat="server" id="tdClient">
                                <asp:Label ID="lblClient" runat="server" />
                            </td>
                            <td>
                                <asp:HyperLink ID="tdJobCode" runat="server" Target="_blank" TabIndex="8" Style="cursor: pointer;"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:Label ID="lblCity" runat="server" />
                            </td>
                             <td >
                                <asp:Label ID="lblNoOfOpenings" runat="server" />
                            </td>--%>
                             <%-- <td style="border-width: 0px;" >
                               
                            </td>
                              <td style="border:none;">
                               
                            </td>
                              <td >
                               
                            </td>
                              <td >
                                
                            </td>--%>
                        <%--  <td  colspan="5"  style="font-weight: normal; text-align: right; font-family : Arial;  font-size:15px" id="tdApply">      
                                              
                               <B> <u> <asp:HyperLink ID="lblApply" runat="server" Target="_blank"  Style="cursor: pointer; " ToolTip="Click here ,to view the requirment details and Apply the Post"></asp:HyperLink> </u> </B>                      
                            </td> --%>
                            
                           <%-- <td>
                                <asp:LinkButton ID="lnkTeamCount" runat="server" CommandName="Managers"></asp:LinkButton>
                            </td>--%>
                            <%-- <td  align="center" style="font-weight: normal;" id="td1">
                                <asp:HyperLink ID="HyperLink1" runat="server"></asp:HyperLink>
                               <%-- <asp:Label ID="lblApply" runat="server" Style="min-height: 100px;" />
                             
                            </td>
                            <td id="tdLevels" runat="server">
                                <asp:HyperLink ID="tdLevel" runat="server"></asp:HyperLink>
                            </td>
                           
                             <td>
                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Managers"></asp:LinkButton>
                            </td>--%>                        
                      <%-- </tr>                


                         <tr id="trCandidateDetails" runat="server"  style="display: none" >

                                    <td>
                                    </td>
                                    <td colspan="1" align="left" id="tdDetail" style="font-weight: normal;">
                                        <asp:Panel ID="pnlContent1" runat="server">
                                            <b>No.of Opening:</b>
                                            <asp:Label ID="lblopening" runat="server"></asp:Label>
                                            <br />
                                            <b>Open Date:</b><asp:Label ID="lblopendate" runat="server"></asp:Label>
                                            <br />
                                            <b>Experience:</b>
                                            <asp:Label ID="lblexperience" runat="server" Text=""></asp:Label><br />                                           
                                        </asp:Panel>
                                    </td>
                               </tr>
                    </ItemTemplate>
                </asp:ListView>--%>
                
                 <div id="div1" runat="server" style="width:100%; height:100%; overflow: inherit; " >
        
     
                 </div>
                
                
            </div>
        </div>
        
        
       <%--code comment by pravin khot --%>
    <%-- <div id="div1" runat="server"     style="align:center">
        
        <table id="Tb_Header"  style='font-family : Arial; align:left ; width:1000; height:'75'> 

       <tr> 
          <td style='color:#3AC0BB' align="left" > <font size=5> <u> <b>Opportunities </b></u> </font> </td> 
        </tr> 
        
    </table>--%>
        
        <%--<table id = "Tb_List" style=width='2400px' align=center runat = "server">
        <tr  runat="server"> 
        <td></td>
        </tr> 
        
        </table>
        </div> --%>
      <%--  ***********end***********--%>
        
        
        
        
        <%--<table>
        <tr>
        <td>--%>
                <%--<asp:LinkButton ID="LinkButton1" runat="server"  ></asp:LinkButton>--%>

<%--        <asp:LinkButton ID="lnkTeamCount" runat="server"  OnClick="EditModal(('" + UrlConstants.ApplicationBaseUrl + "Modals/CareerJobDetail.aspx?JID=714&FromPage=JobDetail" + "','700px','570px'); return false;")" ></asp:LinkButton>
--%>        <%--</td>
        </tr>
        
        </table>--%>
        
    </ContentTemplate>
</asp:UpdatePanel>
