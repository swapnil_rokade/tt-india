﻿using System;
using System.Collections;
using System.Web;
using System.Web.UI.WebControls;

using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;

namespace TPS360.Web.UI
{
    public partial class LeftPanel : BaseControl
    {
        #region Private variable

        ArrayList _permittedMenuIdList;
        string _currentNode = string.Empty;
        #endregion

        #region Methods

        private bool IsInPermitedMenuList(int menuId)
        {
            if (_permittedMenuIdList == null)
            {
                _permittedMenuIdList = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
            }

            if ((_permittedMenuIdList != null) && (_permittedMenuIdList.Count > 0))
            {
                if (_permittedMenuIdList.Contains(menuId))
                {
                    return true;
                }
            }

            return false;
        }

        public int CurrentSiteMapType
        {
            get
            {
                return (int)(ViewState[this.ClientID + "_SiteMapType"] ?? 0);
            }
            set
            {
                ViewState[this.ClientID + "_SiteMapType"] = value;
            }
        }

        public bool EnablePermissionChecking
        {
            get
            {
                return (bool)(ViewState[this.ClientID + "_EnablePermissionChecking"] ?? true);
            }
            set
            {
                ViewState[this.ClientID + "_EnablePermissionChecking"] = value;
            }
        }

        private void BuildHeaderMenu()
        {
            if (CurrentSiteMapType > 0)
            {
                SiteMapNodeCollection permittedNodeList = new SiteMapNodeCollection();

                try
                {
                    SiteMapNodeCollection nodeList = LeftMenuSiteMap.Provider.FindSiteMapNodeFromKey(StringHelper.Convert(CurrentSiteMapType)).ChildNodes;

                    if (nodeList != null && nodeList.Count > 0)
                    {
                        foreach (SiteMapNode node in nodeList)
                        {
                           // if (EnablePermissionChecking)
                            {
                                if (IsInPermitedMenuList(Convert.ToInt32(node["Id"])))
                                {
                                    permittedNodeList.Add(node);
                                }
                            }
                            //else
                            {
                               // permittedNodeList.Add(node);
                            }
                        }
                    }

                    
                    SiteMapNodeCollection permittedNodeList1 = new SiteMapNodeCollection();
                    foreach (SiteMapNode node in permittedNodeList )
                    {
                        if (Request.Url.ToString().ToLower().Contains("email"))
                        {
                            if (node.Title == "Email History") permittedNodeList1.AddRange(node.ChildNodes);

                        }
                        else
                        {
                            if (node.Title == "My Settings") permittedNodeList1.AddRange(node.ChildNodes);

                        }

                        
                    }
                    lvNavigationItems.DataSource = permittedNodeList1;
                    lvNavigationItems.DataBind();
                }
                catch
                {
                }
            }
        }

        protected SiteMapNodeCollection GetChildMenu(SiteMapNode parentNode)
        {
            SiteMapNodeCollection permittedNodeList = new SiteMapNodeCollection();
            SiteMapNodeCollection nodeList = LeftMenuSiteMap.Provider.GetChildNodes(parentNode);

            if (nodeList != null && nodeList.Count > 0)
            {
                foreach (SiteMapNode node in nodeList)
                {
                  //  if (EnablePermissionChecking)
                    {
                        if ( IsInPermitedMenuList(Convert.ToInt32(node["Id"])))
                        {
                            permittedNodeList.Add(node);
                        }
                    }
                  //  else
                   // {
                    //    permittedNodeList.Add(node);
                   // }
                }
            }

            return permittedNodeList;
        }

        protected string BuildUrl(string pageUrl)
        {
            pageUrl = "../" + pageUrl;

            if (IsUserClient || IsUserVendor || IsUserPartner)
            {
                string companyId = Helper.Url.SecureUrl[UrlConstants.PARAM_COMPANY_ID];

                if (!string.IsNullOrEmpty(Helper.Url.SecureUrl[UrlConstants.PARAM_COMPANY_ID]))
                {
                    pageUrl += MiscUtil.GetQueryStringPrefix(pageUrl) + UrlConstants.PARAM_COMPANY_ID + "=" + Helper.Url.SecureUrl[UrlConstants.PARAM_COMPANY_ID];
                }
            }

            pageUrl = (new SecureUrl(pageUrl)).ToString();

            return pageUrl;
        }

        #endregion

        #region Events

        protected void lvNavigationItems_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                SiteMapNode node = ((ListViewDataItem)e.Item).DataItem as SiteMapNode;
                if (node != null)
                {
                    System.Web.UI.HtmlControls.HtmlGenericControl linkLeftMenuItem = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("lnkLeftMenuItem");
                    
                    string strActiveClass = node["Id"].Equals(_currentNode) ? " active" : "";
                    if (Request.Url.ToString().ToLower().Contains("allemail"))
                    {
                        if ( node.Title == "All Emails") strActiveClass = "active";
                    }
                    else if (Request.Url.ToString().ToLower().Contains("sentemail"))
                    {
                        if (node.Title == "Sent Emails") strActiveClass = "active";
                    }
                    else if (Request.Url.ToString().ToLower().Contains("receivedemail"))
                    {
                        if (node.Title == "Received Emails") strActiveClass = "active";
                    }
                    else if (Request.Url.ToString().ToLower().Contains("signature"))
                    {
                        if (node.Title == "My Email Signature") strActiveClass = "active";
                    }
                    else if (Request.Url.ToString().ToLower().Contains("changepassword"))
                    {
                        if (node.Title == "Change Password") strActiveClass = "active";
                    }
                    else if (Request.Url.ToString().ToLower().Contains("mailsetup"))
                    {
                        if (node.Title == "Mail Setup") strActiveClass = "active";
                    }
                    else if (Request.Url.ToString().ToLower().Contains("sessiontimeout"))
                    {
                        if (node.Title == "Session Timeout") strActiveClass = "active";
                    }
                    linkLeftMenuItem.Attributes.Add("class", strActiveClass);
                    linkLeftMenuItem.InnerHtml = "<a href=\"" + BuildUrl(node.Url) + "\">"+ node .Title + "</a>";
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _currentNode = Helper.Url.SecureUrl[UrlConstants.PARAM_SITEMAP_ID].ToString();
            if (!IsPostBack)
            {
                
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (!IsPostBack)
            {
                BuildHeaderMenu();
            }
        } 

        #endregion
    }
}