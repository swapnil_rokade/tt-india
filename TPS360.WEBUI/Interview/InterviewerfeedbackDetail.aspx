﻿<%-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: InterviewerfeedbackDetail.ascx
    Description:  
    Created By: pravin khot
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date           Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------

    ------------------------------------------------------------------------------------------------------------------------------          
--%>
<%@ Page Language="C#" MasterPageFile="~/Modals/Modal.master" AutoEventWireup="true" CodeFile="InterviewerfeedbackDetail.aspx.cs" Inherits="TPS360.Web.UI.InterviewerfeedbackDetail"  EnableViewState ="true"  %>
<%@ Register Src="~/Controls/InterviewerfeedbackDetail.ascx" TagName="InterviewerfeedbackDetail"
    TagPrefix="uc1" %>
<asp:Content ID="cntCandidateResumeBuilder" ContentPlaceHolderID="cphModalMaster"
    runat="Server">
    <div style ="width : 670px; height : 495px;  max-height :500px; overflow:auto; ">
            <uc1:InterviewerfeedbackDetail ID="ucntrlInterviewerfeedbackDetail" runat="server" />
    </div>
</asp:Content>
        