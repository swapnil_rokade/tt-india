﻿using System;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common.Helper;
using System.Collections.Generic;
using System.Web.Security;
using System.Web;
using TPS360.Providers;
using TPS360.Common.Shared;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls.WebParts;
namespace TPS360.Web.UI
{
    public partial class GlobalSearchResult : BasePage
    {
        #region Member Variables

        string _searchKeyword = string.Empty;
        ArrayList _permittedMenuIdList;
        SiteMapNodeCollection permittedNodeList = new SiteMapNodeCollection();
        string final = string.Empty;
        SqlSiteMapProvider provider = SiteMap.Providers["SqlSiteMap"] as SqlSiteMapProvider;
        bool _getCandidateCount;
        bool _getConsultantCount;
        bool _getCompanyCount;
        bool _getEmployeeCount;
        private static string Url
        {
            get;
            set;
        }


        private bool _IsAccessToEmployee = true;
        private static string UrlForEmployee = string.Empty;
        private static int IdForEmployeeSitemap = 0;

        private bool _IsAccessToCandidate = true;
        private static string UrlForCandidate = string.Empty;
        private static int IdForCandidateSitemap = 0;

        private bool _IsAccessToCompany = true;
        private static string UrlForCompany = string.Empty;
        private static int IdForCompanySitemap = 0;

        private static bool _mailsetting = false;
        const string _defaultPageSize= "10";
        #endregion

        #region Methods
        //protected override void Render(HtmlTextWriter writer)
        //{
        //    writer.RenderBeginTag(writer);
        //    if (writer.TextMode == TextBoxMode.MultiLine)
        //    {
        //        HttpUtility.HtmlEncode(writer.Text, writer);
        //    }
        //    writer.RenderEndTag(writer);
        //}
        private void PrepareInitialView(string searchKey)
        {
            SearchDatabase(MiscUtil .RemoveScript ( searchKey));
        }

        private void SearchDatabase(string searchKey)
        {
            lblEmployeeTotal.Text = "Employee (0)";
            lsvEmployee.DataSource = null;
            lsvEmployee.DataBind();
            lblCandidateTotal .Text ="Candidate (0)";
            lsvCandidate.DataSource = null;
            lsvCandidate.DataBind();
            lblCompanyTotal.Text = "Company (0)";
            lsvCompany.DataSource = null;
            lsvCompany.DataBind();
            searchKey = searchKey.Replace("amp;", "&");
            lblSearchMessage.Text = "Search Results for ";
            lblSearchKey.Text =MiscUtil .RemoveScript ( searchKey);
            txtSearch.Text =MiscUtil .RemoveScript ( searchKey,string .Empty );
            
            odsCandidateQuickSearch.SelectParameters["quickSearchKeyWord"].DefaultValue =MiscUtil .RemoveScript ( txtSearch.Text.Trim());
            odsEmployeeQuickSearch.SelectParameters["quickSearchKeyWord"].DefaultValue =MiscUtil .RemoveScript ( txtSearch.Text.Trim());
            odsCompanyQuickSearch.SelectParameters["quickSearchKeyWord"].DefaultValue =MiscUtil .RemoveScript ( txtSearch.Text.Trim());

            
            ListViewDataBind();
            MemberPrivilege();
        }

        private bool IsInPermitedMenuList(int menuId)
        {
            if (_permittedMenuIdList == null)
            {
                _permittedMenuIdList = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
            }

            if ((_permittedMenuIdList != null) && (_permittedMenuIdList.Count > 0))
            {
                if (_permittedMenuIdList.Contains(menuId))
                {
                    return true;
                }
            }

            return false;
        }

        private void MemberPrivilege()
        {
            if (CurrentMember == null || CurrentUser == null)
            {
                return;
            }

            SiteMapNode root = provider.FindSiteMapNodeFromKey(StringHelper.Convert((int)SiteMapType.ApplicationTopMenu));

            if (root != null)
            {
                SiteMapNodeCollection nodeList = root.ChildNodes;

                if (nodeList != null && nodeList.Count > 0)
                {
                    foreach (SiteMapNode node in nodeList)
                    {
                        if (IsInPermitedMenuList(Convert.ToInt32(node["Id"])))
                        {
                            if (node.Title == "ATS")
                            {
                                lblCandidateTotal.Visible = true;
                                lsvCandidate.Visible = true;
                            }
                            if (node.Title == "Employee")
                            {
                                lblEmployeeTotal.Visible = true;
                                lsvEmployee.Visible = true;
                                
                            }
                            
                            if (node.Title == "SFA")
                            {
                                SiteMapNode roots = provider.FindSiteMapNodeFromKey(StringHelper.Convert((int)SiteMapType.SFA ));
                                final = NodeForGlobalDataBind(roots);
                                if (final.Trim() != "2")
                                {
                                    lblCompanyTotal.Visible = true;
                                    lsvCompany.Visible = true;
                                }
                            }
                        }
                    }
                }
            }
        }
        private string  NodeForGlobalDataBind(SiteMapNode root)
        {
            string  status = string .Empty ;
            if (root != null)
            {
                SiteMapNodeCollection nodeList = root.ChildNodes;

                if (nodeList != null && nodeList.Count > 0)
                {
                    foreach (SiteMapNode node in nodeList)
                    {
                        if (IsInPermitedMenuList(Convert.ToInt32(node["Id"])))
                        {
                            if (node.Title == "Master Employee List")
                            {
                                status = "1";
                            }
                            else if (node.Title == "My Employee List")
                            {
                                status = status +"2";
                            }
                            if (node.Title == "Master Company List")
                            {
                                status = "1";
                            }
                            else if (node.Title == "My Company List")
                            {
                                status =status + "2";
                            }
                            if (node.Title == "Master Candidate List")
                            {
                                status = "1";
                            }
                            else if (node.Title == "Precise Search")
                            {
                                status = "1";
                            }
                            else if (node.Title == "My Candidate List")
                            {
                                status = status + "2";
                            }
                        }
                    }
                }
            }
            return status;
        }
        private void ListViewDataBind()
        {

            if (SortOrder_Candidate.Text == string.Empty && SortColumn_Candidate.Text == string.Empty)
            {
                SortOrder_Candidate.Text = "asc";
                SortColumn_Candidate.Text = "[C].[FirstName]";
            }
            odsCandidateQuickSearch.SelectParameters["SortOrder"].DefaultValue = SortOrder_Candidate.Text.ToString();


            if (SortOrder_Company.Text == string.Empty && SortColumn_Company.Text == string.Empty)
            {
                SortOrder_Company.Text = "asc";
                SortColumn_Company.Text = "CompanyName";
            }

            odsCompanyQuickSearch.SelectParameters["SortOrder"].DefaultValue = SortOrder_Company.Text.ToString();

            if (SortOrder_Employee.Text == string.Empty && SortColumn_Employee.Text == string.Empty)
            {
                SortOrder_Employee.Text = "asc";
                SortColumn_Employee.Text = "FirstName";
            }

            odsEmployeeQuickSearch.SelectParameters["SortOrder"].DefaultValue = SortOrder_Employee.Text.ToString();





            if (CurrentMember == null || CurrentUser == null)
            {
                return;
            }

            
            SiteMapNode root = provider.FindSiteMapNodeFromKey(StringHelper.Convert((int)SiteMapType.ApplicationTopMenu ));

            if (root != null)
            {
                SiteMapNodeCollection nodeList = root.ChildNodes;

                if (nodeList != null && nodeList.Count > 0)
                {
                    foreach (SiteMapNode node in nodeList)
                    {
                        if (IsInPermitedMenuList(Convert.ToInt32(node["Id"])))
                        {
                            if(Convert.ToInt32(node["Id"])==12)//if (node.Title == "ATS" )
                            {
                                lsvCandidate.DataSourceID = "odsCandidateQuickSearch";
                                lsvCandidate.DataBind();
                            }
                            if (Convert.ToInt32(node["Id"]) == 15)//if (node.Title == "Employee")
                            {
                                
                                    lsvEmployee.DataSourceID = "odsEmployeeQuickSearch";
                                    lsvEmployee.DataBind();
                                
                            }

                            if (Convert.ToInt32(node["Id"]) == 11) //if (node.Title == "SFA")
                            {
                                SiteMapNode roots = provider.FindSiteMapNodeFromKey(StringHelper.Convert((int)SiteMapType.SFA));
                                final = NodeForGlobalDataBind(roots);
                                if (final.Trim() != "2")
                                {
                                        lsvCompany.DataSourceID = "odsCompanyQuickSearch";
                                        lsvCompany.DataBind();
                                }
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region Events

        #region Page Load Events
        
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (base.CurrentMember != null)
            {
            CustomSiteMap CustomMap = new CustomSiteMap();

            CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(235, CurrentMember.Id);
            if (CustomMap == null) _IsAccessToCompany = false;
            else
            {
                IdForCompanySitemap = CustomMap.Id;
                UrlForCompany = "~/" + CustomMap.Url.ToString();
            }

            CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(360, CurrentMember.Id);
            if (CustomMap == null) _IsAccessToEmployee = false;
            else
            {
                IdForEmployeeSitemap = CustomMap.Id;
                UrlForEmployee = "~/" + CustomMap.Url.ToString();
            }

            CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(358, CurrentMember.Id);
            if (CustomMap == null) _IsAccessToCandidate = false;
            else
            {
                IdForCandidateSitemap = CustomMap.Id;
                UrlForCandidate = "~/" + CustomMap.Url.ToString();
            }

            _mailsetting = MiscUtil.IsValidMailSetting(Facade, CurrentMember.Id);
                if (!IsPostBack)
                {
                    if (Request.Params["globalsearchkey"] != null)
                    {
                        _searchKeyword = Request.Params["globalsearchkey"].ToString();
                         PrepareInitialView(MiscUtil .RemoveScript ( _searchKeyword));
                    }
                    else if (Url != string.Empty)
                    {
                        PrepareInitialView(Url);
                        Url = string.Empty;
                    }
                    if (!IsPostBack)
                    {
                        SiteMapNode root = provider.FindSiteMapNodeFromKey(StringHelper.Convert((int)SiteMapType.Employee));
                        final = NodeForGlobalDataBind(root);
                        odsEmployeeQuickSearch.SelectParameters["quickSearchKeyWord"].DefaultValue =MiscUtil .RemoveScript ( txtSearch.Text.Trim());
                        if (final.Trim() == "2")
                            odsEmployeeQuickSearch.SelectParameters["CreatorId"].DefaultValue = base.CurrentMember.Id.ToString();

                        final = string.Empty;
                    } ASP.controls_pagercontrol_ascx PagerControl_Candidate = (ASP.controls_pagercontrol_ascx)this.lsvCandidate.FindControl("pagerControl_Candidate");
                      if (PagerControl_Candidate != null)
                      {
                          DataPager Candidate_pager = (DataPager)PagerControl_Candidate.FindControl("pager");
                          if (Candidate_pager != null)
                          {
                              string pagesize = (Request.Cookies["GlobalCandidateListRowPerPage"] == null ? _defaultPageSize : Request.Cookies["GlobalCandidateListRowPerPage"].Value);
                              Candidate_pager.PageSize = Convert.ToInt32(pagesize);
                          }
                      }
                       ASP.controls_pagercontrol_ascx PagerControl_Company = (ASP.controls_pagercontrol_ascx)this.lsvCompany .FindControl("pagerControl_Company");
                       if (PagerControl_Company != null)
                       {
                           DataPager Company_pager = (DataPager)PagerControl_Company.FindControl("pager");
                           if (Company_pager != null)
                           {
                               string pagesize = (Request.Cookies["GlobalCompanyListRowPerPage"] == null ? _defaultPageSize : Request.Cookies["GlobalCompanyListRowPerPage"].Value);
                               Company_pager.PageSize = Convert.ToInt32(pagesize);
                           }
                       }
                       ASP.controls_pagercontrol_ascx PagerControl_Employee = (ASP.controls_pagercontrol_ascx)this.lsvEmployee.FindControl("pagerControl_Employee");
                       if (PagerControl_Employee != null)
                       {
                           DataPager Employee_pager = (DataPager)PagerControl_Employee.FindControl("pager");
                           if (Employee_pager != null)
                           {
                               string pagesize = (Request.Cookies["GlobalEmployeeListRowPerPage"] == null ? _defaultPageSize : Request.Cookies["GlobalEmployeeListRowPerPage"].Value);
                               Employee_pager.PageSize = Convert.ToInt32(pagesize);
                           }
                       }



                    txtSortColumn_Candidate.Text = "btnName";
                    txtSortOrder_Candidate.Text = "ASC";
                    txtSortColumn_Company.Text = "btnCompanyName";
                    txtSortOrder_Company.Text = "ASC";
                    txtSortColumn_Employee.Text = "btnEmployeeName";
                    txtSortOrder_Employee.Text = "ASC";

                    PlaceUpDownArrow(lsvCandidate, txtSortColumn_Candidate, txtSortOrder_Candidate);
                    PlaceUpDownArrow(lsvCompany, txtSortColumn_Company, txtSortOrder_Company);
                    PlaceUpDownArrow(lsvEmployee, txtSortColumn_Employee, txtSortOrder_Employee);
             
                }
            }
            else
            {
                Helper.Url.Redirect(UrlConstants.LOGIN_PAGE, string.Empty);

            }
        }

        #endregion

        #region Button Event

        protected void btnGlobalSearch_Click(object sender, EventArgs e)
        {
            string url = Request.Url.AbsoluteUri;
            if (url.Contains("?"))
                url = url.Substring(0, url.IndexOf('?'));
            Url =MiscUtil .RemoveScript ( txtSearch.Text);
            Helper.Url.Redirect(url);
        }

        #endregion

        #region ListView Events

        protected void lsvCandidate_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                Candidate candidate = ((ListViewDataItem)e.Item).DataItem as Candidate;

                if (candidate != null)
                {
                    HyperLink lnkCandidateName = (HyperLink)e.Item.FindControl("lnkCandidateName");
                    Label lblPosition = (Label)e.Item.FindControl("lblPosition");
                    Label lblCandidateMobile = (Label)e.Item.FindControl("lblCandidateMobile");
                    LinkButton lblPrimaryEmail = (LinkButton)e.Item.FindControl("lblPrimaryEmail");
                   
                    if (candidate.Id > 0)
                    {
                        Member member = Facade.GetMemberById(candidate.Id);
                        string _strFullName = MiscUtil.GetMemberNameById(member.Id, Facade);
                        if (_strFullName.Trim() == "")
                            _strFullName = "No Candidate Name";
                        if (member != null)
                        {

                            if (_IsAccessToCandidate)
                                ControlHelper.SetHyperLink(lnkCandidateName, UrlForCandidate, string.Empty, _strFullName, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(member.Id), UrlConstants.PARAM_SITEMAP_ID, IdForCandidateSitemap.ToString());
                            else
                                lnkCandidateName.Text = _strFullName;// MiscUtil.GetMemberNameById(member.Id, Facade);
                            lblCandidateMobile.Text = member.CellPhone;
                            if (!_mailsetting)
                            {
                                lblPrimaryEmail.Text = "<a href=MailTo:" + member.PrimaryEmail +">" + member.PrimaryEmail +"</a>";
                            }
                            else
                            {
                                lblPrimaryEmail.Text = member.PrimaryEmail;
                                SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.Dashboard.DIR + UrlConstants.Dashboard.NEWMAIL, string.Empty, UrlConstants.PARAM_MEMBER_ID, member.Id.ToString());
                                lblPrimaryEmail.OnClientClick = "window.open('" + url + "','NewMail','height=670,width=770,toolbar=no,directories=no,status=no,linemenubar=no,scrollbars=no,resizable=no,modal=no');";
                            }

                            MemberExtendedInformation memberExtendedInformation = Facade.GetMemberExtendedInformationByMemberId(candidate.Id);

                            if (memberExtendedInformation != null)
                            {
                                lblPosition.Text = memberExtendedInformation.CurrentPosition;
                            }
                        }
                    }
                }
            }
        }

        protected void lsvEmployee_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                Employee employee = ((ListViewDataItem)e.Item).DataItem as Employee;

                if (employee != null)
                {
                    HyperLink lnkEmployeeName = (HyperLink)e.Item.FindControl("lnkEmployeeName");
                    Label lblPosition = (Label)e.Item.FindControl("lblPosition");
                    Label lblEmployeeMobile = (Label)e.Item.FindControl("lblEmployeeMobile");
                    LinkButton lblPrimaryEmail = (LinkButton)e.Item.FindControl("lblPrimaryEmail");

                    if (employee.Id > 0)
                    {
                        Member member = Facade.GetMemberById(employee.Id);

                        if (member != null)
                        {
                            if (_IsAccessToEmployee)
                                ControlHelper.SetHyperLink(lnkEmployeeName, UrlForEmployee, string.Empty, MiscUtil.GetMemberNameById(member.Id, Facade), UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(member.Id),UrlConstants .PARAM_SITEMAP_ID ,IdForEmployeeSitemap .ToString ());
                            else
                                lnkEmployeeName.Text = MiscUtil.GetMemberNameById(member.Id, Facade);
                            lblEmployeeMobile.Text = member.CellPhone;

                            if (!_mailsetting)
                            {
                                lblPrimaryEmail.Text = "<a href=MailTo:" + member.PrimaryEmail + ">" + member.PrimaryEmail + "</a>";
                            }
                            else
                            {
                                lblPrimaryEmail.Text = member.PrimaryEmail;
                                SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.Dashboard.DIR + UrlConstants.Dashboard.NEWMAIL, string.Empty, UrlConstants.PARAM_MEMBER_ID, member.Id.ToString());
                                lblPrimaryEmail.OnClientClick = "window.open('" + url + "','NewMail','height=670,width=770,toolbar=no,directories=no,status=no,linemenubar=no,scrollbars=no,resizable=no,modal=no');";
                            }
                            lblPosition.Text = employee.SystemAccess;
                        }
                    }
                }
            }
        }

        protected void lsvCompany_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                Company company = ((ListViewDataItem)e.Item).DataItem as Company;

                if (company != null)
                {
                    HyperLink lnkCompanyName = (HyperLink)e.Item.FindControl("lnkCompanyName");
                    Label lblOfficePhone = (Label)e.Item.FindControl("lblOfficePhone");
                    Label lblPrimaryContact = (Label)e.Item.FindControl("lblPrimaryContact");
                    LinkButton lblPrimaryEmail = (LinkButton)e.Item.FindControl("lblPrimaryEmail");

                    if (company.Id > 0)
                    {
                        if (_IsAccessToCompany)
                            ControlHelper.SetHyperLink(lnkCompanyName, UrlForCompany, string.Empty, company.CompanyName, UrlConstants.PARAM_COMPANY_ID, StringHelper.Convert(company.Id),UrlConstants .PARAM_SITEMAP_ID ,IdForCompanySitemap .ToString ());
                        else
                            lnkCompanyName.Text = company.CompanyName;
                        lblOfficePhone.Text = company.OfficePhone;
                        if (company.OfficePhoneExtension != "")
                        {
                            lblOfficePhone.Text += " x" + company.OfficePhoneExtension;
                        }
                        lblPrimaryContact.Text = company.PrimaryContact.FirstName + " " + company.PrimaryContact.LastName;


                        if (!_mailsetting)
                        {
                            lblPrimaryEmail.Text = "<a href=MailTo:" + company.PrimaryContact.Email + ">" + company.PrimaryContact.Email + "</a>";
                        }
                        else
                        {
                            lblPrimaryEmail.Text = company.PrimaryContact.Email;
                            SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.Dashboard.DIR + UrlConstants.Dashboard.NEWMAIL, string.Empty, UrlConstants.PARAM_COMPANY_ID, company.Id.ToString());
                            lblPrimaryEmail.OnClientClick = "window.open('" + url + "','NewMail','height=670,width=770,toolbar=no,directories=no,status=no,linemenubar=no,scrollbars=no,resizable=no,modal=no');";
                        }
                    }
                }
            }
        }
       
        protected void lsvCandidate_PreRender(object sender, EventArgs e)
        {
                ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvCandidate.FindControl("pagerControl_Candidate");
                 if (PagerControl != null)
                 {
                     DataPager pager = (DataPager)PagerControl.FindControl("pager");
                     if (pager != null)
                     {
                         DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                         if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                        
                     }
                     HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                     if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "GlobalCandidateListRowPerPage";

                     HiddenField hdnTotalRowCount = (HiddenField)PagerControl.FindControl("hdnTotalRowCount");
                    if(hdnTotalRowCount!=null) lblCandidateTotal.Text = "Candidate (" + hdnTotalRowCount.Value + ")";
                 }
                 PlaceUpDownArrow(lsvCandidate , txtSortColumn_Candidate, txtSortOrder_Candidate);
                 if (lsvCandidate.Controls.Count == 0)
                 {
                     lsvCandidate.DataSource = null;
                     lsvCandidate.DataBind();
                     lblCandidateTotal.Text = "Candidate (0)";
                 }
        }

        protected void lsvCompany_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvCompany.FindControl("pagerControl_Company");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "GlobalCompanyListRowPerPage";

                HiddenField hdnTotalRowCount = (HiddenField)PagerControl.FindControl("hdnTotalRowCount");
                if (hdnTotalRowCount != null) lblCompanyTotal.Text = "Company (" + hdnTotalRowCount.Value + ")";
            }
            PlaceUpDownArrow(lsvCompany , txtSortColumn_Company, txtSortOrder_Company);

            if (lsvCompany.Controls.Count == 0)
            {
                lsvCompany.DataSource = null;
                lsvCompany.DataBind();
                lblCompanyTotal.Text = "Company (0)";
            }
        }

        protected void lsvEmployee_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvEmployee.FindControl("pagerControl_Employee");
                if (PagerControl != null)
                {
                    DataPager pager = (DataPager)PagerControl.FindControl("pager");
                    if (pager != null)
                    {
                        DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                        if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                    }
                    HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                    if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "GlobalEmployeeListRowPerPage";

                    HiddenField hdnTotalRowCount = (HiddenField)PagerControl.FindControl("hdnTotalRowCount");
                    if (hdnTotalRowCount != null) lblEmployeeTotal.Text = "Employee (" + hdnTotalRowCount.Value + ")";
                }
                PlaceUpDownArrow(lsvEmployee, txtSortColumn_Employee, txtSortOrder_Employee);
                if (lsvEmployee.Controls.Count == 0)
                {
                    lsvEmployee.DataSource = null;
                    lsvEmployee.DataBind();
                    lblEmployeeTotal.Text = "Employee (0)";
                }
        }

        protected void lsvCandidate_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn_Candidate.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder_Candidate.Text == "ASC") txtSortOrder_Candidate.Text = "DESC";
                        else  txtSortOrder_Candidate.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn_Candidate.Text = lnkbutton.ID;
                        txtSortOrder_Candidate.Text = "ASC";
                    }

                    if (SortColumn_Candidate.Text == string.Empty || SortColumn_Candidate.Text != e.CommandArgument.ToString())
                        SortOrder_Candidate.Text = "asc";
                    else
                        SortOrder_Candidate.Text = SortOrder_Candidate.Text == "asc" ? "desc" : "asc";
                    SortColumn_Candidate.Text = e.CommandArgument.ToString();
                    odsCandidateQuickSearch.SelectParameters["SortOrder"].DefaultValue = SortOrder_Candidate.Text.ToString();
                    PlaceUpDownArrow(lsvCandidate, txtSortColumn_Candidate, txtSortOrder_Candidate);
                    PlaceUpDownArrow(lsvCompany, txtSortColumn_Company, txtSortOrder_Company);
                    PlaceUpDownArrow(lsvEmployee, txtSortColumn_Employee, txtSortOrder_Employee);
                }
            }
            catch
            {
            }
        }

        protected void lsvCompany_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn_Company.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder_Company.Text == "ASC") txtSortOrder_Company.Text = "DESC";
                        else txtSortOrder_Company.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn_Company.Text = lnkbutton.ID;
                        txtSortOrder_Company.Text = "ASC";
                    }
                    if (SortColumn_Company.Text == string.Empty || SortColumn_Company.Text != e.CommandArgument.ToString())
                        SortOrder_Company.Text = "asc";
                    else
                        SortOrder_Company.Text = SortOrder_Company.Text == "asc" ? "desc" : "asc";
                    SortColumn_Company.Text = e.CommandArgument.ToString();
                    odsCompanyQuickSearch.SelectParameters["SortOrder"].DefaultValue = SortOrder_Company.Text.ToString();
                    PlaceUpDownArrow(lsvCandidate, txtSortColumn_Candidate, txtSortOrder_Candidate);
                    PlaceUpDownArrow(lsvCompany, txtSortColumn_Company, txtSortOrder_Company);
                    PlaceUpDownArrow(lsvEmployee, txtSortColumn_Employee, txtSortOrder_Employee);
                }
            }
            catch
            {
            }
        }

        protected void lsvEmployee_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn_Employee.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder_Employee.Text == "ASC") txtSortOrder_Employee.Text = "DESC";
                        else txtSortOrder_Employee.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn_Employee.Text = lnkbutton.ID;
                        txtSortOrder_Employee.Text = "ASC";
                    }
                    if (SortColumn_Employee.Text == string.Empty || SortColumn_Employee.Text != e.CommandArgument.ToString())
                        SortOrder_Employee.Text = "asc";
                    else
                        SortOrder_Employee.Text = SortOrder_Employee.Text == "asc" ? "desc" : "asc";
                   SortColumn_Employee.Text = e.CommandArgument.ToString();
                   odsEmployeeQuickSearch.SelectParameters["SortOrder"].DefaultValue = SortOrder_Employee.Text.ToString();
                   PlaceUpDownArrow(lsvCandidate, txtSortColumn_Candidate, txtSortOrder_Candidate);
                   PlaceUpDownArrow(lsvCompany, txtSortColumn_Company, txtSortOrder_Company);
                   PlaceUpDownArrow(lsvEmployee, txtSortColumn_Employee, txtSortOrder_Employee);
                }
            }
            catch
            {
            }
        }

        private void PlaceUpDownArrow(ListView lv,TextBox tb_SortColumn,TextBox tb_SortOrder)
        {
            try
            {
                LinkButton lnk = (LinkButton)lv.FindControl(tb_SortColumn.Text);
                ImageButton imgUp = new ImageButton();
                imgUp.ID = "imgArrow";
                imgUp.Style.Add("disploay", "inline");
                imgUp.Style.Add("float", "right");
                lnk.Style.Add("float", "left");
                lnk.Style.Add("disploay", "inline");
                imgUp.Enabled = false;
                if (tb_SortOrder.Text == "ASC") imgUp.ImageUrl = "../Images/uparrow-header.gif";
                else imgUp.ImageUrl = "../Images/downarrow-header.gif";
                ImageButton img = (ImageButton)lnk.Parent.FindControl("imgArrow");
                if(img==null)  lnk.Parent.Controls.Add(imgUp);
               
            }
            catch
            {
            }

        }
        #endregion
        #endregion
    }
}
