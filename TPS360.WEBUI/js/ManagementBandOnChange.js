
//Global XMLHTTP Request object
var XmlHttp;
//Creating and setting the instance of appropriate XMLHTTP Request object to a �XmlHttp� variable  

function CreateXmlHttp()
{
	//Creating object of XMLHTTP in IE
	try
	{
		XmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
	}
	catch(e)
	{
		try
		{
			XmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		} 
		catch(oc)
		{
			XmlHttp = null;
		}
	}
	//Creating object of XMLHTTP in Mozilla and Safari 
	if(!XmlHttp && typeof XMLHttpRequest != "undefined") 
	{
		XmlHttp = new XMLHttpRequest();
	}
}

    var ddlBandList='';
    
    
function BandOnChange(ddlBand,hdnSelectedBand)
{
    $('#' +hdnSelectedBand ).val($('#' + ddlBand ).val());
}
function ManagementOnChange(ddlManagementBand,ddlBand,hdnSelectedMBand) 
{
    ddlBandList =ddlBand ;
    $('#' +hdnSelectedMBand ).val($('#' + ddlManagementBand ).val());
	var ManagementBandList = document.getElementById (ddlManagementBand );
    var selectedMB= ManagementBandList.options[ManagementBandList.selectedIndex].value;
    var requestUrl = "../AJAXServers/ManagementBandAjaxServer.aspx?SelectedMBId=" + encodeURIComponent(selectedMB)  ;
	CreateXmlHttp();
	if(XmlHttp)
	{
		XmlHttp.onreadystatechange = HandleResponseCompanyRequisition;
		XmlHttp.open("GET", requestUrl,  true);
		XmlHttp.send(null);		
	}
}


function HandleResponseCompanyRequisition()
{	
	if(XmlHttp.readyState == 4)
	{

		if(XmlHttp.status == 200)
		{			
			
			ClearAndSetRequisitionListItems(XmlHttp.responseText);
					
		}
		else
		{
			alert("There was a problem retrieving data from the server." );
		}
	}
}

function ClearAndSetRequisitionListItems(CompanyNode)
{

     var BandList = document.getElementById(ddlBandList );
      if( BandList.options.length>0)
      {
	    for (var count = BandList.options.length-1; count >-1; count--)
	    {
		    BandList.options[count] = null;
	    }
      }
   
    xmlDoc = $.parseXML( CompanyNode );
    var $xml = $( xmlDoc );
    $xml.find( "Requisition" ).each(function (){
        optionItem = new Option( $(this).find('Name').text() , $(this).find('Id').text());
		BandList.options[BandList.length] = optionItem;
   
    });
    
    if( BandList.options.length==1)
	{

	     BandList .options[0].text="No Band";
	     BandList .disabled=true ;
	}
	else
	{
	     BandList.options[0].text="Please Select" ;
	     BandList .disabled =false;
	}

}
