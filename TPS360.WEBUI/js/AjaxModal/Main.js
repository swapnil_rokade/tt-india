﻿/// --------------------------------------------------
/// mainScreen object
/// --------------------------------------------------
var mainScreen =
{
    result : null,                      // Page method execution result
    mainModalExtender : null,           // modalExtender object on main page
   // mainModalTitleSpan : null,          // title span object
    mainModalContentsDiv : null,        // div inside modal dialog
    activityImg : "activitynew.gif",
    pageTheme : "Default",
    activityImgObj : null,
    styleSheets : null,
    RowId : null,
    ModalTitle : null

}

mainScreen.Init = function() {
    /// <summary>
    /// Initializes mainScreen variables
    /// </summary>
    this.mainModalExtender = $find('mbMain');
  //  this.mainModalTitleSpan = $get("spanTitle");
    this.mainModalContentsDiv =  $get("mainModalContents");
    if(document.images) {
        this.activityImgObj = new Image(220,19);
        this.activityImgObj.src = "../Images/"+this.activityImg;
    }
    this.styleSheets = new Array();
};
mainScreen.ShowModal = function(_title, _html) {
    /// <summary>
    /// Shows modal dialog with contents equal to _html
    /// </summary>
    /// <param name="_title">Title of modal popup</param>
    /// <param name="_html">HTML that should be shown inside popup</param>
   // this.mainModalTitleSpan.innerHTML = _title;
    this.mainModalContentsDiv.innerHTML = _html;
    this.mainModalExtender.show();
    

   
};
mainScreen.CancelModal = function() {
    /// <summary>
    /// Hides modal dialog 
    /// </summary>
    this.mainModalExtender.hide();
    var _path;
    for(_path in this.styleSheets) {
        document.getElementsByTagName("head")[0].removeChild(this.styleSheets[_path]);
        delete this.styleSheets[_path];
    }
};
mainScreen.LoadServerControlHtml = function(_title, _obj, _callback) {
    /// <summary>
    /// Loads Server user control to the modal dialog 
    /// </summary>
    /// <param name="_title">Title of modal popup</param>
    /// <param name="_obj">
    /// object that we pass to the server
    /// </param>
    
    mainScreen.ShowModal(
        _title,
        (mainScreen.activityImgObj)
        ?
        ("<div style='padding-top:175px'><center><img src='" + mainScreen.activityImgObj.src + "' /></center></div>")
        :
        ""
        );
    mainScreen.ExecuteCommand(
        'GetWizardPage', 
        _callback, 
        _obj
        );
}
mainScreen.ExecuteCommand = function (
    methodName, 
    targetMethod, 
    parameters
    ) {
    PageMethods.ExecuteCommand(
        methodName, 
        targetMethod, 
        parameters, 
        mainScreen.ExecuteCommandCallback, 
        mainScreen.ExecuteCommandFailed);
};



mainScreen.SaveHiringNotesClicked=function ()
{
var MemberId=document .getElementById ('ctl00_hfMemberId');
var CurrentMemberId=document .getElementById ('ctl00_hfCurrentMemberId');
 var txtHiringNotes=document .getElementById ('ctl00_txtHiringNotes');
 var hfJobPostingId=document .getElementById ('ctl00_hfJobPostingId');
 var OldrNote=document .getElementById ('ctl00_hfNote');
 mainScreen .SaveHiringNotes ( {'HiringNotes':txtHiringNotes .value,'Member_Id':MemberId .value,'UpdatorId':CurrentMemberId .value ,'JobPostingId':hfJobPostingId .value,'OlderNote':OldrNote .value});
 mainScreen .CancelModal ();
};

mainScreen.SaveSourceClicked=function ()
{
var MemberId=document .getElementById ('ctl00_hfMemberId');
 var ddlEmployee=document .getElementById ('ctl00_ddlEmployee');
 var hfJobPostingId=document .getElementById ('ctl00_hfJobPostingId');
  var hfCurrentMemberId=document .getElementById ('ctl00_hfCurrentMemberId');
mainScreen.SaveHiringMatrixSource( {'SelectedSource':ddlEmployee.options[ddlEmployee.selectedIndex].value,'Member_Id':MemberId .value,'JobPostingId':hfJobPostingId .value,'CurrentMember':hfCurrentMemberId.value});
 mainScreen .CancelModal ();
};




mainScreen .SaveHiringNotes =function (parameters)
{
        PageMethods.SaveHiringNotes(
        parameters, 
        mainScreen.SaveHiringNotesCallback, 
        mainScreen.ExecuteCommandFailed);
};
mainScreen.SaveHiringMatrixSource=function (parameters)
{

        PageMethods.SaveHiringMatrixSource(
        parameters, 
        mainScreen.SaveHiringMatrixSourceCallback, 
        mainScreen.ExecuteCommandFailed);
};


mainScreen.SaveHiringMatrixSourceCallback= function (result ) 
{  if(result)
   {
       try
        {
                var lblmessage='ctl00_cphHomeMaster_lblMessage';
                if(result [1]!='')
                {
                      ShowMessage(lblmessage ,'Successfully changed Candidate source.');
                      document .getElementById ( mainScreen .RowId  ).innerHTML= result [0];
              
                }
        }
       catch(err) 
       {
       }
    }
};
mainScreen.SaveHiringNotesCallback = function (result ) 
{ 

    if(result) {
        try {
        
 
        var hiringnotes=document .getElementById (mainScreen.RowId);
        var memberid=mainScreen.RowId.substr(13,mainScreen.RowId.length-13);
        var note=document .getElementById ('hfHiringnote'+memberid  );
        var updatorname=document .getElementById ('hfUpdatorName'+memberid  );
        var updatetime=document .getElementById ('hfUpdateTime'+memberid  );
        var lblmessage='ctl00_cphHomeMaster_lblMessage';
         if(result [1]!='')
         {
                ShowMessage(lblmessage ,'Successfully saved hiring notes.');
                hiringnotes.innerHTML=result [0]+"<br/>"+"Last updated by "+result [1]+" on "+result [2]+" "+result [3]+".<br/>";
                note .value=result [0];
                updatorname .value=result [1];
                updatetime .value=result [2]+" "+result [3];
            }
        } catch(err) {
          alert (err.message)  ; 
        }
    }
};

mainScreen.ExecuteCommandCallback = function (
    result
    ) { 
    /// <summary>
    /// Is called when server sent result back
    /// </summary>
    /// <param name="result">Result of calling server command</param>
    if(result) {
        try {
            mainScreen.result = result[0];
          
            eval(result[1]+"(mainScreen.result);");
        } catch(err) {
            ; // TODO: Add error handling
        }
    }
};
mainScreen.ExecuteCommandFailed = function (
    error, 
    userContext, 
    methodName
    ) {
    /// <summary>
    /// Callback function invoked on failure of the page method 
    /// </summary>
    /// <param name="error">error object containing error</param>
    /// <param name="userContext">userContext object</param>
    /// <param name="methodName">methodName object</param>
    if(error) {
        ;// TODO: add error handling, and show it to the user
    }
};
mainScreen.LoadStyleSheet = function(_path) {
    if(!this.styleSheets[_path]) {
        var styleSheet;
        styleSheet=document.createElement('link');
        styleSheet.type="text/css";
        styleSheet.rel='stylesheet';
        styleSheet.href = _path;
        document.getElementsByTagName("head")[0].appendChild(styleSheet);
        this.styleSheets[_path] = styleSheet;
    }
};


/// --------------------------------------------------
/// Page events processing
/// --------------------------------------------------

Sys.Application.add_load(applicationLoadHandler);
Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequestHandler);
Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequestHandler);

function applicationLoadHandler() {
    /// <summary>
    /// Raised after all scripts have been loaded and 
    /// the objects in the application have been created 
    /// and initialized.
    /// </summary>
    mainScreen.Init()
}

function endRequestHandler() {
    /// <summary>
    /// Raised before processing of an asynchronous 
    /// postback starts and the postback request is 
    /// sent to the server.
    /// </summary>
    
    // TODO: Add your custom processing for event
}

function beginRequestHandler() {
    /// <summary>
    /// Raised after an asynchronous postback is 
    /// finished and control has been returned 
    /// to the browser.
    /// </summary>

    // TODO: Add your custom processing for event
}