
//Global XMLHTTP Request object
var XmlHttp;
//Creating and setting the instance of appropriate XMLHTTP Request object to a �XmlHttp� variable  

function CreateXmlHttp()
{
	//Creating object of XMLHTTP in IE
	try
	{
		XmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
	}
	catch(e)
	{
		try
		{
			XmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		} 
		catch(oc)
		{
			XmlHttp = null;
		}
	}
	//Creating object of XMLHTTP in Mozilla and Safari 
	if(!XmlHttp && typeof XMLHttpRequest != "undefined") 
	{
		XmlHttp = new XMLHttpRequest();
	}
}

    var ddlGrade='';
    
    
function OfferGradeOnChange(ddlOfferGrade,hdnSelectedOfferGrade)
{
    $('#' +hdnSelectedOfferGrade ).val($('#' + ddlOfferGrade ).val());
}
function OfferCategoryOnChange(ddlOfferCat,ddlOfferGrade, hdnSelectedOfferGrade,divEmbedded,divMech) 
{
    
    ddlGrade =ddlOfferGrade ;
    $('#' +hdnSelectedOfferGrade ).val('');
	var ddlOfferCategory = document.getElementById (ddlOfferCat );
	
	//$('#'+ divMech ).slideToggle("slow",function (){$('#'+ divEmbedded  ).slideToggle("slow")});
	if(ddlOfferCategory .selectedIndex==0)
	{
	    $('#'+ divMech ).fadeOut(function(){$('#'+ divEmbedded  ).fadeIn();});
	}
	else 
	{
	    $('#'+ divEmbedded ).fadeOut(function(){$('#'+ divMech  ).fadeIn();});
	}
	
    var selectedCategory= ddlOfferCategory.options[ddlOfferCategory .selectedIndex].value;
    var requestUrl = "../AJAXServers/OfferedDetailsAjaxServer.aspx?SelectedCategory=" + encodeURIComponent(selectedCategory);
	CreateXmlHttp();
	if(XmlHttp)
	{
		XmlHttp.onreadystatechange = HandleResponseOfferCategoryChanged;
		XmlHttp.open("GET", requestUrl,  true);
		XmlHttp.send(null);		
	}
}


function HandleResponseOfferCategoryChanged()
{	
	if(XmlHttp.readyState == 4)
	{
		if(XmlHttp.status == 200)
		{			
			ClearAndSetOfferGrade(XmlHttp.responseText);
		}
		else
		{
			alert("There was a problem retrieving data from the server." );
		}
	}
}

function ClearAndSetOfferGrade(GradeNode)
{

     var GradeList = document.getElementById(ddlGrade );
      if( GradeList.options.length>0)
      {
	    for (var count = GradeList.options.length-1; count >-1; count--)
	    {
		    GradeList.options[count] = null;
	    }
      }
   
   // $("#"+ddlGrade).next().find('.chzn-results').html('');
    xmlDoc = $.parseXML( GradeNode );
    var $xml = $( xmlDoc );
    
    $xml.find( "OfferGrade" ).each(function (){
        optionItem = new Option( $(this).find('Name').text() , $(this).find('Id').text());
		GradeList.options[GradeList.length] = optionItem;
		
//		$("#"+ddlGrade).next().remove();
//		    $("#"+ddlGrade).removeClass('chzn-done');
		    
		   
    });
//    
//    if( ReqList.options.length==1)
//	{

//	     ReqList .options[0].text="No Requisitions";
//	     ReqList .disabled=true ;
//	     $('.chzn-container').attr('disabled', true);
//	}
//	else
//	{
//	     ReqList.options[0].text="Select Requisition" ;
//	     ReqList .disabled =false;
//	      $('.chzn-container').attr('disabled', false);
//	}
//	$(".chzn-select").chosen();

}
