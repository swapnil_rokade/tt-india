﻿<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="LookUpEditor.aspx.cs"
    Inherits="TPS360.Web.UI.Admin.LookUpEditor" Title="LookUp Editor" %>
<%@ Register Src="../Controls/LookUpEditor.ascx" TagName="LookUpEditor" TagPrefix="uc1" %>
<%@ Register Src="../Controls/LookUpList.ascx" TagName="LookUpList" TagPrefix="uc2" %>

<asp:Content ID="cntEditorHeader" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="cntEditorTitle" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
Field Lookup Editor
</asp:Content>
<asp:Content ID="cntEditor" ContentPlaceHolderID="cphHomeMaster" runat="Server">
    <div class="GridContainer" style="width: 100%;">
        
                            <uc1:LookUpEditor ID="ctrlLookUpEditor" runat="server" />
                            <uc2:LookUpList ID="ctrlLookupList" runat="server" />
        
    </div>
</asp:Content>