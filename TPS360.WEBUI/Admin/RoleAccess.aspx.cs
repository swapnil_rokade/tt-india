﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: RoleAccess.aspx.cs
    Description: This page is used for change the previlages of role 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Oct-16-2008           Anand Dixit         Defect ID: 9002; Modified function SaveMenuAccess. 
                                                             OnClientClick event of btnSave is assign to ConfirmAction method (Scripts/Common.js)
    0.2            Jan-02-2009           Yogeesh Bhat        Defect ID:9631; Changes made in SaveMenuAccess() method; checked memberList for null.
    0.3            Jun-18-2009           Gopala Swamy J      Defect ID:10721; Changed the Message content;
    0.4            Jun-29-2009              Veda             Defect ID:10511; Modified Default Sitemap 
-------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using System.Collections;

namespace TPS360.Web.UI.Admin
{
    public partial class RoleAccess : AdminBasePage
    {
        #region Private variable

        CustomRole _customRole;

        #endregion

        #region Properties

        private CustomRole CurrentCustomRole
        {
            get
            {
                int id = 0;

                if (_customRole == null)
                {
                    if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_ID]))
                    {
                        id = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_ID]);
                    }
                    else
                    {
                        id = Convert.ToInt32(ddlRole.SelectedValue);
                    }
                    
                    if (id > 0)
                    {
                        _customRole = Facade.GetCustomRoleById(id);
                    }

                    if (_customRole == null)
                    {
                        _customRole = new CustomRole();
                    }
                }

                return _customRole;
            }
        }

        #endregion

        #region Methods

        private CustomRole BuildCustomRole()
        {
            CustomRole roleEditor = CurrentCustomRole;
            
            roleEditor.CreatorId = CurrentMember.Id;
            roleEditor.UpdatorId = CurrentMember.Id;

            return roleEditor;
        }

        private void SaveCustomRole()
        {
            if (IsValid)
            {
                try
                {
                    CustomRole customRole = BuildCustomRole();

                    if (!customRole.IsNew)
                    {
                        SaveMenuAccess(customRole);
                        MiscUtil.ShowMessage(lblMessage, "Privilege has been updated successfully.", false);
                    }
                    else
                    {
                        MiscUtil.ShowMessage(lblMessage, "Failed to update privilege.", true);
                    }
                }
                catch (ArgumentException ex)
                {
                    MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                }
            }
        }

        private void SaveMenuAccess(CustomRole customRole)
        {
            TreeNodeCollection checkedNodes = ctlMenuSelector.SiteMapTree.CheckedNodes;
            IList<Member> memberList = Facade.GetAllMemberByCustomrRoleId(customRole.Id);

            if (memberList != null)     
            {
                foreach (Member m in memberList)
                {
                    Facade.DeleteMemberPrivilegeByMemberId(m.Id);
                }
            }

            Facade.DeleteCustomRolePrivilegeByRoleId(customRole.Id);
            
            foreach (TreeNode node in checkedNodes)
            {
                CustomRolePrivilege customRolePrivilege = new CustomRolePrivilege();
                 
                customRolePrivilege.CustomSiteMapId = Convert.ToInt32(node.Value);
                customRolePrivilege.CustomRoleId = customRole.Id;
                Facade.AddCustomRolePrivilege(customRolePrivilege);

                if (memberList != null)     
                {
                    foreach (Member m in memberList)
                    {
                        MemberPrivilege memberPrivilege = new MemberPrivilege();

                        memberPrivilege.CustomSiteMapId = Convert.ToInt32(node.Value);
                        memberPrivilege.MemberId = m.Id;
                        Facade.AddMemberPrivilege(memberPrivilege);
                    }
                }
            }
        }

        private void PrepareViewForRole(int RoleId)
        {
            ctlMenuSelector.SelectList = Facade.GetAllCustomRolePrivilegeIdsByRoleId(RoleId);
            ctlMenuSelector.CurrentSiteMapTypes = new SiteMapType[] 
                                                                { 
                                                                    SiteMapType.ApplicationTopMenu,
                                                                    SiteMapType.EmployeeOverviewMenu,
                                                                    SiteMapType.CandidateOverviewMenu,
                                                                    SiteMapType.CompanyOverviewMenu,
                                                                    SiteMapType.EmployeePortalMenu, 
                                                                    SiteMapType.CandidateCareerPortalMenu, 
                                                                    SiteMapType .DepartmentOverviewMenu ,
                                                                    SiteMapType .VendorProfileMenu,
                                                                    SiteMapType .VendorPortalTopMenu ,
                                                                    SiteMapType .CandidateProfileMenuForVendor 
                                                                };
            ctlMenuSelector.BindList();
            btnSave.Visible = true;
        }

        #endregion

        #region Events
        
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (!IsPostBack)
            {
                MiscUtil.PopulateCustomRole(ddlRole, Facade);
                btnSave.Visible = false;
            }
            btnSave.OnClientClick = "return ConfirmAction('Warning: Access privileges for users in this role will be reset. Proceed?')"; //0.3
            
        }

        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlRole.SelectedValue != "" && ddlRole.SelectedValue!="Please Select")
            {
                PrepareViewForRole(Convert.ToInt32(ddlRole.SelectedValue));
            }
            else
            {
                ctlMenuSelector.SelectList = null;
                ctlMenuSelector.CurrentSiteMapTypes = null;

                ctlMenuSelector.BindList();
                btnSave.Visible = false;
            }

            pnlDynamic.Update();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveCustomRole();
        }
        
        #endregion
    }
}