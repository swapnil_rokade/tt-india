﻿<%@ Page Language="C#" MasterPageFile="~/Default.master" EnableEventValidation="false"
    AutoEventWireup="true" CodeFile="RoleAccess.aspx.cs" Inherits="TPS360.Web.UI.Admin.RoleAccess"
    Title="Role Access" %>

<%@ Register Src="../Controls/MenuSelector.ascx" TagName="MenuSelector" TagPrefix="uc1" %>
<asp:Content ID="cntRoleAccessEditorHeader" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="cntRoleAccessEditorTitle" ContentPlaceHolderID="cphHomeMasterTitle"
    runat="Server">
    Role Access Editor
</asp:Content>
<asp:Content ID="cntRoleAccessEditor" ContentPlaceHolderID="cphHomeMaster" runat="Server">
    <asp:UpdatePanel ID="pnlDynamic" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="TableRow" style="text-align: center">
                <asp:Label ID="lblMessage" EnableViewState="false" runat="server"></asp:Label>
            </div>
            <div class="TableRow">
            <div class="alert alert-info" style="width:300px;margin: 10px auto 10px auto;">
                Select a role to edit access permissions.
            </div>
                <div class="TableFormLeble">
                    <asp:Label ID="lblRole" runat="server" Text="Role"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:DropDownList ID="ddlRole" CssClass="CommonDropDownList" runat="server" AutoPostBack="true"
                        Width="200" DataValueField="RoleID" EnableViewState="true" OnSelectedIndexChanged="ddlRole_SelectedIndexChanged" />
                </div>
            </div>
            <asp:Panel ID="pnlSaveMenuAccess" runat="server">
                <div class="TableRow" style="text-align: center;min-height:100px;">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblMenuAccess" runat="server" Text="Application Access"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <uc1:MenuSelector ID="ctlMenuSelector" runat="server" />
                    </div>
                </div>
                <br />
                <div class="TableRow" style="text-align: center">
                    <asp:Button ID="btnSave" CssClass="CommonButton" runat="server" Text="Save" OnClick="btnSave_Click" />
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
