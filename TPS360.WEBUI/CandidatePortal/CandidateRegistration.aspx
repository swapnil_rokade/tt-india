﻿<%@ Page Language="C#" MasterPageFile="~/CandidatePortal/Portal.master" AutoEventWireup="true" CodeFile="CandidateRegistration.aspx.cs" EnableEventValidation ="false" 
    Inherits="TPS360.Web.UI.CandidateRegistration" Title="New Candidate Registration" %>

<%@ Register Src="~/Controls/RegistrationInternal.ascx" TagName="regI" TagPrefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphHomeMasterTitle" Runat="Server">
<asp:Label ID="titleContainer"  runat ="server" Text ="New Candidate Registration" ></asp:Label> <%--line added by pravin khot using title format change--%>
 <%-- New Candidate Registration--%>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHomeMaster" Runat="Server">
<div style =" padding : 18px;">
    <asp:UpdatePanel ID="pnlRegistrationInformation" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divRegInternal" runat="server">
                 <uc1:regI ID="rgInternal" runat="server" />                                      
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </div>
</asp:Content>

