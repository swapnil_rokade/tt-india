﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System.Collections.Generic;
using System.Drawing.Text;
using TPS360.BusinessFacade;
using TPS360.Common;

namespace TPS360.Web.UI
{
    public partial class CandidateRegistration : BasePage     //System .Web .UI .Page  change by pravin khot replace : BasePage
    {
        #region Veriables

        #endregion

        #region Properties


        #endregion

        #region Methods



        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {

            rgInternal.Role = ContextConstants.ROLE_CANDIDATE;
            //**************** Code added by pravin khot on 28/Jan/2016************************
            if (!IsPostBack)
            {

                IFacade facade = new Facade();
                int JobId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID]);
                JobPosting jobposting;
                if (JobId > 0)
                {
                    jobposting = Facade.GetJobPostingById(JobId);
                    titleContainer.Text = titleContainer.Text + " - " + "APPLY FOR" + " - " + jobposting.JobTitle + " [JOB CODE-" + jobposting.JobPostingCode + "]";

                }

                else
                {
                    titleContainer.Text = titleContainer.Text;  //line introduced by Prasanth on 22/Dec/2015

                }
                //**********************************End********************************************
            }


            
        }

        #endregion
    }
}