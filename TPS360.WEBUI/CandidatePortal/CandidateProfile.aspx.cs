﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Common.Shared;
namespace TPS360.Web.UI
{
    public partial class CandidateProfile : BasePage 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
         
        }
        private void Page_PreRender(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                hdnCandidateProfile.Value = MiscUtil.GetMemberNameById(CurrentMember.Id, Facade);

            }
            this.Page.Title = hdnCandidateProfile.Value + " - Profile Builder";
        }
    }
}