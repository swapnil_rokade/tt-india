﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using TPS360.Providers;
using System.Text;
namespace TPS360.Web.UI
{
    public partial class PortalPublic : BaseMasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Url.ToString().ToLower().Contains("/vendor/"))
                A1.Attributes.Add("href", "~/Vendor/Login.aspx");
        }
    }
}