﻿<%@ Page Language="C#" MasterPageFile="~/CandidatePortal/CandidatePortalLeft.master" AutoEventWireup="true" CodeFile="MyJobs.aspx.cs" Inherits="TPS360.Web.UI.MyJobs" Title="Untitled Page" %>
<%@ Register Src ="~/Controls/PagerControl.ascx" TagName ="Pager" TagPrefix ="ucl" %>

<%-- Add content controls here --%>

<asp:Content ID="Content3" ContentPlaceHolderID="cphCandidateMaster" Runat="Server">

        <div style =" min-height : 300px;" >
        
        <asp:UpdatePanel ID="upAppliedJobsList" runat ="server" >
<ContentTemplate >
<asp:HiddenField ID="hdnMyJobs" runat="server" />
 <asp:ObjectDataSource ID="odsRequisitionList" runat="server" SelectMethod="GetPaged"
    OnSelecting="odsRequisitionList_Selecting" TypeName="TPS360.Web.UI.MemberJobAppliedDataSource"
    SelectCountMethod="GetListCount" EnablePaging="True" SortParameterName="sortExpression">
    <SelectParameters>
        <asp:Parameter Name="memberId" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:HiddenField ID="hdnSortColumn" runat="server"  />
<asp:HiddenField ID="hdnSortOrder" runat="server" />
<div class="GridContainer" style =" padding-top : 5px;">
    <div style="overflow: auto; overflow-y: hidden" id="bigDiv" onscroll='SetScrollPosition()'>
        <asp:ListView ID="lsvJobPosting" runat="server" EnableViewState="true" DataKeyNames="Id"
            OnItemDataBound="lsvJobPosting_ItemDataBound" OnItemCommand="lsvJobPosting_ItemCommand"
            OnPreRender="lsvJobPosting_PreRender" DataSourceID="odsRequisitionList">
            <LayoutTemplate>
                <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                    <tr id="trHeadLevel" runat="server">
                        <th style="white-space: nowrap; width : 100px !important;  ">
                            <asp:LinkButton ID="btnPostedDate" runat="server" CommandName="Sort" CommandArgument="[J].[PostedDate]"
                                Width="60%" Text="Date Posted" TabIndex="2" />
                        </th>
                        <th style="white-space: nowrap; min-width: 60px">
                            <asp:LinkButton ID="btnJobTitle" runat="server" CommandName="Sort" CommandArgument="[J].[JobTitle]"
                                Width="50%" Text="Job Title" TabIndex="5" />
                        </th>
                        <th style="white-space: nowrap; min-width: 60px">
                            <asp:LinkButton ID="btnCity" runat="server" CommandName="Sort" CommandArgument="[J].[City]"
                                Width="50%" Text="Location" TabIndex="6" />
                        </th>
                        <th style="text-align: center; white-space: nowrap; " id="thAction" runat="server">
                            <asp:LinkButton ID="lnkStatus" runat="server" CommandName="Sort" CommandArgument="[JA].[CreateDate]"
                                 Text="Status" TabIndex="6" />
                        </th>
                    </tr>
                    
                    <tr id="itemPlaceholder" runat="server">
                    </tr>
                    <tr class="Pager">
                        <td colspan="4" runat="server" id="tdPager">
                            <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true"  />
                        </td>
                    </tr>
                </table>
            </LayoutTemplate>
            <EmptyDataTemplate>
                <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                    <tr>
                        <td>
                            No data was returned.
                        </td>
                    </tr>
                </table>
            </EmptyDataTemplate>
            <ItemTemplate>
                <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                    <td>
                        <asp:Label ID="lblPostedDate" runat="server" Width="80px" />
                    </td>
                  
                    <td>
                        <asp:HyperLink ID="lnkJobTitle" runat="server" Target="_blank" TabIndex="8" Width="120px"></asp:HyperLink>
                    </td>
                    <td>
                        <asp:Label ID="lblCity" runat="server" Width="80px" />
                    </td>
                    <td  runat="server" id="tdAction"
                        enableviewstate="true">
                          <asp:Label ID="lblStatus" runat ="server"></asp:Label>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
    </div>


</div>


                        
                        
</ContentTemplate>
</asp:UpdatePanel>
        
        </div>   
</asp:Content>

