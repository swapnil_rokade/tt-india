﻿ <%-- -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CareerRequisitionList.aspx
    Description: using carrer page
    Created By: pravin khot
    Created On:  17/Jan/2016
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date           Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
  
-------------------------------------------------------------------------------------------------------------------------------------------       

--%>
<%@ Page Language="C#" MasterPageFile="~/CandidatePortal/Portal.master" AutoEventWireup="true" CodeFile="CareerPage.aspx.cs" Inherits="TPS360.Web.UI.CareerPage" Title="Career Page" EnableEventValidation ="false"  %>
<%@ Register Src ="~/Controls/CareerRequisitionList.ascx" TagName ="CareerPage" TagPrefix ="ucl"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">

Opportunities

</asp:Content>
<asp:Content ID="cntRequisitionList" ContentPlaceHolderID="cphHomeMaster" runat="Server">

<asp:UpdatePanel ID="pnlRequisition" runat="server" UpdateMode="Conditional">
 <ContentTemplate> 
 
<ucl:CareerPage ID="uclRequisition" runat ="server" />
</ContentTemplate>                               
</asp:UpdatePanel>

</asp:Content>
