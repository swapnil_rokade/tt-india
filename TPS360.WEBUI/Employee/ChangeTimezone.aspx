﻿<%@ Page Language="C#" MasterPageFile="~/Masters/MemberPortal.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="ChangeTimezone.aspx.cs" Inherits="TPS360.Web.UI.ChangeTimezone"
    Title="Change Timezone" %>

<asp:Content ID="cntChangeTimezone" ContentPlaceHolderID="cphHomeMaster" runat="Server">

    <%--<script type="text/javascript" language="javascript">
function Remove()
{
document .getElementById ('<%=txtSMTPwd.ClientID %>').value="";
}
    </script>--%>

    <div class="TabPanelHeader">
        Change Timezone
    </div>
    <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
    <div style="text-align: left; width: 100%; padding-top: 50px;">
        <div runat="server" class="TableRow" visible="false">
            <div class="TableFormLeble" style="padding-left: 30px">
                <asp:CheckBox ID="chkSessionTime" runat="server" Text="Enable Session Timeout" Font-Bold="true"
                    onclick="javascript:CheckBoxCheck()"></asp:CheckBox>
            </div>
            <div class="TableFormContent">
                <asp:DropDownList ID="drpTimeoutValueList" runat="server" EnableViewState="true" AutoPostBack="false" />
            </div>
        </div>
        <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label EnableViewState="false" ID="lblchangetimezone" runat="server" Text="Select Timezone"></asp:Label>:
            </div>
            <div class="TableFormContent">
                <asp:DropDownList ID="ddlTimezone" runat="server" CssClass="CommonDropDown"
                    Width="333px">
                </asp:DropDownList>
               
            </div>
        </div>
         <div class="TableRow">
        <div class="TableFormValidatorContent" style="margin-left: 42%">
         <asp:CompareValidator ID="rfvTeamLeader" runat="server" ControlToValidate="ddlTimezone"
                            ErrorMessage="Please select TimeZone." EnableViewState="False" Display="Dynamic"
                            ValueToCompare="0" Operator="GreaterThan" ValidationGroup="TeamInfo"></asp:CompareValidator>
        </div>
        </div>
    </div>
    <br />
    <div class="TableRow">
        <div class="TableFormLeble">
        </div>
        <div class="TableFormContent">
            <asp:Button ID="btnSave" Text="Save" runat="server" CssClass="CommonButton" OnClick="btnSave_Click" ValidationGroup="TeamInfo" />
        </div>
    </div>
</asp:Content>
