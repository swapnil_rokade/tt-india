﻿<%@ Page Language="C#" MasterPageFile="~/Masters/EmployeeProfile.master" AutoEventWireup="true"
    CodeFile="InternalDocumentUpload.aspx.cs" Inherits="TPS360.Web.UI.EmployeeInternalDocumentUpload"
    Title="Employee Documents" %>

<%@ Register Src="~/Controls/MemberDocumentUploader.ascx" TagName="Document" TagPrefix="uc1" %>
<asp:Content ID="cntEmployeeDocumentsUpload" ContentPlaceHolderID="cphEmployeeMaster"
    runat="Server">
    <asp:UpdatePanel ID="upDocuent" runat="server">
        <ContentTemplate>
            <uc1:Document ID="ucntrlDocument" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
