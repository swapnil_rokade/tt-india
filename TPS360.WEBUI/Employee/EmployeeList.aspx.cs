﻿/* 
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: EmployeeList.ascx.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              23/Feb/2016           pravin khot          added condition - [else if (StatusSeclected == 3)] 
------------------------------------------------------------------------------------------------------------------------------------------- 
 */

using System;
using System.Collections.Generic;
using TPS360.Common.Helper;
using System.Web.Security;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.BusinessFacade;

namespace TPS360.Web.UI
{
    public partial class EmployeeList : EmployeeBasePage
    {
        [System.Web.Services.WebMethod]
        public static object[] EmployeeAccessStatusChange(object data) 
        {
            Dictionary<string, object> param = (Dictionary<string, object>)data;
            IFacade Facade = new Facade();
            int StatusSeclected = Convert.ToInt32(param["StatusId"]);
            int UpdatorId = Convert.ToInt32(param["UpdatorId"]);
            int MemberId = Convert.ToInt32(param["MemberId"]);

            Member member = Facade.GetMemberById(MemberId);

            if (StatusSeclected==1) 
            {
                MembershipUser thisuser = Membership.GetUser(member.UserId);
                if (thisuser.UnlockUser()) 
                {
                    member.Status = (int)AccessStatus.Enabled;
                    member.UpdatorId = UpdatorId;
                    Facade.UpdateMemberStatus(member);
                }
            }
            else if (StatusSeclected == 2)
            {
                if (Facade.LockUser(member.UserId))
                {
                    member.Status = (int)AccessStatus.Blocked;
                    member.UpdatorId = UpdatorId;
                    Facade.UpdateMemberStatus(member);
                }
            }
            //condition added by pravin khot on 23/Feb/2016*****
            else if (StatusSeclected == 3)
            {
                if (Facade.LockUser(member.UserId))
                {
                    member.Status = (int)AccessStatus.InActive;
                    member.UpdatorId = UpdatorId;
                    Facade.UpdateMemberStatus(member);
                }
            }
            //*************END***************************
            
            return null;
        }
        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            ucntrlEmployeeList.Status = 0;
        }

        #endregion

    }
}