﻿<%@ Page Language="C#" MasterPageFile ="~/Masters/MemberPortal.master" AutoEventWireup="true" CodeFile="SentEmailHistory.aspx.cs" Inherits="TPS360.Web.UI.EmployeeSentEmailHistory" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src ="~/Controls/SendEmailList.ascx" TagName ="EmailSent" TagPrefix ="uc1" %>
<asp:Content ID="cntSentEmailHistoryHeader" ContentPlaceHolderID="head"
    runat="Server">
  
</asp:Content>
<asp:Content ID="cntSentEmailHistory" ContentPlaceHolderID="cphHomeMaster"
    runat="Server">
        <script src ="../Scripts/jsUpdateProgress.js" type ="text/javascript" ></script>
<script language="javascript" type="text/javascript">
var ModalProgress ='<%= Modal.ClientID %>';

</script>
       <div class="TabPanelHeader" >
        Sent Emails
    </div>  
                                <asp:UpdatePanel ID="pnlSentEmailHistory" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <uc1:EmailSent ID ="ucSentEmailHistory" runat ="server" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                           
     <asp:Panel ID="pnlmodal" runat="server" style="display: none">
                    <asp:UpdatePanel runat="server" ID="UpdatePanel2" RenderMode="Inline" UpdateMode="Conditional">
                        <ContentTemplate>
                        <img src="../Images/AjaxLoading.gif" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID ="Modal" runat ="server" TargetControlID ="pnlmodal" PopupControlID ="pnlmodal"
             BackgroundCssClass ="divModalBackground" ></ajaxToolkit:ModalPopupExtender>
</asp:Content>
