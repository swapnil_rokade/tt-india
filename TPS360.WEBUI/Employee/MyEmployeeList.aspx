﻿<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="MyEmployeeList.aspx.cs"
    Inherits="TPS360.Web.UI.Employee.MyEmployeeList" Title="My User List" %>

<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc1" %>
<asp:Content ID="cntMemberEmployeeListHeader" ContentPlaceHolderID="cphHomeMasterTitle"
    runat="Server">
My User List
</asp:Content>
<asp:Content ID="cntEmployeeListBody" ContentPlaceHolderID="cphHomeMaster"
    runat="Server">
    <div style="text-align: left; width: 100%;">
        <asp:HiddenField ID="hfMemberId" runat="server" Value="0"/>
        <uc1:EmployeeList ID="ucntrlEmployeeList" IsMylist="True" IsTier="false" IsMapping="false" HeaderTitle="List of My Employees" runat="server" />
    </div>
</asp:Content>