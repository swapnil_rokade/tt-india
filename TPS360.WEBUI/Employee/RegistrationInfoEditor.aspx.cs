﻿using System;

namespace TPS360.Web.UI
{
    public partial class EmployeeRegistrationInfoEditor : EmployeeBasePage
    {
        #region Veriables

        #endregion

        #region Properties


        #endregion

        #region Methods



        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            {
                uctlInternal.Role = ContextConstants.ROLE_EMPLOYEE;
            }
        }

        #endregion
    }
}