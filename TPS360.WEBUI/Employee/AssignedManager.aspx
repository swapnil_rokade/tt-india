﻿<%@ Page Language="C#" MasterPageFile="~/Masters/EmployeeProfile.master" AutoEventWireup="true"
    CodeFile="AssignedManager.aspx.cs" Inherits="TPS360.Web.UI.CandidateList" Title="Employee Assigned Managers" %>

<%@ Register Src="~/Controls/MemberAssignedManager.ascx" TagName="AssignedManager"
    TagPrefix="uc1" %>
<asp:Content ID="cntEmployeeAssignedManagersBody" ContentPlaceHolderID="cphEmployeeMaster"
    runat="Server">
    <asp:HiddenField ID="hfMemberId" runat="server" Value="0" />
    <asp:UpdatePanel ID="pnlAssignedManagerList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <uc1:AssignedManager ID="ucntrlAssignedManagerList" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
