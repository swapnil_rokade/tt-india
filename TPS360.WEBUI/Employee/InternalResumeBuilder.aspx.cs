﻿using System;
using TPS360.Common.Helper;

namespace TPS360.Web.UI
{
    public partial class EmployeeInternalResumeBuilder : EmployeeBasePage
    {
        #region Veriables

        int _memberId = 0;
        
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
               
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
                {
                    _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
                }
                if (_memberId > 0)
                {
                    string name = MiscUtil.GetMemberNameById(_memberId, Facade);
                    this.Page.Title = name + " - " + "Profile Builder";
                }
            
            ucntrlBasicInfo.MemberRole = ContextConstants.ROLE_EMPLOYEE;
        }

        #endregion
    }
}