﻿<%@ Page Language="C#" MasterPageFile="~/Masters/EmployeeProfile.master" AutoEventWireup="true"
    CodeFile="TaskAndSchedule.aspx.cs" Inherits="TPS360.Web.UI.Employee.TaskAndSchedule" %>


<%@ Register Src="~/Dashboard/Controls/ScheduleAppoinment.ascx" TagName="Schedule"
    TagPrefix="ucc" %>
    
<asp:Content ID="cntContentList" ContentPlaceHolderID="cphEmployeeMaster" runat="Server">
<div class="TabPanelHeader">Schedule & Appoinments</div>
    <ucc:Schedule ID="cntrlSchedule" runat="server" />
</asp:Content>
