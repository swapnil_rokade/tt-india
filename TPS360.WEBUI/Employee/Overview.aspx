﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: Overview.aspx
    Description: This is the page which is used to display overview information of employee
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Oct-20-2008          Gopala Swamy          Defect id: 8988;Put javascript code to enlarge the window 
    0.2             Nov-18-2008          Gopala Swamy          Defect id:9156:Replace "l" to "L" 
    0.3             Apr-9-2010           Basavaraj A           Defect Id; 12531 , Changed from "avaiable" to "available" 
    0.4             Apr-21-2010          Sudarshan.R.          Defect Id:12711 Removed the scroll bar from pop-up window
    0.5             Apr-23-2010          Ganapati Bhat         Defect Id:12701 Code Added to Display Table header in each ListView
-------------------------------------------------------------------------------------------------------------------------------------------       
--%>

<%@ Page Language="C#" MasterPageFile="~/Masters/EmployeeProfile.master" AutoEventWireup="true"
    CodeFile="Overview.aspx.cs" Inherits="TPS360.Web.UI.EmployeeOverview" Title="Employee Overview" %>

<%@ Register Src="~/Controls/EmployeeOverviewHeader.ascx" TagName="Employee" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>

<%@ Register Src="~/Controls/EmployeeLandingTop.ascx" TagPrefix="ucl" TagName="TopLandingPage" %>
<%@ Register Src="~/Dashboard/Controls/UpcomingInterviews.ascx" TagName="MyInterviews"
    TagPrefix="ucl" %>
<asp:Content ID="cntMemberLandingPage" ContentPlaceHolderID="cphEmployeeMaster" runat="Server">

    <div class="TabPanelHeader">
                Overview
            </div>
    <div style="width: 100%; overflow: auto">
        <ucl:Employee ID="uclEmployee" runat="server" />
        
        
        <div class="tabbable">
            <!-- Only required for left/right tabs -->
            
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab1" id="tabone" data-toggle="tab">Requisitions</a></li>
                <li><a href="#tab2" data-toggle="tab" id="tabtwo">Candidates</a></li>
                <li><a href="#tab3" data-toggle="tab" id="tabthree">Upcoming Interviews</a></li>
                <li><a href="#tab4" data-toggle="tab" id="tabfour">Assigned Managers</a></li>
                <li><a href="#tab5" data-toggle="tab" id="tabfive">Documents</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab1">
                    <asp:UpdatePanel ID="upRequisition" runat="server">
                        <ContentTemplate>
                            <asp:ObjectDataSource ID="odsMyRequisitionList" runat="server" SelectMethod="GetPaged"
                                OnSelecting="odsMyRequisitionList_Selecting" TypeName="TPS360.Web.UI.JobPostingDataSource"
                                SelectCountMethod="GetListCount" EnablePaging="True" SortParameterName="sortExpression"
                                OnSelected="odsMyRequisitionList_Selected">
                                <SelectParameters>
                                    <asp:Parameter Name="IsTemplate" DefaultValue="False" Type="Boolean" Direction="Input" />
                                    <asp:Parameter Name="memberId" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                            <div class="GridContainer" style="text-align: left; padding-bottom: 20px;">
                                <asp:ListView ID="lsvRequisition" runat="server" DataSourceID="odsMyRequisitionList"
                                    DataKeyNames="Id" OnItemDataBound="lsvRequisition_ItemDataBound" OnPreRender="lsvRequisition_PreRender">
                                    <LayoutTemplate>
                                    
                                        <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                            <tr>
                                                <th style="white-space: nowrap;">
                                                    Job title
                                                </th>
                                                <th style="white-space: nowrap;">
                                                    Code
                                                </th>
                                                <th style="white-space: nowrap;">
                                                    Status
                                                </th>
                                                <th style="white-space: nowrap;">
                                                    Posted Date
                                                </th>
                                            </tr>
                                            <tr id="itemPlaceholder" runat="server">
                                            </tr>
                                            <tr class="Pager">
                                                <td colspan="4">
                                                    <ucl:Pager ID="pagerControl_Requisition" runat="server" EnableViewState="true" />
                                                </td>
                                            </tr>
                                        </table>
                                    </LayoutTemplate>
                                    <EmptyDataTemplate>
                                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                            <tr>
                                                <td>
                                                    No requisition posted yet.
                                                </td>
                                            </tr>
                                        </table>
                                    </EmptyDataTemplate>
                                    <ItemTemplate>
                                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                            <td>
                                                <asp:HyperLink ID="lblJobTitle" runat="server" Target="_blank" style=" cursor :pointer ;" />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblJobCode" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblJobstatus" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblJobPostedDate" runat="server" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="tab-pane" id="tab2">
                    <asp:UpdatePanel ID="upCandidate" runat="server">
                        <ContentTemplate>
                            <asp:ObjectDataSource ID="odsCandidateList" runat="server" SelectMethod="GetPaged"
                                TypeName="TPS360.Web.UI.CandidateDataSource" SelectCountMethod="GetListCount"
                                EnablePaging="True" OnSelecting="odsCandidateList_Selecting" SortParameterName="sortExpression"
                                OnSelected="odsCandidateList_Selected">
                                <SelectParameters>
                                    <asp:Parameter Name="memberId" DefaultValue="0" />
                                    <asp:Parameter Name="hotListManagerId" DefaultValue="0" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                            <div class="GridContainer" style="text-align: left; padding-bottom: 20px;">
                                <asp:ListView ID="lsvApplicantList" runat="server" DataSourceID="odsCandidateList"
                                    DataKeyNames="Id" OnItemDataBound="lsvApplicantList_ItemDataBound" OnPreRender="lsvApplicantList_PreRender">
                                    <LayoutTemplate>
                                   
                                        <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                            <tr>
                                                <th style="white-space: nowrap;">
                                                    Name
                                                </th>
                                                <th style="white-space: nowrap;">
                                                    Position
                                                </th>
                                                <th style="white-space: nowrap;">
                                                    Location
                                                </th>
                                            </tr>
                                            <tr id="itemPlaceholder" runat="server">
                                            </tr>
                                            <tr class="Pager">
                                                <td colspan="3">
                                                    <ucl:Pager ID="pagerControl_Candidate" runat="server" EnableViewState="true" />
                                                </td>
                                            </tr>
                                        </table>
                                    </LayoutTemplate>
                                    <EmptyDataTemplate>
                                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                            <tr>
                                                <td>
                                                    No Assigned Candidates.
                                                </td>
                                            </tr>
                                        </table>
                                    </EmptyDataTemplate>
                                    <ItemTemplate>
                                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                            <td>
                                                <asp:HyperLink ID="lblApplicantName" runat="server" Target="_blank" />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblApplicantPosition" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblApplicantLocation" runat="server" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="tab-pane" id="tab3">
                
                    <asp:UpdatePanel ID="upInterviews" runat="server">
                        <ContentTemplate>
                            <ucl:MyInterviews ID="uclMyInterviews" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="tab-pane" id="tab4">
                    <asp:UpdatePanel ID="upManagers" runat="server">
                        <ContentTemplate>
                            <asp:ObjectDataSource ID="OdsManager" runat="server" SelectMethod="GetPaged" TypeName="TPS360.Web.UI.MemberManagerDataSource"
                                SelectCountMethod="GetListCount" EnablePaging="True" OnSelecting="OdsassignedManager_Selecting"
                                SortParameterName="sortExpression" OnSelected="OdsManager_Selected">
                                <SelectParameters>
                                    <asp:Parameter Name="memberId" DefaultValue="0" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                            <div class="GridContainer" style="text-align: left; padding-bottom: 20px;">
                                <asp:ListView ID="lsvAssignedManager" runat="server" DataSourceID="OdsManager" DataKeyNames="Id"
                                    OnItemDataBound="lsvAssignedManager_ItemDataBound" OnPreRender="lsvAssignedManager_PreRender">
                                    <LayoutTemplate>
                                    
                                        <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0" width="98%">
                                            <tr>
                                                <th style="white-space: nowrap;">
                                                    Name
                                                </th>
                                                <th style="white-space: nowrap;">
                                                    Access Role
                                                </th>
                                            </tr>
                                            <tr id="itemPlaceholder" runat="server">
                                            </tr>
                                            <tr class="Pager">
                                                <td colspan="2">
                                                    <ucl:Pager ID="pagerControl_Manager" runat="server" EnableViewState="true" />
                                                </td>
                                            </tr>
                                        </table>
                                    </LayoutTemplate>
                                    <EmptyDataTemplate>
                                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                            <tr>
                                                <td>
                                                    No manager assigned yet.
                                                </td>
                                            </tr>
                                        </table>
                                    </EmptyDataTemplate>
                                    <ItemTemplate>
                                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                            <td>
                                                <asp:Label ID="lblAssignedManagerName" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblAssignedManagerAccessRole" runat="server" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="tab-pane" id="tab5">
                    <asp:UpdatePanel ID="upDocuments" runat="server">
                        <ContentTemplate>
                            <asp:ObjectDataSource ID="odsDocuments" runat="server" SelectMethod="GetPagedByMemberId"
                                OnSelecting="odsDocumentList_Selecting" TypeName="TPS360.Web.UI.MemberDocumentsDataSource"
                                SelectCountMethod="GetListCount" OnSelected="odsDocuments_Selected" EnablePaging="True"
                                SortParameterName="sortExpression">
                                <SelectParameters>
                                    <asp:Parameter Name="MemberID" DefaultValue="" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                            <div class="GridContainer" style="text-align: left; padding-bottom: 20px;">
                                <asp:ListView ID="lsvDocuments" runat="server" DataSourceID="odsDocuments" DataKeyNames="Id"
                                    OnItemDataBound="lsvDocuments_ItemDataBound" OnPreRender="lsvDocuments_PreRender">
                                    <LayoutTemplate>
                                    
                                        <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                            <tr>
                                                <th style="white-space: nowrap;">
                                                    Title
                                                </th>
                                                <th style="white-space: nowrap;">
                                                    Type
                                                </th>
                                                <th style="white-space: nowrap;">
                                                    FileName
                                                </th>
                                            </tr>
                                            <tr id="itemPlaceholder" runat="server">
                                            </tr>
                                            <tr class="Pager">
                                                <td colspan="3">
                                                    <ucl:Pager ID="pagerControl_Documents" runat="server" EnableViewState="true" />
                                                </td>
                                            </tr>
                                        </table>
                                    </LayoutTemplate>
                                    <EmptyDataTemplate>
                                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                            <tr>
                                                <td>
                                                    No Documents Uploaded.
                                                </td>
                                            </tr>
                                        </table>
                                    </EmptyDataTemplate>
                                    <ItemTemplate>
                                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                            <td>
                                                <asp:Label ID="lblDocTitle" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblDocType" runat="server" />
                                            </td>
                                            <td>
                                                <asp:HyperLink ID="hlnkDocFileName" runat="server" Target="_blank" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
    </div>
</asp:Content>
