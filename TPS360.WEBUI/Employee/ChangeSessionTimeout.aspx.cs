﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace TPS360.Web.UI
{
    public partial class ChangeSessionTimeout : EmployeeBasePage
    {
        const string AppName = "TPS";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                drpTimeoutValueList.Items.Add("1");
                drpTimeoutValueList.Items.Add("2");
                drpTimeoutValueList.Items.Add("5");
                drpTimeoutValueList.Items.Add("10");
                drpTimeoutValueList.Items.Add("15");
                drpTimeoutValueList.Items.Add("20");
                drpTimeoutValueList.Items.Add("30");
                drpTimeoutValueList.Items.Add("60");
                drpTimeoutValueList.Items.Add("90");
                drpTimeoutValueList.Items.Add("120");
                drpTimeoutValueList.Items[4].Selected = true;
                string DomainName = GettingCommonValues.GetDomainName();//Request.ServerVariables["HTTP_HOST"];
                int sessionval = Facade.GetSessionTimeOutValue(CurrentMember.PrimaryEmail, AppName, DomainName);
                if (sessionval > 0)
                {
                    EnableControls(true);
                    TPS360.Web.UI.Helper.ControlHelper.SelectListByValue(drpTimeoutValueList, sessionval.ToString());
                }
                else
                {
                    EnableControls(false);
                }
            }
            else
            {
                if(chkSessionTime.Checked)
                    drpTimeoutValueList.Enabled=true;
                else
                {
                    drpTimeoutValueList.Enabled = false; drpTimeoutValueList.Items[4].Selected = true;
                }
            }
           
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            string DomainName = GettingCommonValues.GetDomainName();
            int val=0;
            if (chkSessionTime.Checked)
                val = Convert.ToInt32(drpTimeoutValueList.Text);

            int IsInserted = Facade.InsertSessionTimeout(CurrentMember.PrimaryEmail, val, Session.SessionID, AppName, DomainName);
            if (IsInserted > 0)
            {
                MiscUtil.ShowMessage(lblMessage, "Session Logout Time Successfully Changed", false);
            }
        }


       

        void EnableControls(bool value)
        {
            chkSessionTime.Checked = value;
            drpTimeoutValueList.Enabled = value;
        }
}
}