﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using AjaxControlToolkit;
namespace TPS360.Web.UI
{
    public partial class EmployeeEmailHistory : EmployeeBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
         
            ucAllEmailHistory.EmailFromType = EmailFromType.EmailHistory;
            Page.Title = CurrentMember.FirstName + " " + CurrentMember.LastName + " - All Emails";
        }
    }
}
