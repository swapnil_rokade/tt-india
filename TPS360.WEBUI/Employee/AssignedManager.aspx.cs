﻿using System;
using TPS360.Common.Helper;

namespace TPS360.Web.UI
{
    public partial class CandidateList : CandidateBasePage
    {
        #region Member Variables

        private int _memberId = 0;

        #endregion

        #region Methods

        private void GetMemberId()
        {
            //From Member Profile
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                hfMemberId.Value = Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID];

            }
            //From Member Login
            else
            {
                hfMemberId.Value = base.CurrentMember.Id.ToString();
            }
            _memberId = Int32.Parse(hfMemberId.Value);
        }

        #endregion

        #region Events

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {           
            if (!IsPostBack)
            {               
                GetMemberId();
            }
            if (_memberId > 0)
            {
                string name = MiscUtil.GetMemberNameById(_memberId, Facade);
                this.Page.Title = name + " - " + "Assigned Managers";
            }
        }

        #endregion

        #endregion
    }
}