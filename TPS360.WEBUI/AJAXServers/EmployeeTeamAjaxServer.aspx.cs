﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TPS360.Common.BusinessEntities;
using TPS360.BusinessFacade;
namespace TPS360.Web.UI
{
    /// <summary>
    /// Summary description for AjaxServer.
    /// </summary>System.Web.UI.Page
    public partial class EmployeeTeamAjaxServer : BasePage  
    {
        //Sends the response back, with XML data.

             protected void Page_Load(object sender, System.EventArgs e)
        {
            if (!IsPostBack)
            {
                string SelectedTeamId = Request["SelectedTeamId"];

                if (SelectedTeamId.Length > 0)
                {
                    Response.Clear();
                    CountryStateXml countryStateXml = new CountryStateXml();

                    //For a given country, getting country and states under that country in XML format.
                   
                 
                    IFacade facade = new Facade();
                    ArrayList reqList = null;
                    ArrayList builder = null;
                    if (SelectedTeamId == "0")
                    {
                        builder = facade.GetAllEmployeeNameWithEmail (ContextConstants .ROLE_EMPLOYEE );
                    }
                    else 
                     builder =   Facade.GetAllEmployeeByTeamId(SelectedTeamId);// Facade.EmployeeTeamBuilder_GetByTeamId(Convert.ToInt32(SelectedTeamId));
                    string statesString = "<Employee><Team><Id>0</Id><Name>" + (builder.Count > 0 ? " Please Select" : "No Requisitions") + "</Name></Team>";// countryStateXml.GetCompanyContactXMLString(Convert.ToInt32(selectedCompany.ToLower().Trim() == "all" ? "0" : selectedCompany), FillEmail);
                    if (builder != null)
                    {
                            foreach (Employee job in builder)
                            {
                                statesString += "<Team><Id>";
                                statesString += job.Id.ToString() + "</Id><Name>";
                                statesString += job.FirstName + "</Name></Team>";
                            }
                    }
                    statesString += "</Employee>";


                    Response.Clear();
                    Response.ContentType = "text/xml";

                    Response.Write(statesString);
                    //end the response
                    Response.End();
                }
                else
                {
                    //clears the response written into the buffer and end the response.
                    Response.Clear();
                    Response.End();
                }


            }
            else
            {
                //clears the response written into the buffer and end the response.
                Response.Clear();
                Response.End();
            }


        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
