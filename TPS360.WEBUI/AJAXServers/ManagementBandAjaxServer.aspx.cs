﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TPS360.Common.BusinessEntities;
using TPS360.BusinessFacade;
using System.Collections.Generic;
namespace TPS360.Web.UI
{
    /// <summary>
    /// Summary description for AjaxServer.
    /// </summary>System.Web.UI.Page
    public partial class ManagementBandAjaxServer : BasePage  
    {
        //Sends the response back, with XML data.
        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (!IsPostBack)
            {
                string selectedMB = Request["SelectedMBId"];

                if (selectedMB.Length > 0)
                {
                    Response.Clear();
                    IFacade facade = new Facade();
                    IList<MemberHiringDetails> bandList = null;
                    int MBId = Convert.ToInt32(selectedMB);

                    if (MBId > 0)
                    {
                        bandList = Facade.GetAllManagementBandByManagementBandId(MBId);
                    }
                    else
                    {
                        bandList = Facade.GetAllBand();
                    }
                    string statesString = "<Company><Requisition><Id>0</Id><Name>" + (bandList.Count > 0 ? " Please Select" : "No Band") + "</Name></Requisition>";// countryStateXml.GetCompanyContactXMLString(Convert.ToInt32(selectedCompany.ToLower().Trim() == "all" ? "0" : selectedCompany), FillEmail);


                    foreach (MemberHiringDetails detail in bandList)
                    {
                        statesString += "<Requisition><Id>";
                        statesString += detail.BandId.ToString() + "</Id><Name>";
                        statesString += detail.Band + "</Name></Requisition>";
                    }
                    statesString += "</Company>";


                    Response.Clear();
                    Response.ContentType = "text/xml";

                    Response.Write(statesString);
                    Response.End();
                }
                else
                {
                    Response.Clear();
                    Response.End();
                }


            }
            else
            {
                //clears the response written into the buffer and end the response.
                Response.Clear();
                Response.End();
            }


        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
