﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Web.UI;
using TPS360.Common.BusinessEntities;
using System.Text;

public partial class AJAXServers_CandidateSearchSub :BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            int candidateId = Convert.ToInt32(Request["CandidateId"]);
            string country = Request["Country"].ToString();
            string viewstatename = Request["ViewStateName"].ToString();
            string IsKeyWordPresent = Request["IsKeyWordPresent"].ToString();

            string sdf= (string)Session[viewstatename];
            Candidate ca = Facade.GetOnSearchForPrecise_Sub(candidateId);
            Response.Clear();
            Response.ContentType = "text/xml";

            //Binding Skills
            if (ca.Skills.Trim().Length > 0 && ca.Skills.Trim().EndsWith(","))
            {
                ca.Skills = ca.Skills.Trim().Substring(0, ca.Skills.Trim().Length - 1);
            }
            string skill = ca.Skills;//.Length<=25 ? ca.Skills :(ca.Skills.Substring(0,25)+"...");

            //Binding Availability
            string strAvailabilityText = "";
            if (ca.AvailabilityText == "Available From")
                strAvailabilityText = ca.AvailabilityText + " " + ca.AvailableDate.ToShortDateString();
            else
                strAvailabilityText = ca.AvailabilityText;

            //Remarks
            string strRemarks = ca.Remarks;
            //if (ca.Remarks != null)
            //{
            //    strRemarks = ((ca.Remarks.Length <= 25) ? ca.Remarks : (ca.Remarks.Substring(0, 25) + " ..."));
            //}
            
            //Objective
            string strObjective = "";
            try
            {
                strObjective = ca.Objective;// (ca.Objective.Length <= 25 ? MiscUtil.RemoveScript(ca.Objective) : MiscUtil.RemoveScript(ca.Objective.Substring(0, 25) + "..."));//1.1
            }
            catch
            {
            }

                        
            string[] payCycle = { "", "Hour", "Day", "Month", "Year" };
            string lblCandidateCurrentSalary = "";
            string lblCandidateExpectedSalary = "";
            {
                if (country.ToLower() == "India".ToLower())
                {
                    if (ca.CurrentYearlyRate != null && ca.CurrentYearlyRate != 0) lblCandidateCurrentSalary = "Rs " + ca.CurrentYearlyRate.ToString() + " Lacs " + "/ " + payCycle[ca.CurrentSalaryPayCycle];
                    if (ca.ExpectedYearlyRate != null && ca.ExpectedYearlyRate != 0 && ca.ExpectedYearlyMaxRate != null && ca.ExpectedYearlyMaxRate != 0) lblCandidateExpectedSalary = "Rs " + ca.ExpectedYearlyRate.ToString() + " - " + ca.ExpectedYearlyMaxRate.ToString() + " Lacs " + "/ " + payCycle[ca.ExpectedSalaryPayCycle];
                }
                else if (country.ToLower() == "United States".ToLower())
                {
                    if (ca.CurrentYearlyRate != null && ca.CurrentYearlyRate != 0) lblCandidateCurrentSalary = "$ " + ca.CurrentYearlyRate.ToString() + "/" + payCycle[ca.CurrentSalaryPayCycle];
                    if (ca.ExpectedYearlyRate != null && ca.ExpectedYearlyRate != 0 && ca.ExpectedYearlyMaxRate != null && ca.ExpectedYearlyMaxRate != 0) lblCandidateExpectedSalary = "$ " + ca.ExpectedYearlyRate.ToString() + " - " + ca.ExpectedYearlyMaxRate.ToString() + "/ " + payCycle[ca.ExpectedSalaryPayCycle];
                }
                else
                {
                    if (ca.CurrentYearlyRate != null && ca.CurrentYearlyRate != 0)
                    {
                       lblCandidateCurrentSalary = (ca.CurrentYearlyCurrency) + ca.CurrentYearlyRate.ToString() + " / " + payCycle[ca.CurrentSalaryPayCycle];
                    }
                    if (ca.ExpectedYearlyRate != null && ca.ExpectedYearlyRate != 0 && ca.ExpectedYearlyMaxRate != null && ca.ExpectedYearlyMaxRate != 0)
                    {
                       lblCandidateExpectedSalary = (ca.ExpectedYearlyCurrency) + ca.ExpectedYearlyRate.ToString() + " - " + ca.ExpectedYearlyMaxRate.ToString() + " / " + payCycle[ca.ExpectedSalaryPayCycle];
                    }
                }
            }
        

            StringBuilder strBUildResult = new StringBuilder();
            strBUildResult.Append("<td></td>");
            strBUildResult.Append("<td  colspan =\"2\" align =\"left\"  style =\" font-weight :normal;\">");
            strBUildResult.Append(" <B> Primary Manager: </B>");
            strBUildResult.Append(ca.PrimaryManagerName);
            strBUildResult.Append("<br/>");
            strBUildResult.Append("<B> Skills: </B> ");
            strBUildResult.Append("<span style=\"overflow: hidden;white-space: nowrap;text-overflow: ellipsis !important;\" class='ShowToolTip'>" + skill + "</span>");//(ca.Skills);
            strBUildResult.Append("<br/>");
            strBUildResult.Append("<B> Availability: </B> ");
            strBUildResult.Append(strAvailabilityText);
            strBUildResult.Append("<br/>");

            strBUildResult.Append("<B> Work Schedule: </B> ");
            strBUildResult.Append(ca.WorkSchedule);
            strBUildResult.Append("<br/>");

            strBUildResult.Append("<B> Work Status: </B> ");
            strBUildResult.Append(ca.WorkAuthorization);
            strBUildResult.Append("<br/>");

            strBUildResult.Append("<B> Objective: </B> ");
            strBUildResult.Append("<span style=\"overflow: hidden;white-space: nowrap;text-overflow: ellipsis !important;\" class='ShowToolTip'>" +strObjective + "</span>");
            strBUildResult.Append("<br/>");

            strBUildResult.Append("<B> Remarks: </B> ");
            strBUildResult.Append("<span style=\"overflow: hidden;white-space: nowrap;text-overflow: ellipsis !important;\" class='ShowToolTip'>" + strRemarks + "</span>");
            strBUildResult.Append("<br/>");
            strBUildResult.Append("</td>");

            strBUildResult.Append("<td  colspan =\"" + (IsKeyWordPresent=="0"? "5" : "6") + "\" align =\"left\"  style =\" font-weight :normal;\">");
            strBUildResult.Append("<B> Current Salary: </B> ");
            strBUildResult.Append(lblCandidateCurrentSalary);
            strBUildResult.Append("<br/>");

            strBUildResult.Append("<B> Expected Salary: </B> ");
            strBUildResult.Append(lblCandidateExpectedSalary);
            strBUildResult.Append("<br/>");

            strBUildResult.Append("<B> Current Company: </B> ");
            strBUildResult.Append(ca.CurrentPosition);
            strBUildResult.Append("<br/>");

            strBUildResult.Append("<B> Highest Degree: </B> ");
            strBUildResult.Append(ca.HighestDegree);
            strBUildResult.Append("<br/>");

            strBUildResult.Append("<B> Candidate Type: </B> ");
            strBUildResult.Append(ca.MemberTypeName);
            strBUildResult.Append("<br/>");

            string img = "";
            MemberExtendedInformation currentCandidateExInfo = Facade.GetMemberExtendedInformationByMemberId(candidateId);
            if (currentCandidateExInfo.InternalRatingLookupId != 0)
            {
                GenericLookup rating = Facade.GetGenericLookupById(currentCandidateExInfo.InternalRatingLookupId);
                if (rating != null && !string.IsNullOrEmpty(rating.Name))
                {
                    img = MiscUtil.GetInternalRatingImage(rating.Name);
                }
            }
            else
            {
                img = MiscUtil.GetInternalRatingImage("0");
            }

            strBUildResult.Append("<B> Internal Rating:</B> ");
            strBUildResult.Append("<img src=\"" + img.Replace("~/","../") + "\"/>");
            strBUildResult.Append("<br/>"); 

            strBUildResult.Append("<B> Last Updated: </B> ");
            strBUildResult.Append(ca.UpdateDate.ToShortDateString());
            strBUildResult.Append("<br/>");

            strBUildResult.Append("</td>");
            Response.Write(strBUildResult.ToString());
            Response.End();
        }
    }

    string AppendHighLight(string value,bool IsAppendHighLight)
    {
        if (IsAppendHighLight)
            return "<span class=highlight>" + value + "</span>";
        else
            return value;
    }

}
