﻿using System;
using TPS360.Common.Shared;
namespace TPS360.Web.UI
{
    public partial class CreateNewCandidate: CandidateBasePage
    {
        #region Veriables

        #endregion

        #region Properties


        #endregion

        #region Methods



        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            rgInternal.Role = ContextConstants.ROLE_CANDIDATE;
            if (SiteSetting[DefaultSiteSetting.CompanyName.ToString()].ToString() != null) 
            {
                hdnCreateNewCandidate.Value = SiteSetting[DefaultSiteSetting.CompanyName.ToString()].ToString();
                this.Page.Title = hdnCreateNewCandidate.Value + " - Create New Candidate";
            
            }
        }

        #endregion
    }
}