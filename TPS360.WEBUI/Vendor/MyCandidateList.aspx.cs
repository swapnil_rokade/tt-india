﻿using System;
using TPS360.Common.Shared;
namespace TPS360.Web.UI
{
    public partial class MyCandidateList: CandidateBasePage
    {
        #region Veriables

        #endregion

        #region Properties


        #endregion

        #region Methods



        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (SiteSetting[DefaultSiteSetting.CompanyName.ToString()].ToString() != null) 
            {
                hdnMyCandidateList.Value = SiteSetting[DefaultSiteSetting.CompanyName.ToString()].ToString();
                this.Page.Title = hdnMyCandidateList.Value + " - My Candidates";
            
            } 
        }

        #endregion
    }
}