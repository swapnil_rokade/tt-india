﻿<%@ Page Language="C#" MasterPageFile="~/Masters/CandidateProfile.master" AutoEventWireup="true"  Async="true"
    CodeFile="TPS360Overview.aspx.cs" Inherits="TPS360.Web.UI.CandidateOverview"
    Title="Applicant Overview" Trace="false" EnableEventValidation="false" %>
                      <%@ Register Src ="~/Controls/CandidateOverviewHeader.ascx" TagName ="OverviewHeader" TagPrefix ="ucl" %>
<%@ Register Src="~/Controls/TPS360Overview.ascx" TagPrefix="uc1" TagName="CandidateOverView" %>
<asp:Content ID="cntTPS360Overview" ContentPlaceHolderID="cphCandidateMaster" runat="Server">
<asp:HiddenField ID="hdnPageTitle" runat ="server" />
   <script src="../Scripts/GirdOverflowToopTip.js" ></script>
   
    <ucl:OverviewHeader  ID="uclOverviewHeader" runat="server"></ucl:OverviewHeader>
    <uc1:CandidateOverView ID="uclCandidateOverView" runat="server" />  
     
</asp:Content>
