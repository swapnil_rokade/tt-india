﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using TPS360.Providers;
using System.Text;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
namespace TPS360.Web.UI
{
    public partial class VendorPortalLeft : BaseMasterPage
    {
        string _currentNode = string.Empty;
        string _currentParentNode = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {  try
            {
                if ( CurrentMember ==null)
                {
                    //FormsAuthentication.SignOut();
                    //FormsAuthentication.RedirectToLoginPage();
                    Response.Redirect(UrlConstants.CandidatePortal.LOGIN);
                }
            }
            catch { }

            
             _currentParentNode = Helper.Url.SecureUrl[UrlConstants.PARAM_SITEMAP_PARENT_ID ].ToString();
            _currentNode = Helper.Url.SecureUrl[UrlConstants.PARAM_SITEMAP_ID].ToString();
            BuildMenu();
        }
        private void BuildMenu()
        {
            if (CurrentMember == null || CurrentUser == null)
            {
                return;
            }

            SiteMapNodeCollection permittedNodeList = new SiteMapNodeCollection();

            SqlSiteMapProvider provider = SiteMap.Providers["SqlSiteMap"] as SqlSiteMapProvider;
            SiteMapNode root = null;
            bool setActive = false;
            if (_currentNode !="" && Convert.ToInt32(_currentNode) == (int)SiteMapType.VendorProfileMenu )
            {
                setActive = true;
                root = provider.FindSiteMapNodeFromKey(StringHelper.Convert((int)SiteMapType.VendorProfileMenu));

            }
            else if (_currentNode != "" && Convert.ToInt32(_currentNode) == (int)SiteMapType.CandidateSearchJob)
            {
                setActive = true;
                root = provider.FindSiteMapNodeFromKey(StringHelper.Convert((int)SiteMapType.CandidateSearchJob ));

            }
            else if (_currentParentNode != "") root = provider.FindSiteMapNodeFromKey(StringHelper.Convert(_currentParentNode));

            else
            {
                setActive = true;
                root = provider.FindSiteMapNodeFromKey(StringHelper.Convert((int)SiteMapType.CandidateProfileMenu));
            }
            int ID = base.CurrentMember.Id;
            System.Collections.Generic.IList <TPS360.Common .BusinessEntities . MenuListCount> MenuListcount = Facade.CustomSiteMap_GetMenuListCount(Convert.ToInt32(_currentParentNode),ID);
            if (root != null)
            {
                SiteMapNodeCollection nodeList = root.ChildNodes;
                StringBuilder strMenu = new StringBuilder();

                if (nodeList != null && nodeList.Count > 0)
                {
                    foreach (SiteMapNode node in nodeList)
                    {

                        bool HasCount = Convert.ToBoolean(node["HasCount"]);
                        int itemcount = 0;
                        if (HasCount)
                        {
                            var a = MenuListcount.Where(x => x.Id == Convert.ToInt32(node["Id"]));
                            if (a.Count() > 0) itemcount = a.ToList()[0].Count;
                        }

                        string strActiveClass = "";// node["Id"].Equals(_currentNode) ? " class=\"active\"" : "calss=\"active\"";
                        if (node["Id"].Equals(_currentNode) || setActive)
                        {
                            strActiveClass = " class=\"active\"";
                            setActive = false;
                        }
                        else strActiveClass = "";
                        strMenu.Append(("<li " + strActiveClass + " ><a href=\"../" + BuildUrl(node.Url) + "\" title=\"" + node.Description + "\">" + node.Title + (HasCount ? " <span class=\"badge\" style=\" float:right;\">" + itemcount + "</span>" : "") + "</a></li>"));

                    }
                    MenuList.InnerHtml = strMenu.ToString();
                }


            }
        }

        protected string BuildUrl(string url)
        {
            string pageUrl = url;
            pageUrl = (new SecureUrl(pageUrl)).ToString();

            return pageUrl;
        }

    }
}