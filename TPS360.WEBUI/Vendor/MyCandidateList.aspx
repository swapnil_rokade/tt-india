﻿<%@ Page Language="C#" MasterPageFile="~/Vendor/VendorPortalLeft.master" AutoEventWireup="true" CodeFile="MyCandidateList.aspx.cs"  EnableEventValidation ="false" 
    Inherits="TPS360.Web.UI.MyCandidateList" Title="New Candidate Registration" %>

<%@ Register Src="~/Controls/VendorCandidateList.ascx" TagName="VendorCandidates" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHomeMasterTitle" Runat="Server">
Candidates
</asp:Content>
<asp:Content ID="cntRegistrationInformation" ContentPlaceHolderID="cphVendorMaster"
    runat="Server">
  <asp:HiddenField ID="hdnMyCandidateList" runat="server" />
    <asp:UpdatePanel ID="pnlRegistrationInformation" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        <div class="TabPanelHeader">
            My Candidates
        </div>
            <div id="divRegInternal" runat="server">
                 <uc1:VendorCandidates ID="uclVendorCandidates" runat ="server" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

