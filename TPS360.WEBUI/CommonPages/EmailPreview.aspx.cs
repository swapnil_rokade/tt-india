﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: 
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              Jun-08-2009             Veda            Defect id: 10529;Changes made in Page_Load.
-------------------------------------------------------------------------------------------------------------------------------------------       
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ExpertPdf.HtmlToPdf;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Common;
using TPS360.Common.Helper;
using System.Configuration;
using System.Drawing;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.IO;

namespace TPS360.Web.UI.CommonPages
{
    public partial class EmailPreview : AdminBasePage
    { 
        #region Events
        string  TempFolder
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl["TempFolder"]))
                {
                    return Helper.Url.SecureUrl["TempFolder"];

                }
                    return string .Empty ;
            }
        }
    
      
        private void PopulatePreviewFromXML()
        {
            string dir = CurrentUserId.ToString();
            string path = System .IO .Path.Combine(UrlConstants.TempDirectory, dir) +"\\"+ TempFolder ;
            ParsePreviewXML(path + "\\Preview.xml");
        }
      
        private void AddSignature()
        {
            System.Web.HttpBrowserCapabilities browser = Request.Browser;
            string Footer = string.Empty;
            string UnsubscribeFooter = string.Empty;
            StringBuilder email = new StringBuilder();
            EmailHelper emailManager = new EmailHelper();//veda

            emailManager.To.Clear();
            EmailHelper emailHelper = new EmailHelper();
            emailHelper.SenderSignature = MiscUtil.GetMemberSignature(base.CurrentMember.Id, Facade);
            emailHelper.SignatureFile = MiscUtil.GetMemberSignatureLogo(base.CurrentMember.Id, Facade);
            emailHelper.CompanyLogo = UIConstants.EMAIL_COMPANY_LOGO;
            if (!string.IsNullOrEmpty(emailHelper.SignatureFile))
            {
                emailManager.LinkedResources.Add("Member_Signature", Server.MapPath(emailHelper.SignatureFile));
            }

            if (!string.IsNullOrEmpty(UIConstants.EMAIL_COMPANY_LOGO))
            {
                emailManager.LinkedResources.Add("Email_Company_Logo", Server.MapPath(UIConstants.EMAIL_COMPANY_LOGO));
            }
            Footer = emailHelper.GetEmailFooter();
            email.Append("<table width=780px align=left cellpadding=0 cellspacing=0>");
            email.Append("    <tr>");
            email.Append("        <td><div align=justify>");
            if (!string.IsNullOrEmpty(Footer))
            {
                email.Append(Footer);
            }
            email.Append("<br/>");
            email.Append("        </div></td>");
            email.Append("    </tr>");
            email.Append("    <tr>");
            email.Append("        <td><div align=justify>");
            if (!string.IsNullOrEmpty(UnsubscribeFooter))
            {
                email.Append("<br/><br/><br/>"); 
                email.Append(UnsubscribeFooter);
            }
            email.Append("<br/>");
            email.Append("        </div></td>");
            email.Append("    </tr>");
            email.Append("</table>");
            EmailDescription.InnerHtml = EmailDescription.InnerHtml + email.ToString();
        }
        private void ParsePreviewXML(string xml)
        {
            bool isBCCAvailable = false;
            bool isCCAvailable = false;
            bool isAttachmentAvailable = false;
            XmlTextReader reader = new XmlTextReader(xml);
            reader.MoveToContent();
            string ElementName = "";
            if (reader.NodeType == XmlNodeType.Element && reader.Name == "Preview")
            {
                try
                {
                    while (reader.Read())
                    {
                        if (reader.NodeType == XmlNodeType.Element)
                        {
                            ElementName = reader.Name;
                        }
                        else
                        {
                            if (reader.NodeType == XmlNodeType.Text)
                            {
                                switch (ElementName)
                                {
                                    case "From":
                                        From.InnerHtml = ": " +MiscUtil .RemoveScript ( reader.Value);
                                        break;
                                    case "To":
                                        To.InnerHtml = ": " + MiscUtil .RemoveScript (reader.Value);
                                        break;
                                    case "Subject":
                                        Subject.InnerHtml = ": " +MiscUtil .RemoveScript ( reader.Value);
                                        break;
                                    case "BCC":
                                        isBCCAvailable = true;
                                        BCC.InnerHtml = ": " +MiscUtil .RemoveScript ( reader.Value);
                                        break;
                                    case "CC":
                                        isCCAvailable = true;
                                        CC.InnerHtml = ": " +MiscUtil .RemoveScript ( reader.Value);
                                        break;
                                    case "Body":
                                        if (EmailDescription.InnerHtml == "") EmailDescription.InnerHtml  = ": <br/>" +reader.Value;
                                        else EmailDescription.InnerHtml +=reader.Value;
                                        break;
                                    case "Attachments":
                                        isAttachmentAvailable = true;
                                        string[] att = reader.Value.Split(';');
                                        string display = "<table><tr><td>&nbsp;:";
                                        foreach (string s in att)
                                        {
                                            if (s != string.Empty)
                                            {
                                                try
                                                {
                                                    string[] pt = s.Split('?');
                                                    if (!pt[1].Contains("Resources/Member/"))
                                                    {
                                                        if (s.Contains("$$")) { display += getUploadedDocument(s); continue; }
                                                        string dir = CurrentUserId.ToString();
                                                        //string path = System.IO.Path.Combine(UrlConstants.ApplicationBaseUrl + "Temp/", dir) + "/" + TempFolder;
                                                        IList<string> path = new List<string>();
                                                        path = s.Split('?');
                                                        if (path[1].Contains("../Temp"))
                                                        {
                                                            SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + path[1].Substring(3), string.Empty);
                                                            display += "&nbsp;<a href=\"" + url + "\" Target=\"_blank\">" + path[0] + "</a>         ;";
                                                        }
                                                        else
                                                        {
                                                            SecureUrl url = UrlHelper.BuildSecureUrl(System.IO.Path.Combine(UrlConstants.ApplicationBaseUrl + "Temp/", dir) + "/" + path[1].Replace("\\", "/") + "/" + path[0], string.Empty);
                                                            display += "&nbsp;<a href=\"" + url + "\" Target=\"_blank\">" + path[0] + "</a>         ;";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        SecureUrl url = UrlHelper.BuildSecureUrl(pt[1], string.Empty);
                                                        display += "&nbsp;<a href=\"" + url + "\" Target=\"_blank\">" + pt[0] + "</a>         ;";
                                                    }

                                                }
                                                catch { }
                                            }
                                        }
                                        display = display.Trim().Remove(display.Length - 1, 1);
                                        display += "</td></tr></table>";
                                        dd.InnerHtml = display;
                                        break;
                                }
                                DateAndTime.InnerHtml = ": " + DateTime.Now.ToString();
                            }
                        }
                    }
                }
                catch
                {
                }
            }
            reader.Close();
            if (!isAttachmentAvailable ) tr3.Visible = false;
            if (!isBCCAvailable) tr2.Visible = false;
            if (!isCCAvailable) tr1.Visible = false;

        }
        private string getUploadedDocument(string s)
        {
            string[] sarray={"$$"};
            string[] file = s.Split(sarray,StringSplitOptions.RemoveEmptyEntries);
            return "<a href=\"javascript:OpenFile('" + file[1] + "')\">" + file[0] + "</a>         ;         ";

        }
        protected void Page_Load(object sender, EventArgs e)
         {
            System.Web.HttpBrowserCapabilities browser = Request.Browser;
            string Footer = string.Empty;
            string UnsubscribeFooter = string.Empty;
            StringBuilder email = new StringBuilder();
            if (!string.IsNullOrEmpty(Helper.Url.SecureUrl[UrlConstants.PARAM_MemberEmail_ID].ToString()))
            {
                PopulateSubmissionEmail(Int32.Parse(Helper.Url.SecureUrl[UrlConstants.PARAM_MemberEmail_ID]));
            }
            else
            {
                PopulatePreviewFromXML();
                return;
            }
            this.Page.SetFocus(this);     
        }

        private void PopulateMailData(string strFrom, string strTo, string strSubject, string strBCC, string strCC, string strAttachments)
        {
            From.InnerHtml = strFrom;
            To.InnerHtml = strTo;
            Subject.InnerHtml = strSubject;
            BCC.InnerHtml = strBCC;
            CC.InnerHtml = strCC;
            dd.InnerHtml = strAttachments;
        }

        private void Show(int _mailId)
        {
            MemberEmail memberEmail = Facade.GetMemberEmailById(_mailId);

            if (memberEmail != null)
            {
                DateAndTime.InnerHtml = ": " + memberEmail.SentDate.ToString();
                From.InnerHtml = ": " +MiscUtil .RemoveScript ( memberEmail.SenderEmail.ToString());
                To.InnerHtml = ": " +MiscUtil .RemoveScript ( memberEmail.ReceiverEmail.ToString());
                if (memberEmail.CC.ToString().Trim() != "")
                    CC.InnerHtml = ": " +MiscUtil .RemoveScript ( memberEmail.CC.ToString());
                else
                {
                    tr1.Visible = false;
                }
                if (memberEmail.BCC.ToString().Trim() != "")
                    BCC.InnerHtml = ": " +MiscUtil .RemoveScript ( memberEmail.BCC.ToString());
                else
                {
                    tr2.Visible = false;
                }
                int noofAtt = memberEmail.NoOfAttachedFiles;
                string[] attarray = memberEmail.AttachedFileNames.Split(';');
                string display = "<table><tr><td>:</td><td>";

                for (int i = 0; i < noofAtt; i++)
                {

                    Label att = new Label();

                    string s = "";
                    string[] filen = attarray[i].Split(new string[] { "$$$" },StringSplitOptions.RemoveEmptyEntries );
                    s = "..\\Resources\\Member\\" + memberEmail.ReceiverId + "\\Outlook%20Sync\\" + memberEmail.Id + "\\" +attarray [i].Replace (" ","%20")+"\\"+filen[0].Replace(" ", "%20");

                    display += "<a href=" + s + ">" + filen [0] + "</a>         ;         ";
                }
                display += "</td></tr></table>";

                dd.InnerHtml = display;
                if (memberEmail.NoOfAttachedFiles == 0)
                {
                    tr3.Visible = false;
                }
                Subject.InnerHtml = ": " +MiscUtil .RemoveScript ( memberEmail.Subject.ToString());
                EmailDescription.InnerHtml = ": " + memberEmail.EmailBody;
            }
        }
        protected void PopulateSubmissionEmail(int id)
        {

            Show(id);
            return;
            MemberEmail memail = Facade.GetMemberEmailById(id);
            if (memail != null)
            {

                EmailHelper emailHelper = new EmailHelper();

                emailHelper.FromEmail = (memail.SenderEmail);
                emailHelper.AdditionalMailMessage = HttpUtility.HtmlDecode(memail.EmailBody);
                emailHelper.SenderSignature = MiscUtil.GetMemberSignature(base.CurrentMember.Id, Facade);
                emailHelper.SignatureFile = MiscUtil.GetMemberSignatureLogo(base.CurrentMember.Id, Facade);
                emailHelper.CompanyLogo = UIConstants.EMAIL_COMPANY_LOGO;
                emailHelper.ShowJobDescription = false;
                emailHelper.HiringMatrixApplicant = Helper.Url.SecureUrl[UrlConstants.PARAM_APPLICANT_ID].ToString();
                emailHelper.ShowScreeningQuestion = false;
                emailHelper.IsPreview = true;
                emailHelper.ToEmail = memail.ReceiverEmail;

                Company CurrentCompany = Facade.GetCompanyById(Int32.Parse(Helper.Url.SecureUrl[UrlConstants.PARAM_COMPANY_ID]));
                State ContactState = new State();
                Country ContactCountry = new Country();
                string strState = string.Empty;
                string strCountry = string.Empty;

                if (CurrentCompany.PrimaryContact.StateId > 0)
                {
                    ContactState = Facade.GetStateById(CurrentCompany.PrimaryContact.StateId);
                }
                strState = (ContactState == null) ? "" : ContactState.Name;

                if (CurrentCompany.PrimaryContact.CountryId > 0)
                {
                    ContactCountry = Facade.GetCountryById(CurrentCompany.PrimaryContact.CountryId);
                }
                strCountry = (ContactCountry == null) ? "" : ContactCountry.Name;

                StringBuilder strbldrAddress = new StringBuilder();
                if (CurrentCompany.PrimaryContact.Address1.IsNotNullOrEmpty())
                    strbldrAddress.Append(CurrentCompany.PrimaryContact.Address1);
                if (CurrentCompany.PrimaryContact.Address2.IsNotNullOrEmpty())
                    strbldrAddress.Append("<br/>" + CurrentCompany.PrimaryContact.Address2);
                if (CurrentCompany.PrimaryContact.City.IsNotNullOrEmpty())
                    strbldrAddress.Append("<br/>" + CurrentCompany.PrimaryContact.City);
                if (strState.IsNotNullOrEmpty())
                    strbldrAddress.Append("<br/>" + strState);
                if (strCountry.IsNotNullOrEmpty())
                    strbldrAddress.Append("<br/>" + strCountry);
                if (CurrentCompany.PrimaryContact.ZipCode.IsNotNullOrEmpty())
                    strbldrAddress.Append("<br/>" + CurrentCompany.PrimaryContact.ZipCode);
                ListBox lstToList = new ListBox();
                lstToList.Items.Add(new ListItem("First Name", "FirstName")); 
                lstToList.Items.Add(new ListItem("Last Name", "LastName"));  
                lstToList.Items.Add(new ListItem("Current City", "CurrentCiry"));  
                lstToList.Items.Add(new ListItem("Current Position", "CurrentPosition"));  
                lstToList.Items.Add(new ListItem("Total Experience", "TotalExperience"));  
                lstToList.Items.Add(new ListItem("Work Permit Status", "WorkPermitStatus"));  
                lstToList.Items.Add(new ListItem("Availability", "AvailabilityStatus")); 

                lstToList.Items.Add(new ListItem("Summary", "Summary")); 

                lstToList.Items.Add(new ListItem("Skill Set", "SkillSet")); 
                lstToList.Items.Add(new ListItem("Education", "Education"));  
                lstToList.Items.Add(new ListItem("Experience", "Experience"));  

                CheckBox chkbx = new CheckBox();
                chkbx.Checked = true;
                chkbx.ID = "dcsd";
                PopulateMailData(emailHelper.FromEmail , emailHelper.ToEmail ,"", "","", "");
                EmailDescription.InnerHtml = emailHelper.GetSubmittedApplicantsResumesEmailBody(this.Page, lstToList, null, chkbx, Int32.Parse(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID]), "", "", "", "", Facade);
            }
        }

        #endregion
    }
}
