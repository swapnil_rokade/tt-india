﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CandidateSourcingReport.aspx.cs
    Description: This is the page used to display the Candidate Sourcing Report in excel,word,Pdf formats.
    Created By: 
    Created On:
    Modification Log: 
    --------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    --------------------------------------------------------------------------------------------------------------------------------------
   
 * ------------------------------------------------------------------------------------------------------------------------------------------- 
*/
using System;
using System.Web.Security;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;
using TPS360.Common;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using ExpertPdf.HtmlToPdf;
using System.Drawing;
using System.Collections.Generic;
using System.Collections;
using System.Web.UI.HtmlControls;
using System.Linq;

namespace TPS360.Web.UI.Reports
{

    public partial class CandidateSourcingReport : BasePage
    {
        #region Members
        private bool _IsAccessToEmployee = true;
        private static string UrlForEmployee = string.Empty;
        private static int IdForSitemap = 0;
        private string companyStatus = null;

        string _cookiename;
        string CookieName
        {
            get { return _cookiename; }
            set { _cookiename = value; }
        }
         
        #endregion

        #region Methods
        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvCandidateSourcingRpt.FindControl(txtSortColumn.Text);
                HtmlTableCell im = (HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));

            }
            catch
            {
            }

        }

        private void GenerateCandidateSourcingReport(string format)
        {
            if (string.Equals(format, "word"))
            {

                Response.AddHeader("content-disposition", "attachment;filename=CandidateSourcingReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".doc");
                Response.ContentEncoding = System.Text.Encoding.ASCII;
                Response.ContentType = "application/msword";
                Response.Output.Write(GetCandidateSourcingReportTable());
                Response.Flush();
                Response.End();

            }
            else if (string.Equals(format, "excel"))
            {
                Response.AddHeader("content-disposition", "attachment;filename=CandidateSourcingReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".xls");
                Response.ContentEncoding = System.Text.Encoding.ASCII;
                Response.ContentType = "application/msexcel";
                Response.Output.Write(GetCandidateSourcingReportTable());
                Response.Flush();
                Response.End();

            }
            else if (string.Equals(format, "pdf"))
            {
                PdfConverter pdfConverter = new PdfConverter();
                UnicodeEncoding unicodeEncoding = new UnicodeEncoding();
                pdfConverter.PageWidth = 0;
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfDocumentOptions.ShowFooter = false;
                pdfConverter.PdfDocumentOptions.LeftMargin = 15;
                pdfConverter.PdfDocumentOptions.RightMargin = 5;
                pdfConverter.PdfDocumentOptions.TopMargin = 15;
                pdfConverter.PdfDocumentOptions.BottomMargin = 5;
                pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfHeaderOptions.DrawHeaderLine = false;
                pdfConverter.PdfFooterOptions.FooterTextColor = Color.Black;
                pdfConverter.PdfFooterOptions.DrawFooterLine = true;
                pdfConverter.PdfFooterOptions.PageNumberText = "Page";
                pdfConverter.PdfFooterOptions.ShowPageNumber = true;


                pdfConverter.LicenseKey = ContextConstants.EPHTMLTOPDFKEY;
                byte[] downloadBytes = pdfConverter.GetPdfBytesFromHtmlString(GetCandidateSourcingReportTable());

                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.Clear();
                response.AddHeader("Content-Type", "binary/octet-stream");
                response.AddHeader("Content-Disposition", "attachment; filename=CandidateSourcingReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".pdf; size=" + downloadBytes.Length.ToString());
                response.Flush();
                response.BinaryWrite(downloadBytes);
                response.Flush();
                response.End();
            }
        }

        private string GetCandidateSourcingReportTable()
        {
            StringBuilder sourcingReport = new StringBuilder();

            CandidateDataSource candidateDataSource = new CandidateDataSource();

            IList<CandidateSourcingInfo> candidateSourcingInfo = candidateDataSource.GetPagedForCandidateSourcingRpt(wdcSourcedDate.StartDate.ToString(), wdcSourcedDate.EndDate.ToString(),
                wdcClientSubmissionDate.StartDate.ToString(), wdcClientSubmissionDate.EndDate.ToString(),
                wdcOfferedDate.StartDate.ToString(), wdcOfferedDate.EndDate.ToString(),
                wdcJoinedDate.StartDate.ToString(), wdcJoinedDate.EndDate.ToString(),
                ddlRequisition.SelectedValue.ToString(),
                ddlAccount.SelectedValue.ToString(),
                //ddlAccount.SelectedItems.ToString(),
                ddlTopLevelStatus.SelectedValue.ToString(),
                ddlSubLevelStatus.SelectedValue.ToString(),
                ddlRecruiter.SelectedValue.ToString(),
                ddlLead.SelectedValue.ToString(),
                MiscUtil.RemoveScript(txtCandidateName.Text),
                MiscUtil.RemoveScript(txtLocation.Text), companyStatus.ToString(),
                null, -1, -1);

            if (candidateSourcingInfo != null)
            {
                sourcingReport.Append("<table style='font-family : calibriz;width:100%; border-collapse:collapse; border-spacing: 1px;' align='left' border = '1' bordercolor='#000000'  cellspacing='0' cellpadding='0'>");

                sourcingReport.Append("    <tr>");
                //sourcingReport.Append("        <td align=left>&nbsp;<img src='" + Server.MapPath(UIConstants.EMAIL_COMPANY_LOGO) + "' style='height:56px;width:56px;'/></td>");
                //sourcingReport.Append("        <td align=left  width='300' height='75'>&nbsp;<img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png' style= style='height:56px;width:56px'/></td>"); //0.1
                sourcingReport.Append("    </tr>");

                sourcingReport.Append(" <tr>");
                //Sourcing Report starts
                sourcingReport.Append(" <tr>");
                sourcingReport.Append("     <th>Candidate ID # </th>");
                sourcingReport.Append("     <th>Name </th>");
                sourcingReport.Append("     <th>BU </th>");
                sourcingReport.Append("     <th>Requisition Code</th>");
                sourcingReport.Append("     <th>Requisition </th>");
                sourcingReport.Append("     <th>Current Status</th>");
                sourcingReport.Append("     <th>Resume Source</th>");
                sourcingReport.Append("     <th>Sourced Date</th>");
                sourcingReport.Append("     <th>Client Submission Date </th>");
                sourcingReport.Append("     <th>Offered Date</th>");
                sourcingReport.Append("     <th>Joined date</th>");
                sourcingReport.Append("     <th>Recruiter </th>");
                sourcingReport.Append("     <th>Lead </th>");
                sourcingReport.Append("     <th>Remarks </th>");
                sourcingReport.Append("     <th>Status </th>");
                sourcingReport.Append(" </tr>");

                foreach (CandidateSourcingInfo info in candidateSourcingInfo)
                {
                    if (info != null)
                    {
                        sourcingReport.Append(" <tr>");
                        sourcingReport.Append("     <td>");
                        sourcingReport.Append("A" + info.Id.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.CandidateName.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.Account.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.JobPostingCode.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.Requisition.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.CurrentStatus.ToString() + "&nbsp;</td>");
                        //sourcingReport.Append("     <td>" + info.Location.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.ResumeSourceName.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>");
                        if (info.SourcedDate != DateTime.MinValue)
                        {
                            sourcingReport.Append(
                            (MiscUtil.IsValidDate(info.SourcedDate) ? info.SourcedDate.ToShortDateString() : string.Empty).ToString().Trim());
                        }
                        sourcingReport.Append("&nbsp;</td>");
                        sourcingReport.Append("     <td>");
                        if (info.ClientSubmissionDate != DateTime.MinValue)
                        {
                            sourcingReport.Append(
                            (MiscUtil.IsValidDate(info.ClientSubmissionDate) ? info.ClientSubmissionDate.ToShortDateString() : string.Empty).ToString().Trim());
                        }
                        sourcingReport.Append("&nbsp;</td>");
                        sourcingReport.Append("     <td>");
                        if (info.OfferedDate != DateTime.MinValue)
                        {
                            sourcingReport.Append(
                            (MiscUtil.IsValidDate(info.OfferedDate) ? info.OfferedDate.ToShortDateString() : string.Empty).ToString().Trim());

                        }
                        sourcingReport.Append("&nbsp;</td>");
                        sourcingReport.Append("     <td>");
                        if (info.Joineddate != DateTime.MinValue)
                        {
                            sourcingReport.Append(
                            (MiscUtil.IsValidDate(info.Joineddate) ? info.Joineddate.ToShortDateString() : string.Empty).ToString().Trim());

                        }
                        sourcingReport.Append("&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.Recruiter.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.Lead.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.Remarks.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>");
                        if (info.Status == 1)
                        {
                            sourcingReport.Append("InActive");;
                        }
                        else
                        {
                            int reqCount = Facade.GetActiveRequsiutionCountByMemberId(info.Id);
                            if (reqCount > 1)
                            {
                                sourcingReport.Append("Profile is Process By " + reqCount.ToString());
                            }
                            else
                            {
                                sourcingReport.Append("Profile in Process by " + Facade.GetRecruiternameByMemberId(info.Id));
                            }
                        }
                        sourcingReport.Append("&nbsp;</td>");
                        sourcingReport.Append(" </tr>");
                    }
                }
            }

            sourcingReport.Append("    <tr>");
            //sourcingReport.Append("        <td align=left  width='300' height='75' >&nbsp;<img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png'  style= style='height:56px;width:56px;'/></td>");//0.1
            sourcingReport.Append("    </tr>");
            sourcingReport.Append(" </table>");
            return sourcingReport.ToString();
        }

        private void BindList()
        {
            this.lsvCandidateSourcingRpt.DataSourceID = "odsCandidateList";
            this.lsvCandidateSourcingRpt.DataBind();
            divCandidateSourcingReport.Visible = true;
        }

        private void Clear()
        {
            divCandidateSourcingReport.Visible = false;
            ddlAccount.SelectedValue  = "0";
            //foreach (ListItem item in ddlAccount.ListItem.Items)
            //{
            //    item.Selected = false;
            //}
            Response.Cookies["RequisitionStatus"].Value = ddlAccount.SelectedValue;
            ClearSearchCriteria();
        }

        private void ClearSearchCriteria()
        {
            lblMessage.Text = string.Empty;
            wdcSourcedDate.ClearRange();
            wdcClientSubmissionDate.ClearRange();
            wdcOfferedDate.ClearRange();
            wdcJoinedDate.ClearRange();
            ddlRequisition.SelectedIndex = 0;
            ddlAccount.SelectedIndex = 0;
            ddlTopLevelStatus.SelectedIndex = 0;
            ddlSubLevelStatus.SelectedIndex = 0;
            ddlRecruiter.SelectedIndex = 0;
            ddlLead.SelectedIndex = 0;
            txtCandidateName.Text = string.Empty;
            //txtLocation.Text = string.Empty;

        }

        private void PopulateClientDropdowns(ListControl list)
        {
            list.Items.Clear();
            int StatusVal = (int)CompanyStatus.Department;
            string ApplicationEdition = MiscUtil.GetApplicationEdition(Facade);
            if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
            {
                StatusVal = (int)CompanyStatus.Department;
                list.DataSource = Facade.GetAllClientsByStatus(StatusVal);
                list.DataTextField = "CompanyName";
                list.DataValueField = "Id";
                list.DataBind();
                list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0")); 
               
            }
            else
            {
                StatusVal = (int)CompanyStatus.Client;
                list.DataSource = Facade.GetAllClientsByStatus(StatusVal);
                list.DataTextField = "CompanyName";
                list.DataValueField = "Id";
                list.DataBind();               
            }

        }

        void PopulateRequisition(int StatusId)
        {
            ddlRequisition.Items.Clear();
            System.Collections.ArrayList arr = Facade.GetAllJobPostingListByStatusId(StatusId);
            JobPosting j = new JobPosting();
            j.Id = 0;
            j.JobTitle = "Any";
            arr.Insert(0, j);
            ddlRequisition.DataSource = arr;
            ddlRequisition.DataTextField = "JobTitle";
            ddlRequisition.DataValueField = "Id";
            ddlRequisition.DataBind();
            ControlHelper.SelectListByValue(ddlRequisition, hdnRequisitionSelected.Value);
        }


        private void PrepareView()
        {
           // SelectAccountList();
            //PopulateClientDropdowns(ddlAccount.ListItem);
            PopulateClientDropdowns(ddlAccount);
            divCandidateSourcingReport.Visible = true;
            PopulateRequisition(0);
            MiscUtil.PopulateHiringMatrixLevelsList(ddlTopLevelStatus, Facade);
            MiscUtil.PopulateHiringMatrixLevelsList(ddlSubLevelStatus, Facade);
            MiscUtil.PopulateMemberListByRole(ddlRecruiter, ContextConstants.ROLE_EMPLOYEE, Facade);
            MiscUtil.PopulateMemberListByRole(ddlLead, ContextConstants.ROLE_EMPLOYEE, Facade);
            
        }

        private void SelectAccountList()
        {
            if (Request.Cookies["RequisitionStatus"] != null)
            {
                //ddlAccount.SelectedItems = Request.Cookies["RequisitionStatus"].Value;
                ddlAccount.SelectedValue = Request.Cookies["RequisitionStatus"].Value;
            }
            else
            {
                ddlAccount.SelectedValue  ="0";
                //foreach (ListItem item in ddlAccount.ListItem.Items)
                //{
                //    item.Selected = false;
                //}
            }
        }
        private void ResetDataSourceParameters()
        {

        }

        private void SetsStatusValue()
        {
            int value;
             if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Department)
            {
                value = (int)CompanyStatus.Department;
                companyStatus = value.ToString();
                odsCandidateList.SelectParameters["CompanyStatus"].DefaultValue = companyStatus;
            }
             else if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Client)
             {
                 value = (int)CompanyStatus.Client;
                 companyStatus = value.ToString();
                 odsCandidateList.SelectParameters["CompanyStatus"].DefaultValue = companyStatus;

             }
             else 
             {
                 value = (int)CompanyStatus.Vendor;
                 companyStatus = value.ToString();
                 odsCandidateList.SelectParameters["CompanyStatus"].DefaultValue = companyStatus;
             }
           
        }

        private void SetDataSourceParameters()
        {
           
            if (SortOrder.Text == string.Empty && SortColumn.Text == string.Empty)
            {
                SortOrder.Text = "asc";
                SortColumn.Text = "[C].[FirstName]";
            }
            //odsCandidateList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
            odsCandidateList.SelectParameters["ByCandidateName"].DefaultValue = MiscUtil.RemoveScript(txtCandidateName.Text);
            //odsCandidateList.SelectParameters["ByLocation"].DefaultValue = MiscUtil.RemoveScript(txtLocation.Text);
            odsCandidateList.SelectParameters["ByRequisition"].DefaultValue = ddlRequisition.SelectedValue;
            odsCandidateList.SelectParameters["ByAccount"].DefaultValue = ddlAccount.SelectedValue;
            //odsCandidateList.SelectParameters["ByAccount"].DefaultValue = ddlAccount.SelectedItems.ToString();
            
            odsCandidateList.SelectParameters["ByTopLevelStatus"].DefaultValue = ddlTopLevelStatus.SelectedValue;
            odsCandidateList.SelectParameters["BySubLevelStatus"].DefaultValue = ddlSubLevelStatus.SelectedValue;
            odsCandidateList.SelectParameters["ByRecruiter"].DefaultValue = ddlRecruiter.SelectedValue;
            odsCandidateList.SelectParameters["ByLead"].DefaultValue = ddlLead.SelectedValue;
            odsCandidateList.SelectParameters["SourcedDateFrom"].DefaultValue = DateTime.MinValue.ToString();
            odsCandidateList.SelectParameters["SourcedDateTo"].DefaultValue = DateTime.MinValue.ToString();
            odsCandidateList.SelectParameters["SubmissionFrom"].DefaultValue = DateTime.MinValue.ToString();
            odsCandidateList.SelectParameters["SubmissionTo"].DefaultValue = DateTime.MinValue.ToString();
            odsCandidateList.SelectParameters["OfferedFrom"].DefaultValue = DateTime.MinValue.ToString();
            odsCandidateList.SelectParameters["OfferedTo"].DefaultValue = DateTime.MinValue.ToString();
            odsCandidateList.SelectParameters["JoinedFrom"].DefaultValue = DateTime.MinValue.ToString();
            odsCandidateList.SelectParameters["JoinedTo"].DefaultValue = DateTime.MinValue.ToString();
            SetsStatusValue();  
        }

        public void MessageAnswered(object sender, ConfirmationWindow.MsgBoxEventArgs e)
        {
            if (e.Answer == ConfirmationWindow.enmAnswer.OK)
            {
                repor("pdf");
            }

        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            uclConfirm.MsgBoxAnswered += MessageAnswered;
            CookieName = "CandidateReportUserColumnOption_" + base.CurrentMember.PrimaryEmail;
            CustomSiteMap CustomMap = new CustomSiteMap();
            CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(358, CurrentMember.Id);
            if (CustomMap == null) _IsAccessToEmployee = false;
            else
            {
                IdForSitemap = CustomMap.Id;
                UrlForEmployee = "~/" + CustomMap.Url.ToString();
            }
            dvHiringMatrixLevel.Visible = false;
            dvlocation.Visible = false;
            if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
            {
                lblAccountname.Text = "BU";
            }
            else
            {
                lblAccountname.Text = "Account";
            }
            
            if (!IsPostBack)
            {
                PrepareView();
            }
            if (!IsPostBack)
            {
                btnSearch_Click(sender, e);
            }
        }

       
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string s = hdnRequisitionSelected.Value;
            hdnScrollPos.Value = "0";
            lsvCandidateSourcingRpt.Items.Clear();
            //Response.Cookies["RequisitionStatus"].Value = ddlAccount.SelectedItems;
            Response.Cookies["RequisitionStatus"].Value = ddlAccount.SelectedValue;
            SetDataSourceParameters();
            ASP.controls_pagercontrol_ascx PagerControls = (ASP.controls_pagercontrol_ascx)this.lsvCandidateSourcingRpt.FindControl("pagerControl");
            if (PagerControls != null)
            {
                DataPager pager = (DataPager)PagerControls.FindControl("pager");
                if (pager != null)
                {
                    if (pager.Page.IsPostBack)
                        pager.SetPageProperties(0, pager.MaximumRows, true);
                }
            }
            divExportButton.Visible = false;
            BindList();

            string pagesize = "";
            pagesize = (Request.Cookies["CandidateSourcingReportRowPerPage"] == null ? "" : Request.Cookies["CandidateSourcingReportRowPerPage"].Value); ;
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvCandidateSourcingRpt.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
                    if (pager.Page.IsPostBack)
                        pager.SetPageProperties(0, pager.MaximumRows, true);
                }
            }
            PlaceUpDownArrow();
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
            divExportButton.Visible = false;
        }

        public void repor(string rep)
        {
            SetsStatusValue();
            GenerateCandidateSourcingReport(rep);

        }

        protected void btnExportToPDF_Click(object sender, EventArgs e)
        {
            uclConfirm.AddMessage("Note: Some columns may not appear in report if too many are selected.", ConfirmationWindow.enmMessageType.Attention, true, true, "");

        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            repor("excel");
        }

        protected void btnExportToWord_Click(object sender, EventArgs e)
        {
            repor("word");
        }

        protected void lsvCandidateSourcingRpt_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvCandidateSourcingRpt.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "CandidateSourcingReportRowPerPage";
            }

            PlaceUpDownArrow();
            if (IsPostBack)
            {
                if (lsvCandidateSourcingRpt.Items.Count == 0)
                {
                    lsvCandidateSourcingRpt.DataSource = null;
                    lsvCandidateSourcingRpt.DataBind();
                }
            }

            LinkButton lnkBtnAccount = (LinkButton)lsvCandidateSourcingRpt.FindControl("lnkBtnAccount");
            if (lnkBtnAccount != null)
            {
                lnkBtnAccount.Text = lblAccountname.Text;
                lnkBtnAccount.ToolTip = "Sort By " + lblAccountname.Text;

            }
        }

        protected void lsvCandidateSourcingRpt_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                        else txtSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn.Text = lnkbutton.ID;
                        txtSortOrder.Text = "ASC";
                    }
                    if (SortColumn.Text == string.Empty || SortColumn.Text != e.CommandArgument.ToString())
                        SortOrder.Text = "asc";
                    else SortOrder.Text = SortOrder.Text == "asc" ? "desc" : "asc";
                    SortColumn.Text = e.CommandArgument.ToString();
                    odsCandidateList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
                }
            }
            catch
            {
            }
        }
        protected void lsvCandidateSourcingRpt_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            lsvCandidateSourcingRpt.Visible = true;
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                CandidateSourcingInfo candidatSourcingInfo = ((ListViewDataItem)e.Item).DataItem as CandidateSourcingInfo;

                if (candidatSourcingInfo != null)
                {
                    divExportButton.Visible = true;
                    Label lblCandidateID = (Label)e.Item.FindControl("lblCandidateID");
                    Label lblCandidateName = (Label)e.Item.FindControl("lblCandidateName");
                    Label lblRequisition = (Label)e.Item.FindControl("lblRequisition");
                    Label lblAccount = (Label)e.Item.FindControl("lblAccount");
                    Label lblJobPostingCode = (Label)e.Item.FindControl("lblJobPostingCode");
                    //Label lblLocation = (Label)e.Item.FindControl("lblLocation");
                    Label lblResumeSource = (Label)e.Item.FindControl("lblResumeSource");
                    Label lblSourcedDate = (Label)e.Item.FindControl("lblSourcedDate");
                    Label lblCurrentStatus = (Label)e.Item.FindControl("lblCurrentStatus");
                    Label lblClientSubmissionDate = (Label)e.Item.FindControl("lblClientSubmissionDate");
                    Label lblOfferedDate = (Label)e.Item.FindControl("lblOfferedDate");
                    Label lblJoineddate = (Label)e.Item.FindControl("lblJoineddate");
                    Label lblRecruiter = (Label)e.Item.FindControl("lblRecruiter");
                    Label lblLead = (Label)e.Item.FindControl("lblLead");
                    Label lblRemarks = (Label)e.Item.FindControl("lblRemarks");
                    Label lblStatus = (Label)e.Item.FindControl("lblStatus");

                    lblCandidateID.Text = "A" + candidatSourcingInfo.Id.ToString();
                    lblCandidateName.Text = candidatSourcingInfo.CandidateName;
                    lblAccount.Text = candidatSourcingInfo.Account;
                    lblJobPostingCode.Text = candidatSourcingInfo.JobPostingCode;
                    lblRequisition.Text = candidatSourcingInfo.Requisition;
                    lblCurrentStatus.Text = candidatSourcingInfo.CurrentStatus;
                    //lblLocation.Text = candidatSourcingInfo.Location;
                    lblResumeSource.Text = candidatSourcingInfo.ResumeSourceName;
                    if (candidatSourcingInfo.SourcedDate != DateTime.MinValue)
                    {
                        lblSourcedDate.Text = candidatSourcingInfo.SourcedDate.ToShortDateString();
                    }
                    if (candidatSourcingInfo.ClientSubmissionDate != DateTime.MinValue)
                    {
                        lblClientSubmissionDate.Text = candidatSourcingInfo.ClientSubmissionDate.ToShortDateString();
                    }
                    if (candidatSourcingInfo.OfferedDate != DateTime.MinValue)
                    {
                        lblOfferedDate.Text = candidatSourcingInfo.OfferedDate.ToShortDateString();
                    }
                    if (candidatSourcingInfo.Joineddate != DateTime.MinValue)
                    {
                        lblJoineddate.Text = candidatSourcingInfo.Joineddate.ToShortDateString();
                    }
                    lblRecruiter.Text = candidatSourcingInfo.Recruiter;
                    lblLead.Text = candidatSourcingInfo.Lead;
                    lblRemarks.Text = candidatSourcingInfo.Remarks;
                    if (candidatSourcingInfo.Status == 1)
                    {
                        lblStatus.Text = "InActive";
                    }
                    else
                    {
                        int reqCount = Facade.GetActiveRequsiutionCountByMemberId(candidatSourcingInfo.Id);
                        if (reqCount > 1)
                        {
                            lblStatus.Text = "Profile is Process By " + reqCount.ToString();
                        }
                        else
                        {
                            lblStatus.Text = "Profile in Process by " + Facade.GetRecruiternameByMemberId(candidatSourcingInfo.Id);
                        }
                    }

                }
            }
        }


        #endregion

    }
}