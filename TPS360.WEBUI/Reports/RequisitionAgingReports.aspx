﻿ <%---------------------------------------------------------------------------------------------------------------------------------------------
    FileName        :   RequisitionAgingReport.aspx
    Description     :   Requisition Aging Report
    Created By      :   Prasanth Kumar G
    Created On      :   20/May/2015
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.         Date            Author                  MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             11/Jun/2015     Prasanth Kumar G        Introduced Turn Around Time and Time To Fill
    0.2             24/Jun/2015     Prasanth Kumar G        Changed the Tool Tip Name
   ----------------------------------------------------------------------------------------------------------------------------------------        --%>

<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="RequisitionAgingReports.aspx.cs"
    EnableEventValidation="false" Inherits="TPS360.Web.UI.Reports.RequisitionAgingReports"
    Title="Requisition Aging Report" %>
  <%@ Register Src="~/Controls/DateRangePicker.ascx" TagName = "DateRangePicker" TagPrefix ="ucl" %>
  <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
  <%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
  <%@ Register Src="~/Controls/ConfirmationWindow.ascx" TagName="Confirm" TagPrefix="ucl" %>
  <%@ Register Src ="~/Controls/CountryStatePicker.ascx" TagName ="CountryState" TagPrefix ="ucl" %>
<asp:Content ID="title" ContentPlaceHolderID="cphHomeMasterTitle" runat="server">
    Requisition Aging Report
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHomeMaster" runat="Server">

    <script src="../Scripts/jsUpdateProgress.js" type="text/javascript"></script>

    <script src="../Scripts/ToolTipscript.js"></script>
    <script src="../js/JobPostingByStatus.js"></script>
    <link type="text/css" href="../Style/ToolTip.css" rel="Stylesheet" />
    <script language="javascript" type="text/javascript">

        //To Keep the scroll bar in the Exact Place
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoadedReq);
        function PageLoadedReq(sender, args) {
            try {
               
                var hdnScroll = document.getElementById('ctl00_cphHomeMaster_hdnScrollPos');
                var bigDiv = document.getElementById('ctl00_cphHomeMaster_divlsvJobPosting');
                bigDiv.scrollLeft = hdnScroll.value;
            }
            catch (e) {
            }
        }
        function SetScrollPosition() {
            var hdnScroll = document.getElementById('ctl00_cphHomeMaster_hdnScrollPos');
            var bigDiv = document.getElementById('ctl00_cphHomeMaster_divlsvJobPosting');
            hdnScroll.value = bigDiv.scrollLeft;
        }
    </script>

    <script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

    <script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>

    <ucl:Confirm ID="uclConfirm" runat="server"></ucl:Confirm>
    <asp:UpdatePanel ID="pnlCandidatereport" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="text-align: left">
                <asp:Label ID="lbMessage" runat="server" EnableViewState="false"></asp:Label>
            </div>
            <asp:HiddenField ID="hdnSortColumn" runat="server" />
            <%--<asp:HiddenField ID="hdnRequisitionSelected" runat="server" Value="0" />--%>
            
            <asp:HiddenField ID="hdnSortOrder" runat="server" />
            <asp:HiddenField ID="hdnScrollPos" runat="server" />
            <asp:HiddenField ID="hdnMatrixLevels" runat="server" />
            <asp:ObjectDataSource ID="odsJobPostingList" runat="server" SelectMethod="GetPaged"
                TypeName="TPS360.Web.UI.JobPostingDataSource" SelectCountMethod="GetListCount"
                EnablePaging="True" SortParameterName="sortExpression">
                <SelectParameters>
                    <asp:Parameter Name="IsTemplate" DefaultValue="False" Type="Boolean" Direction="Input" />
                    <%--<asp:ControlParameter ControlID="ddlJobStatus" Name="jobStatus" PropertyName="SelectedValue"
                        Type="String" />
                    <asp:ControlParameter ControlID="ddlJobStatus" Name="subJobStatus" PropertyName="SelectedValue"
                        Type="String" />
                        
                    <asp:ControlParameter ControlID="ddlRequisition" Name = "Account" PropertyName="SelectedValue"
                       Type="String" />--%>
                    <asp:ControlParameter ControlID="dtPicker" Name="ReqStartDateFrom"
                        PropertyName="StartDate" Type="String" />
                    <asp:ControlParameter ControlID="dtPicker" Name="ReqStartDateTo" PropertyName="EndDate"
                        Type="String" />
                    <asp:ControlParameter ControlID="ddlRequisitionCreator" Name="ReqCreator" PropertyName="SelectedValue"
                        Type="String" />
                     <%--   
                    <asp:ControlParameter ControlID="ddlEmployee" Name="employee" PropertyName="SelectedValue"
                        Type="String" />
                    <asp:ControlParameter ControlID="TxtCity" Name="City" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="uclCountryState" Name="StateID" PropertyName="SelectedStateId"
                        Type="String" />
                    <asp:ControlParameter ControlID="uclCountryState" Name="CountryID" PropertyName="SelectedCountryId"
                        Type="String" />--%>
                    <asp:ControlParameter ControlID="ddlEndClient" Name="endClients" PropertyName="SelectedValue"
                        Type="String" />
              <%--      <asp:ControlParameter ControlID="hdnRequisitionSelected" Name="JobPostingId" PropertyName="Value"
                        Type="String" />--%>
                   <%-- <asp:ControlParameter ControlID="hdnAccountSelected" Name="AccountId" PropertyName="Value"
                        Type="String" /> --%>   
                        
                </SelectParameters>
            </asp:ObjectDataSource>
            
            
            
            
            <asp:Panel ID="pnlSearchRegion" runat="server" DefaultButton="btnSearch">
                <div style="width: 100%;">
                    <div class="TableRow">
                        <div class="TabPanelHeader nomargin" style="border-style: none none none;">Filter Options</div>
                    </div>
                    <div class="TableRow" style="text-align: center">
                        <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                    </div>
                    <div class="FormLeftColumn" style="width: 50%;">
                    
                        <div class="TableRow" style="white-space: nowrap">
                            <div class="TableFormLeble" style="width: 40%;">
                                <asp:Label EnableViewState="false" ID="lblDateHeader" runat="server" Text="Start Date Range"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                              <ucl:DateRangePicker ID="dtPicker" runat ="server"  />
                            </div>
                        </div>
                         <div class="TableRow">
                            <div class="TableFormLeble" style="width: 40%;">
                                <asp:Label EnableViewState="true" ID="lblByEndClientsHeader" runat="server"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:DropDownList ID="ddlEndClient" CssClass="CommonDropDownList" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                         <%--
                        <div class="TableRow" style="white-space: nowrap">
                            <div class="TableFormLeble" style="width: 40%;">
                                <asp:Label EnableViewState="false" ID="lblByRequisition" runat="server" Text="By Requisition"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:DropDownList EnableViewState="true" ID="ddlRequisition" runat="server" CssClass="CommonDropDownList"
                                    AutoPostBack="false">
                                </asp:DropDownList>
                            </div>
                         </div>
                       <div class="TableRow" style="white-space: nowrap;">
                            <div class="TableFormLeble" style="width: 50%;">
                                <asp:Label EnableViewState="false" ID="lblByLocation" runat="server" Text="By City"></asp:Label>
                                :
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox EnableViewState="false" ID="txtCity" CssClass="SearchTextbox" runat="server"></asp:TextBox>
                            </div>
                        </div>
                       <ucl:CountryState ID="uclCountryState" runat="server" FirstOption="Any" TableFormLabel_Width="50%" ShowAndHideState ="False" />
                    </div>--%>
                      <div class="TableRow">
                            <div class="TableFormLeble" style="width: 40%;">
                                <asp:Label EnableViewState="false" ID="lblByReqCreator" runat="server" Text="By Req Creator"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:DropDownList ID="ddlRequisitionCreator" runat="server" CssClass="CommonDropDownList">
                                </asp:DropDownList>
                            </div>
                        </div>
              </div>
                    <div class="FormRightColumn" style="width: 49%">
                        <%-- <div class="TableRow">
                            <div class="TableFormLeble" style="width: 40%;">
                                <asp:Label EnableViewState="true" ID="lblByEndClientsHeader" runat="server"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:DropDownList ID="ddlEndClient" CssClass="CommonDropDownList" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>--%>
                        <%--<div class="TableRow">
                            <div class="TableFormLeble" style="width: 40%;">
                                <asp:Label EnableViewState="false" ID="lblJobStatusHeader" runat="server" Text="By Requisition Status"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:DropDownList EnableViewState="true" ID="ddlJobStatus" runat="server" CssClass="CommonDropDownList"
                                    AutoPostBack="false" OnSelectedIndexChanged="ddlJobStatus_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </div>--%>
                       
                        
                      
                       <%-- <div class="TableRow">
                            <div class="TableFormLeble" style="width: 40%;">
                                <asp:Label EnableViewState="false" ID="lblByEmployee" runat="server" Text="By User"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:DropDownList ID="ddlEmployee" CssClass="CommonDropDownList" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>--%>

                    </div>
                    
                    
                        <%--<div class="TableRow well well-small nomargin">
                        <asp:CollapsiblePanelExtender ID="cpnlCandidateTopBar" runat="server" TargetControlID="pnlContent"
                            ExpandControlID="pnlHeader" CollapseControlID="pnlHeader" Collapsed="true" ImageControlID="imgShowHide"
                            CollapsedImage="~/Images/expand-plus.png" ExpandedImage="~/Images/collapse-minus.png"
                            SuppressPostBack="true">
                        </asp:CollapsiblePanelExtender>
                        <asp:Panel ID="pnlHeader" runat="server">
                            <div class="" style="clear: both; cursor :pointer ;">
                                <asp:Image ID="imgShowHide" ImageUrl="~/Images/expand-plus.png" runat="server" />
                                Included Columns
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="pnlContent" runat="server" Style="overflow: hidden;" Height="0">
                            <div class="TableRow" style="text-align: left; padding-top: 5px">
                                <asp:CheckBox runat="server" Text="Select All" ID="chkColumns" CssClass ="ColumnSelection" onclick="javascript:SelectUnselectColumnOption(this);" />
                                <asp:CheckBoxList ID="chkColumnList" runat="server" RepeatColumns="5" CssClass ="ColumnSelection" onclick="javascript:SelectUnselectAllColumnOption('chkColumns')">
                                    <asp:ListItem Value="PostedDate" Selected="True">Post Date</asp:ListItem>
                                    <asp:ListItem Value="JobTitle" Selected="True">Job Title</asp:ListItem>
                                    <asp:ListItem Value="JobPostingCode" Selected="True">Req. Code</asp:ListItem>
                                    <asp:ListItem Value="TimeToFill" Selected="True">Time To Fill</asp:ListItem>
                                    <asp:ListItem Value="ClientJobId">Job ID</asp:ListItem>
                                    <asp:ListItem Value="NoOfOpenings" Selected="True">No of Openings</asp:ListItem>
                                    <asp:ListItem Value="NoofUnfilledOpenings" Selected="True">No. of Unfilled Openings</asp:ListItem>
                                    <asp:ListItem Value="PayRate" Selected="True">Pay Rate</asp:ListItem>
                                    
                                    <asp:ListItem Value="JobLocation"  Selected="True">Job Location</asp:ListItem>
                                    <asp:ListItem Value="SalesRegion" Enabled="false" Selected="True">Sales Region</asp:ListItem>
                                    <asp:ListItem Value="SalesGroup" Selected="True">Sales Group</asp:ListItem>
                                    <asp:ListItem Value="POAvailability" Selected="True">PO Availability</asp:ListItem>
                                    <asp:ListItem Value="CustomerName" Selected="True">Customer Name</asp:ListItem>
                                    
                                    <asp:ListItem Value="TravelRequired">Travel Required</asp:ListItem>
                                    <asp:ListItem Value="OtherBenefits">Benefits</asp:ListItem>
                                    <asp:ListItem Value="JobStatus" Selected="True">Current Status</asp:ListItem>
                                    <asp:ListItem Value="JobDurationLookupId" Selected="True">Employment Type</asp:ListItem>
                                    <asp:ListItem Value="JobDurationMonth" Selected="True">Duration</asp:ListItem>
                                    <asp:ListItem Value="JobAddress1">Address 1</asp:ListItem>
                                    <asp:ListItem Value="JobAddress2">Address 2</asp:ListItem>
                                    <asp:ListItem Value="City" Selected="True">City</asp:ListItem>
                                    <asp:ListItem Value="StateId" Selected="True">State</asp:ListItem>
                                    <asp:ListItem Value="ZipCode">Zip Code</asp:ListItem>
                                    <asp:ListItem Value="CountryId">Country</asp:ListItem>
                                    <asp:ListItem Value="ReportingTo">Reporting To</asp:ListItem>
                                    <asp:ListItem Value="NoOfReportees">No. of Reportees</asp:ListItem>
                                    <asp:ListItem Value="StartDate" Selected="True">Start Date</asp:ListItem>
                                    <asp:ListItem Value="FinalHiredDate" Selected="True">Closing Date</asp:ListItem>
                                    <asp:ListItem Value="AuthorizationTypeLookupId">Work Authorization</asp:ListItem>
                                    
                                    <asp:ListItem Value="RequiredDegreeLookupId">Education</asp:ListItem>
                                    <asp:ListItem Value="ExpRequired">Exp Required</asp:ListItem>
                                    <asp:ListItem Value="ClientId" Selected="True">BU</asp:ListItem>
                                    <asp:ListItem Value="BUContact" Selected="True">BU Contact</asp:ListItem>
                                    <asp:ListItem Value="TeleCommunication">Telecommuting</asp:ListItem>
                                    <asp:ListItem Value="CreatorId" Selected="True">Published By</asp:ListItem>
                                    <asp:ListItem Value="UpdateDate">Date Updated</asp:ListItem>
                                    <asp:ListItem Value="JobCategory">Job Category</asp:ListItem>
                                    
                                    <asp:ListItem Value="Skills" Selected="True">Skills</asp:ListItem>
                                    <asp:ListItem Value="MinimumQualifyingParameters">Minimum Qualifying Parameters</asp:ListItem>
                                    <asp:ListItem Value="WorkSchedule" Selected="True">Work Schedule</asp:ListItem>
                                    <asp:ListItem Value="SecurityClearance">Security Clearance</asp:ListItem>
                                    <asp:ListItem Value="Expenses">Expenses</asp:ListItem>
                                    <asp:ListItem Value="AdditionalNotes">Additional Notes</asp:ListItem>
                                    <asp:ListItem Value="AssignedRecruiters" Selected="True">Assigned Recruiters</asp:ListItem>
                                    <asp:ListItem Value="TurnArroundTime" Selected="True">
                                                    
                                                        Turn Around Time <input class="span2" type="image" style ="  width :15px; height :15px" src ="../Images/tooltip_icon.png"  rel="tooltip" data-original-title="Time between the requisition publish date and the first candidate offer." data-placement="right" value="" onclick ="javascript:return false;"   >                                                             
                                    </asp:ListItem>
                                    <asp:ListItem Value="ScheduledInterviews">Scheduled Interviews </asp:ListItem>
                                    <asp:ListItem Value="Submissions">Submissions</asp:ListItem>
                                    <asp:ListItem Value="Offers">Offers</asp:ListItem>
                                    <asp:ListItem Value="Joined">Joined</asp:ListItem>
                                    <asp:ListItem Value="JPICN" Text="Interview Candidate Name"></asp:ListItem>
                                    <asp:ListItem Value="JPSCN" Text="Submitted Candidate Name"></asp:ListItem>
                                    <asp:ListItem Value="JPOCN" Text="Offered Candidate Name"></asp:ListItem>
                                    <asp:ListItem Value="JPJCN" Text="Joined Candidate Name"></asp:ListItem>
                                    <asp:ListItem Value="JobDesc" Text="Job Description"></asp:ListItem>
                                    <asp:ListItem Value="Jobpostnotes" Text="Notes"></asp:ListItem>
                                    <asp:ListItem Value="RequisitionType" Text="Requisition Type"></asp:ListItem>
                                    
                                </asp:CheckBoxList>
                            </div>
                        </asp:Panel>
                    </div>--%>
                    <div class="PanelDevider">
                    </div>
<%--                    <div class="TableRow well well-small nomargin">
                        <asp:CollapsiblePanelExtender ID="cpnlHiringMatrix" runat="server" TargetControlID="pnlHiringMatrix"
                            ExpandControlID="pnlHiringHeader" CollapseControlID="pnlHiringHeader" Collapsed="true"
                            ImageControlID="imgShowHideHirining" CollapsedImage="~/Images/expand-plus.png"
                            ExpandedImage="~/Images/collapse-minus.png" SuppressPostBack="true">
                        </asp:CollapsiblePanelExtender>
                        <asp:Panel ID="pnlHiringHeader" runat="server">
                            <div class="" style="clear: both; cursor :pointer ;">
                                <asp:Image ID="imgShowHideHirining" ImageUrl="~/Images/expand-plus.png" runat="server" />
                                Hiring Matrix Levels
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="pnlHiringMatrix" runat="server" Style="overflow: hidden;" Height="0">
                            <div class="TableRow" style="text-align: left; padding-top: 5px">
                                <asp:CheckBox runat="server" Text="Select All Levels" ID="chkAll" CssClass ="ColumnSelection" onclick="javascript:SelectUnselectheader(this);" />
                                <div id="divHiringList">
                                    <asp:CheckBoxList ID="chkHiringList" runat="server" RepeatColumns="5" onclick="javascript:SelectUnselectAllMLevels('chkAll')" CssClass ="ColumnSelection">
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>--%>           
                    <div class="TableRow" style="text-align: center; padding-top: 5px; padding-bottom: 5px">
                        <asp:LinkButton ID="btnSearch" runat="server" Text="Search" AlternateText="Search" CssClass="btn btn-primary"
                            EnableViewState="false" OnClick="btnSearch_Click" ValidationGroup="Search" ><i class="icon-search icon-white"></i> Search</asp:LinkButton>
                        <asp:Button ID="btnClear" runat="server" Text="Clear" AlternateText="Clear" CssClass="btn"
                            EnableViewState="false" OnClick="btnClear_Click" />
                    </div>
                    <div class="TableRow" runat="server" id="ExportButtons" style="padding-bottom: 5px;">
                        <div class="TabPanelHeader nomargin" style="margin-bottom: 5px;border-style: none none none;">Report Results</div>
                         <asp:ImageButton ID="btnExportToExcel" CssClass ="btn"  SkinID ="sknExportToExcel" runat ="server" ToolTip ="Export To Excel" OnClick ="btnExportToExcel_Click" />
                            <asp:ImageButton ID="btnExportToPDF" CssClass ="btn"  SkinID ="sknExportToPDF" runat ="server" ToolTip ="Export To PDF" OnClick ="btnExportToPDF_Click" />
                            <asp:ImageButton ID="btnExportToWord" CssClass ="btn"  SkinID ="sknExportToWord" runat ="server" ToolTip ="Export To Word" OnClick ="btnExportToWord_Click" />
                            
                       <%--    <asp:Button ID="btnExportToPDF" runat="server" Text="Export To PDF" AlternateText="Export To PDF"
                            CssClass="btn" EnableViewState="false" OnClick="btnExportToPDF_Click" />
                        <asp:Button ID="btnExportToExcel" runat="server" Text="Export To Excel" AlternateText="Export To Excel"
                            CssClass="btn" EnableViewState="false" OnClick="btnExportToExcel_Click" />
                        <asp:Button ID="btnExportToWord" runat="server" Text="Export To Word" AlternateText="Export To Word"
                            CssClass="btn" EnableViewState="false" OnClick="btnExportToWord_Click" />
                            --%>
                    </div>
                  </div>
                 </asp:Panel>

            <asp:Panel ID="pnlGridRegion" runat="server">
                <div id="divlsvJobPosting" runat="server" class="GridContainer" style="overflow: auto; overflow-y: hidden; width: 100%; text-align: left;" onscroll='SetScrollPosition()'>
                    <%--0.7--%>
                    <asp:ListView ID="lsvJobPosting" runat="server" DataKeyNames="Id" OnItemDataBound="lsvJobPosting_ItemDataBound"
                        OnItemCommand="lsvJobPosting_ItemCommand" OnPreRender="lsvJobPosting_PreRender"
                        EnableViewState="true">
                        <LayoutTemplate>
                            <table id="tlbTemplate" runat="server" class="Grid ReportGrid" cellspacing="0" border="0">
                                <tr id="trHeader" runat="server">
                                  
                                    
                                    <th runat="server" id="thJobTitle" style="min-width: 100px; white-space: nowrap"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnJobTitle" CommandName="Sort" CommandArgument="JobTitle"
                                            Text="Job Title" ToolTip="Sort By Job Title" EnableViewState="false" />
                                    </th>
                                     <%--<th runat="server" id="thPostDate" style="min-width: 100px; white-space: nowrap"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnPostdate" CommandName="Sort" CommandArgument="[J].[PostedDate]"
                                            Text="Post Date" ToolTip="Sort By Post Date" EnableViewState="false" />
                                    </th>--%>
                                    
                                    <th runat="server" id="thStartDate" style="min-width: 100px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnStartDate" CommandName="Sort" CommandArgument="OpenDate"
                                            Text="Start Date" ToolTip="Sort By Start Date" />
                                    </th>
                                    <th runat="server" id="thFinalHiredDate" style="min-width: 100px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnFinalHiredDate" CommandName="Sort" CommandArgument="FinalHireDate"
                                            Text="Closing Date" ToolTip=" Sort By Closing Date" EnableViewState="false" />
                                    </th>
                                    <%--Code Modified by Prasanth on 24/Jun/2015 Start--%>
                                    <th runat="server" id="thTotalTimeTaken" style="min-width: 100px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnTotalTimeTaken" CommandName="Sort" CommandArgument="TotalTimeTaken"
                                            Text="Total Time Taken" ToolTip=" Sort By Total Time Taken" EnableViewState="false" />
                                    </th>
                                    <%--Code introduced by Prasanth on 11/Jun/2015 Start--%>
                                     <th runat="server" id="thTimeToFill" style="min-width: 100px"  enableviewstate ="false" >
                                        <asp:Label runat="server" ID="btnTimeToFill" CommandName="Sort" CommandArgument="TimeToFill"
                                            Text="Time To Fill" ToolTip="Sort By Time To Fill" EnableViewState="false" />
                                    </th>
                                    <%--******************END************************--%>
                                </tr>
                                <tr id="itemPlaceholder" runat="server">
                                </tr>
                                <tr class="Pager">
                                    <td id="tdpager" runat="server" colspan="11">
                                        <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                    </td>
                                </tr>
                            </table>
                        </LayoutTemplate>
                        <EmptyDataTemplate>
                            <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                <tr>
                                    <td>
                                        No data was returned.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                              
                                <td runat="server" id="tdReqTitle">
                                    <asp:HyperLink ID="lnkJobTitle" runat="server" Target="_blank" ></asp:HyperLink>
                                </td>
                                 <td runat="server" id="tdPostdate">
                                    <asp:Label ID="lblPostdate" runat="server" Target="_blank" ></asp:Label>
                                </td>
                                <td runat="server" id="tdFinalHiredDate">
                                    <asp:Label runat="server" ID="lblFinalHiredDate" />
                                </td>
                               <td runat="server" id="tdTotalTimeTaken">
                                    <asp:Label runat="server" ID="lblTotalTimeTaken" />
                                </td>
                                
                                <%--Code introduced by Prasanth on 11/Jun/2015 Start--%>
                                <td runat="server" id="tdTimeToFill">
                                    <asp:Label runat="server" ID="lblTimeToFill"  />
                                </td>
                                <%--******************END************************--%>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
            </asp:Panel>
          
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportToPDF" />
            <asp:PostBackTrigger ControlID="btnExportToExcel" />
            <asp:PostBackTrigger ControlID="btnExportToWord" />
        </Triggers>
    </asp:UpdatePanel>

    <script language="javascript" type="text/javascript">


        function SelectUnselectheader(chkAllheader) 
        {
           
        }

        function SelectUnselectColumnOption(chkColumnOption) {

        }

        function SelectUnselectAllMLevels(chkAllheader) {

        }

        function SelectUnselectAllColumnOption(chkColumnOptionHeader) {

        }
        
    </script>

</asp:Content>