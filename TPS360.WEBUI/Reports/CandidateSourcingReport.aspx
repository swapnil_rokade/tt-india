﻿<%---------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CandidateSourcingReport.aspx
    Description: This is the page used to display the Candidate Sourcing reports in excel,word,Pdf formats.
    Created By: 
    Created On:
    Modification Log: 
    --------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    --------------------------------------------------------------------------------------------------------------------------------------
  
    ------------------------------------------------------------------------------------------------------------------------------------------- 
--%>

<%@ Page Language="C#" MasterPageFile="~/Default.master" EnableEventValidation="false"
    AutoEventWireup="true" CodeFile="CandidateSourcingReport.aspx.cs" Inherits="TPS360.Web.UI.Reports.CandidateSourcingReport"
    Title="Candidate Sourcing Report" EnableViewStateMac="false" %>

<%@ Register Src="~/Controls/DateRangePicker.ascx" TagName="DateRangePicker" TagPrefix="ucl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/ConfirmationWindow.ascx" TagName="Confirm" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/MultipleItemPicker.ascx" TagName="MultipleSelection"
    TagPrefix="ucl" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
    Candidate Sourcing Report
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHomeMaster" runat="Server">
    <style>
        </style>

    <script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

    <script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>

    <script language="javascript" type="text/javascript">
        //To Keep the scroll bar in the Exact Place
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoadedReq);
        function PageLoadedReq(sender, args) {
            try {
                var hdnScroll = document.getElementById('ctl00_cphHomeMaster_hdnScrollPos');
                var bigDiv = document.getElementById('bigDiv');
                bigDiv.scrollLeft = hdnScroll.value;
            }
            catch (e) {
            }
        }
        function SetScrollPosition() {
            var hdnScroll = document.getElementById('ctl00_cphHomeMaster_hdnScrollPos');
            var bigDiv = document.getElementById('bigDiv');
            hdnScroll.value = bigDiv.scrollLeft;
        }

        

        
    </script>

    <style>
        .TableFormLeble
        {
            width: 25%;
        }
    </style>
    <ucl:Confirm ID="uclConfirm" runat="server"></ucl:Confirm>
    <asp:UpdatePanel ID="pnlCandidateSourcingReport" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnScrollPos" runat="server" />
            <asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="SortColumn" runat="server" EnableViewState="true" Visible="false" />
            <asp:TextBox ID="SortOrder" runat="server" EnableViewState="true" Visible="false" />
            <asp:HiddenField ID="hdnRequisitionSelected" runat="server" Value="0" />
            <asp:ObjectDataSource ID="odsCandidateList" runat="server" SelectMethod="GetPagedForCandidateSourcingRpt"
                TypeName="TPS360.Web.UI.CandidateDataSource" SelectCountMethod="GetListCountForCandidateSourcingRpt"
                EnablePaging="True" SortParameterName="sortExpression">
                <SelectParameters>
                    <asp:ControlParameter ControlID="wdcSourcedDate" Name="SourcedDateFrom" PropertyName="StartDate"
                        Type="String" />
                    <asp:ControlParameter ControlID="wdcSourcedDate" Name="SourcedDateTo" PropertyName="EndDate"
                        Type="String" />
                    <asp:ControlParameter ControlID="wdcClientSubmissionDate" Name="SubmissionFrom" PropertyName="StartDate"
                        Type="String" />
                    <asp:ControlParameter ControlID="wdcClientSubmissionDate" Name="SubmissionTo" PropertyName="EndDate"
                        Type="String" />
                    <asp:ControlParameter ControlID="wdcOfferedDate" Name="OfferedFrom" PropertyName="StartDate"
                        Type="String" />
                    <asp:ControlParameter ControlID="wdcOfferedDate" Name="OfferedTo" PropertyName="EndDate"
                        Type="String" />
                    <asp:ControlParameter ControlID="wdcJoinedDate" Name="JoinedFrom" PropertyName="StartDate"
                        Type="String" />
                    <asp:ControlParameter ControlID="wdcJoinedDate" Name="JoinedTo" PropertyName="EndDate"
                        Type="String" />
                    <asp:Parameter Name="ByRequisition" DefaultValue="" Type="String" />
                    <asp:Parameter Name="ByAccount" DefaultValue="" Type="String" />
                    <asp:Parameter Name="ByTopLevelStatus" DefaultValue="" Type="String" />
                    <asp:Parameter Name="BySubLevelStatus" DefaultValue="" Type="String" />
                    <asp:Parameter Name="ByRecruiter" DefaultValue="" Type="String" />
                    <asp:Parameter Name="ByLead" DefaultValue="" Type="String" />
                    <asp:Parameter Name="ByCandidateName" DefaultValue="" Type="String" />
                    <asp:Parameter Name="ByLocation" DefaultValue="" Type="String" />
                    <asp:Parameter Name="CompanyStatus" DefaultValue="0" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:Panel ID="pnlSearchRegion" runat="server" DefaultButton="btnSearch">
                <div style="width: 100%;">
                    <div>
                        <div class="TableRow">
                            <div class="TabPanelHeader nomargin" style="border-style: none none none;">
                                Filter Options</div>
                        </div>
                        <div class="TableRow" style="text-align: center">
                            <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                        </div>
                        <asp:Panel ID="pnlSearchContent" runat="server" Visible="true" >
                            <div class="FormRightColumn" style="width: 50%;">
                                <div class="TableRow" style="white-space: nowrap;">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="lblCandidateName" runat="server" Text="Candidate Name"></asp:Label>
                                        :
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:TextBox EnableViewState="false" ID="txtCandidateName" CssClass="SearchTextbox"
                                            runat="server" Style="width: 28%;"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="TableRow" style="white-space: nowrap">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="lblRequisition" runat="server" Text="Requisition "></asp:Label>
                                        :
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:DropDownList EnableViewState="true" ID="ddlRequisition" TabIndex="9" CssClass="CommonDropDownList"
                                            runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="TableRow" style="white-space: nowrap">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="lblAccountname" runat="server" Text="Account"></asp:Label>
                                        :
                                    </div>
                                    <div class="TableFormContent">
                                        <%--<ucl:MultipleSelection ID="ddlAccount" runat="server" />--%>
                                        <asp:DropDownList EnableViewState="true" ID="ddlAccount" TabIndex="9" CssClass="CommonDropDownList"
                                            runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div id="dvlocation" runat="server">
                                    <div class="TableRow" style="white-space: nowrap;">
                                        <div class="TableFormLeble" style="width: 40%;">
                                            <asp:Label EnableViewState="false" ID="lblLocation" runat="server" Text="Location"></asp:Label>
                                            :
                                        </div>
                                        <div class="TableFormContent">
                                            <asp:TextBox EnableViewState="false" ID="txtLocation" CssClass="SearchTextbox" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div id="dvHiringMatrixLevel" runat="server">
                                    <div class="TableRow" style="white-space: nowrap">
                                        <div class="TableFormLeble" style="width: 40%;">
                                            <asp:Label EnableViewState="false" ID="lblTopLevelStatus" runat="server" Text="Top Level Status"></asp:Label>
                                            :
                                        </div>
                                        <div class="TableFormContent">
                                            <asp:DropDownList EnableViewState="true" ID="ddlTopLevelStatus" TabIndex="9" CssClass="CommonDropDownList"
                                                runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="TableRow" style="white-space: nowrap">
                                        <div class="TableFormLeble" style="width: 40%;">
                                            <asp:Label EnableViewState="false" ID="lblSubLevelStatus" runat="server" Text="Sub Level Status"></asp:Label>
                                            :
                                        </div>
                                        <div class="TableFormContent">
                                            <asp:DropDownList EnableViewState="true" ID="ddlSubLevelStatus" TabIndex="9" CssClass="CommonDropDownList"
                                                runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="lblRecruiter" runat="server" Text="Recruiter"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:DropDownList ID="ddlRecruiter" runat="server" CssClass="CommonDropDownList">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="lblLead" runat="server" Text="Lead"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:DropDownList ID="ddlLead" CssClass="CommonDropDownList" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="FormLeftColumn" style="width: 49%">
                                <div class="TableRow" style="white-space: nowrap">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="lblSourcedDate" runat="server" Text="Sourced Date"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <ucl:DateRangePicker ID="wdcSourcedDate" runat="server" />
                                    </div>
                                </div>
                                <div class="TableRow" style="white-space: nowrap">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="lblClientSubmissionDate" runat="server" Text="Client Submission Date"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <ucl:DateRangePicker ID="wdcClientSubmissionDate" runat="server" />
                                    </div>
                                </div>
                                <div class="TableRow" style="white-space: nowrap">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="lblOfferedDate" runat="server" Text="Offered Date"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <ucl:DateRangePicker ID="wdcOfferedDate" runat="server" />
                                    </div>
                                </div>
                                <div class="TableRow" style="white-space: nowrap">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="lblJoinedDate" runat="server" Text="Joined Date"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <ucl:DateRangePicker ID="wdcJoinedDate" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <div class="TableRow" style="text-align: center; padding-top: 5px; padding-bottom: 5px;">
                                <asp:LinkButton ID="btnSearch" runat="server" Text="Search" AlternateText="Search"
                                    TabIndex="12" CssClass="btn btn-primary" EnableViewState="false" ValidationGroup="Search"
                                    OnClick="btnSearch_Click"><i class="icon-search icon-white"></i> Search</asp:LinkButton>
                                <asp:Button ID="btnClear" runat="server" Text="Clear" AlternateText="Clear" CssClass="btn"
                                    EnableViewState="false" CausesValidation="false" TabIndex="13" OnClick="btnClear_Click" />
                            </div>
                        </asp:Panel>
                        <div class="TableRow" style="padding-bottom: 5px;" id="divExportButton" runat="server"
                            visible="false">
                            <div class="TableRow">
                                <div class="TabPanelHeader nomargin" style="margin-bottom: 5px; border-style: none none none;">
                                    Report Results</div>
                            </div>
                            <asp:ImageButton ID="btnExportToExcel" CssClass="btn" TabIndex="14" SkinID="sknExportToExcel"
                                runat="server" ToolTip="Export To Excel" OnClick="btnExportToExcel_Click" />
                            <asp:ImageButton ID="btnExportToPDF" CssClass="btn" TabIndex="15" SkinID="sknExportToPDF"
                                runat="server" ToolTip="Export To PDF" OnClick="btnExportToPDF_Click" />
                            <asp:ImageButton ID="btnExportToWord" CssClass="btn" TabIndex="16" SkinID="sknExportToWord"
                                runat="server" ToolTip="Export To Word" OnClick="btnExportToWord_Click" />
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <div id="divCandidateSourcingReport" runat="server">
                <asp:Panel ID="pnlCandidateSourcingBody" runat="server">
                    <div class="GridContainer" style="overflow: auto; overflow-y: hidden; width: 100%;
                        text-align: left;" id="bigDiv" onscroll='SetScrollPosition()'>
                        <asp:ListView ID="lsvCandidateSourcingRpt" runat="server" DataKeyNames="Id" OnItemDataBound="lsvCandidateSourcingRpt_ItemDataBound"
                            OnItemCommand="lsvCandidateSourcingRpt_ItemCommand" OnPreRender="lsvCandidateSourcingRpt_PreRender">
                            <LayoutTemplate>
                                <table id="tlbTemplate" class="Grid ReportGrid" cellspacing="0" border="0">
                                    <tr id="trHeader" runat="server">
                                        <th id="thId" runat="server">
                                            <asp:LinkButton ID="lnkCandidateID" Style="min-width: 120px" runat="server" TabIndex="17"
                                                ToolTip="Sort By Candidate ID #" CommandName="Sort" CommandArgument="[C].[ID]"
                                                Text="Candidate ID #" />
                                        </th>
                                        <th runat="server" id="thName" style="min-width: 100px">
                                            <asp:LinkButton ID="lnkBtnName" runat="server" TabIndex="17" ToolTip="Sort By Name"
                                                CommandName="Sort" CommandArgument="[C].[FirstName]" Text="Name" />
                                        </th>
                                        <th runat="server" id="thAccount" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkBtnAccount" runat="server" TabIndex="18" ToolTip="Sort By Account"
                                                CommandName="Sort" CommandArgument="[CY].CompanyName" Text="Account" />
                                        </th>
                                        <th runat="server" id="thJobPostingCode" style="min-width: 100px" enableviewstate="false">
                                            <asp:LinkButton runat="server" ID="btnJobPostingCode" CommandName="Sort" CommandArgument="[J].[JobPostingCode]"
                                                Text="Req. Code" ToolTip="Sort By Requisition code" EnableViewState="false" />
                                        </th>
                                        <th runat="server" id="thRequisition" style="min-width: 100px">
                                            <asp:LinkButton ID="lnkBtnRequisition" runat="server" ToolTip="Sort By Requisition"
                                                CommandName="Sort" CommandArgument="[J].JobTitle" Text="Requisition" />
                                        </th>
                                        <th runat="server" id="thCurrentStatus" style="min-width: 100px">
                                            <asp:LinkButton ID="lnkBtnCurrentStatus" runat="server" TabIndex="20" ToolTip="Sort By Current Status"
                                                CommandName="Sort" CommandArgument="[HMS].Name" Text="Current Status" />
                                        </th>
                                        <%--<th runat="server" id="thLocation" style="min-width: 130px">
                                            <asp:LinkButton ID="lnkBtnLocation" runat="server" ToolTip="Sort By Location" CommandName="Sort"
                                                CommandArgument="[C].[Location]" Text="Location" />
                                        </th>--%>
                                        <th runat="server" id="thResumeSource" style="min-width: 100px">
                                            <asp:Label ID="lnkBtnResumeSource" runat="server" ToolTip="Sort By Resume Source"
                                                CommandName="Sort" CommandArgument="[C].[ResumeSource]" Text="Resume Source" />
                                        </th>
                                        <th runat="server" id="thSourcedDate" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkBtnSourcedDate" runat="server" TabIndex="19" ToolTip="Sort By Sourced Date"
                                                CommandName="Sort" CommandArgument="[MJC].CreateDate" Text="Sourced Date" />
                                        </th>
                                        <th runat="server" id="thClientSubmissionDate" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkBtnClientSubmissionDate" runat="server" ToolTip="Sort By Client Submission Date"
                                                CommandName="Sort" CommandArgument="[MS].SubmittedDate" Text="Client Submission Date" />
                                        </th>
                                        <th runat="server" id="thOfferedDate" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkBtnOfferedDate" runat="server" ToolTip="Sort By Offered Date"
                                                CommandName="Sort" CommandArgument="[MH].OfferedDate" Text="Offered Date" />
                                        </th>
                                        <th runat="server" id="thJoineddate" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkBtnJoineddate" runat="server" TabIndex="21" ToolTip="Sort By Joined date"
                                                CommandName="Sort" CommandArgument="[MJ].JoiningDate" Text="Joined date" />
                                        </th>
                                        <th runat="server" id="thRecruiter" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkBtnRecruiter" runat="server" ToolTip="Sort By Recruiter "
                                                CommandName="Sort" CommandArgument="[E].FirstName" Text="Recruiter " />
                                        </th>
                                        <th runat="server" id="thLead" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkBtnLead" runat="server" ToolTip="Sort By Lead" CommandName="Sort"
                                                CommandArgument="[Emp].FirstName" Text="Lead" />
                                        </th>
                                        <th runat="server" id="thRemarks" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkRemarks" runat="server" ToolTip="Sort By Remarks" CommandName="Sort"
                                                CommandArgument="[C].[Remarks]" Text="Remarks" />
                                        </th>
                                        <th runat="server" id="thStatus" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkStatus" runat="server" ToolTip="Sort By Status" CommandName="Sort"
                                                CommandArgument="[C].[Status]" Text="Status" />
                                        </th>
                                    </tr>
                                    <tr id="itemPlaceholder" runat="server">
                                    </tr>
                                    <tr class="Pager">
                                        <td id="tdpager" runat="server" colspan="15">
                                            <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                        </td>
                                    </tr>
                                </table>
                            </LayoutTemplate>
                            <EmptyDataTemplate>
                                <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                    <tr>
                                        <td>
                                            No data was returned.
                                        </td>
                                    </tr>
                                </table>
                            </EmptyDataTemplate>
                            <ItemTemplate>
                                <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                    <td id="tdId" runat="server">
                                        <asp:Label ID="lblCandidateID" runat="server"></asp:Label>
                                    </td>
                                    <td runat="server" id="tdName" style="text-align: left;">
                                        <asp:Label ID="lblCandidateName" runat="server" />
                                    </td>
                                    <td runat="server" id="tdAccount">
                                        <asp:Label ID="lblAccount" runat="server" />
                                    </td>
                                    <td runat="server" id="tdJobPostingCode">
                                        <asp:Label runat="server" ID="lblJobPostingCode" />
                                    </td>
                                    <td runat="server" id="tdRequisition">
                                        <asp:Label ID="lblRequisition" runat="server" />
                                    </td>
                                    <td runat="server" id="tdCurrentStatus">
                                        <asp:Label ID="lblCurrentStatus" runat="server" />
                                    </td>
                                    <%-- <td runat="server" id="tdLocation">
                                        <asp:Label ID="lblLocation" runat="server" />
                                    </td>--%>
                                    <td runat="server" id="tdResumeSource">
                                        <asp:Label ID="lblResumeSource" runat="server" />
                                    </td>
                                    <td runat="server" id="tdSourcedDate">
                                        <asp:Label ID="lblSourcedDate" runat="server" />
                                    </td>
                                    <td runat="server" id="tdClientSubmissionDate">
                                        <asp:Label ID="lblClientSubmissionDate" runat="server" />
                                    </td>
                                    <td runat="server" id="tdOfferedDate">
                                        <asp:Label ID="lblOfferedDate" runat="server" />
                                    </td>
                                    <td runat="server" id="tdJoineddate">
                                        <asp:Label ID="lblJoineddate" runat="server" />
                                    </td>
                                    <td runat="server" id="tdRecruiter">
                                        <asp:Label ID="lblRecruiter" runat="server" />
                                    </td>
                                    <td runat="server" id="tdLead">
                                        <asp:Label ID="lblLead" runat="server" />
                                    </td>
                                    <td runat="server" id="tdRemarks">
                                        <asp:Label ID="lblRemarks" runat="server" />
                                    </td>
                                    <td runat="server" id="tdStatus">
                                        <asp:Label ID="lblStatus" runat="server" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                </asp:Panel>
            </div>
            <asp:Panel ID="pnlmodal" runat="server" Style="display: none">
                <asp:UpdatePanel runat="server" ID="UpdatePanel2" RenderMode="Inline" UpdateMode="Conditional">
                    <ContentTemplate>
                        <img src="../Images/AjaxLoading.gif" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="Modal" runat="server" TargetControlID="pnlmodal"
                PopupControlID="pnlmodal" BackgroundCssClass="divModalBackground">
            </ajaxToolkit:ModalPopupExtender>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportToPDF" />
            <asp:PostBackTrigger ControlID="btnExportToExcel" />
            <asp:PostBackTrigger ControlID="btnExportToWord" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
