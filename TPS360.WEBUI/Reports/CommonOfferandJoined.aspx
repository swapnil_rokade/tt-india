<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="CommonOfferandJoined.aspx.cs"
    Inherits="TPS360.Web.UI.Reports.OfferandJoined" Title="Offer & Joined Report"
    EnableEventValidation="false" EnableViewStateMac="false" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/DateRangePicker.ascx" TagName="DateRangePicker" TagPrefix="ucl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/ConfirmationWindow.ascx" TagName="Confirm" TagPrefix="ucl" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
    Offer & Joined Report
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHomeMaster" runat="Server">
    <link href="../Style/TabStyle.css" rel="Stylesheet" type="text/css" />

    <script>
Sys.Application.add_load(function() {
 $('input[rel=Integer]').keypress(function (event){return allowInteger(event,$(this));});
 });
    </script>

    <div style="display:none" class="tabbable">
        <!-- Only required for left/right tabs -->
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab1" data-toggle="tab">Offer & Joined</a></li>
            <li><a href="#tab2" data-toggle="tab">Salary Details</a></li>
        </ul>
    </div>
    <div class="tab-content">
        <div class="tab-pane active" id="tab1">

            <script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

            <div class="TableRow">
                <div class="TableRow">
                    <div class="TabPanelHeader nomargin" style="border-style: none none none;">
                        Filter Options</div>
                </div>
                <div style="text-align: center">
                    <asp:UpdatePanel ID="upFilter" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="hdnSortColumninofferjoined" runat="server" Visible="false" Text="btnDateofofferinOfferJoined"></asp:TextBox>
                            <asp:TextBox ID="hdnSortOrderinofferjoined" runat="server" Visible="false" Text="Desc"></asp:TextBox>
                            <asp:ObjectDataSource ID="odsOfferJoinedForCommon" runat="server" SelectMethod="GetpagedOfferJoinedReportForCommon"
                                TypeName="TPS360.Web.UI.MemberHiringDetailsDataSource" SelectCountMethod="GetListCountOfferJoinedReportForCommon"
                                EnablePaging="True" SortParameterName="sortExpression">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="OfferdateinOfferJoined" Name="DateofOfferStartdate"
                                        PropertyName="StartDate" Type="DateTime" />
                                    <asp:ControlParameter ControlID="OfferdateinOfferJoined" Name="DateofOfferEnddate"
                                        PropertyName="EndDate" Type="DateTime" />
                                    <asp:ControlParameter ControlID="ActualinOfferJoined" Name="ActualDOJStartdate" PropertyName="StartDate"
                                        Type="DateTime" />
                                    <asp:ControlParameter ControlID="ActualinOfferJoined" Name="ActualDOJEnddate" PropertyName="EndDate"
                                        Type="DateTime" />
                                    <asp:ControlParameter ControlID="CreateDateinOfferJoined" Name="CreateDateStartdate"
                                        PropertyName="StartDate" Type="DateTime" />
                                    <asp:ControlParameter ControlID="CreateDateinOfferJoined" Name="CreateDateEnddate"
                                        PropertyName="EndDate" Type="DateTime" />
                                    <asp:ControlParameter ControlID="ddlCreatedByinOfferJoined" Name="CreatedBy" PropertyName="SelectedValue"
                                        Type="String" />
                                    <asp:ControlParameter ControlID="ddlRequisitioninOfferJoined" Name="Requisition"
                                        PropertyName="SelectedValue" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                            <div class="FormLeftColumn" style="width: 50%;">
                                <div class="TableRow" style="white-space: nowrap">
                                    <div class="TableFormLeble" style="width: 50%;">
                                        <asp:Label EnableViewState="false" ID="lblOfferdateinOfferJoined" runat="server"
                                            Text="Date of Offer"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <ucl:DateRangePicker ID="OfferdateinOfferJoined" runat="server" />
                                    </div>
                                </div>
                                <div class="TableRow" style="white-space: nowrap;display:none">
                                    <div class="TableFormLeble" style="width: 50%;">
                                        <asp:Label EnableViewState="false" ID="lblOfferDeclineDateinOfferJoined" runat="server"
                                            Text="Offer Decline Date"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <ucl:DateRangePicker ID="OfferDeclineDateinOfferJoined" runat="server" />
                                    </div>
                                </div>
                                <div class="TableRow" style="white-space: nowrap">
                                    <div class="TableFormLeble" style="width: 50%;">
                                        <asp:Label EnableViewState="false" ID="lblActualinOfferJoined" runat="server" Text="Actual -DOJ"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <ucl:DateRangePicker ID="ActualinOfferJoined" runat="server" />
                                    </div>
                                </div>
                                <div class="TableRow" style="white-space: nowrap;display:none">
                                    <div class="TableFormLeble" style="width: 50%;">
                                        <asp:Label EnableViewState="false" ID="lblPSinOfferJoined" runat="server" Text="PS #"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:TextBox ID="txtPSinOfferJoined" runat="server" CssClass="CommonTextBox" Width="75px"
                                            MaxLength="10" TabIndex="1"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 50%;">
                                        <asp:Label EnableViewState="false" ID="lblCreateDateinOfferJoined" runat="server"
                                            Text="Create Date"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <ucl:DateRangePicker ID="CreateDateinOfferJoined" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <div class="FormRightColumn" style="width: 49%">
                                <%--<div class="TableRow">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="lblCreateDateinOfferJoined" runat="server"
                                            Text="Create Date"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <ucl:DateRangePicker ID="CreateDateinOfferJoined" runat="server" />
                                    </div>
                                </div>--%>
                                <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="lblCreatedByinOfferJoined" runat="server"
                                            Text="Created By"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:DropDownList EnableViewState="true" ID="ddlCreatedByinOfferJoined" runat="server"
                                            CssClass="CommonDropDownList" AutoPostBack="false">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="lblRequisitoninOfferJoined" runat="server"
                                            Text="Requisition"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:DropDownList EnableViewState="true" ID="ddlRequisitioninOfferJoined" runat="server"
                                            CssClass="CommonDropDownList" AutoPostBack="false">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="TableRow" style="text-align: center">
                                <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                            </div>
                            <div class="TableRow" style="text-align: center; padding-top: 5px">
                                <asp:LinkButton ID="LinkButton1" runat="server" Text="Search" AlternateText="Search"
                                    CssClass="btn btn-primary" EnableViewState="false" OnClick="btnSearchofferjoined_Click"><i class="icon-search icon-white"></i> Search</asp:LinkButton>
                                <asp:Button ID="Button1" runat="server" Text="Clear" AlternateText="Clear" CssClass="btn"
                                    EnableViewState="false" OnClick="btnClearofferjoined_Click" />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <asp:UpdatePanel ID="upOfferJoined" runat="server">
                <ContentTemplate>
                    <div class="TableRow" style="padding-bottom: 5px" id="ExportButtonsinOfferJoined"
                        runat="server" visible="true">
                        <div class="TabPanelHeader nomargin" style="margin-bottom: 5px; border-style: none none none;">
                            Report Results</div>
                        <div>
                            <asp:ImageButton ID="btnExportToExcelOfferJoined" CssClass="btn" SkinID="sknExportToExcel"
                                runat="server" ToolTip="Export To Excel" OnClick="btnExportToExcelOfferJoined_Click" />
                            <asp:ImageButton ID="btnExportToPDFOfferJoined" CssClass="btn" SkinID="sknExportToPDF"
                                runat="server" ToolTip="Export To PDF" OnClick="btnExportToPDFOfferJoined_Click" />
                            <asp:ImageButton ID="btnExportToWordOfferJoined" CssClass="btn" SkinID="sknExportToWord"
                                runat="server" ToolTip="Export To Word" OnClick="btnExportToWordOfferJoined_Click" />
                        </div>
                    </div>
                    <asp:Panel ID="pnlGridRegion" runat="server">
                        <div id="divlsvOfferJoined" runat="server" class="GridContainer" style="overflow: auto;
                            overflow-y: hidden; width: 100%; text-align: left;" onscroll='SetScrollPosition()'>
                            <asp:ListView ID="lsvOfferJoined" runat="server" DataKeyNames="Id" DataSourceID="odsOfferJoinedForCommon"
                                OnItemDataBound="lsvOfferJoined_ItemDataBound" OnItemCommand="lsvOfferJoined_ItemCommand"
                                OnPreRender="lsvOfferJoined_PreRender" EnableViewState="true">
                                <LayoutTemplate>
                                    <table id="tlbtemplate" runat="server" class="Grid ReportGrid" cellspacing="0" border="0">
                                        <tr id="trHeader" runat="server">
                                            <th>
                                                <asp:LinkButton runat="server" ID="lnkJobCode" CommandName="Sort" CommandArgument="[J].[JobPostingCode]"
                                                    Text="Req Code #" ToolTip="Sort by Requisition Code" EnableViewState="false" />
                                            </th>
                                            <th>
                                                <asp:LinkButton runat="server" ID="btnRequisitioninOfferJoined" CommandName="Sort"
                                                    Text="Requisition" ToolTip="Sort by Requisition" EnableViewState="false" CommandArgument="[J].[JobTitle]" />
                                            </th>
                                            <th>
                                                <asp:LinkButton runat="server" ID="lnkActiveRecruiter" CommandName="Sort" CommandArgument="[MA].[FirstName]"
                                                    Text="Active Recruiter" ToolTip="Sort by ActiveRecruiter" EnableViewState="false"
                                                    Width="100%" />
                                            </th>
                                            <th>
                                                <asp:LinkButton runat="server" ID="lnkOfferStatus" CommandName="Sort" CommandArgument="[HML].[Name]"
                                                    Text="Offer Status" ToolTip="Sort by OfferStatus" EnableViewState="false" />
                                            </th>
                                            <th>
                                                <asp:LinkButton runat="server" ID="lnkCandidateID" CommandName="Sort" CommandArgument="[M].[ID]"
                                                    Text="Candidate ID #" ToolTip="Sort by Candidate ID" EnableViewState="false"
                                                    Width="100%" />
                                            </th>
                                            <th>
                                                <asp:LinkButton runat="server" ID="btnCandidateinOfferJoined" CommandName="Sort"
                                                    Text="Candidate" ToolTip="Sort by Candidate" EnableViewState="false" CommandArgument="[M].[FirstName]" />
                                            </th>
                                            <th style="display:none">
                                                <asp:LinkButton runat="server" ID="btnWorkLocation" CommandName="Sort" Text="Job Location"
                                                    ToolTip="Sort by Location" EnableViewState="false" CommandArgument="[G8].[Name]" />
                                            </th>
                                            <th>
                                                <asp:LinkButton runat="server" ID="btnBUinOfferJoined" CommandName="Sort" Text="BU"
                                                    CommandArgument="[Com].[CompanyName]" ToolTip="Sort by BU" EnableViewState="false" />
                                            </th>
                                            <th>
                                                <asp:LinkButton runat="server" ID="btnDateofofferinOfferJoined" CommandName="Sort"
                                                    CommandArgument="[MHD].[OfferedDate]" Text="Date of Offer" ToolTip="Sort by Date of Offer"
                                                    EnableViewState="false" />
                                            </th>
                                            <th style="display:none">
                                                <asp:LinkButton runat="server" ID="btnEDOJinOfferJoined" CommandName="Sort" Text="E-DOJ"
                                                    CommandArgument="[MHD].[JoiningDate]" ToolTip="Sort by E-DOJ" EnableViewState="false" />
                                            </th>
                                            <th>
                                                <asp:LinkButton runat="server" ID="btnCurrentCTCinOfferJoined" CommandName="Sort"
                                                    CommandArgument="ABS([MXI].[CurrentYearlyRate])" Text="Current CTC" ToolTip="Sort by Current CTC"
                                                    EnableViewState="false" />
                                            </th>
                                            <th>
                                                <asp:Label runat="server" ID="btnOfferedCTCinOfferJoined" CommandName="Sort" 
                                                CommandArgument="[MHD].[OfferedSalary]" Text="Offered Salary" ToolTip="Sort by Offered Salary" EnableViewState="false" />
                                            </th>
                                            <th style="display:none">
                                                <asp:LinkButton runat="server" ID="btnGradeinOfferJoined" CommandName="Sort" Text="Grade"
                                                    CommandArgument="G1.Name" ToolTip="Sort by Grade" EnableViewState="false" />
                                            </th>
                                            <th>
                                                Offered Position
                                            </th>
                                            <th>
                                                Job Title
                                            </th>
                                            <th>
                                                Supervisor
                                            </th>
                                            <th>
                                                Commission/Referral (INR) (Lacs)
                                            </th>
                                            <th>
                                                <asp:LinkButton runat="server" ID="btnExperienceinOfferJoined" CommandName="Sort"
                                                    CommandArgument="ABS([C].[TotalExperienceYears])" Text="Experience" ToolTip="Sort by Experience"
                                                    EnableViewState="false" />
                                            </th>
                                            <th>
                                                <asp:LinkButton runat="server" ID="btnSourceinOfferJoined" CommandName="Sort" Text="Source"
                                                    CommandArgument="G4.Name" ToolTip="Sort by Source" EnableViewState="false" />
                                            </th>
                                            <th>
                                                Source Description
                                            </th>
                                            <th style="display:none">
                                                <asp:LinkButton runat="server" ID="btnFitmentPercentileinOfferJoined" CommandName="Sort"
                                                    CommandArgument="G3.Name" Text="Fitment Percentile" ToolTip="Sort by FitmentPercentile"
                                                    EnableViewState="false" Width="100%" />
                                            </th>
                                            <th style="display:none">
                                                <asp:LinkButton runat="server" ID="btnJoiningBonusinOfferJoined" CommandName="Sort"
                                                    CommandArgument="ABS([MHD].[JoiningBonus])" Text="Joining Bonus" ToolTip="Sort by Joining Bonus"
                                                    EnableViewState="false" />
                                            </th>
                                            <th style="display:none">
                                                <asp:LinkButton runat="server" ID="btnRelocationAllowanceinOfferJoined" CommandName="Sort"
                                                    CommandArgument="ABS([MHD].[RelocationAllowance])" Text="Relocation Allowance" ToolTip="Sort by RelocationAllowance"
                                                    EnableViewState="false" Width="100%" />
                                            </th>
                                            <th style="display:none">
                                                <asp:LinkButton runat="server" ID="btnSpecialLumpSuminOfferJoined" CommandName="Sort"
                                                    CommandArgument="ABS([MHD].[SpecialLumpSum])" Text="Special Lump Sum" ToolTip="Sort by Special Lump Sum"
                                                    EnableViewState="false" Width="100%" />
                                            </th>
                                            <th style="display:none">
                                                <asp:LinkButton runat="server" ID="btnNoticePeriodPayoutinOfferJoined" CommandName="Sort"
                                                    CommandArgument="ABS([MHD].[NoticePeriodPayout])" Text="Notice Period Payout" ToolTip="Sort by Notice Period Payout"
                                                    EnableViewState="false" Width="100%" />
                                            </th>
                                            <th style="display:none">
                                                <asp:LinkButton runat="server" ID="btnPlacementsFeesinOfferJoined" CommandName="Sort"
                                                    CommandArgument="ABS([MHD].[PlacementFees])" Text="Placements Fees" ToolTip="Sort by Placements Fees"
                                                    EnableViewState="false" Width="100%" />
                                            </th>
                                            <th style="display:none">
                                                <asp:LinkButton runat="server" ID="btnBVStatusinOfferJoined" CommandName="Sort" Text="BV Status"
                                                    CommandArgument="G2.Name" ToolTip="Sort by BV Status" EnableViewState="false" />
                                            </th>
                                            <th style="display:none">
                                                <asp:LinkButton runat="server" ID="btnOfferDeclineDateinOfferJoined" CommandName="Sort"
                                                    CommandArgument="[MOR].[RejectedDate]" Text="Offer Decline Date" ToolTip="Sort by Offer Decline Date"
                                                    EnableViewState="false" Width="100%" />
                                            </th>
                                            <th style="display:none">
                                                <asp:LinkButton runat="server" ID="btnReasonforDeclineinOfferJoined" CommandName="Sort"
                                                    CommandArgument="G6.Name" Text="Reason for Decline" ToolTip="Sort by Reason for Decline"
                                                    EnableViewState="false" Width="100%" />
                                            </th>
                                            <th>
                                                <asp:LinkButton runat="server" ID="btnActualDOJinOfferJoined" CommandName="Sort"
                                                    CommandArgument="[MJD].[JoiningDate]" Text="Actual�DOJ" ToolTip="Sort by Actual�DOJ"
                                                    EnableViewState="false" />
                                            </th>
                                            <th style="display:none">
                                                <asp:LinkButton runat="server" ID="btnPSDOJinOfferJoined" CommandName="Sort" Text="PS#"
                                                    CommandArgument="ABS([MJD].[PSID])" ToolTip="Sort by PS#" EnableViewState="false" />
                                            </th>
                                            <th>
                                                <asp:LinkButton runat="server" ID="btnCurrentCompanyinOfferJoined" CommandName="Sort"
                                                    CommandArgument="[MEI].[LastEmployer]" Text="Current Company" ToolTip="Sort by Current Company"
                                                    EnableViewState="false" Width="100%" />
                                            </th>
                                            <%--<th>
                                        <asp:LinkButton runat="server" ID="btnWorkLocationinOfferJoined" CommandName="Sort" CommandArgument ="G7.Name"
                                            Text="Work Location" ToolTip="Sort by Work Location" EnableViewState="false" />
                                    </th>--%>
                                            <th style="width: 100px !important">
                                                Skills
                                            </th>
                                            <th style="width: 100px !important;display:block;">
                                        
                                               <asp:label id="lblEducationQualification" runat="server" style="" Text="Education/Qualification" ToolTip="Education/Qualification"></asp:label>
                                            </th>
                                            <th style="width: 100px !important">
                                                Basic
                                            </th>
                                            <th style="width: 100px !important">
                                                HRA
                                            </th>
                                            <th style="width: 100px !important">
                                                Conveyance
                                            </th>
                                            <th style="width: 100px !important">
                                                Special Allowance
                                            </th>
                                            <th style="width: 100px !important">
                                                Medical Allowance
                                            </th>
                                            <th style="width: 100px !important;display:block;">
                                            <asp:label id="lblEducationalAllowance" runat="server" style="" Text="Group Insurance" ToolTip="Educational Allowance"></asp:label>
                                                
                                            </th>
                                            <th style="width: 100px !important">
                                               Professional Tax
                                            </th>
                                            <th style="width: 100px !important">
                                                Performance Linked Incentive
                                            </th>
                                            <th style="width: 100px !important">
                                                E.A (Fixed)
                                            </th>
                                            <th style="width: 100px !important" visible="false">
                                                Relocation Allowance
                                            </th>
                                            <th style="width: 100px !important" visible="false">
                                                Reimbursement
                                            </th>
                                            <th style="width: 100px !important">
                                                PF
                                            </th>
                                            <th style="width: 100px !important">
                                                ESIC
                                            </th>
                                            <th style="width: 100px !important">
                                                Gratuity
                                            </th>
                                            <th style="width: 100px !important;display:block;">
                                                Bonus
                                            </th>
                                            <th style="width: 100px !important">
                                                Sales Incentive
                                            </th>
                                            <th style="width: 100px !important">
                                                Grand Total
                                            </th>
                                            <th style="width: 100px !important">
                                                Billable Salary (INR) (In Lacs)
                                            </th>
                                            <th style="width: 100px !important">
                                                Billing Rate (%)
                                            </th>
                                            <th style="width: 100px !important">
                                                Revenue (INR) (In Lacs)
                                            </th>
                                            <th style="width: 100px !important">
                                                Location
                                            </th>
                                        </tr>
                                        <tr id="itemPlaceholder" runat="server">
                                        </tr>
                                        <tr class="Pager">
                                            <td id="tdpager" runat="server" colspan="42">
                                                <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" NoOfRowsCookie="OfferJoinedReportReportRowPerPage" />
                                            </td>
                                        </tr>
                                    </table>
                                </LayoutTemplate>
                                <EmptyDataTemplate>
                                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                        <tr>
                                            <td>
                                                No data was returned.
                                            </td>
                                        </tr>
                                    </table>
                                </EmptyDataTemplate>
                                <ItemTemplate>
                                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                        <td>
                                            <asp:Label ID="lblJobPostingCode" runat="server"></asp:Label>
                                        </td>
                                        <td runat="server" id="tdRequisitioninOfferJoined">
                                            <asp:HyperLink runat="server" ID="lblRequisitioninOfferJoined" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblActiveRecruiter" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblOfferStatus" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCandidateID" runat="server"></asp:Label>
                                        </td>
                                        <td runat="server" id="tdCandidateinOfferJoined">
                                            <asp:HyperLink runat="server" ID="lblCandidateinOfferJoined" />
                                        </td>
                                        <td style="display:none">
                                            <asp:Label ID="lblWorkLocation" runat="server"></asp:Label>
                                        </td>
                                        <td runat="server" id="tdBUinOfferJoined">
                                            <asp:Label runat="server" ID="lblBUinOfferJoined" />
                                        </td>
                                        <td runat="server" id="tdDateofOfferinOfferJoined">
                                            <asp:Label runat="server" ID="lblDateofOfferinOfferJoined" />
                                        </td>
                                        <td runat="server" id="tdEDOJinOfferJoined" style="display:none">
                                            <asp:Label runat="server" ID="lblEDOJinOfferJoined" />
                                        </td>
                                        <td runat="server" id="tdCurrentCTCinOfferJoined">
                                            <asp:Label runat="server" ID="lblCurrentCTCinOfferJoined" />
                                        </td>
                                        <td runat="server" id="tdOfferedCTCinOfferJoined">
                                            <asp:Label runat="server" ID="lblOfferedCTCinOfferJoined" />
                                        </td>
                                        <td runat="server" id="tdGradeinOfferJoined" style="display:none">
                                            <asp:Label runat="server" ID="lblGradeinOfferJoined" />
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblOfferedPosition" />
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblJobTitle" />
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblSupervisor" />
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblOfferedCommision" />
                                        </td>
                                        <td runat="server" id="tdExperienceinOfferJoined">
                                            <asp:Label runat="server" ID="lblExperienceinOfferJoined" />
                                        </td>
                                        <td runat="server" id="tdSourceinOfferJoined">
                                            <asp:Label runat="server" ID="lblSourceinOfferJoined" />
                                        </td>
                                        <td runat="server" id="tdSourceDescriptionBUinOfferJoined">
                                            <asp:Label runat="server" ID="lblSourceDescriptioninOfferJoined" />
                                        </td>
                                        <td runat="server" id="tdFitmentPercentileinOfferJoined" style="display:none">
                                            <asp:Label runat="server" ID="lblFitmentPercentileinOfferJoined" />
                                        </td>
                                        <td runat="server" id="tdJoiningBonusinOfferJoined" style="display:none">
                                            <asp:Label runat="server" ID="lblJoiningBonusinOfferJoined" />
                                        </td>
                                        <td runat="server" id="tdRelocationAllowanceinOfferJoined" style="display:none">
                                            <asp:Label runat="server" ID="lblRelocationAllowanceinOfferJoined" />
                                        </td>
                                        <td runat="server" id="tdSpecialLumpSuminOfferJoined" style="display:none">
                                            <asp:Label runat="server" ID="lblSpecialLumpSuminOfferJoined" />
                                        </td>
                                        <td runat="server" id="tdNoticePeriodPayoutinOfferJoined" style="display:none">
                                            <asp:Label runat="server" ID="lblNoticePeriodPayoutinOfferJoined" />
                                        </td>
                                        <td runat="server" id="tdPlacementsFeesinOfferJoined" style="display:none">
                                            <asp:Label runat="server" ID="lblPlacementsFeesinOfferJoined" />
                                        </td>
                                        <td runat="server" id="tdBVStatusinOfferJoined" style="display:none">
                                            <asp:Label runat="server" ID="lblBVStatusinOfferJoined" />
                                        </td>
                                        <td runat="server" id="tdOfferDeclineDateinOfferJoined" style="display:none">
                                            <asp:Label runat="server" ID="lblOfferDeclineDateinOfferJoined" />
                                        </td>
                                        <td runat="server" id="tdReasonforDeclineinOfferJoined" style="display:none">
                                            <asp:Label runat="server" ID="lblReasonforDeclineinOfferJoined" />
                                        </td>
                                        <td runat="server" id="tdActualDOJinOfferJoined">
                                            <asp:Label runat="server" ID="lblActualDOJinOfferJoined" />
                                        </td>
                                        <td runat="server" style="display:none" id="tdPSinOfferJoined">
                                            <asp:Label runat="server" ID="lblPSinOfferJoined" />
                                        </td>
                                        <td runat="server" id="tdCurrentCompanyinOfferJoined">
                                            <asp:Label runat="server" ID="lblCurrentCompanyinOfferJoined" />
                                        </td>
                                        <%-- <td runat="server" id="tdWorkLocationinOfferJoined">
                                    <asp:Label runat="server" ID="lblWorkLocationinOfferJoined" />
                                </td>--%>
                                        <td style="width: 100px !important; white-space: normal;">
                                            <asp:Label runat="server" ID="lblSkillsinOfferJoined" />
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblEducationQualificationinOfferJoined" />
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblBasicofferJoined" />
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblHRAofferJoined" />
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblConveyenceOfferJoined" />
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblSpecialOfferJoined" />
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblMedicalOfferJoined" />
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblRoleAllowance" />
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblSiteOfferJoined" />
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblPerformanceOfferJoined" />
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblRetentionOfferJoined" />
                                        </td>
                                       <td id = "lblRelocationOfferJoineddisable" visible ="false">
                                            <asp:Label runat="server" ID="lblRelocationOfferJoined" />
                                        </td>
                                        <td id ="lblReimbursementOfferJoineddisable" visible ="false">
                                            <asp:Label runat="server" ID="lblReimbursementOfferJoined" />
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblPFOfferJoined" />
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblESICOfferJoined" />
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblGratuityOfferJoined" />
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblBonusOfferJoined" />
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblSalesIncentiveOfferJoined" />
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblGrandTotalOfferJoined" />
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblBillableSalary" />
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblBillingRate" />
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblRevenue" />
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblOfferLocation" />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:ListView>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnExportToExcelOfferJoined" />
                    <asp:PostBackTrigger ControlID="btnExportToPDFOfferJoined" />
                    <asp:PostBackTrigger ControlID="btnExportToWordOfferJoined" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <div style="display:none" class="tab-pane " id="tab2">

            <script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

            <asp:UpdatePanel ID="up" runat="server">
                <ContentTemplate>
                    <div class="TableRow">
                        <div class="TableRow">
                            <div class="TabPanelHeader nomargin" style="border-style: none none none;">
                                Filter Options</div>
                        </div>
                        <div class="FormLeftColumn" style="width: 50%;">
                            <div class="TableRow" style="white-space: nowrap">
                                <div class="TableFormLeble" style="width: 50%;">
                                    <asp:Label EnableViewState="false" ID="lblOfferdateinSalarydetails" runat="server"
                                        Text="Date of Offer"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <ucl:DateRangePicker ID="OfferdateinSalarydetails" runat="server" />
                                </div>
                            </div>
                            <div class="TableRow" style="white-space: nowrap;">
                                <div class="TableFormLeble" style="width: 50%;">
                                    <asp:Label EnableViewState="false" ID="lblActualinSalarydetails" runat="server" Text="E-DOJ"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <ucl:DateRangePicker ID="ActualinSalarydetails" runat="server" />
                                </div>
                            </div>
                            <div class="TableRow" style="white-space: nowrap; display: none;">
                                <div class="TableFormLeble" style="width: 50%;">
                                    <asp:Label EnableViewState="false" ID="lblPSinSalarydetails" runat="server" Text="PS #"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:TextBox ID="txtPSinSalarydetails" runat="server" CssClass="CommonTextBox" Width="75px"
                                        MaxLength="10" TabIndex="1"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="FormRightColumn" style="width: 49%">
                            <div class="TableRow">
                                <div class="TableFormLeble" style="width: 40%;">
                                    <asp:Label EnableViewState="false" ID="lblCreatedate" runat="server" Text="Create Date"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <ucl:DateRangePicker ID="CreatedateinSalarydetails" runat="server" />
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble" style="width: 40%;">
                                    <asp:Label EnableViewState="false" ID="lblCreatedByinSalarydetails" runat="server"
                                        Text="Created By"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:DropDownList EnableViewState="true" ID="ddlCreatedbyinSalarydetails" runat="server"
                                        CssClass="CommonDropDownList" AutoPostBack="false">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble" style="width: 40%;">
                                    <asp:Label EnableViewState="false" ID="lblRequisitioninSalarydetails" runat="server"
                                        Text="Requisition"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:DropDownList EnableViewState="true" ID="ddlRequisitioninSalarydetails" runat="server"
                                        CssClass="CommonDropDownList" AutoPostBack="false">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="TableRow" style="text-align: center">
                        <asp:Label ID="Label8" runat="server" EnableViewState="false"></asp:Label>
                    </div>
                    <div class="TableRow" style="text-align: center; padding-top: 5px">
                        <asp:LinkButton ID="btnSearch" runat="server" Text="Search" AlternateText="Search"
                            CssClass="btn btn-primary" EnableViewState="false" OnClick="btnSearchsalarydetail_Click"><i class="icon-search icon-white"></i> Search</asp:LinkButton>
                        <asp:Button ID="btnClear" runat="server" Text="Clear" AlternateText="Clear" CssClass="btn"
                            EnableViewState="false" CausesValidation="false" OnClick="btnClearsalarydetail_Click" />
                    </div>
                    <asp:TextBox ID="hdnSortColumninembedded" runat="server" Visible="false" Text="btnCreateDateinEmbeddedSalarydetails"></asp:TextBox>
                    <asp:TextBox ID="hdnSortOrderinembedded" runat="server" Visible="false" Text="Desc"></asp:TextBox>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div style="overflow: inherit" id="bigDiv" onscroll='SetScrollPosition()'>
                <div class="TabPanelHeader nomargin" style="margin-bottom: 5px; border-style: none none none;">
                    Report Results</div>
                <div class="TabPanelHeader">
                    Embedded Salary Details
                </div>
                <div class="TableRow" style="padding-bottom: 5px" id="EmbeddedExportButton" runat="server"
                    visible="true">
                    <asp:ImageButton ID="btnExportToExcelinEmbeddedSalarydetails" CssClass="btn" SkinID="sknExportToExcel"
                        runat="server" ToolTip="Export To Excel" OnClick="btnExportToExcelEmbeddedSalarydetails_Click" />
                    <asp:ImageButton ID="btnExportToPDFEmbeddedSalarydetails" CssClass="btn" SkinID="sknExportToPDF"
                        runat="server" ToolTip="Export To PDF" OnClick="btnExportToPDFEmbeddedSalarydetails_Click" />
                    <asp:ImageButton ID="btnExportToWordEmbeddedSalarydetails" CssClass="btn" SkinID="sknExportToWord"
                        runat="server" ToolTip="Export To Word" OnClick="btnExportToWordEmbeddedSalarydetails_Click" />
                </div>
                <asp:ObjectDataSource ID="odsEmbeddedSalarydetails" runat="server" SelectMethod="GetpagedEmbeddedSalarydetailsReport"
                    TypeName="TPS360.Web.UI.MemberHiringDetailsDataSource" SelectCountMethod="GetListCountEmbeddedSalarydetailsReport"
                    EnablePaging="True" SortParameterName="sortExpression">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="OfferdateinSalarydetails" Name="DateofOfferStartdate"
                            PropertyName="Startdate" Type="String" />
                        <asp:ControlParameter ControlID="OfferdateinSalarydetails" Name="DateofOfferEnddate"
                            PropertyName="Enddate" Type="String" />
                        <asp:ControlParameter ControlID="ActualinSalarydetails" Name="ActualDOJStartdate"
                            PropertyName="Startdate" Type="String" />
                        <asp:ControlParameter ControlID="ActualinSalarydetails" Name="ActualDOJEnddate" PropertyName="Enddate"
                            Type="String" />
                        <asp:ControlParameter ControlID="txtPSinSalarydetails" Name="PS" PropertyName="Text" />
                        <asp:ControlParameter ControlID="CreatedateinSalarydetails" Name="CreateDateStartdate"
                            PropertyName="Startdate" Type="String" />
                        <asp:ControlParameter ControlID="CreatedateinSalarydetails" Name="CreateDateEnddate"
                            PropertyName="Enddate" Type="String" />
                        <asp:ControlParameter ControlID="ddlCreatedbyinSalarydetails" Name="CreatedBy" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="ddlRequisitioninSalarydetails" Name="Requisition"
                            PropertyName="SelectedValue" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:Panel ID="pnlGridRegioninEmbeddedSalarydetails" runat="server">
                    <div id="divlsvJobPostinginEmbeddedSalarydetails" runat="server" class="GridContainer"
                        style="overflow: auto; overflow-y: hidden; width: 100%; text-align: left;" onscroll='SetScrollPosition()'>
                        <asp:UpdatePanel ID="upEmbedded" runat="server">
                            <ContentTemplate>
                                <asp:ListView ID="lsvEmbeddedSalarydetails" runat="server" DataKeyNames="Id" DataSourceID="odsEmbeddedSalarydetails"
                                    OnItemDataBound="lsvEmbeddedSalarydetails_ItemDataBound" OnItemCommand="lsvEmbeddedSalarydetails_ItemCommand"
                                    OnPreRender="lsvEmbeddedSalarydetails_PreRender" EnableViewState="true">
                                    <LayoutTemplate>
                                        <table id="tlbtemplate" runat="server" class="Grid ReportGrid" cellspacing="0" border="0">
                                            <tr id="trHeader" runat="server">
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnCreateDateinEmbeddedSalarydetails" CommandName="Sort"
                                                        CommandArgument="[MHD].[CreateDate]" Text="Create Date" ToolTip="Sort by Create Date"
                                                        EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="lnkJobCode" CommandName="Sort" CommandArgument="[J].[JobPostingCode]"
                                                        Text="Req Code #" ToolTip="Sort by Requisition Code" EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnRequisitioninEmbeddedSalarydetails" CommandName="Sort"
                                                        CommandArgument="[J].[JobTitle]" Text="Requisition" ToolTip="Sort by Requisition"
                                                        EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="lnkActiveRecruiter" CommandName="Sort" CommandArgument="[MA].[FirstName]"
                                                        Text="Active Recruiter" ToolTip="Sort by ActiveRecruiter" EnableViewState="false"
                                                        Width="100%" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="lnkOfferStatus" CommandName="Sort" CommandArgument="[HML].[Name]"
                                                        Text="Offer Status" ToolTip="Sort by OfferStatus" EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="lnkCandidateID" CommandName="Sort" CommandArgument="[M].[ID]"
                                                        Text="Candidate ID #" ToolTip="Sort by Candidate ID" EnableViewState="false"
                                                        Width="100%" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnCandidateinEmbeddedSalarydetails" CommandName="Sort"
                                                        CommandArgument="[M].[FirstName]" Text="Candidate" ToolTip="Sort by Candidate"
                                                        EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnOfferCategoryinEmbeddedSalarydetails" CommandName="Sort"
                                                        CommandArgument="[MHD].[offeredcategory]" Text="Offer Category" ToolTip="Sort by Offer Category"
                                                        EnableViewState="false" Width="100%" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnOfferedGradeinEmbeddedSalarydetails" CommandName="Sort"
                                                        CommandArgument="[g].name" Text="Offered Grade" ToolTip="Sort by Offered Grade"
                                                        EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnBasicinEmbeddedSalarydetails" CommandName="Sort"
                                                        CommandArgument="[MHD].[BasicEmbedded]" Text="Basic" ToolTip="Sort by Basic"
                                                        EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnHRA" CommandName="Sort" CommandArgument="[MHD].[HRAEmbedded]"
                                                        Text="HRA" ToolTip="Sort by HRA" EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnConveyence" CommandName="Sort" CommandArgument="[MHD].[Conveyance]"
                                                        Text="Conveyence" ToolTip="Sort by Conveyence" EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnMedical" CommandName="Sort" CommandArgument="[MHD].[Medical]"
                                                        Text="Medical" ToolTip="Sort by Medical" EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnEducationalAllowance" CommandName="Sort" CommandArgument="[MHD].[EducationAllowanceEmbedded]"
                                                        Text="Educational Allowance" ToolTip="Sort by Educational Allowance" EnableViewState="false"
                                                        Width="100%" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnLUFS" CommandName="Sort" CommandArgument="[MHD].[LUFS]"
                                                        Text="LUFS" ToolTip="Sort by LUFS" EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnAdhoc" CommandName="Sort" CommandArgument="[MHD].[AdHoc]"
                                                        Text="Ad-hoc" ToolTip="Sort by Ad-hoc" EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnMPP" CommandName="Sort" CommandArgument="[MHD].[MPP]"
                                                        Text="MPP" ToolTip="Sort by MPP" EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnAGVI" CommandName="Sort" CommandArgument="[MHD].[AGVI]"
                                                        Text="AGVI" ToolTip="Sort by AGVI" EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnLTA" CommandName="Sort" CommandArgument="[MHD].[LTAEmbedded]"
                                                        Text="LTA" ToolTip="Sort by LTA" EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnPF" CommandName="Sort" CommandArgument="[MHD].[PFEmbedded]"
                                                        Text="PF" ToolTip="Sort by PF" EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnGratuity" CommandName="Sort" CommandArgument="[MHD].[GratuityEmbedded]"
                                                        Text="Gratuity" ToolTip="Sort by Gratuity" EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnMaximunAnnualIncentive" CommandName="Sort"
                                                        CommandArgument="[MHD].[MaximumAnnualIncentives]" Text="Maximun Annual Incentive"
                                                        ToolTip="Sort by Maximun Annual Incentive" EnableViewState="false" Width="100%" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnEmbeddedAllowance" CommandName="Sort" CommandArgument="[MHD].[EmbeddedAllowance]"
                                                        Text="Embedded Allowance" ToolTip="Sort by Embedded Allowance" EnableViewState="false"
                                                        Width="100%" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnPLMAllowance" CommandName="Sort" CommandArgument="[MHD].[PLMAllowance]"
                                                        Text="PLM Allowance" ToolTip="Sort by PLM Allowance" EnableViewState="false"
                                                        Width="100%" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnSalesIncentive" CommandName="Sort" CommandArgument="[MHD].[SalesIncentive]"
                                                        Text="Sales Incentive" ToolTip="Sort by Sales Incentive" EnableViewState="false"
                                                        Width="100%" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnCarAllowance" CommandName="Sort" CommandArgument="[MHD].[CarAllowance]"
                                                        Text="Car Allowance" ToolTip="Sort by Car Allowance" EnableViewState="false"
                                                        Width="100%" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnGrandTotal" CommandName="Sort" CommandArgument="[MHD].[GrandTotal]"
                                                        Text="Grand Total" ToolTip="Sort by Grand Total" EnableViewState="false" />
                                                </th>
                                            </tr>
                                            <tr id="itemPlaceholder" runat="server">
                                            </tr>
                                            <tr class="Pager">
                                                <td id="tdpager" runat="server" colspan="27">
                                                    <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" NoOfRowsCookie="EmbeddedReportReportRowPerPage" />
                                                </td>
                                            </tr>
                                        </table>
                                    </LayoutTemplate>
                                    <EmptyDataTemplate>
                                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                            <tr>
                                                <td>
                                                    No data was returned.
                                                </td>
                                            </tr>
                                        </table>
                                    </EmptyDataTemplate>
                                    <ItemTemplate>
                                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                            <td>
                                                <asp:Label runat="server" ID="lblCreatedateinEmbeddedSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblJobPostingCode" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:HyperLink runat="server" ID="lblRequisitioninEmbeddedSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblActiveRecruiter" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblOfferStatus" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblCandidateID" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:HyperLink runat="server" ID="lblCandidateinEmbeddedSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblOfferCategoryinEmbeddedSalryDetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblOfferedgradeinEmbeddedSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblBasicinEmbeddedSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblHRAinEmbeddedSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblConveyenceinEmbeddedSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblMedicalinEmbeddedSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblEducationalAllowanceinEmbeddedSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblLUFSinEmbeddedSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblAdhocinEmbeddedSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblMPPinEmbeddedSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblAGVIinEmbeddedSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblLTAinEmbeddedSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblPFinEmbeddedSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblGratuityinEmbeddedSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblMaximunAnnualIncentiveinEmbeddedSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblEmbeddedAllowanceinEmbeddedSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblPLMAllowanceinEmbeddedSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblSalesIncentiveinEmbeddedSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblCarAllowanceinEmbeddedSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblGrandTotalinEmbeddedSalarydetails" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnExportToExcelinEmbeddedSalarydetails" />
                                <asp:PostBackTrigger ControlID="btnExportToPDFEmbeddedSalarydetails" />
                                <asp:PostBackTrigger ControlID="btnExportToWordEmbeddedSalarydetails" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </asp:Panel>
                <div class="TabPanelHeader">
                    Mech Salary Details
                </div>
                <div class="TableRow" style="padding-bottom: 5px" id="MechExportButton" runat="server"
                    visible="true">
                    <asp:ImageButton ID="btnExportToExcelMechSalarydetails" CssClass="btn" SkinID="sknExportToExcel"
                        runat="server" ToolTip="Export To Excel" OnClick="btnExportToExcelMechSalarydetails_Click" />
                    <asp:ImageButton ID="btnExportToPDFMechSalarydetails" CssClass="btn" SkinID="sknExportToPDF"
                        runat="server" ToolTip="Export To PDF" OnClick="btnExportToPDFMechSalarydetails_Click" />
                    <asp:ImageButton ID="btnExportToWordMechSalarydetails" CssClass="btn" SkinID="sknExportToWord"
                        runat="server" ToolTip="Export To Word" OnClick="btnExportToWordMechSalarydetails_Click" />
                </div>
                <asp:TextBox ID="hdnSortColumninmech" runat="server" Visible="false" Text="btnCreateDateinMechSalarydetails"></asp:TextBox>
                <asp:TextBox ID="hdnSortOrderinmech" runat="server" Visible="false" Text="Desc"></asp:TextBox>
                <asp:ObjectDataSource ID="odsMechSalarydetails" runat="server" SelectMethod="GetpagedMechSalarydetailsReport"
                    TypeName="TPS360.Web.UI.MemberHiringDetailsDataSource" SelectCountMethod="GetListCountMechSalarydetailsReport"
                    EnablePaging="True" SortParameterName="sortExpression">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="OfferdateinSalarydetails" Name="DateofOfferStartdate"
                            PropertyName="Startdate" Type="String" />
                        <asp:ControlParameter ControlID="OfferdateinSalarydetails" Name="DateofOfferEnddate"
                            PropertyName="Enddate" Type="String" />
                        <asp:ControlParameter ControlID="ActualinSalarydetails" Name="ActualDOJStartdate"
                            PropertyName="Startdate" Type="String" />
                        <asp:ControlParameter ControlID="ActualinSalarydetails" Name="ActualDOJEnddate" PropertyName="Enddate"
                            Type="String" />
                        <asp:ControlParameter ControlID="txtPSinSalarydetails" Name="PS" PropertyName="Text" />
                        <asp:ControlParameter ControlID="CreatedateinSalarydetails" Name="CreateDateStartdate"
                            PropertyName="Startdate" Type="String" />
                        <asp:ControlParameter ControlID="CreatedateinSalarydetails" Name="CreateDateEnddate"
                            PropertyName="Enddate" Type="String" />
                        <asp:ControlParameter ControlID="ddlCreatedbyinSalarydetails" Name="CreatedBy" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="ddlRequisitioninSalarydetails" Name="Requisition"
                            PropertyName="SelectedValue" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:Panel ID="Panel1" runat="server">
                    <div id="div1" runat="server" class="GridContainer" style="overflow: auto; overflow-y: hidden;
                        width: 100%; text-align: left;" onscroll='SetScrollPosition()'>
                        <asp:UpdatePanel ID="upMech" runat="server">
                            <ContentTemplate>
                                <asp:ListView ID="lsvMechSalarydetails" runat="server" DataKeyNames="Id" DataSourceID="odsMechSalarydetails"
                                    OnItemDataBound="lsvMechSalarydetails_ItemDataBound" OnItemCommand="lsvMechSalarydetails_ItemCommand"
                                    OnPreRender="lsvMechSalarydetails_PreRender" EnableViewState="true">
                                    <LayoutTemplate>
                                        <table id="tlbtemplate" runat="server" class="Grid ReportGrid" cellspacing="0" border="0">
                                            <tr id="trHeader" runat="server">
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnCreateDateinMechSalarydetails" CommandName="Sort"
                                                        CommandArgument="[MHD].[CreateDate]" Text="Create Date" ToolTip="Sort by Create Date"
                                                        EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="lnkJobCode" CommandName="Sort" CommandArgument="[J].[JobPostingCode]"
                                                        Text="Req Code #" ToolTip="Sort by Requisition Code" EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnRequisitioninMechSalarydetails" CommandName="Sort"
                                                        CommandArgument="[J].[JobTitle]" Text="Requisition" ToolTip="Sort by Requisition"
                                                        EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="lnkActiveRecruiter" CommandName="Sort" CommandArgument="[MA].[FirstName]"
                                                        Text="Active Recruiter" ToolTip="Sort by ActiveRecruiter" EnableViewState="false"
                                                        Width="100%" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="lnkOfferStatus" CommandName="Sort" CommandArgument="[HML].[Name]"
                                                        Text="Offer Status" ToolTip="Sort by OfferStatus" EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="lnkCandidateID" CommandName="Sort" CommandArgument="[M].[ID]"
                                                        Text="Candidate ID #" ToolTip="Sort by Candidate ID" EnableViewState="false"
                                                        Width="100%" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnCandidateinMechSalarydetails" CommandName="Sort"
                                                        CommandArgument="[M].[FirstName]" Text="Candidate" ToolTip="Sort by Candidate"
                                                        EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnOfferCategoryinMechSalarydetails" CommandName="Sort"
                                                        CommandArgument="[MHD].[offeredcategory]" Text="Offer Category" ToolTip="Sort by Offer Category"
                                                        EnableViewState="false" Width="1005"/>
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnOfferedGrade" CommandName="Sort" CommandArgument="[g].name"
                                                        Text="Offered Grade" ToolTip="Sort by Offered Grade" EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnBasic" CommandName="Sort" CommandArgument="[MHD].[BasicMech]"
                                                        Text="Basic" ToolTip="Sort by Basic" EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnFlexiPay1" CommandName="Sort" CommandArgument="[MHD].[FlexiPay1]"
                                                        Text="Flexi Pay 1" ToolTip="Sort by Flexi Pay 1" EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnFlexiPay2" CommandName="Sort" CommandArgument="[MHD].[FlexiPay2]"
                                                        Text="Flexi Pay 2" ToolTip="Sort by Flexi Pay 2" EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnAdditionalAllowance" CommandName="Sort" CommandArgument="[MHD].[AdditionalAllowance]"
                                                        Text="Additional Allowance" ToolTip="Sort by Additional Allowance" EnableViewState="false"
                                                        Width="100%" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnAdHocAllowance" CommandName="Sort" CommandArgument="[MHD].[AdHocAllowance]"
                                                        Text="Ad Hoc Allowance" ToolTip="Sort by Ad Hoc Allowance" EnableViewState="false"
                                                        Width="100%" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnLocationAllowance" CommandName="Sort" CommandArgument="[MHD].[LocationAllowance]"
                                                        Text="Location Allowance" ToolTip="Sort by Location Allowance" EnableViewState="false"
                                                        Width="100%" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnSAFAllowance" CommandName="Sort" CommandArgument="[MHD].[SAFAllowance]"
                                                        Text="SAF Allowance" ToolTip="Sort by SAF Allowance" EnableViewState="false"
                                                        Width="100%" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnHRA" CommandName="Sort" CommandArgument="[MHD].[HRAMech]"
                                                        Text="HRA" ToolTip="Sort by HRA" EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnHLISA" CommandName="Sort" CommandArgument="[MHD].[HLISA]"
                                                        Text="H-LISA" ToolTip="Sort by H-LISA" EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnEducationAllowance" CommandName="Sort" CommandArgument="[MHD].[EducationAllowanceMech]"
                                                        Text="Education Allowance" ToolTip="Sort by Education Allowance" EnableViewState="false"
                                                        Width="100%" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="LinkButton6" CommandName="Sort" CommandArgument="[MHD].[ACLRA]"
                                                        Text="ACLRA" ToolTip="Sort by ACLRA" EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnCLRA" CommandName="Sort" CommandArgument="[MHD].[CLRA]"
                                                        Text="CLRA" ToolTip="Sort by CLRA" EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnSpecialAllowance" CommandName="Sort" CommandArgument="[MHD].[SpecialAllowance]"
                                                        Text="Special Allowance" ToolTip="Sort by Special Allowance" EnableViewState="false"
                                                        Width="100%" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnSpecialPerformancePay" CommandName="Sort" CommandArgument="[MHD].[SpecialPerformancePay]"
                                                        Text="Special Performance Pay" ToolTip="Sort by Special Performance Pay" EnableViewState="false"
                                                        Width="100%" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnECAL" CommandName="Sort" CommandArgument="[MHD].[ECAL]"
                                                        Text="ECAL" ToolTip="Sort by ECAL" EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnCarMileageReimbursement" CommandName="Sort"
                                                        CommandArgument="[MHD].[CarMileageReimbursement]" Text="Car Mileage Reimbursement"
                                                        ToolTip="Sort by Car Mileage Reimbursement" EnableViewState="false" Width="100%" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnTelephoneReimbursement" CommandName="Sort"
                                                        CommandArgument="[MHD].[TelephoneReimbursement]" Text="Telephone Reimbursement"
                                                        ToolTip="Sort by Telephone Reimbursement" EnableViewState="false" Width="100%" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnLTA" CommandName="Sort" CommandArgument="[MHD].[LTAMech]"
                                                        Text="LTA" ToolTip="Sort by LTA" EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnPF" CommandName="Sort" CommandArgument="[MHD].[PFMech]"
                                                        Text="PF" ToolTip="Sort by PF" EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnGratuity" CommandName="Sort" CommandArgument="[MHD].[GratuityMech]"
                                                        Text="Gratuity" ToolTip="Sort by Gratuity" EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnMedicalreimbursements" CommandName="Sort" CommandArgument="[MHD].[MedicalReimbursement]"
                                                        Text="Medical reimbursements" ToolTip="Sort by Medical reimbursements" EnableViewState="false"
                                                        Width="100%" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnPCScheme" CommandName="Sort" CommandArgument="[MHD].[PCScheme]"
                                                        Text="PC Scheme" ToolTip="Sort by PC Scheme" EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnRetentionPay" CommandName="Sort" CommandArgument="[MHD].[RetentionPay]"
                                                        Text="Retention Pay" ToolTip="Sort by Retention Pay" EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnPLR" CommandName="Sort" CommandArgument="[MHD].[PLR]"
                                                        Text="PLR" ToolTip="Sort by PLR" EnableViewState="false" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnSalesIncentiveMech" CommandName="Sort" CommandArgument="[MHD].[SalesIncentive]"
                                                        Text="Sales Incentive" ToolTip="Sort by Sales Incentive" EnableViewState="false"
                                                        Width="100%" />
                                                </th>
                                                <th>
                                                    <asp:LinkButton runat="server" ID="btnTotalCTC" CommandName="Sort" CommandArgument="[MHD].[TotalCTC]"
                                                        Text="Total CTC" ToolTip="Sort by Total CTC" EnableViewState="false" />
                                                </th>
                                            </tr>
                                            <tr id="itemPlaceholder" runat="server">
                                            </tr>
                                            <tr class="Pager">
                                                <td id="tdpager" runat="server" colspan="35">
                                                    <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" NoOfRowsCookie="MechReportReportRowPerPage" />
                                                </td>
                                            </tr>
                                        </table>
                                    </LayoutTemplate>
                                    <EmptyDataTemplate>
                                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                            <tr>
                                                <td>
                                                    No data was returned.
                                                </td>
                                            </tr>
                                        </table>
                                    </EmptyDataTemplate>
                                    <ItemTemplate>
                                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                            <td>
                                                <asp:Label runat="server" ID="lblCreatedateinMechSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblJobPostingCode" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:HyperLink runat="server" ID="lblRequisitioninMechSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblActiveRecruiter" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblOfferStatus" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblCandidateID" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:HyperLink runat="server" ID="lblCandidateinMechSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblOfferCategoryinMechSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblOfferedGradeinMechSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblBasicinMechSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblFlexiPay1inMechSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblFlexiPay2inMechSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblAdditionalAllowanceinMechSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblAdHocAllowanceinMechSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblLocationAllowanceinMechSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblSAFAllowanceinMechSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblHRAinMechSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblHLISAinMechSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblEducationAllowanceinMechSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblACLRAinMechSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblCLRAinMechSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblSpecialAllowanceinMechSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblSpecialPerformancePayinMechSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblECALinMechSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblCarMileageReimbursementinMechSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblTelephoneReimbursementinMechSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblLTAinMechSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblPFinMechSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblGratuityinMechSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblMedicalreimbursementsinMechSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblPCSchemeinMechSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblRetentionPayinMechSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblPLRinMechSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblSalesIncentiveinMechSalarydetails" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblTotalCTCinMechSalarydetails" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnExportToExcelMechSalarydetails" />
                                <asp:PostBackTrigger ControlID="btnExportToPDFMechSalarydetails" />
                                <asp:PostBackTrigger ControlID="btnExportToWordMechSalarydetails" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
    </div> </div>
</asp:Content>
