﻿
using System;
using System.Web.Security;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;
using TPS360.Common;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using ExpertPdf.HtmlToPdf;
using System.Drawing;
using System.Collections.Generic;
using System.Collections;
using System.Web.UI.HtmlControls;
using System.Linq;
using System.Configuration;
using System.Web;

namespace TPS360.Web.UI.Reports
{

    public partial class ReferralReport:BasePage 
    {

        bool _IsAccessToCandidate = true;
        private string UrlForCandidate = string.Empty;
        private  int SitemapIdForCandidate = 0;
        #region Methods


        private void PlaceUpDownArrowByReferrer()
        {
            try
            {

                LinkButton lnk = (LinkButton)lsvByReferrer.FindControl(txtByRefSortColumn.Text);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (txtByRefSortOrder.Text == "ASC" ? "Ascending" : "Descending"));

            }
            catch
            {
            }

        }

        private void PlaceUpDownArrowByRequisition()
        {
            try
            {

                LinkButton lnk = (LinkButton) lsvByRequisition.FindControl(txtByReqSortColumn.Text);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (txtByReqSortOrder.Text == "ASC" ? "Ascending" : "Descending"));

            }
            catch
            {
            }

        }
        private void PlaceUpDownArrow()
        {
            try
            {

                LinkButton lnk = (LinkButton)lsvReferralDetail.FindControl(txtSortColumn.Text);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));

            }
            catch
            {
            }

        }

        public void repor(string rep)
        {
                GenerateEmployeeReferralReport(rep);          
        }

        private void GenerateEmployeeReferralReport(string format)
        {
            if (string.Equals(format, "word"))
            {
                Response.AddHeader("content-disposition", "attachment;filename=EmployeeReferralReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".doc");
                Response.ContentEncoding = System.Text.Encoding.ASCII;
                Response.ContentType = "application/msword";
                Response.Output.Write(GetRequisitionReportTable());
                Response.Flush();
                Response.End();
            }
            else if (string.Equals(format, "excel"))
            {
                Response.AddHeader("content-disposition", "attachment;filename=EmployeeReferralReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".xls");
                Response.ContentEncoding = System.Text.Encoding.ASCII;
                Response.ContentType = "application/msexcel";
                Response.Output.Write(GetRequisitionReportTable());
                Response.Flush();
                Response.End();
            }
            else if (string.Equals(format, "pdf"))
            {
                PdfConverter pdfConverter = new PdfConverter();
                UnicodeEncoding unicodeEncoding = new UnicodeEncoding();
                pdfConverter.PageWidth = 0;
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfDocumentOptions.ShowFooter = false;
                pdfConverter.PdfDocumentOptions.LeftMargin = 15;
                pdfConverter.PdfDocumentOptions.RightMargin = 5;
                pdfConverter.PdfDocumentOptions.TopMargin = 15;
                pdfConverter.PdfDocumentOptions.BottomMargin = 5;
                pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfHeaderOptions.DrawHeaderLine = false;
                pdfConverter.PdfFooterOptions.FooterTextColor = Color.Black;
                pdfConverter.PdfFooterOptions.DrawFooterLine = true;
                pdfConverter.PdfFooterOptions.PageNumberText = "Page";
                pdfConverter.PdfFooterOptions.ShowPageNumber = true;

                pdfConverter.LicenseKey = ContextConstants.EPHTMLTOPDFKEY;
                byte[] downloadBytes = pdfConverter.GetPdfBytesFromHtmlString(GetRequisitionReportTable());

                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.Clear();
                response.AddHeader("Content-Type", "binary/octet-stream");
                response.AddHeader("Content-Disposition", "attachment; filename=EmployeeReferralReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".pdf; size=" + downloadBytes.Length.ToString());
                response.Flush();
                response.BinaryWrite(downloadBytes);
                response.Flush();
                response.End();
            }
        }
        private string GetRequisitionReportTable() 
        {
            StringBuilder reqReport = new StringBuilder();
            EmployeeReferralDataSource referralDateSource = new EmployeeReferralDataSource();
            List<EmployeeReferral> referralList = referralDateSource.GetPaged_ForDetail((ddlRequisition.SelectedIndex > 0 ? Convert.ToInt32(ddlRequisition.SelectedValue) : 0), (ddlRefferedby.SelectedIndex > 0 ? Convert.ToInt32(ddlRefferedby.SelectedValue) : 0), dtReferralDate.StartDate, dtReferralDate.EndDate, null, -1, -1);
            if (referralList != null) 
            {
                string WebRoot = ConfigurationManager.AppSettings["WebRoot"].ToString();
                string LogoPath;
                if (string.IsNullOrEmpty(HttpRuntime.AppDomainAppVirtualPath))
                {
                    LogoPath = WebRoot;
                }
                else
                {
                    LogoPath = WebRoot + HttpRuntime.AppDomainAppVirtualPath;
                }
                reqReport.Append("<table style='font-family : calibriz;width:100%; border-collapse:collapse; border-spacing: 1px;' align='left' border = '1' bordercolor='#000000'  cellspacing='0' cellpadding='0'>");

                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=left  width='300' height='75'>&nbsp;<img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png' style= style='height:56px;width:56px'/></td>");
                reqReport.Append("    </tr>");
                reqReport.Append(" <tr>");
                reqReport.Append("     <th>Date</th>");
                reqReport.Append("     <th>Refferrer</th>");
                reqReport.Append("     <th>Refferrer Email</th>");
                reqReport.Append("     <th>Refferrer PS #</th>");
                reqReport.Append("     <th>Requisition</th>");
                reqReport.Append("     <th>Candidate ID #</th>");
                reqReport.Append("     <th>Candidate</th>");
                reqReport.Append(" </tr>");

                foreach (EmployeeReferral log in referralList)
                {
                    if (log != null)
                    {
                        reqReport.Append(" <tr>");
                        reqReport.Append("     <td>" + log.CreateDate.ToShortDateString() + "&nbsp;</td>");
                        reqReport.Append("     <td>" + log.RefererFirstName+" "+log.RefererLastName+ "&nbsp;</td>");
                        reqReport.Append("     <td>" + log.RefererEmail + "&nbsp;</td>");
                        reqReport.Append("     <td>" + log.EmployeeId  + "&nbsp;</td>");
                        reqReport.Append("     <td>" + log.JobTitle + "&nbsp;</td>");
                        reqReport.Append("     <td>" + "A" + log.MemberId .ToString () + "&nbsp;</td>");
                        reqReport.Append("     <td>" + log.CandidateName + "&nbsp;</td>");
                        reqReport.Append(" </tr>");
                    }
                }

                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=left  width='300' height='75' >&nbsp;<img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png'  style= style='height:56px;width:56px;'/></td>");//0.1
                reqReport.Append("    </tr>");
               
                reqReport.Append(" </table>"); 

            
            }
            return reqReport.ToString();
        }


        #endregion

        #region Events


        private void PrepareView()
        {
            FillEmployeeReferrerList();
            FillJobPosting();
        }
        protected void ddlRefferedby_SelectedIndexChanged(object sender, EventArgs e)
        {
            int ReferrerID = Convert.ToInt32(ddlRefferedby.SelectedValue);
            if (ReferrerID == 0)
            {
                ddlRequisition.DataSource = Facade.JobPosting_GetAllEmployeeReferralEnabled();
            }
            else
            {
               
                ddlRequisition.DataSource = Facade.JobPosting_GetAllEmployeeReferralEnabledByReferrerId(ReferrerID);
            }
            ddlRequisition.DataValueField = "Id";
            ddlRequisition.DataTextField = "JobTitle";
            ddlRequisition.DataBind();
            ddlRequisition.Items.Insert(0, new ListItem("Any", "0"));
        }

        private void FillEmployeeReferrerList()
        {
            ddlRefferedby.DataSource = Facade.GetAllEmployeeReferrerList();
            ddlRefferedby.DataValueField = "Id";
            ddlRefferedby.DataTextField = "Name";
            ddlRefferedby.DataBind();
            ddlRefferedby.Items.Insert(0, new ListItem("Any", "0"));
        }

        private void FillJobPosting()
        {
            ddlRequisition.DataSource = Facade.JobPosting_GetAllEmployeeReferralEnabled();
            ddlRequisition.DataValueField = "Id";
            ddlRequisition.DataTextField = "JobTitle";
            ddlRequisition.DataBind();
            ddlRequisition.Items.Insert(0, new ListItem("Any", "0"));
        }
        protected void Page_Load(object sender, EventArgs e)
        {


            if (IsPostBack)
            {
                hdnIsPostback.Value  = "1";
            }
            //try
            //{
            //    if (Facade.IsAvailableSessionId(Session.SessionID) != 1)
            //    {
            //        FormsAuthentication.SignOut();
            //        FormsAuthentication.RedirectToLoginPage();
            //        return;
            //    }
            //}
            //catch { }
            if (!IsPostBack)
            {
                divExportButtons.Visible = false;
                dtReferralDate.setDateRange(DateTime.Now.AddDays(-6), DateTime.Now);
                txtSortColumn.Text = "btnDate";
                txtSortOrder.Text  = "Desc";

                txtByRefSortColumn.Text = "btnName";
                txtByRefSortOrder.Text = "ASC";

                txtByReqSortColumn.Text = "btnName";
                txtByReqSortOrder.Text = "ASC";


                PrepareView();

              
                
            }
            CustomSiteMap CustomMap = new CustomSiteMap();
            CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(358, CurrentMember.Id);
            if (CustomMap == null) _IsAccessToCandidate = false;
            else
            {
                SitemapIdForCandidate = CustomMap.Id;
                UrlForCandidate = "~/" + CustomMap.Url.ToString();
            }
            if (dtReferralDate.StartDate == DateTime.MinValue) hdnStartDate.Value = "0";
            else hdnStartDate.Value = dtReferralDate.StartDate.ToString("yyyyMMdd");
            if (dtReferralDate.EndDate == DateTime.MinValue) hdnEndDate.Value = "0";
            else hdnEndDate.Value = dtReferralDate.EndDate.ToString("yyyyMMdd");
        }


        protected void lsvReferralDetail_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                EmployeeReferral   referral = ((ListViewDataItem)e.Item).DataItem as EmployeeReferral   ;

                if (referral != null)
                {
                    divExportButtons.Visible = true;
                    Label lblDate = (Label)e.Item.FindControl("lblDate");
                    Label lblReferrer = (Label)e.Item.FindControl("lblReferrer");
                    Label lblReferrerEmail = (Label)e.Item.FindControl("lblReferrerEmail");
                    Label lblReferrerPS = (Label)e.Item.FindControl("lblReferrerPS");
                    Label lblCandidateID = (Label)e.Item.FindControl("lblCandidateID");
                    lblCandidateID.Text = "A" + referral.MemberId.ToString();
                    HyperLink hlkJobTitle = (HyperLink )e.Item.FindControl("hlkJobTitle");
                    HyperLink hlnkCandidate = (HyperLink)e.Item.FindControl("hlnkCandidate");
                    lblDate.Text = referral.CreateDate.ToShortDateString();
                    lblReferrer.Text = referral.RefererFirstName + " " + referral.RefererLastName;// +"(" + referral.RefererEmail + ")";
                    hlnkCandidate.Text = referral.CandidateName;
                    lblReferrerEmail.Text = referral.RefererEmail;
                    lblReferrerPS.Text = referral.EmployeeId;
                    

                    hlkJobTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + referral .JobpostingId  + "&FromPage=JobDetail" + "','700px','570px'); return false;");
                    hlkJobTitle.Text = referral.JobTitle;

                    if (_IsAccessToCandidate)
                    {
                        if (SitemapIdForCandidate > 0 && UrlForCandidate != string.Empty)
                            ControlHelper.SetHyperLink(hlnkCandidate  , UrlForCandidate, string.Empty, referral .CandidateName , UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(referral .MemberId ), UrlConstants.PARAM_SITEMAP_ID, SitemapIdForCandidate.ToString(), UrlConstants .PARAM_SITEMAP_PARENT_ID , UrlConstants .Candidate .ATS_OVERVIEW_SITEMAP_PARENTID );
                    }
                }
            }
        }

        protected void lsvReferralDetail_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvReferralDetail .FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "ReferralReportRowPerPage";
            }
            PlaceUpDownArrow();

            if (lsvReferralDetail != null) 
            {
                if (lsvReferralDetail.Items.Count == 0) 
                {
                    lsvReferralDetail.DataSource = null;
                    lsvReferralDetail.DataBind();
                }
            }

           
        }

        protected void lsvReferralDetail_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                        else txtSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn.Text = lnkbutton.ID;
                        txtSortOrder.Text = "ASC";
                    }
                }
            }
            catch
            { }
        }




        protected void lsvByReferrer_ItemDataBound(object sender, ListViewItemEventArgs e)
        {

            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                ListWithCount  list  = ((ListViewDataItem)e.Item).DataItem as ListWithCount ;

                if (list != null)
                {
                    Label lblName = (Label)e.Item.FindControl("lblName");
                    Label lblcount = (Label)e.Item.FindControl("lblcount");
                    lblName.Text = list.Name;
                    lblcount.Text = list.Count.ToString();
                }
            }
        }

        protected void lsvByReferrer_PreRender(object sender, EventArgs e)
        {
            ASP.controls_smallpagercontrol_ascx PagerControl = (ASP.controls_smallpagercontrol_ascx)this.lsvByReferrer.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "ByReferrerReferralReportRowPerPage";
            }
            PlaceUpDownArrowByReferrer();


        }

        protected void lsvByReferrer_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtByRefSortColumn .Text == lnkbutton.ID)
                    {
                        if (txtByRefSortOrder.Text == "ASC") txtByRefSortOrder.Text = "DESC";
                        else txtByRefSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtByRefSortColumn.Text = lnkbutton.ID;
                        txtByRefSortOrder.Text = "ASC";
                    }

                    if (ByRefSortColumn.Text == string.Empty || ByRefSortColumn.Text != e.CommandArgument.ToString())
                       ByRefSortOrder.Text = "asc";
                    else ByRefSortOrder.Text = ByRefSortOrder.Text.ToLower() == "asc" ? "desc" : "asc";
                    ByRefSortColumn.Text = e.CommandArgument.ToString();

                }
            }
            catch
            { }
        }



        protected void lsvByRequisition_ItemDataBound(object sender, ListViewItemEventArgs e)
        { 

            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                ListWithCount list = ((ListViewDataItem)e.Item).DataItem as ListWithCount;

                if (list != null)
                {
                    HyperLink lnkJobTitle = (HyperLink )e.Item.FindControl("lnkJobTitle");
                    Label lblcount = (Label)e.Item.FindControl("lblcount");
                    
                    lnkJobTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + list .Id  + "&FromPage=JobDetail" + "','700px','570px'); return false;");
                    lnkJobTitle.Text = list.Name;
                    lblcount.Text = list.Count.ToString();
                }
            }
        }

        protected void lsvByRequisition_PreRender(object sender, EventArgs e)
        {
            ASP.controls_smallpagercontrol_ascx PagerControl = (ASP.controls_smallpagercontrol_ascx)this.lsvByRequisition.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "ByRequisitionReferralReportRowPerPage";
            }
            PlaceUpDownArrowByRequisition();


        }

        protected void lsvByRequisition_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtByReqSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtByReqSortOrder.Text == "ASC") txtByReqSortOrder.Text = "DESC";
                        else txtByReqSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtByReqSortColumn.Text = lnkbutton.ID;
                        txtByReqSortOrder.Text = "ASC"; 
                    }

                    if (ByReqSortColumn.Text == string.Empty || ByReqSortColumn.Text != e.CommandArgument.ToString())
                        ByReqSortOrder.Text = "asc";
                    else ByReqSortOrder.Text = ByReqSortOrder.Text.ToLower() == "asc" ? "desc" : "asc";
                    ByReqSortColumn.Text = e.CommandArgument.ToString();
                }
            }
            catch
            { }
        }

        protected void btnApply_Click(object sender, EventArgs e)
        {
            divExportButtons.Visible = false;
            lsvReferralDetail.DataBind();
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {

             FillJobPosting();

            ddlRequisition.SelectedIndex = 0;
            ddlRefferedby.SelectedIndex = 0;
            dtReferralDate.setDateRange(DateTime.Now.AddDays(-6), DateTime.Now);
            if (dtReferralDate.StartDate == DateTime.MinValue) hdnStartDate.Value = "0";
            else hdnStartDate.Value = dtReferralDate.StartDate.ToString("yyyyMMdd");
            if (dtReferralDate.EndDate == DateTime.MinValue) hdnEndDate.Value = "0";
            else hdnEndDate.Value = dtReferralDate.EndDate.ToString("yyyyMMdd");
        }
        protected void btnExportToPDF_Click(object sender, EventArgs e)
        {
            //uclConfirm.AddMessage("Note: Some columns may not appear in report if too many are selected.", ConfirmationWindow.enmMessageType.Attention, true, true, "");
            repor("pdf");
        }
        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
          repor("excel");
        }
        protected void btnExportToWord_Click(object sender, EventArgs e)
        {
            repor("word");
        }

        #endregion

    }
}