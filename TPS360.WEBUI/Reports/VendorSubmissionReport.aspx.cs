﻿
using System;
using System.Web.Security;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;
using TPS360.Common;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using ExpertPdf.HtmlToPdf;
using System.Drawing;
using System.Collections.Generic;
using System.Collections;
using System.Web.UI.HtmlControls;
using System.Linq;
using System.Configuration;
using System.Web;

namespace TPS360.Web.UI.Reports
{

    public partial class VendorSubmissionReport:BasePage 
    {

        bool _IsAccessToCandidate = true;
        private string UrlForCandidate = string.Empty;
        private  int SitemapIdForCandidate = 0;
        #region Methods


        private void PlaceUpDownArrowByContact()
        {
            try
            {

                LinkButton lnk = (LinkButton)lsvByCompanyContact.FindControl(txtByRefSortColumn.Text);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (txtByRefSortOrder.Text == "ASC" ? "Ascending" : "Descending"));

            }
            catch
            {
            }

        }

        private void PlaceUpDownArrowByRequisition()
        {
            try
            {

                LinkButton lnk = (LinkButton) lsvByRequisition.FindControl(txtByReqSortColumn.Text);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (txtByReqSortOrder.Text == "ASC" ? "Ascending" : "Descending"));

            }
            catch
            {
            }

        }
        private void PlaceUpDownArrow()
        {
            try
            {

                LinkButton lnk = (LinkButton)lsvVendorSubmissions.FindControl(txtSortColumn.Text);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));

            }
            catch
            {
            }

        }
      
   

    


        #endregion

        #region Events

        private void GenerateVendorSubmissionReport(string format)
        {
            if (string.Equals(format, "word"))
            {
                Response.AddHeader("content-disposition", "attachment;filename=VendorSubmissionReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".doc");
                Response.ContentEncoding = System.Text.Encoding.ASCII;
                Response.ContentType = "application/msword";
                Response.Output.Write(GetRequisitionReportTable());
                Response.Flush();
                Response.End();
            }
            else if (string.Equals(format, "excel"))
            {
                Response.AddHeader("content-disposition", "attachment;filename=VendorSubmissionReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".xls");
                Response.ContentEncoding = System.Text.Encoding.ASCII;
                Response.ContentType = "application/msexcel";
                Response.Output.Write(GetRequisitionReportTable());
                Response.Flush();
                Response.End();
            }
            else if (string.Equals(format, "pdf"))
            {
                PdfConverter pdfConverter = new PdfConverter();
                UnicodeEncoding unicodeEncoding = new UnicodeEncoding();
                pdfConverter.PageWidth = 0;
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfDocumentOptions.ShowFooter = false;
                pdfConverter.PdfDocumentOptions.LeftMargin = 15;
                pdfConverter.PdfDocumentOptions.RightMargin = 5;
                pdfConverter.PdfDocumentOptions.TopMargin = 15;
                pdfConverter.PdfDocumentOptions.BottomMargin = 5;
                pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfHeaderOptions.DrawHeaderLine = false;
                pdfConverter.PdfFooterOptions.FooterTextColor = Color.Black;
                pdfConverter.PdfFooterOptions.DrawFooterLine = true;
                pdfConverter.PdfFooterOptions.PageNumberText = "Page";
                pdfConverter.PdfFooterOptions.ShowPageNumber = true;

                pdfConverter.LicenseKey = ContextConstants.EPHTMLTOPDFKEY;
                byte[] downloadBytes = pdfConverter.GetPdfBytesFromHtmlString(GetRequisitionReportTable());

                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.Clear();
                response.AddHeader("Content-Type", "binary/octet-stream");
                response.AddHeader("Content-Disposition", "attachment; filename=VendorSubmissionReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".pdf; size=" + downloadBytes.Length.ToString());
                response.Flush();
                response.BinaryWrite(downloadBytes);
                response.Flush();
                response.End();
            }
        }

        private string GetRequisitionReportTable()
        {
            StringBuilder vendorsubreport = new StringBuilder();
           CompanyConatctDataSource obcompanyContact=new CompanyConatctDataSource();
           IList<VendorSubmissions> loglist = obcompanyContact.GetPaged_ForVendorSubmissionDetailForReport(Convert.ToInt32(ddlRequisition.SelectedValue), Convert.ToInt32(ddlVendor.SelectedValue), Convert.ToInt32(ddlVendorContact.SelectedValue), 0, dtSubmissionDate.StartDate, dtSubmissionDate.EndDate, "", null, -1, -1);
            if (loglist!= null)
            {
                string WebRoot = ConfigurationManager.AppSettings["WebRoot"].ToString();
                string LogoPath;
                if (string.IsNullOrEmpty(HttpRuntime.AppDomainAppVirtualPath))
                {
                    LogoPath = WebRoot;
                }
                else
                {
                    LogoPath = WebRoot + HttpRuntime.AppDomainAppVirtualPath;
                }

                vendorsubreport.Append("<table style='font-family : calibriz;width:100%; border-collapse:collapse; border-spacing: 1px;' align='left' border = '1' bordercolor='#000000'  cellspacing='0' cellpadding='0'>");

                vendorsubreport.Append("    <tr>");
                vendorsubreport.Append("        <td align=left  width='300' height='75'>&nbsp;<img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png' style= style='height:56px;width:56px'/></td>");
                vendorsubreport.Append("    </tr>");
                vendorsubreport.Append(" <tr>");
                vendorsubreport.Append("     <th>DateSubmitted</th>");
                vendorsubreport.Append("     <th>Candidate ID #</th>");
                vendorsubreport.Append("     <th>CandidateName</th>");
                vendorsubreport.Append("     <th>Vendor</th>");
                vendorsubreport.Append("     <th>SubmittedBy </th>");
                vendorsubreport.Append("     <th>JobTitle</th>");
                vendorsubreport.Append("     <th>CurrentStatus</th>");
                vendorsubreport.Append(" </tr>");

                foreach (VendorSubmissions log in loglist)
                {
                    if (log != null)
                    {
                        vendorsubreport.Append(" <tr>");
                        vendorsubreport.Append("     <td>" + log.SubmittedDate.ToShortDateString() + "&nbsp;</td>");
                        vendorsubreport.Append("     <td>" + "A" + log.CandidateId .ToString () + "&nbsp;</td>");
                        vendorsubreport.Append("     <td>" + log.CandidateName + "&nbsp;</td>");
                        vendorsubreport.Append("     <td>" + log.VendorName + "&nbsp;</td>");
                        vendorsubreport.Append("     <td>" + log.ContactName + "&nbsp;</td>");
                        vendorsubreport.Append("     <td>" + log.JobTitle + "&nbsp;</td>");
                        vendorsubreport.Append("     <td>" +log.CurrentStatus+ "&nbsp;</td>");
                        vendorsubreport.Append(" </tr>");
                    }
                }

               vendorsubreport.Append("    <tr>");
               vendorsubreport.Append("        <td align=left  width='300' height='75'><img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png' style= style='height:56px;width:56px;'/></td>");
               vendorsubreport.Append("    </tr>");
              
               vendorsubreport.Append(" </table>");
            }
            return vendorsubreport.ToString();
        }


        private void PrepareView()
        {
          
            FillJobPosting();
            MiscUtil.PopulateClients(ddlVendor, (int)CompanyStatus.Vendor, Facade);
            ddlVendor.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
            ddlVendor.Items.RemoveAt(1);
            PopulateClientContactList();
        }

        private void PopulateClientContactList()
        {

            ddlVendorContact.DataSource = Facade.GetAllCompanyContactsByCompanyId(Convert.ToInt32(ddlVendor .SelectedValue == "" ? "0" : ddlVendor .SelectedValue));
            ddlVendorContact.DataTextField = "FirstName";
            ddlVendorContact.DataValueField = "Id";
            ddlVendorContact.DataBind();
            ListItem item = new ListItem();
            item.Text = "Please Select";
            item.Value = "0";
            ddlVendorContact.Items.Insert(0, item);
            if (ddlVendorContact.Items.Count == 1) ddlVendorContact.Enabled = false;
            else ddlVendorContact.Enabled = true;
        }

        private void FillJobPosting()
        {
            ddlRequisition.DataSource = Facade.JobPosting_GetAllVendorPortalEnabled ();
            ddlRequisition.DataValueField = "Id";
            ddlRequisition.DataTextField = "JobTitle";
            ddlRequisition.DataBind();
            ddlRequisition.Items.Insert(0, new ListItem("Any", "0"));
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                hdnIsPostback.Value = "1";
            }

            ddlVendor.Attributes.Add("onChange", "return Company_OnChangeFillContactEmail('" + ddlVendor.ClientID + "','" + ddlVendorContact.ClientID + "','" + hdnSelectedContactID.ClientID + "','MemberId')");
            ddlVendorContact.Attributes.Add("OnChange", "return Contact_OnChange('" + ddlVendorContact.ClientID + "','" + hdnSelectedContactID.ClientID + "')");

            //try
            //{
            //    if (Facade.IsAvailableSessionId(Session.SessionID) != 1)
            //    {
            //        FormsAuthentication.SignOut();
            //        FormsAuthentication.RedirectToLoginPage();
            //        return;
            //    }
            //}
            //catch { }
            if (!IsPostBack)
            {
                divExportButtons.Visible = false;
                dtSubmissionDate.setDateRange(DateTime.Now.AddDays(-6), DateTime.Now);
                txtSortColumn.Text = "btnDate";
                txtSortOrder.Text  = "Desc";

                txtByRefSortColumn.Text = "btnName";
                txtSortOrder.Text = "ASC";

                txtByReqSortColumn.Text = "btnName";
                txtByReqSortOrder.Text = "ASC";
                PrepareView();
            }
            CustomSiteMap CustomMap = new CustomSiteMap();
            CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(358, CurrentMember.Id);
            if (CustomMap == null) _IsAccessToCandidate = false;
            else
            {
                SitemapIdForCandidate = CustomMap.Id;
                UrlForCandidate = "~/" + CustomMap.Url.ToString();
            }
            if (dtSubmissionDate.StartDate == DateTime.MinValue) hdnStartDate.Value = "0";
            else hdnStartDate.Value = dtSubmissionDate.StartDate.ToString("yyyyMMdd");
            if (dtSubmissionDate.EndDate == DateTime.MinValue) hdnEndDate.Value = "0";
            else hdnEndDate.Value = dtSubmissionDate.EndDate.ToString("yyyyMMdd");
        }
        public void repor(string rep)
        {


            GenerateVendorSubmissionReport(rep);


        }


        protected void lsvVendorSubmissions_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                divExportButtons.Visible = true;
                VendorSubmissions vendorSubmission = ((ListViewDataItem)e.Item).DataItem as VendorSubmissions;
                Label lblDateTime = (Label)e.Item.FindControl("lblDateTime");
                HyperLink hlnkCandidateName = (HyperLink)e.Item.FindControl("hlnkCandidateName");
                HyperLink lnkJobTitle = (HyperLink)e.Item.FindControl("lnkJobTitle");
                HyperLink hnlkVendor = (HyperLink)e.Item.FindControl("hnlkVendor");
                HyperLink hlnkContact = (HyperLink)e.Item.FindControl("hlnkContact");
                Label hlnkCurrentStatus = (Label)e.Item.FindControl("hlnkCurrentStatus");
                Label lblCandidateID = (Label)e.Item.FindControl("lblCandidateID");
                lblCandidateID.Text = "A" + vendorSubmission.CandidateId.ToString();
                lblDateTime.Text = vendorSubmission.SubmittedDate.ToShortDateString();
                hlnkCandidateName.Text = vendorSubmission.CandidateName;
                if (_IsAccessToCandidate)
                {
                    if (SitemapIdForCandidate > 0 && UrlForCandidate != string.Empty)
                        ControlHelper.SetHyperLink(hlnkCandidateName, UrlForCandidate, string.Empty, vendorSubmission.CandidateName, UrlConstants.PARAM_MEMBER_ID, vendorSubmission.CandidateId.ToString(), UrlConstants.PARAM_SITEMAP_ID, SitemapIdForCandidate.ToString(),UrlConstants .PARAM_SITEMAP_PARENT_ID , UrlConstants.Candidate .ATS_OVERVIEW_SITEMAP_PARENTID );
                }
                else
                {
                    hlnkCandidateName.Enabled = false;
                    hlnkCandidateName.Text = vendorSubmission.CandidateName;
                }

                JobPostingHiringTeam details = new JobPostingHiringTeam();
                details = Facade.GetJobPostingHiringTeamByMemberId(CurrentMember.Id, vendorSubmission.JobPostingID);
                if (details != null)
                {

                    lnkJobTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + vendorSubmission.JobPostingID + "&FromPage=JobDetail" + "','700px','570px'); return false;");
                    lnkJobTitle.Text = vendorSubmission.JobTitle;
                }

                else
                {
                    lnkJobTitle.Enabled = false;
                    lnkJobTitle.Text = vendorSubmission.JobTitle;
                }
                hnlkVendor.Text = vendorSubmission.VendorName;

                hlnkContact.Text = vendorSubmission.ContactName;
                 SecureUrl Modalurl = UrlHelper.BuildSecureUrl("../Modals/CompanyContact.aspx", string.Empty, UrlConstants.PARAM_CONTACT_ID, vendorSubmission .Id .ToString ());
                hlnkContact.Attributes .Add ("onclick","javascript:EditModal('" + Modalurl.ToString() + "','675px','450px'); return false;");
                hlnkCurrentStatus.Text = vendorSubmission.CurrentStatus;

            }
        }
       
        protected void lsvVendorSubmissions_PreRender(object sender, EventArgs e)
        {
            lsvVendorSubmissions.DataBind();
            ASP.controls_pagercontrol_ascx  PagerControl = (ASP.controls_pagercontrol_ascx )this.lsvVendorSubmissions.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    if (pager.Controls.Count >= 1)
                    {
                        DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                        if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                    }
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "DashboardRecentChangesRowPerPage";
            }
            PlaceUpDownArrow();
            if (lsvVendorSubmissions.Controls.Count == 0)
            {
                lsvVendorSubmissions.DataSource = null;
                lsvVendorSubmissions.DataBind();
            }
        }

        protected void lsvVendorSubmissions_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                        else txtSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn.Text = lnkbutton.ID;
                        txtSortOrder.Text = "ASC";
                    }

                }
            }
            catch
            {
            }
        }

        protected void lsvByCompanyContact_ItemDataBound(object sender, ListViewItemEventArgs e)
        {

            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                ListWithCount  list  = ((ListViewDataItem)e.Item).DataItem as ListWithCount ;

                if (list != null)
                {
                    Label lblVendorName = (Label)e.Item.FindControl("lblVendorName");
                    Label lblcontactname = (Label)e.Item.FindControl("lblcontactname");
                    Label lblcount = (Label)e.Item.FindControl("lblcount");
                    lblVendorName.Text = list.AdditionalName ;
                    lblcontactname.Text = list.Name;
                    lblcount.Text = list.Count.ToString();
                }
            }
        }

        protected void lsvByCompanyContact_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvByCompanyContact .FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "ByContactVSReportRowPerPage";
            }
            PlaceUpDownArrowByContact();
        }

        protected void lsvByCompanyContact_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtByRefSortColumn .Text == lnkbutton.ID)
                    {
                        if (txtByRefSortOrder.Text == "ASC") txtByRefSortOrder.Text = "DESC";
                        else txtByRefSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtByRefSortColumn.Text = lnkbutton.ID;
                        txtByRefSortOrder.Text = "ASC";
                    }
                }
            }
            catch
            { }
        }



        protected void lsvByRequisition_ItemDataBound(object sender, ListViewItemEventArgs e)
        { 

            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                ListWithCount list = ((ListViewDataItem)e.Item).DataItem as ListWithCount;

                if (list != null)
                {
                    HyperLink lnkJobTitle = (HyperLink )e.Item.FindControl("lnkJobTitle");
                    Label lblcount = (Label)e.Item.FindControl("lblcount");
                    
                    lnkJobTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + list .Id  + "&FromPage=JobDetail" + "','700px','570px'); return false;");
                    lnkJobTitle.Text = list.Name;
                    lblcount.Text = list.Count.ToString();
                }
            }
        }

        protected void lsvByRequisition_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvByRequisition.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "ByRequisitionVSReportRowPerPage";
            }
            PlaceUpDownArrowByRequisition();


        }

        protected void lsvByRequisition_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtByReqSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtByReqSortOrder.Text == "ASC") txtByReqSortOrder.Text = "DESC";
                        else txtByReqSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtByReqSortColumn.Text = lnkbutton.ID;
                        txtByReqSortOrder.Text = "ASC"; 
                    }
                }
            }
            catch
            { }
        }

        protected void btnApply_Click(object sender, EventArgs e)
        {
            divExportButtons.Visible = false;
            lsvVendorSubmissions.DataBind();
            PopulateClientContactList();
            ControlHelper.SelectListByValue(ddlVendorContact, hdnSelectedContactID.Value);
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {


            ddlRequisition.SelectedIndex = 0;
            ddlVendor.SelectedIndex = 0;
            PopulateClientContactList();
            hdnSelectedContactID.Value = "0";
            dtSubmissionDate.setDateRange(DateTime.Now.AddDays(-6), DateTime.Now);
            if (dtSubmissionDate.StartDate == DateTime.MinValue) hdnStartDate.Value = "0";
            else hdnStartDate.Value = dtSubmissionDate.StartDate.ToString("yyyyMMdd");
            if (dtSubmissionDate.EndDate == DateTime.MinValue) hdnEndDate.Value = "0";
            else hdnEndDate.Value = dtSubmissionDate.EndDate.ToString("yyyyMMdd");
        }
        protected void btnExportToPDF_Click(object sender, EventArgs e)
        {
            repor("pdf");
            //uclConfirm.AddMessage("Note: Some columns may not appear in report if too many are selected.", ConfirmationWindow.enmMessageType.Attention, true, true, "");
        }
        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            repor("excel");
        }
        protected void btnExportToWord_Click(object sender, EventArgs e)
        {
            repor("word");
        }

        #endregion

    }
}