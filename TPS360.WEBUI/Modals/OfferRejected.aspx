﻿<%-- -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: InternalResumeBuilder.aspx
    Description: This is the page which is used to present tab controls
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date           Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Oct-16-2008        Gopala Swamy         Defect id: 8994; Renamed in tabcontrol from "Job tittle & Category" to "Job tittles"
    0.2             Mar-04-2009        Shivanand            Defect #10053; Removed tab "Job Titles".
    0.3             May-26-2009         Sandeesh            Defect id : 10464 Changes made to load the tabs instantaneously
    0.4             July-01-2009       Shivanand            Defect #10464; AsyncMode is enabled for Tabs.
    0.5             Jul-24-2009           Veda              Defect id: 11072 ; Contineous Spinning occurs in the browser tab.
    0.6             Jul-30-2009           Veda              Defect id: 11132; 
    0.7             Aug-31-2009         Ranjit Kumar.I      Defect id:#11421;Desabled Trace in Page Level Script
    0.8             Apr-07-2010        Ganapati Bhat        Enhancement #12139; Removed tab "Skill Set"
-------------------------------------------------------------------------------------------------------------------------------------------       

--%>


<%@ Page Language="C#" MasterPageFile="~/Modals/Modal.master" AutoEventWireup="true" CodeFile="OfferRejected.aspx.cs" Inherits="TPS360.Web.UI.OfferRejection"  EnableViewState ="true"  %>
<%@ Register Assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.Web.UI.EditorControls" TagPrefix="ig" %>
<%@ Register assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.Web.UI.LayoutControls" tagprefix="ig" %>


<asp:Content ID="cntCandidateResumeBuilder" ContentPlaceHolderID="cphModalMaster"
    runat="Server">
          
   <asp:HiddenField ID ="hfStatusId" runat ="server" /> 
    <div style =" width : 450px; height : 300px; display:table-cell    ; vertical-align : middle ; ">
    <div class="TableRow" style="text-align: center">
        <asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
        <asp:HiddenField ID="hfMemberId" runat="server" Value="0" />
    </div>
    <div class="TableRow">
        <div class="TableFormLeble">
            <asp:Label EnableViewState="false" ID="lblOfferRejectedDate" runat="server" Text="Offer Decline Date"></asp:Label>:
        </div>
        
        <div class="TableFormContent" style="white-space:nowrap">
            <div>
                <div style="float:left" id="divOfferRejectedDate" runat="server">
                   <%-- <igsch:WebDateChooser ID="wdcRejectedDate" runat="server" 
                        NullDateLabel="" EnableKeyboardNavigation="True" AutoCloseUp="true"  
                            FocusOnInitialization="True" Editable="True">
                        <CalendarLayout HideOtherMonthDays="True" >
                        </CalendarLayout>
                    </igsch:WebDateChooser>--%>
                    <ig:WebDatePicker ID="wdcRejectedDate"  DropDownCalendarID="ddcRejectedDate" runat="server"></ig:WebDatePicker>
                    <ig:WebMonthCalendar  ID="ddcRejectedDate" AllowNull="true"  runat="server"></ig:WebMonthCalendar>
                    </div>   
                     <span class="RequiredField">*</span>
                    <div class="TableRow" > 
        <div Class="TableFormValidatorContent"   style =" margin-left : 42%">  
          <asp:RequiredFieldValidator ID="rfvDate" runat="server" ControlToValidate="wdcRejectedDate" SetFocusOnError="true" 
            ErrorMessage="Please Select Rejected Date." Display="Dynamic" ValidationGroup="ValGrp"></asp:RequiredFieldValidator>
        </div>
     </div>
                                        
                
        
    </div>
    
    </div>
    </div>
    
    <div class="TableRow">
        <div class="TableFormLeble">
            <asp:Label EnableViewState="false" ID="lblReasonForRejection" runat="server" Text="Reason For Decline"></asp:Label>:
        </div>
         <div class="TableFormContent">
         

            <asp:DropDownList ID="ddlReasonForRejection" runat ="server" CssClass ="CommonDropDownList" >

             </asp:DropDownList>
             <span class="RequiredField">*</span>
             </div>
             
             
             
             <div class="TableRow" > 
        <div Class="TableFormValidatorContent"   style =" margin-left : 42%">  
          <asp:RequiredFieldValidator InitialValue="0" ID="rfvReasonForRejection" Display="Dynamic" ValidationGroup="ValGrp" runat="server" ControlToValidate="ddlReasonForRejection"
    ErrorMessage="Please Select Reason For Rejection"></asp:RequiredFieldValidator>
        </div>
     </div>
        
        
        <br />
        <div class="TableRow" style="text-align: center;">
        <asp:Button ID="btnSave" CssClass="CommonButton" runat="server" Text="Save" OnClick="btnSave_Click" ValidationGroup="ValGrp" />
    </div>
             
             
             </div>
             </div>
             
             
</asp:Content>
        