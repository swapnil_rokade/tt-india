﻿<%@ Page Language="C#" MasterPageFile="~/Modals/Modal.master" AutoEventWireup="true"
    CodeFile="WelcomePage.aspx.cs" Inherits="TPS360.Web.UI.WelcomePage" EnableViewState="true" %>

<%@ Register Src="~/Controls/ChangePassword.ascx" TagName="ChangePassword" TagPrefix="ucl" %>
<asp:Content ID="cntCandidateResumeBuilder" ContentPlaceHolderID="cphModalMaster"
    runat="Server">
    <style>
        
        
        .WelcomeSteps
        {
            font-weight: bold;
            font-size: 12px;
            font-family: sans-serif;
            padding: 10px 0px;
        }
        .footer
        {
            background-color: #F5F5F5;
            border-radius: 0 0 6px 6px;
            border-top: 1px solid #DDDDDD;
            box-shadow: 0 1px 0 #FFFFFF inset;
            margin-bottom: 0;
            padding: 14px 15px 15px;
            text-align: right;
        }
    </style>
    <link href="../Style/Common.css" />
    <div style="width: 525px; height: 390px;  overflow :auto;">
        <asp:Label ID="lblWelcomeNote" runat="server" CssClass="WelcomeSteps">Please take a look at these options before you get started.</asp:Label>
        <div class="WelcomeSteps">
            1. If you have assigned a default random password, please change it now:
        </div>

        <script>
 function CheckAllValidatorControls()
 {
 
    var txtNewPassword=document .getElementById ('<%= txtNewPassword.ClientID %>');
    var txtConfirmPassword=document .getElementById ('<%= txtConfirmPassword.ClientID %>');
    
    var revNewPassword=document .getElementById ('<%= revNewPassword.ClientID %>');
    var cvNewPassword=document .getElementById ('<%= cvNewPassword.ClientID %>');
    if(txtNewPassword .value !='' && txtConfirmPassword .value !=''&& revNewPassword.isvalid &&  cvNewPassword.isvalid)
    {
        $('#imgCorrect').show();
    }else $('#imgCorrect').hide();
    
 }
 
 function CheckBoxCheck()
{

    var chkbox=document.getElementById('<%=chkSessionTime.ClientID %>');
    var drpdown=document.getElementById('<%=drpTimeoutValueList.ClientID %>');
    drpdown.disabled=!chkbox.checked;
    
}

        </script>

        <asp:UpdatePanel ID="upChangePassword" runat="server">
            <ContentTemplate>
                <div style="padding: 15px;">
                    <div class="TableRow" style="text-align: center;">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            <asp:Label ID="lblNewPassword" runat="server" Text="New Password"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtNewPassword" MaxLength="15" runat="server" CssClass="CommonTextBox"
                                onblur="CheckAllValidatorControls()" TextMode="Password"></asp:TextBox>
                            <%--<span class="RequiredField">*</span>--%>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="padding-left: 43%;">
                        <%--    <asp:RequiredFieldValidator ID="rfvNewPassword" ValidationGroup="changePassword"
                                runat="server" ControlToValidate="txtNewPassword" ErrorMessage="Please enter new password."
                                EnableViewState="False" Display="Dynamic">
                            </asp:RequiredFieldValidator>--%>
                            <asp:RegularExpressionValidator ID="revNewPassword" ValidationGroup="changePassword"
                                runat="server" ControlToValidate="txtNewPassword" Display="Dynamic" ErrorMessage="Please enter at least 8 characters."
                                ValidationExpression="^[\s\S]{8,}$"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            <asp:Label ID="lblConfirmPassword" runat="server" Text="Confirm Password"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtConfirmPassword" MaxLength="15" runat="server" CssClass="CommonTextBox"
                                onblur="CheckAllValidatorControls()" TextMode="Password"></asp:TextBox>
                            <%--<span class="RequiredField">*</span>--%>
                            <img src="../Images/good_or_tick.png" width="25px" height="25px" id="imgCorrect"
                                style="display: none;" />
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="padding-left: 43%;">
                        <%--     <asp:RequiredFieldValidator ID="rfvConfirmPassword" ValidationGroup="changePassword"
                                runat="server" ControlToValidate="txtConfirmPassword" ErrorMessage="Please retype new password."
                                EnableViewState="False" Display="Dynamic">
                            </asp:RequiredFieldValidator>--%>
                            <asp:CompareValidator ID="cvNewPassword" ValidationGroup="changePassword" runat="server"
                                ControlToValidate="txtConfirmPassword" Type="String" Operator="Equal" ControlToCompare="txtNewPassword"
                                ErrorMessage="Passwords must match." EnableViewState="False" Display="Dynamic">
                            </asp:CompareValidator>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="WelcomeSteps">
            2. The user session will timeout after this length of inactivity:
        </div>
        <div class="TableRow">
            <div class="TableFormLeble" style="padding-left: 30px">
                <asp:CheckBox ID="chkSessionTime" runat="server" Text="Enable Session Timeout" Font-Bold="true"
                    onclick="javascript:CheckBoxCheck()"></asp:CheckBox>
            </div>
            <div class="TableFormContent">
            </div>
        </div>
        <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label EnableViewState="false" ID="lblsessiontimeout" runat="server" Text="Log out after"></asp:Label>:
            </div>
            <div class="TableFormContent">
                <asp:DropDownList ID="drpTimeoutValueList" runat="server" CssClass="CommonDropDownList"
                    Width="100px">
                    <asp:ListItem Text="1">1</asp:ListItem>
                    <asp:ListItem Text="2">2</asp:ListItem>
                    <asp:ListItem Text="5">5</asp:ListItem>
                    <asp:ListItem Text="10">10</asp:ListItem>
                    <asp:ListItem Text="15">15</asp:ListItem>
                    <asp:ListItem Text="20" Selected="True">20</asp:ListItem>
                    <asp:ListItem Text="30">30</asp:ListItem>
                    <asp:ListItem Text="60">60</asp:ListItem>
                    <asp:ListItem Text="90">90</asp:ListItem>
                    <asp:ListItem Text="120">120</asp:ListItem>
                </asp:DropDownList>
                minutes
            </div>
        </div>
        <div class="WelcomeSteps">
            3. If you plan to send emails, please enter your mail account SMTP info:
        </div>
        <div class="WelcomeSteps">
        
            <div class="FormLeftColumn">
             <div class="TableRow" >
                    <div class="TableFormLeble"  style =" width : 30%">
                        User:
                    </div>
                    <div class="TableFormContent"  >
                        <asp:TextBox ID="txtUserEmail" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble"  style =" width : 30%">
                        Password:
                    </div>
                    <div class="TableFormContent">
                        <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                    </div>
                </div>
                
            </div>
            <div class=" FormRightColumn">
               
                <div class="TableRow">
                    <div class="TableFormLeble"  style =" width : 30%">
                        SMTP Server:</div>
                    <div class="TableFormContent">
                        <asp:TextBox ID="txtSMTPServer" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble"  style =" width : 30%">
                        Port:
                    </div>
                    <div class="TableFormContent">
                        <asp:TextBox ID="txtPort" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                    </div>
                    
                </div>
                 <div class="TableRow">
                    <div class="TableformValidatorContent" style="margin-left: 30%">
                        <asp:RegularExpressionValidator ID="revSMTPPort" runat="server" ControlToValidate="txtPort"
                            Display="Dynamic" ErrorMessage="Please enter valid port number." ValidationExpression="^(6553[0-5]|655[0-2]\d|65[0-4]\d\d|6[0-4]\d{3}|[1-5]\d{4}|[1-9]\d{0,3}|0)$"
                            ValidationGroup="changePassword"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble"  style =" width : 30%">
                        SSL:
                    </div>
                    <div class="TableFormContent">
                        <asp:CheckBox ID="chkEnableSSL" runat="server" />
                    </div>
                </div>
            </div>
           
            <asp:UpdatePanel ID="up" runat="server">
                <ContentTemplate>
                    <div class="TableRow" style="text-align: center">
                        <asp:Button ID="btnTestSMTPSettings" runat="server" Text="Test" CssClass="btn" OnClick="btnTextSMTPSettings_Click"
                            OnClientClick="RemoveModal(); " />
                        <asp:UpdateProgress ID="UpdateProgress1" EnableViewState="false" runat="server" DisplayAfter="0"
                            DynamicLayout="true">
                            <ProgressTemplate>
                                <asp:Image runat="server" ID="imgLogo1" ImageUrl="~/Images/loading.gif" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </div>
                    <div class="TableRow" style="text-align: center;  width : 50%; margin-left : 25%; margin-top : 5px;">
                        <div id="divTestMessage" runat="server">
                        </div>
                    </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="TableRow WelcomeSteps">
                4. You can download the Resume Parser desktop application from here:
            </div>
            <div class="TableRow" style="text-align: center">
                <a  ID="lnkDownloadParser" runat="server" onclick ="return false;" >Download Resume Parser</a>
            </div>
            </div>
    </div> 
            <div class="footer">
                <asp:Button ID="btnSaveAndStart" runat="server" Text="Ok, let's get started!" CssClass=" btn btn-primary"
                    OnClick="btnSaveAndStart_Click" ValidationGroup ="changePassword" />
            </div>
            
</asp:Content>
