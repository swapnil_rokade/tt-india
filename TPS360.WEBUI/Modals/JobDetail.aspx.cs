﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: InternalResumeBuilder.aspx.cs
    Description: This is the user control page used for resume builder.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.           Date               Author             Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1               Mar-04-2009        Shivanand          Defect #10053; usercontrol "ucntrlJobTitle" is removed.
    0.2               July-01-2009       Shivanand          Defect #10464; AsyncOptions are provided for Tabs.
 *  0.3               Apr-07-2010        Ganapati Bhat      Enhancement #12139; Removed tab "Skill Set"
------------------------------------------------------------------------------------------------------------------------------------------- 
*/

using System;

using TPS360.Common.Helper;
using System.Drawing;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities ;
namespace TPS360.Web.UI
{
    public partial class JobDetail : RequisitionBasePage
    {
        protected void Page_Load(object sender, EventArgs args)
        {
            if (!IsPostBack)
            {
                string JID = Request.QueryString["JID"].ToString();
                string FromPage = "";
                if (Request.QueryString["FromPage"] != null) FromPage = Request.QueryString["FromPage"].ToString();


                //Job Description View in Employee Referral Portal
                if (FromPage != "JobDetail" && FromPage != "Vendor" && FromPage != "Referrer")
                {

                    divContent.Style.Add(" max-height", "470px");
                    MemberJobApplied obj = Facade.MemberJobApplied_GetByMemberIDANDJobPostingID(CurrentMember.Id, Convert.ToInt32(JID));
                    if (obj != null)
                    {
                        btnApply.Visible = false;
                        lblApplied.Visible = true;
                    }
                    else
                    {
                        btnApply.Visible = true;
                        lblApplied.Visible = false;
                    }
                }
                else
                {
                    divApply.Visible = false;
                }
            }
        }
        private void buildMemberJobApplied()
        {
            MemberJobApplied newjob = new MemberJobApplied();
            string JID = Request.QueryString["JID"].ToString();
            newjob.MemberId = CurrentMember.Id;
            newjob.JobPostingId = Convert.ToInt32(JID);
            Facade.AddMemberJobApplied(newjob);

        }
        protected void btnApply_Click(object sender, EventArgs args)
        {

            MemberDocument memberDocu = Facade.GetRecentResumeByMemberID(CurrentMember.Id);
            string JID = Request.QueryString["JID"].ToString();
            if (memberDocu != null)
            {
                buildMemberJobApplied();
                Facade.MemberJobCart_AddCandidateToRequisition(CurrentMember.Id, CurrentMember.Id.ToString(), Convert.ToInt32(JID));
                MiscUtil.ShowMessage(lblMessage, "success “Thank you for your application.”", false);
                btnApply.Visible = false;
                lblApplied.Visible = true;
            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Please upload a resume before applying to jobs.", true);
            }
        }
    }
}
