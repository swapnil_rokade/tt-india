﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="JobDetailsPrintView.aspx.cs" Inherits="TPS360.Web.UI.JobDetailsPrintView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src ="~/Controls/PrintJob.ascx" TagName ="PrintJob" TagPrefix ="ucl" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Print Job</title>
              <link href="~/Style/Common.css" type="text/css" rel="stylesheet" />
    <link href="~/Style/styles.css" type="text/css" rel="stylesheet" />
     <script type="text/javascript"  language ="javascript" >
    function Print()
    {
    window .print ();
    }
    </script>
 
</head>
<body style="background-color:White">
    <form id="form1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release" EnableHistory ="true">
      <Scripts>
            <asp:ScriptReference Path="~/Scripts/Common.js" />
            <asp:ScriptReference Path="~/Scripts/TopMenu.js" />
            <asp:ScriptReference Path="~/Scripts/AddScrollEffect.js" />
        </Scripts>
        </asp:ScriptManager>

    <div style =" width :500px; "> 
            <div style =" width : 560px; text-align : center ; margin-left : 10%" >
                          
                          <br />
                          <asp:LinkButton ID="ere" runat="server" OnClientClick ="Print()" Text ="Print Job Details"></asp:LinkButton>
                          <ucl:PrintJob ID="uclPrintJob" runat ="server" />
                               </div> 
    </div>
    </form>
</body>
</html>
