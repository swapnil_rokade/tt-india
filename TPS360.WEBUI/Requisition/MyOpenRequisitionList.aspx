﻿<%--
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MyOpenRequisitionList.aspx
    Description: This is the page which is used to display the MyOpenRequisation list 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author                Modification
    --------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    0.1             Feb-12-2009          Kalyani Pallgani       Defect id: 9932;Modified commandarguments because of column abiguity
    0.2             May-15-2009          Sandeesh                Defect id:10440 :Changes made to get the Requisition status from database instead of reading from the  Enum  
    0.3             Jan -20-2010        Basavaraj              Defect Id 8879;Defect Id 12901; Changed the style attribute.
    0.4             Apr-30-2010          Sudarshan.R.           Defect Id:12726; Removed scroll option from GridContainer
    --%>
    

<%@ Page Language="C#" MasterPageFile="~/Default.master" EnableEventValidation="false" AutoEventWireup="true" CodeFile="MyOpenRequisitionList.aspx.cs" Inherits="TPS360.Web.UI.Requisition.MyOpenRequisitionList" Title="My Open Requisition List" %>

<asp:Content ID="cntBasicInfoTitle" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
    My Open Requisition List
</asp:Content>
<asp:Content ID="cntBasicInfoEditor" ContentPlaceHolderID="cphHomeMaster" runat="Server">
<div>
    <asp:UpdatePanel ID="pnlDynamic" runat="server">
        <ContentTemplate>
            <asp:ObjectDataSource ID="odsJobPostingList" runat="server" 
                SelectMethod="GetPaged" TypeName="TPS360.Web.UI.JobPostingDataSource"
                SelectCountMethod="GetListCount" EnablePaging="True" 
                SortParameterName="sortExpression" OnLoad="odsJobPostingList_Load" 
                oninit="odsJobPostingList_Init">
                <SelectParameters>
                    <asp:Parameter Name="IsTemplate" DefaultValue="False" Type="Boolean" Direction="Input"/>
<%--                    <asp:Parameter Name="JobStatus" DefaultValue="1" Type="Int32" Direction="Input"/>
--%>         <%--0.2--%>          <asp:Parameter Name="JobStatus" DefaultValue="0" Type="Int32" Direction="Input"/>
                    <asp:Parameter Name="IsJobActive" DefaultValue="true" Type="Boolean" Direction="Input"/>
                    <asp:Parameter Name="memberId" Type="String" Direction="Input" DefaultValue="0"/>
                </SelectParameters>
            </asp:ObjectDataSource>

            <div>
                <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                <div style="text-align: right;">                    
                    <asp:HyperLink ID="lnkAddJobPosting" CssClass="NormalLink" ToolTip="Add Requisition"
                        runat="server"></asp:HyperLink>
                </div>
            </div>          
            <div class="GridContainer">              
                <asp:ListView ID="lsvJobPosting" runat="server" DataSourceID="odsJobPostingList" DataKeyNames="Id"
                    OnItemDataBound="lsvJobPosting_ItemDataBound" OnItemCommand="lsvJobPosting_ItemCommand">
                    <LayoutTemplate>
                        <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                            <tr>
                            <%-- 0.1 starts here --%>
                                <th style="width: 10%; white-space: nowrap;">
                                    <asp:LinkButton ID="btnPostedDate" runat="server" CommandName="Sort" CommandArgument="[J].[PostedDate]"
                                        Text="Date Posted" />
                                </th>
                                
                                <th style="width: 8%; white-space: nowrap;">
                                    <asp:LinkButton ID="btnClient" runat="server" CommandName="Sort" CommandArgument="[J].[ClientId]"
                                        Text="Client" />
                                </th>
                                
                                <th style="width: 8%; white-space: nowrap;">
                                    <asp:LinkButton ID="btnJobTitle" runat="server" CommandName="Sort" CommandArgument="[J].[JobTitle]"
                                        Text="Position" />
                                </th>
                                
                                <th style="width: 8%; white-space: nowrap;">
                                    <asp:LinkButton ID="btnCity" runat="server" CommandName="Sort" CommandArgument="[J].[City]"
                                        Text="City,State" />
                                </th>
                                
                                <th style="width: 8%; white-space: nowrap;">
                                    Revenue
                                </th>                                
                                
                                <th style="width: 8%; white-space: nowrap;">
                                    <asp:LinkButton ID="btnJobType" runat="server" CommandName="Sort" CommandArgument="[J].[JobType]"
                                        Text="Job Type" />
                                </th>
                                
                                <th style="width: 8%; white-space: nowrap;">
                                    <asp:LinkButton ID="btnJobStatus" runat="server" CommandName="Sort" CommandArgument="[J].[JobStatus]"
                                        Text="Status" />
                                </th>                                
                                   <%-- 0.1 ends here --%>
                                <th style="width: 10%; white-space: nowrap;">
                                    Assigned Manager
                                </th>                                
                                <th style="text-align: center; white-space: nowrap; width: 8%;">
                                    Action
                                </th>
                            </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                            <tr class="Pager">
                                <td colspan="13">
                                    <div class="Container">
                                        <asp:DataPager ID="pager" runat="server" PageSize="20">
                                            <Fields>
                                                <asp:TemplatePagerField OnPagerCommand="PagerCommand">
                                                    <PagerTemplate>
                                                        <div class="Command">
                                                            <asp:ImageButton ID="btnFirst" runat="server" CommandName="First" SkinID="sknFirstPage"
                                                                AlternateText="First Page" ToolTip="First Page" />
                                                            <asp:ImageButton ID="btnPrevious" runat="server" CommandName="Previous" SkinID="sknPreviousPage"
                                                                AlternateText="Previous Page" ToolTip="Previous Page" />
                                                        </div>
                                                        <div class="Command">
                                                            <asp:TextBox ID="txtSlider" runat="server" Text='<%# 
                                                                Container.TotalRowCount > 0 
                                                                    ? Math.Ceiling(((double)(Container.StartRowIndex + Container.MaximumRows) / Container.MaximumRows)) 
                                                                    : 0 
                                                                %>' AutoPostBack="true" OnTextChanged="CurrentPageChanged" Style="display: run-in" /> <!--0.3 -->
                                                            <asp:SliderExtender ID="slider" runat="server" TargetControlID="txtSlider" Orientation="Horizontal"
                                                                Minimum="1" Maximum='<%# Math.Ceiling((double)Container.TotalRowCount / Container.MaximumRows) %>'
                                                                TooltipText='<%# "Page {0} of " + Math.Ceiling ((double)Container.TotalRowCount / Container.MaximumRows).ToString() + " (" + Container.TotalRowCount + " items)" %>' />
                                                        </div>
                                                        <div class="Command">
                                                            <asp:ImageButton ID="btnNext" runat="server" CommandName="Next" SkinID="sknNextPage"
                                                                AlternateText="Next Page" ToolTip="Next Page" />
                                                            <asp:ImageButton ID="btnLast" runat="server" CommandName="Last" SkinID="sknLastPage"
                                                                AlternateText="Last Page" ToolTip="Last Page" />
                                                        </div>
                                                        <div class="Info">
                                                            Page <b>
                                                                <%# Container.TotalRowCount > 0 ? Math.Ceiling(((double)(Container.StartRowIndex + Container.MaximumRows) / Container.MaximumRows)) : 0 %>
                                                            </b>of <b>
                                                                <%# Math.Ceiling((double)Container.TotalRowCount / Container.MaximumRows)%>
                                                            </b>(<%# Container.TotalRowCount %>
                                                            items)
                                                        </div>
                                                    </PagerTemplate>
                                                </asp:TemplatePagerField>
                                            </Fields>
                                        </asp:DataPager>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <EmptyDataTemplate>
                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                            <tr>
                                <td>
                                    No data was returned.
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <ItemTemplate>
                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                            <td>
                                <asp:Label ID="lblPostedDate" runat="server" />
                            </td>
                            
                            <td>
                                <asp:Label ID="lblClientName" runat="server" />
                            </td>
                            
                            <td>
                                <asp:HyperLink ID="lnkJobTitle" runat="server" Target="_blank"></asp:HyperLink>
                                <asp:Label ID="lblJobTitle" runat="server" />
                            </td>
                            
                            <td>
                                <asp:Label ID="lblCity" runat="server" />
                            </td>
                            
                            <td>
                                <asp:Label ID="lblRevenue" runat="server" />
                            </td>
                            
                            <td>
                                <asp:Label ID="lblJobType" runat="server" />
                            </td>
                            
                            <td>
                                <asp:Label ID="lblJobStatus" runat="server" />
                            </td>
                            
                            <td>
                                <asp:Label ID="lblAssignedManager" runat="server" />
                            </td>
                            
                            <td style="text-align: center; white-space: nowrap;">
                                <asp:ImageButton ID="btnEdit" SkinID="sknEditButton" runat="server" CommandName="EditItem">
                                </asp:ImageButton>
                                <asp:ImageButton ID="btnCreateNewFromExisting" SkinID="sknCreateNewButton" runat="server" CommandName="CreateNewFromExisting">
                                </asp:ImageButton>
                                <asp:ImageButton ID="btnDelete" SkinID="sknDeleteButton" runat="server" CommandName="DeleteItem">
                                </asp:ImageButton>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>              
            </div>            
       </ContentTemplate>
    </asp:UpdatePanel>
</div>
</asp:Content>
