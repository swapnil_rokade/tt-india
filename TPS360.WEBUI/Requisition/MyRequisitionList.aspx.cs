﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MyRequisitionList.aspx.cs
    Description: This is the page which is used to display the My Requisation list 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    --------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    0.1             Nov-27-2008          Gopala Swamy          Defect Id: 9336; Added one line to get the Code of user using ternary operator and modified functional prototype
    0.2             Dec-03-2008          Yogeesh Bhat          Defect Id: 9396; Changes made in GetJobPostingAssignedManagers() method
    0.3             Dec-11-2008          Yogeesh Bhat          Defect Id: 9503; Changes made in lsvJobPosting_ItemCommand event
    0.4             Dec-16-2008          Yogeesh Bhat          Defect Id: 9525; Added/Removed specified columns in lsvJobPosting listview.
    0.5             Dec-17-2008          Yogeesh Bhat          Defect Id: 9528; Changes made in lsvJobPosting_ItemDataBound(); 
                                                               In Assigning Manager column, showing only requisition creator name
    0.6             Dec-22-2008          Yogeesh Bhat          Defect Id: 9577; Changes made in Page_Load(); "Add Requisition" link is made visible false.
    0.7             Feb-04-2008          Jagadish              Defect ID: 9606; Changes made in method BindList();
 *  0.8             Apr-01-2009          Rajendra Prasad       Defect Id: 9380: Added one new Session.
 *  0.9             May-15-2009          Sandeesh              Defect id:10440 :Changes made to get the Requistion status from database instead of reading from Enum
    1.0             July-01-2009         Yogeesh Bhat          Defect Id: 10827: Changes made in lsvJobPosting_ItemDataBound()
    1.1             Aug-6-2009           Yogeesh Bhat          Defect Id:10958; Changes made in lsvJobPosting_ItemCommand();
 * *1.2             Nov-27-2009          Gopala Swamy J        Defect Id:11588; Changed functions prototype
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------       
*/

using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls; 
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Common;
using TPS360.BusinessFacade;
namespace TPS360.Web.UI.Requisition
{
    public partial class MyRequisitionList : RequisitionBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (CurrentMember != null && CurrentMember.Id > 0)
            {
                string CurrentMemberRole = Facade.GetCustomRoleNameByMemberId(CurrentMember.Id);
                ObjectDataSource odsRequisitionList = (ObjectDataSource)this.uclRequisition.FindControl("odsRequisitionList");
                if (CurrentMemberRole == ContextConstants.ROLE_DEPARTMENT_CONTACT)
                {
                    //odsRequisitionList.SelectParameters["IsCompanyContact"].DefaultValue = "true";
                    //CompanyContact contact = Facade.GetCompanyContactByMemberId(CurrentMember.Id);
                    //odsRequisitionList.SelectParameters["CompanyContactId"].DefaultValue = contact.Id.ToString();
                    odsRequisitionList.SelectParameters["memberId"].DefaultValue = CurrentMember.Id.ToString();
                }
                else
                {

                    odsRequisitionList.SelectParameters["memberId"].DefaultValue = CurrentMember.Id.ToString();
                }
            }
            else
            {
                Response.Redirect("..//Login.aspx?SeOut=SessionOut",true);
            }
        }

        [System.Web.Services.WebMethod]
        public static object[] ChangeRequisitionStatus(object data)
        {
            IFacade Facade = new Facade();
            Dictionary<string, object> param =
                  (Dictionary<string, object>)data;
            try
            {
                JobPosting jobPosting = Facade.GetJobPostingById(Convert.ToInt32(param["RequisitionId"]));
                jobPosting.JobStatus = Convert.ToInt32(param["StatusId"]);
                jobPosting.UpdatorId = Convert.ToInt32(param["UpdatorId"]);
                ApplicationWorkflow applicationWorkflow = Facade.GetApplicationWorkflowByTitle(WorkFlowTitle.RequisitionManagementShouldSelectedApprovedUsersAbilityToChangeStats.ToString());
                if (applicationWorkflow != null && applicationWorkflow.RequireApproval)
                {
                    IList<ApplicationWorkflowMap> applicationWorkflowMapList = Facade.GetAllApplicationWorkflowMapByApplicationWorkflowId(applicationWorkflow.Id);
                    if (applicationWorkflowMapList != null && applicationWorkflowMapList.Count > 0)
                    {
                        foreach (ApplicationWorkflowMap applicationWorkflowMap in applicationWorkflowMapList)
                        {
                            if (applicationWorkflowMap.MemberId == Convert.ToInt32(param["UpdatorId"]))
                            {
                                Facade.UpdateJobPosting(jobPosting);
                                MiscUtil.EventLogForRequisition(EventLogForRequisition.RequisitionStatusChange, jobPosting.Id, Convert.ToInt32(param["UpdatorId"]), Facade);
                                break;
                            }
                        }
                    }
                }
                else
                {
                    Facade.UpdateJobPosting(jobPosting);
                    MiscUtil.EventLogForRequisition(EventLogForRequisition.RequisitionStatusChange, jobPosting.Id, Convert.ToInt32(param["UpdatorId"]), Facade);
                    string[] result = { "Changed", "" };
                    return result;
                }
            }
            catch (ArgumentException ax)
            {

            }
            string[] result1 = { "Changed", "" };
            return result1;
        }

    }
}
