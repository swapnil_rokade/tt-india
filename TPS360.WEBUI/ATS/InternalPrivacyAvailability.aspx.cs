﻿using System;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using System.Web.UI.WebControls;
namespace TPS360.Web.UI
{
    public partial class CandidateInternalPrivacyAvailability : CandidateBasePage
    {
        #region Member Variables
        private int _memberId = 0;
        #endregion

        #region Methods

        private void GetMemberId()
        {
            //From Member Profile
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                hfMemberId.Value = Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID];

            }
            //From Member Login
            else
            {
                hfMemberId.Value = base.CurrentMember.Id.ToString();
            }
            _memberId = Int32.Parse(hfMemberId.Value);
        }

        #endregion

        #region Events

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            _memberId = Int32.Parse(hfMemberId.Value);
            GetMemberId();
            MemberManager Manager = Facade.GetMemberManagerByMemberIdAndManagerId(_memberId, CurrentMember.Id);
            if (Manager == null && !IsUserAdmin && !IsUserVendor )
                Helper.Url.Redirect(UrlConstants.Dashboard.DEFAULT_PAGE);
            if (!IsPostBack)
            {
                if (_memberId > 0)
                {
                    string name = MiscUtil.GetMemberNameById(_memberId, Facade);
                    Label lbModalTitle = (Label)this.Master.FindControl("lbModalTitle");
                    if (lbModalTitle != null) lbModalTitle.Text = name + " - " + "Availability";
                    
                    //this.Page.Title = name + " - " + " Availability";
                }
                hdnPageTitle.Value = Page.Title;
            }
            Page.Title = hdnPageTitle.Value;
            ucntrlPrivacyAvailability.MemberId = _memberId;
        }

        #endregion

        #endregion
    }
}