﻿using System;

using TPS360.Common.Helper;

namespace TPS360.Web.UI
{
    public partial class CandidateInternalInterviewSchedule : CandidateBasePage
    {
        #region Member Variables

        private static int _memberId = 0;
        
        #endregion

        #region Methods

        private void GetMemberId()
        {
            //From Member Profile
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                _memberId = Int32.Parse(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
            }
            //From Member Login
            else
            {
                _memberId = base.CurrentMember.Id;
            }
        }

        #endregion
        
        #region Events

        #region Page event

        protected void Page_Load(object sender, EventArgs e)
        {
            try 
            {
               

                if (!IsPostBack)
                {
                    GetMemberId();
                    if (_memberId > 0)
                    {
                        string name = MiscUtil.GetMemberNameById(_memberId, Facade);
                        this.Page.Title = name + " - " + "Interviews";
                    }
                }
                ucntrlMemberInterview.MemberId = _memberId;
            }
            catch 
            { 
            
            }
            


        }

        #endregion

        #endregion
    }
}