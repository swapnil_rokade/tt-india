﻿<%@ Page Language="C#" MasterPageFile="~/Masters/CandidateProfile.master" AutoEventWireup="true" CodeFile="AssignedManager.aspx.cs"
    Inherits="TPS360.Web.UI.CandidateList" Title="Candidate Assigned Managers" %>
<%@ Register Src="~/Controls/MemberAssignedManager.ascx" TagName="AssignedManager" TagPrefix="uc1" %>

<asp:Content ID="cntCandidateListBody" ContentPlaceHolderID="cphCandidateMaster"
    runat="Server">
      <asp:HiddenField ID="hdnPageTitle" runat ="server" />
      <asp:HiddenField ID="hfMemberId" runat="server" Value="0"/>
     <asp:UpdatePanel ID="pnlAssignedManagerList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <uc1:AssignedManager ID="ucntrlAssignedManagerList" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>