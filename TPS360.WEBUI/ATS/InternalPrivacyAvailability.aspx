﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: InternalPrivacyAvailability.aspx
    Description: This is the page used to update availability.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              Jan-13-2009          Jagadish        Defect id: 9691; Changed 'Applicant Privacy, Sharing & Availability' to 'Candidate Availability'
                                                                            and removed the tab 'Share My Candidate'.
   -------------------------------------------------------------------------------------------------------------------------------------------       
--%>
<%@ Page Language="C#" MasterPageFile="~/Modals/Modal.master" AutoEventWireup="true" CodeFile="InternalPrivacyAvailability.aspx.cs"
    Inherits="TPS360.Web.UI.CandidateInternalPrivacyAvailability"  ValidateRequest="false" Title="Applicant Availability" %>

<%@ Register Src="~/Controls/MemberPrivacyAvailability.ascx" TagName="PrivacyAvailability" TagPrefix="uc1" %>

<asp:Content ID="cntCandidatePrivacyAvailability" ContentPlaceHolderID="cphModalMaster"
    runat="Server">
    
    <div  style =" width : 475px; height : 330px; max-height : 330px ; overflow :auto ; display : table-cell ; vertical-align :50px; " align="center"  >
                 <asp:HiddenField ID="hdnPageTitle" runat ="server" />
                 <asp:HiddenField ID="hfMemberId" runat="server" Value="0"/>
                 <uc1:PrivacyAvailability id="ucntrlPrivacyAvailability" runat="server"  />
                 </div> 
                          
</asp:Content>
