﻿using System;

namespace TPS360.Web.UI
{
    public partial class CandidateRegistrationInfoEditor : CandidateBasePage
    {
        #region Veriables

        #endregion

        #region Properties


        #endregion

        #region Methods



        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            rgInternal.Role = ContextConstants.ROLE_CANDIDATE;
        }

        #endregion
    }
}