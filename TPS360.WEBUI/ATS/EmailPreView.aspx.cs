﻿using System;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Common.BusinessEntities;
using System.Web;
using System.Web.UI.WebControls;
namespace TPS360.Web.UI.ATS
{
    public partial class Candidate_EmailPreView : CandidateBasePage
    {
        #region Variable

        private int _mailId = 0;

        #endregion

        #region Properties

        #endregion

        #region Methods

        #endregion

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Helper.Url.SecureUrl[UrlConstants.PARAM_ID]))
                {
                    _mailId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_ID]);

                    if (_mailId > 0)
                    {
                        MemberEmail memberEmail = Facade.GetMemberEmailById(_mailId);

                        if (memberEmail != null)
                        {
                            DateAndTime.InnerHtml = ": " + memberEmail.SentDate.ToString();
                            From.InnerHtml = ": " + memberEmail.SenderEmail.ToString();
                            To.InnerHtml = ": " + memberEmail.ReceiverEmail.ToString();
                            if (memberEmail.CC.ToString().Trim() != "")
                                CC.InnerHtml = ": " + memberEmail.CC.ToString();
                            else
                            {
                                tr1.Visible = false;
                            }
                            if (memberEmail.BCC.ToString().Trim() != "")
                                BCC.InnerHtml = ": " + memberEmail.BCC.ToString();
                            else
                            {
                                tr2.Visible = false;
                            }
                               int noofAtt = memberEmail.NoOfAttachedFiles;
                                string[] attarray = memberEmail.AttachedFileNames.Split(';');
                                string display = "";
                                string s = "";
                                for (int i = 0; i < noofAtt; i++)
                                {
                                   Label att = new Label();
                                   string DisplayResume = string.Empty;
                                   if (display == string.Empty)
                                       display = ": ";
                                   if (attarray[i].Contains("$$$"))
                                   {
                                       string[] filen = attarray[i].Split(new string[] { "$$$" }, StringSplitOptions.RemoveEmptyEntries);
                                       s = "Resources/Member/" + memberEmail.SenderId + "/Outlook%20Sync/" + memberEmail.Id + "/" + attarray[i].Replace(" ", "%20") + "/" + filen[0].Replace(" ", "%20");
                                       DisplayResume = filen[0];
                                   }
                                   else
                                   {
                                       s = "Resources/Member/" + memberEmail.SenderId + "/Outlook%20Sync/" + memberEmail.Id + "/" + attarray[i].Replace(" ", "%20");
                                       DisplayResume = attarray[i];
                                   }
                                   string path = UrlConstants.ApplicationBaseUrl + s;
                                   SecureUrl url = UrlHelper.BuildSecureUrl(path, string.Empty);
                                   display += "<a href=" + path + ">" +  DisplayResume  + "</a>       ;       ";
                                }
                                dd.InnerHtml = display;
                           if (memberEmail.NoOfAttachedFiles ==0)
                                {
                                    tr3.Visible = false;
                                }
                            Subject.InnerHtml = ": " + memberEmail.Subject.ToString();
                            EmailDescription.InnerHtml =": "+ memberEmail.EmailBody;
                        }
                    }
                }
                else
                {
                    Subject.InnerHtml = "sorry no text to show!!!";
                    EmailDescription.InnerHtml = "sorry no text to show!!!";
                }
            }
        }

        #endregion
    }
}