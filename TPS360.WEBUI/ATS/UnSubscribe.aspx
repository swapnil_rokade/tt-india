﻿<%@ Page Language="C#" MasterPageFile="~/Public.master" AutoEventWireup="true" CodeFile="UnSubscribe.aspx.cs" Inherits="TPS360.Web.UI.UnSubscribe" Title="Unsubscribe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHomeMasterTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHomeMaster" Runat="Server">
   <div class="MidBodyBoxRow">
        <div class="MainBox" style="width: 55%; height:10px">
        
            <div class="MasterBoxContainer">
                <div class="ChildBoxContainer">
                   <br />
                    <div class="BoxContainer">   <div class="sectionTitle" style =" padding-bottom : 20px">UnSubscribe</div>
                    
                      <asp:Label ID ="lblConfirmHeading" runat ="server" Width ="95%" Font-Bold ="true" Text ="  Your email <candidate email> is now unsubscribed from our mailing list." ></asp:Label>
                    </div>
                    <asp:Image ID="imgRightTopImage" runat="server" SkinID="sknRightTopImage" AlternateText="" /><asp:Image
                        ID="imgBoxLeftBottom" runat="server" SkinID="sknBoxLeftBottom" AlternateText="" /><asp:Image
                            ID="imgBoxRightBottom" runat="server" SkinID="sknBoxRightBottom" AlternateText="" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>

