﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: InternalCandidateHotLists.aspx.cs
    Description: This is the page which is used as parent page for hotlist in ATS.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Nov-25-2008          Gopala Swamy          Defect id: 9243 ; Put two lines to carry "member role" and "group type" to child page 
    0.2             Nov-27-2008          Jagadish              Defect id: 9111; Replaced the two lines that carry "member role" and "group type" to child page to
                                                               outside of if(!IsPostBack) condition.
-------------------------------------------------------------------------------------------------------------------------------------------       
*/




using System;

using TPS360.Common.Helper;

namespace TPS360.Web.UI
{
    public partial class InternalCandidateHotLists : CandidateBasePage
    {
        #region Member Variables

        int _memberId = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            ucntrlCandidateHotList.GroupType = Convert.ToInt32(TPS360.Common.Shared.MemberGroupType.Candidate);
            ucntrlCandidateHotList.MemberRole = ContextConstants.ROLE_CANDIDATE;
            if (!IsPostBack)
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
                {
                    _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
                }
                if (_memberId > 0)
                {
                    string name = MiscUtil.GetMemberNameById(_memberId, Facade);
                    this.Page.Title = name + " - " + "Hot List";
                }
                hdnPageTitle.Value = Page.Title;
            }
            Page.Title = hdnPageTitle.Value;
        }
        #endregion
    }
}