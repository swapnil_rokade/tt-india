﻿using System;

using TPS360.Common.Helper;

namespace TPS360.Web.UI
{
    public partial class MasterCandidateList : CandidateBasePage
    {

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["PreciseSearch"] != null)
                    Session["PreciseSearch"] = null;
            }
        }

        #endregion
    }
}