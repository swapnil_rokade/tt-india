﻿ <%------------------------------------------------------------------------------------------------------------------------------------
    FileName: MyCandidateList.aspx
    Description: 
    Created By: 
    Created On:
    Modification Log:
    -----------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date           Author            Modification
    -----------------------------------------------------------------------------------------------------------------------------------
    0.1            Nov-18-2009          Sandeesh          Enahncement#11814 :AM Notification - When a recruiter adds a candidate to a req the app will send a notification email to the Account Manager
---------------------------------------------------------------------------------------------------------------------------------------

--%>
<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true"  Async="true" CodeFile="MyCandidateList.aspx.cs" 
    Inherits="TPS360.Web.UI.MyCandidateList" Title="My Candidate List" EnableEventValidation ="false"  %>
<%@ Register Src="~/Controls/CandidateList.ascx" TagName="CandidateList" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="cntMemberCandidateListHeader" ContentPlaceHolderID="head"
    runat="Server">
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHomeMasterTitle"
    runat="Server">
    My Candidate List
</asp:Content>
<asp:Content ID="cntCandidateListBody" ContentPlaceHolderID="cphHomeMaster"
    runat="Server">
        <script src="../Scripts/jsUpdateProgress.js" type="text/javascript"></script>
<script type ="text/javascript" >
var ModalProgress ='<%= Modal.ClientID %>';
</script>
    <div style="text-align: left; width: 100%;">
        <asp:HiddenField ID="hfMemberId" runat="server" Value="0"/>
        <uc1:CandidateList ID="ucntrlCandidateList" IsMyList="true" HeaderTitle="My Candidates List" runat="server" />
    </div>
       <asp:Panel ID="pnlmodal" runat="server" style="display: none">
                    <asp:UpdatePanel runat="server" ID="UpdatePanel2" RenderMode="Inline" UpdateMode="Conditional">
                        <ContentTemplate>
                        <img src="../Images/AjaxLoading.gif" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID ="Modal" runat ="server" TargetControlID ="pnlmodal" PopupControlID ="pnlmodal"
             BackgroundCssClass ="divModalBackground" ></ajaxToolkit:ModalPopupExtender> 
</asp:Content>