﻿using System;
using TPS360.Common.Shared;
namespace TPS360.Web.UI
{
    public partial class CandidateMasterHotList : CandidateBasePage
    {
        #region Member Variables        

        #endregion
        
        #region Properties      

        #endregion
        
        #region Methods
        
        #endregion

        #region Events

        protected void Page_Init(object sender, EventArgs e)
        {
            uclMasterHotList.GroupType = Convert.ToInt32(MemberGroupType.Candidate);
        }

        #endregion
    }
      
}