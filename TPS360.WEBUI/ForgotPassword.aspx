﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: Login.aspx
    Description: Login Page
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              Dec-19-2008          Gopala Swamy J        Defect id:9320 Put one new javascript function called "HandleFailureText()" and modified the required field validator
    0.2              Jul-16-2009           Veda                 Defect id:10947 Blank white space below the 'Login In' button has been removed
-------------------------------------------------------------------------------------------------------------------------------------------       

--%>

<%@ Page Language="C#" MasterPageFile="~/Public.master" AutoEventWireup="true" CodeFile="ForgotPassword.aspx.cs"
    Inherits="TPS360.Web.UI.ForgotPassword" Title="Forgot Password" %>
<%@ Register Src="~/Controls/ForgotPassword.ascx" TagName="forgotpassword" TagPrefix="ucl"%>
<asp:Content ID="cDefault" ContentPlaceHolderID="cphHomeMaster" runat="Server">
<ucl:forgotpassword runat="server" ID="uclForgotPassword" />
    </asp:Content> 