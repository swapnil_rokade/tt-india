﻿<%@ WebService Language="C#" Class="ExcelValueInsertion" %>

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Security;
using System.Data;
using TPS360.BusinessFacade;
using TPS360.DataAccess;
using TPS360.Controls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System.Web.Security;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Text.RegularExpressions;
using System.Data;
using System.Windows.Forms;
using System.Linq;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class ExcelValueInsertion  : System.Web.Services.WebService {

    [WebMethod]
    public bool IsConnect()
    {
        return true;
    }

    static int DefaultCreatorandUpdatorId = 2;
    static IList<CustomRole> CustomRoleList;

    #region HiringMatrix
    [WebMethod]
    public void InsertHiringMatrix(byte[] list)
    {
        DataTable dt = new DataTable();
        try
        {
            dt = ConvertByteArrayToDataTable(list);
        }
        catch
        {
        }
        int i = 0;
        if (dt.Rows.Count > 0)
        {
            _Facade.DeleteHiringMatrixLevels();
        }
        foreach (DataRow item in dt.Rows)
        {
            HiringMatrixLevels hiringMatrixLevels = new HiringMatrixLevels();
            hiringMatrixLevels.Name = item["LevelName"].ToString();
            hiringMatrixLevels.SortingOrder = i;
            hiringMatrixLevels.CreatorId = DefaultCreatorandUpdatorId;
            hiringMatrixLevels.UpdatorId = DefaultCreatorandUpdatorId;
            _Facade.AddHiringMatrixLevels(hiringMatrixLevels);
            i++;
        }
    }
    #endregion

    [WebMethod]
    public void InsertUserList(byte[] list)
    {
        DataTable dt = new DataTable();
        dt = ConvertByteArrayToDataTable(list);
        CustomRoleList = _Facade.GetAllCustomRole();
        foreach (DataRow item in dt.Rows)
        {
            SaveUsers(item["FirstName"].ToString(), item["LastName"].ToString(), item["EmailAddress"].ToString(), item["AccessRole"].ToString());
        }
    }

    void SaveUsers(string _firstName, string _lastname, string _Email, string _Role)
    {
        string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" + @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
        Regex re = new Regex(strRegex);
        if (!(re.IsMatch(_Email)))
        {
            return;
        }
        CheckMemberAvailability(_Email);
        MembershipUser user = Membership.GetUser(_Email);

        if (user == null)
        {
            RegisterUser(_Email, _firstName, _lastname, _Role);
        }
    }

    protected IFacade _Facade
    {
        get
        {
            IFacade facade = new Facade();
            return facade;
        }
    }

    private void CheckMemberAvailability(string Email)
    {
        MembershipUser user = Membership.GetUser(Email);
       // _Facade.DeleteMemberById(8362);
        if (user != null)
        {
            Member mem = _Facade.GetMemberByMemberEmail(Email);
            if (mem == null)
            {
                Membership.DeleteUser(Email);
            }
        }
    }
    
    private void RegisterUser(string _Email, string _firstname, string _lastname, string _RoleName)
    {
        TPS360.Web.UI.Helper.SecureUrl url;
        string SecureURL;
        string _role = TPS360.Web.UI.ContextConstants.ROLE_EMPLOYEE;
        try
        {

            MembershipUser newUser;
            newUser = Membership.CreateUser(_Email, "changeme", _Email);
            if (newUser.IsApproved == true)
            {
                Roles.AddUserToRole(newUser.UserName, _role);
                Member newMember = new Member();
                newMember.UserId = (Guid)newUser.ProviderUserKey;
                newMember.ResumeSource = Convert.ToInt32(ResumeSource.SelfRegistration);
                newMember.PermanentCountryId = 0;
                newMember.PermanentStateId = 0;
                newMember.IsRemoved = false;
                newMember.AutomatedEmailStatus = true;
                newMember.FirstName = _firstname;
                newMember.LastName = _lastname;
                newMember.DateOfBirth = DateTime.MinValue;
                newMember.PrimaryEmail = newUser.Email;
                newMember.Status = (int)MemberStatus.Active;
                newMember.ResumeSource = (int)ResumeSource.Admin;
                newMember.CreatorId = DefaultCreatorandUpdatorId;
                newMember.CreateDate = DateTime.Now;
                newMember.UpdatorId = DefaultCreatorandUpdatorId;
                newMember.UpdateDate = DateTime.Now;
                newMember = _Facade.AddMember(newMember);

                Resource newResource = new Resource();
                newResource.EmailAddress = newMember.PrimaryEmail;
                newResource.ResourceName = newMember.Id.ToString();
                _Facade.AddResource(newResource);


                MemberDetail newMemberDetail = new MemberDetail();
                newMemberDetail.MemberId = newMember.Id;
                newMemberDetail.CurrentStateId = 0;
                newMemberDetail.CurrentCountryId = 0;
                newMemberDetail.GenderLookupId = 0;
                newMemberDetail.EthnicGroupLookupId = 0;
                newMemberDetail.MaritalStatusLookupId = 0;
                newMemberDetail.NumberOfChildren = 0;
                newMemberDetail.BloodGroupLookupId = 0;
                newMemberDetail.AnniversaryDate = DateTime.MinValue;
                _Facade.AddMemberDetail(newMemberDetail);

                //--Trace
                MemberExtendedInformation memberExtended = new MemberExtendedInformation();
                memberExtended.MemberId = newMember.Id;
                memberExtended.Availability = TPS360.Web.UI.ContextConstants.MEMBER_DEFAULT_AVAILABILITY;
                _Facade.AddMemberExtendedInformation(memberExtended);

                MemberObjectiveAndSummary m_memberObjectiveAndSummary = new MemberObjectiveAndSummary();
                m_memberObjectiveAndSummary.CreatorId = DefaultCreatorandUpdatorId;
                m_memberObjectiveAndSummary.UpdatorId = DefaultCreatorandUpdatorId;
                m_memberObjectiveAndSummary.CopyPasteResume = "";
                m_memberObjectiveAndSummary.CreateDate = DateTime.Today;
                m_memberObjectiveAndSummary.UpdateDate = DateTime.Today;
                m_memberObjectiveAndSummary.RawCopyPasteResume = "";
                m_memberObjectiveAndSummary.MemberId = newMember.Id;
                _Facade.AddMemberObjectiveAndSummary(m_memberObjectiveAndSummary);


                //Add to self as manager                            

                MemberManager memberManager = new MemberManager();
                memberManager.IsPrimaryManager = true;
                memberManager.ManagerId = DefaultCreatorandUpdatorId;
                memberManager.MemberId = newMember.Id;
                memberManager.CreateDate = DateTime.Now;
                memberManager.CreatorId = DefaultCreatorandUpdatorId;
                memberManager.UpdateDate = DateTime.Now;
                memberManager.UpdatorId = DefaultCreatorandUpdatorId;
                _Facade.AddMemberManager(memberManager);

                TPS360.Web.UI.MiscUtil.AddActivity(_role, 0, DefaultCreatorandUpdatorId, ActivityType.AddMember, _Facade);
                string countryName = string.Empty;
                //
                if (_role.ToLower() == TPS360.Web.UI.ContextConstants.ROLE_EMPLOYEE.ToLower())
                {
                    MemberCustomRoleMap memberRolemap = new MemberCustomRoleMap();
                    IList<CustomRole> roleList = _Facade.GetAllCustomRole();
                    int _intcustrole = 0;
                    foreach (CustomRole cr in roleList)
                    {
                        if (cr.Name == "Employee")
                        {
                            _intcustrole = cr.Id;
                            break;
                        }
                    }
                    AddDefaultMenuAccess(_intcustrole, newMember.Id);
                    LoadUserPageSetup(newMember.Id);
                    AssignRole(newMember.Id, _RoleName);
                   //
                }
            }
        }
        catch (MembershipCreateUserException ex)
        {
            switch (ex.StatusCode)
            {
            }
        }

    }
    private void AddDefaultMenuAccess(int roleId, int memberId)
    {

        ArrayList previlegeList = _Facade.GetAllCustomRolePrivilegeIdsByRoleId(roleId);

        if (previlegeList != null && previlegeList.Count > 0)
        {
            _Facade.DeleteMemberPrivilegeByMemberId(memberId);

            for (int i = 0; i <= previlegeList.Count - 1; i++)
            {
                MemberPrivilege previlege = new MemberPrivilege();

                previlege.CustomSiteMapId = Convert.ToInt32(previlegeList[i]);
                previlege.MemberId = memberId;
                _Facade.AddMemberPrivilege(previlege);
            }
        }

    }
    private UserPageSetup _Setup
    {
        get { return Context.Items[typeof(UserPageSetup)] as UserPageSetup; }
        set { Context.Items[typeof(UserPageSetup)] = value; }
    }
    
    private void LoadUserPageSetup(int NewMemberId)
    {
        UserPageDataAccess upda = new UserPageDataAccess();
        int pageCount = upda.GetUserPageCount(NewMemberId);

        if (pageCount == 0)
        {
            //new user. Create page for him.

            UserPage page = new UserPage();
            page.Title = "Dashboard";
            page.UserId = NewMemberId;
            upda.AddUserPage(page);

            //set the current page
            UserPageSetting userPageSetting = new UserPageSetting();
            userPageSetting.CurrentPageId = page.Id;
            userPageSetting.UserId = NewMemberId;
            new UserPageSettingDataAccess().AddUserPageSetting(userPageSetting);

            List<Widget> defaultWidgetList = new WidgetDataAccess().GetAllWidget(true);

            if (defaultWidgetList != null && defaultWidgetList.Count > 0)
            {
                _Setup = new UserPageSetup(NewMemberId);
                //add widget to instance in a page
                foreach (Widget widget in defaultWidgetList)
                {
                    WidgetInstance wi = new WidgetInstance();
                    wi.Title = widget.Name;
                    wi.PageId = _Setup.CurrentPage.Id;
                    wi.State = string.Empty;
                    wi.WidgetId = widget.Id;
                    wi.Expanded = true;
                    wi.State = widget.DefaultState;

                    WidgetInstanceDataAccess wida = new WidgetInstanceDataAccess();
                    wida.AddWidgetInstance(wi);
                    wida.UpdateWidgetInstancePosition(_Setup.CurrentPage.Id);
                }
            }
        }
    }

    private void AssignRole(int _memberId, string role)
    {
        if (role.ToLower() == TPS360.Web.UI.ContextConstants.ROLE_ADMIN.ToLower())
        {
            string strUserName = string.Empty;
            if (_memberId > 0)
            {
                strUserName = _Facade.GetMemberUserNameById(_memberId);
                if (!Roles.IsUserInRole(strUserName, TPS360.Web.UI.ContextConstants.ROLE_ADMIN))
                    Roles.AddUserToRole(strUserName, TPS360.Web.UI.ContextConstants.ROLE_ADMIN);
            }
        }
        else
        {
            if (_memberId > 0)
            {
                string strUserName = _Facade.GetMemberUserNameById(_memberId);
                if (Roles.IsUserInRole(strUserName, TPS360.Web.UI.ContextConstants.ROLE_ADMIN))
                    Roles.RemoveUserFromRole(strUserName, TPS360.Web.UI.ContextConstants.ROLE_ADMIN);


            }

        }

        MemberCustomRoleMap map = _Facade.GetMemberCustomRoleMapByMemberId(_memberId);

        int roleId=0;
        if (CustomRoleList != null && CustomRoleList.Count > 0)
        {
            var lis = CustomRoleList.Where(x => x.Name.Trim() == role.Trim());
            if (lis.Count() > 0)
            {
                roleId = lis.ToList()[0].Id;
            }
        }

        if (map == null)
        {
            map = new MemberCustomRoleMap();
            map.CustomRoleId = roleId;
            map.MemberId = _memberId;
            map.CreatorId = DefaultCreatorandUpdatorId;

            _Facade.AddMemberCustomRoleMap(map);
            AddDefaultMenuAccess(roleId, _memberId);
        }
        else if (map.CustomRoleId != roleId)
        {
            map.CustomRoleId = roleId;
            map.UpdatorId = _memberId;
            _Facade.UpdateMemberCustomRoleMap(map);
            AddDefaultMenuAccess(roleId, _memberId);
        }
    }

    [WebMethod]
    public void InsertRolesList(byte[] list)
    {
        _Facade.DeleteAllMemberPrivilige();
        _Facade.DeleteAllCustomRolePrivilege();
        DataTable dt = new DataTable();
        dt = ConvertByteArrayToDataTable(list);
        if (dt.Rows.Count > 0)
        {
            DataTable x = dt.DefaultView.ToTable(true, "AllowedRoles");
            CustomRoleList = _Facade.GetAllCustomRole();
            foreach ( DataRow row in x.Rows )
            {
                DataRow[] Groups = dt.Select(" AllowedRoles ='" + row[0].ToString().ToLower() + "'");
                var roleId=from a in CustomRoleList where a.Name.ToLower()==row[0].ToString().ToLower() select a.Id;
                if(roleId .Count() >0)
                {
                    int intRoleId=Convert.ToInt32(roleId.ToList()[0]);
                    if ( intRoleId  > 0)
                    {
                        IList<Member> memberList = _Facade.GetAllMemberByCustomrRoleId(intRoleId);
                        foreach (DataRow dr in Groups)
                        {
                            if (memberList != null)
                            {
                                
                                foreach (Member m in memberList)
                                {
                                    MemberPrivilege memberPrivilege = new MemberPrivilege();
                                    memberPrivilege.CustomSiteMapId = Convert.ToInt32(dr[0]);
                                    memberPrivilege.MemberId = m.Id;
                                    _Facade.AddMemberPrivilege(memberPrivilege);
                                }
                            }
                            SaveMenuAccess(intRoleId, Convert.ToInt32(dr[0]));
                            InsertParentMenus(Convert.ToInt32(dr[0]), memberList, intRoleId);
                        }
                    }

                }
                
            }
        }
    }

    private void InsertParentMenus(int CustomSiteMapId, IList<Member> memberList,int intRoleId)
    {
        CustomSiteMap cus = _Facade.GetCustomSiteMapById(CustomSiteMapId);
        if (cus.ParentId > 0)
        {

            CustomSiteMap cusp = _Facade.GetCustomSiteMapById(cus.ParentId);
            if (cusp != null)
            {
                if (memberList != null)
                {

                    foreach (Member m in memberList)
                    {
                        MemberPrivilege memberPrivilege = new MemberPrivilege();
                        memberPrivilege.CustomSiteMapId = cusp.Id ;
                        memberPrivilege.MemberId = m.Id;
                        _Facade.AddMemberPrivilege(memberPrivilege);
                    }
                }
            }
            SaveMenuAccess(intRoleId, CustomSiteMapId);
            InsertParentMenus(cusp.Id, memberList, intRoleId);
        }
    }
    private void SaveMenuAccess(int CustomRoleId, int CustomSiteMapId)
    {

            CustomRolePrivilege customRolePrivilege = new CustomRolePrivilege();
            customRolePrivilege.CustomSiteMapId = CustomSiteMapId;
            customRolePrivilege.CustomRoleId = CustomRoleId;
           _Facade.AddCustomRolePrivilege(customRolePrivilege);
    }

    public static DataTable ConvertByteArrayToDataTable(byte[] byteArray)
    {
        DataTable Result = new DataTable();
        BinaryFormatter bf = new BinaryFormatter();
        using (MemoryStream ms = new MemoryStream(byteArray))
        {
            Result = (DataTable)bf.Deserialize(ms);
        }
        return Result;
    }

    
}

