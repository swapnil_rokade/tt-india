﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: Productivity.aspx
    Description: DashBoard / Productivity Page
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              Dec-14-2009          Basavaraj A        Defect id:11991 Cosmetic Changes. Blank space is displayed towards left side and bottom of the grid
    0.2              Jan-04-2010          Srividya s         Defect id:10915 Cosmetic Changes.Userinterface Pagination links move to Right hand side and removed blue line.
 -------------------------------------------------------------------------------------------------------------------------------------------       

--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="cntrlProductivity.ascx.cs"
    Inherits="cntrlProductivity" %>
<%@ Register Src="~/Controls/DateRangePicker.ascx" TagName="DateRange" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/SmallPagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<asp:Panel ID="pnlSettings" runat="server">
</asp:Panel>
<style type="text/css">
    .AspNet-GridView-Pagination
    {
        padding: 0;
        margin: 0;
        border-bottom: 0;
        clear: both;
        letter-spacing: normal;
        border-bottom-style: none;
    }
</style>

<script type="text/javascript">
Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoadedReq); 
function PageLoadedReq(sender,args)
{
    try
    { 
        var hdnScroll=document .getElementById ('<%=hdnScrollPos.ClientID%>');
       
        var bigDiv = document .getElementById ('bigDiv' );
        bigDiv.scrollLeft = hdnScroll.value;
    }
    catch (e)
    {
    }
}

function SetScrollPosition() 
{
    var hdnScroll=document .getElementById ('<%=hdnScrollPos.ClientID%>');

    var bigDiv = document .getElementById ('bigDiv' );
    hdnScroll.value = bigDiv.scrollLeft ;  
}
</script>

<asp:Panel ID="widgetBody" runat="server" EnableViewState="true">
    <asp:UpdatePanel ID="upProd" runat="server">
        <ContentTemplate>
            <div class="TableRow" style="display: table; margin-bottom: 5px;">
                <ucl:DateRange ID="dtRange" runat="server" />
                <asp:ImageButton ID="imgSearch" ImageUrl="~/Images/search-icon.png" runat="server"
                    Width="25px" Height="25px" Style="cursor: pointer; margin: 0px" ToolTip ="Apply Search Filter" OnClick ="btnSearch_Click" />
            </div>
            <div>
                <div>
                    <asp:HiddenField ID="hdnProductivity" runat="server" Value="My" />
                </div>
                <br />
                <asp:ObjectDataSource ID="odsProductivity" runat="server" SelectMethod="GetPagedEmployeeProductivity"
                    TypeName="TPS360.Web.UI.EmployeeDataSource" SelectCountMethod="GetListCountEmployeeProductivity"
                    EnablePaging="True" SortParameterName="sortExpression">
                    <SelectParameters>
                         <asp:ControlParameter ControlID ="dtRange" Type ="DateTime" Name ="StartDate" PropertyName ="StartDate" />
                         <asp:ControlParameter ControlID ="dtRange" Type ="DateTime" Name ="EndDate" PropertyName ="EndDate" />
                        <asp:Parameter Name="MemberId" DefaultValue="0" />
                        <asp:Parameter Name="SortOrder" DefaultValue="" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <div style="overflow: auto; overflow-y: hidden; width: 100%; text-align: left;" id="bigDiv"
                    onscroll='SetScrollPosition()'>
                    <div style="padding-bottom: 18px">
                        <asp:HiddenField ID="hdnSortColumn" runat="server" />
                        <asp:HiddenField ID="hdnSortOrder" runat="server" />
                        <asp:HiddenField ID="hdnScrollPos" runat="server" />
                        <asp:TextBox ID="SortColumn" runat="server" EnableViewState="true" Visible="false" />
                        <asp:TextBox ID="SortOrder" runat="server" EnableViewState="true" Visible="false" />
                        <asp:ListView ID="lsvProductivity" runat="server" EnableViewState="true" DataKeyNames="ID"
                            DataSourceID="odsProductivity" OnItemDataBound="lsvProductivity_ItemDataBound"
                            OnItemCommand="lsvProductivity_ItemCommand" OnPreRender="lsvProductivity_PreRender">
                            <LayoutTemplate>
                                <table id="tlbTemplate" runat="server" border="0" cellspacing="0" class="Grid ReportGrid">
                                    <tr>
                                        <th style="min-width: 100px; max-width: 100px;" enableviewstate="false">
                                            <asp:LinkButton ID="lnkEmpName" runat="server" Text="Employee" CommandArgument="[M].[FirstName]"
                                                CommandName="Sort"></asp:LinkButton>
                                        </th>
                                        <th style="min-width: 120px" enableviewstate="false">
                                            <asp:LinkButton ID="lnkReqCount" runat="server" Text="Requisitions" CommandName="Sort"
                                                CommandArgument="JobCount"></asp:LinkButton>
                                        </th>
                                        <th style="min-width: 120px" id="thLevels" runat="server" enableviewstate="false">
                                        </th>
                                      <%--  <th style="min-width: 120px" enableviewstate="false">
                                            <asp:LinkButton ID="LinkButton1" runat="server" Text="Submissions" CommandName="Sort"
                                                CommandArgument="Submissioncount"></asp:LinkButton>
                                        </th>
                                        <th style="min-width: 120px" enableviewstate="false">
                                            <asp:LinkButton ID="LinkButton2" runat="server" Text="New Accounts" CommandName="Sort"
                                                CommandArgument="ClientCount"></asp:LinkButton>
                                        </th>--%>
                                    </tr>
                                    <tr id="itemPlaceholder" runat="server">
                                    </tr>
                                    <tr class="Pager" style="width: 97%">
                                        <td colspan="4" id="tdPager" runat="server">
                                            <ucl:Pager ID="pagerControl" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </LayoutTemplate>
                            <EmptyDataTemplate>
                                <table id="tblEmptyData" runat="server" class="EmptyDataTable alert alert-warning"
                                    style="width: 100%; margin: 0px 0px;">
                                    <tr>
                                        <td>
                                            No data found.
                                        </td>
                                    </tr>
                                </table>
                            </EmptyDataTemplate>
                            <ItemTemplate>
                                <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                    <td>
                                        <asp:HyperLink ID="lblEmployee" runat="server"  />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblRequistions" runat="server" />
                                    </td>
                                    <td id="tdLevels" runat="server">
                                    </td>
                                   <%-- <td>
                                        <asp:Label ID="lblSubmissions" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblNewClients" runat="server" />
                                    </td>--%>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
