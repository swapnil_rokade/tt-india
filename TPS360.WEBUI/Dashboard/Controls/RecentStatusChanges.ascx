﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RecentStatusChanges.ascx.cs"
    Inherits="RecentStatusChanges" %>
<%@ Register Src="~/Controls/SmallPagerControl.ascx" TagName="Pagers" TagPrefix="ucll" %>
<asp:Panel ID="pnlSettings" runat="server">
</asp:Panel>
<asp:TextBox ID="txtSortColumn" runat="server" Visible="false" />
<asp:TextBox ID="txtSortOrder" runat="server" Visible="false" />
<asp:Panel ID="widgetBody" runat="server">

            <asp:HiddenField ID="hdnStatus" Value="" runat="server" />
            <asp:ObjectDataSource ID="odsStatusChange" runat="server" SelectMethod="GetPaged"
                TypeName="TPS360.Web.UI.CandidateRequisitionStatusDataSource" SelectCountMethod="GetListCount"
                EnablePaging="True" SortParameterName="sortExpression" >
                <SelectParameters>
                    <asp:Parameter Name="ManagerId" DefaultValue="0" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:ListView ID="lsvRecentChanges" runat="server" DataKeyNames="Id" DataSourceID="odsStatusChange"
                OnItemDataBound="lsvRecentChanges_ItemDataBound" EnableViewState="true" OnPreRender="lsvRecentChanges_PreRender"
                OnItemCommand="lsvRecentChanges_ItemCommand">
                <LayoutTemplate>
                    <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                        <tr>
                            <th>
                                <asp:LinkButton ID="btnDateTime" runat="server" ToolTip="Sort By Date & Time" CommandName="Sort"
                                    CommandArgument="[CRS].[CreateDate]" Text="Date & Time" />
                            </th>
                            <th id="thbtnUsers" runat="server">
                                <asp:LinkButton ID="btnUsers" runat="server" ToolTip="Sort By Users" CommandName="Sort"
                                    CommandArgument="[e].[Firstname]" Text="Users" />
                            </th>
                            <th>
                                <asp:LinkButton ID="btnCandidateName" runat="server" ToolTip="Sort By Candidate Name"
                                    CommandName="Sort" CommandArgument="[C].CandidateName" Text="Candidate" />
                            </th>
                            <th>
                                <asp:LinkButton ID="btnRequisition" runat="server" ToolTip="Sort By Job Title" CommandName="Sort"
                                    CommandArgument="[J].[JobTitle]" Text="Requisition" />
                            </th>
                            <th>
                                <asp:LinkButton ID="btnNewStatus" runat="server" ToolTip="Sort By New Status" CommandName="Sort"
                                    CommandArgument="[HML].Name" Text="New Status" />
                            </th>
                        </tr>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        <tr class="Pager">
                            <td colspan="4" id="tdPager" runat="server">
                                <ucll:Pagers ID="pagerControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </LayoutTemplate>
                <EmptyDataTemplate>
                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server"
                        style="width: 100%; margin: 0px 0px;">
                        <tr>
                            <td>
                                No Recent Status Changes.
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <ItemTemplate>
                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                        <td>
                            <asp:Label ID="lblDateTime" runat="server"  />
                        </td>
                        <td id="tdlnkUsers" runat="server">
                            <asp:Label ID="lnkUsers" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:HyperLink ID="lnkCandidateName" runat="server"  ></asp:HyperLink>
                        </td>
                        <td>
                            <asp:HyperLink ID="lnkJobTitle" runat="server"  ></asp:HyperLink>
                        </td>
                        <td>
                            <asp:Label ID="lblNewStatus" runat="server" />
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>

</asp:Panel>
