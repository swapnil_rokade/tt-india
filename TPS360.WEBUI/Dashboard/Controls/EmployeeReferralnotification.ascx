﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EmployeeReferralnotification.ascx.cs"
    Inherits="EmployeeReferralnotification" %>
<%@ Register Src="~/Controls/DateRangePicker.ascx" TagName="DateRange" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/SmallPagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<asp:Panel ID="pnlSettings" runat="server">
</asp:Panel>

 
      

<asp:UpdatePanel ID="upReferral" runat="server">
    <ContentTemplate>
<div class="TableRow" style =" display:table ; margin-bottom : 5px;">    
           <ucl:DateRange ID="dtRange" runat="server" /> 
        <asp:ImageButton ID="imgSearch" ImageUrl ="~/Images/search-icon.png" runat ="server" Width ="25px" Height ="25px" style="cursor :pointer ; margin :0px" ToolTip ="Apply Search Filter"/>

</div>

        <asp:TextBox ID="hdnSortOrder" runat ="server" Visible ="false" ></asp:TextBox>
        <asp:TextBox ID="hdnSortColumn" runat ="server" Visible ="false" ></asp:TextBox>
        <asp:ObjectDataSource ID="odsReferralDetial" runat="server" SelectMethod="GetPaged_ForDetail"
            TypeName="TPS360.Web.UI.EmployeeReferralDataSource" SelectCountMethod="GetListCount_ForDetail"
            EnablePaging="True" SortParameterName="sortExpression">
            <SelectParameters>
                <asp:ControlParameter ControlID="dtRange" Name="StartDate" PropertyName="StartDate"
                    Type="DateTime" />
                <asp:ControlParameter ControlID="dtRange" Name="EndDate" PropertyName="EndDate" Type="DateTime" />
                <asp:Parameter Name="JobPostingId" DefaultValue="0" />
                <asp:Parameter Name="ReferrerID" DefaultValue="0" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <asp:ListView ID="lsvReferralDetail" runat="server" DataKeyNames="Id" DataSourceID="odsReferralDetial"
            OnItemDataBound="lsvReferralDetail_ItemDataBound" OnItemCommand="lsvReferralDetail_ItemCommand"
            OnPreRender="lsvReferralDetail_PreRender">
            <LayoutTemplate>
                <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                    <tr>
                        <th>
                            <asp:LinkButton runat="server" ID="btnDate" CommandName="Sort" CommandArgument="[ER].[CreateDate]"
                                Text="Date" ToolTip="Date" />
                        </th>
                        <th>
                            <asp:LinkButton runat="server" ID="btnReferrer" CommandName="Sort" CommandArgument="[ERR].[RefererFirstName]"
                                Text="Referrer" ToolTip="Referrer" />
                        </th>
                         <th>
                            <asp:LinkButton runat="server" ID="btnCandidate" CommandName="Sort" CommandArgument="[C].[CandidateName]"
                                Text="Candidate" ToolTip="Candidate" />
                        </th>
                        <th>
                            <asp:LinkButton runat="server" ID="btnRequisition" CommandName="Sort" CommandArgument="[J].[JobTitle]"
                                Text="Requisition" ToolTip="Requisition" />
                        </th>
                        <th>
                        <asp:Label runat="server" ID="btnStatus" Text="Status" ToolTip="Status"></asp:Label>
                        </th>
                       
                    </tr>
                    <tr id="itemPlaceholder" runat="server">
                    </tr>
                    <tr class="Pager">
                        <td id="tdpager" runat="server" colspan="5">
                            <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                        </td>
                    </tr>
                </table>
            </LayoutTemplate>
            <EmptyDataTemplate>
                <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                    <tr>
                        <td>
                            No data was returned.
                        </td>
                    </tr>
                </table>
            </EmptyDataTemplate>
            <ItemTemplate>
                <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                    <td>
                        <asp:Label ID="lblDate" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblReferrer" />
                    </td>
                     <td>
                        <asp:HyperLink ID="hlnkCandidate"  runat ="server"  ></asp:HyperLink>
                    </td>
                    <td>
                        <asp:HyperLink ID ="hlnkJobTitle" runat ="server" ></asp:HyperLink>
                    </td>
                   <td>
                   <asp:Label ID="lblStatus" runat="server"></asp:Label>
                   </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
    </ContentTemplate>
</asp:UpdatePanel>
