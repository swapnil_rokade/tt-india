﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Web.UI;
using System.Collections;

public partial class RecentStatusChanges : TPS360.Web.UI.BaseControl, IWidget
{
    #region Member Variables
    private bool _IsAccessToCandidate = true ;
    private static string UrlForCandidate = string.Empty;
    private static int SitemapIdForCandidate = 0;
    #endregion

    #region Properties

    public string Status
    {
        set
        { hdnStatus.Value = value; }
        get
        {
            return hdnStatus.Value;
        }
    }

    private IWidgetHost _Host;

    public IWidgetHost Host
    {
        get { return _Host; }
        set { _Host = value; }
    }

    private string _userId;
    public string UserId
    {
        get { return _userId; }
        set { _userId = value; }
    }

    #endregion
    #region Page Events

    private void setParameters()
    {
        odsStatusChange.SelectParameters.Clear();
        odsStatusChange.SelectParameters .Add ("ManagerId",(Status=="All"?"0":base .CurrentMember .Id .ToString ()));
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        setParameters();
        (this as IWidget).HideSettings();
        CustomSiteMap CustomMap = new CustomSiteMap();
        CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(358, CurrentMember.Id);
        if (CustomMap == null) _IsAccessToCandidate = false;
        else
        {
            SitemapIdForCandidate = CustomMap.Id;
            UrlForCandidate = "~/" + CustomMap.Url.ToString();
        }
        
        if (!IsPostBack)
        {
            GetWidgetData();
            txtSortColumn.Text = "btnDateTime";
            txtSortOrder.Text = "DESC";
            PlaceUpDownArrow();
        }
        string pagesize = "";
        pagesize = (Request.Cookies["DashboardRecentChangesRowPerPage"] == null ? "" : Request.Cookies["DashboardRecentChangesRowPerPage"].Value); ;
        ASP.controls_smallpagercontrol_ascx PagerControl = (ASP.controls_smallpagercontrol_ascx)this.lsvRecentChanges .FindControl("pagerControl");
        if (PagerControl != null)
        {
            DataPager pager = (DataPager)PagerControl.FindControl("pager");
            if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "10" : pagesize); 

        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
    }
    #endregion

    #region Listview Events
    protected void lsvRecentChanges_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
        {

            CandidateRequisitionStatus statuschange = ((ListViewDataItem)e.Item).DataItem as CandidateRequisitionStatus;
            Label lblDateTime = (Label)e.Item.FindControl("lblDateTime");
            HyperLink lnkCandidateName = (HyperLink)e.Item.FindControl("lnkCandidateName");
            HyperLink lnkJobTitle = (HyperLink)e.Item.FindControl("lnkJobTitle");
            Label  lnkUsers = (Label )e.Item.FindControl("lnkUsers");
            Label lblNewStatus = (Label)e.Item.FindControl("lblNewStatus");
            System.Web.UI.HtmlControls.HtmlTableCell thbtnUsers = (System.Web.UI.HtmlControls.HtmlTableCell)lsvRecentChanges.FindControl("thbtnUsers");
            System.Web.UI.HtmlControls.HtmlTableCell tdlnkUsers = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdlnkUsers");
          
            lblDateTime.Text = statuschange.CreateDate.ToShortDateString() + " " +statuschange.CreateDate.ToShortTimeString () ;

            if (_IsAccessToCandidate)
            {
                if (SitemapIdForCandidate > 0 && UrlForCandidate != string.Empty)
                    ControlHelper.SetHyperLink(lnkCandidateName, UrlForCandidate, string.Empty, statuschange .CandidateName , UrlConstants.PARAM_MEMBER_ID , statuschange .MemberId.ToString(), UrlConstants.PARAM_SITEMAP_ID, SitemapIdForCandidate.ToString());
            }
            else
            {
                lnkCandidateName.Enabled = false;
                lnkCandidateName.Text = statuschange.CandidateName;// submission.ApplicantName;
            }

            JobPostingHiringTeam details = new JobPostingHiringTeam();
            details = Facade.GetJobPostingHiringTeamByMemberId(CurrentMember.Id,  statuschange .JobPostingId );
            if (details != null){
                
                lnkJobTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + statuschange.JobPostingId + "&FromPage=JobDetail" + "','700px','570px'); return false;");
                lnkJobTitle.Text = statuschange.JobTitle;
            }
               
            else
            {
                lnkJobTitle.Enabled = false;
                lnkJobTitle.Text = statuschange.JobTitle;
            }
            lnkUsers.Text = statuschange .UsersName ;
            
      
            lblNewStatus.Text = statuschange.LevelName;
            //if (statuschange.RejectedDate != DateTime.MinValue)
            //{
            //    lblNewStatus.Text ="Rejected";//.Visible = false;
            //}
            thbtnUsers.Visible = tdlnkUsers.Visible =  Status == "All";
        }
    }

    protected void lsvRecentChanges_PreRender(object sender, EventArgs e)
    {
        lsvRecentChanges.DataBind();
        ASP.controls_smallpagercontrol_ascx PagerControl = (ASP.controls_smallpagercontrol_ascx)this.lsvRecentChanges .FindControl("pagerControl");
        if (PagerControl != null)
        {
            DataPager pager = (DataPager)PagerControl.FindControl("pager");
            if (pager != null)
            {
                if (pager.Controls.Count >= 1)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
            }
            HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
            if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "DashboardRecentChangesRowPerPage";
        }
        PlaceUpDownArrow();
        if ( lsvRecentChanges.Controls.Count == 0)
        {
            lsvRecentChanges.DataSource = null;
            lsvRecentChanges.DataBind();
        }

        System.Web.UI.HtmlControls.HtmlTableCell tdPager = (System.Web.UI.HtmlControls.HtmlTableCell)lsvRecentChanges.FindControl("tdPager");
        if (tdPager != null) tdPager.ColSpan = Status == "All" ? 5 : 4;
    }

    protected void lsvRecentChanges_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        try
        {
            if (string.Equals(e.CommandName, "Email"))
            {
                string[] ParamID = e.CommandArgument.ToString().Split('$');

                string sub_MemberEmailId = ParamID[0];
                string sub_ClientID = ParamID[1];
                string sub_SubmissionId = ParamID[2];
                string sub_MemberId = ParamID[3];
                SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.CommonPages.EMAIL_PREVIEW.Substring(3), string.Empty, UrlConstants.PARAM_MemberEmail_ID, sub_MemberEmailId, UrlConstants.PARAM_COMPANY_ID, sub_ClientID, UrlConstants.PARAM_JOB_ID, sub_SubmissionId, UrlConstants.PARAM_APPLICANT_ID, sub_MemberId);
                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "EmailDashBoard", "<script>window.open('" + url + "');</script>", false);
            }
            if (e.CommandName == "Sort")
            {
                LinkButton lnkbutton = (LinkButton)e.CommandSource;
                if (txtSortColumn.Text == lnkbutton.ID)
                {
                    if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                    else txtSortOrder.Text = "ASC";
                }
                else
                {
                    txtSortColumn.Text = lnkbutton.ID;
                    txtSortOrder.Text = "ASC";
                }

            }
        }
        catch
        {
        }
    }
    #endregion
    #region Methods
    public void GetWidgetData()
    {
        try
        {
            //odsSubmissions.SelectParameters["MemberId"].DefaultValue = base.CurrentMember.Id.ToString();
            this.lsvRecentChanges .DataBind();
        }
        catch
        {
        }
    }

    private void PlaceUpDownArrow()
    {
        try
        {
            LinkButton lnk = (LinkButton) lsvRecentChanges .FindControl(txtSortColumn.Text);
            System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
            im.EnableViewState = false;
            im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));
        }
        catch
        {
        }

    }
    #endregion
    #region Implementation

    void IWidget.Init(IWidgetHost host)
    {
        this.Host = host;
    }

    void IWidget.ShowSettings()
    {
        pnlSettings.Visible = true;
    }
    void IWidget.HideSettings()
    {
        pnlSettings.Visible = false;
    }
    void IWidget.Minimized()
    {
    }
    void IWidget.Maximized()
    {
       
        if (lsvRecentChanges  != null)
        {
            if (lsvRecentChanges.Items.Count == 0)
            {
                lsvRecentChanges.DataSource = null;
                lsvRecentChanges.DataBind();
            }
        }
    }
    void IWidget.Closed()
    {
    }

    #endregion
}
