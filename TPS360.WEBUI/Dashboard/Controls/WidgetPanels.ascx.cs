﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class WidgetPanels : System.Web.UI.UserControl
{
    private double leftPanelSize;
    private double middlePanelSize;
    private double rightPanelSize;

    private string rightPanelVisible;
    private string middlePanelVisible;
    private int padding = 3;
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public string MiddlePanelSize
    {
        get { return (this.middlePanelSize - padding).ToString() + "%"; }
    }
    public string RightPanelSize
    {
        get { return (this.rightPanelSize - padding).ToString() + "%"; }
    }
    public string LeftPanelSize
    {
        get { return (this.leftPanelSize - padding).ToString() + "%"; }
    }
    public string RightPanelVisible
    {
        get { return this.rightPanelVisible; }
    }

    public string MiddlePanelVisible
    {
        get { return this.middlePanelVisible; }
    }

    public void SetLayout(int layoutID)
    {
        this.leftPanelSize = 50;
        this.middlePanelSize = 50;
        this.rightPanelSize = 0;
        this.rightPanelVisible = "visible";
        this.middlePanelVisible = "hidden";
    }
}
