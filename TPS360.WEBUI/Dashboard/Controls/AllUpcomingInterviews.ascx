﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllUpcomingInterviews.ascx.cs"
    Inherits="AllUpcomingInterviews" %>
<%@ Register Src="~/Controls/SmallPagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<asp:Panel ID="pnlSettings" runat="server">
</asp:Panel>
<asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
<asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>
<asp:Panel ID="widgetBody" runat="server">
<asp:UpdatePanel ID="upUpcomming" runat ="server" >
<ContentTemplate >

                <asp:ObjectDataSource ID="odsInterviewSchedule" runat="server" SelectMethod="GetPagedForDashboard"
                    TypeName="TPS360.Web.UI.MemberInterviewDataSource" SelectCountMethod="GetListCountForDashboard"
                    EnablePaging="True" SortParameterName="sortExpression">
                    <SelectParameters>
                        <asp:Parameter Name="MemberId" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ListView ID="lsvInterviewSchedule" runat="server" DataKeyNames="Id" DataSourceID="odsInterviewSchedule"
                    OnItemDataBound="lsvInterviewSchedule_ItemDataBound" EnableViewState="true" OnPreRender="lsvInterviewSchedule_PreRender"
                    OnItemCommand="lsvInterviewSchedule_ItemCommand">
                    <LayoutTemplate>
                        <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                            <tr>
                                <th style="width: 30%;" enableviewstate="false">
                                    <asp:LinkButton ID="btnDateTime" runat="server" ToolTip="Sort By Date & Time" CommandName="Sort"
                                        CommandArgument="[I].[StartDateTime]" Text="Date & Time" />
                                </th>
                                <th style="width: 20%;" enableviewstate="false">
                                    <asp:LinkButton ID="btnRequisition" runat="server" ToolTip="Sort By Requisition"
                                        CommandName="Sort" CommandArgument="[JP].[JobTitle]" Text="Requisition" />
                                </th>
                                <th style="width: 20%;" enableviewstate="false">
                                    <asp:LinkButton ID="btnName" runat="server" ToolTip="Sort By Candidate Name" CommandName="Sort"
                                        CommandArgument="[M].[FirstName]" Text="Candidate Name" />
                                </th>
                            
                                <th style="width: 25%;" enableviewstate="false">
                                    <asp:LinkButton ID="btnInternalInterviewers" runat="server" ToolTip="Sort By Interviewers"
                                        CommandName="Sort" CommandArgument="[GIV].[InterviewerName]" Text="Interviewers" />
                                </th>
                             
                            </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                            <tr class="Pager">
                                <td colspan="4">
                                    <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <EmptyDataTemplate>
                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server"
                            style="width: 100%; margin: 0px 0px;">
                            <tr>
                                <td>
                                    No upcoming interviews.
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <ItemTemplate>
                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                            <td>
                                <asp:Label ID="lblDateTime" runat="server" />
                            </td>
                            <td>
                                <asp:HyperLink ID="lnkRequisition" runat="server" ></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="lnkCandidateName"  runat="server" ></asp:HyperLink>
                            </td>
                         
                            <td>
                                <asp:Label ID="lblInternalInterviewers" runat="server"  />
                            </td>
                           
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
                
                </ContentTemplate>
</asp:UpdatePanel>

</asp:Panel>
