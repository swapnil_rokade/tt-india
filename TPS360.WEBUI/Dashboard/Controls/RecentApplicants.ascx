<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RecentApplicants.ascx.cs"
    Inherits="RecentApplicants" %>
<%@ Register Src="~/Controls/DateRangePicker.ascx" TagName="DateRange" TagPrefix="ucl" %>    
<%@ Register Src="~/Controls/SmallPagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<asp:Panel ID="pnlSettings" runat="server">
</asp:Panel>
<asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
<asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>
<asp:TextBox ID="SortColumn" runat="server" EnableViewState="true" Visible="false" />
<asp:TextBox ID="SortOrder" runat="server" EnableViewState="true" Visible="false" />
<asp:Panel ID="widgetBody" runat="server">
    <%-- <h4 class="CommonDashboardHeader"><img align="middle" src="../Images/Dashboard/RecentApplicant.png" />&nbsp;Candidate List</h4>--%>
    <div class="TableRow" style="margin: 0px; padding: 0px; margin-bottom : 5px;">
               <div class="TableFormContent">
            <ucl:DateRange ID="uclDateRange" runat ="server" />
              <asp:ImageButton ID="imgSearch" ImageUrl ="~/Images/search-icon.png" runat ="server" Width ="25px" Height ="25px" style="cursor :pointer ; margin :0px" ToolTip ="Apply Search Filter" />
        </div>
    </div>
    <asp:ObjectDataSource ID="odsCandidateList" runat="server" SelectMethod="GetPaged"
        TypeName="TPS360.Web.UI.CandidateDataSource" SelectCountMethod="GetListCount"
        EnablePaging="True" SortParameterName="sortExpression">
        <SelectParameters>
            <asp:Parameter Name="memberId" Type="Int32" />
              <asp:Parameter Name="IsVendor" Type="Boolean"  DefaultValue ="false" />
              <asp:Parameter Name ="IsVendorContact" Type ="Boolean" DefaultValue ="false" />
            <asp:ControlParameter ControlID ="uclDateRange" PropertyName ="StartDate" Name ="DateFrom" Type ="DateTime" />
            <asp:ControlParameter ControlID ="uclDateRange" PropertyName ="EndDate" Name ="DateTo" Type ="DateTime" />
            <asp:Parameter Name="SortOrder" DefaultValue="asc" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ListView ID="lsvCandidateList" runat="server" DataSourceID="odsCandidateList"
        EnableViewState="true" DataKeyNames="Id" OnItemDataBound="lsvCandidateList_ItemDataBound"
        OnItemCommand="lsvCandidateList_ItemCommand" OnPreRender="lsvCandidateList_PreRender">
        <LayoutTemplate>
            <table id="tlbTemplate" class="Grid" cellspacing="0" border="0">
                <tr runat="server" id="trRecentApplicant">
                    <th style="min-width: 100px; white-space: nowrap;">
                        <asp:LinkButton ID="btnName" runat="server" ToolTip="Sort By Name" CommandName="Sort"
                            CommandArgument="[C].[FirstName]" Text="Name" />
                    </th>
                    <th style="min-width: 100px; white-space: nowrap;">
                        <asp:LinkButton ID="btnEmail" runat="server" ToolTip="Sort By Email" CommandName="Sort"
                            CommandArgument="[C].[PrimaryEmail]" Text="Email" />
                    </th>
                    <th style="min-width: 100px; white-space: nowrap;">
                        <asp:LinkButton ID="btnMobilePhone" runat="server" ToolTip="Sort By Cell Phone" CommandName="Sort"
                            CommandArgument="[C].[CellPhone]" Text="Cell Phone" />
                    </th>
                </tr>
                <tr id="itemPlaceholder" runat="server">
                </tr>
                <tr class="Pager">
                    <td colspan="3">
                        <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                    </td>
                </tr>
            </table>
        </LayoutTemplate>
        <EmptyDataTemplate>
            <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server"
                style="width: 100%; margin: 0px 0px;">
                <tr>
                    <td>
                        No candidates available.
                    </td>
                </tr>
            </table>
        </EmptyDataTemplate>
        <ItemTemplate>
            <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                <td>
                    <asp:HyperLink ID="lnkCandidateName"  runat="server"></asp:HyperLink>
                </td>
                <td>
                    <asp:LinkButton ID="lblEmail" runat="server" />
                </td>
                <td>
                    <asp:Label ID="lblMobilePhone" runat="server" />
                </td>
            </tr>
        </ItemTemplate>
    </asp:ListView>
</asp:Panel>
