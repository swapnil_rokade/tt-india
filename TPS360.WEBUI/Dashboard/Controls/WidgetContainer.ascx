<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WidgetContainer.ascx.cs"
    Inherits="WidgetContainer" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Panel ID="Widget" runat="server">
    <div class="sidebar">
        <div class="widgetContainer">
            <div class="titleContainer">
                <asp:Panel ID="WidgetHeader" runat="server">
                    <asp:UpdatePanel ID="WidgetHeaderUpdatePanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <table class="widget_header_table" cellspacing="2" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <td class="widget_title">
                                            <asp:LinkButton ID="WidgetTitle" runat="Server" Text="Widget Title" />
                                            <asp:TextBox ID="WidgetTitleTextBox" runat="Server" Visible="False" CssClass="CommonTextBox" />
                                            <asp:Button ID="SaveWidgetTitle" runat="Server" OnClick="SaveWidgetTitle_Click" Visible="False"
                                                Text="OK" CssClass="CommonButton" />
                                        </td>
                                        <td class="widget_edit">
                                            <asp:LinkButton ID="EditWidget" runat="Server" Text="edit" OnClick="EditWidget_Click"
                                                Visible="false" />
                                            <asp:LinkButton ID="CancelEditWidget" runat="Server" Text="cancel edit" OnClick="EditWidget_Click"
                                                Visible="false" />
                                        </td>
                                        <td class="widget_button">
                                            <asp:LinkButton ID="CollapseWidget" runat="Server" Text="" OnClick="CollapseWidget_Click"
                                                CssClass="widget_min widget_box" />
                                            <asp:LinkButton ID="ExpandWidget" runat="Server" Text="" CssClass="widget_max widget_box"
                                                OnClick="ExpandWidget_Click" />
                                        </td>
                                        <td class="widget_button">
                                            <asp:LinkButton ID="CloseWidget" runat="Server" Text="" CssClass="widget_close widget_box"
                                                OnClick="CloseWidget_Click" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
            </div>
            <asp:UpdateProgress ID="UpdateProgress2" runat="server" DisplayAfter="10" AssociatedUpdatePanelID="WidgetHeaderUpdatePanel">
                <ProgressTemplate>
                    <center>
                        <asp:Image runat="server" ID="imgLogo1" ImageUrl="~/Images/progress.gif" /></center>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="WidgetBodyUpdatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="pnlBody" runat="Server">
                        <div class="contentContainer">
                            <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="10" AssociatedUpdatePanelID="WidgetBodyUpdatePanel">
                                <ProgressTemplate>
                                    <center>
                                        <asp:Image runat="server" ID="imgLogo" ImageUrl="~/Images/progress.gif" /></center>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:Panel ID="WidgetBodyPanel" runat="Server">
                            </asp:Panel>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div class="divider">
    </div>
</asp:Panel>
