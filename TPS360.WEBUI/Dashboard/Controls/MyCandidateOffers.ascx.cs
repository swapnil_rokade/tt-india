﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Web.UI;
using System.Collections;

public partial class MyCandidateOffers : BaseControl, IWidget
{
    #region Member Variables
    private bool _IsAccessToCandidate = true;
    private static string UrlForCandidate = string.Empty;
    private static int SitemapIdForCandidate = 0;
    #endregion

    #region Properties

    private IWidgetHost _Host;

    public IWidgetHost Host
    {
        get { return _Host; }
        set { _Host = value; }
    }

    private string _userId;
    public string UserId
    {
        get { return _userId; }
        set { _userId = value; }
    }

    #endregion

    #region Page Events
    private void setParamenters()
    {
        var webConfig = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");
        var offer = webConfig.AppSettings.Settings["Offer"];
        odsCandidateOffer.SelectParameters.Clear();
        odsCandidateOffer.SelectParameters.Add("OfferLevelName", offer.Value);
        odsCandidateOffer.SelectParameters.Add("MemberId", base.CurrentMember.Id.ToString());
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        setParamenters();
        (this as IWidget).HideSettings();
        CustomSiteMap CustomMap = new CustomSiteMap();
        CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(358, CurrentMember.Id);
        if (CustomMap == null) _IsAccessToCandidate = false;
        else
        {
            SitemapIdForCandidate = CustomMap.Id;
            UrlForCandidate = "~/" + CustomMap.Url.ToString();
        }
        
        if (!IsPostBack)
        {
            GetWidgetData();
            txtSortColumn.Text = "btnOfferDate";
            txtSortOrder.Text = "DESC";
            PlaceUpDownArrow();
        }
        string pagesize = "";
        pagesize = (Request.Cookies["DashboardMyCandidateOffersRowPerPage"] == null ? "" : Request.Cookies["DashboardMyCandidateOffersRowPerPage"].Value); ;
        ASP.controls_smallpagercontrol_ascx PagerControl = (ASP.controls_smallpagercontrol_ascx)this.lsvCandidateOffer.FindControl("pagerControl");
        if (PagerControl != null)
        {
            DataPager pager = (DataPager)PagerControl.FindControl("pager");
            if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "10" : pagesize);
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
    }

    #endregion

    #region Listview Events
    protected void lsvCandidateOffer_PreRender(object sender, EventArgs e)
    {
        lsvCandidateOffer.DataBind();
        ASP.controls_smallpagercontrol_ascx PagerControl = (ASP.controls_smallpagercontrol_ascx)this.lsvCandidateOffer.FindControl("pagerControl");
        if (PagerControl != null)
        {
            DataPager pager = (DataPager)PagerControl.FindControl("pager");
            if (pager != null)
            {
                if (pager.Controls.Count >= 1)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
            }
            HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
            if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "DashboardMyCandidateOffersRowPerPage";
        }
        PlaceUpDownArrow();

        if (lsvCandidateOffer.Controls.Count == 0)
        {
            lsvCandidateOffer.DataSource = null;
            lsvCandidateOffer.DataBind();

        }
    }

    protected void lsvCandidateOffer_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
        {
            MemberHiringDetails HiringDetails = ((ListViewDataItem)e.Item).DataItem as MemberHiringDetails;

            if (HiringDetails != null)
            {
                HyperLink lnkCandidateName = (HyperLink)e.Item.FindControl("lnkCandidateName");
                Label lblOfferedDate = (Label)e.Item.FindControl("lblOfferedDate");
                HyperLink lnkRequisition = (HyperLink)e.Item.FindControl("lnkRequisition");
                Image imgAccept = (Image)e.Item.FindControl("imgAccept");
                lblOfferedDate.Text = HiringDetails.OfferedDate.ToShortDateString();

                string _strFullName = MiscUtil.RemoveScript(HiringDetails.JobTitle);
               

                JobPostingHiringTeam details = new JobPostingHiringTeam();
                details = Facade.GetJobPostingHiringTeamByMemberId(CurrentMember.Id, HiringDetails.JobPostingId);
                if (details != null)
                {
                    lnkRequisition.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + HiringDetails.JobPostingId + "&FromPage=JobDetail" + "','700px','570px'); return false;");
                    lnkRequisition.Text = _strFullName;
                }
                else
                {
                    lnkRequisition.Enabled = false;
                    lnkRequisition.Text = _strFullName;//HiringDetails.JobTitle;
                }
                if (_IsAccessToCandidate)
                {
                    if (SitemapIdForCandidate > 0 && UrlForCandidate != string.Empty)
                        ControlHelper.SetHyperLink(lnkCandidateName, UrlForCandidate, string.Empty, HiringDetails.ApplicantName, UrlConstants.PARAM_MEMBER_ID, HiringDetails.MemberId.ToString(), UrlConstants.PARAM_SITEMAP_ID, SitemapIdForCandidate.ToString());
                }
                else
                {
                    lnkCandidateName.Enabled = false;
                    lnkCandidateName.Text = HiringDetails.ApplicantName;
                }
                if (HiringDetails.OfferAccepted)
                    imgAccept.Visible = true;
                else
                    imgAccept.Visible = false;
            }
        }
    }

    protected void lsvCandidateOffer_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Sort")
            {
                LinkButton lnkbutton = (LinkButton)e.CommandSource;
                if (txtSortColumn.Text == lnkbutton.ID)
                {
                    if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                    else txtSortOrder.Text = "ASC";
                }
                else
                {
                    txtSortColumn.Text = lnkbutton.ID;
                    txtSortOrder.Text = "ASC";
                }

            }
        }
        catch
        {
        }
    }

    #endregion

    #region Methods
    private void PlaceUpDownArrow()
    {
        try
        {
            LinkButton lnk = (LinkButton)lsvCandidateOffer.FindControl(txtSortColumn.Text);
            System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
            im.EnableViewState = false;
            im.Attributes.Add("class", (txtSortOrder .Text  == "ASC" ? "Ascending" : "Descending"));
        }
        catch
        {
        }

    }
    public void GetWidgetData()
    {
        odsCandidateOffer.SelectParameters["MemberId"].DefaultValue = base.CurrentMember.Id.ToString();
        this.lsvCandidateOffer.DataBind();
    }

    #endregion

    #region Implementation

    void IWidget.Init(IWidgetHost host)
    {
        this.Host = host;
    }

    void IWidget.ShowSettings()
    {
        pnlSettings.Visible = true;
    }
    void IWidget.HideSettings()
    {
        pnlSettings.Visible = false;
    }
    void IWidget.Minimized()
    {
    }
    void IWidget.Maximized()
    {
        if (lsvCandidateOffer != null)
        {
            if (lsvCandidateOffer.Items.Count == 0)
            {
                lsvCandidateOffer.DataSource = null;
                lsvCandidateOffer.DataBind();
            }
        }
    }
    void IWidget.Closed()
    {
    }

    #endregion
}
