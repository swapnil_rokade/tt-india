﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Web.UI;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Web.UI.WebControls;



public partial class cntrlProductivity : BaseControl
{
    #region Properties
    private bool _IsAccessToEmployee = true;
    private static string UrlForEmployee = string.Empty;
    private static int IdForSitemap = 0;
    #endregion

  

    #region methods
    private void PlaceUpDownArrow()
    {
        try
        {

            LinkButton lnk = (LinkButton)lsvProductivity.FindControl(hdnSortColumn.Value );
            System.Web.UI.HtmlControls.HtmlTableCell th = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
            if (lnk.CommandArgument.Contains("Level["))
            {
                th.Attributes.Add("class", (hdnSortOrder.Value  == "ASC" ? "Descending" : "Ascending"));
            }
            else
            {
                th.Attributes.Add("class", (hdnSortOrder.Value  == "ASC" ? "Ascending" : "Descending"));
            }

        }
        catch
        {
        }

    }
    public void BindList()
    {
        if (SortOrder.Text == string.Empty && SortColumn.Text == string.Empty)
        {
            SortOrder.Text = "asc";
            SortColumn.Text = "[M].[FirstName]";
        }
       // odsProductivity.SelectParameters["RequireDate"].DefaultValue = "Today";
     //   odsProductivity.SelectParameters["MemberId"].DefaultValue = hdnMemberID.Value == string.Empty ? "0" : hdnMemberID.Value ;
        odsProductivity.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text;
        lsvProductivity.DataBind();
     }
    public void BindList(string RequireDate)
    {
        if (SortOrder.Text == string.Empty && SortColumn.Text == string.Empty)
        {
            SortOrder.Text = "asc";
            SortColumn.Text = "[M].[FirstName]";
        }
        //odsProductivity.SelectParameters["RequireDate"].DefaultValue = RequireDate;
        //odsProductivity.SelectParameters["MemberId"].DefaultValue = hdnMemberID.Value == string.Empty ? "0" : hdnMemberID.Value;
        odsProductivity.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text;

        lsvProductivity.DataBind();
    }
    #endregion
    
    #region Events
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        lsvProductivity.DataBind();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        CustomSiteMap CustomMap = new CustomSiteMap();
        CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(360, CurrentMember.Id);
        if (CustomMap == null) _IsAccessToEmployee = false;
        else
        {
            IdForSitemap = CustomMap.Id;
            UrlForEmployee = "~/" + CustomMap.Url.ToString();
        }

        if (dtRange.StartDate == DateTime.MinValue)
        {
            dtRange.setDateRange(DateTime.Now.AddDays(-6), DateTime.Now);
            BindList();
            hdnSortColumn.Value = "lnkEmpName";
            hdnSortOrder.Value = "ASC";
        }
        if (!IsPostBack)
        {
            dtRange.setDateRange(DateTime.Now.AddDays(-6), DateTime.Now);
            BindList();
            hdnSortColumn.Value = "lnkEmpName";
            hdnSortOrder.Value = "ASC";
        }

       
        System.Web.UI.HtmlControls.HtmlTableCell thLevels = (System.Web.UI.HtmlControls.HtmlTableCell)this.lsvProductivity .FindControl("thLevels");
        if (thLevels != null)
        {
            IList<HiringMatrixLevels> hiringMatrixLevels = Facade.GetAllHiringMatrixLevels();
            //hiringLevels = hiringMatrixLevels;
            int ColLocation = 0;

            if (hiringMatrixLevels != null)
            {
                if (hiringMatrixLevels.Count > 0)
                {
                    foreach (HiringMatrixLevels levels in hiringMatrixLevels)
                    {
                        System.Web.UI.HtmlControls.HtmlTableCell col = new System.Web.UI.HtmlControls.HtmlTableCell("TH");
                        LinkButton lnkLevel = new LinkButton();
                        lnkLevel.Text = levels.Name.Trim();
                        lnkLevel.CommandName = "Sort";
                        lnkLevel.ID = "thLevel" + ColLocation;
                        
                         //                      lnkLevel.Style.Add(HtmlTextWriterStyle.WhiteSpace  , "nowrap");
                         //lnkLevel.Style.Add(HtmlTextWriterStyle.TextOverflow  , "ellipsis");
                         lnkLevel.ToolTip = lnkLevel.Text;
                        //width += lnkLevel.Text.Length * 7;
                        //col.Style.Add("min-width", (lnkLevel.Text.Length * 10) + "px");
                        //lnkLevel.Width = Unit.Percentage(50);

                        //   col.Style.Add("min-width","500px");

                        LinkButton lnktemp = (LinkButton)lsvProductivity.FindControl(lnkLevel.ID);
                        if (lnktemp != null) lsvProductivity.Controls.Remove(lnktemp);
                        lnkLevel.EnableViewState = false;
                        lnkLevel.CommandArgument = "Level[" + levels.Id + "]";
                        lnkLevel.EnableViewState = false;
                        if (levels.SortingOrder == 0)
                        {
                            thLevels.Controls.AddAt(ColLocation, lnkLevel);
                            //thLevels.Style.Add("min-width", (lnkLevel.Text.Length * 7 )+ "px");
                        }
                        else
                        {
                            col.Controls.Add(lnkLevel);
                            thLevels.Controls.AddAt(ColLocation, col);

                        }
                        ColLocation++;

                    }
                }
            }
            System.Web.UI.HtmlControls.HtmlTableCell tdPager = (System.Web.UI.HtmlControls.HtmlTableCell)this.lsvProductivity.FindControl("tdPager");
            tdPager.ColSpan = 4 + hiringMatrixLevels.Count;
            //string pagesize = "";
            //pagesize = (Request.Cookies["DashboardProductivity"] == null ? "" : Request.Cookies["DashboardProductivity"].Value); ;
            //ASP.controls_smallpagercontrol_ascx PagerControl = (ASP.controls_smallpagercontrol_ascx)this.lsvProductivity.FindControl("pagerControl");
            //if (PagerControl != null)
            //{
            //    DataPager pager = (DataPager)PagerControl.FindControl("pager");
            //    if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "10" : pagesize);
            //}
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        (this as IWidget).HideSettings();
    }
    public void GetWidgetData()
    {
        BindList();
    }
    protected void lsvProductivity_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Sort")
            {
                LinkButton lnkbutton = (LinkButton)e.CommandSource;
                if (hdnSortColumn.Value == lnkbutton.ID)
                {
                    if (hdnSortOrder.Value == "ASC") hdnSortOrder.Value = "DESC";
                    else hdnSortOrder.Value = "ASC";
                }
                else
                {
                    hdnSortColumn.Value = lnkbutton.ID;
                    hdnSortOrder.Value = "ASC";
                } 
                if (SortColumn.Text == string.Empty || SortColumn.Text != e.CommandArgument.ToString())
                    SortOrder.Text = "asc";
                else  SortOrder.Text = SortOrder.Text == "asc" ? "desc" : "asc";
                    SortColumn.Text = e.CommandArgument.ToString();
                    odsProductivity.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
            }
        }
        catch
        {
        }
    }
    protected void lsvProductivity_PreRender(object sender, EventArgs e)
    {
        DataBind();
        try
        {
            ASP.controls_smallpagercontrol_ascx PagerControl = (ASP.controls_smallpagercontrol_ascx)this.lsvProductivity .FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }

                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null)
                {
                  if(hdnProductivity .Value =="My")
                     hdnRowPerPageName.Value = "DashboardMyProductivity";
                  else hdnRowPerPageName.Value = "DashboardMasterProductivity";
                }

             
            }

        }
        catch
        {
        }
        PlaceUpDownArrow();
    }

    protected void lsvProductivity_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
        {
            
            int ColLocation = 0;
           EmployeeProductivity prod = ((ListViewDataItem)e.Item).DataItem as EmployeeProductivity;
           if (prod != null)
           {
               HyperLink lblEmployee = (HyperLink)e.Item.FindControl("lblEmployee");
               Label lblRequistions = (Label)e.Item.FindControl("lblRequistions");
             //  Label lblSubmissions = (Label)e.Item.FindControl("lblSubmissions");
             //  Label lblNewClients = (Label)e.Item.FindControl("lblNewClients");
               lblEmployee.Text = prod.EmployeeName;
               lblRequistions.Text = prod.JobCount.ToString();
             //  lblSubmissions.Text = prod.SubmissionCount.ToString();
              // lblNewClients.Text = prod.NewClientCount.ToString();

               if (_IsAccessToEmployee)
               {
                   if (IdForSitemap > 0 && UrlForEmployee != string.Empty)
                       ControlHelper.SetHyperLink(lblEmployee, UrlForEmployee, string.Empty, prod .EmployeeName , UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(prod .EmployeeId ), UrlConstants.PARAM_SITEMAP_ID, IdForSitemap.ToString());
               }
               else lblEmployee.Text = prod .EmployeeName ;


               System.Web.UI.HtmlControls.HtmlTableCell tdLevels = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdLevels");
               int i = 0;
               foreach (Hiring_Levels lev in  prod .HiringMatrixLevels)
               {
                   System.Web.UI.HtmlControls.HtmlTableCell col1 = new System.Web.UI.HtmlControls.HtmlTableCell("TD");
                   Label  lnkLevel = new Label ();
                   lnkLevel.ID = "lblLevel" + i;
                   lnkLevel.Text = lev.CandidateCount.ToString();
                   HiringMatrixLevels l = Facade.GetHiringMatrixLevelsById(lev.HiringMatrixLevelID);
                   if (i == 0) tdLevels.Controls.AddAt(ColLocation, lnkLevel);
                   else
                   {
                       col1.Controls.Add(lnkLevel);
                       tdLevels.Controls.AddAt(ColLocation, col1);
                   }
                   i++;
                   ColLocation++;
               }
           }

    
        }

    }

    protected void lnkbtnEmployee_Click(object sender, EventArgs e)
    {
        BindList("Today");
    }

    protected void lnkbtnPastWeek_Click(object sender, EventArgs e)
    {
        BindList("PastWeek");
    }
    protected void lnkbtnPastMonth_Click(object sender, EventArgs e)
    {
        BindList("PastMonth");
    }
    protected void lnkbtnPastQuarter_Click(object sender, EventArgs e)
    {
        BindList("PastQuarter");
    }
    protected void lnkbtnPastYear_Click(object sender, EventArgs e)
    {
        BindList("PastYear");
    }
    protected void lnkbtnYesterday_Click(object sender, EventArgs e)
    {
        BindList("Yesterday");
    }

    protected void lnkbtnTillDate_Click(object sender, EventArgs e)
    {
        BindList("MonthTillDate");
    }

    #endregion
}
   

