﻿namespace OutlookAddin
{
    #region Namespaces
    using System;
    using System.Data;
    using System.Web;
    using System.Collections;
    using System.Web.Services;
    using System.Web.Services.Protocols;
    using System.ComponentModel;
    using System.IO;
    using System.Text;
    using System.Runtime.Serialization;
    using System.Web.Security;
    using System.Linq;
    using System.Xml.Linq;
    using ParserLibrary;
    using TPS360.BusinessFacade;
    using TPS360.Common.Helper;
    using TPS360.Common.Shared;
    using TPS360.DataAccess;
    using TPS360.Controls;
    using TPS360.Common.BusinessEntities;
    using System.Data;
    using System.IO;
    using System.Text;
    using System.Xml.Serialization;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Web.Security;
    using System.Collections.Generic;
    using System.Collections;
    using System.ComponentModel;
    using System.Reflection;
    using System.Windows.Forms;
    using TPS360.Web.UI;
    #endregion

    /// <summary>
    /// Summary description for OutlookAddinWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class OutlookAddinWebService : System.Web.Services.WebService
    {
        IFacade facade = new Facade();
        public OutlookAddinWebService()
        {

            //Uncomment the following line if using designed components 
            //InitializeComponent(); 
        }

       


		[WebMethod]
		public string ValidateWebService()
		{
		   return "Success";
		}
        public Recurrence BuildRecurrence(ActivityRecurrence activity)
        {
            Recurrence rec = new Recurrence();
            rec.RecurrenceID = activity.RecurrenceID;
            rec.DayOfMonth = activity.DayOfMonth;
            rec.EndDateUtc = activity.EndDateUtc;
            rec.Period  = activity.Period ;
            rec.MonthOfYear = activity.MonthOfYear;
            rec.PeriodMultiple = activity.PeriodMultiple;
            rec.DayOfWeekMaskUtc = activity.DayOfWeekMaskUtc;
            rec.UtcOffset = activity.UtcOffset;
            try
            {
                rec.LastReminderDateTimeUtc = activity.EndDateUtc ;
            }
            catch
            {
            }
            return rec;
        }
        public Activity BuildActivity(ActivityRecurrence activity)
        {
            Activity act = new Activity();
            act.Subject = activity.Subject;
            act.Location = activity.Location;
            act.ActivityDescription = activity.ActivityDescription;
            act.StartDateTimeUtc = activity.StartDateTimeUtc;
            act.Duration = activity.Duration;
            act.ReminderInterval = activity.ReminderInterval;
            act.Importance = activity.Importance;
            act.AllDayEvent = activity.AllDayEvent;
            act.EnableReminder = activity.EnableReminder;
            act.ShowTimeAs = activity.ShowTimeAs;
            act.RecurrenceID = activity.RecurrenceID;
            act.ResourceName = activity.ResourceName;
            act.ActivityID = activity.ActivityID;
            return act;
        }
        public ActivityRecurrence BuildActivityRecurrence(Recurrence recur, Activity activity)
        {
            ActivityRecurrence actrec = new ActivityRecurrence();
            try
            {

                if (recur != null)
                {
                    actrec.DayOfMonth = recur.DayOfMonth;
                    actrec.EndDateUtc = recur.EndDateUtc;
                    actrec.Period = recur.Period;
                    actrec.MonthOfYear = recur.MonthOfYear;
                    actrec.PeriodMultiple = recur.PeriodMultiple;
                    actrec.DayOfWeekMaskUtc = recur.DayOfWeekMaskUtc;

                    actrec.UtcOffset = recur.UtcOffset;
                }
                if (activity != null)
                {
                    actrec.Subject = activity.Subject;
                    actrec.Location = activity.Location;
                    actrec.ActivityDescription = activity.ActivityDescription;
                    actrec.StartDateTimeUtc = activity.StartDateTimeUtc;
                    actrec.Duration = activity.Duration;
                    actrec.ReminderInterval = activity.ReminderInterval;
                    actrec.Importance = activity.Importance;
                    actrec.AllDayEvent = activity.AllDayEvent;
                    actrec.EnableReminder = activity.EnableReminder;
                    actrec.ShowTimeAs = activity.ShowTimeAs;
                    actrec.ActivityID = activity.ActivityID;
                    actrec.RecurrenceID = activity.RecurrenceID;
                    actrec.Status = activity.Status;
                }
            }
            catch
            {
            }
            return actrec;
        }
        public IList<ActivityRecurrence> AddActToDataBase(IList<Activity> ActFromDataBase, IList<ActivityRecurrence> ActFromOutlook, int MemberId, out IList<ActivityRecurrence> Duplicate)
        {
            Duplicate = new List<ActivityRecurrence>();
            IList<ActivityRecurrence> rsult = new List<ActivityRecurrence>();
            try
            {
                if (ActFromOutlook != null)
                {
                    
                    foreach (ActivityRecurrence  act in ActFromOutlook)
                    {
                       
                        if (ActFromDataBase != null)
                        {
                            if (act.ActivityID != 0)
                            {
                               
                                var cont = from a in ActFromDataBase
                                           where a.ActivityID == act.ActivityID
                                           select a;
                                if (cont.ToArray().Count() == 0)
                                {
                                    
                                    act.ResourceName = MemberId + "";
                                   Activity actfrom=new Activity ();
                                   Recurrence reccuren = new Recurrence();
                                   ActivityRecurrence actrec = new ActivityRecurrence();
                                   if (act.IsRecurred)
                                   {
                                       reccuren = BuildRecurrence(act);
                                       if (act.RecurrenceID != 0)
                                       {
                                           reccuren = facade.UpdateRecurrence(reccuren);
                                           act.RecurrenceID = reccuren.RecurrenceID;
                                       }
                                       else
                                       {
                                           reccuren = facade.AddRecurrence(reccuren);
                                           act.RecurrenceID = reccuren.RecurrenceID;
                                       }
                                   }
                                   actfrom = BuildActivity(act);
                                   actfrom = facade.AddActivity(actfrom);
                                   actrec = BuildActivityRecurrence(reccuren, actfrom);
                                   //actfrom = facade.AddActivity(act);
                                   if (act.ActivityID != 0) Duplicate.Add(actrec);
                                   rsult.Add(actrec);
                                }
                                else
                                {
                                    
                                    foreach (Activity dbact in cont.ToArray())
                                    {

                                        if (act.UpdateDate >= dbact.UpdateDate)
                                        {
                                            
                                            IFacade facade = new Facade();
                                            act.ActivityID = dbact.ActivityID;
                                            act.ResourceName = MemberId + "";
                                            Activity actfrom = new Activity();
                                            Recurrence reccuren = new Recurrence();
                                            ActivityRecurrence actrec = new ActivityRecurrence();
                                            if (act.IsRecurred)
                                            {
                                                reccuren = BuildRecurrence(act);
                                                if (act.RecurrenceID != 0)
                                                {
                                                    reccuren = facade.UpdateRecurrence(reccuren);
                                                    act.RecurrenceID = reccuren.RecurrenceID;
                                                }
                                                else
                                                {
                                                    reccuren = facade.AddRecurrence(reccuren);
                                                    act.RecurrenceID = reccuren.RecurrenceID;
                                                }
                                            }
                                            actfrom = BuildActivity(act);
                                            actfrom = facade.UpdateActivity(actfrom);
                                            actrec = BuildActivityRecurrence(reccuren, actfrom);
                                            rsult.Add(actrec);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                
                                act.ResourceName = MemberId + "";
                                
                                try
                                {
                                    Activity actfrom = new Activity();
                                    Recurrence reccuren = new Recurrence();
                                    ActivityRecurrence actrec = new ActivityRecurrence();
                                    if (act.IsRecurred)
                                    {
                                        reccuren = BuildRecurrence(act);
                                        if (act.RecurrenceID != 0)
                                        {
                                            reccuren = facade.UpdateRecurrence(reccuren);
                                            act.RecurrenceID = reccuren.RecurrenceID;
                                        }
                                        else
                                        {
                                            reccuren = facade.AddRecurrence(reccuren);
                                            act.RecurrenceID = reccuren.RecurrenceID;
                                        }
                                    }
                                    actfrom = BuildActivity(act);
                                    actfrom = facade.AddActivity(actfrom);
                                    actrec = BuildActivityRecurrence(reccuren, actfrom);
                                    rsult.Add(actrec );
                                }
                                catch
                                {
                                }
                            }
                        }
                        else
                        {
                            act.ResourceName = MemberId + "";
                            Activity actfrom = new Activity();
                            Recurrence reccuren = new Recurrence();
                            ActivityRecurrence actrec = new ActivityRecurrence();
                            //reccuren = BuildRecurrence(act);
                            if (act.IsRecurred)
                            {
                                reccuren = BuildRecurrence(act);
                                if (act.RecurrenceID != 0)
                                {
                                    reccuren = facade.UpdateRecurrence(reccuren);
                                    act.RecurrenceID = reccuren.RecurrenceID;
                                }
                                else
                                {
                                    reccuren = facade.AddRecurrence(reccuren);
                                    act.RecurrenceID = reccuren.RecurrenceID;
                                }
                            }
                            actfrom = BuildActivity(act);
                            try 
                            {
                                actfrom = facade.AddActivity(actfrom);
                            }
                            catch { }
                            
                            actrec = BuildActivityRecurrence(reccuren, actfrom);
                            if (act.ActivityID != 0) Duplicate.Add(actrec);
                            rsult.Add(actrec);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
               
            }
            
            return rsult;
        }
        [WebMethod]
        public string getServerTime()
        {
            using (System.Data.SqlClient.SqlConnection sqlconnections = new System.Data.SqlClient.SqlConnection(MiscUtil.ConnectionString))
            {
                sqlconnections.Open();
                using (System.Data.SqlClient.SqlCommand sqlcom = new System.Data.SqlClient.SqlCommand("select getdate()", sqlconnections))
                {
                    System.Data.SqlClient.SqlDataReader reader = sqlcom.ExecuteReader();
                    reader.Read();
                    return reader.GetValue(0).ToString();
                }
            }


        }

	[WebMethod]
 	public ScheduleDetails AddScheduleToDataBase(ScheduleDetails value)
	{
        
        ScheduleDetails valueResult = new ScheduleDetails();
        try
        {


            IList<ActivityRecurrence> OutlookActivity = new List<ActivityRecurrence>();
            
            try
            {
                ConvertByteArrayToIList(value.Appointments, out OutlookActivity);
               
            }
            catch
            {
            }
            
            Member mem = new Member();
            
            mem = facade.GetMemberByMemberEmail(value.CurrentUserMailAddress);
           
            Resource Res = new Resource();
            Res = facade.GetResourceByResourceName(mem.Id + "");
        
            int ResourseName = Res.ResourceID;
            
            IList<Activity> DataBaseActivity = GetAppointmentsFromDatabase(ResourseName);

            IList<ActivityRecurrence> DuplicateCheck = new List<ActivityRecurrence>();
            IList<ActivityRecurrence> ActFromOutlook = AddActToDataBase(DataBaseActivity, OutlookActivity, mem.Id, out DuplicateCheck);
            


            IList<Activity> DataBaseNewActivity = GetAppointmentsFromDatabase(ResourseName);

            IList<ActivityRecurrence> ResultActivity = CollectOutlookDataToSend(ActFromOutlook, DataBaseNewActivity, DuplicateCheck);
             valueResult.Appointments = ConvertIListToByteArray(ResultActivity);
            
            
            //return value;
        }
        catch(Exception ex)
        {
            
        }
      
        return valueResult;
	}

	[WebMethod]
	 public ScheduleDetails GetScheduleFromDataBase(ScheduleDetails value)
	{
		return value;
	}

    private IList<ActivityRecurrence> CollectOutlookDataToSend(IList<ActivityRecurrence> outlookappointments, IList<Activity> databaseappointments, IList<ActivityRecurrence> Duplicate)
    {
        IList<ActivityRecurrence> Result = new List<ActivityRecurrence>();
        try
        {
            
            

                if (databaseappointments != null)
                {
                    foreach (Activity act in databaseappointments)
                    {
                        if (outlookappointments.Count >0)
                        {
                            var k = from a in outlookappointments
                                    where a.ActivityID == act.ActivityID
                                    select a;
                            if (k.ToArray().Count() == 0)
                            {
                                if (Duplicate.Where(x => x.ActivityID == act.ActivityID).Count() > 0)
                                {
                                    act.Status = 1;
                                    Activity acti = new Activity();
                                    Recurrence recur = new Recurrence();
                                    ActivityRecurrence actrec = new ActivityRecurrence();
                                    if (act.RecurrenceID > 0)
                                    {
                                        recur = facade.GetRecurrenceByRecurrenceID(act.RecurrenceID);
                                        if (recur != null)
                                        {
                                            actrec = BuildActivityRecurrence(recur, act);
                                        }
                                    }
                                    else
                                    {
                                        actrec = BuildActivityRecurrence(recur, act);
                                    }
                                    Result.Add(actrec);
                                }
                                else
                                {
                                    Activity acti = new Activity();
                                    Recurrence recur = new Recurrence();
                                    ActivityRecurrence actrec = new ActivityRecurrence();
                                    if (act.RecurrenceID > 0)
                                    {
                                        recur = facade.GetRecurrenceByRecurrenceID(act.RecurrenceID);
                                        if (recur != null)
                                        {
                                            actrec = BuildActivityRecurrence(recur, act);
                                        }
                                    }
                                    else
                                    {
                                        actrec = BuildActivityRecurrence(recur, act);
                                    }
                                    Result.Add(actrec);
                                }
                            }
                            else
                            {
                                foreach (ActivityRecurrence activit in k.ToArray())
                                {
                                    if (act.UpdateDate >= activit.UpdateDate || act.CreateDate >= activit.CreateDate)
                                    {
                                        if (Duplicate.Where(x => x.ActivityID == act.ActivityID).Count() > 0)
                                        {
                                            act.Status = 1;
                                            Activity acti = new Activity();
                                            Recurrence recur = new Recurrence();
                                            ActivityRecurrence actrec = new ActivityRecurrence();
                                            if (act.RecurrenceID > 0)
                                            {
                                                recur = facade.GetRecurrenceByRecurrenceID(act.RecurrenceID);
                                                if (recur != null)
                                                {
                                                    actrec = BuildActivityRecurrence(recur, act);
                                                }
                                            }
                                            else
                                            {
                                                actrec = BuildActivityRecurrence(recur, act);
                                            }
                                            Result.Add(actrec);
                                        }
                                        else
                                        {
                                            Activity acti = new Activity();
                                            Recurrence recur = new Recurrence();
                                            ActivityRecurrence actrec = new ActivityRecurrence();
                                            if (act.RecurrenceID > 0)
                                            {
                                                recur = facade.GetRecurrenceByRecurrenceID(act.RecurrenceID);
                                                if (recur != null)
                                                {
                                                    actrec = BuildActivityRecurrence(recur, act);
                                                }
                                            }
                                            else
                                            {
                                                actrec = BuildActivityRecurrence(recur, act);
                                            }
                                            Result.Add(actrec);
                                        }
                                    }
                                }
                            }
                            
                           
                        }
                        else
                        {
                            if (Duplicate.Where(x => x.ActivityID == act.ActivityID).Count() > 0)
                            {
                                act.Status = 1;
                                Activity acti = new Activity();
                                Recurrence recur = new Recurrence();
                                ActivityRecurrence actrec = new ActivityRecurrence();
                                if (act.RecurrenceID > 0)
                                {
                                    recur = facade.GetRecurrenceByRecurrenceID(act.RecurrenceID);
                                    if (recur != null)
                                    {
                                        actrec = BuildActivityRecurrence(recur, act);
                                    }
                                }
                                else
                                {
                                    actrec = BuildActivityRecurrence(recur, act);
                                }
                                Result.Add(actrec);
                            }
                            else
                            {
                                Activity acti = new Activity();
                                Recurrence recur = new Recurrence();
                                ActivityRecurrence actrec = new ActivityRecurrence();
                                if (act.RecurrenceID > 0)
                                {
                                    recur = facade.GetRecurrenceByRecurrenceID(act.RecurrenceID);
                                    if (recur != null)
                                    {
                                        actrec = BuildActivityRecurrence(recur, act);
                                    }
                                }
                                else
                                {
                                    actrec = BuildActivityRecurrence(recur, act);
                                }
                                Result.Add(actrec);
                            }
                        }
                    }
                }
            
        }
        catch(Exception ex)
        {
        }
       
        return Result;
    }



  	private void AddtoDataBase(Activity activity,int ResourseName)
        {
	    activity .ResourceName =ResourseName + "";
            IFacade facade = new Facade();
            facade.AddActivity(activity);
        }

        public void UpdateActivity(Activity activity,int ResourseName)
        {
            activity .ResourceName =ResourseName + "";
            IFacade facade = new Facade();
            facade.UpdateActivity(activity);
        }


  	 private IList <Activity >GetAppointmentsFromDatabase(int ResourseID)
        {
            IFacade facade = new Facade();
            IList<Activity> activity = new List<Activity>();
            activity = facade.GetAllActivityByResourceIdAndDate(ResourseID,  DateTime.Now.Date.AddMinutes  (-1).ToUniversalTime ().ToShortDateString ());
           return activity;
        }


 	private byte[] ConvertIListToByteArray<T>(IList <T> obj)
        {
            BinaryFormatter binary = new BinaryFormatter();
            MemoryStream mem = new MemoryStream();
            if (obj != null)
            {
                binary.Serialize(mem, obj);
            }
            byte[] byteResult = mem.ToArray();
            mem.Close();
            return byteResult;
        }
        private void ConvertByteArrayToIList<T>(byte[] byteArray,out IList <T> obj)
        {
          
                BinaryFormatter bf = new BinaryFormatter();
                using (MemoryStream ms = new MemoryStream(byteArray))
                {
                    obj = (IList<T>)bf.Deserialize(ms);
                }
               
           
        }





    private byte[] ConvertDataTableToByteArray(DataTable datatable)
    {
        BinaryFormatter binary = new BinaryFormatter();
        MemoryStream mem = new MemoryStream();
        if (datatable != null)
        {
            binary.Serialize(mem, datatable);
        }
        byte[] byteResult = mem.ToArray();
        mem.Close();
        return byteResult;
    }
    private DataTable ConvertByteArrayToDataTable(byte[] byteArray)
    {
        DataTable Result = new DataTable();
        BinaryFormatter bf = new BinaryFormatter();
        using (MemoryStream ms = new MemoryStream(byteArray))
        {
            Result = (DataTable)bf.Deserialize(ms);
        }
        return Result;
    }

        [WebMethod]
        public GetAttachments GetAttachments(GetAttachments value)
        {
            GetAttachments result = new GetAttachments();
            
            int memberid = value.memberId;
            int mailId = value.MailId;
            string AttachedFileNames = value.AttachedFileNames;
             string[] att = AttachedFileNames.Split(new string [] {";"},StringSplitOptions .RemoveEmptyEntries);
               string strFilePath = System.Web.Hosting.HostingEnvironment.MapPath("~/Resources/Member");
               strFilePath = strFilePath + "\\" + memberid + "\\Outlook Sync\\" + mailId + "\\";
         
               for (int i = 0; i < att.Length; i++)
               {
                   if (att[i].Contains("$$$"))
                   {
                       string[] filen = att[i].Split(new string[] { "$$$" }, StringSplitOptions.RemoveEmptyEntries);
                       FileStream fileS = new FileStream(strFilePath + att[i]+"\\"+filen [0], FileMode.Open, FileAccess.Read);
                       byte[] temp = new byte[fileS.Length];
                       fileS.Read(temp, 0, Convert.ToInt32(fileS.Length));
                       result.attachments[i] = temp;
                   }
                   else
                   {
                       FileStream fileS = new FileStream(strFilePath + att[i], FileMode.Open, FileAccess.Read);
                       byte[] temp = new byte[fileS.Length];
                       fileS.Read(temp, 0, Convert.ToInt32(fileS.Length));
                       result.attachments[i] = temp;
                   }
                   
               }
                
            return result;
        }
        [WebMethod]
        public GetValidSender GetValidSenderID(GetValidSender value)
        {
            GetValidSender objsender = new GetValidSender();
            IList<string> senderid = new List<string>();

            if (value.SenderId.Length > 0)
				ConvertByteArrayToIList(value.SenderId,out senderid );

            IList<string> dtresule = new List<string>();
            foreach (string email in senderid)
            {
                facade = new Facade();
                try
                {
                    if (facade.ValidateSenderEmailID(email) == true)
                    {  
                        dtresule.Add(email);
                    }
                }
                catch (Exception ex)
                {
                    objsender.errormessage = ex.Message + ex.Data;
                }
            }


			 byte[] se =ConvertIListToByteArray(dtresule);
             objsender.SenderId = se;
             return objsender;

        }

     


        [WebMethod]
        public SyncUploadDownloadDetails SentItemSync(SyncUploadDownloadDetails value)
        {
           return sentitem(value);
        }
        public SyncUploadDownloadDetails sentitem(SyncUploadDownloadDetails value)
        {
            SyncUploadDownloadDetails newobj = new SyncUploadDownloadDetails();
            MemberEmail _member = new MemberEmail();
            _member.SenderId = value.SenderID;
            _member.SenderEmail = value.SenderEmail;
            _member.ReceiverEmail = value.ReceiverEmail;
            _member.ReceiverId = value.ReceiverID;
            _member.IsRemoved = value.isRemoved;
            _member.ParentId = value.ParentId;
            _member.EmailTypeLookupId = value.EmailTypeLookupId;
            _member.Subject = value.Subject;
            _member.EmailBody = value.EmailBody;
            _member.Status = value.Status;
            _member.CreatorId = value.CreatorId;
            _member.BCC = value.BCC;
            _member.CC = value.CC;
           _member.SentDate = value.SentDate;
            _member.AttachedFileNames = value.AttachedFileNames;
            _member.NoOfAttachedFiles = value.Attcount;
            MemberEmail _memberemail = new MemberEmail();

            _memberemail = facade.AddMemberEmail(_member);

            int memberid = 0;

            if (value.mtype == MailType.SentMail) memberid = _memberemail.SenderId;
            else memberid = _memberemail.ReceiverId;

            int emailid = _memberemail.Id;
            int i = 0;
            if (value.Attcount > 0)
            {
                string strFilePath = System.Web.Hosting.HostingEnvironment.MapPath("~/Resources/Member");
                strFilePath = strFilePath + "\\" + memberid;
                if (!Directory.Exists(strFilePath))
                {
                    Directory.CreateDirectory(strFilePath);
                }

                strFilePath = strFilePath + "\\Outlook Sync";
                if (!Directory.Exists(strFilePath))
                {
                    Directory.CreateDirectory(strFilePath);
                }
                strFilePath = strFilePath + "\\" + emailid;
                if (!Directory.Exists(strFilePath))
                {
                    Directory.CreateDirectory(strFilePath);
                }
                for (i = 0; i < value.Attcount; i++)
                {
                    if (value.att[i] != null)
                    {
                        MemoryStream ms = new MemoryStream(value.att[i]);
                        FileStream fs = new FileStream
                       (strFilePath + "\\" + value.FileName[i], FileMode.Create);
                        ms.WriteTo(fs);
                        ms.Close();
                        fs.Close();
                        fs.Dispose();
                    }
                }
            }
            newobj.SyncStatus = true;
            return newobj;
        }
        
        [WebMethod]
        public GetAllMails GetByEmailAddressAndCreatedDate(GetAllMails value)
        {
            try
            { 

               // if (value.mail != null) return value;
                GetAllMails obj = new GetAllMails();
                IList<MemberEmail> _memberEmail=new List<MemberEmail>();

                try
                {
                    _memberEmail = facade.GetByEmailAddressAndCreatedDate(value.EmailAddress,value.CreatedDate);
                }
                catch(Exception ex)
                {
                    //System.IO.StreamWriter file = new System.IO.StreamWriter("C:\\Develop\\test2.txt");
                    //file.WriteLine(_memberEmail.Count + "" + ex.Message+ex.Data);file.Close();
                }


          
                  DataTable tosend = new DataTable();
        tosend.Columns.Add("Id");
        tosend.Columns.Add("SenderId");
        tosend.Columns.Add("SenderEmail");
        tosend.Columns.Add("ReceiverId");
        tosend.Columns.Add("ReceiverEmail");
        tosend.Columns.Add("EmailTypeLookupId");
        tosend.Columns.Add("Subject");
        tosend.Columns.Add("EmailBody");
        tosend.Columns.Add("Status");
        tosend.Columns.Add("ParentId");
        tosend.Columns.Add("IsRemoved");
        tosend.Columns.Add("CreatorId");
        tosend.Columns.Add("UpdatorId");
        tosend.Columns.Add("CreateDate");
        tosend.Columns.Add("UpdateDate");
        tosend.Columns.Add("SentDate");
         tosend.Columns.Add("BCC");
       tosend.Columns.Add("CC");
       tosend.Columns.Add("AttachedFileNames");
        tosend.Columns.Add("NoOfAttachedFiles");
     try
{
        foreach (MemberEmail mail in _memberEmail)
        {
            DataRow dr = tosend.NewRow();
            dr[0] = mail.Id;
            dr[1] = mail.SenderId;
            dr[2] = mail.SenderEmail;
            dr[3] = mail.ReceiverId;
            dr[4] = mail.ReceiverEmail;
            dr[5] = mail.EmailTypeLookupId;
            dr[6] = mail.Subject;
            dr[7] = mail.EmailBody;
            dr[8] = mail.Status;
            dr[9] = mail.ParentId;
            dr[10] = mail.IsRemoved;
            dr[11] = mail.CreatorId;
            dr[12] = mail.UpdatorId;
            dr[13] = mail.CreateDate.ToString();
            dr[14] = mail.UpdateDate.ToString();
            dr[15] = mail.SentDate.ToString();
            dr[16] = mail.BCC;
            dr[17] = mail.CC;
            dr[18] = mail.AttachedFileNames;
            dr[19] = mail.NoOfAttachedFiles;
            tosend.Rows.Add(dr);
        }

if(tosend.Rows.Count>0)
{
                DataTable dt = new DataTable();
                dt = ConvertTo<MemberEmail>(_memberEmail);
                BinaryFormatter binay = new BinaryFormatter();
                MemoryStream memory = new MemoryStream();
                binay.Serialize(memory, tosend);
                obj.mail = memory.ToArray();
}}
catch
{
}
                return obj;

            }
            catch(Exception e)
            {
//System.IO.StreamWriter file = new System.IO.StreamWriter("C:\\Develop\\test2.txt");
//file.WriteLine(e.Message+e.Data);file.Close();
              return value;
            }
        }
        public static DataTable ConvertTo<T>(IList<T> list)
        {
            DataTable table = CreateTable<T>();
            //DataTable table = new DataTable();
            Type entityType = typeof(T);
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entityType);

            foreach (T item in list)
            {
                DataRow row = table.NewRow();

                foreach (PropertyDescriptor prop in properties)
                {
                    row[prop.Name] = prop.GetValue(item);
                }

                table.Rows.Add(row);
            }

            return table;
        }
        public static DataTable CreateTable<T>()
        {
            Type entityType = typeof(T);
            DataTable table = new DataTable(entityType.Name);
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entityType);

            foreach (PropertyDescriptor prop in properties)
            {
                table.Columns.Add(prop.Name, prop.PropertyType);
            }

            return table;
        }
        
        [WebMethod]
        public CompanyDetails SyncCompany(CompanyDetails value)
        {
            IList<Company> CompanyFromOutlook = new List<Company>();
            IList<Company> CompanyFromOutlooks = new List<Company>();
            ConvertByteArrayToIList(value.Company, out CompanyFromOutlook);
            Member mem = new Member();
            mem = facade.GetMemberByMemberEmail(value.CurrentUserMailAddress);
            ArrayList MemberPrivilege = facade.GetAllMemberPrivilegeIdsByMemberId(mem.Id);
            if (MemberPrivilege.Contains(406))
            {
                CompanyFromOutlooks = AddCompanyAndGetID(CompanyFromOutlook, mem.Id);
              
                CompanyDetails Results = new CompanyDetails();
                Results.Company = ConvertIListToByteArray(CompanyFromOutlooks);
                return Results;
            }
            else
                return value;
        }

        [WebMethod]
        public ContactDetails SyncContact(ContactDetails value)
        {
            IList<CompanyContact> ContactFromOutlook = new List<CompanyContact>();
            ConvertByteArrayToIList(value.Contact, out ContactFromOutlook);
            IList<Company> CompanyFromOutlook = new List<Company>();
            IFacade facade = new Facade();
            IList<CompanyContact> ContactFromDataBase = new List<CompanyContact>();
            Member mem = new Member();
            mem = facade.GetMemberByMemberEmail(value.CurrentUserMailAddress);
            ArrayList MemberPrivilege = facade.GetAllMemberPrivilegeIdsByMemberId(mem.Id);
            if (MemberPrivilege.Contains(406))
            {
                ContactFromDataBase = GetContactFromDataBase(value.CurrentUserMailAddress);
                IList<CompanyContact> DuplicateCheck = new List<CompanyContact>();
                ContactFromOutlook = AddContactToDataBase(ContactFromDataBase, ContactFromOutlook, mem.Id, out DuplicateCheck);
                IList<CompanyContact> ResultContact = CollectContactFromDataBase(ContactFromOutlook, value.CurrentUserMailAddress, out CompanyFromOutlook, DuplicateCheck);
                ContactDetails Result = new ContactDetails();
                Result.Company = ConvertIListToByteArray(CompanyFromOutlook);
                Result.Contact = ConvertIListToByteArray(ResultContact);
                return Result;
            }
            else
                return value;
       }
        [WebMethod]
        public Recurrances GetRecurranceById(string  value)
        {
            Recurrances recurrancess = new Recurrances();
            try
            {
                IList<Recurrence> rec = new List<Recurrence>();
                
                if (value != string.Empty)
                {
                    rec.Add(facade.GetRecurrenceByRecurrenceID(Convert.ToInt32(value)));
                }
                recurrancess.Recurran = ConvertIListToByteArray(rec);
            }
            catch
            {
            }
            return recurrancess;
        }
        [WebMethod]
        public Recurrances GetRecurrance(Recurrances value)
        {
            IList<Recurrence> rec = new List<Recurrence>();
            Recurrances recurrancess = new Recurrances();
            ConvertByteArrayToIList(value.Recurran, out rec);
            IList<Recurrence> recur = new List<Recurrence>();
            foreach (Recurrence recurran in rec)
            {
                if (recurran.RecurrenceID != 0)
                {
                    try
                    {
                        recur.Add(facade.UpdateRecurrence(recurran));
                    }
                    catch
                    {
                    }
                }
                else
                {
                    try
                    {
                        recur.Add(facade.AddRecurrence(recurran));
                    }
                    catch
                    {
                    }
                }
            }
            recurrancess.Recurran = ConvertIListToByteArray(recur);
            return recurrancess;
        }
        private IList<CompanyContact> CollectContactFromDataBase(IList<CompanyContact> ContactFromOutlook, string CurrentUserMailAddress, out IList<Company> CompanyFromOutlook,IList <CompanyContact > Duplicates)
        {

                IList<CompanyContact> ResultContact = new List<CompanyContact>();
                IList<Company> ResultCompany = new List<Company>();
                IList<CompanyContact> ContactFromDataBase = GetContactFromDataBase(CurrentUserMailAddress);
                IFacade facade = new Facade();
               
                if (ContactFromDataBase != null)
                {
                    
                    foreach (CompanyContact contdb in ContactFromDataBase)
                    {
                        
                        if (ContactFromOutlook.Count  != 0)
                        {

                                var cont = from a in ContactFromOutlook
                                           //where a.Email.ToLower().Trim() == (contdb.Email.ToLower().Trim() == null ? "" : contdb.Email.ToLower().Trim()) && a.FirstName.ToLower().Trim() == (contdb.FirstName.ToLower().Trim() == null ? "" : contdb.FirstName.ToLower().Trim())
                                           where a.Id == contdb.Id 
                                           select a;
                           
           
                            if (cont.ToArray().Count() == 0)
                            {
                                if (Duplicates.Where(x => x.Id == contdb.Id).Count() > 0)
                                {
                                    contdb.IsOwner = true;
                                    ResultContact.Add(contdb);
                                }
                                else 
                                ResultContact.Add(contdb);
                                var company = from b in ResultCompany
                                              where b.Id == contdb.CompanyId
                                              select b;
                                if (company.ToArray().Count() == 0)
                                    ResultCompany.Add(facade.GetCompanyById(contdb.CompanyId));
                            }
                            else
                            {

                                foreach (CompanyContact outcont in cont.ToArray())
                                {
                                    if (contdb.UpdateDate >= outcont.UpdateDate || contdb.CreateDate >= outcont.CreateDate)
                                    {
                                        if (Duplicates.Where(x => x.Id == contdb.Id).Count() > 0)
                                        {
                                            contdb.IsOwner = true;
                                            ResultContact.Add(contdb);
                                        }
                                        else
                                            ResultContact.Add(contdb);
                                        var company = from b in ResultCompany
                                                      where b.Id == contdb.CompanyId
                                                      select b;
                                        if (company.ToArray().Count() == 0)
                                            ResultCompany.Add(facade.GetCompanyById(contdb.CompanyId));
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (Duplicates.Where(x => x.Id == contdb.Id).Count() > 0)
                            {
                                contdb.IsOwner = true;
                                ResultContact.Add(contdb);
                            }
                            else
                            ResultContact.Add(contdb);
                            var company = from b in ResultCompany
                                          where b.Id == contdb.CompanyId
                                          select b;
                            if (company.ToArray().Count() == 0)
                                ResultCompany.Add(facade.GetCompanyById(contdb.CompanyId));
                        }
                           
                    }
                            
                }
            CompanyFromOutlook = ResultCompany;
            return ResultContact;

        }
        private int RegisterUser(CompanyContact cont,int _memberId)
        {
            string strMessage = String.Empty;
            bool IsCompany = false;
            TPS360 .Web .UI .Helper . SecureUrl url;
            string SecureURL;
            Member newMember = new Member();
                //if (_role != "")
                {
                    try
                    {
                        MembershipUser newUser;

                        newUser = Membership.CreateUser(cont .Email .Trim (), "vendoraccess", cont .Email .Trim ());
                        if (newUser.IsApproved == true)
                        {
                            Roles.AddUserToRole(newUser.UserName,"Vendor");

                            
                            newMember.UserId = (Guid)newUser.ProviderUserKey;
                            newMember.ResumeSource = Convert.ToInt32(ResumeSource.SelfRegistration);
                            newMember.PermanentCountryId = 0;
                            newMember.PermanentStateId = 0;
                            newMember.IsRemoved = false;
                            newMember.AutomatedEmailStatus = true;
                            newMember.FirstName = cont.FirstName.Trim();
                            newMember.LastName = cont .LastName .Trim ();
                            newMember.DateOfBirth = DateTime.MinValue;
                            newMember.PrimaryEmail = newUser.Email;
                            newMember.Status = (int)MemberStatus.Active;
                            string UserRole = facade.GetCustomRoleNameByMemberId(_memberId);
                            if (UserRole == ContextConstants.ROLE_EMPLOYEE)
                            {
                                newMember.ResumeSource = (int)ResumeSource.Employee;
                            }
                            else if (UserRole == ContextConstants.ROLE_CLIENT)
                            {
                                IsCompany = true;
                                newMember.ResumeSource = (int)ResumeSource.Client;
                            }
                            else if (UserRole == ContextConstants.ROLE_VENDOR)
                            {
                                IsCompany = true;
                                newMember.ResumeSource = (int)ResumeSource.Supplier;
                            }
                            else if (UserRole == ContextConstants.ROLE_ADMIN)
                            {
                                newMember.ResumeSource = (int)ResumeSource.Admin;
                            }
                            newMember.CreatorId = _memberId;
                            newMember.CreateDate = DateTime.Now;
                            newMember.UpdatorId = _memberId;
                            newMember.UpdateDate = DateTime.Now;

                            newMember = facade.AddMember(newMember);
                            
                            Resource newResource = new Resource();
                            newResource.EmailAddress = newMember.PrimaryEmail;
                            newResource.ResourceName = newMember.Id.ToString();
                            facade.AddResource(newResource);
                            //If not a candidate registratiion
                            if ("vendor" != ContextConstants.ROLE_CANDIDATE.ToLower())
                            {
                                MemberDetail newMemberDetail = new MemberDetail();
                                newMemberDetail.MemberId = newMember.Id;
                                newMemberDetail.CurrentStateId = 0;
                                newMemberDetail.CurrentCountryId = 0;
                                newMemberDetail.GenderLookupId = 0;
                                newMemberDetail.EthnicGroupLookupId = 0;
                                newMemberDetail.MaritalStatusLookupId = 0;
                                newMemberDetail.NumberOfChildren = 0;
                                newMemberDetail.BloodGroupLookupId = 0;
                                newMemberDetail.AnniversaryDate = DateTime.MinValue;

                                facade.AddMemberDetail(newMemberDetail);

                                //--Trace
                                MemberExtendedInformation memberExtended = new MemberExtendedInformation();
                                memberExtended.MemberId = newMember.Id;
                                memberExtended.Availability = ContextConstants.MEMBER_DEFAULT_AVAILABILITY;
                                facade.AddMemberExtendedInformation(memberExtended);

                            }
                            //  Trace Tech
                            MemberObjectiveAndSummary m_memberObjectiveAndSummary = new MemberObjectiveAndSummary();
                            m_memberObjectiveAndSummary.CreatorId = _memberId;
                            m_memberObjectiveAndSummary.UpdatorId = _memberId;
                            m_memberObjectiveAndSummary.CopyPasteResume = "";
                            m_memberObjectiveAndSummary.CreateDate = DateTime.Today;
                            m_memberObjectiveAndSummary.UpdateDate = DateTime.Today;
                            m_memberObjectiveAndSummary.RawCopyPasteResume = "";// StripTagsCharArray(m_memberObjectiveAndSummary.CopyPasteResume);
                            m_memberObjectiveAndSummary.MemberId = newMember.Id;
                            facade.AddMemberObjectiveAndSummary(m_memberObjectiveAndSummary);

                           
                            //Add to self as manager                            
                            if (UserRole == ContextConstants.ROLE_EMPLOYEE || UserRole == ContextConstants.ROLE_ADMIN) //0.3
                            {
                                MemberManager memberManager = new MemberManager();
                                memberManager.IsPrimaryManager = true;
                                memberManager.ManagerId = _memberId;
                                memberManager.MemberId = newMember.Id;
                                memberManager.CreateDate = DateTime.Now;
                                memberManager.CreatorId = _memberId;
                                memberManager.UpdateDate = DateTime.Now;
                                memberManager.UpdatorId = _memberId;
                                facade.AddMemberManager(memberManager);
                            }

                            //MiscUtil.AddActivity("Vendor", newMember .Id , _memberId , ActivityType.AddMember, Facade);

                            MemberCustomRoleMap map = facade.GetMemberCustomRoleMapByMemberId(newMember.Id);
                            CustomRole role = facade.GetCustomRoleByName("Vendor");
                            int roleid = 0;
                            if (role != null)
                                roleid = role.Id;

                            if (map == null)
                            {
                                map = new MemberCustomRoleMap();
                                map.CustomRoleId = roleid;
                                map.MemberId = newMember.Id;
                                map.CreatorId = _memberId;

                                facade.AddMemberCustomRoleMap(map);
                                AddDefaultMenuAccess(newMember.Id, roleid);
                            }
                            else
                            {
                                if (map.CustomRoleId == roleid)
                                {
                                    map = new MemberCustomRoleMap();
                                    map.CustomRoleId = roleid;
                                    map.UpdatorId = _memberId;
                                    facade.UpdateMemberCustomRoleMap(map);
                                    AddDefaultMenuAccess(newMember.Id, roleid);
                                }
                            }
                        }
                        newUser.IsApproved = false;
                        Membership.UpdateUser(newUser);
                    }
                    catch (MembershipCreateUserException ex)
                    {

                    }

                }
                return newMember.Id;
          
        }
        private void AddDefaultMenuAccess(int memId, int role)
        {

            ArrayList previlegeList = facade.GetAllCustomRolePrivilegeIdsByRoleId(role);

            if (previlegeList != null && previlegeList.Count > 0)
            {
                facade.DeleteMemberPrivilegeByMemberId(memId);

                for (int i = 0; i <= previlegeList.Count - 1; i++)
                {
                    MemberPrivilege previlege = new MemberPrivilege();

                    previlege.CustomSiteMapId = Convert.ToInt32(previlegeList[i]);
                    previlege.MemberId = memId;
                    facade.AddMemberPrivilege(previlege);
                }
            }
        }
        private IList<CompanyContact> AddContactToDataBase(IList<CompanyContact> ContactFromDB, IList<CompanyContact> ContactFromOutlook, int MemberId,out IList <CompanyContact > Duplicates)
        {
            //System.IO.StreamWriter file = new System.IO.StreamWriter("D:\\WebSite1\\test1.txt",true );
            //file.WriteLine(e.Message+e.Data);file.Close();
            Duplicates = new List<CompanyContact>();
            IList<CompanyContact> rsult = new List<CompanyContact>();
            try
            {
                foreach (CompanyContact outlook in ContactFromOutlook)
                {
                    if (ContactFromDB != null)
                    {
                        if (outlook.Id != 0)
                        {
                            var cont = from a in ContactFromDB
                                       where a.Id == outlook.Id
                                       select a;
                            var d = from h in ContactFromDB
                                    where h.Email.ToLower().Trim() == (outlook.Email.ToLower().Trim() == null ? "" : outlook.Email.ToLower().Trim()) && h.Email.ToString() != ""
                                    select h;
                            if (d.ToArray().Count() == 0)
                            {
                                if (cont.ToArray().Count() == 0)
                                {
                                    IFacade facade = new Facade();
                                    int _mem=0;
                                    //outlook.MemberId = MemberId;
                                    outlook.CreatorId = MemberId;
                                    outlook.UpdatorId = MemberId;
                                    try
                                    {
                                        var duplicate = from c in ContactFromDB
                                                        where c.FirstName.ToLower().Trim() == (outlook.FirstName.ToLower().Trim() == null ? "" : outlook.FirstName.ToLower().Trim()) && c.LastName.ToLower().Trim() == (outlook.LastName.ToLower().Trim() == null ? "" : outlook.LastName.ToLower().Trim()) && c.Email.ToLower().Trim() == (outlook.Email.ToLower().Trim() == null ? "" : outlook.Email.ToLower().Trim())
                                                        select c;
                                        if (duplicate.ToArray().Count() == 0)
                                        {
                                            var check = from b in ContactFromDB
                                                        where b.CompanyId == outlook.CompanyId && b.IsPrimaryContact == true
                                                        select b;
                                            if (check.ToArray().Count() == 0)
                                                outlook.IsPrimaryContact = true;
                                            try
                                            {
                                                CompanyContact con = new CompanyContact();
                                                MembershipUser user = Membership.GetUser(outlook .Email .Trim ());
                                                if (user == null)
                                                  _mem=  RegisterUser(outlook,MemberId );
                                                outlook.MemberId = _mem;
                                                con = facade.AddCompanyContact(outlook);
                                                if (outlook.Id != 0) Duplicates.Add(con);
                                                rsult.Add(con );
                                            }
                                            catch (Exception ex)
                                            {

                                            }
                                        }

                                        else
                                        {
                                            foreach (CompanyContact dbcont in cont.ToArray())
                                            {
                                                if (outlook.UpdateDate >= dbcont.UpdateDate || outlook.CreateDate >= dbcont.CreateDate)
                                                {

                                                    outlook.Id = dbcont.Id;
                                                    outlook.MemberId = dbcont.MemberId;
                                                    outlook.UpdatorId = MemberId;
                                                    var check = from b in ContactFromDB
                                                                where b.CompanyId == outlook.CompanyId && b.IsPrimaryContact == true
                                                                select b;
                                                    if (check.ToArray().Count() != 0)
                                                        outlook.IsPrimaryContact = true;
                                                    else
                                                    {
                                                        IList<CompanyContact> cc = new List<CompanyContact>();
                                                        cc = facade.GetAllCompanyContactByComapnyId(outlook.CompanyId);
                                                        if (cc == null)
                                                            outlook.IsPrimaryContact = true;
                                                    }
                                                    rsult.Add(facade.UpdateCompanyContact(outlook));

                                                }
                                            }
                                        }
                                    }
                                    catch
                                    {
                                    }
                                }

                                else
                                {
                                    foreach (CompanyContact dbcont in cont.ToArray())
                                    {
                                        if (outlook.UpdateDate >= dbcont.UpdateDate || outlook.CreateDate >= dbcont.CreateDate)
                                        {
                                            IFacade facade = new Facade();
                                            outlook.Id = dbcont.Id;
                                            outlook.MemberId = dbcont.MemberId;
                                            outlook.UpdatorId = MemberId;
                                            var check = from b in ContactFromDB
                                                        where b.CompanyId == outlook.CompanyId && b.IsPrimaryContact == true
                                                        select b;
                                            if (check.ToArray().Count() != 0)
                                                outlook.IsPrimaryContact = true;
                                            else
                                            {
                                                IList<CompanyContact> cc = new List<CompanyContact>();
                                                cc = facade.GetAllCompanyContactByComapnyId(outlook.CompanyId);
                                                if (cc == null)
                                                    outlook.IsPrimaryContact = true;
                                            }
                                            //outlook = BuildContact(dbcont, outlook);
                                            rsult.Add(facade.UpdateCompanyContact(outlook));
                                        }
                                    }
                                }
                            }
                            else
                            {
                                foreach (CompanyContact dbcont in d.ToArray())
                                {
                                    if (outlook.UpdateDate >= dbcont.UpdateDate || outlook.CreateDate >= dbcont.CreateDate)
                                    {
                                        IFacade facade = new Facade();
                                        outlook.Id = dbcont.Id;
                                        outlook.MemberId = dbcont .MemberId ;
                                        outlook.UpdatorId = MemberId;
                                        var check = from b in ContactFromDB
                                                    where b.CompanyId == outlook.CompanyId && b.IsPrimaryContact == true
                                                    select b;
                                        if (check.ToArray().Count() != 0)
                                            outlook.IsPrimaryContact = true;
                                        else
                                        {
                                            IList<CompanyContact> cc = new List<CompanyContact>();
                                            cc = facade.GetAllCompanyContactByComapnyId(outlook.CompanyId);
                                            if (cc == null )
                                                outlook.IsPrimaryContact = true;
                                        }
                                        //outlook = BuildContact(dbcont, outlook);
                                        rsult.Add(facade.UpdateCompanyContact(outlook));
                                    }
                                }
                            }
                        }
                        else
                        {
                            IFacade facade = new Facade();
                            var c = from h in ContactFromDB
                                    where h.Email.ToLower().Trim() == (outlook.Email.ToLower().Trim() == null ? "" : outlook.Email.ToLower().Trim()) && h.Email .ToString ()!=""
                                    select h;
                            var cont = from a in ContactFromDB
                                       where a.FirstName.ToLower().Trim() == (outlook.FirstName.ToLower().Trim() == null ? "" : outlook.FirstName.ToLower().Trim()) && a.LastName.ToLower().Trim() == (outlook.LastName.ToLower().Trim() == null ? "" : outlook.LastName.ToLower().Trim()) && a.Email.ToLower().Trim() == (outlook.Email.ToLower().Trim() == null ? "" : outlook.Email.ToLower().Trim())
                                       select a;
                           
                            if (c.ToArray().Count() == 0)
                            {
                                if (cont.ToArray().Count() == 0)
                                {
                                    int _mem = 0;
                                    outlook.UpdatorId = MemberId;
                                    outlook.CreatorId = MemberId;
                                    var check = from b in ContactFromDB
                                                where b.CompanyId == outlook.CompanyId && b.IsPrimaryContact == true
                                                select b;
                                    if (check.ToArray().Count() == 0)
                                        outlook.IsPrimaryContact = true;
                                    try
                                    {
                                        MembershipUser user = Membership.GetUser(outlook.Email.Trim());
                                        if (user == null)
                                         _mem =RegisterUser(outlook, MemberId);
                                        outlook.MemberId = _mem;
                                        rsult.Add(facade.AddCompanyContact(outlook));
                                    }
                                    catch
                                    {
                                    }
                                }
                                else
                                {
                                    foreach (CompanyContact dbcont in cont.ToArray())
                                    {
                                        if (outlook.UpdateDate >= dbcont.UpdateDate || outlook.CreateDate >= dbcont.CreateDate)
                                        {
                                            outlook.Id = dbcont.Id;
                                            outlook.MemberId = dbcont .MemberId ;
                                            outlook.UpdatorId = MemberId;
                                            var check = from b in ContactFromDB
                                                        where b.CompanyId == outlook.CompanyId && b.IsPrimaryContact == true
                                                        select b;
                                            if (check.ToArray().Count() != 0)
                                                outlook.IsPrimaryContact = true;
                                            else
                                            {
                                                IList<CompanyContact> cc = new List<CompanyContact>();
                                                cc = facade.GetAllCompanyContactByComapnyId(outlook.CompanyId);
                                                if (cc == null)
                                                    outlook.IsPrimaryContact = true;
                                            }
                                            //outlook = BuildContact(dbcont, outlook);
                                            rsult.Add(facade.UpdateCompanyContact(outlook));
                                        }
                                    }
                                }
                            }

                            else
                            {
                                foreach (CompanyContact dbcont in c.ToArray())
                                {
                                    if (outlook.UpdateDate >= dbcont.UpdateDate || outlook.CreateDate >= dbcont.CreateDate)
                                    {
                                        outlook.Id = dbcont.Id;
                                        outlook.MemberId = dbcont .MemberId ;
                                        outlook.UpdatorId = MemberId;
                                        var check = from b in ContactFromDB
                                                    where b.CompanyId == outlook.CompanyId && b.IsPrimaryContact == true
                                                    select b;
                                        if (check.ToArray().Count() != 0)
                                            outlook.IsPrimaryContact = true;
                                        else
                                        {
                                            IList<CompanyContact> cc = new List<CompanyContact>();
                                            cc = facade.GetAllCompanyContactByComapnyId(outlook.CompanyId);
                                            if (cc == null)
                                                outlook.IsPrimaryContact = true;
                                        }
                                        //outlook = BuildContact(dbcont, outlook);
                                        rsult.Add(facade.UpdateCompanyContact(outlook));
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        IFacade facade = new Facade();
                        int _mem = 0;
                        outlook.UpdatorId = MemberId;
                        outlook.CreatorId = MemberId;
                        outlook.IsPrimaryContact = true;
                        try
                        {
                            CompanyContact con = new CompanyContact();
                            MembershipUser user = Membership.GetUser(outlook.Email.Trim());
                            if (user == null)
                              _mem = RegisterUser(outlook, MemberId);
                            outlook.MemberId = _mem;
                            con = facade.AddCompanyContact(outlook);
                            if (outlook.Id != 0) Duplicates.Add(con);
                            rsult.Add(con);
                        }
                        catch
                        {
                        }

                    }
                }
            }
            catch (Exception ex)
            {


            }
            //file.Close();
            return rsult;
        }
        private IList<CompanyContact> GetContactFromDataBase(string CurrentUserMailAddress)
        {
            IFacade facade = new Facade();
            Member _member = new Member();
            _member = facade.GetMemberByMemberEmail(CurrentUserMailAddress);
            return facade.GetAllContactByIdAndDate(_member.Id, DateTime.Now.AddYears (-1).ToString("MM/dd/yyyy"));//LastSync.ToString("MM/dd/yyyy"));

        }
        private void AssignCompanyManager(int CompanyId,int MemberId)
        {
            CompanyAssignedManager companyTeam = new CompanyAssignedManager();
            companyTeam = facade.GetCompanyAssignedManagerByCompanyIdAndMemberId(CompanyId ,MemberId );
            if (companyTeam == null)
            {
                companyTeam = new CompanyAssignedManager();
                companyTeam.CompanyId = CompanyId;
                companyTeam.MemberId = MemberId;
                companyTeam.IsPrimaryManager = true;
                companyTeam.CreatorId = MemberId;
                try
                {
                    facade.AddCompanyAssignedManager(companyTeam);
                }
                catch { }
            }
        }
        private IList<Company> AddCompanyAndGetID(IList<Company> companyList,int MemberId)
        {
            IList<Company> ResultCompany = new List<Company>();
            IFacade facade = new Facade();
            
                foreach (Company com in companyList)
                {
                    Company res = new Company();
                    com.CreatorId = MemberId;
                    com.UpdatorId = MemberId;
                    com.IsEndClient = true;
                    int ID = facade.GetCompanyIdByName(com.CompanyName);
                    if (ID == 0)
                    {
                    res = facade.AddCompany(com, CompanyStatus.Client, false);
                    AssignCompanyManager(res.Id, MemberId);
                    facade.UpdateCompanyStatus(res.Id, CompanyStatus.Client);
                    }//CompanyStatus.Client
                    else { res.Id = ID; res.CompanyName = com.CompanyName; }
                    ResultCompany.Add(res);
                }
          
           
            return ResultCompany;
            
        }



        public SyncUploadDownloadDetails receiveditem(SyncUploadDownloadDetails value)
        {
            return value;
        }

        public struct ContactDetails
        {
            public string CurrentUserMailAddress;
            public byte[] Contact;
            public byte[] Company;
            //public DateTime LastSyncDate;
        }
        public struct CompanyDetails
        {
            public string CurrentUserMailAddress;
            public byte[] Company;
        }
        

    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.1432")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
    public partial class SyncUploadDownloadDetails
    {
        public byte[][] att;
        public MailType mtype;
        public bool SyncStatus;
        public int SenderID;
        public string SenderEmail;
        public string ReceiverEmail;
        public int ReceiverID;
        public bool isRemoved;
        public int ParentId;
        public int EmailTypeLookupId;
        public string Subject;
        public string EmailBody;
        public int Status;
        public int CreatorId;
        public int Attcount;
        public string BCC;
        public string CC;
        public string SentDate;
        public string[] FileName;
        public string AttachedFileNames;

    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.1432")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
    public partial class GetAllMails
    {
        public string CreatedDate;
        public string EmailAddress;
        public byte[] mail;
        public int Id;

    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.1432")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
    public enum MailType
    {
        SentMail,
        ReceivedMail,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.1432")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
    public partial class GetValidSender
    {
        public byte[] SenderId;
        public string errormessage;
    }

      [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.1432")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
    public partial class GetAttachments
    {
        public int memberId;
        public int MailId;
      
        public string AttachedFileNames;
        public byte[][] attachments = new byte[100][];
    }


  [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.1432")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
    public enum OlBusyStatus
    {
        Free ,
        Tentative,
        Busy,
        OutOfOffice
    }



  /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.1432")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
    public partial class ScheduleDetails
    {
        public string CurrentUserMailAddress;
          public byte[] Appointments;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.1432")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
    public partial class Recurrances
    {

        public byte[] Recurran;
    }

}