﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Common;
using System.Web.Configuration;

/// <summary>
/// Summary description for GettingCommonValues
/// </summary>
public static class GettingCommonValues
{
    

    public static string GetDomainName()
    {
        string DomainName = "";
        try
        {
            TPS360SectionHandler licenseSection = WebConfigurationManager.GetSection("tps360license") as TPS360SectionHandler;
           
            if (licenseSection != null)
            {
                try
                {
                    DomainName = (licenseSection.Details["DomainName"].Value != null) ? licenseSection.Details["DomainName"].Value : String.Empty;
                }
                catch { }
            }
        }
        catch { }
        return DomainName;
    }
}

