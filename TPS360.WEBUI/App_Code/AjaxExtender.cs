﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using TPS360.Common.Shared;
using System.Web.UI;
using System.Linq;
namespace TPS360.Web.UI
{
    /// <summary>
    /// Summary description for AjaxExtender
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]
    public class AjaxExtender : System.Web.Services.WebService
    {

       
        [WebMethod]
        public string[] GetSkills(string prefixText)
        {
            List<string> items = new List<string>();
            IFacade facade = new Facade();
            IList<Skill> skillList = facade.GetAllSkillsBySearch (prefixText ,8);
            if (skillList == null) return null;
            foreach (Skill s in skillList)
                items.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(s.Name ,s.Id .ToString() ));
           return  items.ToArray();
        }

        [WebMethod]
        public string[] GetClients(string prefixText)
        {
            List<string> items = new List<string>();
            IFacade facade = new Facade();
            IList<Company> company = facade.Company_GetPaged(prefixText, 8);
            foreach (Company com in company)
                items.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(com.CompanyName ,com .Id .ToString ()));
            if (items.Count() < 8)
            {
                IList<Company> company1 = facade.Company_GetPaged("%"+prefixText, 8-items .Count() );
                foreach (Company com in company1)
                {
                    if(company.Where (x=>x.Id ==com.Id ).Count ()==0)
                    items.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(com.CompanyName, com.Id.ToString()));
                }

            }
            return items .ToArray ();
        }
        [WebMethod]
        public string[] GetRequisition(string prefixText, string contextKey)
        {
            char[] delim={'|'};
            string[] contextarray = contextKey.Split(delim, StringSplitOptions.RemoveEmptyEntries);

            List<string > items = new List<string >();
           IFacade facade=new Facade ();
            // int JobStatusid=0;
            //IList<GenericLookup> RequisitionStatusList = facade.GetAllGenericLookupByLookupTypeAndName(LookupType.RequisitionStatus, JobStatus.Open.ToString ());
            //if (RequisitionStatusList != null)
            //{ 
            //     JobStatusid = RequisitionStatusList[0].Id;
            //}


           ArrayList jobposting = null;// facade.GetPagedJobPostingByStatusAndManagerId(JobStatusid, Convert.ToInt32(contextKey), 8, prefixText);
               jobposting = facade.GetJobPostingByClientIdAndManagerId(Convert.ToInt32(contextarray[1]), Convert.ToInt32(contextarray[0]), 8, prefixText);
          
                string[] jobs = new string[jobposting.Count];
            
                foreach (JobPosting j in jobposting)
                    items .Add (AjaxControlToolkit .AutoCompleteExtender .CreateAutoCompleteItem (j.JobTitle ,j.Id  .ToString ()));
                if (items.Count < 8)
                {
                    ArrayList jobposting1 = facade.GetJobPostingByClientIdAndManagerId(Convert.ToInt32(contextarray[1]), Convert.ToInt32(contextarray[0]), 8 - items.Count, "%" + prefixText);//facade.GetPagedJobPostingByStatusAndManagerId(JobStatusid, Convert.ToInt32(contextKey), 8-items .Count , "%"+prefixText);
                    foreach (JobPosting j in jobposting1)
                    {
                        if(!items .Contains (AjaxControlToolkit .AutoCompleteExtender .CreateAutoCompleteItem (j.JobTitle ,j .Id .ToString ())))
                        items.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(j.JobTitle, j.Id.ToString()));
                    }
                }


                return items.ToArray();
          
        }
        [WebMethod]
        public string[] GetHotListItems(string prefixText, string contextKey)
        {
            List<string> items = new List<string>();
            try
            {
                PagedRequest request = new PagedRequest();
                request.PageIndex = 0;
                request.RowPerPage = 8;
                request.Conditions.Add("GroupType", ((int)MemberGroupType.Candidate).ToString());
                if (contextKey != "0") request.Conditions.Add("MemberId", contextKey);
                request.Conditions.Add("SearchKey", prefixText);
                IFacade facade = new Facade();
                PagedResponse<MemberGroup> memGroup = facade.GetPagedMemberGroupByMemberIdandGroupType(request);
                if (memGroup.Response != null)
                {
                    if (memGroup.Response.Count > 0)
                    {
                        foreach (MemberGroup m in memGroup.Response as List<MemberGroup>)
                        {
                            items.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(m.Name , m.Id.ToString()));
                        }
                        return items.ToArray ();
                    }

                }

                return null;
            }
            catch
            {
                return null;
            }

        }
        [WebMethod]
        public string[] GetHotList(string prefixText, string contextKey)
        {
            
            try
            {
                PagedRequest request = new PagedRequest();
                request.PageIndex = 0;
                request.RowPerPage = 8;
                request.Conditions.Add("GroupType", ((int)MemberGroupType.Candidate).ToString());
             if(contextKey!="0")   request.Conditions.Add("MemberId", contextKey);
                request.Conditions.Add("SearchKey", prefixText);
                IFacade facade = new Facade();
                PagedResponse<MemberGroup> memGroup = facade.GetPagedMemberGroupByMemberIdandGroupType(request);
                if (memGroup.Response != null)
                {
                    if (memGroup.Response.Count > 0)
                    {
                        string[] hotList = new string[memGroup.Response.Count];
                        int i = 0;
                        foreach (MemberGroup m in memGroup.Response as List<MemberGroup>)
                        {
                            hotList[i] = m.Name;
                            i++;
                        }
                        return hotList;
                    }

                }

                return null;
            }
            catch
            {
                return null;
            }

        }
     


        [WebMethod]
        public string[] GetDepartments(string prefixText)
        {
            try
            {
                IFacade facade = new Facade();

                IList<Department> departmentList = facade.GetAllDepartmentBySearch(prefixText, 8);
                string[] departmentNames = new string[departmentList.Count()];

                int i = 0;
                foreach (Department s in departmentList)
                {
                    departmentNames[i] = s.Name;
                    i++;
                }
                return departmentNames;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [WebMethod]
        public string[] GetLocationName(string prefixText)
        {
            List<string> items = new List<string>();
            IFacade facade = new Facade();
            IList<string> CityList = facade.GetAllCity(prefixText);
            if (CityList == null) return null;
            foreach (string s in CityList)
                items.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(s, "0"));
            return items.ToArray();
        }

        
        [WebMethod]
        public string[] GetContact(string prefixText, string contextKey)
       {
            List<string> items = new List<string>();
            IFacade facade = new Facade();
            
            TPS360.Common.Shared.PagedRequest obj = new PagedRequest();
            obj.Conditions.Add("CompanyId", contextKey);
            obj.Conditions.Add("contactPrefix", prefixText);
            obj.RowPerPage = 8;
            IList<CompanyContact> companycontact = facade.GetPagedCompanyContact(obj).Response as List<CompanyContact>;
            foreach (CompanyContact com in companycontact)
                items.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(com.FirstName+" "+com.LastName, com.Id.ToString()));
           
            return items.ToArray();
        }

    }

}