/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: SavedQueryDataSource.cs
    Description: This page is used to access saved queries.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date               Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------    
    0.1            Nov-18-2008        Shivanand           Defect #8670; New method added "GetPagedConsultantPreviousSearch".
-------------------------------------------------------------------------------------------------------------------------------------------        
*/


using System;
using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;
using System.Web.UI;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;


namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class SavedQueryDataSource : ObjectDataSourceBase
    {
        PagedResponse<SavedQuery> pageResponse = null;

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(string creatorId)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<SavedQuery> GetPaged(string creatorId, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                if (!string.IsNullOrEmpty(creatorId))
                {
                    base.AppendParameter(pageRequest, "creatorId", creatorId);
                }
                
                base.AppendParameter(pageRequest, "QueryType", (int)SearchQuery.ATSPreciseSearch);              

                pageResponse = Facade.GetPagedSavedQuery(pageRequest);
                return pageResponse.Response as List<SavedQuery>;
            }
        }


    // 0.1 starts.

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<SavedQuery> GetPagedConsultantPreviousSearch(string creatorId, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                if (!string.IsNullOrEmpty(creatorId))
                {
                    base.AppendParameter(pageRequest, "creatorId", creatorId);
                }

                base.AppendParameter(pageRequest, "QueryType", (int)SearchQuery.ConsultantPreciseSearch);              

                pageResponse = Facade.GetPagedSavedQuery(pageRequest);
                return pageResponse.Response as List<SavedQuery>;
            }
        }

    // 0.1 ends

    }
}