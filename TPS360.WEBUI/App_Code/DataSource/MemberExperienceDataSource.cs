using System;
using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class MemberExperienceDataSource : ObjectDataSourceBase
    {
        PagedResponse<MemberExperienceDetail> pageResponse = null;

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount()
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(int memberId)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<MemberExperienceDetail> GetPaged(string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageResponse = Facade.GetPagedMemberExperienceDetail(pageRequest);
                return pageResponse.Response as List<MemberExperienceDetail>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<MemberExperienceDetail> GetPagedByMemberId(int memberId, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                if (memberId != 0)
                {
                    pageRequest.Conditions.Add("memberId", memberId.ToString());
                }

                pageResponse = Facade.GetPagedMemberExperienceDetail(pageRequest);
                return pageResponse.Response as List<MemberExperienceDetail>;
            }
        }
    }
}