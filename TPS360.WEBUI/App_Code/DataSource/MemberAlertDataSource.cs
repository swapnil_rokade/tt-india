using System;
using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class MemberAlertDataSource : ObjectDataSourceBase
    {
        PagedResponse<MemberAlert> pageResponse = null;

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(string employeeId)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<MemberAlert> GetPaged(string employeeId, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                base.AppendParameter(pageRequest, "employeeid", employeeId);

                pageResponse = Facade.GetPagedMemberAlert(pageRequest);
                return pageResponse.Response as List<MemberAlert>;
            }
        }
    }
}