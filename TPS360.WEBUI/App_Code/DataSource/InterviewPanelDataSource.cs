/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName:       InterviewPanelDataSource.cs
    Description:    This is the objects source file
    Created By:     Pravin khot
    Created On:     20/Nov/2015
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
      
---------------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;
using System.Web;
using System.Web.UI; 

namespace TPS360.Web.UI
{


    [DataObject(true)]
    public class InterviewPanelDataSource : ObjectDataSourceBase
     {

        public IList<InterviewPanel> GetAllInterviewPanel(string sortExpression)
        {
           IList<InterviewPanel> InterviewPanels = Facade.InterviewPanel_GetAll(sortExpression);

            return InterviewPanels;
        }    

        public string  AppendSortParameter(string sortExpression)
        {
            string strOutput = "";
            if (!string.IsNullOrEmpty(sortExpression))
            {
                string[] part = sortExpression.Split(' ');

                if (part != null && part.Length > 0)
                {
                    strOutput = part[0];

                    if (part.Length > 1 && part[1] != null)
                    {
                        strOutput = part[1];
                    }
                    else
                    {
                        strOutput = UIConstants.SORT_ORDER_ASC;
                    }
                }
            }
            return strOutput;
        }

    }
}