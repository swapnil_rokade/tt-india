using System;
using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class MemberManagerDataSource : ObjectDataSourceBase
    {
        PagedResponse<Member> pageResponse1 = null;
        PagedResponse<MemberManager> pageResponse = null;

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount()
        {
            return pageResponse.TotalRow;
        }

       

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(int memberId)
        {
            return pageResponse1.TotalRow;
        }

      

     
        public List<Member> GetPaged(int memberId, string sortExpression, int startRowIndex, int maximumRows)
        {
            
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageResponse1 = Facade.GetPagedMemberManagerByMemberID(memberId, pageRequest);
                return pageResponse1.Response as List<Member >;
            }
        }
    }
}