/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberEmailDataSource.cs
    Description: This is the .cs file used as datasource for Object datasources used in email pages.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Dec-05-2008           Jagadish            Defect id: 9358; changed case of variable id's from 'ReceiverId' and
                                                             'SenderId' to 'receiverId' and 'senderId'.
-------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class MemberEmailDataSource : ObjectDataSourceBase
    {
        PagedResponse<MemberEmail> pageResponse = null;

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(string emailTypeLookupId, string receiverId, string senderId,string company, string AnyKey) // 0.1
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<MemberEmail> GetPaged(string emailTypeLookupId, string receiverId, string senderId,string company, string AnyKey, string sortExpression, int startRowIndex, int maximumRows) // 0.1
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                if (emailTypeLookupId != "" && Convert.ToInt32(emailTypeLookupId) > 0)
                {
                    pageRequest.Conditions.Add("EmailTypeLookupId", emailTypeLookupId.ToString());
                    
                }
                // 0.1 starts here.
                if (receiverId != "" && Convert.ToInt32(receiverId) > 0)
                {
                    pageRequest.Conditions.Add("ReceiverId", receiverId.ToString());
                }
                if (senderId != "" && Convert.ToInt32(senderId) > 0)
                {
                    pageRequest.Conditions.Add("SenderId", senderId.ToString());
                }
                if (company != "" && Convert.ToInt32(company) > 0)
                {
                    pageRequest.Conditions.Add("company", company.ToString());
                }
                // 0.1 ends here.
                if (AnyKey != "")
                {
                    pageRequest.Conditions.Add("AnyKey", AnyKey);
                }
                pageResponse = Facade.GetPagedMemberEmail(pageRequest);
                return pageResponse.Response as List<MemberEmail>;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCountForReceived(string emailTypeLookupId, string comId,string receiverId, string senderId, string AnyKey) // 0.1
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<MemberEmail> GetPagedForReceived(string emailTypeLookupId, string comId,string receiverId, string senderId, string AnyKey, string sortExpression, int startRowIndex, int maximumRows) // 0.1
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                if (emailTypeLookupId != "" && Convert.ToInt32(emailTypeLookupId) > 0)
                {
                    pageRequest.Conditions.Add("EmailTypeLookupId", emailTypeLookupId.ToString());

                }
                // 0.1 starts here.
                if (comId != "" && Convert.ToInt32(comId) > 0)
                {
                    pageRequest.Conditions.Add("comId", comId.ToString());
                }
                if (senderId != "" && Convert.ToInt32(senderId) > 0)
                {
                    pageRequest.Conditions.Add("SenderId", senderId.ToString());
                }
                if (receiverId != "" && Convert.ToInt32(receiverId) > 0)
                {
                    pageRequest.Conditions.Add("ReceiverId", receiverId.ToString());
                }
                if (AnyKey != "")
                {
                    pageRequest.Conditions.Add("AnyKey", AnyKey);
                }
                pageResponse = Facade.GetPagedMemberEmail(pageRequest);
                return pageResponse.Response as List<MemberEmail>;
            }
        }
        // Defect # 9566 (for all emails).
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(string emailTypeLookupId, string receiverId, string senderId, string recId, string senId, string AnyKey) // 0.1
        {
            return pageResponse.TotalRow;
        }
        
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<MemberEmail> GetPaged(string emailTypeLookupId, string receiverId, string senderId, string recId, string senId, string AnyKey, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                if (emailTypeLookupId != "" && Convert.ToInt32(emailTypeLookupId) > 0)
                {
                    pageRequest.Conditions.Add("EmailTypeLookupId", emailTypeLookupId.ToString());

                }
                // 0.1 starts here.
                if (receiverId != "" && Convert.ToInt32(receiverId) > 0)
                {
                    pageRequest.Conditions.Add("ReceiverId", receiverId.ToString());
                }
                
                if (senderId != "" && Convert.ToInt32(senderId) > 0)
                {
                    pageRequest.Conditions.Add("SenderId", senderId.ToString());
                }
                
                if (recId != "" && Convert.ToInt32(recId) > 0)
                {
                    pageRequest.Conditions.Add("RecId", recId.ToString());
                }
                
                if (senId != "" && Convert.ToInt32(senId) > 0)
                {
                    pageRequest.Conditions.Add("SenId", senId.ToString());
                }
                
                // 0.1 ends here.
                if (AnyKey != "")
                {
                    pageRequest.Conditions.Add("AnyKey", AnyKey);
                }
                pageResponse = Facade.GetPagedMemberEmail(pageRequest);
                return pageResponse.Response as List<MemberEmail>;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(string emailTypeLookupId, string receiverId, string senderId, string recId, string senId,string companyId, string AnyKey) // 0.1
        {
            return pageResponse.TotalRow;
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<MemberEmail> GetPaged(string emailTypeLookupId, string receiverId, string senderId, string recId, string senId,string companyId, string AnyKey, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                if (emailTypeLookupId != "" && Convert.ToInt32(emailTypeLookupId) > 0)
                {
                    pageRequest.Conditions.Add("EmailTypeLookupId", emailTypeLookupId.ToString());

                }
                // 0.1 starts here.
                if (receiverId != "" && Convert.ToInt32(receiverId) > 0)
                {
                    pageRequest.Conditions.Add("ReceiverId", receiverId.ToString());
                }

                if (senderId != "" && Convert.ToInt32(senderId) > 0)
                {
                    pageRequest.Conditions.Add("SenderId", senderId.ToString());
                }

                if (recId != "" && Convert.ToInt32(recId) > 0)
                {
                    pageRequest.Conditions.Add("RecId", recId.ToString());
                }

                if (senId != "" && Convert.ToInt32(senId) > 0)
                {
                    pageRequest.Conditions.Add("SenId", senId.ToString());
                }
                if (companyId != "" && Convert.ToInt32(companyId) > 0)
                {
                    pageRequest.Conditions.Add("companyId", companyId.ToString());
                }
                // 0.1 ends here.
                if (AnyKey != "")
                {
                    pageRequest.Conditions.Add("AnyKey", AnyKey);
                }
                pageResponse = Facade.GetPagedMemberEmail(pageRequest);
                return pageResponse.Response as List<MemberEmail>;
            }
        }
        // Defect # 9566
    }
}