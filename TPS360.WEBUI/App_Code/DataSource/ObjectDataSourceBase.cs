using System;

using TPS360.BusinessFacade;
using TPS360.Common.Helper;
using TPS360.Common.Shared;

namespace TPS360.Web.UI
{
    public class ObjectDataSourceBase
    {
        private IFacade _facade;

        protected IFacade Facade
        {
            get
            {
                if (_facade == null)
                {
                    _facade = System.Web.HttpContext.Current.Items[ContextConstants.Facade] as IFacade;
                }

                return _facade;
            }
        }

        public PagedRequest PreparePagedRequest(string sortExpression, int startRowIndex, int maximumRows)
        {
            PagedRequest pageRequest = new PagedRequest();
            pageRequest.PageIndex = startRowIndex;
            pageRequest.RowPerPage = maximumRows;

            if (!string.IsNullOrEmpty(sortExpression))
            {
                this.AppendSortParameter(pageRequest, sortExpression);
            }

            return pageRequest;
        }

        public void AppendParameter(PagedRequest pageRequest, string paramName, string paramValue)
        {
            ArgumentValidator.CheckForEmptyString(paramName, "paramName");

            if (!string.IsNullOrEmpty(paramValue))
            {
                pageRequest.Conditions.Add(paramName, paramValue.Trim());
            }
        }

        public void AppendParameter(PagedRequest pageRequest, string paramName, int paramValue)
        {
            ArgumentValidator.CheckForEmptyString(paramName, "paramName");

            if (paramValue>0)
            {
                pageRequest.Conditions.Add(paramName, paramValue.ToString());
            }
        }

        public void AppendSortParameter(PagedRequest pageRequest, string sortExpression)
        {
            if (pageRequest != null && !string.IsNullOrEmpty(sortExpression))
            {
                string[] part = sortExpression.Split(' ');

                if (part != null && part.Length > 0)
                {
                    pageRequest.SortColumn = part[0];

                    if (part.Length >1 && part[1] != null)
                    {
                        pageRequest.SortOrder = part[1];
                    }
                    else
                    {
                        pageRequest.SortOrder = UIConstants.SORT_ORDER_ASC;
                    }
                }
            }
        }

        public void AppendCommonParameter(PagedRequest pageRequest, string fromCreateDate, string toCreateDate, string fromUpdateDate, string toUpdateDate)
        {
            AppendCreateDateParameter(pageRequest, fromCreateDate, toCreateDate);
            AppendUpdateDateParameter(pageRequest, fromUpdateDate, toUpdateDate);
        }

        public void AppendCommonParameter(PagedRequest pageRequest, string creatorName, string updatorName, string fromCreateDate, string toCreateDate, string fromUpdateDate, string toUpdateDate)
        {
            AppendCreateDateParameter(pageRequest, fromCreateDate, toCreateDate);
            AppendUpdateDateParameter(pageRequest, fromUpdateDate, toUpdateDate);
            AppendCreatorParameter(pageRequest, creatorName);
            AppendUpdatorParameter(pageRequest, updatorName);
        }

        public void AppendCreateDateParameter(PagedRequest pageRequest, string fromCreateDate, string toCreateDate)
        {
            if (pageRequest != null)
            {
                if ((!string.IsNullOrEmpty(fromCreateDate)) && (!string.IsNullOrEmpty(toCreateDate)))
                {
                    DateTime fromDate = DateTime.MinValue;
                    DateTime toDate = DateTime.MinValue;

                    try
                    {
                        fromDate = DateTime.Parse(fromCreateDate.Trim());
                    }
                    catch (FormatException)
                    {
                    }

                    try
                    {
                        toDate = DateTime.Parse(toCreateDate.Trim());
                    }
                    catch (FormatException)
                    {
                    }

                    if ((MiscUtil.IsValidDate(fromDate)) && (MiscUtil.IsValidDate(toDate)))
                    {
                        pageRequest.Conditions.Add("fromCreateDate", StringHelper.Convert(UIConstants.SHORT_DATE_FORMAT, fromDate));
                        pageRequest.Conditions.Add("toCreateDate", StringHelper.Convert(UIConstants.SHORT_DATE_FORMAT, toDate));
                    }
                }
            }
        }

        public void AppendUpdateDateParameter(PagedRequest pageRequest, string fromUpdateDate, string toUpdateDate)
        {
            if (pageRequest != null)
            {
                if ((!string.IsNullOrEmpty(fromUpdateDate)) && (!string.IsNullOrEmpty(toUpdateDate)))
                {
                    DateTime fromDate = DateTime.MinValue;
                    DateTime toDate = DateTime.MinValue;

                    try
                    {
                        fromDate = DateTime.Parse(fromUpdateDate.Trim());
                    }
                    catch (FormatException)
                    {
                    }

                    try
                    {
                        toDate = DateTime.Parse(toUpdateDate.Trim());
                    }
                    catch (FormatException)
                    {
                    }

                    if ((MiscUtil.IsValidDate(fromDate)) && (MiscUtil.IsValidDate(toDate)))
                    {
                        pageRequest.Conditions.Add("fromUpdateDate", StringHelper.Convert(UIConstants.SHORT_DATE_FORMAT, fromDate));
                        pageRequest.Conditions.Add("toUpdateDate", StringHelper.Convert(UIConstants.SHORT_DATE_FORMAT, toDate));
                    }
                }
            }
        }

        public void AppendCreatorParameter(PagedRequest pageRequest, string creatorName)
        {
            if (pageRequest != null)
            {
                if (!string.IsNullOrEmpty(creatorName))
                {
                    pageRequest.Conditions.Add("CreatorName", creatorName.Trim());
                }
            }
        }

        public void AppendUpdatorParameter(PagedRequest pageRequest, string updatorName)
        {
            if (pageRequest != null)
            {
                if (!string.IsNullOrEmpty(updatorName))
                {
                    pageRequest.Conditions.Add("UpdatorName", updatorName.Trim());
                }
            }
        }
    }
}