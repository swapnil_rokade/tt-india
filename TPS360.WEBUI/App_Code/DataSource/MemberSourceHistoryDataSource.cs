using System;
using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class MemberSourceHistoryDataSource : ObjectDataSourceBase
    {
        PagedResponse<MemberSourceHistory> pageResponse = null;

       

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(string  MemberId)
        {
            return pageResponse.TotalRow;
        }
      
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<MemberSourceHistory> GetPaged( string MemberId,string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.Conditions.Add("MemberId", MemberId);
                pageResponse = Facade.GetPagedMemberSourceHistory(pageRequest);
                return pageResponse.Response as List<MemberSourceHistory>;
            }
        }


        
    }
}