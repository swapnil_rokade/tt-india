using System;
using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class MemberCertificationDataSource : ObjectDataSourceBase
    {
        PagedResponse<MemberCertificationMap > pageResponse = null;

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount()
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(int memberId)
        {
            return pageResponse.TotalRow;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(int memberId,string SortOrder)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<MemberCertificationMap > GetPaged(string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageResponse = Facade.GetPagedMemberCertificationMap (pageRequest);
                return pageResponse.Response as List<MemberCertificationMap >;
            }
        }


        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<MemberCertificationMap > GetPagedByMemberId(int memberId, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                if (memberId != 0)
                {
                    pageRequest.Conditions.Add("MemberId", memberId.ToString());
                }
              
                pageResponse = Facade.GetPagedMemberCertificationMapByMemberId  (pageRequest);
                return pageResponse.Response as List<MemberCertificationMap >;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<MemberCertificationMap> GetPagedByMemberId(int memberId,string SortOrder, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.SortOrder = SortOrder;
                if (memberId != 0)
                {
                    pageRequest.Conditions.Add("MemberId", memberId.ToString());
                }

                pageResponse = Facade.GetPagedMemberCertificationMapByMemberId(pageRequest);
                return pageResponse.Response as List<MemberCertificationMap>;
            }
        }
    }
}