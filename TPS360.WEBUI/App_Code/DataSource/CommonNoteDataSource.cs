/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName:CommonNoteDataSource.cs
    Description: This is the objects source file
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
      0.1           Feb-2-2009              Gopala Swamy        Defect Id:9061;Prototypes are changed for "GetAllByProjectID()"
---------------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class CommonNoteDataSource : ObjectDataSourceBase
    {
        public IList<CommonNote> GetAllByMemberID(int memberId, string sortExpression)
        {
            IList<CommonNote> CommonNotes = Facade.GetAllCommonNoteByMemberId(memberId, sortExpression );

            return CommonNotes;

        }

        public IList<CommonNote> GetAllByCompanyID(int companyId, string sortExpression)
        {
            IList<CommonNote> CommonNotes = Facade.GetAllCommonNoteByCompanyId(companyId, sortExpression);

            return CommonNotes;

        }
        public IList<CommonNote> GetAllByProjectID(int projectId, string sortExpression)//0.1
        {
             //PagedRequest pageRequest=base.PreparePagedRequest(sortExpression, 0, 1000);//They have not put  the pager for the grid.So in order to make use of this functin i am sending 0 and 1000 as parameters

            IList<CommonNote> CommonNotes = Facade.GetAllCommonNoteByProjectId(projectId, sortExpression);//0.1 

            return CommonNotes;
        }

        public string  AppendSortParameter(string sortExpression)
        {
            string strOutput = "";
            if (!string.IsNullOrEmpty(sortExpression))
            {
                string[] part = sortExpression.Split(' ');

                if (part != null && part.Length > 0)
                {
                    strOutput = part[0];

                    if (part.Length > 1 && part[1] != null)
                    {
                        strOutput = part[1];
                    }
                    else
                    {
                        strOutput = UIConstants.SORT_ORDER_ASC;
                    }
                }
            }
            return strOutput;
        }

    }
}