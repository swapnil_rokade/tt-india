﻿using System;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Xml;
using AjaxControlToolkit;
using TPS360.Common.Helper;
using TPS360.Common.BusinessEntities;
using System.Collections.Generic;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class DataService : WebServiceBase
{  
    public DataService()
    {
        
    }

    [WebMethod]
    public CascadingDropDownNameValue[] PopulateRoles(string knownCategoryValues, string category)
    {
        List<CascadingDropDownNameValue> values = new List<CascadingDropDownNameValue>();
        IList<CustomRole> roleList = Facade.GetAllCustomRole();

        if (roleList != null && roleList.Count > 0)
        {
            foreach (CustomRole role in roleList)
            {
                values.Add(new CascadingDropDownNameValue(role.Name, StringHelper.Convert(role.Id)));
            }
        }

        return values.ToArray();
    }

    [WebMethod]
    public CascadingDropDownNameValue[] PopulateMembers(string knownCategoryValues, string category)
    {
        List<CascadingDropDownNameValue> values = new List<CascadingDropDownNameValue>();

        StringDictionary dictionary = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues);

        int roleId;

        if (!dictionary.ContainsKey("Role") || !Int32.TryParse(dictionary["Role"], out roleId))
        {
            return null;
        }

        IList<Member> memberList = Facade.GetAllMemberByCustomrRoleId(roleId);

        if (memberList != null && memberList.Count > 0)
        {
            foreach (Member member in memberList)
            {
                values.Add(new CascadingDropDownNameValue(member.FirstName + " " + member.LastName, StringHelper.Convert(member.Id)));
            }
        }

        return values.ToArray();
    }

    [WebMethod]
    public CascadingDropDownNameValue[] PopulateCountry(string knownCategoryValues, string category)
    {
        List<CascadingDropDownNameValue> values = new List<CascadingDropDownNameValue>();
        IList<Country> countryList = Facade.GetAllCountry();

        if (countryList != null && countryList.Count > 0)
        {
            foreach (Country country in countryList)
            {
                values.Add(new CascadingDropDownNameValue(country.Name, StringHelper.Convert(country.Id)));
            }
        }

        return values.ToArray();
    }

    [WebMethod]
    public CascadingDropDownNameValue[] PopulateStates(string knownCategoryValues, string category)
    {
        List<CascadingDropDownNameValue> values = new List<CascadingDropDownNameValue>();

        StringDictionary dictionary = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues);

        int countryId;

        if (!dictionary.ContainsKey("Country") || !Int32.TryParse(dictionary["Country"], out countryId))
        {
            return null;
        }

        IList<State> stateList = Facade.GetAllStateByCountryId(countryId);

        if (stateList != null && stateList.Count > 0)
        {
            foreach (State state in stateList)
            {
                values.Add(new CascadingDropDownNameValue(state.Name, StringHelper.Convert(state.Id)));
            }
        }

        return values.ToArray();
    }
}