﻿using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;

using System.Web.Script.Services;
using AjaxControlToolkit;
using System.Collections.Generic;
using System.IO;
using TPS360.Web.UI;

[System.Web.Script.Services.ScriptService]
public class UserPageService : WebServiceBase
{
    public UserPageService()
    {

    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = false, XmlSerializeString = true)]
    public void NewPage(string newLayout)
    {
        UserPage page = new UserPage();
        page.Title = "My New Page";
        page.UserId = CurrentMember.Id;
        new UserPageDataAccess().AddUserPage(page);
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = false, XmlSerializeString = true)]
    public void ChangeCurrentPage(int pageId)
    {
        UserPageSetup setup = new UserPageSetup(CurrentMember.Id);

        if (setup.UserSetting.CurrentPageId != pageId)
        {
            setup.UserSetting.CurrentPageId = setup.CurrentPage.Id = pageId;
            new UserPageSettingDataAccess().UpdateUserPageSetting(setup.UserSetting);
        }
    }


    [WebMethod]
    [ScriptMethod(UseHttpGet = false, XmlSerializeString = true)]
    public void DeletePage(int PageID)
    {
        UserPageSetup setup = new UserPageSetup(CurrentMember.Id);

        if (setup.Pages.Count > 1)
        {
            UserPageDataAccess upda = new UserPageDataAccess();
            upda.DeleteUserPage(setup.CurrentPage.Id);
            //reset the current page

            UserPage topPage = upda.GetTopUserPage(CurrentMember.Id);

            if (topPage != null)
            {
                setup.UserSetting.CurrentPageId = setup.CurrentPage.Id = topPage.Id;
                new UserPageSettingDataAccess().UpdateUserPageSetting(setup.UserSetting);

                //Context.Cache.Remove(Profile.UserName);
            }
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = false, XmlSerializeString = true)]
    public void RenamePage(int PageID, string newName)
    {
        UserPageSetup setup = new UserPageSetup(CurrentMember.Id);

        new UserPageDataAccess().UpdateUserPageTitle(setup.CurrentPage.Id, newName);
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = false, XmlSerializeString = true)]
    public void ChangePageLayout(int PageID, int newLayout)
    {
    }
}

