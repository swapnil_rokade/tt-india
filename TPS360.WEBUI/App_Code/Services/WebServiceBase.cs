﻿using System.Linq;
using System.Web.Services;
using TPS360.BusinessFacade;
using System.Web;
using System.Diagnostics;
using System;
using System.Web.Security;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class WebServiceBase : System.Web.Services.WebService
{
    #region Constants

    public const string ServiceNamespace = "https://www.tps360.com/services";

    #endregion

    #region Instance Variables

    private IFacade _facade;

    #endregion

    #region Private Properties

    public IFacade Facade
    {
        get
        {
            if (_facade == null)
            {
                _facade = new Facade();
            }

            return _facade;
        }
    }

    protected UserProfile Profile
    {
        [DebuggerStepThrough]
        get
        {
            return HttpContext.Current.Profile as UserProfile;
        }
    }

    protected string CurrentUserName
    {
        [DebuggerStepThrough]
        get
        {
            return HttpContext.Current.User.Identity.Name;
        }
    }

    protected bool IsUserAuthenticated
    {
        [DebuggerStepThrough]
        get
        {
            return HttpContext.Current.User.Identity.IsAuthenticated;
        }
    }

    protected Guid CurrentUserId
    {
        [DebuggerStepThrough]
        get
        {
            if (!IsUserAuthenticated)
            {
                return Guid.Empty;
            }

            MembershipUser user = Membership.GetUser(CurrentUserName, true);

            return (Guid)user.ProviderUserKey;
        }
    }

    protected Member CurrentMember
    {
        [DebuggerStepThrough()]
        get
        {
            return Facade.GetMemberByUserId(CurrentUserId);
        }
    }

    #endregion

    #region Constructer & Destructor

    public WebServiceBase()
    {
    }

    #endregion

    #region Public Methods

    #endregion
}