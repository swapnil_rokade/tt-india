﻿using System;
using System.Web.UI;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using System.Diagnostics;
namespace TPS360.Web.UI
{
    public class TemplatePage :   Page
    {
       
        public override void VerifyRenderingInServerForm(Control control) 
        {
        }
        #region Member Variables
        private WebHelper _helper;
        JobPosting _jobPosting;

        #endregion

        #region Properties
        protected WebHelper Helper
        {
            [DebuggerStepThrough()]
            get
            {
                if (_helper == null)
                {
                    _helper = new WebHelper(this);
                }

                return _helper;
            }
        }

        public int CurrentJobPostingId
        {
            get
            {
                int id = 0;

                int.TryParse(StringHelper.Convert(ViewState["JobPostingId"]), out id);

                if (id == 0)
                {
                    int.TryParse(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID], out id);
                }

                return id;
            }
        }
        #endregion

    }
}