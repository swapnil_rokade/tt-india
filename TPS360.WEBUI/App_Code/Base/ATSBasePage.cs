using System;
using System.Web.UI;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;

namespace TPS360.Web.UI
{
    public class ATSBasePage : AdminBasePage
    {
        public override void VerifyRenderingInServerForm(Control control) { }

        #region Member Variables

        JobPosting _jobPosting;

        #endregion

        #region Properties

        public int CurrentJobPostingId
        {
            get
            {
                int id = 0;

                int.TryParse(StringHelper.Convert(ViewState["JobPostingId"]), out id);

                if (id == 0)
                {
                    int.TryParse(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID], out id);
                }

                return id;
            }
        }

        public JobPosting CurrentJobPosting
        {
            get
            {
                if (_jobPosting == null)
                {
                    if (CurrentJobPostingId > 0)
                    {
                        _jobPosting = Facade.GetJobPostingById(CurrentJobPostingId);
                    }

                    if (_jobPosting == null)
                    {
                        _jobPosting = new JobPosting();
                    }
                }

                return _jobPosting;
            }
            set
            {
                _jobPosting = value;
            }
        }

        #endregion

        #region Methods

        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);
            if (string.IsNullOrEmpty((string)Session["MyTheme"]) || (string)Session["MyTheme"] == "Default")
            {
                Page.Theme = "Default";
            }
        }

        #endregion

        #region Events

        #endregion
    }
}