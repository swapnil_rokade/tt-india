using System;
using System.Diagnostics;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.Web.UI
{
    public class CompanyBaseControl : BaseControl
    {
        #region Member Variables

        Company _company;

        #endregion

        #region Properties

        public int CurrentCompanyId
        {
            get
            {
                int id = 0;              

                // Get the company id from viewstate & using try parse method, convert it into int.. the id will hold the result.
                // Id will be zero if if the viewstate parameter is null, is not  of the correct format, or represents a number less 
                //than System.Int32.MinValue or greater than System.Int32.MaxValue.
                
                int.TryParse(StringHelper.Convert(ViewState[UrlConstants.PARAM_COMPANY_ID]), out id);

                // If company id is null or not in proper format
                if (id == 0)
                {
                    int.TryParse(Helper.Url.SecureUrl[UrlConstants.PARAM_COMPANY_ID], out id);
                }

                return id;
            }
            set
            {
                ViewState[UrlConstants.PARAM_COMPANY_ID] = value;
            }
        }

        public Company CurrentCompany
        {
            get
            {
                if (_company == null)
                {
                    if (CurrentCompanyId > 0)
                    {
                        _company = Facade.GetCompanyById(CurrentCompanyId);
                    }

                    if (_company == null)
                    {
                        _company = new Company();
                    }
                }

                return _company;
            }
            set
            {
                _company = value;
            }
        }

        public bool IsExternalRegistration
        {
            get
            {
                return (bool)(ViewState["_ExternalRegistration"] ?? false);
            }
            set
            {
                ViewState["_ExternalRegistration"] = value;
            }
        }

        public CompanyStatus RequestedCompanyStatus
        {
            get
            {
                string companyType = Helper.Url.SecureUrl[UrlConstants.PARAM_COMPANY_TYPE];

                if (string.Equals(companyType, "Vendor"))
                {
                    return CompanyStatus.Vendor;
                }
                else if (string.Equals(companyType, "Client"))
                {
                    return CompanyStatus.Client;
                }
                else if (string.Equals(companyType, "Partner"))
                {
                    return CompanyStatus.Partner;
                }
                else if (string.Equals(companyType, "Company"))
                {
                    return CompanyStatus.Company;
                }
                else
                {
                    return CompanyStatus.Unknown;
                }
            }
        }
        public CompanyStatus CurrentCompanyStatus
        {
            get
            {
                string ParentID = Helper.Url.SecureUrl[UrlConstants.PARAM_SITEMAP_PARENT_ID];
                if (ParentID == "11")
                    return CompanyStatus.Client;
                else if (ParentID == "638")
                    return CompanyStatus.Vendor;
                else return CompanyStatus.Department;

            }
        }
        public bool IsInformationReadOnly
        {
            get
            {
                return (bool)(ViewState["_IsInformationReadOnly"] ?? false);
            }
            set
            {
                ViewState["_IsInformationReadOnly"] = value;
            }
        }

        #endregion

        #region Methods

        #endregion
    }
}