using System;
using System.Diagnostics;
using System.Web.Security;

using TPS360.BusinessFacade;
using TPS360.Web.UI.Helper;
using TPS360.Common.BusinessEntities;

namespace TPS360.Web.UI
{
    public class CandidateMasterPage : BaseMasterPage
    {
        #region Member Variables

        #endregion

        #region Properties

        public int CurrentCandidateId
        {
            get
            {
                return ((CandidateBasePage)this.Page).CurrentCandidateId;

            }
        }

        public Member CurrentCandidate
        {
            get
            {
                return ((CandidateBasePage)this.Page).CurrentCandidate;

            }
            set
            {
                ((CandidateBasePage)this.Page).CurrentCandidate = value;
            }
        }

        #endregion

        #region Methods

        #endregion

        #region Events

        #endregion
    }
}