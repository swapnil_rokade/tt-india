using System;
using System.Diagnostics;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;

namespace TPS360.Web.UI
{
    public class RequisitionBaseControl : BaseControl
    {
        #region Member Variables

        #endregion

        #region Properties

        public int CurrentJobPostingId
        {
            get
            {
                return ((RequisitionBasePage)this.Page).CurrentJobPostingId;
            }
        }

        public JobPosting CurrentJobPosting
        {
            get
            {
                return ((RequisitionBasePage)this.Page).CurrentJobPosting;

            }
            set
            {
                ((RequisitionBasePage)this.Page).CurrentJobPosting = value;
            }
        }

        #endregion

        #region Methods

        #endregion
    }
}