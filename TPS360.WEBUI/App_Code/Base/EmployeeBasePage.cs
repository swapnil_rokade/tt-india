using System;
using System.Diagnostics;
using System.Web.Security;
using System.Web.UI;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common.Helper;

namespace TPS360.Web.UI
{
    public class EmployeeBasePage : AdminBasePage
    {
        #region Member Variables

        Member _candidate;

        #endregion

        #region Properties

        public int CurrentEmployeeId
        {
            get
            {
                int id = 0;

                int.TryParse(StringHelper.Convert(ViewState["EmployeeId"]), out id);

                if (id == 0)
                {
                    int.TryParse(Helper.Url.SecureUrl[UrlConstants.PARAM_ID], out id);
                }

                return id;
            }
        }

        public Member CurrentEmployee
        {
            get
            {
                if (_candidate == null)
                {
                    if (CurrentEmployeeId > 0)
                    {
                        _candidate = Facade.GetMemberById(CurrentEmployeeId);
                    }

                    if (_candidate == null)
                    {
                        _candidate = new Member();
                    }
                }

                return _candidate;
            }
            set
            {
                _candidate = value;
            }
        }

        #endregion

        #region Methods

        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);
            if (string.IsNullOrEmpty((string)Session["MyTheme"]) || (string)Session["MyTheme"] == "Default")
            {
                Page.Theme = "Default";
            }
        }

        #endregion

        #region Events

        #endregion
    }
}