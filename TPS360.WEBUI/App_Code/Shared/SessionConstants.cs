using System;

namespace TPS360.Web.UI
{
    public static class SessionConstants
    {
        public const string UPLOADED_COMPANY_LOGO_FILE = "uploadCompanyLogoFile";
        public const string SELECTED_APPLICANT = "SelectedApplicant";
        public const string EMAIL_BODY = "EmailBody";
        public const string FROM = "SenderEmail";
        public const string TO = "ReceiverEmail";
        public const string CC = "CC";
        public const string BCC = "BCC";
        public const string SUBJECT = "Subject";
        public const string FILENAME = "FileName";
        public const string FILECOUNT = "FileCount";
        public const string From_Page = "FromPage";
        public const string clientLocalPath = "LocalPath";
        public const string UserId = "UserId";
        public const string CompanyId = "CompanyId";
        public const string EmailFromType = "EmailFromType";
        public const string Selectedindex = "SelectIndex";
        public const string VendorEmail = "VendorEmail";
        public const string PreviousPath = "Path";
        public const string Status = "status";
        public const string Signature = "Signature";
    }
}