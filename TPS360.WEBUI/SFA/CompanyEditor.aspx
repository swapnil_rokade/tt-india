﻿<%@ Page Language="C#" MasterPageFile="~/Masters/CompanyProfile.master" AutoEventWireup="true"
    CodeFile="CompanyEditor.aspx.cs" Inherits="TPS360.Web.UI.Admin.CompanyEditor"
    EnableEventValidation="false" Title="Company Editor" %>

<%@ Register Src="~/Controls/Company.ascx" TagName="Company" TagPrefix="uc1" %>
<asp:Content ID="cntCompanyEditor" ContentPlaceHolderID="cphCompanyMaster" runat="Server">
    <asp:HiddenField ID="hdnPageTitle" runat="server" />
    <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
    <uc1:Company ID="ctlCompany" runat="server" />
</asp:Content>
