﻿<%@ Page Language="C#" MasterPageFile="~/Masters/CompanyProfile.master" AutoEventWireup="true"
    CodeFile="CompanyContact.aspx.cs" Inherits="TPS360.Web.UI.Admin.CompanyContactPage"
    Title="Company Contact" EnableEventValidation="false" %>

<%@ Register Src="../Controls/CompanyContact.ascx" TagName="CompanyContact" TagPrefix="uc2" %>
<asp:Content ID="cntCompanyEditor" ContentPlaceHolderID="cphCompanyMaster" runat="Server">
    <asp:HiddenField ID="hdnPageTitle" runat="server" />
    <uc2:CompanyContact ID="ctlCompanyContact" runat="server" />
</asp:Content>
