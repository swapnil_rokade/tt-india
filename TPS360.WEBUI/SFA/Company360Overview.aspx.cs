﻿using System;
using TPS360.Web.UI;


public partial class SFA_Client360Overview : CompanyBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Page.Title = base.CurrentCompany.CompanyName + " - " + "Overview";
            hdnPageTitle.Value  = Page.Title;
        }

        Page.Title = hdnPageTitle.Value;
    }
}
