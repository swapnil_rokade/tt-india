﻿<%@ Page Language="C#" MasterPageFile="~/Masters/CompanyProfile.master" AutoEventWireup="true"
    CodeFile="CompanyDocument.aspx.cs" Inherits="TPS360.Web.UI.Admin.CompanyDocumentPage"
    Title="Company Documents" %>

<%@ Register Src="~/Controls/CompanyDocument.ascx" TagName="Document" TagPrefix="uc1" %>
<asp:Content ID="cntCompanyEditor" ContentPlaceHolderID="cphCompanyMaster" runat="Server">
    <asp:HiddenField ID="hdnPageTitle" runat="server" />
    <uc1:Document ID="ucntrlDocument" runat="server" />
</asp:Content>
