﻿<%@ Page Language="C#" MasterPageFile="~/CandidatePortal/Portal.master" AutoEventWireup="true"
    CodeFile="EmployeeReferral.aspx.cs" EnableEventValidation="false" Inherits="TPS360.Web.UI.EmployeeReferral"
    Title="Employee Referral Portal" %>

<%@ Register Src="~/Controls/RegistrationInternal.ascx" TagName="regI" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ReferrerInfo.ascx" TagName="Refer" TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
    Refer Candidate for job openings
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHomeMaster" runat="Server">
    <div style="padding: 18px;">
        <asp:UpdatePanel ID="pnlRegistrationInformation" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="divRegInternal" runat="server">
                <uc1:Refer ID="uclRefer" runat ="server" />
                    <uc1:regI ID="rgInternal" runat="server" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
