﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Web.UI;
using System.Collections.Generic ;
using TPS360.BusinessFacade ;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
namespace TPS360.Web.UI
{

    public partial class SFA_CompanyVendorPortalAccess : CompanyBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                VendorPortalAccess.Role = ContextConstants.ROLE_VENDOR;
                Page.Title = base.CurrentCompany.CompanyName + " - " + "Vendor Portal Access";
                hdnPageTitle.Value = Page.Title;

            }
            Page.Title = hdnPageTitle.Value;
        }

        [System.Web.Services.WebMethod]
        public static object[] EmployeeAccessStatusChange(object data)
        {
            Dictionary<string, object> param = (Dictionary<string, object>)data;
            IFacade Facade = new Facade();
            string f = param["StatusId"].ToString();
            int StatusSeclected = Convert.ToInt32(param["StatusId"]);
            int UpdatorId = Convert.ToInt32(param["UpdatorId"]);
            int MemberId = Convert.ToInt32(param["MemberId"]);

            Member member = Facade.GetMemberById(MemberId);

            if (StatusSeclected == 1)
            {
                MembershipUser user = Membership.GetUser(member.UserId);

                if (user != null)
                {
                    user.IsApproved = true;
                    Membership.UpdateUser(user);
                    CompanyContact con = Facade.GetCompanyContactByEmail(member.PrimaryEmail);

                    try
                    {
                        con.PortalAccess = true;
                        Facade.UpdateCompanyContact(con);
                    }
                    catch { }

                    MemberCustomRoleMap map = Facade.GetMemberCustomRoleMapByMemberId(con.MemberId);

                    if (member != null)
                    {
                        member.IsRemoved = false;
                        Facade.UpdateMember(member);
                    }
                    if (map != null)
                    {
                        map.IsRemoved = false;
                        Facade.UpdateMemberCustomRoleMap(map);
                    }
                }
            }
            else
            {
                MembershipUser user = Membership.GetUser(member.UserId);

                if (user != null)
                {
                    user.IsApproved = false;
                    Membership.UpdateUser(user);
                    CompanyContact con = Facade.GetCompanyContactByEmail(member.PrimaryEmail);
                    con.PortalAccess = false;
                    Facade.UpdateCompanyContact(con);
                    MemberCustomRoleMap map = Facade.GetMemberCustomRoleMapByMemberId(con.MemberId);

                    if (member != null)
                    {
                        member.IsRemoved = true;
                        Facade.UpdateMember(member);
                    }
                    if (map != null)
                    {
                        map.IsRemoved = true;
                        Facade.UpdateMemberCustomRoleMap(map);
                    }
                }
            }

            return null;
        }
    }
}
