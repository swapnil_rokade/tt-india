﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CreateNewClient.aspx.cs
    Description: This page is used to add new client in SFA module
    Created By: Yogeesh Bhat
    Created On: Dec-16-2008
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              Jan-05-2009        Yogeesh Bhat        Defect ID:9648: Changes made in btnSave_Click() event.
                                                            Getting country id from site setting.
    0.2              March-02-2009      Gopala Swamy        Defect Id:10002: Put "If condition "statement 
    0.3              May-21-2009        Veda                Defect Id:10490: To display the message when company name is blank
-------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI;
using TPS360.Common.Shared;
using TPS360.Common.Helper;
using System.Text.RegularExpressions;
using TPS360.Web.UI.Helper;
public partial class SFA_CreateNewClient : BasePage
{
    #region Member Variables
    private bool _IsAccessForCompanyOverview = true ;
    private string UrlForCompany = string.Empty;
    private int IdForSitemap = 0;
    #endregion
    public CompanyStatus CurrentCompanyStatus
    {
        get
        {
            string ParentID = Helper.Url.SecureUrl[UrlConstants.PARAM_SITEMAP_PARENT_ID];
            if (ParentID == "11")
                return CompanyStatus.Client;
            else if (ParentID == "638")
                return CompanyStatus.Vendor;
            else return CompanyStatus.Department;

        }
    }
    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        CustomSiteMap CustomMap = new CustomSiteMap();
        CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(235, CurrentMember.Id);
        if (CustomMap == null) _IsAccessForCompanyOverview = false  ;
        else
        {
            IdForSitemap = CustomMap.Id;
            UrlForCompany = "~/" + CustomMap.Url.ToString();
        }
        if (!IsPostBack)
        {

            switch (CurrentCompanyStatus)
            {
                case CompanyStatus.Client:
                    lblAccountHeader.Text = "Create New Account";
                    break;
                case CompanyStatus.Department:
                    lblAccountHeader.Text = "Create New BU";
                    break;
                case CompanyStatus.Vendor:
                    lblAccountHeader.Text = "Create New Vendor";
                    break;
            }
            this.Page.Title = lblAccountHeader.Text;
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        /*
        try
        {
            Company company = new Company();
            company.CompanyName =MiscUtil .RemoveScript (txtCompanyName.Text.Trim());
            company.IsEndClient = true;
            company.CreatorId = company.UpdatorId = base.CurrentMember.Id;
            company.CreateDate = company.UpdateDate = System.DateTime.Now;
            if (company.CompanyName == "")
            {
                if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
                {
                    MiscUtil.ShowMessage(lblMessage, "Please enter the Department name", true);
                }
                else  MiscUtil.ShowMessage(lblMessage, "Please enter the Account name", true);
                return;
            }

            if (SiteSetting != null)
            {
                company.CountryId = Convert.ToInt32( SiteSetting ["Country"]);
            }
            else
            {
                string CountryName =ConfigurationSettings.AppSettings["defaultCountry"];
                company.CountryId = Convert.ToInt32(Facade.GetCountryIdByCountryName(CountryName));

            }
            company = Facade.AddCompany(company, CompanyStatus.Client, false);
            CompanyAssignedManager companyTeam = new CompanyAssignedManager();
            companyTeam.CompanyId = company.Id;
            companyTeam.MemberId = base.CurrentMember.Id;
            companyTeam.IsPrimaryManager = true;
            companyTeam.CreatorId = base.CurrentMember.Id;
            try
            {
                Facade.AddCompanyAssignedManager(companyTeam);
            }
            catch (ArgumentException ex)
            {
                MiscUtil.ShowMessage(lblMessage, ex.Message, true);
            }
            Facade.UpdateCompanyStatus(company.Id, CompanyStatus.Client);
            if (_IsAccessForCompanyOverview)
            {
                SecureUrl UR = UrlHelper.BuildSecureUrl(UrlForCompany.Substring (6) , string.Empty, UrlConstants.PARAM_COMPANY_ID, company.Id.ToString(), UrlConstants.PARAM_SITEMAP_ID, IdForSitemap.ToString () );
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "open", "<script language='javaScript'>window.open('" + UR + "');</script>", false);
            }
            if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
            {

                MiscUtil.ShowMessage(lblMessage, "Department has been created successfully.", false);
            }
            else MiscUtil.ShowMessage(lblMessage, "Company has been created successfully.", false);
           txtCompanyName.Text = "";
        }
        catch (ArgumentException ex)
        {
            MiscUtil.ShowMessage(lblMessage, ex.Message, true);
        }
         * */

    }
    #endregion

}
