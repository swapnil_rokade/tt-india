﻿<%@ Page Language="C#" MasterPageFile="~/Masters/CompanyProfile.master" AutoEventWireup="true"
    CodeFile="Company360Overview.aspx.cs" Inherits="SFA_Client360Overview" Title="Company 360 Overview" %>
<%@ Register Src ="~/Controls/CompanyOverviewHeader.ascx"  TagName ="Overview" TagPrefix ="ucl" %>
<%@ Register Src="../Controls/CompanyOverview.ascx" TagName="CompanyOverview" TagPrefix="uc1" %>
<asp:Content ID="Content3" ContentPlaceHolderID="cphCompanyMaster" runat="Server">
    <asp:HiddenField ID="hdnPageTitle" runat="server" />
    <ucl:Overview ID ="uclOverview" runat ="server" />
    <uc1:CompanyOverview ID="CompanyOverview1" runat="server" />
</asp:Content>
 