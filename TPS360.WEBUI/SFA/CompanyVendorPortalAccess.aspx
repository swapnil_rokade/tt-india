﻿<%@ Page Language="C#" MasterPageFile="~/Masters/CompanyProfile.master" AutoEventWireup="true"
    CodeFile="CompanyVendorPortalAccess.aspx.cs" Inherits="TPS360.Web.UI.SFA_CompanyVendorPortalAccess"
    EnableEventValidation="false" Title="Vendor Portal Access" Async="true" %>

<%@ Register Src="~/Controls/VendorPortalAccess.ascx" TagName="VendorPortalAccess"
    TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="cntCompanyVendorPortal" ContentPlaceHolderID="cphCompanyMaster"
    runat="Server">

    <script src="../Scripts/fixWebkit.js" type="text/javascript"></script>

    <div class="MidBodyBoxRow">
        <asp:HiddenField ID="hdnPageTitle" runat="server" />
        <asp:UpdatePanel ID="pnlVendorAccessInformation" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <uc1:VendorPortalAccess ID="VendorPortalAccess" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
