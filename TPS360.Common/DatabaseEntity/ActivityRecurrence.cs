﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "ActivityRecurrence", Namespace = "http://www.tps360.com/types")]
    public class ActivityRecurrence : BaseEntity
    {
        #region Properties

        [DataMember]
        public int ActivityID
        {
            get;
            set;
        }

        [DataMember]
        public DateTime StartDateTimeUtc
        {
            get;
            set;
        }

        [DataMember]
        public int Duration
        {
            get;
            set;
        }

        [DataMember]
        public string Subject
        {
            get;
            set;
        }

        [DataMember]
        public string ActivityDescription
        {
            get;
            set;
        }

        [DataMember]
        public bool AllDayEvent
        {
            get;
            set;
        }

        [DataMember]
        public string Location
        {
            get;
            set;
        }

        [DataMember]
        public int Status
        {
            get;
            set;
        }

        [DataMember]
        public bool EnableReminder
        {
            get;
            set;
        }

        [DataMember]
        public int ReminderInterval
        {
            get;
            set;
        }

        [DataMember]
        public int ShowTimeAs
        {
            get;
            set;
        }

        [DataMember]
        public int Importance
        {
            get;
            set;
        }

        [DataMember]
        public byte[] _ts
        {
            get;
            set;
        }

        [DataMember]
        public int RecurrenceID
        {
            get;
            set;
        }

        [DataMember]
        public DateTime OriginalStartDateTimeUtc
        {
            get;
            set;
        }

        [DataMember]
        public Guid VarianceID
        {
            get;
            set;
        }

        [DataMember]
        public string ResourceName
        {
            get;
            set;
        }


        [DataMember]
        public bool IsRecurred
        {
            get;
            set;
        }

        [DataMember]
        public DateTime EndDateUtc
        {
            get;
            set;
        }

        [DataMember]
        public int DayOfWeekMaskUtc
        {
            get;
            set;
        }

        [DataMember]
        public int UtcOffset
        {
            get;
            set;
        }

        [DataMember]
        public int DayOfMonth
        {
            get;
            set;
        }

        [DataMember]
        public int MonthOfYear
        {
            get;
            set;
        }

        [DataMember]
        public int PeriodMultiple
        {
            get;
            set;
        }

        [DataMember]
        public string Period
        {
            get;
            set;
        }

        [DataMember]
        public int EditType
        {
            get;
            set;
        }

        [DataMember]
        public DateTime LastReminderDateTimeUtc
        {
            get;
            set;
        }

        //[DataMember]
        //public byte[] _ts
        //{
        //    get;
        //    set;
        //}


       

        #endregion
         #region Constructor

        public ActivityRecurrence()
            : base()
        {
        }

        #endregion
    }
}
