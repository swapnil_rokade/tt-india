﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "LeadTeam", Namespace = "http://www.tps360.com/types")]
    public class LeadTeam : BaseEntity
    {
        #region Properties

        [DataMember]
        public int LeadId
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public DateTime AssignDate
        {
            get;
            set;
        }

        public bool IsManager
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public LeadTeam()
            : base()
        {
        }

        #endregion
    }
}
