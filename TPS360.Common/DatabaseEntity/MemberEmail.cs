﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberEmail", Namespace = "http://www.tps360.com/types")]
    public class MemberEmail : BaseEntity
    {
        #region Properties

       [DataMember]
        public int SenderId
        {
            get;
            set;
        }

        [DataMember]
        public string SenderEmail
        {
            get;
            set;
        }

        [DataMember]
        public int ReceiverId
        {
            get;
            set;
        }

        [DataMember]
        public string ReceiverEmail
        {
            get;
            set;
        }

        [DataMember]
        public int EmailTypeLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string Subject
        {
            get;
            set;
        }

        [DataMember]
        public string EmailBody
        {
            get;
            set;
        }

        [DataMember]
        public int Status
        {
            get;
            set;
        }

        [DataMember]
        public int ParentId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }
        [DataMember]
        public string  BCC
        {
            get;
            set;
        }
        [DataMember]
        public string CC
        {
            get;
            set;
        }
        [DataMember]
        public String  SentDate
        {
            get;
            set;
        }
        [DataMember]
        public string AttachedFileNames
        {
            get;
            set;
        }
        [DataMember]
        public int NoOfAttachedFiles
        {
            get;
            set;
        }
        #endregion

        #region Constructor

        public MemberEmail()
            : base()
        {
        }

        #endregion
    }
}