﻿using System;
using System.Runtime.Serialization;
using TPS360.Common.Shared;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "Submission", Namespace = "http://www.tps360.com/types")]
    public class Submission : JobPosting
    {
        [DataMember]
        public string CompanyName
        {
            get;
            set;
        }

        [DataMember]
        public string CompanyPrimaryEmail
        {
            get;
            set;
        }


        [DataMember]
        public CompanyStatus CompanyStatus
        {
            get;
            set;
        }


        [DataMember]
        public DateTime SubmissionDate
        {
            get;
            set;
        }


        [DataMember]
        public string SubmissionPrimaryEmail
        {
            get;
            set;
        }


        [DataMember]
        public string SubmittedBy
        {
            get;
            set;
        }

        [DataMember]
        public string SubmittedTo
        {
            get;
            set;
        }

        [DataMember]
        public string ApplicantName
        {
            get;
            set;
        }
        [DataMember]
        public int MemberType
        {
            get;
            set;
        }
        //12410 
        public int MemberId
        {
            get;
            set;
        }
        public int MemberEmailId //12129
        {
            get;
            set;
        }
    }
}
