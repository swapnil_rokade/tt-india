﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TPS360.Common.BusinessEntities;
namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MailQueue", Namespace = "http://www.tps360.com/types")]
    public class MailQueue : BaseEntity
    {
        #region Properties
        [DataMember]
        public int Id
        {
            get;
            set;
        }
        [DataMember]
        public int SenderId
        {
            get;
            set;
        }
        [DataMember]
        public string ReceiverEmailId
        {
            get;
            set;
        }
        [DataMember]
        public string Subject
        {
            get;
            set;
        }
        [DataMember]
        public string EmailBody
        {
            get;
            set;
        }
        [DataMember]
        public string bcc
        {
            get;
            set;
        }
        [DataMember]
        public string cc
        {
            get;
            set;
        }
        [DataMember]
        public DateTime CreateDate
        {
            get;
            set;
        }
        [DataMember]
        public string AttachedFileNames
        {
            get;
            set;
        }
        [DataMember]
        public int NoOfAttachments
        {
            get;
            set;
        }
        #endregion
        
        #region Constructor
        public MailQueue()
            : base()
        {
        }

        #endregion
    }
}
