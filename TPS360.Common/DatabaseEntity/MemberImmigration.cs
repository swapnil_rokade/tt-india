﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberImmigration", Namespace = "http://www.tps360.com/types")]
    public class MemberImmigration : BaseEntity
    {
        #region Properties

        [DataMember]
        public string VisaType
        {
            get;
            set;
        }

        [DataMember]
        public string VisaIssuedOffice
        {
            get;
            set;
        }


        [DataMember]
        public int VisaIssuedCountryId
        {
            get;
            set;
        }

        [DataMember]
        public DateTime VisaIssuedDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime VisaExpirationDate
        {
            get;
            set;
        }

        [DataMember]
        public string Dependents
        {
            get;
            set;
        }

        [DataMember]
        public string LCALocation
        {
            get;
            set;
        }

        [DataMember]
        public string LCAState
        {
            get;
            set;
        }

        [DataMember]
        public string Port
        {
            get;
            set;
        }

        [DataMember]
        public string I94Number
        {
            get;
            set;
        }

        [DataMember]
        public string I94Address
        {
            get;
            set;
        }

        [DataMember]
        public DateTime I94ExpirationDate
        {
            get;
            set;
        }

        [DataMember]
        public string H1BEmployeerHistory
        {
            get;
            set;
        }

        [DataMember]
        public DateTime H1BApplicationDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime H1BRecordDate
        {
            get;
            set;
        }

        [DataMember]
        public string H1BEACNumber
        {
            get;
            set;
        }

        [DataMember]
        public DateTime H1BApprovalDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime H1BExpirationDate
        {
            get;
            set;
        }

        [DataMember]
        public string H1BJobTitle
        {
            get;
            set;
        }

        [DataMember]
        public string H1BQuery
        {
            get;
            set;
        }

        [DataMember]
        public DateTime H1BQueryRecordDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime H1BQueryResponseDate
        {
            get;
            set;
        }

        [DataMember]
        public int AttornyId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberImmigration()
            : base()
        {
        }

        #endregion
    }
}