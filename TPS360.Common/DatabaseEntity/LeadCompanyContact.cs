﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "LeadCompanyContact", Namespace = "http://www.tps360.com/types")]
    public class LeadCompanyContact : BaseEntity
    {
        #region Properties

        [DataMember]
        public int LeadId
        {
            get;
            set;
        }

        [DataMember]
        public int CompanyContactId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public LeadCompanyContact()
            : base()
        {
        }

        #endregion
    }
}