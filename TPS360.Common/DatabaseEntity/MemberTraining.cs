﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberTraining", Namespace = "http://www.tps360.com/types")]
    public class MemberTraining : BaseEntity
    {
        #region Properties

        [DataMember]
        public string BPTraining
        {
            get;
            set;
        }

        [DataMember]
        public int BPTrainingProvidedBy
        {
            get;
            set;
        }

        [DataMember]
        public bool BPTrainingChecked
        {
            get;
            set;
        }

        [DataMember]
        public int BPTrainingCheckedBy
        {
            get;
            set;
        }

        [DataMember]
        public string RPMSTraining
        {
            get;
            set;
        }

        [DataMember]
        public int RPMSTrainingProvidedBy
        {
            get;
            set;
        }

        [DataMember]
        public bool RPMSTrainingChecked
        {
            get;
            set;
        }

        [DataMember]
        public int RPMSTrainingCheckedBy
        {
            get;
            set;
        }

        [DataMember]
        public string PhoneTraining
        {
            get;
            set;
        }

        [DataMember]
        public int PhoneTrainingProvidedBy
        {
            get;
            set;
        }

        [DataMember]
        public bool PhoneTrainingChecked
        {
            get;
            set;
        }

        [DataMember]
        public int PhoneTrainingCheckedBy
        {
            get;
            set;
        }

        [DataMember]
        public string GeneralEmail
        {
            get;
            set;
        }

        [DataMember]
        public string GeneralEmailBy
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberTraining()
            : base()
        {
        }

        #endregion
    }
}