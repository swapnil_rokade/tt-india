﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "ProjectJobPostingMap", Namespace = "http://www.tps360.com/types")]
    public class ProjectJobPostingMap : BaseEntity
    {
        #region Properties

        [DataMember]
        public int JobPostingId
        {
            get;
            set;
        }

        [DataMember]
        public int ProjectId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public ProjectJobPostingMap()
            : base()
        {
        }

        #endregion
    }
}