﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using TPS360.Common.Shared;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberAlert", Namespace = "http://www.tps360.com/types")]
    public class MemberAlert : BaseEntity
    {
        #region Properties

        [DataMember]
        public int AlertId
        {
            get;
            set;
        }

        [DataMember]
        public Alert Alert
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public DateTime AlertDate
        {
            get;
            set;
        }

        public string CreatedAgo
        {
            get
            {
                return AlertDate.Ago();
            }
        }

        [DataMember]
        public AlertStatus AlertStatus
        {
            get;
            set;
        }

        [DataMember]
        public DateTime AlertViewDate
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberAlert()
            : base()
        {
        }

        public MemberAlert(int alertId, int memberId): base()
        {
            this.AlertId = alertId;
            this.MemberId = memberId;
            this.AlertStatus = AlertStatus.NotViewed;
        }

        #endregion
    }
}