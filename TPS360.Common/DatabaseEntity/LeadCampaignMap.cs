﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "LeadCampaignMap", Namespace = "http://www.tps360.com/types")]
    public class LeadCampaignMap : BaseEntity
    {
        #region Properties

        [DataMember]
        public int LeadId
        {
            get;
            set;
        }

        [DataMember]
        public int CampaignId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public LeadCampaignMap()
            : base()
        {
        }

        #endregion
    }
}
