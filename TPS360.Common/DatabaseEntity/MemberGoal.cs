﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberGoal", Namespace = "http://www.tps360.com/types")]
    public class MemberGoal : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Title
        {
            get;
            set;
        }

        [DataMember]
        public string Objective
        {
            get;
            set;
        }

        [DataMember]
        public decimal Weight
        {
            get;
            set;
        }

        [DataMember]
        public string Description
        {
            get;
            set;
        }

        [DataMember]
        public DateTime DueDate
        {
            get;
            set;
        }

        [DataMember]
        public string ActionSteps
        {
            get;
            set;
        }

        [DataMember]
        public string MeasureOfSuccess
        {
            get;
            set;
        }

        [DataMember]
        public int Status
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberGoal()
            : base()
        {
        }

        #endregion
    }
}