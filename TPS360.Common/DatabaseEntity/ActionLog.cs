﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "ActionLog", Namespace = "http://www.tps360.com/types")]
    public class ActionLog : BaseEntity
    {
        #region Properties

        [DataMember]
        public string TableName
        {
            get;
            set;
        }

        [DataMember]
        public byte[] PrimaryObject
        {
            get;
            set;
        }

        [DataMember]
        public byte[] ModifiedObject
        {
            get;
            set;
        }

        [DataMember]
        public string Action
        {
            get;
            set;
        }

        [DataMember]
        public int PerformerId
        {
            get;
            set;
        }

        [DataMember]
        public DateTime ActionDate
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public ActionLog()
            : base()
        {
        }

        #endregion
    }
}