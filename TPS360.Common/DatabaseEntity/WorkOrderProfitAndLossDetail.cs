﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "WorkOrderProfitAndLossDetail", Namespace = "http://www.tps360.com/types")]
    public class WorkOrderProfitAndLossDetail : BaseEntity
    {
        #region Properties

        [DataMember]
        public decimal RegularHour
        {
            get;
            set;
        }

        [DataMember]
        public decimal OverTimeHour
        {
            get;
            set;
        }

        [DataMember]
        public decimal RegularHourIncome
        {
            get;
            set;
        }

        [DataMember]
        public decimal OverTimeHourIncome
        {
            get;
            set;
        }

        [DataMember]
        public decimal ExpenseIncome
        {
            get;
            set;
        }

        [DataMember]
        public decimal RegularHourCost
        {
            get;
            set;
        }

        [DataMember]
        public decimal OverTimeHourCost
        {
            get;
            set;
        }

        [DataMember]
        public decimal ExpensesCost
        {
            get;
            set;
        }

        [DataMember]
        public decimal Commission
        {
            get;
            set;
        }

        [DataMember]
        public decimal GrossProfit
        {
            get;
            set;
        }

        [DataMember]
        public decimal TaxRate
        {
            get;
            set;
        }

        [DataMember]
        public decimal TotalTax
        {
            get;
            set;
        }

        [DataMember]
        public decimal NetProfit
        {
            get;
            set;
        }

        [DataMember]
        public int WorkOrderProfitAndLossMasterId
        {
            get;
            set;
        }

        [DataMember]
        public int TimeSheetId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public WorkOrderProfitAndLossDetail()
            : base()
        {
        }

        #endregion
    }
}