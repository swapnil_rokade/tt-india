﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberDocuments.cs
    Description: This is Property Page from MemberDocument.
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    0.1              23/Dec/2015           Prasanth            Introduced InterviewId, InterviewerEmail
 */

using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberDocument", Namespace = "http://www.tps360.com/types")]
    public class MemberDocument : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Title
        {
            get;
            set;
        }

        [DataMember]
        public string FileName
        {
            get;
            set;
        }

        [DataMember]
        public string Description
        {
            get;
            set;
        }

        [DataMember]
        public int FileTypeLookupId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        //Code introduced by Prasanth on 23/Dec/2015 Start
        [DataMember]
        public int InterviewId
        {
            get;
            set;
        }

        [DataMember]
        public string InterviewerEmail
        {
            get;
            set;
        }


        //*******************END************************



        #endregion

        #region Constructor

        public MemberDocument()
            : base()
        {
        }

        #endregion
    }
}