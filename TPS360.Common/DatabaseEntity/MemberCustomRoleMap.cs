﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberCustomRoleMap", Namespace = "http://www.tps360.com/types")]
    public class MemberCustomRoleMap : BaseEntity
    {
        #region Properties

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int CustomRoleId
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberCustomRoleMap()
            : base()
        {
        }

        #endregion
    }
}