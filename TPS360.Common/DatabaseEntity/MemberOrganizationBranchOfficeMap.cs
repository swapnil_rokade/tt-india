﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberOrganizationBranchOfficeMap", Namespace = "http://www.tps360.com/types")]
    public class MemberOrganizationBranchOfficeMap : BaseEntity
    {
        #region Properties

        [DataMember]
        public int OrganizationBranchOfficeId
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberOrganizationBranchOfficeMap()
            : base()
        {
        }

        #endregion
    }
}