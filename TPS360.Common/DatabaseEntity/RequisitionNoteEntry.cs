﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "EventLogForRequisitionAndCandidate")]
    public class RequisitionNotesEntry:BaseEntity
    {
      
        [DataMember]
        public string NoteDetail
        {
            get;
            set;
        }

        [DataMember]
        public string CreatorName
        {
            get;
            set;
        }

        public RequisitionNotesEntry(): base()
        {
        }
    }
}
