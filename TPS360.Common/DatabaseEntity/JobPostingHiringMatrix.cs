﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "JobPostingHiringMatrix", Namespace = "http://www.tps360.com/types")]
    public class JobPostingHiringMatrix : BaseEntity
    {
        #region Properties

        [DataMember]
        public string JobTitleDescription
        {
            get;
            set;
        }

        [DataMember]
        public int JobTitlePercentage
        {
            get;
            set;
        }

        [DataMember]
        public string LocationDescription
        {
            get;
            set;
        }

        [DataMember]
        public int LocationPercentage
        {
            get;
            set;
        }

        [DataMember]
        public string YearOfExpDescription
        {
            get;
            set;
        }

        [DataMember]
        public int YearOfExpPercentage
        {
            get;
            set;
        }

        [DataMember]
        public string IndustryExpDescription
        {
            get;
            set;
        }

        [DataMember]
        public int IndustryExpPercentage
        {
            get;
            set;
        }

        [DataMember]
        public string TechnicalSkillDescription
        {
            get;
            set;
        }

        [DataMember]
        public int TechnicalSkillPercentage
        {
            get;
            set;
        }

        [DataMember]
        public string EducationDescription
        {
            get;
            set;
        }

        [DataMember]
        public int EducationPercentage
        {
            get;
            set;
        }

        [DataMember]
        public string LegalStatusDescription
        {
            get;
            set;
        }

        [DataMember]
        public int LegalStatusPercentage
        {
            get;
            set;
        }

        [DataMember]
        public string SalaryDescription
        {
            get;
            set;
        }

        [DataMember]
        public int SalaryPercentage
        {
            get;
            set;
        }

        [DataMember]
        public int MinScoreForFirstInterview
        {
            get;
            set;
        }

        [DataMember]
        public int JobPostingId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public JobPostingHiringMatrix()
            : base()
        {
        }

        #endregion
    }
}