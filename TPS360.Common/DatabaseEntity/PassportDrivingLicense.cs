﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "PassportDrivingLicense", Namespace = "http://www.tps360.com/types")]
    public class PassportDrivingLicense : BaseEntity
    {
        #region Properties

        [DataMember]
        public string PassportNumber
        {
            get;
            set;
        }

        [DataMember]
        public DateTime PassportIssueDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime PassportExpirationDate
        {
            get;
            set;
        }

        [DataMember]
        public string PassportIssueCity
        {
            get;
            set;
        }

        [DataMember]
        public int PassportIssueCountryId
        {
            get;
            set;
        }

        [DataMember]
        public int PassportStatus
        {
            get;
            set;
        }

        [DataMember]
        public string PassportType
        {
            get;
            set;
        }

        [DataMember]
        public string PassportProfession
        {
            get;
            set;
        }

        [DataMember]
        public string DrivingLicenseNumber
        {
            get;
            set;
        }

        [DataMember]
        public DateTime DrivingLicenseIssueDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime DrivingLicenseExpirationDate
        {
            get;
            set;
        }

        [DataMember]
        public int DrivingLicenseStateId
        {
            get;
            set;
        }

        [DataMember]
        public int DrivingLicenseCountryId
        {
            get;
            set;
        }

        [DataMember]
        public int DrivingLicenseStatus
        {
            get;
            set;
        }

        [DataMember]
        public int IsECR
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public PassportDrivingLicense()
            : base()
        {
        }

        #endregion
    }
}