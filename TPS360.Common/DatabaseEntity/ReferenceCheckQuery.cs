﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "ReferenceCheckQuery", Namespace = "http://www.tps360.com/types")]
    public class ReferenceCheckQuery : BaseEntity
    {
        #region Properties

        [DataMember]
        public string QueryQuestion
        {
            get;
            set;
        }

        [DataMember]
        public int QueryCategoryLookupId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public ReferenceCheckQuery()
            : base()
        {
        }

        #endregion
    }
}
