﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "PositionCompetencyMap", Namespace = "http://www.tps360.com/types")]
    public class PositionCompetencyMap : BaseEntity
    {
        #region Properties

        [DataMember]
        public int PositionId
        {
            get;
            set;
        }

        [DataMember]
        public int CompetencyId
        {
            get;
            set;
        }

        [DataMember]
        public string ShortName
        {
            get;
            set;
        }

        [DataMember]
        public int Rating
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public PositionCompetencyMap()
            : base()
        {
        }

        #endregion
    }
}