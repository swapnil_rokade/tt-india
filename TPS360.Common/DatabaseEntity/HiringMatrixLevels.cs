﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "HiringMatrixLevels", Namespace = "http://www.tps360.com/types")]
    public class HiringMatrixLevels : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Name
        {
            get;
            set;
        }
        [DataMember]
        public int SortingOrder
        {
            get;
            set;
        }

        [DataMember]
        public int Count
        {
            get;
            set;
        }
        #endregion

        #region Constructor

        public HiringMatrixLevels()
            : base()
        {
        }

        #endregion
    }
}