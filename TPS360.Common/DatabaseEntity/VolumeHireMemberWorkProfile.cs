﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "VolumeHireMemberWorkProfile", Namespace = "http://www.tps360.com/types")]
    public class VolumeHireMemberWorkProfile : BaseEntity
    {
        #region Properties

        [DataMember]
        public int FunctionalCategoryId
        {
            get;
            set;
        }

        [DataMember]
        public string JobTitle
        {
            get;
            set;
        }

        [DataMember]
        public int EducationLookupId
        {
            get;
            set;
        }

        [DataMember]
        public decimal CurrentSalary
        {
            get;
            set;
        }

        [DataMember]
        public int CurrentSalaryCurrencylookupId
        {
            get;
            set;
        }

        [DataMember]
        public string YearOfExperience
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public VolumeHireMemberWorkProfile()
            : base()
        {
        }

        #endregion
    }
}