﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberNote", Namespace = "http://www.tps360.com/types")]
    public class MemberNote : BaseEntity
    {
        #region Properties

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public int CommonNoteId
        {
            get;
            set;
        }

        [DataMember]
        public int JobPostingId
        {
            get;
            set;
        }
        #endregion

        #region Constructor

        public MemberNote()
            : base()
        {
        }

        #endregion
    }
}