using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "AutomatedEmailRemoval", Namespace = "http://www.tps360.com/types")]
    public class AutomatedEmailRemoval : BaseEntity
    {
        #region Properties

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public DateTime RemovalDate
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public AutomatedEmailRemoval()
            : base()
        {
        }

        #endregion
    }
}