﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberEmailDetail", Namespace = "http://www.tps360.com/types")]
    public class MemberEmailDetail : BaseEntity
    {
        #region Properties

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public string EmailAddress
        {
            get;
            set;
        }

        [DataMember]
        public int AddressTypeId
        {
            get;
            set;
        }

        [DataMember]
        public int SendingTypeId
        {
            get;
            set;
        }

        [DataMember]
        public int MemberEmailId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberEmailDetail()
            : base()
        {
        }

        #endregion
    }
}