﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "Invoice", Namespace = "http://www.tps360.com/types")]
    public class Invoice : BaseEntity
    {
        #region Properties

        [DataMember]
        public int ToId
        {
            get;
            set;
        }

        [DataMember]
        public string ToName
        {
            get;
            set;
        }

        [DataMember]
        public string ToAddress1
        {
            get;
            set;
        }

        [DataMember]
        public string ToAddress2
        {
            get;
            set;
        }

        [DataMember]
        public string ToCity
        {
            get;
            set;
        }

        [DataMember]
        public int ToStateId
        {
            get;
            set;
        }

        [DataMember]
        public int ToCountryId
        {
            get;
            set;
        }

        [DataMember]
        public string ToZip
        {
            get;
            set;
        }

        [DataMember]
        public string ToPhone
        {
            get;
            set;
        }

        [DataMember]
        public string ToFax
        {
            get;
            set;
        }

        [DataMember]
        public int FromId
        {
            get;
            set;
        }

        [DataMember]
        public string FromName
        {
            get;
            set;
        }

        [DataMember]
        public string FromAddress1
        {
            get;
            set;
        }

        [DataMember]
        public string FromAddress2
        {
            get;
            set;
        }

        [DataMember]
        public string FromCity
        {
            get;
            set;
        }

        [DataMember]
        public int FromStateId
        {
            get;
            set;
        }

        [DataMember]
        public string FromZip
        {
            get;
            set;
        }

        [DataMember]
        public string FromPhone
        {
            get;
            set;
        }

        [DataMember]
        public string FromFax
        {
            get;
            set;
        }

        [DataMember]
        public DateTime InvoiceDate
        {
            get;
            set;
        }

        [DataMember]
        public string InvoiceNumber
        {
            get;
            set;
        }

        [DataMember]
        public string POReferenceNumber
        {
            get;
            set;
        }

        [DataMember]
        public DateTime DueDate
        {
            get;
            set;
        }

        [DataMember]
        public string PurchaseOrder
        {
            get;
            set;
        }

        [DataMember]
        public string CreatedBy
        {
            get;
            set;
        }

        [DataMember]
        public decimal UnitPrice
        {
            get;
            set;
        }

        [DataMember]
        public int UnitPriceCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int Quantity
        {
            get;
            set;
        }

        [DataMember]
        public int ProductId
        {
            get;
            set;
        }

        [DataMember]
        public decimal ByProductTotal
        {
            get;
            set;
        }

        [DataMember]
        public decimal SubTotal
        {
            get;
            set;
        }

        [DataMember]
        public decimal AccumulatedSubTotal
        {
            get;
            set;
        }

        [DataMember]
        public decimal TaxRate
        {
            get;
            set;
        }

        [DataMember]
        public decimal TaxAmount
        {
            get;
            set;
        }

        [DataMember]
        public decimal Discount
        {
            get;
            set;
        }

        [DataMember]
        public decimal Total
        {
            get;
            set;
        }

        [DataMember]
        public string Description
        {
            get;
            set;
        }

        [DataMember]
        public string OfficeComment
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public Invoice()
            : base()
        {
        }

        #endregion
    }
}