﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "OccupationalSeries", Namespace = "http://www.tps360.com/types")]
    public class OccupationalSeries : BaseEntity
    {
        #region Properties

        [DataMember]
        public string SeriesTitle
        {
            get;
            set;
        }
        [DataMember]
        public int GroupId
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public OccupationalSeries()
            : base()
        {
        }

        #endregion
    }
}