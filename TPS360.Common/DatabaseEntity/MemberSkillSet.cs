﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberSkillSet", Namespace = "http://www.tps360.com/types")]
    public class MemberSkillSet : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Proficiency
        {
            get;
            set;
        }

        [DataMember]
        public string Description
        {
            get;
            set;
        }

        [DataMember]
        public string YearsOfExperience
        {
            get;
            set;
        }

        [DataMember]
        public DateTime LastUsed
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public int SkillId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberSkillSet()
            : base()
        {
        }

        #endregion
    }
}