﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "TrainingProgram", Namespace = "http://www.tps360.com/types")]
    public class TrainingProgram : BaseEntity
    {
        #region Properties

        
        [DataMember]
        public string Title
        {
            get;
            set;
        }
        
        [DataMember]
        public int TotalClass
        {
            get;
            set;
        }

        [DataMember]
        public decimal PerClassDuration
        {
            get;
            set;
        }

        [DataMember]
        public decimal Fees
        {
            get;
            set;
        }

        [DataMember]
        public int FeesCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string Location
        {
            get;
            set;
        }

        [DataMember]
        public DateTime StartDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime EndDate
        {
            get;
            set;
        }

        [DataMember]
        public int TrainingCourseId
        {
            get;
            set;
        }

        [DataMember]
        public int Status
        {
            get;
            set;
        }        

        [DataMember]
        public bool IsRecurring
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public TrainingProgram()
            : base()
        {
        }

        #endregion
    }
}