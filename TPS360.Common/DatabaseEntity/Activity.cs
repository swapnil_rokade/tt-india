﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "Activity", Namespace = "http://www.tps360.com/types")]
    public class Activity : BaseEntity
    {
        #region Properties

        [DataMember]
        public int ActivityID
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }
        [DataMember]
        public DateTime StartDateTimeUtc
        {
            get;
            set;
        }

        [DataMember]
        public int Duration
        {
            get;
            set;
        }

        [DataMember]
        public string Subject
        {
            get;
            set;
        }

        [DataMember]
        public string ActivityDescription
        {
            get;
            set;
        }

        [DataMember]
        public bool AllDayEvent
        {
            get;
            set;
        }

        [DataMember]
        public string Location
        {
            get;
            set;
        }

        [DataMember]
        public int Status
        {
            get;
            set;
        }

        [DataMember]
        public bool EnableReminder
        {
            get;
            set;
        }

        [DataMember]
        public int ReminderInterval
        {
            get;
            set;
        }

        [DataMember]
        public int ShowTimeAs
        {
            get;
            set;
        }

        [DataMember]
        public int Importance
        {
            get;
            set;
        }

        [DataMember]
        public byte[] _ts
        {
            get;
            set;
        }

        [DataMember]
        public int RecurrenceID
        {
            get;
            set;
        }

        [DataMember]
        public DateTime OriginalStartDateTimeUtc
        {
            get;
            set;
        }

        [DataMember]
        public Guid VarianceID
        {
            get;
            set;
        }

        [DataMember]
        public string ResourceName
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public Activity()
            : base()
        {
        }

        #endregion
    }
}