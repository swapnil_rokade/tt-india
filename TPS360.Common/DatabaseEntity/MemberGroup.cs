﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberGroup", Namespace = "http://www.tps360.com/types")]
    public class MemberGroup : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public string Description
        {
            get;
            set;
        }
        
        [DataMember]
        public int GroupType
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int CandidateCount
        {
            get;
            set;
        }

        [DataMember]
        public string  ManagerName
        {
            get;
            set;
        }
        #endregion

        #region Constructor

        public MemberGroup()
            : base()
        {
        }

        #endregion
    }
}