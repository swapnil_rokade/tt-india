﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "HROfferLetterAllounce", Namespace = "http://www.tps360.com/types")]
    public class HROfferLetterAllounce : BaseEntity
    {
        #region Properties

        [DataMember]
        public int AllounceLookupId
        {
            get;
            set;
        }

        [DataMember]
        public decimal AllounceValue
        {
            get;
            set;
        }

        [DataMember]
        public string AllounceValueInWords
        {
            get;
            set;
        }

        [DataMember]
        public int AllounceCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string AllounceNote
        {
            get;
            set;
        }

        [DataMember]
        public int HROfferLetterId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public HROfferLetterAllounce()
            : base()
        {
        }

        #endregion
    }
}