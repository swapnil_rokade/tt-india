﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "TrainingProgramTraineeMap", Namespace = "http://www.tps360.com/types")]
    public class TrainingProgramTraineeMap : BaseEntity
    {
        #region Properties

        [DataMember]
        public int Status
        {
            get;
            set;
        }

        [DataMember]
        public string Note
        {
            get;
            set;
        }

        [DataMember]
        public string StudentFeedback
        {
            get;
            set;
        }

        [DataMember]
        public string StudentScore
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public int TrainingProgramId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public TrainingProgramTraineeMap()
            : base()
        {
        }

        #endregion
    }
}