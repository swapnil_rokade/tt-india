﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "Alert", Namespace = "http://www.tps360.com/types")]
    public class Alert : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Title
        {
            get;
            set;
        }

        [DataMember]
        public string Description
        {
            get;
            set;
        }

        [DataMember]
        public int CategoryLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int WorkflowTitleId
        {
            get;
            set;
        }

        [DataMember]
        public int IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public Alert()
            : base()
        {
        }

        #endregion
    }
}