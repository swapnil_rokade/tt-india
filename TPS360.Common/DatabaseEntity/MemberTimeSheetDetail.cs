﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberTimeSheetDetail", Namespace = "http://www.tps360.com/types")]
    public class MemberTimeSheetDetail : BaseEntity
    {
        #region Properties

        [DataMember]
        public DateTime WorkDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime StartTime
        {
            get;
            set;
        }

        [DataMember]
        public DateTime EndTime
        {
            get;
            set;
        }

        [DataMember]
        public decimal HoursWorked
        {
            get;
            set;
        }

        [DataMember]
        public decimal OvertimeWorked
        {
            get;
            set;
        }

        [DataMember]
        public int MemberTimeSheetId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberTimeSheetDetail()
            : base()
        {
        }

        #endregion
    }
}