﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "GoalComment", Namespace = "http://www.tps360.com/types")]
    public class GoalComment : BaseEntity
    {
        #region Properties

        [DataMember]
        public string MemberComment
        {
            get;
            set;
        }

        [DataMember]
        public bool IsMemberAgree
        {
            get;
            set;
        }

        [DataMember]
        public int ManagerId
        {
            get;
            set;
        }

        [DataMember]
        public string ManagerComment
        {
            get;
            set;
        }

        [DataMember]
        public int MemberGoalId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public GoalComment()
            : base()
        {
        }

        #endregion
    }
}