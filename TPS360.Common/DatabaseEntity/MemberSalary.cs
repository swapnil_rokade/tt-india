﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberSalary", Namespace = "http://www.tps360.com/types")]
    public class MemberSalary : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Month
        {
            get;
            set;
        }

        [DataMember]
        public string Year
        {
            get;
            set;
        }

        [DataMember]
        public decimal HourlySalary
        {
            get;
            set;
        }

        [DataMember]
        public int HourlySalaryCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public decimal MonthlySalary
        {
            get;
            set;
        }

        [DataMember]
        public int MonthlySalaryCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public decimal YearlySalary
        {
            get;
            set;
        }

        [DataMember]
        public int YearlySalaryCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string TaxTerm
        {
            get;
            set;
        }

        [DataMember]
        public string Benefits
        {
            get;
            set;
        }

        [DataMember]
        public string NoteForBenefit
        {
            get;
            set;
        }

        [DataMember]
        public DateTime HireDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime BenefitEligibleDate
        {
            get;
            set;
        }

        [DataMember]
        public string ProbationPeriod
        {
            get;
            set;
        }

        [DataMember]
        public string AdditionalSalaryNote
        {
            get;
            set;
        }

        [DataMember]
        public string PerformanceNote
        {
            get;
            set;
        }

        [DataMember]
        public string WorkHour
        {
            get;
            set;
        }

        [DataMember]
        public string OverTimePayDetail
        {
            get;
            set;
        }

        [DataMember]
        public decimal Conveyance
        {
            get;
            set;
        }

        [DataMember]
        public int ConveyanceCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public decimal MonthlyMedical
        {
            get;
            set;
        }

        [DataMember]
        public int MonthlyMedicalCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public decimal MonthlySpecial
        {
            get;
            set;
        }

        [DataMember]
        public int MonthlySpecialCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public decimal MonthlyArrear
        {
            get;
            set;
        }

        [DataMember]
        public int MonthlyArrearCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public decimal MonthlyPT
        {
            get;
            set;
        }

        [DataMember]
        public int MonthlyPTCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public decimal MonthlyLoP
        {
            get;
            set;
        }

        [DataMember]
        public int MonthlyLoPCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public decimal MontylyPF
        {
            get;
            set;
        }

        [DataMember]
        public int MontylyPFCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public decimal MonthlyCellPhoneBill
        {
            get;
            set;
        }

        [DataMember]
        public int MonthlyCellPhoneBillCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public decimal MonthlyGrossSalary
        {
            get;
            set;
        }

        [DataMember]
        public int MonthlyGrossSalaryCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public decimal MonthlyNetSalary
        {
            get;
            set;
        }

        [DataMember]
        public int MonthlyNetSalaryCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberSalary()
            : base()
        {
        }

        #endregion
    }
}