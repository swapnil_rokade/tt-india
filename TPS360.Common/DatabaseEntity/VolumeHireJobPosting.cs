﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "VolumeHireJobPosting", Namespace = "http://www.tps360.com/types")]
    public class VolumeHireJobPosting : BaseEntity
    {
        #region Properties

        [DataMember]
        public int JobPostingId
        {
            get;
            set;
        }

        [DataMember]
        public int VHClientId
        {
            get;
            set;
        }

        [DataMember]
        public int PaymentReqcruitmentSourceId
        {
            get;
            set;
        }

        [DataMember]
        public decimal PaymentReqcruitmentAmount
        {
            get;
            set;
        }

        [DataMember]
        public int PaymentReqcruitmentCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int PaymentAirTicketSourceId
        {
            get;
            set;
        }

        [DataMember]
        public decimal PaymentAirTicketAmount
        {
            get;
            set;
        }

        [DataMember]
        public int PaymentAirTicketCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int PaymentInsuranceSourceId
        {
            get;
            set;
        }

        [DataMember]
        public decimal PaymentInsuranceAmount
        {
            get;
            set;
        }

        [DataMember]
        public int PaymentInsuranceCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int PaymentMedicalSourceId
        {
            get;
            set;
        }

        [DataMember]
        public decimal PaymentMedicalAmount
        {
            get;
            set;
        }

        [DataMember]
        public int PaymentMedicalCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public decimal OverTimeRate
        {
            get;
            set;
        }

        [DataMember]
        public int OverTimeRateCurrencyLookUpId
        {
            get;
            set;
        }

        [DataMember]
        public int OverTimeRatePayCycleLookUpId
        {
            get;
            set;
        }

        [DataMember]
        public decimal HolidayRate
        {
            get;
            set;
        }

        [DataMember]
        public int HolidayRateCurrencyLookUpId
        {
            get;
            set;
        }

        [DataMember]
        public int HolidayRatePayCycleLookUpId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public VolumeHireJobPosting()
            : base()
        {
        }

        #endregion
    }
}