﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "SearchAgentMailToCandidate", Namespace = "http://www.tps360.com/types")]
    public class SearchAgentMailToCandidate : BaseEntity 
    {
        #region Properties
        [DataMember]
        public int JobPostingId
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public int MemberEmailId
        {
            get;
            set;
        }
        #endregion
        #region Constructor

        public SearchAgentMailToCandidate()
            : base()
        {
        }

        #endregion
    }
}
