﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberGroupManager", Namespace = "http://www.tps360.com/types")]
    public class MemberGroupManager : BaseEntity
    {
        #region Properties

        [DataMember]
        public int MemberGroupId
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberGroupManager()
            : base()
        {
        }

        #endregion
    }
}