﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "TableAlias", Namespace = "http://www.tps360.com/types")]
    public class TableAlias : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public string Alias
        {
            get;
            set;
        }

        [DataMember]
        public bool Status
        {
            get;
            set;
        }

        [DataMember]
        public int ParentId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public TableAlias()
            : base()
        {
        }

        #endregion
    }
}