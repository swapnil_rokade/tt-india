﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "SiteSetting", Namespace = "http://www.tps360.com/types")]
    public class SiteSetting : BaseEntity
    {
        #region Properties

        [DataMember]
        public int SettingType
        {
            get;
            set;
        }

        [DataMember]
        public byte[] SettingConfiguration
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public SiteSetting()
            : base()
        {
        }

        #endregion
    }
}