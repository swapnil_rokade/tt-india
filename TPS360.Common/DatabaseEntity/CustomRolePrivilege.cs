﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CustomRolePrivilege", Namespace = "http://www.tps360.com/types")]
    public class CustomRolePrivilege : BaseEntity
    {
        #region Properties

        [DataMember]
        public int Privilege
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int CustomRoleId
        {
            get;
            set;
        }

        [DataMember]
        public int CustomSiteMapId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public CustomRolePrivilege()
            : base()
        {
        }

        #endregion
    }
}