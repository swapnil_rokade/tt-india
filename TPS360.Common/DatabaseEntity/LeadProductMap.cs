﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "LeadProductMap", Namespace = "http://www.tps360.com/types")]
    public class LeadProductMap : BaseEntity
    {
        #region Properties

        [DataMember]
        public int LeadId
        {
            get;
            set;
        }

        [DataMember]
        public int ProductId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public LeadProductMap()
            : base()
        {
        }

        #endregion
    }
}
