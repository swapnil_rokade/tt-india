﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CustomRole", Namespace = "http://www.tps360.com/types")]
    public class CustomRole : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public string Description
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public bool IsSystemDefault
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public CustomRole()
            : base()
        {
        }

        #endregion
    }
}