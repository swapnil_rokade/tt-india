﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CampaignTeam", Namespace = "http://www.tps360.com/types")]
    public class CampaignTeam : BaseEntity
    {
        #region Properties

        [DataMember]
        public int CampaignId
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        public DateTime AssignDate
        {
            get;
            set;
        }

        public bool IsManager
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public CampaignTeam()
            : base()
        {
        }

        #endregion
    }
}
