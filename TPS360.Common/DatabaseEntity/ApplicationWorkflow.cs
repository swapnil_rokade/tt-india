﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "ApplicationWorkflow", Namespace = "http://www.tps360.com/types")]
    public class ApplicationWorkflow : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Title
        {
            get;
            set;
        }

        [DataMember]
        public string Description
        {
            get;
            set;
        }

        [DataMember]
        public bool RequireApproval
        {
            get;
            set;
        }

        [DataMember]
        public string TimeFrame
        {
            get;
            set;
        }

        [DataMember]
        public bool IsEmailAlert
        {
            get;
            set;
        }

        [DataMember]
        public bool IsSmsAlert
        {
            get;
            set;
        }

        [DataMember]
        public bool IsDashboardAlert
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public ApplicationWorkflow()
            : base()
        {
        }

        #endregion
    }
}