﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CompanyGroupTeam", Namespace = "http://www.tps360.com/types")]
    public class CompanyGroupTeam : BaseEntity
    {
        #region Properties

        [DataMember]
        public int CompanyGroupId
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsManager
        {
            get;
            set;
        }

        [DataMember]
        public DateTime AssignDate
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public CompanyGroupTeam()
            : base()
        {
        }

        #endregion
    }
}