﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "HolidayCalendarDetail", Namespace = "http://www.tps360.com/types")]
    public class HolidayCalendarDetail : BaseEntity
    {
        #region Properties

        [DataMember]
        public int HolidayDay
        {
            get;
            set;
        }

        [DataMember]
        public int HolidayMonth
        {
            get;
            set;
        }

        [DataMember]
        public int HolidayYear
        {
            get;
            set;
        }

        [DataMember]
        public int HolidayCalendarId
        {
            get;
            set;
        }

        [DataMember]
        public int Status
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public HolidayCalendarDetail()
            : base()
        {
        }

        #endregion
    }
}
