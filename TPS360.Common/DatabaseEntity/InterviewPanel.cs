﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :   InterviewPanel.cs
    Description         :   This page is used to Create Properties for InterviewPanel.
    Created By          :   Pravin
    Created On          :   20/nov/2015
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "InterviewPanel", Namespace = "http://www.tps360.com/types")]
    public class InterviewPanel : BaseEntity
    {
        #region Properties

        public int InterviewPanel_Id
        {
            get;
            set;
        }

        public int InterviewPanelType_Id
        {
            get;
            set;
        }
        public int Interviewer_Id
        {
            get;
            set;
        }

        public string Interviewer_Name    
        {
            get;
            set;
        }

        public string OtherInterviewers    
        {
            get;
            set;
        }

        public string Interview_Mode    
        {
            get;
            set;
        }
        public string InterviewPanel_Name  
        {
            get;
            set;
        }
        public string InterviewSkill  
        {
            get;
            set;
        }
             
        #endregion

        #region Constructor

        public InterviewPanel()
            : base()
        {
        }

        #endregion
    }
}