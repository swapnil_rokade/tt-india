﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "GuestHouse", Namespace = "http://www.tps360.com/types")]
    public class GuestHouse : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public string Address1
        {
            get;
            set;
        }

        [DataMember]
        public string Address2
        {
            get;
            set;
        }

        [DataMember]
        public string Phone
        {
            get;
            set;
        }

        [DataMember]
        public string City
        {
            get;
            set;
        }

        [DataMember]
        public int StateId
        {
            get;
            set;
        }

        [DataMember]
        public int CountryId
        {
            get;
            set;
        }

        [DataMember]
        public string Zip
        {
            get;
            set;
        }

        [DataMember]
        public decimal Rent
        {
            get;
            set;
        }

        [DataMember]
        public int RentCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string RentPayableTo
        {
            get;
            set;
        }

        [DataMember]
        public string Utilities
        {
            get;
            set;
        }

        [DataMember]
        public int NumberOfSeat
        {
            get;
            set;
        }

        [DataMember]
        public int TypeLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string Photo
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public GuestHouse()
            : base()
        {
        }

        #endregion
    }
}