﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberOnBoardingOffBoardingChecklistDetail", Namespace = "http://www.tps360.com/types")]
    public class MemberOnBoardingOffBoardingChecklistDetail : BaseEntity
    {
        #region Properties

        [DataMember]
        public string MemberComment
        {
            get;
            set;
        }

        [DataMember]
        public string ManagementComent
        {
            get;
            set;
        }

        [DataMember]
        public int Status
        {
            get;
            set;
        }

        [DataMember]
        public int OnBoardingOffBoardingChecklistId
        {
            get;
            set;
        }

        [DataMember]
        public int MemberOnBoardingOffBoardingChecklistId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberOnBoardingOffBoardingChecklistDetail()
            : base()
        {
        }

        #endregion
    }
}