﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "ProductByProductMap", Namespace = "http://www.tps360.com/types")]
    public class ProductByProductMap : BaseEntity
    {
        #region Properties

        [DataMember]
        public decimal UnitPrice
        {
            get;
            set;
        }

        [DataMember]
        public int ProductId
        {
            get;
            set;
        }

        [DataMember]
        public int ByProductId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public ProductByProductMap()
            : base()
        {
        }

        #endregion
    }
}