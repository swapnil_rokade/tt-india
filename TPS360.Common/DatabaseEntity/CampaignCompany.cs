﻿using System;
using System.Runtime.Serialization;
using TPS360.Common.Shared;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CampaignCompany", Namespace = "http://www.tps360.com/types")]
    public class CampaignCompany : BaseEntity
    {
        #region Properties

        [DataMember]
        public int CampaignId
        {
            get;
            set;
        }

        [DataMember]
        public int CompanyId
        {
            get;
            set;
        }

        private Company _company;

        [DataMember]
        public Company Company
        {
            get
            {
                if (_company == null)
                {
                    _company = new Company();
                }

                return _company;
            }
            set
            {
                _company = value;
            }
        }

        [DataMember]
        public CampaignCompanyStatus CampaignCompanyStatus
        {
            get;
            set;
        }

        [DataMember]
        public string RemoveComments
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public CampaignCompany() : base()
        {
        }

        #endregion
    }
}
