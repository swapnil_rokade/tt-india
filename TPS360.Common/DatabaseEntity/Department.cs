﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "Department", Namespace = "http://www.tps360.com/types")]
    public class Department : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Name
        {
            get;
            set;
        }

       

        #endregion

        #region Constructor

        public Department()
            : base()
        {
        }

        #endregion
    }
}