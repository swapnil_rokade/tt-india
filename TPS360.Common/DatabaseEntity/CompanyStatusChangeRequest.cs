﻿using System;
using System.Runtime.Serialization;
using TPS360.Common.Shared;
using System.Collections.Specialized;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CompanyStatusChangeRequest", Namespace = "http://www.tps360.com/types")]
    public class CompanyStatusChangeRequest : BaseEntity
    {
        #region Properties

        private StringCollection _checkList;

        [DataMember]
        public int LastStatus
        {
            get;
            set;
        }

        [DataMember]
        public int RequestedStatus
        {
            get;
            set;
        }

        [DataMember]
        public StringCollection CheckList
        {
            get
            {
                if (_checkList == null)
                {
                    _checkList = new StringCollection();
                }

                return _checkList;
            }
        }

        [DataMember]
        public string Notes
        {
            get;
            set;
        }

        [DataMember]
        public int CompanyId
        {
            get;
            set;
        }

        [DataMember]
        public int RequestorId
        {
            get;
            set;
        }

        [DataMember]
        public DateTime RequestDate
        {
            get;
            set;
        }

        [DataMember]
        public ApproveStatus ApproveStatus
        {
            get;
            set;
        }

        [DataMember]
        public int ApproveStatusChangerId
        {
            get;
            set;
        }

        [DataMember]
        public DateTime ApproveStatusChangeDate
        {
            get;
            set;
        }

        [DataMember]
        public string ApproveStatusComments
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public CompanyStatusChangeRequest()
            : base()
        {
        }

        #endregion
    }
}