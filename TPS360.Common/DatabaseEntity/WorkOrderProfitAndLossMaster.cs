﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "WorkOrderProfitAndLossMaster", Namespace = "http://www.tps360.com/types")]
    public class WorkOrderProfitAndLossMaster : BaseEntity
    {
        #region Properties

        [DataMember]
        public decimal RegularHour
        {
            get;
            set;
        }

        [DataMember]
        public decimal OvertimeHour
        {
            get;
            set;
        }

        [DataMember]
        public decimal RegularHourIncome
        {
            get;
            set;
        }

        [DataMember]
        public decimal OverTimeHourIncome
        {
            get;
            set;
        }

        [DataMember]
        public decimal ExpenseIncome
        {
            get;
            set;
        }

        [DataMember]
        public decimal RegularHourCost
        {
            get;
            set;
        }

        [DataMember]
        public decimal OverTimeHourCost
        {
            get;
            set;
        }

        [DataMember]
        public decimal ExpenseCost
        {
            get;
            set;
        }

        [DataMember]
        public decimal Commission
        {
            get;
            set;
        }

        [DataMember]
        public decimal GrossProfit
        {
            get;
            set;
        }

        [DataMember]
        public decimal TotalTax
        {
            get;
            set;
        }

        [DataMember]
        public decimal NetProfit
        {
            get;
            set;
        }

        [DataMember]
        public int SupplierWorkOrderId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public WorkOrderProfitAndLossMaster()
            : base()
        {
        }

        #endregion
    }
}