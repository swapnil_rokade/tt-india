﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "PositionIndustryCapabilityMap", Namespace = "http://www.tps360.com/types")]
    public class PositionIndustryCapabilityMap : BaseEntity
    {
        #region Properties

        [DataMember]
        public int PositionId
        {
            get;
            set;
        }

        [DataMember]
        public int IndustryCapabilityId
        {
            get;
            set;
        }

        [DataMember]
        public int Rating
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public PositionIndustryCapabilityMap()
            : base()
        {
        }

        #endregion
    }
}