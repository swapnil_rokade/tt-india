﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "TestQuestionFile", Namespace = "http://www.tps360.com/types")]
    public class TestQuestionFile : BaseEntity
    {
        #region Properties

        [DataMember]
        public string FileType
        {
            get;
            set;
        }

        [DataMember]
        public string FileName
        {
            get;
            set;
        }

        [DataMember]
        public int TestQuestionId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public TestQuestionFile()
            : base()
        {
        }

        #endregion
    }
}