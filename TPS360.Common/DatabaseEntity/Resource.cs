﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "Resource", Namespace = "http://www.tps360.com/types")]
    public class Resource : BaseEntity
    {
        #region Properties

        [DataMember]
        public int ResourceID
        {
            get;
            set;
        }

        [DataMember]
        public string ResourceName
        {
            get;
            set;
        }

        [DataMember]
        public string ResourceDescription
        {
            get;
            set;
        }

        [DataMember]
        public string EmailAddress
        {
            get;
            set;
        }

        [DataMember]
        public byte[] _ts
        {
            get;
            set;
        }
        
        [DataMember]
        public int EnableEmailReminders
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public Resource()
            : base()
        {
        }

        #endregion
    }
}