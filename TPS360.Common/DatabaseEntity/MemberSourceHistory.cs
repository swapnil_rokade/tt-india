﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberSourceHistory", Namespace = "http://www.tps360.com/types")]
    public class MemberSourceHistory : BaseEntity
    {
        #region Properties

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }
		
        
		[DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int Source
        {
            get;
            set;
        }

        
		[DataMember]
        public DateTime ExpireDate
        {
            get;
            set;
        }
       

        [DataMember]
        public string SourceDescription
        {
            get;
            set;
        }

        [DataMember]
        public int UserId
        {
            get;
            set;
        }

        [DataMember]
        public string UserName
        {
            get;
            set;
        } 
        [DataMember]
        public string SourceName
        {
            get;
            set;
        }
        [DataMember]
        public string SourceDescriptionName
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberSourceHistory()
            : base()
        {
        }

        #endregion
    }
}