﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: UserRoleMapEditor.cs
    Description:  
    Created By: pravin khot
    Created On: 2/Feb/2016
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
 * ----------------------------------------------------------------------------------------------------------------------------------------------
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "UserRoleMapEditor", Namespace = "http://www.tps360.com/types")]
    public class UserRoleMapEditor : BaseEntity
    {
        #region Properties
        [DataMember]
        public int UserId
        {
            get;
            set;
        }

        public string UserName
        {
            get;
            set;
        }
        [DataMember]
        public int RoleId
        {
            get;
            set;
        }
        [DataMember]
        public string Role
        {
            get;
            set;
        }
        [DataMember]
        public string SystemRole
        {
            get;
            set;
        }
        [DataMember]
        public int CreatorId
        {
            get;
            set;
        }
        [DataMember]
        public int UpdatorId
        {
            get;
            set;
        }
        [DataMember]
        public DateTime CreateDate
        {
            get;
            set;
        }
        [DataMember]
        public DateTime UpdateDate
        {
            get;
            set;
        }

        #endregion
    }
}
