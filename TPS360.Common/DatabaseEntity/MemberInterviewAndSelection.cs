﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberInterviewAndSelection", Namespace = "http://www.tps360.com/types")]
    public class MemberInterviewAndSelection : BaseEntity
    {
        #region Properties

        [DataMember]
        public int TradeTestStatus
        {
            get;
            set;
        }

        [DataMember]
        public int AssessmentTestStatus
        {
            get;
            set;
        }

        [DataMember]
        public int ClientContactStatus
        {
            get;
            set;
        }

        [DataMember]
        public int CompanyId
        {
            get;
            set;
        }

        [DataMember]
        public decimal Salary
        {
            get;
            set;
        }

        [DataMember]
        public int SalaryType
        {
            get;
            set;
        }

        [DataMember]
        public int SalaryCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int MedicalStatus
        {
            get;
            set;
        }

        [DataMember]
        public string MOFANumber
        {
            get;
            set;
        }

        [DataMember]
        public string PaidBy
        {
            get;
            set;
        }

        [DataMember]
        public decimal Amount
        {
            get;
            set;
        }

        [DataMember]
        public int ProviderId
        {
            get;
            set;
        }

        [DataMember]
        public int TestTypeId
        {
            get;
            set;
        }

        [DataMember]
        public DateTime MedicalTestDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime MedicalTestReceivedDate
        {
            get;
            set;
        }

        [DataMember]
        public int PCCStatus
        {
            get;
            set;
        }

        [DataMember]
        public int AttestationStatus
        {
            get;
            set;
        }

        [DataMember]
        public string DocumentName
        {
            get;
            set;
        }

        [DataMember]
        public DateTime DocumentSentDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime DocumentReceivedDate
        {
            get;
            set;
        }

        [DataMember]
        public int Status
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberInterviewAndSelection()
            : base()
        {
        }

        #endregion
    }
}