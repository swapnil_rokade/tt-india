﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberHiringProces", Namespace = "http://www.tps360.com/types")]
    public class MemberHiringProcess : BaseEntity
    {
        #region Properties

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public int JobPostingId
        {
            get;
            set;
        }

        [DataMember]
        public int InterviewLevelId
        {
            get;
            set;
        }

        [DataMember]
        public int InterviewerId
        {
            get;
            set;
        }

        [DataMember]
        public string InterviewerNote
        {
            get;
            set;
        }

        [DataMember]
        public DateTime InterviewerDate
        {
            get;
            set;
        }

        [DataMember]
        public decimal InterviewTotal
        {
            get;
            set;
        }

        [DataMember]
        public int InterviewSubmittedStages
        {
            get;
            set;
        }

        [DataMember]
        public int ApplicantMatchingPercent
        {
            get;
            set;
        }

        [DataMember]
        public string InterviewRemarks
        {
            get;
            set;
        }

      #endregion

        #region Constructor

        public MemberHiringProcess()
            : base()
        {
        }

        #endregion
    }
}