using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberTestScore", Namespace = "http://www.tps360.com/types")]
    public class MemberTestScore : BaseEntity
    {
        #region Properties

        [DataMember]
        public int TestScore
        {
            get;
            set;
        }

        [DataMember]
        public DateTime TestTakenTime
        {
            get;
            set;
        }

        [DataMember]
        public bool IsShowResult
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public int TestMasterId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberTestScore()
            : base()
        {
        }

        #endregion
    }
}