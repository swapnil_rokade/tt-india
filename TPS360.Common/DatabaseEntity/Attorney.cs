﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "Attorney", Namespace = "http://www.tps360.com/types")]
    public class Attorney : BaseEntity
    {
        #region Properties

        [DataMember]
        public string FirstName
        {
            get;
            set;
        }

        [DataMember]
        public string LastName
        {
            get;
            set;
        }

        [DataMember]
        public string MiddleName
        {
            get;
            set;
        }

        [DataMember]
        public string Email
        {
            get;
            set;
        }

        [DataMember]
        public string DayTimePhoneNumber
        {
            get;
            set;
        }

        [DataMember]
        public string Fax
        {
            get;
            set;
        }

        [DataMember]
        public string FirmName
        {
            get;
            set;
        }

        [DataMember]
        public string FirmAddress1
        {
            get;
            set;
        }

        [DataMember]
        public string FirmAddress2
        {
            get;
            set;
        }

        [DataMember]
        public string City
        {
            get;
            set;
        }

        [DataMember]
        public int StateId
        {
            get;
            set;
        }

        [DataMember]
        public int CountryId
        {
            get;
            set;
        }

        [DataMember]
        public string ZipCode
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public Attorney()
            : base()
        {
        }

        #endregion
    }
}