﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "AutoMailSending", Namespace = "http://www.tps360.com/types")]
    public class AutoMailSending : BaseEntity 
    {
        #region Properties
        [DataMember]
        public DateTime StartDate
        {
            get;
            set;
        }

        [DataMember]
        public string Repeat
        {
            get;
            set;
        }

        [DataMember]
        public DateTime EndDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime NextSendDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime LastSentDate
        {
            get;
            set;
        }
       
        [DataMember]
        public int ScheduleId
        {
            get;
            set;
        }

        [DataMember]
        public int EmailFormatId
        {
            get;
            set;
        }

        [DataMember]
        public string JobTitle
        {
            get;
            set;
        }

        [DataMember]
        public string KeyWord
        {
            get;
            set;
        }

        [DataMember]
        public string SalaryFrom
        {
            get;
            set;
        }

        [DataMember]
        public string SalaryTo
        {
            get;
            set;
        }

        [DataMember]
        public int CurrencyLookUpId
        {
            get;
            set;
        }

        [DataMember]
        public int JobPostingId
        {
            get;
            set;
        }

        [DataMember]
        public int HotListId
        {
            get;
            set;
        }

        [DataMember]
        public string City
        {
            get;
            set;
        }

        [DataMember]
        public int CountryId
        {
            get;
            set;
        }

        [DataMember]
        public int StateId
        {
            get;
            set;
        }

        [DataMember]
        public string EducationLookupId
        {
            get;
            set;
        }

        [DataMember]
        public DateTime AvailableAfter
        {
            get;
            set;
        }

        [DataMember]
        public string MinExperience
        {
            get;
            set;
        }

        [DataMember]
        public string MaxExperience
        {
            get;
            set;
        }

        [DataMember]
        public string SalaryType
        {
            get;
            set;
        }

        [DataMember]
        public int EmployementTypeLookUpId
        {
            get;
            set;
        }

        [DataMember]
        public string ResumeLastUpdated
        {
            get;
            set;
        }

        [DataMember]
        public int CandidateTypeLookUpId
        {
            get;
            set;
        }

        [DataMember]
        public int WorkScheduleLookupId
        {
            get;
            set;
        }

        [DataMember]
        public bool SecurityClearance
        {
            get;
            set;
        }

        [DataMember]
        public string WorkStatusLookUpId
        {
            get;
            set;
        }

        [DataMember]
        public string HiringStatusLookUpId
        {
            get;
            set;
        }

        [DataMember]
        public int SenderId
        {
            get;
            set;
        }

        [DataMember]
        public string Subject
        {
            get;
            set;
        }

        [DataMember]
        public string EmailBody
        {
            get;
            set;
        }


        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }
 
        #endregion

        #region Constructor

        public AutoMailSending()
            : base()
        {
        }

        #endregion
    }
}
