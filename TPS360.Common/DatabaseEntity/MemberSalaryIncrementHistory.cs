﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberSalaryIncrementHistory", Namespace = "http://www.tps360.com/types")]
    public class MemberSalaryIncrementHistory : BaseEntity
    {
        #region Properties

        [DataMember]
        public DateTime IncrementDate
        {
            get;
            set;
        }

        [DataMember]
        public string IncrementPositionName
        {
            get;
            set;
        }

        [DataMember]
        public decimal IncrementSalaryAmount
        {
            get;
            set;
        }

        [DataMember]
        public int IncrementSalaryAmountCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public decimal IncrementRatio
        {
            get;
            set;
        }

        [DataMember]
        public int IncrementRatioCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string Note
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberSalaryIncrementHistory()
            : base()
        {
        }

        #endregion
    }
}