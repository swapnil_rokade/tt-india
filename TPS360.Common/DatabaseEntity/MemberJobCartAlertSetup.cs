﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberJobCartAlertSetup", Namespace = "http://www.tps360.com/types")]
    public class MemberJobCartAlertSetup : BaseEntity
    {
        #region Properties

        [DataMember]
        public string AlertTitle
        {
            get;
            set;
        }

        [DataMember]
        public bool IsEmailAlert
        {
            get;
            set;
        }

        [DataMember]
        public bool IsSMSAlert
        {
            get;
            set;
        }

        [DataMember]
        public string JobTitle
        {
            get;
            set;
        }

        [DataMember]
        public string City
        {
            get;
            set;
        }

        [DataMember]
        public int CountryId
        {
            get;
            set;
        }

        [DataMember]
        public int StateId
        {
            get;
            set;
        }

        [DataMember]
        public string SkillSet
        {
            get;
            set;
        }

        [DataMember]
        public string EmploymentLength
        {
            get;
            set;
        }

        [DataMember]
        public decimal SalaryRangeFrom
        {
            get;
            set;
        }

        [DataMember]
        public decimal SalaryRangeTo
        {
            get;
            set;
        }

        [DataMember]
        public int SalaryRangeCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int SalaryType
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public int AlertStatus
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberJobCartAlertSetup()
            : base()
        {
        }

        #endregion
    }
}