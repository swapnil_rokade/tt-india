﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CompanyInsurance", Namespace = "http://www.tps360.com/types")]
    public class CompanyInsurance : BaseEntity
    {
        #region Properties

        [DataMember]
        public int InsuranceTypeLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string PolicyNumber
        {
            get;
            set;
        }

        [DataMember]
        public string NameOfInsurer
        {
            get;
            set;
        }

        [DataMember]
        public string NamedInsured
        {
            get;
            set;
        }

        [DataMember]
        public decimal SumInsured
        {
            get;
            set;
        }

        [DataMember]
        public int SumInsuredCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public DateTime ExpireDate
        {
            get;
            set;
        }

        [DataMember]
        public string InsurancePolicy
        {
            get;
            set;
        }

        [DataMember]
        public int CompanyId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public CompanyInsurance()
            : base()
        {
        }

        #endregion
    }
}