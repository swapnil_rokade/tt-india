using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberTestMasterMap", Namespace = "http://www.tps360.com/types")]
    public class MemberTestMasterMap : BaseEntity
    {
        #region Properties

        [DataMember]
        public bool CanRetakeTest
        {
            get;
            set;
        }

        [DataMember]
        public bool IsTestTaken
        {
            get;
            set;
        }

        [DataMember]
        public int RetakeDaysAfterTestTaken
        {
            get;
            set;
        }

        [DataMember]
        public bool IsActive
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int TestMasterId
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberTestMasterMap()
            : base()
        {
        }

        #endregion
    }
}