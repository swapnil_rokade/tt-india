﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "HRSalesCommission", Namespace = "http://www.tps360.com/types")]
    public class HRSalesCommission : BaseEntity
    {
        #region Properties

        [DataMember]
        public decimal SalesCommission
        {
            get;
            set;
        }

        [DataMember]
        public bool IsPercent
        {
            get;
            set;
        }

        [DataMember]
        public int WeeklyNumberOfSale
        {
            get;
            set;
        }

        [DataMember]
        public decimal WeeklyAmount
        {
            get;
            set;
        }

        [DataMember]
        public int MonthlyNumberOfSale
        {
            get;
            set;
        }

        [DataMember]
        public decimal MonthlyAmount
        {
            get;
            set;
        }

        [DataMember]
        public int QuaterlyNumberOfSale
        {
            get;
            set;
        }

        [DataMember]
        public decimal QuaterlyAmount
        {
            get;
            set;
        }

        [DataMember]
        public int YearlyNumberOfSale
        {
            get;
            set;
        }

        [DataMember]
        public decimal YearlyAmount
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int HROfferLetterId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public HRSalesCommission()
            : base()
        {
        }

        #endregion
    }
}