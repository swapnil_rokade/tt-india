﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "PasswordReset", Namespace = "http://www.tps360.com/types")]
    public class PasswordReset : BaseEntity
    {
        #region Properties

        [DataMember]
        public int MemberID
        {
            get;
            set;
        }

        [DataMember]
        public string Link
        {
            get;
            set;
        }

        [DataMember]
        public DateTime  RequestDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime  ExpiredDate
        {
            get;
            set;
        }

        [DataMember]
        public int IsValid
        {
            get;
            set;
        }
       

        #endregion

        #region Constructor

        public PasswordReset()
            : base()
        {
        }

        #endregion
    }
}