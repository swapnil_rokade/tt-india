﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "JobPostingNote", Namespace = "http://www.tps360.com/types")]
    public class JobPostingNote : BaseEntity
    {
        #region Properties

        [DataMember]
        public int JobPostingId
        {
            get;
            set;
        }

        [DataMember]
        public int CommonNoteId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public JobPostingNote()
            : base()
        {
        }

        #endregion
    }
}