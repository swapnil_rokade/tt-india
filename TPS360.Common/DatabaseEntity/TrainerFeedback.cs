﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "TrainerFeedback", Namespace = "http://www.tps360.com/types")]
    public class TrainerFeedback : BaseEntity
    {
        #region Properties

        [DataMember]
        public int TrainingProgramTraineeMapId
        {
            get;
            set;
        }

        [DataMember]
        public int TrainingCourseModuleTrainerMapId
        {
            get;
            set;
        }

        [DataMember]
        public int TrainingCourseModuleId
        {
            get;
            set;
        }

        [DataMember]
        public int FeedbackId
        {
            get;
            set;
        }

        [DataMember]
        public string FeedbackNote
        {
            get;
            set;
        }

        [DataMember]
        public string TraineeFeedback
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public TrainerFeedback()
            : base()
        {
        }

        #endregion
    }
}