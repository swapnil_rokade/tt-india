﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CompanyFinancialData", Namespace = "http://www.tps360.com/types")]
    public class CompanyFinancialData : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Year
        {
            get;
            set;
        }

        [DataMember]
        public decimal GrossRevenue
        {
            get;
            set;
        }

        [DataMember]
        public int GrossRevenueCurrencyLookup
        {
            get;
            set;
        }

        [DataMember]
        public decimal NetProfit
        {
            get;
            set;
        }

        [DataMember]
        public int NetProfitCurrencyLookup
        {
            get;
            set;
        }

        [DataMember]
        public int CompanyId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public CompanyFinancialData()
            : base()
        {
        }

        #endregion
    }
}