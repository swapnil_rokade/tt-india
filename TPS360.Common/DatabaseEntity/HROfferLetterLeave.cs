﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "HROfferLetterLeave", Namespace = "http://www.tps360.com/types")]
    public class HROfferLetterLeave : BaseEntity
    {
        #region Properties

        [DataMember]
        public int LeaveLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int LeaveAllowedDays
        {
            get;
            set;
        }        

        [DataMember]
        public string LeaveDescription
        {
            get;
            set;
        }

        [DataMember]
        public int HROfferLetterId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public HROfferLetterLeave()
            : base()
        {
        }

        #endregion
    }
}