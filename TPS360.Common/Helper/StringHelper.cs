/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: StringHelper.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ----------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date               Author           Modification
    ----------------------------------------------------------------------------------------------------------------------------------------    
    1.2          May-5-2009          Sandeesh          Defect #10378  To Combine the "All Keywords" and "Any Keyword" searching boxes into one "Keyword Search" box that supports boolean syntax searching */


using System;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions; //1.2
using System.Configuration;


namespace TPS360.Common.Helper
{
	public static class StringHelper
	{
		private static System.Globalization.CultureInfo CurrentCulture
		{
            [DebuggerStepThrough()]
			get
			{
				return System.Globalization.CultureInfo.CurrentCulture;
			}
		}
		
		[DebuggerStepThrough()]
		public static string Convert(string value)
		{
			if ((value == null) || (value.Trim().Length == 0))
			{
				return string.Empty;
			}
			else
			{
				return value.Trim();
			}
		}

		[DebuggerStepThrough()]
		public static string Convert(byte value)
		{
			return value.ToString(CurrentCulture);
		}

		[DebuggerStepThrough()]
		public static string Convert(string format,
		byte value)
		{
			return value.ToString(format, CurrentCulture);
		}

		[DebuggerStepThrough()]
		public static string Convert(short value)
		{
			return value.ToString(CurrentCulture);
		}

		[DebuggerStepThrough()]
		public static string Convert(string format,
		short value)
		{
			return value.ToString(format, CurrentCulture);
		}

		[DebuggerStepThrough()]
		public static string Convert(int value)
		{
			return value.ToString(CurrentCulture);
		}

		[DebuggerStepThrough()]
		public static string Convert(string format,
		int value)
		{
			return value.ToString(format, CurrentCulture);
		}

		[DebuggerStepThrough()]
		public static string Convert(long value)
		{
			return value.ToString(CurrentCulture);
		}

		[DebuggerStepThrough()]
		public static string Convert(string format,
		long value)
		{
			return value.ToString(format, CurrentCulture);
		}

		[DebuggerStepThrough()]
		public static string Convert(float value)
		{
			return value.ToString(CurrentCulture);
		}

		[DebuggerStepThrough()]
		public static string Convert(string format,
		float value)
		{
			return value.ToString(format, CurrentCulture);
		}

		[DebuggerStepThrough()]
		public static string Convert(double value)
		{
			return value.ToString(CurrentCulture);
		}

		[DebuggerStepThrough()]
		public static string Convert(string format,
		double value)
		{
			return value.ToString(format, CurrentCulture);
		}

		[DebuggerStepThrough()]
		public static string Convert(string format,
		decimal value)
		{
			return value.ToString(format, CurrentCulture);
		}

		[DebuggerStepThrough()]
		public static string Convert(decimal value)
		{
			return value.ToString(CurrentCulture);
		}

		[DebuggerStepThrough()]
		public static string Convert(DateTime value)
		{
			return value.ToString(CurrentCulture);
		}

		[DebuggerStepThrough()]
		public static string Convert(string format,
		DateTime value)
		{
			return value.ToString(format, CurrentCulture);
		}

		[DebuggerStepThrough()]
		public static string Convert(object value)
		{
			if ((value == null) || (value == DBNull.Value))
			{
				return string.Empty;
			}
			else
			{
				return value.ToString().Trim();
			}
		}

        [DebuggerStepThrough()]
        public static string Format(string format, params object[] arg)
        {
            return string.Format(CurrentCulture, format, arg);
        }

		[DebuggerStepThrough()]
		public static string[] ToArray(System.Collections.Specialized.StringCollection collection)
		{
            if ((collection == null) || (collection.Count == 0))
            {
                return null;
            }

			string[] values = new string[collection.Count];

			collection.CopyTo(values, 0);

			return values;
		}

		[DebuggerStepThrough()]
		public static string ToLower(string value)
		{
			if (IsBlank(value))
			{
				return string.Empty;
			}
			else
			{
				return value.Trim().ToLower(CurrentCulture);
			}
		}

		[DebuggerStepThrough()]
		public static string ToUpper(string value)
		{
			if (IsBlank(value))
			{
				return string.Empty;
			}
			else
			{
				return value.Trim().ToUpper(CurrentCulture);
			}
		}

		[DebuggerStepThrough()]
		public static bool IsBlank(string value)
		{
			//return ((value == null) || (value.Trim().Length == 0));
            return string.IsNullOrEmpty(value);
		}

        [DebuggerStepThrough()]
        public static bool IsBlank(StringBuilder value)
        {
            //return ((value == null) || (value.Trim().Length == 0));
            return string.IsNullOrEmpty(value.ToString());
        }

		[DebuggerStepThrough()]
		public static bool IsEqual(string leftHandSide,
		string rightHandSide,
		bool ignoreCase)
		{
			return (Compare(leftHandSide, rightHandSide, ignoreCase) == 0);
		}

		[DebuggerStepThrough()]
		public static bool IsEqual(string leftHandSide,
		string rightHandSide)
		{
			return IsEqual(leftHandSide, rightHandSide, true);
		}
		
		[DebuggerStepThrough()]
		public static int Compare(string leftHandSide,
		string rightHandSide,
		bool ignoreCase)
		{
			return string.Compare(leftHandSide, rightHandSide, ignoreCase, CurrentCulture);
		}

		[DebuggerStepThrough()]
		public static int Compare(string leftHandSide,
		string rightHandSide)
		{
			return Compare(leftHandSide, rightHandSide, true);
		}

		[DebuggerStepThrough()]
		public static string Hash(string plain)
		{
			if (IsBlank(plain)) return string.Empty;

			using(KeyedHashAlgorithm csp = HMACSHA1.Create())
            {
                csp.Key = Key;
                byte[] data = System.Text.Encoding.Default.GetBytes(plain);
                byte[] hash = csp.ComputeHash(data);

                return System.Convert.ToBase64String(hash);
            }
		}

		[DebuggerStepThrough()]
		public static string Encrypt(string plain)
		{
			if (IsBlank(plain)) return string.Empty;

			using(SymmetricAlgorithm crypto = CreateAlgorithm())
			{
				return System.Convert.ToBase64String(Read(crypto.CreateEncryptor(), Encoding.Default.GetBytes(plain)));
			}
		}

		[DebuggerStepThrough()]
		public static string Decrypt(string cipher)
		{
			if (IsBlank(cipher)) return string.Empty;

			using(SymmetricAlgorithm crypto = CreateAlgorithm())
			{
				return Encoding.Default.GetString(Read(crypto.CreateDecryptor(), System.Convert.FromBase64String(cipher)));
			}
		}

		[DebuggerStepThrough()]
		private static SymmetricAlgorithm CreateAlgorithm()
		{
			SymmetricAlgorithm crypto = new RijndaelManaged();

			crypto.Key = Key;
            crypto.IV = new byte[crypto.IV.Length];

			return crypto;
		}

		[DebuggerStepThrough()]
		private static byte[] Read(ICryptoTransform transformer,
		byte[] data)
		{
			using(MemoryStream memoryStream = new MemoryStream())
			{
				using(CryptoStream cryptoStream = new CryptoStream(memoryStream, transformer, CryptoStreamMode.Write))
				{
					cryptoStream.Write(data, 0, data.Length);
					cryptoStream.FlushFinalBlock();

					return  memoryStream.ToArray();
				}
			}
		}

       // [DebuggerStepThrough()]
        private static string ReplaceUnWantedCharacter(string srtInput)
        {
            //input = input.Replace('(', ',');
            //input = input.Replace(')', ',');

            //input = input.Replace(", ", ",");
            //input = input.Replace(" ,", ",");
            //input = input.Replace(' ', ',');
           // input = input.Replace("++", "--"); // 10283
           // input = input.Replace('+', ',');   // 10283
           // input = input.Replace("--", "-");  // 10283

            //input = input.Replace('[', ',');  // 10290
            //input = input.Replace(']', ',');  // 10290

            srtInput = srtInput.Replace('&', ',');  
            srtInput = srtInput.Replace("(", string.Empty);   // 10289
            srtInput = srtInput.Replace(")", string.Empty);  // 10289
            srtInput = srtInput.Replace("[", string.Empty);  // 10290
            srtInput = srtInput.Replace("]", string.Empty);  // 10290    

            return srtInput;
        }

        //[DebuggerStepThrough()]
        public static string[] BuildKeyWordArray(string searchString)
        {
            string correctedString = ReplaceUnWantedCharacter(searchString);
            string[] keyWordArray = correctedString.Split(',');
            return keyWordArray;
        }

        //1.2 start
        public static string[] BuildSearchKeyWordArray(string searchString)
        {
            string correctedString = ReplaceUnWantedCharacter(searchString);
            string pattern = @"(\b and \b)|(\b or \b)"; // Split the keywords based on boolean operators
            string[] keyWordArray = Regex.Split(correctedString, pattern, RegexOptions.IgnoreCase);
            return keyWordArray;
        }//1.2 End

        private static readonly byte[] Key = new byte[] {4, 93, 171, 3, 85, 23, 41, 34, 216, 14, 78, 156, 78, 3, 103, 154, 9, 150, 65, 54, 226, 95, 68, 79, 159, 36, 246, 57, 177, 107, 116, 8};

        public static string getDataBaseName(string connectionStringName)
        {
            
             string DbName ="TPS360Dev";
            int startIndex= ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString.IndexOf("Catalog=") + 8 ;
             int lastIndex=  ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString.IndexOf(";User");
             if (lastIndex > startIndex )
            {
                DbName = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString.Substring(startIndex, lastIndex - startIndex);
            }
          return DbName;
        }
	}
}