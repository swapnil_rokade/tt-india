﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "ReferenceQueryReferenceCheck", Namespace = "http://www.tps360.com/types")]
    public class ReferenceQueryReferenceCheck : BaseEntity
    {
        #region Properties

        [DataMember]
        public int ReferenceQueryId
        {
            get;
            set;
        }

        [DataMember]
        public string QueryQuestion
        {
            get;
            set;
        }

        [DataMember]
        public string Response
        {
            get;
            set;
        }

        [DataMember]
        public int ReferenceQueryReferenceCheckId
        {
            get;
            set;
        }

        [DataMember]
        public string LinkToRespond
        {
            get;
            set;
        }

        [DataMember]
        public DateTime ResponseDate
        {
            get;
            set;
        }
        [DataMember]
        public int MemberReferenceId
        {
            get;
            set;
        }

      

        #endregion

        #region Constructor

        public ReferenceQueryReferenceCheck()
            : base()
        {
        }

        #endregion
    }
}
