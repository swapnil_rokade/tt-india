﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "InterviewInterviewrs", Namespace = "http://www.tps360.com/types")]
    public class InterviewInterviewrs : InterviewInterviewerMap
    {
        #region Properties

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public string PrimaryEmail
        {
            get;
            set;
        }

        [DataMember]
        public string SecondaryEmail
        {
            get;
            set;
        }

        [DataMember]
        public string FirstName
        {
            get;
            set;
        }

        [DataMember]
        public string MiddleName
        {
            get;
            set;
        }

        [DataMember]
        public string LastName
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public InterviewInterviewrs()
            : base()
        {
        }

        #endregion
    }
}
