
/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: DuplicateMember.cs
    Description: This page is used  to find Duplicate Members
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date               Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Sept-08-2009       Sukanta Ghorui      Added Properties to find Duplicate Members
-------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]

    public class DuplicateMember : Member
    {
        #region Properties

        [DataMember]
        public string SSN
        {
            get;
            set;
        }

        [DataMember]
        public string StateName
        {
            get;
            set;
        }

        [DataMember]
        public string CountryName
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public DuplicateMember()
            : base()
        {
        }

        #endregion
    }
}