﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "DynamicDictionary", Namespace = "http://www.tps360.com/types")]
    public class DynamicDictionary
    {
        #region Properties

        public Dictionary<string, object> properties = new Dictionary<string, object>();

        public object this[string name]
        {
            get
            {
                if (properties.ContainsKey(name))
                {
                    return properties[name];
                }
                return null;
            }
            set
            {
                properties[name] = value;
            }

        }
        #endregion

        #region Constructor


        #endregion
    }
}