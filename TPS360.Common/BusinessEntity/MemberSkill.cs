﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberSkill", Namespace = "http://www.tps360.com/types")]
    public class MemberSkill : MemberSkillMap
    {
        #region Properties

        [DataMember]
        public string Proficiency
        {
            get;
            set;
        }

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public string Description
        {
            get;
            set;
        }

        [DataMember]
        public int CategoryId
        {
            get;
            set;
        }

        [DataMember]
        public string Category
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public MemberSkill()
            : base()
        {
        }

        #endregion
    }
}