﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "BusinessOverview", Namespace = "http://www.tps360.com/types")]
    public class BusinessOverview
    {
        #region Properties

        [DataMember]
        public int TodaysApplicantCount
        {
            get;
            set;
        }

        [DataMember]
        public int LastSevenDaysApplicantCount
        {
            get;
            set;
        }

        [DataMember]
        public int LastThirtyDaysApplicantCount
        {
            get;
            set;
        }

        [DataMember]
        public int TotalApplicantCount
        {
            get;
            set;
        }

        [DataMember]
        public int TodaysCompanyCount
        {
            get;
            set;
        }

        [DataMember]
        public int LastSevenDaysCompanyCount
        {
            get;
            set;
        }

        [DataMember]
        public int LastThirtyDaysCompanyCount
        {
            get;
            set;
        }

        [DataMember]
        public int TotalCompanyCount
        {
            get;
            set;
        }

        [DataMember]
        public int TodaysRequisitionCount
        {
            get;
            set;
        }

        [DataMember]
        public int LastSevenDaysRequisitionCount
        {
            get;
            set;
        }

        [DataMember]
        public int LastThirtyDaysRequisitionCount
        {
            get;
            set;
        }

        [DataMember]
        public int TotalRequisitionCount
        {
            get;
            set;
        }

        [DataMember]
        public int TodaysPlacementCount
        {
            get;
            set;
        }

        [DataMember]
        public int LastSevenDaysPlacementCount
        {
            get;
            set;
        }

        [DataMember]
        public int LastThirtyDaysPlacementCount
        {
            get;
            set;
        }

        [DataMember]
        public int TotalPlacementCount
        {
            get;
            set;
        }

        [DataMember]
        public int TodaysConsultantCount
        {
            get;
            set;
        }

        [DataMember]
        public int LastSevenDaysConsultantCount
        {
            get;
            set;
        }

        [DataMember]
        public int LastThirtyDaysConsultantCount
        {
            get;
            set;
        }

        [DataMember]
        public int TotalConsultantCount
        {
            get;
            set;
        }

        [DataMember]
        public int TotalTaskCount
        {
            get;
            set;
        }

        [DataMember]
        public int TotalCompletedTaskCount
        {
            get;
            set;
        }

        [DataMember]
        public int TotalManagerCount
        {
            get;
            set;
        }

        [DataMember]
        public int TotalTrainingCount
        {
            get;
            set;
        }

        [DataMember]
        public decimal TotalHoursWorked
        {
            get;
            set;
        }

        [DataMember]
        public decimal TotalOverTimeWorked
        {
            get;
            set;
        }

        [DataMember]
        public int TotalWorkDays
        {
            get;
            set;
        }

        [DataMember]
        public int TotalLateDays
        {
            get;
            set;
        }

        [DataMember]
        public int TotalAbsentdays
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public BusinessOverview()
            : base()
        {
        }

        #endregion
    }
}
