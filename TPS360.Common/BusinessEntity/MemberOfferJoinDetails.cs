﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberOfferJoinDetails", Namespace = "http://www.tps360.com/types")]
    public class MemberOfferJoinDetails : MemberHiringDetails 
    {
        #region Properties

        [DataMember]

        public string OfferCategoryName
        {
            get;
            set;
        }

        [DataMember]
        public DateTime  OfferDeclinedDate
        {
            get;
            set;
        }

        [DataMember]
        public string ReasonForRejection
        {
            get;
            set;
        }
        [DataMember]
        public DateTime  ActualDateOfJoining
        {
            get;
            set;
        }

        [DataMember]
        public string PSNo
        {
            get;
            set;
        }
        [DataMember]
        public string CurrentCompany
        {
            get;
            set;
        }
        [DataMember]
        public string WorkLocation
        {
            get;
            set;
        }
        [DataMember]
        public string Skills
        {
            get;
            set;
        }
        [DataMember]
        public string EducationalQualification
        {
            get;
            set;
        }
        #endregion
        [DataMember]
        public int BUID
        {
            get;
            set;
        }
        [DataMember]
        public string BUNAME
        {
            get;
            set;
        }
        [DataMember]
        public string OfferedCTC
        {
            get;
            set;
        }
        [DataMember]
        public string BVSTATUSTEXT
        {
            get;
            set;
        }
        [DataMember]
        public string FITMENTPERCENTILETEXT
        {
            get;
            set;
        }
        [DataMember]
        public string SOURCENAME 
        {
            get;
            set;
        }
        [DataMember]
        public string SOURCEDESCRIPTIONNAME
        {
            get;
            set;
        }

        [DataMember]
        public string BillableSalary
        {
            get;
            set;
        }
        [DataMember]
        public string BillingRate
        {
            get;
            set;
        }
        [DataMember]
        public string Revenue
        {
            get;
            set;
        }
        [DataMember]
        public string OfferLocation
        {
            get;
            set;
        }

       
        #region Constructor

        public MemberOfferJoinDetails()
            : base()
        {
        }

        #endregion
    }
}