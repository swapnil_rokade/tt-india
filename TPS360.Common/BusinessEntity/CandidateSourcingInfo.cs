﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CandidateSourcingInfo", Namespace = "http://www.tps360.com/types")]
    public class CandidateSourcingInfo : Member
    {
        #region Properties

        [DataMember]
        public string CandidateName
        {
            get;
            set; 
        }

        [DataMember]
        public string Requisition
        {
            get;
            set;
        }

        [DataMember]
        public string Account
        {
            get;
            set;
        }

        [DataMember]
        public string JobPostingCode
        {
            get;
            set;
        }

        [DataMember]
        public string Location
        {
            get;
            set;
        }        

        [DataMember]
        public DateTime SourcedDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime ClientSubmissionDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime OfferedDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime Joineddate
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentStatus
        {
            get;
            set;
        }

        [DataMember]
        public string Recruiter
        {
            get;
            set;
        }
         
        [DataMember]
        public string ResumeSourceName
        {
            get;
            set;
        }

        [DataMember]
        public string Lead
        {
            get;
            set;
        }

        [DataMember]
        public string Remarks
        {
            get;
            set;
        }
       

        #endregion

        #region Constructor

        public CandidateSourcingInfo()
            : base()
        {
        }

        #endregion
    }
}
