﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberWorkOrderProfitAndLoss", Namespace = "http://www.tps360.com/types")]
    public class MemberWorkOrderProfitAndLoss : WorkOrderProfitAndLossDetail 
    {
        #region Properties

        [DataMember]
        public decimal Received
        {
            get{ return RegularHourIncome+OverTimeHourIncome+ExpenseIncome;}
        }

        [DataMember]
        public decimal Payment
        {
            get { return RegularHourCost + OverTimeHourCost + ExpensesCost; }
        }

        [DataMember]
        public int ConsultantId
        {
            get;
            set;
        }

        [DataMember]
        public string ReferenceNumber
        {
            get;
            set;
        }

        [DataMember]
        public string ConsultantName
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public MemberWorkOrderProfitAndLoss()
            : base()
        {
        }

        #endregion
    }
}