﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MenuListCount", Namespace = "http://www.tps360.com/types")]
    public class VendorSubmissions: BaseEntity
    {
        #region Properties

     

        [DataMember]
        public string ContactName
        {
            get;
            set;
        }

        [DataMember]
        public int VendorID
        {
            get;
            set;
        }


        [DataMember]
        public string  VendorName
        {
            get;
            set;
        }
        [DataMember]
        public int JobPostingID
        {
            get;
            set;
        }

        [DataMember]
        public string JobTitle
        {
            get;
            set;
        }

        [DataMember]
        public int CandidateId
        {
            get;
            set;
        }

        [DataMember]
        public string CandidateName
        {
            get;
            set;
        }

        [DataMember ]
        public DateTime SubmittedDate
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentStatus
        {
            get;
            set;
        }
        #endregion

        #region Constructor

        public VendorSubmissions()
            : base()
        {
        }

        
        #endregion
    }
}