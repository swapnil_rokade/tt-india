﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CandidateReportDashboard", Namespace = "http://www.tps360.com/types")]
    public class CandidateReportDashboard : BaseEntity
    {
        #region Properties

        [DataMember]
        public int NewApplicant
        {
            get;
            set;
        }

        [DataMember]
        public int UpdatedApplicant
        {
            get;
            set;
        }

        [DataMember]
        public int ByInterview
        {
            get;
            set;
        }

        [DataMember]
        public int ByIndustry
        {
            get;
            set;
        }

        [DataMember]
        public int ByFunctionalCategory
        {
            get;
            set;
        }

        [DataMember]
        public int ByWorkPermit
        {
            get;
            set;
        }

        [DataMember]
        public int ByGender
        {
            get;
            set;
        }

        [DataMember]
        public int ByMaritalStatus
        {
            get;
            set;
        }

        [DataMember]
        public int ByEducation
        {
            get;
            set;
        }

        [DataMember]
        public int ByPerformedTests
        {
            get;
            set;
        }

        [DataMember]
        public int ByBroadCastedResume
        {
            get;
            set;
        }

        [DataMember]
        public int ByReferred
        {
            get;
            set;
        }

        [DataMember]
        public int ByJobAgent
        {
            get;
            set;
        }

        [DataMember]
        public int ByLocation
        {
            get;
            set;
        }

        [DataMember]
        public int ByAssignedManager
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public CandidateReportDashboard()
            : base()
        {
        }

        #endregion
    }
}