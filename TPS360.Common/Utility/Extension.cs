using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace TPS360.Common.BusinessEntities
{
    public static class DataTimeExtension
    {
        public static string Ago(this DateTime target)
        {
            StringBuilder result = new StringBuilder();
            TimeSpan diff = (DateTime.Now - target.ToLocalTime());

            if (diff.Days > 0)
            {
                result.AppendFormat("{0} days", diff.Days);
            }

            if (diff.Hours > 0)
            {
                if (result.Length > 0)
                {
                    result.Append(", ");
                }

                result.AppendFormat("{0} hours", diff.Hours);
            }

            if (diff.Minutes > 0)
            {
                if (result.Length > 0)
                {
                    result.Append(", ");
                }

                result.AppendFormat("{0} minutes", diff.Minutes);
            }

            if (result.Length == 0)
            {
                result.Append("few moments");
            }

            return result.ToString();
        }

    }

    public static class StringExtension
    {
        public static bool ContainsIgnoreCase(this string left, string right)
        {
            var pattern = new Regex(right, RegexOptions.IgnoreCase);
            return pattern.IsMatch(left);
        }

        public static bool IsNotNullOrEmpty(this string input)
        {
            return !String.IsNullOrEmpty(input);
        }

        public static bool IsNullOrEmpty(this string input)
        {
            return String.IsNullOrEmpty(input);
        }

        public static string Fill(this string format, params object[] args)
        {
            return String.Format(format, args);
        }

        public static string CssMinify(this string input)
        {
            // add minify logic here
            return input;
        }

        public static bool EqualsIgnoreCase(this string left, string right)
        {
            return String.Compare(left, right, true) == 0;
        }
    }

    public static class PathExtension
    {
        
    }
}