using System;

namespace TPS360.Common
{
	internal sealed class MailMap
	{
		private string _from;
		private string _to;
		private string _cc;
		private string _bcc;
		private string _subject;
		private string _body;

		public string From
		{
			get
			{
				return _from;
			}
		}


		public string To
		{
			get
			{
				return _to;
			}
		}


		public string CC
		{
			get
			{
				return _cc;
			}
		}


		public string Bcc
		{
			get
			{
				return _bcc;
			}
		}


		public string Subject
		{
			get
			{
				return _subject;
			}
		}


		public string Body
		{
			get
			{
				return _body;
			}
		}

		
		public MailMap(string from,
			string to,
			string cc,
			string bcc,
			string subject,
			string body)
		{
			_from = from;
			_to = to;
			_cc = cc;
			_bcc = bcc;
			_subject = subject;
			_body = body;
		}
	}
}