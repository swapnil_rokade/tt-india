﻿using System;

namespace TPS360.Controls
{
    public interface IResourceControl
    {
        String ResourceName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the name of the resource to dispaly.
        /// </summary>
        String ResourceGroup
        {
            get;
            set;
        }
    }
}