﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI.WebControls;

namespace TPS360.Controls
{
    public class ResourceRequiredFieldValidator : RequiredFieldValidator, IResourceControl
    {
        [
        Bindable(true),
        Category("Appearance"),
        Description("Gets or sets the name of the resource to dispaly."),
        DefaultValue(""),
        ]
        public String ResourceName
        {
            get
            {
                Object state = ViewState["ResourceName"];
                
                if (state != null)
                {
                    return (String)state;
                }

                return string.Empty;
            }
            set
            {
                ViewState["ResourceName"] = value;
            }
        }

        [
        Bindable(true),
        Category("Appearance"),
        Description("Gets or sets the group of the resource."),
        DefaultValue(""),
        ]
        public String ResourceGroup
        {
            get
            {
                Object state = ViewState["ResourceGroup"];

                if (state != null)
                {
                    return (String)state;
                }

                return string.Empty;
            }
            set
            {
                ViewState["ResourceGroup"] = value;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (!string.IsNullOrEmpty(ResourceName) && !string.IsNullOrEmpty(ResourceGroup))
            {
                this.ErrorMessage = HttpContext.GetGlobalResourceObject(ResourceGroup, ResourceName) as string;
            }
            else
            {
                this.ErrorMessage = HttpContext.GetLocalResourceObject(HttpContext.Current.Request.Path, ResourceName) as string;
            }
        }
    }
}